#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen tW production (top), DR scheme, dynamic scale, dilepton, pTHard=1, hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, new LHE'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', '2lepton' ]
evgenConfig.contact     = [ 'tetiana.moskalets@cern.ch', 'qibin.liu@cern.ch' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31_tW.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

