#Based on similar Herwig JOs (eg 600253)

shower = "H7"
#decay = "gg" 
#decay = "bb"
decay = "ss"
#decay = "cc"
ma = 2.05 #GeV
mZ = 91.19 #GeV
wZ= 2.4952 #GeV


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'Powheg + '+shower+' ZH, H->aa->'+decay+decay+' with m_a = '+str(ma)+' GeV  By-hand decay of the H and a using a flat matrix element.'
evgenConfig.keywords    =['Higgs','BSM']
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'panagiotis.bellos@cern.ch']

import random
import math
import ROOT

class particle:

    def __init__(self, px=0, py=0, pz=0, m=0):
        self.px = px
        self.py = py
        self.pz = pz
        self.m = m
        pass

    def p(self):
        return (self.px ** 2 + self.py ** 2 + self.pz ** 2) ** 0.5

    def E(self):
        return (self.px ** 2 + self.py ** 2 + self.pz ** 2 + self.m ** 2) ** 0.5

    def boost(self, betavec):
        bx = betavec[0]
        by = betavec[1]
        bz = betavec[2]
        beta = (bx ** 2 + by ** 2 + bz ** 2) ** 0.5
        gamma = 1. / (1. - beta ** 2) ** 0.5
        E = self.E()
        mydotprod = bx * self.px + by * self.py + bz * self.pz
        Enew = gamma * (E + mydotprod)
        self.px = self.px + (gamma - 1.) / (beta ** 2) * \
            mydotprod * bx + gamma * E * bx
        self.py = self.py + (gamma - 1.) / (beta ** 2) * \
            mydotprod * by + gamma * E * by
        self.pz = self.pz + (gamma - 1.) / (beta ** 2) * \
            mydotprod * bz + gamma * E * bz
        E = Enew
    pass

def two_body_decay(mM, mD, mS):
    #M = mother, D = daughter, S = son
    pD = abs((mM ** 2 - (mD - mS) ** 2) * (mM ** 2 - (mD + mS) ** 2)) ** 0.5 / (2. * mM)
    cosThetaD = random.uniform(-1., 1.)
    sinThetaD = (1. - pow(cosThetaD, 2)) ** 0.5
    phiD = random.uniform(0., 2. * math.pi)
    p4D = particle(pD * sinThetaD * math.cos(phiD), pD *sinThetaD * math.sin(phiD), pD * cosThetaD, mD)
    p4S = particle(-pD * sinThetaD * math.cos(phiD), -pD *sinThetaD * math.sin(phiD), -pD * cosThetaD, mS)
    return p4D, p4S

def scinot(myin):
    if (myin < 0):
        return format(myin, "1.9E")
    else:
        return " "+format(myin, "1.9E")

md = 1.
if (decay == 'gg'):
    md = 0.1 #had problems with square root of negative number if this was not set to some small but positive value.
elif (decay == 'bb'):
    md = 5. 
elif (decay == 'cc'):
    md = 1.3 
elif (decay == 'ss'):
    md = 0.1 
    pass

import os, sys, glob
myfiles = []
random_gen = ROOT.TRandom()

for f in glob.glob("*.events"):
    
    myfiles+=[f]
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    
    for line in f1:
       
        if line.startswith('      25     1'):
                       
            px = float(line.split()[6])
            py = float(line.split()[7])
            pz = float(line.split()[8])
            E = float(line.split()[9])
            m = float(line.split()[10])
            p4H = particle(px,py,pz,m)
            mz=random_gen.BreitWigner(mZ, wZ) #Generate Z mass
            if mz<=0 :    # very rare negative values to be replaced
             mz=mZ
            if mz>120 :  #if mz+ma>mH the kinematics calculation fails
             mz=120
            p4a1,p4a2 = two_body_decay(m, mz, ma)
            betaH = [p4H.px / p4H.E(), p4H.py / p4H.E(), p4H.pz / p4H.E()]
            p4a1.boost(betaH)
            p4a2.boost(betaH)
            p4a2a,p4a2b = two_body_decay(ma, md, md)
            betaa2 = [p4a2.px / p4a2.E(), p4a2.py / p4a2.E(), p4a2.pz / p4a2.E()]
            p4a2a.boost(betaa2)
            p4a2b.boost(betaa2)
            p4a1px = float(scinot(p4a1.px))   #Z
            p4a1py = float(scinot(p4a1.py))
            p4a1pz = float(scinot(p4a1.pz))
            p4a1E = float(scinot(p4a1.E()))
            p4a2px = float(scinot(px-p4a1px))  #a
            p4a2py = float(scinot(py-p4a1py))
            p4a2pz = float(scinot(pz-p4a1pz))
            p4a2E = float(scinot(E-p4a1E))
            p4a2apx = float(p4a2a.px)                 #q1
            p4a2apy = float(p4a2a.py)
            p4a2apz = float(p4a2a.pz)
            p4a2aE = float(p4a2a.E())
            p4a2bpx = float(scinot(p4a2px - p4a2apx)) #q2
            p4a2bpy = float(scinot(p4a2py - p4a2apy))
            p4a2bpz = float(scinot(p4a2pz - p4a2apz))
            p4a2bE = float(scinot(p4a2E - p4a2aE))
            p4a1m = (p4a1E**2-p4a1px**2-p4a1py**2-p4a1pz**2)**0.5
            p4a2m = (p4a2E**2-p4a2px**2-p4a2py**2-p4a2pz**2)**0.5
            p4a2am = (p4a2aE**2-p4a2apx**2-p4a2apy**2-p4a2apz**2)**0.5
            p4a2bm = (p4a2bE**2-p4a2bpx**2-p4a2bpy**2-p4a2bpz**2)**0.5
            f2.write('      23     1     1     2     0     0 '+scinot(p4a1px)+" "+scinot(p4a1py)+" "+scinot(p4a1pz)+" "+scinot(p4a1E)+" "+scinot(p4a1m)+"  0.00000E+00  9.000E+00\n")
            f2.write('      25     2     1     2     0     0 '+scinot(p4a2px)+" "+scinot(p4a2py)+" "+scinot(p4a2pz)+" "+scinot(p4a2E)+" "+scinot(p4a2m)+"  0.00000E+00  9.000E+00\n")
            f2.write('      3     1     4     4   522   523 '+scinot(p4a2apx)+" "+scinot(p4a2apy)+" "+scinot(p4a2apz)+" "+scinot(p4a2aE)+" "+scinot(p4a2am)+"  0.00000E+00  9.000E+00\n")
            f2.write('      -3     1     4     4   523   522 '+scinot(p4a2bpx)+" "+scinot(p4a2bpy)+" "+scinot(p4a2bpz)+" "+scinot(p4a2bE)+" "+scinot(p4a2bm)+"  0.00000E+00  9.000E+00\n")
        elif '4  10001' in line:
            f2.write(line.replace("4  10001","7   10001"))
        elif '5  10001' in line:
            f2.write(line.replace("5  10001","8  10001"))
        elif '7  10001' in line:
            f2.write(line.replace("7  10001","10  10001"))
        elif '6  10001' in line:
            f2.write(line.replace("6  10001","9  10001"))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )
    pass

    
# initialize Herwig7 generator configuration for showering of LHE files 
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7  
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename='merged_lhef._0.events', me_pdf_order="NLO")
#Herwig7Config.lhef_powhegbox_commands(lhe_filename="PowhegOTF._1.events", me_pdf_order="NLO")
    
Herwig7Config.add_commands(""" do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+; """)
    
# add EvtGen 
include("Herwig7_i/Herwig71_EvtGen.py")
Herwig7Config.run()

evgenConfig.inputFilesPerJob = 5
evgenConfig.nEventsPerJob = 10000
