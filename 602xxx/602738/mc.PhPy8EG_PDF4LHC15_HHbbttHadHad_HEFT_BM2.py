#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 15 -15 ", # tautau decay
                             "24:mMin = 0", # W minimum mass
                             "24:mMax = 99999", # W maximum mass
                             "23:mMin = 0", # Z minimum mass
                             "23:mMax = 99999", # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to bbtautau, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar", "bottom", "tau"]
evgenConfig.contact        = ['Yanlin Liu <yanlin@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 100

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HTauTauFilter", PDGParent = [25], PDGChild = [15])

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended("TauTauHadHadFilter")
filtSeq.TauTauHadHadFilter.PDGGrandParent = 25
filtSeq.TauTauHadHadFilter.PDGParent = 15
filtSeq.TauTauHadHadFilter.StatusParent = 2
filtSeq.TauTauHadHadFilter.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.TauTauHadHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.IncludeHadTaus = True
filtSeq.LepTauPtFilter.NLeptons = 2
filtSeq.LepTauPtFilter.MinPt = 13000.
filtSeq.LepTauPtFilter.MinVisPtHadTau = 15000.
filtSeq.LepTauPtFilter.MaxEta = 3.

#---------------------------------------------------------------------------------------------------
# Leading tau filter
#---------------------------------------------------------------------------------------------------
filtSeq += MultiElecMuTauFilter("LeadTauPtFilter")
filtSeq.LeadTauPtFilter.IncludeHadTaus = True
filtSeq.LeadTauPtFilter.NLeptons = 1
filtSeq.LeadTauPtFilter.MinPt = 13000.
filtSeq.LeadTauPtFilter.MinVisPtHadTau = 25000.
filtSeq.LeadTauPtFilter.MaxEta = 3.

filtSeq.Expression = "HbbFilter and HTauTauFilter and TauTauHadHadFilter and LepTauPtFilter and LeadTauPtFilter"
