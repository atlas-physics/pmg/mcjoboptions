#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+W+jet->Wincl+WW->Wincl+lvlv production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia showering with A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs->WW->lvlv at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603249.Ph_PDF4LHC21_WpH125J_Wincl_MINLO_HWW_batch1_LHE.evgen.TXT.e8557
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->Wincl+WW->Wincl+lvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'michiel.jan.veen@cern.ch' ]
evgenConfig.inputFilesPerJob = 58
evgenConfig.nEventsPerJob    = 60000
evgenConfig.process = "WpH, Wp->inclusive, H->WW->lvlv"
