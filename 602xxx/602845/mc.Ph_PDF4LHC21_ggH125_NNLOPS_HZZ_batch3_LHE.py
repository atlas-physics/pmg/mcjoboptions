# EVGEN configuration
# Note:  This JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg ggF NNLOPS PDF4LHC21'
evgenConfig.keywords       = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact        = [ 'sabidi@cern.ch', 'xinmeng.ye@cern.ch' ]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob  = 500

#--------------------------------------------------------------
# Powheg ggF setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Hj_Common.py')
PowhegConfig.mass_H = 125.0
PowhegConfig.width_H = 0.00407


PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + [27100] + [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5 ,1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]

PowhegConfig.foldcsi         = 10
PowhegConfig.foldy           = 10
PowhegConfig.foldphi         = 10

PowhegConfig.NNLO_reweighting_inputs['nnlo'] = 'CM136-PDF4LHC40-APX2-HH.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-QCDdn'] = 'CM136-PDF4LHC40-APX2-QQ.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-QCDup'] = 'CM136-PDF4LHC40-APX2-11.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-QCD2up'] = 'CM136-PDF4LHC40-APX2-22.top'

PowhegConfig.NNLO_reweighting_inputs['nnlo-mb'] = 'CM136-MB-PDF4LHC40-APX2-HH.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-mb-QCDdn'] = 'CM136-MB-PDF4LHC40-APX2-QQ.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-mb-QCDup'] = 'CM136-MB-PDF4LHC40-APX2-11.top'
PowhegConfig.NNLO_reweighting_inputs['nnlo-mb-QCD2up'] = 'CM136-MB-PDF4LHC40-APX2-22.top'

PowhegConfig.NNLO_output_weights["nnlops-nominal"] = "combine 'nnlo' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-nominal-QCD2up"] = "combine 'nnlo-QCD2up' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-mb-nominal"] = "combine 'nnlo-mb' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-mb-QCDdn"] = "combine 'nnlo-mb-QCDdn' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-mb-QCDup"] = "combine 'nnlo-mb-QCDup' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-mb-QCD2up"] = "combine 'nnlo-mb-QCD2up' and '0'"
# nominal NNLO scale (mH/2 in HNNLO), with varied Powheg scales
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnDn"] = "combine 'nnlo' and '1001'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnNom"] = "combine 'nnlo' and '1002'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgDnUp"] = "combine 'nnlo' and '1003'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgNomDn"] = "combine 'nnlo' and '1004'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgNomUp"] = "combine 'nnlo' and '1005'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpDn"] = "combine 'nnlo' and '1006'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpNom"] = "combine 'nnlo' and '1007'"
PowhegConfig.NNLO_output_weights["nnlops-nnloNom-pwgUpUp"] = "combine 'nnlo' and '1008'"
# low NNLO scale (mH/4 in HNNLO), with varied Powheg scales
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomNom"] = "combine 'nnlo-QCDdn' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnDn"] = "combine 'nnlo-QCDdn' and '1001'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnNom"] = "combine 'nnlo-QCDdn' and '1002'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgDnUp"] = "combine 'nnlo-QCDdn' and '1003'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomDn"] = "combine 'nnlo-QCDdn' and '1004'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgNomUp"] = "combine 'nnlo-QCDdn' and '1005'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpDn"] = "combine 'nnlo-QCDdn' and '1006'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpNom"] = "combine 'nnlo-QCDdn' and '1007'"
PowhegConfig.NNLO_output_weights["nnlops-nnloDn-pwgUpUp"] = "combine 'nnlo-QCDdn' and '1008'"
# high NNLO scale (mu=mH0) and varied powheg scales
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomNom"] = "combine 'nnlo-QCDup' and '0'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnDn"] = "combine 'nnlo-QCDup' and '1001'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnNom"] = "combine 'nnlo-QCDup' and '1002'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgDnUp"] = "combine 'nnlo-QCDup' and '1003'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomDn"] = "combine 'nnlo-QCDup' and '1004'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgNomUp"] = "combine 'nnlo-QCDup' and '1005'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpDn"] = "combine 'nnlo-QCDup' and '1006'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpNom"] = "combine 'nnlo-QCDup' and '1007'"
PowhegConfig.NNLO_output_weights["nnlops-nnloUp-pwgUpUp"] = "combine 'nnlo-QCDup' and '1008'"

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate(remove_oldStyle_rwt_comments=True)
