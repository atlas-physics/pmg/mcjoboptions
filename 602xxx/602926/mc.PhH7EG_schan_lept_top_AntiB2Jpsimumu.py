# based on 600020 and 602235(Herwig7.2)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark s-channel production (top), inclusive, H7.2-Default tune, ME NNPDF30 NLO, Anti-B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'sChannel', 'inclusive', 'Jpsi' ]
evgenConfig.contact     = [ 'serena.palazzo@cern.ch, asada@hepl.phys.nagoya-u.ac.jp, derue@lpnhe.in2p3.fr' ]
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 50
evgenConfig.tune = "H7.2-Default"

#--------------------------------------------------------------
# Herwig7 showering with the H7UE MMHT2014 tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")


# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['AntiB2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'AntiB2Jpsimumu.DEC'

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
