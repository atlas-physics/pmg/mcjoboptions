# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

evgenConfig.description = "POWHEG+HERWIG7 EW Wjj production (Wm->enu)"
evgenConfig.keywords = ["SM", "W", "2jet", "electroweak"]
evgenConfig.contact = ["philippe.calfayan@cern.ch"]
evgenConfig.generators  = ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.tune     = "H7.1-Default"

#--------------------------------------------------------------                 
# Herwig7 showering                                                             
#--------------------------------------------------------------                 
# initialize Herwig7 generator configuration for showering of LHE files         
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7                                                             
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                   
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7                                                                                                                                  
Herwig7Config.run()


