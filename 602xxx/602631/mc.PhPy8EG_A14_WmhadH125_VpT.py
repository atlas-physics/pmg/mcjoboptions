#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 ZH+jet->qqbbar production
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_HWj_Common.py")

PowhegConfig.nubound=200000
PowhegConfig.xupbound=5

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode = "w- > j j"
PowhegConfig.mass_W_low = 10.

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PDF4LHC21 PDF variations (keeping also variation for PDF4LHC15 correlation studies and NNPDF3.0 for correlation with bkg samples if needed)
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + [260000] + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 200
evgenConfig.description = "POWHEG+MiNLO+Pythia8 ZH+jet->qqbbbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators   = ['Powheg']
evgenConfig.process = "WH, H->bb, W->qq"
