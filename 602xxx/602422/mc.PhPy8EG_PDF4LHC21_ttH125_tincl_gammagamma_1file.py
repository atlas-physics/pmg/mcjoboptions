# EVGEN configuration
#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]



#--------------------------------------------------------------
# Higgs at Pythia8 (H->yy, dipole recoil, removing Dalitz decays)
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',     # decay of Higgs
                             '25:onIfMatch = 22 22' ]

genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ttH NLO tt~->all, H->gamgam mh=125 GeV"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'tong.qiu@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 20000

