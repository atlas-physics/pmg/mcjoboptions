#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "gg->H->WW*->lvlv"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->WW->lvlv"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.contact     = [ 'matous.vozak@cern.ch' ]
evgenConfig.inputFilesPerJob = 45
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]


#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:m0 = 125',        # Higgs mass (just in case)
                             '25:mWidth = 0.00407',# Higgs width (just in case)
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onIfMatch = 11 12',
                             '24:onIfMatch = 13 14',
                             '24:onIfMatch = 15 16' ]


#--------------------------------------------------------------
# Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")
Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter

Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1


Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2


filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"

