
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 LO bblvlv_beta dilepton with all dilep flav combinations, hdamp 1.5 mtop, top width, inverse width correction and Py8 splitting kernel variation weights"
evgenConfig.keywords = [ 'SM', 'top', 'WWbb', 'lepton']
evgenConfig.contact = ["katharin@cern.ch"]
evgenConfig.nEventsPerJob = 1000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bblvlv_modified process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bblvlv_Beta_modified_Common.py")

########################
PowhegConfig.doublefsr = 0
########################

# set to false to reduce log file size
# since only LO event generation integration parameters are more than sufficient
PowhegConfig.check_bad_st1 = 0
PowhegConfig.check_bad_st2 = 0

PowhegConfig.LOevents = 1
PowhegConfig.bornonly = 1

### not modifying the integration parameters, since LO event generation
## should not be expensive

PowhegConfig.width_t = 1.32733   
PowhegConfig.twidth_phsp = 1.32733   
PowhegConfig.tmass_phsp      = 172.5
PowhegConfig.mass_t          = 172.5
PowhegConfig.hdamp           = 258.75
PowhegConfig.ptsqmin         = 1.44 # setting this in accordance with the pTminVeto

# enabling for_reweighting=1 will speed up event generation at expense of some spread in weights
PowhegConfig.for_reweighting = 0
PowhegConfig.ubexcess_correct = 0
### nominal PDFs as in main hvq sample:
# NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118, NNPDF31_nnlo_as_0118_mc_hessian_pdfas (CMS)
PowhegConfig.PDF             = [260000, 25200, 13165, 90900, 265000, 266000, 303400, 325300] 
PowhegConfig.PDF.extend(range(260001, 260101))         # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend(range(90901 , 90931 ))         # Include the PDF4LHC error set
PowhegConfig.PDF.extend(range(93300 , 93343 ))         # Include the PDF4LHC21 error set
# include some newer PDF sets 
# CT18NLO, MSHT20nlo_as118, NNPDF40_nlo_as_01180, PDF4LHC21
PowhegConfig.PDF.extend([14400, 27100, 331100]) 
# include additional NNPDF3.0 sets with varied alphaS value NNPDF30_nlo_as_0115, NNPDF30_nlo_as_0121
PowhegConfig.PDF.extend([ 264000, 267000])

# ## enable all different-flavour lepton combinations
# ## will only work in newer release with the b_bbar_4l_modified process included
PowhegConfig.decay_mode = "b l+ vl b~ l- vl~"



PowhegConfig.nEvents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.1


# width reweighting
PowhegConfig.define_event_weight_group( group_name='topwidth', parameters_to_vary=['width_t'] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p5', parameter_values=[ 0.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt0p8', parameter_values=[ 0.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p0', parameter_values=[ 1.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p2', parameter_values=[ 1.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p4', parameter_values=[ 1.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p6', parameter_values=[ 1.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt1p8', parameter_values=[ 1.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p0', parameter_values=[ 2.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p2', parameter_values=[ 2.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p4', parameter_values=[ 2.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p6', parameter_values=[ 2.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt2p8', parameter_values=[ 2.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p0', parameter_values=[ 3.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt3p5', parameter_values=[ 3.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth', weight_name='Gt4p0', parameter_values=[ 4.0 ] )
PowhegConfig.define_event_weight_group( group_name='topwidth_phsp', parameters_to_vary=['width_t','twidth_phsp'] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt0p5_phsp', parameter_values=[ 0.5, 0.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt0p8_phsp', parameter_values=[ 0.8, 0.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt1p0_phsp', parameter_values=[ 1.0, 1.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt1p2_phsp', parameter_values=[ 1.2, 1.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt1p4_phsp', parameter_values=[ 1.4, 1.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt1p6_phsp', parameter_values=[ 1.6, 1.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt1p8_phsp', parameter_values=[ 1.8, 1.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt2p0_phsp', parameter_values=[ 2.0, 2.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt2p2_phsp', parameter_values=[ 2.2, 2.2 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt2p4_phsp', parameter_values=[ 2.4, 2.4 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt2p6_phsp', parameter_values=[ 2.6, 2.6 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt2p8_phsp', parameter_values=[ 2.8, 2.8 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt3p0_phsp', parameter_values=[ 3.0, 3.0 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt3p5_phsp', parameter_values=[ 3.5, 3.5 ] )
PowhegConfig.add_weight_to_group( group_name='topwidth_phsp', weight_name='Gt4p0_phsp', parameter_values=[ 4.0, 4.0 ] )
PowhegConfig.define_event_weight_group( group_name='width_correction', parameters_to_vary=['width_correction'] )
PowhegConfig.add_weight_to_group( group_name='width_correction', weight_name='width_correction0',parameter_values=[ 0 ])
PowhegConfig.add_weight_to_group( group_name='width_correction', weight_name='width_correction5',parameter_values=[ 5 ])


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
# Produce Dileptonic Events
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, BB4L UserHook and Py8 Splitting Kernel Var. Weights
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

# no Powheg UserHook, since this does not make sense for LO events

genSeq.Pythia8.Commands += [ "TimeShower:recoilStrategyRF = 3" ]



# Pythia8 shower weights are only available in release 2.26 and later.  The
# test below checks the Pythia8 release and also verifies that the Pythia_i
# tag is recent enought to store the shower weights.

if "ShowerWeightNames" in genSeq.Pythia8.__slots__.keys():
    print ("Initalizing Custom Shower Weights from job options following the example from Pythia8_ShowerWeights.py")
    # New Shower Weights which need to overwrite the existing ones so duplicate for now
    # Var3c - A14 tune variation
    # Red - reduced sqrt(2)
    # Def - default 2
    # Con - conservative 4
    # Splitting with default 2
    # \- g2gg, g2qq, q2qg, x2xg; where x is b,t if nFlavQ = 4
    # cNS - non-singular terms
    genSeq.Pythia8.Commands += ['UncertaintyBands:doVariations = on',
    "UncertaintyBands:List = {\
    Var3cUp isr:muRfac=0.549241,\
    Var3cDown isr:muRfac=1.960832,\
    isr:PDF:plus=1,\
    isr:PDF:minus=2,\
    isrRedHi isr:muRfac=0.707,\
    fsrRedHi fsr:muRfac=0.707,\
    isrRedLo isr:muRfac=1.414,\
    fsrRedLo fsr:muRfac=1.414,\
    isrDefHi isr:muRfac=0.5,\
    fsrDefHi fsr:muRfac=0.5,\
    isrDefLo isr:muRfac=2.0,\
    fsrDefLo fsr:muRfac=2.0,\
    isrConHi isr:muRfac=0.25,\
    fsrConHi fsr:muRfac=0.25,\
    isrConLo isr:muRfac=4.0,\
    fsrConLo fsr:muRfac=4.0,\
    isr_cNS_dn isr:cNS=-2.0,\
    isr_cNS_up isr:cNS=2.0,\
    fsr_cNS_dn fsr:cNS=-2.0,\
    fsr_cNS_up fsr:cNS=2.0,\
    fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,\
    fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,\
    fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,\
    fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,\
    fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,\
    fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,\
    fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,\
    fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,\
    fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,\
    fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,\
    fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,\
    fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,\
    fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,\
    fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,\
    fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,\
    fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,\
    isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,\
    isr_G2GG_muR_up isr:G2GG:muRfac=2.0,\
    isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,\
    isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,\
    isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,\
    isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,\
    isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,\
    isr_X2XG_muR_up isr:X2XG:muRfac=2.0,\
    isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,\
    isr_G2GG_cNS_up isr:G2GG:cNS=2.0,\
    isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,\
    isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,\
    isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,\
    isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,\
    isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,\
    isr_X2XG_cNS_up isr:X2XG:cNS=2.0\
    }"]

    genSeq.Pythia8.ShowerWeightNames = ["Var3cUp",
                                        "Var3cDown",
                                        "isr:PDF:plus",
                                        "isr:PDF:minus",
                                        "isrRedHi", 
                                        "fsrRedHi", 
                                        "isrRedLo", 
                                        "fsrRedLo", 
                                        "isrDefHi", 
                                        "fsrDefHi", 
                                        "isrDefLo", 
                                        "fsrDefLo", 
                                        "isrConHi", 
                                        "fsrConHi", 
                                        "isrConLo", 
                                        "fsrConLo", 
                                        "isr_cNS_dn",                                    
                                        "isr_cNS_up",
                                        "fsr_cNS_dn",
                                        "fsr_cNS_up",
                                        "fsr_G2GG_muR_dn", 
                                        "fsr_G2GG_muR_up", 
                                        "fsr_G2QQ_muR_dn", 
                                        "fsr_G2QQ_muR_up", 
                                        "fsr_Q2QG_muR_dn", 
                                        "fsr_Q2QG_muR_up", 
                                        "fsr_X2XG_muR_dn", 
                                        "fsr_X2XG_muR_up", 
                                        "fsr_G2GG_cNS_dn", 
                                        "fsr_G2GG_cNS_up", 
                                        "fsr_G2QQ_cNS_dn", 
                                        "fsr_G2QQ_cNS_up", 
                                        "fsr_Q2QG_cNS_dn", 
                                        "fsr_Q2QG_cNS_up", 
                                        "fsr_X2XG_cNS_dn", 
                                        "fsr_X2XG_cNS_up", 
                                        "isr_G2GG_muR_dn", 
                                        "isr_G2GG_muR_up", 
                                        "isr_G2QQ_muR_dn", 
                                        "isr_G2QQ_muR_up", 
                                        "isr_Q2QG_muR_dn", 
                                        "isr_Q2QG_muR_up", 
                                        "isr_X2XG_muR_dn", 
                                        "isr_X2XG_muR_up", 
                                        "isr_G2GG_cNS_dn", 
                                        "isr_G2GG_cNS_up", 
                                        "isr_G2QQ_cNS_dn", 
                                        "isr_G2QQ_cNS_up", 
                                        "isr_Q2QG_cNS_dn", 
                                        "isr_Q2QG_cNS_up", 
                                        "isr_X2XG_cNS_dn", 
                                        "isr_X2XG_cNS_up"]

    # New settings
    genSeq.Pythia8.Commands += ['UncertaintyBands:nFlavQ = 4', # define X=bottom/top in X2XG variations
                                'UncertaintyBands:MPIshowers = on',
                                'UncertaintyBands:overSampleFSR = 10.0',
                                'UncertaintyBands:overSampleISR = 10.0',
                                'UncertaintyBands:FSRpTmin2Fac = 20',
                                'UncertaintyBands:ISRpTmin2Fac = 1']

