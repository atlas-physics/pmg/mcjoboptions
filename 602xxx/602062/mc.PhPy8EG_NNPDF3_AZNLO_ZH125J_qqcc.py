#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbar production
#--------------------------------------------------------------
# os.environ["ATHENA_PROC_NUMBER"] = "10"

include('PowhegControl/PowhegControl_HZj_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode = "z > j j"

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.define_event_weight_group( group_name='kappas_var', parameters_to_vary=['kappa_ghz','kappa_ght', 'kappa_ghb'] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.0.5, kht.1, khb.1', parameter_values=[0.5,1.,1.] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.1, kht.0, khb.1', parameter_values=[1.,0.,1.] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.1, kht.1, khb.0', parameter_values=[1.,1.,0.] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.1, kht.0, khb.0', parameter_values=[1.,0.,0.] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.0.5, kht.1, khb.0', parameter_values=[0.5,1.,0.] )
#PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khz.0.5, kht.0, khb.1', parameter_values=[0.5,0.,1.] )

# PowhegConfig.mu_F = 1.0
# PowhegConfig.mu_R = 1.0
# PowhegConfig.PDF = 303400

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+Z+jet->qq + ccbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact     = [ 'valerio.dao@cern.ch' ]
evgenConfig.nEventsPerJob=200 
evgenConfig.process = "qq->ZH, H->cc, Z->qq"

#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 4 4' ]

