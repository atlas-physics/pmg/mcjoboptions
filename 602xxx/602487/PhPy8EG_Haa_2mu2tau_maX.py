#--------------------------------------------------------------------
# Get configuration from JO name
#--------------------------------------------------------------------
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# Example JO name: mc.PhPy8EG_Haa_2mu2tau_ma3p5_muhad.py -> tokens = ['ma3p5', 'muhad']
tokens = get_physics_short().replace('.py', '').split('_')[3:] # Remove the PhPy8EG_Haa_2mu2tau common prefix

a_mass = float([t for t in tokens if t.startswith('ma')][0].removeprefix('ma').replace('p', '.')) # GeV

if 'muhad' in tokens:
    channel = 'MuHad'
    channel_str = ', a -> tau(mu) tau(had) channel'
elif 'ehad' in tokens:
    channel = 'EHad'
    channel_str = ', a -> tau(e) tau(had) channel'
elif 'hadhad' in tokens:
    channel = 'HadHad'
    channel_str = ', a -> tau(had) tau(had) channel'
else:
    channel = 'All'
    channel_str = ''

print(f'Generation parameters:')
print(f'  {a_mass=}')
print(f'  {channel=}')



#--------------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------------
evgenConfig.description = f'POWHEG + Pythia 8 ggH, H->aa->2mu2tau, m_a = {a_mass} GeV' + channel_str
evgenConfig.process = 'ggH, H->a(->mumu)a(->tautau)'
evgenConfig.keywords = ['BSM', 'SMHiggs']
evgenConfig.contact = ['wmyao@lbl.gov', 'jean.yves.beaucamp@cern.ch']
evgenConfig.generators = ['Powheg', 'Pythia8', 'EvtGen']
# Use LHE files as input: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HLRSGroup#MC_Information_For_Run_3



#--------------------------------------------------------------------
# Modify the input LHE file, since Pythia doesn't like to decay 
# the SM higgs to BSM products:
# 
# h_0 (SM Higgs, with PDG ID 25) -> H_0 (BSM Higgs, with PDG ID 35)
#--------------------------------------------------------------------
import os, glob
for in_filename in glob.glob('*.events'):
    old_file = open(in_filename)
    new_filename = f'{in_filename}.temp'
    new_file = open(new_filename, 'w')
    for line in old_file:
        if line.startswith('      25     1'):
            new_file.write(line.replace('      25     1', '      35     1'))
        else:
            new_file.write(line)
    old_file.close()
    new_file.close()
    os.system(f'mv {in_filename} {in_filename}.old')
    os.system(f'mv {new_filename} {in_filename}')
    


#--------------------------------------------------------------------
# Pythia 8 showering
#--------------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

# The width parameters specify the Gamma of the Breit-Wigner decay distribution

# SM Higgs (even though we use H_0 in Pythia)
h_mass = 125.0 # GeV
h_width = 0.00407 # GeV

# Low-mass pseudoscalar (A_0)
a_width = 0.1 * a_mass / 100.0 # GeV, 100 MeV width for m_a = 100 GeV
a_massMin = a_mass - 100.0 * a_width
a_massMax = a_mass + 100.0 * a_width
a_br_2mu = 0.5 # BR(a -> mu mu) = 0.5
a_br_2tau = 1 - a_br_2mu # BR(a -> tau tau) = 1 - BR(a -> mu mu)

genSeq.Pythia8.Commands += [
    # Enable the 2HDM model
    'Higgs:useBSM = on',

    # H_0 configuration
    f'35:m0 = {h_mass}',
    f'35:mWidth = {h_width}',
    '35:doForceWidth = off',
    '35:oneChannel = on 1.0 100 36 36', # Only enable the H -> aa decay, with BR(H -> aa) = 1.0
    # The "100" indicates that this is a resonance

    # A_0 configuration
    f'36:m0 = {a_mass}',
    f'36:mWidth = {a_width}',
    f'36:mMin = {a_massMin}',
    f'36:mMax = {a_massMax}',
    f'36:oneChannel = on {a_br_2mu} 100 13 -13', # Enable the a -> 2mu decay
    f'36:addChannel = on {a_br_2tau} 100 15 -15', # Enable the a -> 2tau decay
    # We only keep these two manually-specified decay channels
]



#--------------------------------------------------------------------
# Filters
#--------------------------------------------------------------------

# Total filter selection: N(A_0 -> 2mu) == N(A_0 -> 2tau) == 1 & N_e + N_mu + N_tau >= 4 && N_tau == 2

# Create the required TruthElectrons, TruthMuons, and TruthTaus collections for the harmonized filters to run
include('GeneratorFilters/CreatexAODSlimContainers.py')
createxAODSlimmedContainer('TruthTaus', prefiltSeq) # The TruthTaus collection needs to be inialized explicitly

from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter, xAODTauFilter, xAODDecaysFinalStateFilter

# Require one a -> 2mu decay, and one a -> 2tau decay per-event
# This works by requesting 2 taus and 2 muons decaying from all (2) A_0s in the event,
# because we only allow the a -> 2mu and a -> 2tau decays in Pythia
filtSeq += xAODDecaysFinalStateFilter('AADecayFilter')
filtSeq.AADecayFilter.PDGAllowedParents = [36] # A_0
filtSeq.AADecayFilter.NTaus = 2 # Direct decays (before decaying themselves)
filtSeq.AADecayFilter.NMuons = 2

# At least 4 leptons (including hadronically-decaying taus)
# Used to filter in pT and eta
filtSeq += xAODMultiElecMuTauFilter('LepFourFilter')
filtSeq.LepFourFilter.IncludeHadTaus = True
filtSeq.LepFourFilter.NLeptons = 4
filtSeq.LepFourFilter.MinPt = 5000
filtSeq.LepFourFilter.MinVisPtHadTau = 10000
filtSeq.LepFourFilter.MaxEta = 3

# Only 2 taus (including hadronically-decaying taus)
# Used to filter in pT and eta specifically for taus
filtSeq += xAODTauFilter('TauTwoFilter')
filtSeq.TauTwoFilter.Ntaus = 2 # >= 2
filtSeq.TauTwoFilter.MaxNtaus = 2 # <= 2
filtSeq.TauTwoFilter.UseMaxNTaus = True
filtSeq.TauTwoFilter.EtaMaxe = 3
filtSeq.TauTwoFilter.EtaMaxmu = 3
filtSeq.TauTwoFilter.EtaMaxhad = 3
filtSeq.TauTwoFilter.Ptcute = 5000
filtSeq.TauTwoFilter.Ptcutmu = 5000
filtSeq.TauTwoFilter.Ptcuthad = 10000

# a -> 2tau decay channel filter (optional)
if channel != 'All':
    filtSeq += xAODTauFilter('TauChannelFilter')
    filtSeq.TauChannelFilter.UseNewOptions = True
    filtSeq.TauChannelFilter.UseMaxNTaus = True

    # 1 or 2 hadronic taus
    filtSeq.TauChannelFilter.Nhadtaus = 2 if channel == 'HadHad' else 1
    filtSeq.TauChannelFilter.MaxNhadtaus = 2 if channel == 'HadHad' else 1

    # Total of 2 taus (therefore only 1 or 0 leptonic, since we have 1 or 2 hadronic)
    filtSeq.TauChannelFilter.Ntaus = 2 # >= 2
    filtSeq.TauChannelFilter.MaxNtaus = 2 # <= 2

    # Select electron or muon channel if required
    filtSeq.TauChannelFilter.EtaMaxe = 3 if channel == 'EHad' else -1
    filtSeq.TauChannelFilter.EtaMaxmu = 3 if channel == 'MuHad' else -1
    filtSeq.TauChannelFilter.EtaMaxhad = 3

    filtSeq.TauChannelFilter.Ptcute = 5000
    filtSeq.TauChannelFilter.Ptcutmu = 5000
    filtSeq.TauChannelFilter.Ptcuthad = 10000