
mhc=160

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Madspin+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, Higgs plus decays for masses from 60-168'
evgenConfig.keywords    =['Higgs','MSSM','BSMHiggs','chargedHiggs','top','ttbar']
evgenConfig.contact     = [ 'christian.nass@cern.ch']
evgenConfig.nEventsPerJob = 10000


include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode      = "t t~ > undecayed"

PowhegConfig.hdamp        = 258.75 # 1.5 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900]                 # NNPDF30, MMHT, CT14, PDF4LHC - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                            # Include the NNPDF error set
PowhegConfig.PDF.extend(range(90901 , 90931 ))                            # Include the PDF4LHC error set

# Set top quark Branching ratios
PowhegConfig.BR_t_to_Wb = 1.0
PowhegConfig.BR_t_to_Ws = 0.0
PowhegConfig.BR_t_to_Wd = 0.0

# bwcutoff to match MC@NLO config
PowhegConfig.bwcutoff = 50

#--------------------------------------------------------------
# Charge Higgs, and all other masses in GeV
#--------------------------------------------------------------
import math
mh1=1.250e+02
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2))
mh3=mh2


masses = {'25':str(mh1)+'  #  mh1',
          '35':str(mh2)+'  #  mh2',
          '36':str(mh2)+'  #  mh2',
          '37':str(mhc)+'  #  mhc'}
decayss={'37': 'DECAY 37 1.300000e+02 # whc'}


#--------------------------------------------------------------
# Now preparing for MadSpin
#--------------------------------------------------------------

#write_param_card is part of the 2HDMTypeII model
import shutil,os,subprocess, sys, glob

paramFileNameOld = 'MadGraph_param_card_ttHplus_NLO.dat'
paramFileNameNew = 'Cards/param_card.dat'

path = os.getcwd()
#modify_param_card needs param_card.dat in directory Cards to create new param_card; copy over from MADPATH to have everything locally
os.system('mkdir Cards')
os.system('cp $MADPATH/Template/LO/Cards/param_card.dat Cards/')

from MadGraphControl.MadGraphUtils import *
modify_param_card(param_card_input=paramFileNameOld,process_dir=path,params={'MASS':masses,'DECAY':decayss})
print_cards(param_card=paramFileNameNew)

PowhegConfig.MadSpin_enabled = True
PowhegConfig.MadSpin_decays= ["decay t > b h+", "decay t~ > b~ w-, w- > l- vl~"]
PowhegConfig.MadSpin_mode = "full"
PowhegConfig.MadSpin_model = "/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/2HDMtypeII-nobmass"
PowhegConfig.MadSpin_nFlavours = 5
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_taus_are_leptons = True
PowhegConfig.MadSpin_max_weight_ps_point = 1000
PowhegConfig.MadSpin_Nevents_for_max_weight = 250
PowhegConfig.MadSpin_paramcard = paramFileNameNew
PowhegConfig.nEvents     = 10000*1.1 # nEventsPerJob*1.1/filter_efficiencies to compensate inefficiencies (no filter used so filter_efficiency=1.)
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]

genSeq.Pythia8.Commands += [
    "Higgs:useBSM = on",
    "37:oneChannel = 1 1.0 0 -3 4"
]


