#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.contact = ['Meng-Ju Tsai','metsai@cern.ch']
evgenConfig.description = "Generation of pp > ttWm EW in POWHEGBOX + Herwig7"
evgenConfig.keywords = ["Top"]
evgenConfig.tune = "H7-Default"
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

include("Herwig7_i/Herwig7_LHEF.py")
# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()


# run Herwig7
Herwig7Config.run()
