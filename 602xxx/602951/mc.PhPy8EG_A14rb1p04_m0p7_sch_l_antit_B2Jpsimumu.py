#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top-quark s-channel production (anti-top), inclusive, A14 tune with rb=1.0356, renormMultFac=factorMultFac=0.70711, ME NNPDF30 NLO, A14 NNPDF23 LO, B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel', 'Jpsi' ]
evgenConfig.contact     = [ 'marcus.de.beurs@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 50 # H.A. optional? to be adjusted.

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py') # H.A. updated for JO in 21.6, MC15 JobOptions -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
include("Pythia8_i/Pythia8_Powheg_Main31.py") # H.A. updated for JO in 21.6, MC15 JobOptions -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# For FSR variation
genSeq.Pythia8.Commands += ['TimeShower:renormMultFac=0.70711']
genSeq.Pythia8.Commands += ['TimeShower:factorMultFac=0.70711']
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.0356' ]

#--------------------------------------------------------------
# Special decay of B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['B2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'B2Jpsimumu.DEC'

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
