#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 gg->ZH->ZWW->Zincl+lvqq production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia showering with A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs->WW->lvqq at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16']

#--------------------------------------------------------------
# Filters
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended()
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603205.Ph_PDF4LHC21_ggZH125_Zincl_HWW_batch2_LHE.evgen.TXT.e8557
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->ZH->ZWW->Zincl+lvqq production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'michiel.jan.veen@cern.ch' ]
evgenConfig.inputFilesPerJob = 58
evgenConfig.nEventsPerJob    = 50000
evgenConfig.process = "gg->ZH, H->WW, Z->inclusive, WW->lvqq"
