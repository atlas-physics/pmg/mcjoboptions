#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "4lepton", "mH125", "gluonFusionHiggs"]
evgenConfig.process     = "ggH, H->ZZ->4l tau filter"
evgenConfig.description = "POWHEG+Pythia8 ggF NNLOPS production with A14 NNPDF2.3 tune, H->ZZ->4l tau filter"
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob    = 10000
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15']

#--------------------------------------------------------------
# HVV Tau filter
#--------------------------------------------------------------
include('GeneratorFilters/xAODXtoVVDecayFilterExtended_Common.py')
filtSeq.xAODXtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.xAODXtoVVDecayFilterExtended.PDGParent = 23
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild1 = [15]
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild2 = [11,13,15]
filtSeq.xAODXtoVVDecayFilterExtended.UseStatusParent = True
filtSeq.xAODXtoVVDecayFilterExtended.StatusParent = 22
