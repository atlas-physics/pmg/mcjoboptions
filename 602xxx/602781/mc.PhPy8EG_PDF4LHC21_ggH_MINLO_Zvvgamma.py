# EVGEN configuration
# Note:  These JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards.  Because the current Generate_tf.py requires an output file, we
# need to fake it a bit.  We therefore will run Pythia8 on the first event in the LHE file.
# Note, sence we do not intend to keep the EVNT file, the JO name doesn't include Pythia8


#evgenConfig.minevents   = 1
#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8 (H->Zgamma)
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14',
                             '23:onIfMatch = 16 16'] 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH MINLO, H->Z(vv)gamma  mh=125 GeV"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'tong.qiu@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]
evgenConfig.inputFilesPerJob = 6
evgenConfig.nEventsPerJob    = 10000




