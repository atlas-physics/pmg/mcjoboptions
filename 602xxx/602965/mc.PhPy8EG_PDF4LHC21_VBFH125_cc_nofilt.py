#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "VBF->H->cc"
evgenConfig.description      = "POWHEG+PYTHIA8+EVTGEN, VBF H->bb mh=125 GeV CPS, Dipole ON"
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "mH125", "VBF" ]
evgenConfig.contact          = [ 'matthew.henry.klein@cern.ch' ]
evgenConfig.generators       = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 4 -4']
#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

