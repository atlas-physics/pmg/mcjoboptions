#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = "POWHEG+Herwig72 dijet production."  
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["aknue@cern.ch", "oburlaye@cern.ch"]
evgenConfig.tune = "H7.2-Default"

evgenConfig.nEventsPerJob = 10000
# 1 LHE file = 5000 events, filteff > 0.29
evgenConfig.inputFilesPerJob = 8

#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.enable_angularShowerScaleVariations(True)

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")  

command = """
set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/HardNLOPDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/HardNLOPDF
"""
Herwig7Config.add_commands(command)

# run Herwig7
Herwig7Config.run()

## filters
include("GeneratorFilters/FindJets.py")

CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)

include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4, filtSeq)
