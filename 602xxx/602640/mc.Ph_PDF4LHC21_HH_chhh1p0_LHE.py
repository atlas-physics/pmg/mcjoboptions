#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "SM diHiggs production, with Powheg-Box-V2, at NLO + full top mass, with PDF4LHC15 PDF. LHE-only production."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant"]
evgenConfig.contact = [ "cen.mo@cern.ch" ]
evgenConfig.generators += [ "Powheg" ]
evgenConfig.nEventsPerJob  = 500

# --------------------------------------------------------------
# Configuring Powheg
# --------------------------------------------------------------

### Load ATLAS defaults for the Powheg ggF_HH process
include("PowhegControl/PowhegControl_ggF_HH_Common.py")

filter_factor = 1
PowhegConfig.nEvents = (runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob) * filter_factor


### Modify couplings
PowhegConfig.chhh = 1.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]
PowhegConfig.ct = 1.0 # Top-Higgs Yukawa coupling [default: 1.0 (SM)]
#PowhegConfig.ctt = 0. # Two-top-two-Higgs (tthh) coupling [default: 0. (SM)]
#PowhegConfig.cggh = 0. # Effective gluon-gluon-Higgs coupling [default: 0. (SM)]
#PowhegConfig.cgghh = 0. # Effective two-gluon-two-Higgses coupling [default: 0. (SM)]
PowhegConfig.hdamp = 250


### scales and PDF sets
PowhegConfig.PDF = list(range(93300,93343)) # PDF4LHC21_40_pdfas with error sets
PowhegConfig.PDF += list(range(90400,90433)) # PDF4LHC15_nlo_30_pdfas with error sets
PowhegConfig.PDF += list(range(325300,325403)) # NNPDF30_nnlo_as_0118 with error sets
PowhegConfig.PDF += [27100,14400,331700] # MSHT20nlo_as118, CT18NLO, NNPDF40_nlo_as_01180 nominal sets
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0]

### Generate events
PowhegConfig.generate()

#--------------------------------------------------------------
