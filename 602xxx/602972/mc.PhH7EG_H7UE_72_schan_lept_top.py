#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark s-channel production (top), inclusive, H7.2-Default tune, ME NNPDF30 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'sChannel', 'inclusive' ]
evgenConfig.contact     = [ 'serena.palazzo@cern.ch', 'qibin.liu@cern.ch' ]
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 10
evgenConfig.tune             = "H7.2-Default"

#--------------------------------------------------------------
# Herwig7 showering with the H7UE MMHT2014 tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# run Herwig7
Herwig7Config.run()

