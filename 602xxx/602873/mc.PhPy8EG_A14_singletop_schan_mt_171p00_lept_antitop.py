#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top-quark s-channel production (anti-top) with m(top)=169.00 GeV, inclusive, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel' ]
evgenConfig.contact     = [ 'lukas.kretschmann@cern.ch']
evgenConfig.generators += [ 'Powheg' ]

#--------------------------------------------------------------
# Powheg single-top s-channel setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_t_sch_Common.py')

PowhegConfig.decay_mode = 't~ > b~ l- vl~'
PowhegConfig.mass_t  = 171.00
PowhegConfig.width_t = 1.280

PowhegConfig.nEvents *= 1.1 # Safety factor
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
