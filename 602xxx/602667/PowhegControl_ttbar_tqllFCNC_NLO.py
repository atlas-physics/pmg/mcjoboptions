from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with MadSpin decay, Powheg hdamp equal 1.5*top mass, A14 tune'
evgenConfig.keywords    = [ 'top', 'ttbar', 'FCNC' ]
evgenConfig.contact     = [ 'shayma.wahdan@cern.ch', 'dominic.hirschbuehl@cern.ch', 'dominic.matthew.jones@cern.ch', 'jacob.j.kempster@cern.ch']
evgenConfig.nEventsPerJob = 10000

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.decay_mode      = "t t~ > undecayed"

PowhegConfig.hdamp        = 258.75  # 1.5 * mtop

# example physics short 'PhPy8EG_ttbar_tqllFCNC_ctmumu_lequ3'
JOName = get_physics_short()
splitConfig = JOName.split('_')
flavour_combo = splitConfig[-2]
op_combo = splitConfig[-1]

up_type_quark = flavour_combo[0]
lep_flavour = flavour_combo[2:]

lepton_dict = {
    'ee' : 'e+ e-', 
    'mumu' : 'mu+ mu-', 
    'tautau' : 'ta+ ta-', 
}

PowhegConfig.MadSpin_enabled = True
PowhegConfig.MadSpin_nFlavours = 5
PowhegConfig.MadSpin_model = '/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTsim_general_MwScheme_UFO-FCNC_top_all'
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD] NP=1"
PowhegConfig.nEvents *= 1.1

if 'tqllFCNC' in JOName:
    PowhegConfig.MadSpin_decays = [ 
        "decay t~ > w- b~, w- > l- vl~ ", 
        "decay t > {u} {ll}".format(u=up_type_quark, ll=lepton_dict[lep_flavour]),
        "set spinmode none"
    ]
elif 'tbarqllFCNC' in JOName:
    PowhegConfig.MadSpin_decays = [ 
        "decay t >  w+ b, w+ > l+ vl", 
        "decay t~ > {u}~ {ll}".format(u=up_type_quark, ll=lepton_dict[lep_flavour]),
        "set spinmode none"
    ]
else:
    raise ValueError(f"Unexpected decay type in JOName: {JOName}")

os.system(f'get_files -jo restrict_{op_combo}_{flavour_combo}.dat')
PowhegConfig.MadSpin_paramcard = f"./restrict_{op_combo}_{flavour_combo}.dat"

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]
