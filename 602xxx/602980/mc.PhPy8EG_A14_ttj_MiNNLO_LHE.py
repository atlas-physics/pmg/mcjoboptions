# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 tt+jet production with MiNNLO and A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "top", "1jet"]
evgenConfig.contact = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.nEventsPerJob = 2000
evgenConfig.generators = ["Powheg"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg ttj_MiNNLO process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_ttj_MiNNLO_Common.py")

# --------------------------------------------------------------
# Settings
# --------------------------------------------------------------
#PowhegConfig.PDF   = 261000 
PowhegConfig.ncall1 = 10000000
PowhegConfig.ncall2 = 20000000
PowhegConfig.nubound = 5000000
PowhegConfig.whichscale=4
PowhegConfig.bornsuppfact=-1
PowhegConfig.bornzerodamp=1
PowhegConfig.withdamp=1
PowhegConfig.doublefsr=1

PowhegConfig.foldcsi=2
PowhegConfig.foldphi=2
PowhegConfig.foldy=2

PowhegConfig.itmx1=3
PowhegConfig.itmx2=3


# define the decay mode
PowhegConfig.decay_mode = "t t~ > all"

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

