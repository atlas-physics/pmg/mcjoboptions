#---------------------------------------------------------------
# LHE files of WpH used as inputs 
# POWHEG+Pythia8 WpH, mc23 H-> gam gam_d, mH=125GeV, my_d=0
#---------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 11

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 
#--------------------------------------------------------------
# H->y+yd decay
#--------------------------------------------------------------
# For VBF, Nfinal = 3
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 22 4900022', # H->y+yd
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 WpH production: H->y+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'WHiggs' , 'photon' , 'darkPhoton' ]
evgenConfig.contact     = [ 'hassnae.el.jarrari@cern.ch' ]
evgenConfig.process = "WpH->H, H->y+yd"
