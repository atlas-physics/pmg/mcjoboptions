evgenConfig.description = 'aMcAtNlo Zmumu+0,1,2,3j NLO FxFx HT2-biased, low mass 10<m<40GeV CFilterBVeto Py8 ShowerWeights'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'muon', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 27.4%
# CFilterBVeto+dilep - eff 17.2%
# one LHE file contains 32000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 7

include("MadGraphControl_Vjets_FxFx_shower.py")

include("GeneratorFilters/MultiLeptonFilter.py")
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.0
MultiLeptonFilter.NLeptons = 2

filtSeq.Expression += " and (MultiLeptonFilter)"
