#--------------------------------------------------------------
#Pythia8 pp/gg -> Cascade_bc0 -> Lambda_b0 + K_S production
#--------------------------------------------------------------

evgenConfig.nEventsPerJob = 5000
evgenConfig.description = "Pythia8 pp/gg -> Cascade_bc0 -> Lambda_b0 + K_S"
evgenConfig.keywords = ["heavyFlavour","Jpsi","Muon"]
evgenConfig.contact = ["shaogang.peng@cern.ch"]
evgenConfig.process = "pp/gg -> X -> Cascade_bc0 -> Lambda_b0 + K_S"
evgenConfig.generators += ['Pythia8']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")

genSeq.Pythia8.Commands += [
'Higgs:useBSM = on',
'HiggsBSM:gg2H2 = on',
'HiggsH2:coup2d = 10.0',
'HiggsH2:coup2u = 10.0',
'HiggsH2:coup2Z = 0.0',
'HiggsH2:coup2W = 0.0',
'HiggsA3:coup2H2Z = 0.0',
'HiggsH2:coup2A3A3 = 0.0',
'HiggsH2:coup2H1H1 = 0.0',
'35:mMin = 0',
'35:mMax = 15.0',
############# For Fixed Mass Distribution#############
'35:m0 = 7.45', # Cascade_bc0,
'35:mWidth = 0.1', 
'35:onMode = off',
'35:addChannel = 1 1.00 100 5122 310', #Lambda_b0 + K_S
'5122:onMode = off',
'5122:onIfMatch = 443 3122',
'443:onMode = off',
'443:onIfMatch = 13 13',
]
## Lambda and K_s have long lifetime and decay in Geant4

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   filtSeq += muonfilter1

filtSeq.muonfilter1.Ptcut = 3500.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 2 #minimum


