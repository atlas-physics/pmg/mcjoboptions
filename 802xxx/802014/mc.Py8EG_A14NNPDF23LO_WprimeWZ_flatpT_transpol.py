# W' -> W(trans) Z(rtans) -> qqqq
# Wprime Mass (in GeV) = 13000

evgenConfig.contact = ["andy.buckley@cern.ch"]
evgenConfig.description = "Wprime->WZ->qqqq flat pT with NNPDF23LO PDF and ~pure-transverse W & Z"
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "transverse"]
evgenConfig.process = "pp>Wprime>WZ"
evgenConfig.generators += [ 'Pythia8' ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.UserHooks += ["WprimeWZFlat"]

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                            "Wprime:coup2WZ = 1.",
                            "34:m0 = 13000",
                            "34:mWidth = 10",
                            "34:doForceWidth = true",
                            "34:onMode = off",
                            "34:onIfAll = 23 24",
                            "24:onMode = off",
                            "24:onIfAny = 1 2 3 4 5",
                            "23:onMode = off",
                            "23:onIfAny = 1 2 3 4 5"]

genSeq.Pythia8.Commands += ["WprimeWZFlat:FlattenPT=1"]

genSeq.Pythia8.UserHooks += ["WZPolarization"]
genSeq.Pythia8.Commands += ["WZPolarization:mode = 2"]
