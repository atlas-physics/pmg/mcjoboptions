evgenConfig.description = "VH->mumutautau with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "H -> tau + taubar"
evgenConfig.contact = ["mfujimot@cern.ch"] 
evgenConfig.keywords    = [ 'SM','Higgs', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.generators += ["EvtGen"]
evgenConfig.nEventsPerJob = 10000

include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )
genSeq.Pythia8.Commands += ["HiggsSM:ffbar2HZ = true",
                            "25:m0 = 130.0",
                            "25:mWidth = 400.0",
                            "25:doForceWidth = true ",
                            "25:mMax = 200 ",
                            "25:mMin = 40 ",
                            "25:onMode = off",
                            "25:onIfAny = 15",
                            "23:onMode = off",
                            "23:onIfAny = 13",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionPow = 5."]

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 200.*GeV
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = -1
filtSeq.ParticleFilter.PDG = 25
filtSeq.ParticleFilter.MinParts = 1
filtSeq.ParticleFilter.Exclusive = True

#---------------------------------------------------------------------------------------------------
# Decay Filter
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HTauTauFilter", PDGParent = [25], PDGChild = [15])

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended("TauTauHadHadFilter")
filtSeq.TauTauHadHadFilter.PDGGrandParent = 25
filtSeq.TauTauHadHadFilter.PDGParent = 15
filtSeq.TauTauHadHadFilter.StatusParent = 2
filtSeq.TauTauHadHadFilter.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.TauTauHadHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]

filtSeq.Expression = "HTauTauFilter and TauTauHadHadFilter"

