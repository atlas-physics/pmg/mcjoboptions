#based on 361035
evgenConfig.description = "semi-visible jets leptonic EvtGen"
evgenConfig.keywords = ["BSM"]
evgenConfig.contact = ["sukanya.sinha@cern.ch"]

#evgenConfig.saveJets = True
safefactor=1.1
nevents=runArgs.maxEvents*safefactor
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

bonus_file = open('pdg_extras.dat','w')
#  The most important number is the first: the PDGID of the particle
bonus_file.write('9000002\n')
bonus_file.write('9000003\n')
bonus_file.write('9000004\n')
bonus_file.write('-9000002\n')
bonus_file.write('-9000003\n')
bonus_file.write('-9000004\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdg_extras.dat'

genSeq.Pythia8.Commands += ["HiddenValley:ffbar2Zv = on",
#! 3) Turn on the production process.
"4900023:m0 = 750",
"4900023:mMin = 749",
"4900023:mMax = 751",
"4900023:mWidth = 0.01",
"4900023:doForceWidth = on",
"4900023:oneChannel = 1 0.982 102 4900101 -4900101",
"4900023:addChannel = 1 0.003 102 1 -1",
"4900023:addChannel = 1 0.003 102 2 -2",
"4900023:addChannel = 1 0.003 102 3 -3",
"4900023:addChannel = 1 0.003 102 4 -4",
"4900023:addChannel = 1 0.003 102 5 -5",
"4900023:addChannel = 1 0.003 102 6 -6",
#! 4) For MC efficiency let zprime decay only to dark quarks.
"4900023:onMode = off",
"4900023:onIfAny = 4900101",
#! 5) HiddenValley Settings
"HiddenValley:Ngauge = 3",
"HiddenValley:nFlav = 2",
"HiddenValley:spinFv = 0",
"HiddenValley:FSR = on",
"HiddenValley:fragment = on",
"HiddenValley:alphaOrder = 1",
#"HiddenValley:setLambda = on",
"HiddenValley:Lambda = 5",
"HiddenValley:pTminFSR = 5.5",
"HiddenValley:probVector = 0.75",
#! 6) Dark sector mass spectrum
"4900101:m0 = 5.2",
"4900111:m0 = 8",
"4900211:m0 = 8",
"51:m0 = 0.0",
"51:isResonance = false",
"4900113:m0 = 15.4919",
"4900213:m0 = 15.4919",
"53:m0 = 0.0",
"53:isResonance = false",
"4900001:m0 = 50000",
"4900002:m0 = 50000",
"4900003:m0 = 50000",
"4900004:m0 = 50000",
"4900005:m0 = 50000",
"4900006:m0 = 50000",
"4900011:m0 = 50000",
"4900012:m0 = 50000",
"4900013:m0 = 50000",
"4900014:m0 = 50000",
"4900015:m0 = 50000",
"4900016:m0 = 50000",
#! 7) HV decays
#rinv7
"4900111:oneChannel = 1 0.7 0 51 -51",
"4900111:addChannel = 1 0.3 91 4 -4",
"4900211:oneChannel = 1 0.7 0 51 -51",
"4900211:addChannel = 1 0.3 91 4 -4",
"4900113:oneChannel = 1 0.7 0 53 -53",
"4900113:addChannel = 1 0.0606449 91 2 -2",
"4900113:addChannel = 1 0.014712 91 1 -1",
"4900113:addChannel = 1 0.0606449 91 3 -3",
"4900113:addChannel = 1 0.014712 91 4 -4",
"4900113:addChannel = 1 0.0141895 91 5 -5",
"4900113:addChannel = 1 0.0450322 91 11 -11",
"4900113:addChannel = 1 0.0450322 91 13 -13",
"4900113:addChannel = 1 0.0450322 91 15 -15",
"4900213:oneChannel = 1 0.7 0 53 -53",
"4900213:addChannel = 1 0.0606449 91 2 -2",
"4900213:addChannel = 1 0.014712 91 1 -1",
"4900213:addChannel = 1 0.0606449 91 3 -3",
"4900213:addChannel = 1 0.014712 91 4 -4",
"4900213:addChannel = 1 0.0141895 91 5 -5",
"4900213:addChannel = 1 0.0450322 91 11 -11",
"4900213:addChannel = 1 0.0450322 91 13 -13",
"4900213:addChannel = 1 0.0450322 91 15 -15"
                            ]
evgenConfig.nEventsPerJob = 10000


