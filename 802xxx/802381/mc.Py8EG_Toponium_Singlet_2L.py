include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.description = 'Toponium production with 2L, singlet part, showering only, m_t = 173 GeV'
evgenConfig.generators += ['Pythia8']
evgenConfig.keywords   += ['SM', 'top']
evgenConfig.contact = ["Yoav Afik <yafik@cern.ch>, Baptiste Ravina <baptiste.ravina@cern.ch>"]
evgenConfig.tune = 'A14 NNPDF23LO'

evgenConfig.inputFilesPerJob = 1
