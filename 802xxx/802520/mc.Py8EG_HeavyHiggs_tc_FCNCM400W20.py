evgenConfig.description = "Heavy higgs boson decaying to charm and top quarks (FCNC decay)"
evgenConfig.contact = ['Marc Tost <marctost11@gmail.com>']
evgenConfig.keywords = ['BSM', 'FCNC']
evgenConfig.generators += ['Pythia8']
evgenConfig.process = "H2>tc"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'HiggsBSM:gg2H2 = on',
                            '35:m0 = 400',
                            '35:mMin = 0',
                            '35:mWidth = 20',
                            '35:addChannel 1 1.0 100 -6 4',
                            '35:addChannel 1 1.0 100 6 -4',
                            '35:onMode = off',
                            '35:onIfMatch 6 -4',
                            '35:onIfMatch -6 4',
                           ] 

