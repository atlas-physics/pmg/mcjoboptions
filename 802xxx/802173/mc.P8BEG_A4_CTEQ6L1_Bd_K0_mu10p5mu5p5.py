##############################################################
# Job options for B0 -> K_S0 mumu
##############################################################
evgenConfig.description   = "Exclusive B0 -> K_S0 mumu decay production"
evgenConfig.process       = "B0 -> K_S0 mumu"
evgenConfig.keywords      = [ "bottom", "B0", "2muon", "exclusive" ]
evgenConfig.contact       = [ "andrew.donald.gentry@cern.ch" ]
evgenConfig.nEventsPerJob = 1000
# Create EvtGen decay rules
f = open("Bd_K0_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Decay B0\n")
f.write("1.0000   K_S0 mu+ mu-  PHOTOS BTOSLLBALL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species
genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 11' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 11.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True
genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1
# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ 511 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bd_K0_MUMU_USER.DEC"
filtSeq.BSignalFilter.LVL1MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutPT             = 10500.0
filtSeq.BSignalFilter.LVL2MuonCutPT             = 5500.0
filtSeq.BSignalFilter.B_PDGCode                 = 511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

