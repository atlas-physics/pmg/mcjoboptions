include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

# Pythia configuration
genSeq.Pythia8.Commands += ["ExtraDimensionsG*:gg2G* = on",     # create gravitons
                            "ExtraDimensionsG*:ffbar2G* = on",  # create gravitons
                            "5100039:m0 = 3000.",                # set G pole mass
                            "5100039:onMode = off",             # switch off all graviton decays
                            "5100039:onIfAny = 11",             # turn on only G->ee decays
                            "ExtraDimensionsG*:kappaMG = 1.626",# setting some coupling constant
                            "5100039:mMin = 50.",               #
                            "PhaseSpace:mHatMin = 50."]         # set lower invariant mass

#Pythia user modes
#genSeq.Pythia8.UserHooks += ["GravFlat"]
#genSeq.Pythia8.Commands += ["GravFlat:EnergyMode = 13"]

# EVGEN configuration
evgenConfig.description = "RS Graviton, m = 3000. GeV, kappaMG = 1.626"
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = ["Thomas Martin Baer <thomas.martin.baer@cern.ch>"]
evgenConfig.process = "RS Graviton -> ee"
