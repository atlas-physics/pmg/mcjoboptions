# W' -> WZ -> qqqq
# Wprime Mass (in GeV)
# based on 802008

M_Wprime = 5000.

evgenConfig.contact = ["bnachman@cern.ch", "dnoll@cern.ch"]
evgenConfig.description = (
    "Wprime->WZ->qqqq mWprime = "
    + str(M_Wprime)
    + " GeV with NNPDF23LO PDF and A14 tune and W,Z SM-like"
)
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZ>qqqq"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]  # Allow Wprime
genSeq.Pythia8.Commands += ["Wprime:coup2WZ = 1."]  # Wprime Coupling to WZ
genSeq.Pythia8.Commands += ["34:m0 = " + str(M_Wprime)]  # Wprime mass
genSeq.Pythia8.Commands += ["34:onMode = off"]  # Turn off all Wprime decays
genSeq.Pythia8.Commands += ["34:onIfAll = 23 24"]  # Turn on Wprime->WZ
genSeq.Pythia8.Commands += ["24:onMode = off"]  # Turn off all Z decays
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4"]  # Turn on hadronic Z
genSeq.Pythia8.Commands += ["23:onMode = off"]  # Turn off all W decays
genSeq.Pythia8.Commands += ["23:onIfAny = 1 2 3 4"]  # Turn on hadronic W

genSeq.Pythia8.Commands += ["WeakZ0:gmZmode = 2"]  # No gamma^* contribution

genSeq.Pythia8.Commands += ["34:doForceWidth = on"]
genSeq.Pythia8.Commands += ["34:mWidth = 0.001"]  # small width of 1MeV