evgenConfig.description = "PYTHIA 8 b* -> cb, b* mass = lambda = 5000. GeV, f = 0.01"
evgenConfig.keywords    = ["exotic", "excitedQuark", "jets"]
evgenConfig.contact     = ["bingxuan.liu@cern.ch"]
evgenConfig.process     = "b* -> b+g"
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:bg2bStar = on",             # switch on bg -> b*
    "ExcitedFermion:Lambda = 5000.",           # Compositness scale
    "4000005:m0 = 5000.",                      # b* mass
    "4000005:onMode = off",                     # switch off all b* decays
    "4000005:onIfAny = 21",                     # switch on b*->g+X decays
    
    "ExcitedFermion:coupF = 0.01",        # coupling strength of SU(2)
    "ExcitedFermion:coupFprime = 0.01",   # coupling strength of U(1)
    "ExcitedFermion:coupFcol = 0.01"      # coupling strength of SU(3)
]

