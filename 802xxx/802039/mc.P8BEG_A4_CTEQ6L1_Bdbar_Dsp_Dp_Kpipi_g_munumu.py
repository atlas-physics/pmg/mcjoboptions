####################################################################
# Job Options for anti-B0->D*+(->D+(->K-pi+pi+)gamma)mu-anti-nu_mu #
####################################################################

### Meta Information ###
### ---------------- ### 
evgenConfig.description   = "Exclusive B0bar->D*+(->D+(K-pi+pi+)gamma)mu-anti-nu_mu decay production"
evgenConfig.process       = "anti-B0->D*+(->D+(->K-pi+pi+)gamma)mu-anti-nu_mu"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "B0", "exclusive", "1muon" ]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch" ]
evgenConfig.nEventsPerJob = 500

### Create EvtGen Decay File ###
### ------------------------ ###

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###
userDecayFileName = "Bdbar_Dsp_Dp_Kpipi_gamma_munu_mu_USER.DEC"
f = open( userDecayFileName, "w" )

### Create EvtGen Decay File -> Writing Decay Rules ###
### ----------------------------------------------- ###
f.write( "Define dm_incohMix_B_s0 0.0e12\n"                                                  )
f.write( "Define dm_incohMix_B0 0.0e12\n"                                                    )
f.write( "Alias my_D*+ D*+\n"                                                                )
f.write( "Alias my_D+ D+\n"                                                                  )
f.write( "Decay anti-B0\n"                                                                   )
f.write( "1.0000 my_D*+ mu- anti-nu_mu  PHOTOS HQET2 1.207 0.920 1.406 0.853;\n"                    )
f.write( "Enddecay\n"                                                                        )
f.write( "Decay my_D*+\n"                                                                     )
f.write( "1.0000 my_D+ gamma    VSP_PWAVE;\n"                                                )
f.write( "Enddecay\n"                                                                        )
f.write( "Decay  my_D+\n"                                                                    )
f.write( "1.0000  K- pi+ pi+  D_DALITZ;\n"                                                   )
f.write( "Enddecay\n"                                                                        )

### Create EvtGen Decay File -> Closing the File ###
### -------------------------------------------- ###
f.write( "End\n" )
f.close()

### Inheritance from Pythia8B_i ###
### --------------------------- ###
include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )
include( "GeneratorFilters/BSignalFilter.py"                )

### Process Configuration ###
### --------------------- ###

genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.TriggerPDGCode            = 0
genSeq.Pythia8B.SignalPDGCodes            = [ -511 ]
genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here: 
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src

if not hasattr(filtSeq, "ParentsTracksFilterHard"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterHard" )
if not hasattr(filtSeq, "ParentsTracksFilterSoft"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterSoft" )

filtSeq.ParentsTracksFilterHard.AllowChargeConjParent = False
filtSeq.ParentsTracksFilterSoft.AllowChargeConjParent = False

filtSeq.ParentsTracksFilterHard.PDGParent       = [ -511 ]
filtSeq.ParentsTracksFilterHard.PtMinLeptons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilterHard.PtMinHadrons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterHard.NumMinTracks    = 2
filtSeq.ParentsTracksFilterHard.NumMinLeptons   = 1
filtSeq.ParentsTracksFilterHard.NumMaxLeptons   = 1

filtSeq.ParentsTracksFilterSoft.PDGParent       = [ -511 ]
filtSeq.ParentsTracksFilterSoft.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilterSoft.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterSoft.NumMinTracks    = 4

