# Z' -> tt based on 800030, but turning on ttbar decays only

evgenConfig.description = "Pythia8 Z'->ttbar with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "Z' -> t + tbar"
evgenConfig.contact = ["ohm@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]

include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.UserHooks += ["ZprimeFlatpT"]

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", # create Z bosons
                            "PhaseSpace:mHatMin = 25.0",          # minimum inv.mass cut
                            "32:m0 = 4000.0",                     # mZ' = 4 TeV
                            "32:onMode = off",                    # switch off all Z' decays
                            "32:addChannel = 1 1.00 100 -6 6",    # turn on only tt decay for Z' (onMode bRatio meMode products)
                            "24:onMode = off",                    # switch off all W decays
                            "24:onIfAny = 1 2 3 4 5"              # turn on hadronic W decays
                           ]

#Only Z' - no gamma/Z
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3",
                            "ZprimeFlatpT:MaxSHat=" + str(runArgs.ecmEnergy),
                            "ZprimeFlatpT:DoDecayWeightBelow=2000"]

testSeq.TestHepMC.MaxVtxDisp = 1500.0

include("Pythia8_i/Pythia8_ShowerWeights.py")
