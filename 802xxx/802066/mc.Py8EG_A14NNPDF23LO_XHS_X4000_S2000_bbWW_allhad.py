include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.process     = "gg->X->SH->bb+jets"
evgenConfig.contact = ['JaeJin Hong <jae.jin.hong@cern.ch>']
evgenConfig.description = "Generation of gg > X > SH where S decays to W+W- which decay to jets and H decays to bb"
evgenConfig.keywords = ["BSMHiggs"]

# 36 = X, 35 = S, 25 = H
genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'HiggsA3:parity = 1',
                            'Higgs:clipWings = off',
                            '36:m0 = 4000.0', # XMass
                            '36:mWidth = 0.01', # XWidth
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35', # X > SH
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 2000.0', # SMass
                            '35:mWidth = 0.01', # SWidth
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 5 -5', # H > bb
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24', # S > WW
                            "24:onMode = off",
                            "24:onIfAny = 1 2 3 4 5"  # fully hadronic W decays
                            ]

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("SWWFilter", PDGParent = [35], PDGChild = [24])

filtSeq.Expression = "HbbFilter and SWWFilter"