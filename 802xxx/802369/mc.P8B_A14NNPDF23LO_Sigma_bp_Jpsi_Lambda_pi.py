evgenConfig.description = "Pythia8B Sigma_b+ -> Jpsi + Lambda + pi"
evgenConfig.process = "Sigma_b+ -> Jpsi + Lambda + pi"
evgenConfig.keywords = ["muon","Jpsi","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["shaogang.peng@cern.ch"]
evgenConfig.nEventsPerJob = 50

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
#genSeq.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
genSeq.Pythia8B.QuarkPtCut = 6.0
genSeq.Pythia8B.AntiQuarkPtCut = 6.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
#
genSeq.Pythia8B.NHadronizationLoops = 10  # 1 (old value)

# Bs
genSeq.Pythia8B.Commands += ['5222:onMode = off']
genSeq.Pythia8B.Commands += ['5222:addChannel = 1 1.00 0 443 3122 211']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:onIfMatch = 13 13']
genSeq.Pythia8B.SignalPDGCodes = [5222,443,13,-13,3122,211] ## need the full decay chain, select Sigma_b+ decay

genSeq.Pythia8B.SignalPtCuts = [0.0,0.0,3.5,3.5,0.0,0.48]
genSeq.Pythia8B.SignalEtaCuts = [102.5,102.5,2.7,2.7,102.5,2.6]
genSeq.Pythia8B.NumberOfSignalsRequiredPerEvent = 1

