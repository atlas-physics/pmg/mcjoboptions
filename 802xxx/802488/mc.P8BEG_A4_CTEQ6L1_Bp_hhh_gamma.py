########################################################
# Job Options for B+->hhh_gamma                         #
########################################################

### Meta Information ###
### ---------------- ###
evgenConfig.description   = "Custom B+->hhh_gamma Decay Production"
evgenConfig.process       = "B+->hhh_gamma"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "Bplus", "1photon"]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch", "dvij.mankad@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

### Create EvtGen Decay File ###
### ------------------------ ###
userDecayFileName = "DEC_B+_To_h_h_h_gamma.dat"


### Inheritance from Pythia8B_i ###
### --------------------------- ###
include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )

### Process Configuration ###
### --------------------- ###
genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName
evgenConfig.auxfiles += [ genSeq.EvtInclusiveDecay.userDecayFile ]
genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.TriggerPDGCode            = 0
genSeq.Pythia8B.SignalPDGCodes            = [ 521 ]
genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName
evgenConfig.auxfiles += [ genSeq.EvtInclusiveDecay.userDecayFile ]

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here:
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src

if not hasattr(filtSeq, "ParentsTracksFilterHard"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterHard" )
if not hasattr(filtSeq, "ParentsTracksFilterSoft"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter # Do we need both of them?
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterSoft" )

filtSeq.ParentsTracksFilterHard.AllowChargeConjParent = False
filtSeq.ParentsTracksFilterSoft.AllowChargeConjParent = False   # Do we need both of them?

filtSeq.ParentsTracksFilterHard.PDGParent       = [ 521 ]
filtSeq.ParentsTracksFilterHard.PtMinHadrons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterHard.NumMinTracks    = 2

filtSeq.ParentsTracksFilterSoft.PDGParent       = [ 521 ]
filtSeq.ParentsTracksFilterSoft.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilterSoft.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterSoft.NumMinTracks    = 3

