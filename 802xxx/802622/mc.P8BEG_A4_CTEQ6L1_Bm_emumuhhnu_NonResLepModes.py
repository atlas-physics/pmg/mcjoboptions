#####################################################################
# Job Options for B-->emumuhhnu (w/o DiLepton from "Heavy" Resonance) #
#####################################################################

### Meta Information ###
### ---------------- ### 

evgenConfig.description   = "Almost Exhaustive B-->emumuhhnu (w/o DiLeptons from 'Heavy' Resonance) Production"
evgenConfig.process       = "B-->emumuhhnu"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "Bplus", "3muon", "neutrino" ]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch", "dvij.mankad@cern.ch" ]
evgenConfig.nEventsPerJob = 200

### Create EvtGen Decay File ###
### ------------------------ ###

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###

userDecayFileName = "DEC_Bm_To_mu_mu_mu_h_h_nu_NonResLepModes.dat"

### Inheritance from Pythia8B_i ###
### --------------------------- ###

include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )

### Process Configuration ###
### --------------------- ###

genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName
evgenConfig.auxfiles += [ genSeq.EvtInclusiveDecay.userDecayFile ]
genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.AntiQuarkPtCut                = 0.0
genSeq.Pythia8B.QuarkPtCut            = 7.5
genSeq.Pythia8B.AntiQuarkEtaCut               = 102.5
genSeq.Pythia8B.QuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.TriggerPDGCode            = 0
genSeq.Pythia8B.SignalPDGCodes            = [ -521 ]

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here: 
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src

# NOTE: This filter will allow an event w/ 3 leptons above threshold 
# and only 1 hadron above threshold. This is on purpose since the lepton 
# track can participate as a hadron track in B-vertexing. 
# Might be vetoed later in the analysis.

if not hasattr(filtSeq, "ParentsTracksFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilter" )

filtSeq.ParentsTracksFilter.AllowChargeConjParent = False

filtSeq.ParentsTracksFilter.PDGParent       = [ -521 ]
filtSeq.ParentsTracksFilter.PtMinLeptons    = 4000.0
filtSeq.ParentsTracksFilter.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilter.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter.NumMinLeptons   = 2
filtSeq.ParentsTracksFilter.NumMinTracks    = 4
