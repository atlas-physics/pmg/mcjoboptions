#--------------------------------------------------------------------------------------------------------
#Pythia8 pp/gg -> Cascade_bc0 -> Lambda + B0bar(-> Jpsi + K- + pi+ ) production (Pythia8B as generator)
#--------------------------------------------------------------------------------------------------------

evgenConfig.nEventsPerJob = 5000
evgenConfig.description = "Pythia8B pp/gg -> Cascade_bc0 -> Lambda + B0bar(-> Jpsi + K- + pi+ )"
evgenConfig.keywords = ["heavyFlavour","Jpsi","Muon"]
evgenConfig.contact = ["yuxuan.zhang@cern.ch"]
evgenConfig.process = "pp/gg ->  Cascade_bc0 -> Lambda + B0bar(-> Jpsi + K- + pi+ )"
evgenConfig.generators += ['Pythia8B']

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

genSeq.Pythia8B.Commands += [
'Higgs:useBSM = on',
'HiggsBSM:gg2H2 = on',
'HiggsH2:coup2d = 10.0',
'HiggsH2:coup2u = 10.0',
'HiggsH2:coup2Z = 0.0',
'HiggsH2:coup2W = 0.0',
'HiggsA3:coup2H2Z = 0.0',
'HiggsH2:coup2A3A3 = 0.0',
'HiggsH2:coup2H1H1 = 0.0',
'35:mMin = 0',
'35:mMax = 15.0',
############# For Fixed Mass Distribution#############
'35:m0 = 7.45', # Cascade_bc0,
'35:mWidth = 0.1', 
'35:onMode = off',
'35:addChannel = 1 1.00 100 3122 -511', #Lambda + B0bar
'511:onMode = off',
'511:addChannel = 1 1.00 100 443 321 -211',
'511:onIfMatch = 443 321 -211',
'443:onMode = off',
'443:onIfMatch = 13 13',
]
## Lambda have long lifetime and decay in Geant4

genSeq.Pythia8B.SignalPDGCodes = [35,3122,-511,443,13,-13,-321,211] ## need the full decay chain, select Cascade_bc0 decay
genSeq.Pythia8B.SignalPtCuts = [0.0,0.0,0.0,0.0,3.5,3.5,0.45,0.45]
genSeq.Pythia8B.SignalEtaCuts = [102.5,102.5,102.5,102.5,2.7,2.7,2.6,2.6]
genSeq.Pythia8B.NumberOfSignalsRequiredPerEvent = 1


