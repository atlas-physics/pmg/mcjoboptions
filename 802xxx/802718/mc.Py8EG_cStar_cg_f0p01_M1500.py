evgenConfig.description = "PYTHIA 8 c* -> cg, c* mass = lambda = 1500. GeV, f = 0.01"
evgenConfig.keywords    = ["exotic", "excitedQuark", "jets"]
evgenConfig.contact     = ["bingxuan.liu@cern.ch"]
evgenConfig.process     = "c* -> c+g"
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:cg2cStar = on",             # switch on cg -> c*
    "ExcitedFermion:Lambda = 1500.",           # Compositness scale
    "4000004:m0 = 1500.",                      # c* mass
    "4000004:onMode = off",                     # switch off all c* decays
    "4000004:onIfAny = 21",                     # switch on c*->g+X decays
    
    "ExcitedFermion:coupF = 0.01",        # coupling strength of SU(2)
    "ExcitedFermion:coupFprime = 0.01",   # coupling strength of U(1)
    "ExcitedFermion:coupFcol = 0.01"      # coupling strength of SU(3)
]

