evgenConfig.description   = "Exclusive anti-Lambda_b -> J/psi anti-Lambda0 decay production"
evgenConfig.process       = "anti-Lambda_b -> J/psi(mumu) Lambda0"
evgenConfig.keywords      = [ "bottom", "Lambda", "Lambda_b0", "2muon", "exclusive" ]
evgenConfig.contact       = [ "Semen.Turchikhin@cern.ch" ]
evgenConfig.nEventsPerJob = 200

f = open("ANTILAMBDAB0_JPSI_LAMBDA.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias myJ/psi J/psi\n")
f.write("Decay  myJ/psi\n")
f.write("1.0000   mu+ mu-   PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("Decay anti-Lambda_b0\n")
f.write("  1.0    anti-Lambda0    myJ/psi      PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()


include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("GeneratorFilters/ParentChildwStatusFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 5

genSeq.EvtInclusiveDecay.userDecayFile = "ANTILAMBDAB0_JPSI_LAMBDA.DEC"

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -5122 ]

filtSeq.BSignalFilter.LVL1MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutPT             = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutPT             = 3500.0

filtSeq.ParentChildwStatusFilter.PDGParent  = [5122]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  0.0
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 1000.
filtSeq.ParentChildwStatusFilter.PDGChild = [3122]
filtSeq.ParentChildwStatusFilter.PtMinChild = 2000.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 1000.
