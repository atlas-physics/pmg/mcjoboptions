############################################
# Job Options for B0->D-(->K+pi-pi-)mu+nu_mu #
############################################

### Meta Information ###
### ---------------- ### 
evgenConfig.description   = "Exclusive B0->D-(->K+pi-pi-)mu+nu_mu decay production"
evgenConfig.process       = "B0->D-(->K+pi-pi-)mu+nu_mu"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "B0", "exclusive", "1muon" ]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

### Create EvtGen Decay File ###
### ------------------------ ###

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###
userDecayFileName = "Bd_DMinusKpipi_pi_mu_nu_muUSER.DEC"
f = open( userDecayFileName, "w" )

### Create EvtGen Decay File -> Writing Decay Rules ###
### ----------------------------------------------- ###
f.write( "Define dm_incohMix_B_s0 0.0e12\n"                                              )
f.write( "Define dm_incohMix_B0 0.0e12\n"                                                )
f.write( "Alias my_D- D-\n"                                                              )
f.write( "Decay  my_D-\n"                                                                )
f.write( "1.0000  K+ pi- pi-  D_DALITZ;\n"                                               )
f.write( "Enddecay\n"                                                                    )
f.write( "Decay B0\n"                                                                    )
# BELLE2 does this decay using `BGL` model, but it is not available in Pythia8B_i? 
#f.write( "1.0000  my_D- e+ nu_e  BGL 0.0126 -0.094 0.34 -0.1 0.0115 -0.057 0.12 0.4;\n" )
f.write( "1.0000 my_D- mu+ nu_mu  PHOTOS HQET2 1.185 1.081;\n"                             )
f.write( "Enddecay\n"                                                                    )

### Create EvtGen Decay File -> Closing the File ###
### -------------------------------------------- ###
f.write( "End\n" )
f.close()

### Inheritance from Pythia8B_i ###
### --------------------------- ###
include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )
include( "GeneratorFilters/BSignalFilter.py"                )

### Process Configuration ###
### --------------------- ###

genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here: 
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src


if not hasattr(filtSeq, "ParentsTracksFilterHard"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterHard" )
if not hasattr(filtSeq, "ParentsTracksFilterSoft"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilterSoft" )

filtSeq.ParentsTracksFilterHard.AllowChargeConjParent = False
filtSeq.ParentsTracksFilterSoft.AllowChargeConjParent = False

filtSeq.ParentsTracksFilterHard.PDGParent       = [ 511 ]
filtSeq.ParentsTracksFilterHard.PtMinLeptons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilterHard.PtMinHadrons    = 4000.0
filtSeq.ParentsTracksFilterHard.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterHard.NumMinTracks    = 2
filtSeq.ParentsTracksFilterHard.NumMinLeptons   = 1
filtSeq.ParentsTracksFilterHard.NumMaxLeptons   = 1

filtSeq.ParentsTracksFilterSoft.PDGParent       = [ 511 ]
filtSeq.ParentsTracksFilterSoft.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilterSoft.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilterSoft.NumMinTracks    = 4

