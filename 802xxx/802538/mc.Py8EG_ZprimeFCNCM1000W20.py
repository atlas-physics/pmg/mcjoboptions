evgenConfig.description = "Z' decaying to charm and top quarks (FCNC decay)"
evgenConfig.contact = ['Marc Tost <marctost11@gmail.com>']
evgenConfig.keywords = ['BSM', 'FCNC']
evgenConfig.generators += ['Pythia8']
evgenConfig.process = "Zprime>tc"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ['NewGaugeBoson:ffbar2gmZZprime = on',
                            '32:m0 = 1000',
                            '32:mMin = 0',
                            '32:mWidth = 20',
                            '32:addChannel 1 1.0 100 -6 4',
                            '32:addChannel 1 1.0 100 6 -4',
                            '32:onMode = off',
                            '32:onIfMatch 6 -4',
                            '32:onIfMatch -6 4',
                           ]
