######################################################################
# Job Options for Bs->eehhgamma (w/ DiLepton from "Heavy" Resonance) #
######################################################################

### Meta Information ###
### ---------------- ### 

evgenConfig.description   = "Almost Exhaustive Bs->eehhgamma (w/ DiLeptons from 'Heavy' Resonance) Production"
evgenConfig.process       = "Bs->eehhgamma"
# The list of allowed keywords (for the Athena 21.6.83):
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
evgenConfig.keywords      = [ "bottom", "Bs", "2electron" ]
evgenConfig.contact       = [ "giannis.maniatis@cern.ch", "dvij.mankad@cern.ch" ]
evgenConfig.nEventsPerJob = 100

### Create EvtGen Decay File ###
### ------------------------ ###

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###

userDecayFileName = "DEC_Bs_To_e_e_h_h_gamma_ResLepModes.dat"

### Inheritance from Pythia8B_i ###
### --------------------------- ###

include( "Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py" )
include( "Pythia8B_i/Pythia8B_BPDGCodes.py"                 )

### Process Configuration ###
### --------------------- ###

genSeq.EvtInclusiveDecay.userDecayFile    = userDecayFileName
evgenConfig.auxfiles += [ genSeq.EvtInclusiveDecay.userDecayFile ]

genSeq.Pythia8B.Commands                 += [ "HardQCD:all         = on"  ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ "ParticleDecays:mixB = off" ]
genSeq.Pythia8B.Commands                 += [ "HadronLevel:all     = off" ]
genSeq.Pythia8B.Commands                 += [ "PhaseSpace:pTHatMin = 7"   ]
genSeq.Pythia8B.NHadronizationLoops       = 4
genSeq.Pythia8B.NDecayLoops               = 1

### Intermediate Cuts ###
### ----------------- ###

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.TriggerPDGCode            = 0
genSeq.Pythia8B.SignalPDGCodes            = [ 531 ]

### Final State Cuts ###
### ---------------- ###

# The filters are implemented here: 
# https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.83/Generators/GeneratorFilters/src

if not hasattr(filtSeq, "ParentsTracksFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    filtSeq += ParentsTracksFilter( "ParentsTracksFilter" )

filtSeq.ParentsTracksFilter.AllowChargeConjParent = False

filtSeq.ParentsTracksFilter.PDGParent       = [ 531 ]
filtSeq.ParentsTracksFilter.PtMinLeptons    = 4000.0
filtSeq.ParentsTracksFilter.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter.PtMinHadrons    = 400.0
filtSeq.ParentsTracksFilter.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter.NumMinLeptons   = 2
filtSeq.ParentsTracksFilter.NumMinTracks    = 4
