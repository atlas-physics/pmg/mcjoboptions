evgenConfig.description = "Inclusive pp->bb->J/psi(1S)(mu6mu6) production with EvtGen"
evgenConfig.process = "bb -> J/Psi(1S) -> 2mu"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.contact  = ["emilien.chapon@cern.ch"] #Adpated from 801164
evgenConfig.nEventsPerJob = 10000

# include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py')
include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include("Pythia8B_i/Pythia8B_inclusiveBJpsi_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = off']
genSeq.Pythia8B.Commands += ['HardQCD:hardbbbar = on'] 


genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

# Setting for production
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 6.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.8
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.NHadronizationLoops = 1

# mu pT cuts
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [6.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]

