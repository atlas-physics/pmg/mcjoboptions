#--------------------------------------------------------------
#Pythia8 pp/gg -> X -> J/psi+Psi(2S) ->4mu+2pion production with JP=0-
#--------------------------------------------------------------

evgenConfig.nEventsPerJob = 10000
evgenConfig.description = "Pythia8 pp/gg -> X -> J/psi+Psi(2S) ->4mu"
evgenConfig.keywords = ["heavyFlavour","Jpsi","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg -> X -> J/psi+Psi(2S) ->4mu"
evgenConfig.generators += ['Pythia8']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")

genSeq.Pythia8.Commands += [
'Higgs:useBSM = on',
'HiggsBSM:gg2H2 = on',
'HiggsH2:coup2d = 10.0',
'HiggsH2:coup2u = 10.0',
'HiggsH2:coup2Z = 0.0',
'HiggsH2:coup2W = 0.0',
'HiggsA3:coup2H2Z = 0.0',
'HiggsH2:coup2A3A3 = 0.0',
'HiggsH2:coup2H1H1 = 0.0',
'35:mMin = 0',
'35:mMax = 15.0',
############# For Fixed Mass Distribution#############
'35:m0 = 7.2', # 9.3987
'35:mWidth = 0.1', #0.01
'35:addChannel = 1 1.00 100 443 100443',
'35:onMode = off',
'35:onIfMatch = 443 100443',
'443:onMode = off',
'443:onIfMatch = 13 13',
'100443:onMode = off',
'100443:onIfMatch = 13 13'
]

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   filtSeq += muonfilter1
   filtSeq += muonfilter2

filtSeq.muonfilter1.Ptcut = 2000.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 4 #minimum

filtSeq.muonfilter2.Ptcut = 2500.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 2 #minimum
