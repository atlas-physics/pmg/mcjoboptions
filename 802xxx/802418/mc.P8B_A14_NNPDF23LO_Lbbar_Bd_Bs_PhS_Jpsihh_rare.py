################################################################################
# Job options for Pythia8B_i generation of Lambda_bbar, Bd, Bs->J/psi(mumu)hh
# phase space decays
################################################################################
evgenConfig.description = "Signal Hb->J/psi(mumu)hh (including rare decays), Hb = Lbbar, Bd, Bs"
evgenConfig.keywords = ["bottom","exclusive","Lambda_b0","Jpsi","2muon"]
evgenConfig.contact = [ 'artem.vasyukov@cern.ch' ]
evgenConfig.process = "pp->bb->H_b->J/psi(mumu)hh"
evgenConfig.nEventsPerJob = 500

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.VetoDoubleBEvents = True

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2017
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
#
# Lambda_b:
#
genSeq.Pythia8B.Commands += ['5122:m0 = 5.61960']  # PDG 2017
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.4407'] # PDG 2017
#
# Lambda_b decays:
#
genSeq.Pythia8B.Commands += ['5122:onMode = 2']
#
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 1.00 0 443 2212 -321']
#
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.04 0 443 2212 -211']
#
# B_d:
#
genSeq.Pythia8B.Commands += ['511:m0 = 5.27963']  # PDG 2017
genSeq.Pythia8B.Commands += ['511:tau0 = 0.4557'] # PDG 2017
#
# B_d decays:
#
genSeq.Pythia8B.Commands += ['511:onMode = 3']
#
# channels 850-869 have onMode = 2 in Pythia8 default
#
genSeq.Pythia8B.Commands += ['511:850:onMode = 0']
genSeq.Pythia8B.Commands += ['511:851:onMode = 0']
genSeq.Pythia8B.Commands += ['511:852:onMode = 0']
genSeq.Pythia8B.Commands += ['511:853:onMode = 0']
genSeq.Pythia8B.Commands += ['511:854:onMode = 0']
genSeq.Pythia8B.Commands += ['511:855:onMode = 0']
genSeq.Pythia8B.Commands += ['511:856:onMode = 0']
genSeq.Pythia8B.Commands += ['511:857:onMode = 0']
genSeq.Pythia8B.Commands += ['511:858:onMode = 0']
genSeq.Pythia8B.Commands += ['511:859:onMode = 0']
genSeq.Pythia8B.Commands += ['511:860:onMode = 0']
genSeq.Pythia8B.Commands += ['511:861:onMode = 0']
genSeq.Pythia8B.Commands += ['511:862:onMode = 0']
genSeq.Pythia8B.Commands += ['511:863:onMode = 0']
genSeq.Pythia8B.Commands += ['511:864:onMode = 0']
genSeq.Pythia8B.Commands += ['511:865:onMode = 0']
genSeq.Pythia8B.Commands += ['511:866:onMode = 0']
genSeq.Pythia8B.Commands += ['511:867:onMode = 0']
genSeq.Pythia8B.Commands += ['511:868:onMode = 0']
genSeq.Pythia8B.Commands += ['511:869:onMode = 0']
#
#
genSeq.Pythia8B.Commands += ['511:addChannel = 2 1.00 0  443 321 -211']
#
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.04 0  443 211 -211']
#
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.04 0  443 321 -321']
# B_u:
#
genSeq.Pythia8B.Commands += ['521:m0 = 5.27933']  # PDG 2017
genSeq.Pythia8B.Commands += ['521:tau0 = 0.4911'] # PDG 2017
#
# B_s:
#
genSeq.Pythia8B.Commands += ['531:m0 = 5.36689']  # PDG 2017
genSeq.Pythia8B.Commands += ['531:tau0 = 0.4524'] # PDG 2017
#
#
# B_s decays:
#
genSeq.Pythia8B.Commands += ['531:onMode = 3']
#
# channels 242-243 have onMode = 2 in Pythia8 default
#
genSeq.Pythia8B.Commands += ['531:242:onMode = 0']
genSeq.Pythia8B.Commands += ['531:243:onMode = 0']
#
# channels 244-245 have onMode = 3 in Pythia8 default
#
#genSeq.Pythia8B.Commands += ['531:244:onMode = 0']
#genSeq.Pythia8B.Commands += ['531:245:onMode = 0']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 1.00 0  443 321 -321']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.04 0  443 -321 211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.04 0  443 211 -211']
#
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.Pythia8B.SignalPtCuts = [0.,3.5,3.5]
genSeq.Pythia8B.SignalEtaCuts = [100.,2.5,2.5]
#
genSeq.Pythia8B.OutputLevel = INFO
genSeq.Pythia8B.NHadronizationLoops = 1

from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter

filtSeq += ParentsTracksFilter("ParentsTracksFilter")

filtSeq.ParentsTracksFilter.PDGParent  = [5122,511,531]
filtSeq.ParentsTracksFilter.PtMinParent =  11000.
filtSeq.ParentsTracksFilter.EtaRangeParent = 2.4
filtSeq.ParentsTracksFilter.PtMinLeptons = 3500.
filtSeq.ParentsTracksFilter.EtaRangeLeptons = 2.5
filtSeq.ParentsTracksFilter.PtMinHadrons = 1400.
filtSeq.ParentsTracksFilter.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter.NumMinTracks = 4
filtSeq.ParentsTracksFilter.NumMaxTracks = 4
filtSeq.ParentsTracksFilter.NumMinLeptons = 2
filtSeq.ParentsTracksFilter.NumMaxLeptons = 2
filtSeq.ParentsTracksFilter.NumMinOthers = 0
filtSeq.ParentsTracksFilter.NumMaxOthers = 0
