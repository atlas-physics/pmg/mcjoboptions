#Pythia8 minimum bias ND with A3

evgenConfig.description = "Non-diffractive events producing di-jets. Using the A3 NNPDF23LO tune and Rockefeller."
evgenConfig.keywords = ["QCD", "minBias","diffraction"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "malewick@cern.ch" ]
# how many events should be produced in one production job
evgenConfig.nEventsPerJob = 10000

include ("Pythia8_i/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

