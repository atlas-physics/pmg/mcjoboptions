include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                            "HiggsBSM:gg2A3 = on",
                            "36:m0 = 75.000",
                            "36:mMin = 15",
                            "36:onMode = off",
                            "36:onIfAny = 15",
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "-15:onMode = off",
                            "-15:onIfAny = 11 13"]

evgenConfig.process     = "gg->A(75), A(75)->tautau"
evgenConfig.description = "Pythia 8 A->tau(lep)tau(lep) production with NNPDF23LO tune"
evgenConfig.keywords    = ["BSM", "Higgs", "2tau"]
evgenConfig.contact     = ["Tom Kresse <tom.kresse@cern.ch>"]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep15lep6filter = TauFilter("lep15lep6filter")
  filtSeq += lep15lep6filter

filtSeq.lep15lep6filter.UseNewOptions = True
filtSeq.lep15lep6filter.Ntaus = 2
filtSeq.lep15lep6filter.Nleptaus = 2
filtSeq.lep15lep6filter.Nhadtaus = 0
filtSeq.lep15lep6filter.EtaMaxlep = 2.7
filtSeq.lep15lep6filter.Ptcutlep = 6000.0 #MeV
filtSeq.lep15lep6filter.Ptcutlep_lead = 15000.0 #MeV