###########################################################
# Emerging Jets Event Generation
# Pythia 8: Xd Xd --> q Qd q Qd --> 2j 2EJ
# contact: Colleen Treado (colleen.jennifer.treado@cern.ch)
#==========================================================

evgenConfig.description = "emerging jet events from pair-produced dark quarks"
evgenConfig.keywords = ["exotic", "hiddenValley", "4jet"]
evgenConfig.process = "p p --> Xd Xd --> q Qd q Qd --> 2QcdJ 2EJ"
evgenConfig.contact = ["patrick.scholer@cern.ch"]

# specify PDF + tune
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

print("ARGS: ", runArgs.jobConfig[0])
print("JO ARGS: ", jofile.rstrip('.py').split('_'))

# set sample / model parameters automatically based on jo name
m_Xd = float(jofile.rstrip('.py').split('_')[3])
ctau_pi_d_str = jofile.rstrip('.py').split('_')[4]

if "p" in ctau_pi_d_str:
    ctau_pi_d_pre = ctau_pi_d_str.split('p')[0]
    ctau_pi_d_post = ctau_pi_d_str.split('p')[1]
    ctau_pi_d_str = ctau_pi_d_pre + "." + ctau_pi_d_post
ctau_pi_d = float(ctau_pi_d_str)

print("SCALAR MEDIATOR MASS: %f " % m_Xd)
print("DARK PION LIFETIME: %f " % ctau_pi_d)

mod = jofile.rstrip('.py').split('_')[2]
print("MODEL: %s " % mod)
if mod == "ModelA":
    m_pi_d = 5.0
elif mod == "ModelB":
    m_pi_d = 2.0
elif mod == "ModelC":
    m_pi_d = 10.0
elif mod == "ModelD":
    m_pi_d = 20.0
elif mod == "ModelE":
    m_pi_d = 0.8
print("DARK PION MASS: %f " % m_pi_d)
print("DARK RHO MASS: %f " % (m_pi_d*4))
print("LAMBDA / DARK QUARK MASS: %f " % (m_pi_d*2))
print("PT MIN FSR: %f " % (m_pi_d*2*1.1))

# show 5 events for testing
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 5"]


## OVERRIDE STANDARD ATLAS TAU0 LIMIT ##
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"]


# settings for dark sector 
genSeq.Pythia8.Commands += ["4900001:m0 = " + str(m_Xd), # Xd mass - variable
                            "4900001:mWidth = 10", # Xd width
                            "HiddenValley:spinFV = 0",
                            "4900001:isResonance = on",
                            "4900001:mayDecay = on",
                            "4900001:0:bRatio = 1",
                            "4900001:0:meMode = 102",
                            "HiddenValley:Ngauge = 3", # n dark QCD colors
                            "HiddenValley:alphaFSR = 0.7"] # fixed dark coupling
# Model settings
genSeq.Pythia8.Commands += ["4900101:m0 = " + str(m_pi_d*2), # qd mass
                            "4900111:m0 = " + str(m_pi_d), # pi_d mass
                            "4900113:m0 = " + str(m_pi_d*4), # rho_d mass
                            "4900211:m0 = " + str(m_pi_d), # pi_d off-diag mass
                            "4900213:m0 = " + str(m_pi_d*4), # rho_d off-diag mass
                            "HiddenValley:Lambda = " + str(m_pi_d*2),
                            "HiddenValley:pTminFSR = " + str(m_pi_d*2*1.1)] # pT cutoff for dark shower

# dark pion lifetime
genSeq.Pythia8.Commands += ["4900111:tau0 = " + str(ctau_pi_d)] # pi_d lifetime -- variable
# off-diagonal dark pion lifetime    
genSeq.Pythia8.Commands += ["4900211:tau0 = " + str(ctau_pi_d)] 

# dark meson decays
genSeq.Pythia8.Commands += ["4900111:0:all on 1.0 91 1 -1", # dark pion to down quarks
                            "4900113:0:all on 0.999 102 4900111 4900111", # dark vector to dark pions 99.9%
                            "4900113:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%
# dark meson off-diagonal decays
genSeq.Pythia8.Commands += ["4900211:oneChannel on 1.0 91 1 -1", # dark pion to down quarks
                            "4900213:oneChannel on 0.999 102 4900211 4900211", # dark vector to dark pions 99.9%
                            "4900213:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%

# non-model dependent settings
genSeq.Pythia8.Commands += ["PartonLevel:MPI = on",
                            "PartonLevel:ISR = on"]

# emerging jet event processes
genSeq.Pythia8.Commands += ["HiddenValley:gg2DvDvbar = on",
                            "HiddenValley:qqbar2DvDvbar = on"]

# dark QCD coupling (alphaHV) running
genSeq.Pythia8.Commands += ["HiddenValley:alphaOrder = 1",
                            "HiddenValley:nFlav = 7"]

# HV parton shower settings
genSeq.Pythia8.Commands += ["HiddenValley:FSR = on",
                            "HiddenValley:fragment = on"]

# workarounds for TestHepMC
testSeq.TestHepMC.MaxVtxDisp=5000000.
testSeq.TestHepMC.MaxTransVtxDisp = 5000000.



## JET FILTERING ##
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.TruthJetFilter.Njet = 4
filtSeq.TruthJetFilter.NjetMinPt = 100*GeV
filtSeq.TruthJetFilter.NjetMaxEta = 2.7
                
