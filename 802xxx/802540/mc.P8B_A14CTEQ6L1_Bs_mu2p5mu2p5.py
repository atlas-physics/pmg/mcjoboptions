##############################################################
f = open("BS_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Decay B_s0\n")
f.write("1.0000 mu+   mu-       PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B0s -> mu+ mu- production"
evgenConfig.keywords    = ["exclusive","Bs","2muon","rareDecay"]
evgenConfig.nEventsPerJob = 1000
evgenConfig.contact = ['yue.xu@cern.ch', 'dominic.matthew.jones@cern.ch']

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("Pythia8B_i/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.']

genSeq.Pythia8B.QuarkPtCut      = 0.0
genSeq.Pythia8B.AntiQuarkPtCut  = 4.0
genSeq.Pythia8B.QuarkEtaCut     = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [531]

genSeq.EvtInclusiveDecay.userDecayFile = "BS_MUMU_USER.DEC"

if not hasattr(filtSeq, "BSignalFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
   BSigfilter = BSignalFilter("BSigfilter")
   filtSeq += BSigfilter

filtSeq.BSigfilter.LVL1MuonCutOn  = True
filtSeq.BSigfilter.LVL2MuonCutOn  = True
filtSeq.BSigfilter.LVL1MuonCutPT  = 2500
filtSeq.BSigfilter.LVL1MuonCutEta = 2.7
filtSeq.BSigfilter.LVL2MuonCutPT  = 2500
filtSeq.BSigfilter.LVL2MuonCutEta = 2.7
