#--------------------------------------------------------------
#Pythia8B pp/gg -> X -> J/psi(->2mu) + eta_c(->6pion) production
#--------------------------------------------------------------

evgenConfig.nEventsPerJob = 1000
evgenConfig.description = "Pythia8B pp/gg -> X -> J/psi(->2mu) + eta_c(->6pion)"
evgenConfig.keywords = ["heavyFlavour","Jpsi","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg -> X -> J/psi(->2mu) + eta_c(->6pion)"
evgenConfig.generators += ['Pythia8B']

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

genSeq.Pythia8B.Commands += [
'Higgs:useBSM = on',
'HiggsBSM:gg2H2 = on',
'HiggsH2:coup2d = 10.0',
'HiggsH2:coup2u = 10.0',
'HiggsH2:coup2Z = 0.0',
'HiggsH2:coup2W = 0.0',
'HiggsA3:coup2H2Z = 0.0',
'HiggsH2:coup2A3A3 = 0.0',
'HiggsH2:coup2H1H1 = 0.0',
'35:mMin = 0',
'35:mMax = 10.0',
############# For Fixed Mass Distribution#############
'35:m0 = 6.6', # 9.3987
'35:mWidth = 0.1', #0.01
'35:addChannel = 1 1.00 100 443 441',
'35:onMode = off',
'35:onIfMatch = 443 441',
'443:onMode = off',
'443:onIfMatch = 13 13',
'441:onMode = off',
'441:addChannel = 1 1.00 0 211 211 211 -211 -211 -211',
'441:onIfMatch = 211 211 211 211 211 211'
]

genSeq.Pythia8B.SignalPDGCodes = [35,443,-13,13,441,211,-211,211,-211,211,-211]
genSeq.Pythia8B.SignalPtCuts = [0.0,0.0,3.5,3.5,0.0,0.8,0.8,0.8,0.8,0.8,0.8]
genSeq.Pythia8B.SignalEtaCuts = [102.5,102.5,2.7,2.7,102.5,2.7,2.7,2.7,2.7,2.7,2.7]
genSeq.Pythia8B.NumberOfSignalsRequiredPerEvent = 1
