import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl import MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import subprocess
import os

nevents = runArgs.maxEvents*1.2

## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short()

# Read in arguments
decaytype = str(shortname.split('_')[3][4:])
Zdmass = float(shortname.split('_')[5])
Hmass = float(shortname.split('_')[4])
Zdlifetime_str = shortname.split('_')[-1]

Zdlifetime = float(Zdlifetime_str.replace("ns","").replace(".py","").replace("p","0."))
hbar = 6.582119514e-16
if Zdlifetime > 0: Zd_width = hbar/Zdlifetime
else: Zd_width = 'auto'
avgtau = Zdlifetime*300

decay_dict = {"4e": "e+ e-", "4mu": "mu+ mu-", "4l": "l+ l-"}
print("Check Physics short (final state, mass H, mass Zd, c*tau): ", decay_dict[decaytype], Hmass, Zdmass, avgtau)
print("Check Higgs width: ", Hwidth)


# MadGraph info
proc = f"""
import model HAHM_variableMW_v3_UFO
define l+ = e+ mu+
define l- = e- mu-
generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > Zp Zp, Zp > {decay_dict[decaytype]})
output -f"""

# New old pcard additions
pcard_params = { "HIDDEN": { 'epsilon': '1e-4', #kinetic mixing parameter
                                 'kap': '1e-4', #higgs mixing parameter
                                 'mzdinput': str(Zdmass), #Zd mass
                                 'mhsinput':'1000.0' }, #dark higgs mass
                     "HIGGS": { 'mhinput':str(Hmass)}, #higgs mass
                     "DECAY": { 'wzp':str(Zd_width), 'wh':str(Hwidth), 'wt':'Auto' } #auto-calculate decay widths and BR of Zp, H, t
                  }


# disable PMG settings
usePMGSettings = False


rcard_params = { "nevents" : nevents,
                   'cut_decays':'F',
                   'ptj':'0',
                   'ptb':'0',
                   'pta':'0',
                   'ptl':'0',
                   'etaj':'-1',
                   'etab':'-1',
                   'etaa':'-1',
                   'etal':'-1',
                   'drjj':'0',
                   'drbb':'0',
                   'drll':'0',
                   'draa':'0',
                   'drbj':'0',
                   'draj':'0',
                   'drjl':'0',
                   'drab':'0',
                   'drbl':'0',
                   'dral':'0',
		   'time_of_flight':'1E-25' }

process_dir = new_process(proc)

modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=rcard_params)
modify_param_card(process_dir=process_dir,params=pcard_params)
generate(process_dir=process_dir,runArgs=runArgs) 
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Helper for resetting process number
check_reset_proc_number(opts)



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]
#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
# Old new config
evgenConfig.description="high mass higgs to ZdZd"
evgenConfig.keywords+=['exotic','BSMHiggs','longlived']
evgenConfig.contact = ['eleanor.luise.woodward@cern.ch']
evgenConfig.process = "HAHM_H_ZdZd_4l"
evgenConfig.nEventsPerJob = 10000
