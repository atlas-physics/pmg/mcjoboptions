# MadGraph
import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
# Pythia integration
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# add information
evgenConfig.keywords += ['BSM','exotic','longLived']
evgenConfig.generators = ["aMcAtNlo"]
evgenConfig.contact = [ "Judith Höfer <judith.hoefer@cern.ch>" ]

# set nevents a bit higher in case some get lost
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define process, depending on production mode
if is_DrellYan:
    # Drell-Yan pair-production of detector-stable multi-charged particles
    evgenConfig.description = f'Drell-Yan pair-production of detector-stable multi-charged particles with charge {chargePoint} and mass {massPoint}'
    evgenConfig.keywords += ['drellYan']

    # define MadGraph process
    process = f"""
import model multichargedParticles
generate p p > qb{chargePoint}p0 qb{chargePoint}p0~
output -f
"""

    # note: for Drell-Yan the default PDF settings are used

else: 
    # Photon-Fusion pair-production of detector-stable multi-charged particles
    evgenConfig.description = f'Photon-Fusion pair-production of detector-stable multi-charged particles with charge {chargePoint} and mass {massPoint}'

    # define MadGraph process
    process = f"""
import model multichargedParticles
generate a a > qb{chargePoint}p0 qb{chargePoint}p0~
output -f
"""

    # PDF settings: use LUXqed PDF
    MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':324900, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets - NNPDF31_nlo_as_0118_luxqed
    'pdf_variations':[324900], # list of pdfs ids for which all variations (error sets) will be included as weights - NNPDF31_nlo_as_0118_luxqed
    'alternative_pdfs':[325100,304200,13100,25100,14400,14500,14600,61100,42960,303000,27100,331500,28500,15000,82350], # 325100: NNPDF31_nnlo_as_0118_luxqed (= NNLO of central), 304200: NNPDF31_nlo_as_0118_hessian, 13100: CT14nlo, 25100: MMHT2014nlo68cl, 14400: CT18NLO, 14500: CT18ZNLO, 14600: CT18ANLO, 61100: HERAPDF20_NLO_EIG, 42960: ABMP16_5_nlo, 303000: NNPDF30_nlo_as_0118_hessian, 27100: MSHT20nlo_as118, 331500: NNPDF40_nnlo_as_01180_hessian, 28500: MSHT20qed_nnlo, 15000: CT18qed_proton, 82350: LUXqed17_plus_PDF4LHC15_nnlo_30
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
    }


# define process
process_dir = new_process(process)

# fetch default NLO run_card.dat and set parameters
settings = {'cut_decays' :'F',
            'nevents'    :int(nevents)}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# announce BSM particle PdgId to TestHepMC
PdgId = 10000000+chargePoint*100
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write(f"""
-{PdgId}
{PdgId}
""")
pdgfile.close()

# change mass of MCP in the param_card
param_card_extras = { "MULTICHARGEDPARTICLES": { f"MQb{chargePoint}p0": massPoint}}
modify_param_card(process_dir=process_dir, params=param_card_extras)

# announce the MCP to Geant 4
if os.path.isfile('PDGTABLE.MeV'):
	os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
with open("PDGTABLE.MeV", 'a') as file:
    file.write(f"M {PdgId}                         {massPoint}.E+03       +0.0E+00 -0.0E+00 Qball           +\n")
    file.write(f"W {PdgId}                         0.E+00         +0.0E+00 -0.0E+00 Qball           +\n")

if os.path.isfile('particles.txt'):
	os.remove('particles.txt')
with open("particles.txt", 'a') as file:
    file.write(f"{PdgId}  {massPoint}.00  {chargePoint}  0.0 # Qball\n")
    file.write(f"-{PdgId}  {massPoint}.00  -{chargePoint}  0.0 # QballBar\n")

if os.path.isfile('G4particle_acceptlist.txt'):
	os.remove('G4particle_acceptlist.txt')
os.system('get_files -data G4particle_acceptlist.txt')
with open("G4particle_acceptlist.txt", 'a') as file:
    file.write(f"{PdgId}   qb{chargePoint}p0  {massPoint*1000} (Mev/c) lepton 0\n")
    file.write(f"{-PdgId}   qb{chargePoint}p0bar  {massPoint*1000} (Mev/c) lepton 0\n")

evgenConfig.specialConfig = f'MASS={massPoint};CHARGE={chargePoint};InteractingPDGCodes=[{PdgId},-{PdgId}];preInclude=Monopole.MonopoleConfig.QballPreInclude;MDT_QballConfig=True;postInclude=Monopole.MonopoleConfig.QballCfg'

print_cards()

# generate
generate(process_dir=process_dir, runArgs=runArgs)

# arrange
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3)  
