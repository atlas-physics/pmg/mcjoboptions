import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short


definitions="""import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/NTGC_all
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > vl vl~ a GP==0 GM==0 BTW==0 BW==0 BB==0 WW^2==1
"""

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define cuts and other run card settings
settings={'dynamical_scale_choice':'3', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'event_norm':'average',
        'lhe_version': '3.0',
        'cut_decays'  : 'F',
        'pta':"0",
        'etaa':'3',
        'ptgmin'      : "140",
        'epsgamma':"0.1",
        'R0gamma' :"0.1", 
        'xn'    :"2",
        'isoEM' :"True", 
        'bwcutoff'    :"15",
        'maxjetflavor': 5,
        'asrwgtflavor': "5",
        'ickkw': "0",
        'xqcut': "0",
        'ktdurham'       : -1
}
#############################################################################################

process=definitions+'output -f'

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
param_dict = {
    'DIM8':{'cg' : '0.', 'cgm' : '0.', 'cbtwl4' : '0.', 'cbwl4' : '0.', 'cbbl4' : '0.', 'cwwl4' : '2.'},
}
modify_param_card(process_dir=process_dir,params=param_dict)

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#############################################################################################
# add meta data
evgenConfig.description = 'MadGraph nunugamma EFT nTGC with cww=2 interference term'
evgenConfig.keywords+=['SM','Z','photon','diboson']
evgenConfig.contact = ['Artur Semushin <Artur.Semushin@cern.ch>']
#############################################################################################

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



