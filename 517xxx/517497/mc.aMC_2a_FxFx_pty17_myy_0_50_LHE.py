#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "MadGraph LHE-only samples for diphoton+0-2j at NLO, myy=0-50 GeV"
evgenConfig.keywords = ["SM","diphoton","NLO"]
evgenConfig.contact = ["yanwen.hong@cern.ch","ana.cueto@cern.ch"]
evgenConfig.generators = ["aMcAtNlo"]



# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
evgenConfig.nEventsPerJob = 220000
nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob

gridpack_mode=True


if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > a a [QCD] @0
    add process p p > a a j [QCD] @1
    add process p p > a a j j [QCD] @2
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION



settings = {'parton_shower':'PYTHIA8', 
            'req_acc':0.0001,
            'ickkw':3,
            'ptgmin':17,
            'etagamma':2.7,
            'R0gamma':0.1,
            'xn':2.0,
            'epsgamma':0.1,
            'isoEM':True,
            'nevents':int(nevents)}

scaleToCopy      = 'pty_17_myy_0_50_cuts.f'
scaleDestination = process_dir+'/SubProcesses/cuts.f'
scalefile        = subprocess.Popen(['get_files','-data',scaleToCopy])
scalefile.wait()

if not os.access(scaleToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(scaleToCopy))
shutil.copy(scaleToCopy,scaleDestination)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

