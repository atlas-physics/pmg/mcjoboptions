# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  16:27
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.45806072E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.13264106E+03  # scale for input parameters
    1    6.50333120E+01  # M_1
    2    4.88187180E+02  # M_2
    3    4.64725597E+03  # M_3
   11    6.00214713E+03  # A_t
   12    1.52003764E+03  # A_b
   13    1.34920514E+03  # A_tau
   23    2.76895619E+02  # mu
   25    1.38929317E+01  # tan(beta)
   26    4.80845201E+03  # m_A, pole mass
   31    1.04801229E+03  # M_L11
   32    1.04801229E+03  # M_L22
   33    1.01490542E+03  # M_L33
   34    1.12076821E+03  # M_E11
   35    1.12076821E+03  # M_E22
   36    1.44643706E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.05151153E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.21049299E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.77810174E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.13264106E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.13264106E+03  # (SUSY scale)
  1  1     8.40606372E-06   # Y_u(Q)^DRbar
  2  2     4.27028037E-03   # Y_c(Q)^DRbar
  3  3     1.01552002E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.13264106E+03  # (SUSY scale)
  1  1     2.34675000E-04   # Y_d(Q)^DRbar
  2  2     4.45882499E-03   # Y_s(Q)^DRbar
  3  3     2.32724063E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.13264106E+03  # (SUSY scale)
  1  1     4.09525035E-05   # Y_e(Q)^DRbar
  2  2     8.46767889E-03   # Y_mu(Q)^DRbar
  3  3     1.42412429E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.13264106E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.00214714E+03   # A_t(Q)^DRbar
Block Ad Q=  3.13264106E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.52003765E+03   # A_b(Q)^DRbar
Block Ae Q=  3.13264106E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.34920515E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.13264106E+03  # soft SUSY breaking masses at Q
   1    6.50333120E+01  # M_1
   2    4.88187180E+02  # M_2
   3    4.64725597E+03  # M_3
  21    2.28875243E+07  # M^2_(H,d)
  22    2.85433896E+05  # M^2_(H,u)
  31    1.04801229E+03  # M_(L,11)
  32    1.04801229E+03  # M_(L,22)
  33    1.01490542E+03  # M_(L,33)
  34    1.12076821E+03  # M_(E,11)
  35    1.12076821E+03  # M_(E,22)
  36    1.44643706E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.05151153E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.21049299E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.77810174E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28409393E+02  # h0
        35     4.80784977E+03  # H0
        36     4.80845201E+03  # A0
        37     4.80955018E+03  # H+
   1000001     1.00808422E+04  # ~d_L
   2000001     1.00565893E+04  # ~d_R
   1000002     1.00805974E+04  # ~u_L
   2000002     1.00597033E+04  # ~u_R
   1000003     1.00808436E+04  # ~s_L
   2000003     1.00565908E+04  # ~s_R
   1000004     1.00805989E+04  # ~c_L
   2000004     1.00597047E+04  # ~c_R
   1000005     3.10516540E+03  # ~b_1
   2000005     4.87594255E+03  # ~b_2
   1000006     3.00387506E+03  # ~t_1
   2000006     3.26692683E+03  # ~t_2
   1000011     1.05244252E+03  # ~e_L-
   2000011     1.12496056E+03  # ~e_R-
   1000012     1.04909628E+03  # ~nu_eL
   1000013     1.05243962E+03  # ~mu_L-
   2000013     1.12495653E+03  # ~mu_R-
   1000014     1.04909399E+03  # ~nu_muL
   1000015     1.01867877E+03  # ~tau_1-
   2000015     1.44897909E+03  # ~tau_2-
   1000016     1.01526108E+03  # ~nu_tauL
   1000021     5.07010615E+03  # ~g
   1000022     6.18039747E+01  # ~chi_10
   1000023     2.79449551E+02  # ~chi_20
   1000025     2.94426466E+02  # ~chi_30
   1000035     5.40698246E+02  # ~chi_40
   1000024     2.77878269E+02  # ~chi_1+
   1000037     5.40966263E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -6.85498884E-02   # alpha
Block Hmix Q=  3.13264106E+03  # Higgs mixing parameters
   1    2.76895619E+02  # mu
   2    1.38929317E+01  # tan[beta](Q)
   3    2.43162755E+02  # v(Q)
   4    2.31212107E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -7.99942886E-01   # Re[R_st(1,1)]
   1  2     6.00076144E-01   # Re[R_st(1,2)]
   2  1    -6.00076144E-01   # Re[R_st(2,1)]
   2  2    -7.99942886E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999951E-01   # Re[R_sb(1,1)]
   1  2     3.14466453E-04   # Re[R_sb(1,2)]
   2  1    -3.14466453E-04   # Re[R_sb(2,1)]
   2  2     9.99999951E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.99990295E-01   # Re[R_sta(1,1)]
   1  2     4.40571143E-03   # Re[R_sta(1,2)]
   2  1    -4.40571143E-03   # Re[R_sta(2,1)]
   2  2     9.99990295E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.85527128E-01   # Re[N(1,1)]
   1  2     9.65719395E-03   # Re[N(1,2)]
   1  3    -1.62939990E-01   # Re[N(1,3)]
   1  4     4.57556439E-02   # Re[N(1,4)]
   2  1    -1.47306146E-01   # Re[N(2,1)]
   2  2    -2.35760959E-01   # Re[N(2,2)]
   2  3     6.89054731E-01   # Re[N(2,3)]
   2  4    -6.69269189E-01   # Re[N(2,4)]
   3  1    -8.13322910E-02   # Re[N(3,1)]
   3  2     6.50189608E-02   # Re[N(3,2)]
   3  3     6.95441684E-01   # Re[N(3,3)]
   3  4     7.10998212E-01   # Re[N(3,4)]
   4  1    -2.05484411E-02   # Re[N(4,1)]
   4  2     9.69585501E-01   # Re[N(4,2)]
   4  3     1.22535714E-01   # Re[N(4,3)]
   4  4    -2.10871328E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.72710355E-01   # Re[U(1,1)]
   1  2     9.84972656E-01   # Re[U(1,2)]
   2  1     9.84972656E-01   # Re[U(2,1)]
   2  2     1.72710355E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -2.98165271E-01   # Re[V(1,1)]
   1  2     9.54514259E-01   # Re[V(1,2)]
   2  1     9.54514259E-01   # Re[V(2,1)]
   2  2     2.98165271E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     9.03805343E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.35964167E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.37863882E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.02594226E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.55169975E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.64622947E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.38414581E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     5.59883233E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.74666178E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.92869417E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.79479342E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.52081425E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000013     9.03928390E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.35949673E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.38481977E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.70821464E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.55135035E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.64585977E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.38337675E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.60161452E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.74186039E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.93971704E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.91219167E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.54503681E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.45045621E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.75890337E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.37101763E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.24054431E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.91793695E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.43006569E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.64451242E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.11861732E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     8.32583089E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.47897808E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.83145341E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.66862955E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.24786438E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.27499671E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.93684485E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     6.03067018E-04    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.90732761E-04    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.72886352E-04    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     9.10136855E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.44829962E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.08032918E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.06594525E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.63081951E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     7.79963105E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.98222539E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     9.10260482E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.44809972E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.08017938E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.06524238E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.63044816E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.81230175E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.98155159E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     8.81991944E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.44563243E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.06771092E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.00055328E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.50000093E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.15542642E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.74216359E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     4.47169202E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.21939722E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.72038428E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.87445769E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.77847314E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.63219587E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.36597759E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.84519220E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.23150215E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     4.57397092E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.48135907E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.68492408E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.47177278E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.21939892E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.76238717E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.87428282E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.77855098E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.63234344E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.36919445E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.87835056E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.23141607E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     4.57966463E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.48134495E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.68482307E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.05316155E+02   # ~b_1
#    BR                NDA      ID1      ID2
     5.29148817E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.04456187E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.58940365E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.16033440E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.45206470E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.89783311E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     7.34563598E-03    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.31458951E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.11621401E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.92923374E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.93281075E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.99936026E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.84119894E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.17190916E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.47699544E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.64340767E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.69866752E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.04819370E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.19486127E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.51625335E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.77843066E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.11164470E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.26245973E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.95765923E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.12303452E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.36321376E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.39113526E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.68454120E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.64348169E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.69859472E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.05169295E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.23446689E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.51610411E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.77850830E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.11162982E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.26521810E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.98956705E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.12296801E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.36386211E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.39111883E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.68444011E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.10228884E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.82909720E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.26589227E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.59821419E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.39354977E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.13297599E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.41911616E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     7.19494578E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.43058239E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.85425672E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.28351965E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.45388996E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.10426400E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.66656514E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.53001115E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.51250400E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.81957683E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.16649874E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.65253300E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99943108E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     4.18818380E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.66465123E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.61598181E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.98566627E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.42082114E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.94731673E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.10709844E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.23218896E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.32909002E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.51714450E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.48280560E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.55246991E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.86749590E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.13164314E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.84938345E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.23214092E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.23214092E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.15401188E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.60515175E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.48177410E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.02479685E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.54281981E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.32467662E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.89027271E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.89027271E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.74469438E+02   # ~g
#    BR                NDA      ID1      ID2
     2.33781390E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.33781390E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.99029321E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.99029321E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.30255207E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.30255207E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.67955103E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.67955103E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.42681804E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     2.42681804E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.97756826E-03   # Gamma(h0)
     2.57687197E-03   2        22        22   # BR(h0 -> photon photon)
     1.96371960E-03   2        22        23   # BR(h0 -> photon Z)
     4.07872353E-02   2        23        23   # BR(h0 -> Z Z)
     3.16014682E-01   2       -24        24   # BR(h0 -> W W)
     7.65125042E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.32964640E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.92591986E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.55324780E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.22611514E-07   2        -2         2   # BR(h0 -> Up up)
     2.37988102E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.98421296E-07   2        -1         1   # BR(h0 -> Down down)
     1.80267260E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.78957173E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.48304163E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     7.41365613E+01   # Gamma(HH)
     5.90560179E-08   2        22        22   # BR(HH -> photon photon)
     1.00122829E-07   2        22        23   # BR(HH -> photon Z)
     9.90276250E-07   2        23        23   # BR(HH -> Z Z)
     1.70613214E-07   2       -24        24   # BR(HH -> W W)
     3.75468828E-07   2        21        21   # BR(HH -> gluon gluon)
     1.84205861E-09   2       -11        11   # BR(HH -> Electron electron)
     8.20298537E-05   2       -13        13   # BR(HH -> Muon muon)
     2.37208914E-02   2       -15        15   # BR(HH -> Tau tau)
     6.25613535E-13   2        -2         2   # BR(HH -> Up up)
     1.21376376E-07   2        -4         4   # BR(HH -> Charm charm)
     8.68702754E-03   2        -6         6   # BR(HH -> Top top)
     1.26469080E-07   2        -1         1   # BR(HH -> Down down)
     4.57436616E-05   2        -3         3   # BR(HH -> Strange strange)
     1.14360439E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.81850588E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.28305291E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.28305291E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.24104945E-02   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     4.09751238E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.66744897E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.82921787E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.35381112E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     6.51304777E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.64024295E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     9.29981342E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.64620897E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.41870782E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.66391198E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     7.32883749E-06   2        25        25   # BR(HH -> h0 h0)
     3.16741189E-07   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.62666076E-14   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.62666076E-14   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.01466605E-07   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     3.16253100E-07   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     6.95427837E-10   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     6.95427837E-10   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     2.01334634E-07   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     3.84140478E-08   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     9.54898523E-04   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     9.54898523E-04   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     3.81413426E-07   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     7.35876420E+01   # Gamma(A0)
     6.80305161E-08   2        22        22   # BR(A0 -> photon photon)
     9.40765274E-08   2        22        23   # BR(A0 -> photon Z)
     5.72950730E-06   2        21        21   # BR(A0 -> gluon gluon)
     1.76317017E-09   2       -11        11   # BR(A0 -> Electron electron)
     7.85169038E-05   2       -13        13   # BR(A0 -> Muon muon)
     2.27050604E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.82228311E-13   2        -2         2   # BR(A0 -> Up up)
     1.12956611E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.13015895E-03   2        -6         6   # BR(A0 -> Top top)
     1.21050142E-07   2        -1         1   # BR(A0 -> Down down)
     4.37836414E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.09459712E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.74538844E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.21214319E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.21214319E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.12592332E-02   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     4.47323394E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.76652964E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.84662726E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.89658125E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.66204193E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.35523938E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.24195417E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.81068507E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.06902342E-01   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.12009176E-02   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.62433458E-06   2        23        25   # BR(A0 -> Z h0)
     1.64964368E-36   2        25        25   # BR(A0 -> h0 h0)
     1.59851592E-14   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.59851592E-14   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     6.83415481E-10   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     6.83415481E-10   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     9.61973775E-04   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     9.61973775E-04   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     7.62644534E+01   # Gamma(Hp)
     2.21714545E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     9.47898928E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.68119833E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.33507858E-07   2        -1         2   # BR(Hp -> Down up)
     2.26519402E-06   2        -3         2   # BR(Hp -> Strange up)
     1.40485439E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.29007343E-08   2        -1         4   # BR(Hp -> Down charm)
     4.82541459E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.96730141E-04   2        -5         4   # BR(Hp -> Bottom charm)
     7.28703535E-07   2        -1         6   # BR(Hp -> Down top)
     1.59600501E-05   2        -3         6   # BR(Hp -> Strange top)
     1.43626026E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.80003399E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.47444543E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.41273114E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.60251957E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.15036412E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.30912273E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.36775947E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.46489294E-06   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     2.53457263E-06   2        24        25   # BR(Hp -> W h0)
     2.53811805E-13   2        24        35   # BR(Hp -> W HH)
     2.85174423E-14   2        24        36   # BR(Hp -> W A0)
     1.20957218E-06   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     3.61045401E-14   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.20889962E-06   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     1.54352566E-09   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     7.16775249E-07   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     1.85723757E-03   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.10259458E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.93103292E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.93013551E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00046494E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.71603912E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.18098337E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.10259458E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.93103292E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.93013551E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99989075E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.09245541E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99989075E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.09245541E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26522758E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.96786671E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.00899503E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.09245541E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99989075E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25962582E-04   # BR(b -> s gamma)
    2    1.58921340E-06   # BR(b -> s mu+ mu-)
    3    3.52459751E-05   # BR(b -> s nu nu)
    4    2.49068749E-15   # BR(Bd -> e+ e-)
    5    1.06399375E-10   # BR(Bd -> mu+ mu-)
    6    2.22756181E-08   # BR(Bd -> tau+ tau-)
    7    8.37733463E-14   # BR(Bs -> e+ e-)
    8    3.57879583E-09   # BR(Bs -> mu+ mu-)
    9    7.59162409E-07   # BR(Bs -> tau+ tau-)
   10    9.67517796E-05   # BR(B_u -> tau nu)
   11    9.99408842E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42062648E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93680679E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15762985E-03   # epsilon_K
   17    2.28166127E-15   # Delta(M_K)
   18    2.47953214E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28935560E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.05100880E-15   # Delta(g-2)_electron/2
   21    1.30445190E-10   # Delta(g-2)_muon/2
   22    3.76118877E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.80259394E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.95285095E-01   # C7
     0305 4322   00   2    -7.81964033E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.01007840E-01   # C8
     0305 6321   00   2    -8.96943039E-05   # C8'
 03051111 4133   00   0     1.61773692E+00   # C9 e+e-
 03051111 4133   00   2     1.61794489E+00   # C9 e+e-
 03051111 4233   00   2     5.09320287E-05   # C9' e+e-
 03051111 4137   00   0    -4.44042902E+00   # C10 e+e-
 03051111 4137   00   2    -4.43805313E+00   # C10 e+e-
 03051111 4237   00   2    -3.81767837E-04   # C10' e+e-
 03051313 4133   00   0     1.61773692E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61794467E+00   # C9 mu+mu-
 03051313 4233   00   2     5.09320177E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44042902E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43805335E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.81767865E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50485038E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     8.27145663E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50485038E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     8.27145665E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50485042E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     8.27146048E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85814950E-07   # C7
     0305 4422   00   2     3.76241984E-06   # C7
     0305 4322   00   2    -3.18747104E-07   # C7'
     0305 6421   00   0     3.30476020E-07   # C8
     0305 6421   00   2    -7.02439748E-07   # C8
     0305 6321   00   2    -1.70322197E-07   # C8'
 03051111 4133   00   2    -4.19679689E-07   # C9 e+e-
 03051111 4233   00   2     9.53699384E-07   # C9' e+e-
 03051111 4137   00   2     2.49437377E-06   # C10 e+e-
 03051111 4237   00   2    -7.14882675E-06   # C10' e+e-
 03051313 4133   00   2    -4.19694530E-07   # C9 mu+mu-
 03051313 4233   00   2     9.53699207E-07   # C9' mu+mu-
 03051313 4137   00   2     2.49438986E-06   # C10 mu+mu-
 03051313 4237   00   2    -7.14882727E-06   # C10' mu+mu-
 03051212 4137   00   2    -2.69920529E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.54888270E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.69919974E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.54888270E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.69183297E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.54888234E-06   # C11' nu_3 nu_3
