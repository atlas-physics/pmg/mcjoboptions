# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  11:20
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.10584873E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.46559390E+03  # scale for input parameters
    1   -3.32124143E+02  # M_1
    2   -1.93505824E+03  # M_2
    3    1.66514789E+03  # M_3
   11   -4.30772578E+03  # A_t
   12   -1.65171121E+03  # A_b
   13    1.81981548E+03  # A_tau
   23    1.34979360E+03  # mu
   25    1.05333024E+01  # tan(beta)
   26    6.79267943E+02  # m_A, pole mass
   31    6.87196436E+02  # M_L11
   32    6.87196436E+02  # M_L22
   33    1.23594217E+03  # M_L33
   34    5.64491814E+02  # M_E11
   35    5.64491814E+02  # M_E22
   36    1.42173456E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.71474300E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.38318943E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.59042144E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.46559390E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.46559390E+03  # (SUSY scale)
  1  1     8.42207168E-06   # Y_u(Q)^DRbar
  2  2     4.27841242E-03   # Y_c(Q)^DRbar
  3  3     1.01745391E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.46559390E+03  # (SUSY scale)
  1  1     1.78264035E-04   # Y_d(Q)^DRbar
  2  2     3.38701667E-03   # Y_s(Q)^DRbar
  3  3     1.76782063E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.46559390E+03  # (SUSY scale)
  1  1     3.11083777E-05   # Y_e(Q)^DRbar
  2  2     6.43222589E-03   # Y_mu(Q)^DRbar
  3  3     1.08179458E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.46559390E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.30772943E+03   # A_t(Q)^DRbar
Block Ad Q=  3.46559390E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.65170993E+03   # A_b(Q)^DRbar
Block Ae Q=  3.46559390E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.81981494E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.46559390E+03  # soft SUSY breaking masses at Q
   1   -3.32124143E+02  # M_1
   2   -1.93505824E+03  # M_2
   3    1.66514789E+03  # M_3
  21   -1.44839463E+06  # M^2_(H,d)
  22   -1.66114870E+06  # M^2_(H,u)
  31    6.87196436E+02  # M_(L,11)
  32    6.87196436E+02  # M_(L,22)
  33    1.23594217E+03  # M_(L,33)
  34    5.64491814E+02  # M_(E,11)
  35    5.64491814E+02  # M_(E,22)
  36    1.42173456E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.71474300E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.38318943E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.59042144E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23370045E+02  # h0
        35     6.79508238E+02  # H0
        36     6.79267943E+02  # A0
        37     6.86407017E+02  # H+
   1000001     1.01176651E+04  # ~d_L
   2000001     1.00941887E+04  # ~d_R
   1000002     1.01173153E+04  # ~u_L
   2000002     1.00978160E+04  # ~u_R
   1000003     1.01176659E+04  # ~s_L
   2000003     1.00941893E+04  # ~s_R
   1000004     1.01173162E+04  # ~c_L
   2000004     1.00978170E+04  # ~c_R
   1000005     2.65334342E+03  # ~b_1
   2000005     2.76320628E+03  # ~b_2
   1000006     2.75928865E+03  # ~t_1
   2000006     4.35269470E+03  # ~t_2
   1000011     7.39051867E+02  # ~e_L-
   2000011     5.68001510E+02  # ~e_R-
   1000012     7.34480836E+02  # ~nu_eL
   1000013     7.39058158E+02  # ~mu_L-
   2000013     5.67991807E+02  # ~mu_R-
   1000014     7.34479890E+02  # ~nu_muL
   1000015     1.26874626E+03  # ~tau_1-
   2000015     1.42316396E+03  # ~tau_2-
   1000016     1.26636809E+03  # ~nu_tauL
   1000021     2.03688532E+03  # ~g
   1000022     3.28248628E+02  # ~chi_10
   1000023     1.36404248E+03  # ~chi_20
   1000025     1.36899444E+03  # ~chi_30
   1000035     1.99203929E+03  # ~chi_40
   1000024     1.36435654E+03  # ~chi_1+
   1000037     1.99207736E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -9.44068429E-02   # alpha
Block Hmix Q=  3.46559390E+03  # Higgs mixing parameters
   1    1.34979360E+03  # mu
   2    1.05333024E+01  # tan[beta](Q)
   3    2.43082657E+02  # v(Q)
   4    4.61404938E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.98502343E-01   # Re[R_st(1,1)]
   1  2     5.47089675E-02   # Re[R_st(1,2)]
   2  1    -5.47089675E-02   # Re[R_st(2,1)]
   2  2     9.98502343E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     5.84478240E-02   # Re[R_sb(1,1)]
   1  2     9.98290465E-01   # Re[R_sb(1,2)]
   2  1    -9.98290465E-01   # Re[R_sb(2,1)]
   2  2     5.84478240E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.98623575E-01   # Re[R_sta(1,1)]
   1  2     5.24495563E-02   # Re[R_sta(1,2)]
   2  1    -5.24495563E-02   # Re[R_sta(2,1)]
   2  2     9.98623575E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99433888E-01   # Re[N(1,1)]
   1  2     7.19359421E-05   # Re[N(1,2)]
   1  3    -3.32743428E-02   # Re[N(1,3)]
   1  4    -4.97164654E-03   # Re[N(1,4)]
   2  1     2.69967431E-02   # Re[N(2,1)]
   2  2     8.00237317E-02   # Re[N(2,2)]
   2  3    -7.05581537E-01   # Re[N(2,3)]
   2  4    -7.03578050E-01   # Re[N(2,4)]
   3  1     2.00021544E-02   # Re[N(3,1)]
   3  2    -1.81980119E-02   # Re[N(3,2)]
   3  3    -7.06493371E-01   # Re[N(3,3)]
   3  4     7.07202845E-01   # Re[N(3,4)]
   4  1    -1.73032214E-03   # Re[N(4,1)]
   4  2     9.96626826E-01   # Re[N(4,2)]
   4  3     4.37564847E-02   # Re[N(4,3)]
   4  4     6.94071068E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.22102038E-02   # Re[U(1,1)]
   1  2     9.98063069E-01   # Re[U(1,2)]
   2  1    -9.98063069E-01   # Re[U(2,1)]
   2  2    -6.22102038E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.85147047E-02   # Re[V(1,1)]
   1  2     9.95135595E-01   # Re[V(1,2)]
   2  1     9.95135595E-01   # Re[V(2,1)]
   2  2    -9.85147047E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     1.26459165E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     5.97381822E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999984E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     1.26452640E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     5.97676334E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99525025E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.74969932E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.39964178E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     6.44832903E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.90561363E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.69789058E-04    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.49766452E-04    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.21043877E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.77065603E-03    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.89229357E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.13508773E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     5.90367455E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     5.90365936E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     1.38305581E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.99951649E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.38826209E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.60291740E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92388746E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.60073239E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.63791475E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.82387283E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.75489784E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.86332535E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.53130078E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.54807749E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.38830687E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.60287518E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92382799E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.60079088E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.63790669E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.83670035E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.75486644E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.90416289E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.53124098E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.54802014E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.86385604E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.73088063E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.18543026E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.13358549E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.00815010E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.43328793E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.19209744E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     9.14848132E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     8.69836824E+01   # ~b_2
#    BR                NDA      ID1      ID2
     4.26774196E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.90785945E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.72462329E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.20206938E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.62653197E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.69189903E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.22483829E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.56012658E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.97310456E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.70236420E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.60061420E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.63523442E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.59183072E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.74889581E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     9.68797901E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.47521897E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.54784948E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.56019800E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.97307667E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.70227327E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.60067246E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.63522352E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.61226906E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.74886550E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     9.71362846E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.47515666E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.54779211E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.56002130E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.47424725E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.84776951E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.84306035E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.04964718E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.14006312E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.53001311E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.18140252E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.03795272E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.47487861E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.13542288E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.09099841E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     8.14939616E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.45236179E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.61990228E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.93379098E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.63465332E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.17018868E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     7.92593502E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     4.90625368E-03    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     3.98294114E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.49790109E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     6.07035589E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.47468403E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.32513669E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     8.75613922E-03    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.88462435E-04    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     8.75601541E-03    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     4.13597362E-04    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     2.21834128E-02    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.23954000E-02    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     3.08031755E-03    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     6.15936921E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     2.99865719E-01    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
     1.82875210E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     4.29533739E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.51219418E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.51218597E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     7.15604747E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.90854661E-04    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     1.50925978E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.50926178E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     7.17907656E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     2.32234200E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     6.36494970E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     6.11290460E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     6.13548759E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.25745322E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.99792056E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.38511395E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.50780180E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.26184657E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.38907031E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     1.22855705E-03    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     1.22855705E-03    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     9.62047532E-03    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     9.62047532E-03    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.36589132E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     1.36589132E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     9.72117567E-03    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     9.72117567E-03    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.45311048E-03    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     1.45311048E-03    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     4.68918205E-03    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     4.68918205E-03    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     4.68919192E-03    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     4.68919192E-03    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     1.77337864E-04    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     1.77337864E-04    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     2.08955630E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.40245296E-02    2     1000022        36   # BR(chi^0_2 -> chi^0_1 A^0)
     3.53954160E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     3.09477311E-01    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.65357684E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     1.69364618E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     5.56823013E-04    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     5.56823013E-04    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     6.70531738E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     6.70531738E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.33737541E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.33737541E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     9.19672973E-04    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     9.19672973E-04    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     7.52669022E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     7.52669022E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     7.52670589E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     7.52670589E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     3.17349343E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.50204273E-02    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     4.30023632E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.72621304E-01    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.21441301E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     4.70034767E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     6.87661089E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     6.87661089E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     6.87657498E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     6.87657498E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     3.25417385E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     3.25417385E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     6.92936839E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     6.92936839E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     6.92937400E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     6.92937400E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     3.29541724E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     3.29541724E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     5.95048801E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     5.95048801E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.84776158E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.54486475E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.32212401E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     5.42824057E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     8.06098730E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.85031988E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.23959889E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.71834033E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.71834033E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.57284860E-03   # ~g
#    BR                NDA      ID1      ID2
     1.97958499E-03    2     1000022        21   # BR(~g -> chi^0_1 g)
     2.06585810E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.89827037E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.59501343E-03    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.59501242E-03    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.06745850E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     7.63427272E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     7.63427736E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     2.76067914E-01    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.12734337E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.58051097E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.04335286E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.49948076E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.60484168E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.60484168E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.42823798E-03   # Gamma(h0)
     2.50525913E-03   2        22        22   # BR(h0 -> photon photon)
     1.45941625E-03   2        22        23   # BR(h0 -> photon Z)
     2.54481200E-02   2        23        23   # BR(h0 -> Z Z)
     2.17208562E-01   2       -24        24   # BR(h0 -> W W)
     7.86407446E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.19914279E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.31266244E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.67032674E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.35660922E-07   2        -2         2   # BR(h0 -> Up up)
     2.63290356E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.04591523E-07   2        -1         1   # BR(h0 -> Down down)
     2.18667729E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.81254916E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.21131328E+00   # Gamma(HH)
     7.36027280E-07   2        22        22   # BR(HH -> photon photon)
     1.59074414E-07   2        22        23   # BR(HH -> photon Z)
     7.84443160E-04   2        23        23   # BR(HH -> Z Z)
     1.47584120E-03   2       -24        24   # BR(HH -> W W)
     2.70654487E-04   2        21        21   # BR(HH -> gluon gluon)
     9.01083719E-09   2       -11        11   # BR(HH -> Electron electron)
     4.01022470E-04   2       -13        13   # BR(HH -> Muon muon)
     1.16780084E-01   2       -15        15   # BR(HH -> Tau tau)
     1.28345606E-11   2        -2         2   # BR(HH -> Up up)
     2.48863310E-06   2        -4         4   # BR(HH -> Charm charm)
     1.18730967E-01   2        -6         6   # BR(HH -> Top top)
     8.42552866E-07   2        -1         1   # BR(HH -> Down down)
     3.04758946E-04   2        -3         3   # BR(HH -> Strange strange)
     7.55320836E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.39971945E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.91316077E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.22315373E+00   # Gamma(A0)
     1.53862212E-06   2        22        22   # BR(A0 -> photon photon)
     4.51319298E-07   2        22        23   # BR(A0 -> photon Z)
     6.37383993E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.80927567E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.92052048E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.14169648E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.14722985E-11   2        -2         2   # BR(A0 -> Up up)
     2.22344578E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.44393425E-01   2        -6         6   # BR(A0 -> Top top)
     8.23714075E-07   2        -1         1   # BR(A0 -> Down down)
     2.97944811E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.38538912E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.88544590E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.27704360E-03   2        23        25   # BR(A0 -> Z h0)
     1.63143377E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.31175842E+00   # Gamma(Hp)
     1.05822975E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.52426250E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.27970251E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.07587414E-07   2        -1         2   # BR(Hp -> Down up)
     1.33721918E-05   2        -3         2   # BR(Hp -> Strange up)
     8.12248017E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.56629852E-07   2        -1         4   # BR(Hp -> Down charm)
     2.93656676E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.13743432E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.15518570E-05   2        -1         6   # BR(Hp -> Down top)
     2.52240407E-04   2        -3         6   # BR(Hp -> Strange top)
     8.68610427E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.24950846E-03   2        24        25   # BR(Hp -> W h0)
     1.60489463E-08   2        24        35   # BR(Hp -> W HH)
     1.90415841E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.94814730E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.10955645E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.10950459E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004674E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.96629662E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    9.01303163E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.94814730E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.10955645E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.10950459E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999939E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.07395635E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999939E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.07395635E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26254263E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.02084558E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.17891737E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.07395635E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999939E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.77031429E-04   # BR(b -> s gamma)
    2    1.58834411E-06   # BR(b -> s mu+ mu-)
    3    3.52770627E-05   # BR(b -> s nu nu)
    4    2.56092074E-15   # BR(Bd -> e+ e-)
    5    1.09399618E-10   # BR(Bd -> mu+ mu-)
    6    2.29011433E-08   # BR(Bd -> tau+ tau-)
    7    8.60448790E-14   # BR(Bs -> e+ e-)
    8    3.67583424E-09   # BR(Bs -> mu+ mu-)
    9    7.79659709E-07   # BR(Bs -> tau+ tau-)
   10    9.52995440E-05   # BR(B_u -> tau nu)
   11    9.84407805E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42683067E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93841152E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16021908E-03   # epsilon_K
   17    2.28169023E-15   # Delta(M_K)
   18    2.48209510E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29535343E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.90831060E-15   # Delta(g-2)_electron/2
   21   -1.67094369E-10   # Delta(g-2)_muon/2
   22   -1.37608878E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.49354697E-06   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.44067610E-01   # C7
     0305 4322   00   2    -1.16207422E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.61104966E-01   # C8
     0305 6321   00   2    -1.33327864E-03   # C8'
 03051111 4133   00   0     1.62122686E+00   # C9 e+e-
 03051111 4133   00   2     1.62162072E+00   # C9 e+e-
 03051111 4233   00   2     1.45468610E-05   # C9' e+e-
 03051111 4137   00   0    -4.44391896E+00   # C10 e+e-
 03051111 4137   00   2    -4.44463663E+00   # C10 e+e-
 03051111 4237   00   2    -1.07269747E-04   # C10' e+e-
 03051313 4133   00   0     1.62122686E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62162059E+00   # C9 mu+mu-
 03051313 4233   00   2     1.45465657E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44391896E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44463676E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.07269471E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50551135E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.32086852E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50551135E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.32087480E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50551186E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.32270048E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85834653E-07   # C7
     0305 4422   00   2     8.70709527E-07   # C7
     0305 4322   00   2     3.62448165E-07   # C7'
     0305 6421   00   0     3.30492897E-07   # C8
     0305 6421   00   2    -1.94203910E-05   # C8
     0305 6321   00   2    -1.91751770E-07   # C8'
 03051111 4133   00   2    -3.32973853E-07   # C9 e+e-
 03051111 4233   00   2     7.38995462E-07   # C9' e+e-
 03051111 4137   00   2     3.63613778E-06   # C10 e+e-
 03051111 4237   00   2    -5.49224116E-06   # C10' e+e-
 03051313 4133   00   2    -3.32972857E-07   # C9 mu+mu-
 03051313 4233   00   2     7.38995370E-07   # C9' mu+mu-
 03051313 4137   00   2     3.63613746E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.49224142E-06   # C10' mu+mu-
 03051212 4137   00   2    -6.80907685E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.18881093E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.80907669E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.18881093E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.71763814E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.18882071E-06   # C11' nu_3 nu_3
