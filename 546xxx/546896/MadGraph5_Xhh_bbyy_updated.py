#---------------------------------------------------------------------------------------------------
# Master joboption for X->HH->bbyy with MadGraph
# EVGEN Configuration
# joboption fragment adapted from MC15.450517 and MC15.450529
#---------------------------------------------------------------------------------------------------

from PyJobTransforms.trfLogger import msg
msg.info("Entering joboption")

if "mHH" not in globals():
    msg.error("Need to define parameter mHH for this joboption (mass in GeV of the HH resonance)")
    raise RuntimeError("Could not find parameter mHH")
else:
    msg.info("X->HH->bbyy production with resonance mass={} GeV.".format(mHH))
evgenConfig.description = "Di-Higgs production through {} GeV Heavy Higgs resonance which decays to bbyy. Higgs decays in the parton shower generator.".format(mHH)

evgenConfig.generators = ["MadGraph"]
if "PS" not in globals():
    msg.info("Parameter PS not defined for this joboption (parton shower). Will run without a parton shower (lhe-only production).")
elif PS == "Py8EG":
    msg.info("Parton shower and hadronisation: Pythia8+EvtGen.")
    evgenConfig.generators += [ "Pythia8", "EvtGen" ]
elif PS == "H7EG":
    msg.info("Parton shower and hadronisation: Herwig7+EvtGen.")
    evgenConfig.generators += [ "Herwig7", "EvtGen" ]
msg.debug("Possible values for PS are Py8EG (Pythia8+EvtGen) and H7EG (Herwig7+EvtGen).")

evgenConfig.keywords = ["hh","BSM", "BSMHiggs", "resonance", "diphoton", "bottom"]
evgenConfig.contact = ["tpelzer@cern.ch"]

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[247000], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}
from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------------------------------
# Filter efficiency is low
# Thus, setting the number of generated events to 100 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=3*1.1
nevents=runArgs.maxEvents*safefactor if runArgs.maxEvents > 0 else 10000*safefactor

#---------------------------------------------------------------------------------------------------
# Setting mHH, WHH, and Higgs mass for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters={
    'bsm':  {
        '1560':'{:e}'.format(mHH), #MHH
        '1561':'1.000000e-02',     #WHH
            },
    'mass': {
        '1560':'{:e}'.format(mHH), #MHH
        '25':'1.250000e+02', # Higgs mass set to 125GeV
            },
    }

#---------------------------------------------------------------------------------------------------
# Telling TestHepMC that particles with pdgid 1560 are ok
#---------------------------------------------------------------------------------------------------
with open('pdgid_extras.txt', 'w') as f:
    f.write('1560')

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
settings = {
    'lhe_version':'3.0',
    'cut_decays':'F',
    'fixed_ren_scale': 'True', # use fixed ren scale
    'fixed_fac_scale': 'True', # use fixed fac scale
    'scale':'{}'.format(mHH/2), # MHH/2 ren scale
    'dsqrt_q2fact1':'{}'.format(mHH/2), #MHH/2 fac scale for pdf1
    'dsqrt_q2fact2':'{}'.format(mHH/2), #MHH/2 fac scale for pdf2
    'nevents':int(nevents),
        }

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
#---------------------------------------------------------------------------------------------------
gridpack_mode = False # set to True to produce a gridpack

if not is_gen_from_gridpack():
    process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model HeavyHiggsTHDM
generate p p > hh > h h
output -f
"""
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for modifying up the param_card
# Build a new param_card.dat from an existing one
# If not set in "parameters", values in 'param_card.HeavyScalar.dat' are used
#---------------------------------------------------------------------------------------------------
modify_param_card(param_card_input='param_card.HeavyScalar.dat',process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Modify the run_card.dat from the process directory
# Using the values given in "settings" above for the selected parameters when setting up the run_card
# If not set in "settings", default values are used
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

msg.info("Printing contents of MadGraph cards")
print_cards()

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
msg.info("Finished generate")

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)
msg.info("Finished arrange_output")
if PS == "Py8EG":
    #---------------------------------------------------------------------------------------------------
    # Pythia8 Showering with A14_NNPDF23LO
    #---------------------------------------------------------------------------------------------------
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_LHEF.py")
    include("Pythia8_i/Pythia8_MadGraph.py")

    #---------------------------------------------------------------------------------------------------
    # Decaying hh to bbyy with Pythia8
    #---------------------------------------------------------------------------------------------------
    genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ", #bb decay
                                "25:addChannel = on 0.5 100 22 22 " ] # gammagamma decay
elif PS == "H7EG":
    #--------------------------------------------------------------
    # Herwig 7 (H7UE) showering setup
    #--------------------------------------------------------------
    evgenConfig.tune = "H7.2-Default" # not setup in joboption fragments
    # initialize Herwig7 generator configuration for showering of LHE files
    include("Herwig7_i/Herwig72_LHEF.py")

    # add EvtGen
    include("Herwig7_i/Herwig7_EvtGen.py")

    # retrieving lhe file name for Herwig7
    # treating case of saving or not saving lhe files
    lhe_filename=runArgs.outputTXTFile.split('.tar.gz')[0] + ".events" if hasattr(runArgs, "outputTXTFile") else runArgs.inputGeneratorFile + ".events"
    print(lhe_filename)

    # configure Herwig7
    Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
    Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="LO")

    #HW7 settings and Higgs BR
    Herwig7Config.add_commands ("""
    cd /Herwig/Particles
    create /ThePEG/ParticleData XH
    setup XH 1560  XH MASS WIDTH MAX_WIDTH 0 0 0 1 0
    do /Herwig/Particles/h0:SelectDecayModes none
    set /Herwig/Particles/h0/h0->b,bbar;:OnOff On
    set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff On
    set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
    set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
    do /Herwig/Particles/h0:PrintDecayModes
    """)

    # run Herwig7
    Herwig7Config.run()

if "PS" in globals():
    #---------------------------------------------------------------------------------------------------
    # Filter for bbyy
    #---------------------------------------------------------------------------------------------------
    from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
    filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
    filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
    filtSeq.Expression = "HbbFilter and HyyFilter"
