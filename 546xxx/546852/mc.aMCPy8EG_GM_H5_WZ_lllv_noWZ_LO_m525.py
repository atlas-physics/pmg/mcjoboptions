from MadGraphControl.MadGraphUtils import *
import os, math

useCluster = False
useUserScaleMG = False

# set low_mem_multicore_nlo_generation True

MADGRAPH_CATCH_ERRORS=False

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING["alternative_dynamic_scales"] =  [1,2,3,4]

evgenConfig.generators += ["MadGraph", "EvtGen"]
evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'SameSign', 'electroweak', '2lepton', '2jet']
evgenConfig.contact = ['Karolos Potamianos <karolos.potamianos@cern.ch>', 'Aram Apyan <aram.apyan@cern.ch>']

mass_hp5 = [200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 600, 700, 800, 900, 1000, 1500, 2000, 3000]
# here keep the width of the 3TeV to be narrow as for the given sh value it becomes too large and would violate unitarity. In any case the model dependent GM limits will go up to 1 TeV while the 3 TeV sample will be used only for xsec*BR model independent limits with narrow width.
width_hp5pp = [0.2515, 0.3945, 0.6847, 0.9375, 1.4165, 1.8087, 2.5150, 3.0725, 4.0425, 4.7850, 6.0575, 7.0125, 8.6175, 10.0, 11.7875, 15.6225, 25.5250, 38.8500, 14.0062, 19.3937, 66.8750, 159.8125, 542.25]
width_hp5p = [0.2152, 0.3525, 0.629, 0.871, 1.333, 1.7135, 2.403, 2.9475, 3.9, 4.6325, 5.8875, 6.8325, 8.42, 9.99125, 11.5625, 15.37, 25.225, 38.475, 13.9, 19.275, 66.6875, 158.25, 541.6875]

M1 = -999.
lam5 = -999.
tanth = -999.
vev = 246.22

# Creating pdgid_extras.txt file containing non-standard PDG ID particles
# OK because the particles are intermediate
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""255
-255
256
-256
252
-252
253
-253
254
-254
""")
pdgfile.close()

physShort = get_physics_short()

evgenConfig.nEventsPerJob = 10000
safetyFactor = 1.5
nevents = int(runArgs.maxEvents*safetyFactor if runArgs.maxEvents>0 else safetyFactor*evgenConfig.nEventsPerJob)

required_accuracy = 0.01

mass = int(physShort[physShort.find("LO_m")+4:])
massIdx = mass_hp5.index(mass)

print("Test", physShort, mass, massIdx)

if mass <= 800:
  lam5 = -0.8666
  tanth = 0.57735
else:
  lam5 = -0.7065
  tanth = 0.2581988897

sinth = math.sqrt(tanth**2/(1+tanth**2))
M1  = math.sqrt(2)*sinth*(mass**2 + vev**2)/vev

useDipoleRecoil = True

isPythia = physShort.find("Py8") != -1
isHerwig = physShort.find("H7") != -1

if isPythia: evgenConfig.generators += ["Pythia8"]
elif isHerwig: evgenConfig.generators += ["Herwig7"]

gridpack_mode=True

# Processes
evgenConfig.description = 'MadGraph_' + physShort

model = "GM_UFO"
gen_extra = ""


process = """
  define p = g u c d s u~ c~ d~ s~
  define j = g u c d s u~ c~ d~ s~
  define l+ = e+ mu+ ta+
  define l- = e- mu- ta-
  define vl = ve vm vt
  define vl~ = ve~ vm~ vt~
  import model {}
  generate    p p > H5p  > w+ z j j $$ w+ w- z QCD=0, w+ > l+ vl, z > l+ l- @1
  add process p p > H5p~  > w- z j j $$ w+ w- z QCD=0, w- > l- vl~, z > l+ l- @2
  output -f""".format(model, gen_extra, gen_extra)

print(process)

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'3.0',
           'bwcutoff'     :'15',
           'nevents'      :nevents,
           'dynamical_scale_choice': 0 if useUserScaleMG else -1,
           'maxjetflavor': 5, 
           'ptj': 15.0, 
           'mmjj': 100.0,
           'etal': 3.0, 
           'pta':"0",
           'ptb':"0",
           'drjj':"0",
           'drll':"0",
           'draa':"0",
           'draj':"0",
           'drjl':"0",
           'dral':"0",
           'systematics_program': 'systematics'}

process_dir = new_process(process)


if not is_gen_from_gridpack(): # When generating the gridpack
  if useUserScaleMG:
    os.system("cp setscales_lo.f  "+process_dir+"/SubProcesses/setscales.f")
lo_extras = { 'asrwgtflavor': 5, 
              'auto_ptj_mjj': False, 
              'cut_decays': True, 
              'ptb': 15.0, 
              'etab': 5.5, 
              'dral': 0.1, 
              'drbb': 0.2, 
              'drbj': 0.2, 
              'drbl': 0.2, 
              'drjj': 0.2,
              'drjl': 0.2, 
              'mmll': 0.0, 
              'gridpack': '.true.',
            }
extras.update(lo_extras)

# Modifying param_card
params = {
  'DECAY' : {
    '255' : str(width_hp5pp[massIdx]), # WH5pp
    '256' : str(width_hp5p[massIdx]), # WH5p
  },
  'POTENTIALPARAM' : {
    '5' : str(M1), #M1coeff
    '6' : str(M1/6.), #M2coeff
    'lam3' : str(-0.1),
    'lam5' : str(lam5),
    'lam4' : str(0.2),
    'lam2' : str(0.4*(mass_hp5[massIdx]/1000.)),
  }
}

if mass <= 800:
  params.update( { 'vev' : { 'tanth' : 0.57735 } } )
else:
  params.update( { 'vev' : { 'tanth' : 0.2581988897 } } )

modify_param_card(process_dir=process_dir,params=params)

if useCluster: 
  modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)
#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)

#### Shower                    

if isPythia:
  include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

  if useDipoleRecoil:
    genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]

  include("Pythia8_i/Pythia8_MadGraph.py")
  include("Pythia8_i/Pythia8_ShowerWeights.py")

elif isHerwig:
  from Herwig7_i.Herwig7_iConf import Herwig7
  from Herwig7_i.Herwig72ConfigLHEF import Hw72ConfigLHEF

  genSeq += Herwig7()
  Herwig7Config = Hw72ConfigLHEF(genSeq, runArgs)

# configure Herwig7
  Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
  Herwig7Config.tune_commands()
  lhe_filename = runArgs.outputTXTFile.split('.tar.gz')[0]+'.events'
  Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")
  
  if useDipoleShower:
     dipoleShowerCommands = """
     cd /Herwig/EventHandlers
     set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
     cd /Herwig/DipoleShower
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=2.0 2.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=1.0 2.0 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=0.5 2.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=2.0" 1.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.5" 1.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=2.0" 0.5 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=1.0 0.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=0.5 0.5 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.75_fsr:muRfac=1.0 1.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.5_fsr:muRfac=1.0 1.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.25_fsr:muRfac=1.0 1.25 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.625_fsr:muRfac=1.0" 0.625 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.75_fsr:muRfac=1.0 0.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.875_fsr:muRfac=1.0 0.875 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.75 1.0 1.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.5 1.0 1.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.25 1.0 1.25 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.625 1.0 0.625 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.75 1.0 0.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.875 1.0 0.85 All
     """
     print(dipoleShowerCommands)
     Herwig7Config.add_commands(dipoleShowerCommands)
  
# add EvtGen
  include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
  Herwig7Config.run()
