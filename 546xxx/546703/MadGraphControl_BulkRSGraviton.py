import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('MadGraphControl_BulkRS')

## to run systematic variations on the fly, new in MG2.6.2 (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
## the following setting is needed only if the default base fragments can not be used
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[247000], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[246800,13205,25000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

# extract dataset short name from filename, should be of the form MadGraphPythia8EvtGen_AU2MSTW2008LO_VBF_RS_G_WW_qqqq_kt1_m1000
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

gentype=shortname[5]
decaytype=shortname[6]
scenario=shortname[7]
mass=int(shortname[8][1:])
isVBF=('VBF' in shortname)

# PDF information, in MadGraph's PDF naming scheme.  
# Should probably extract this from shortname[1] in some fancy way.
# For now, specify lhapdf and the ID so that madgraph doesn't get 
# confused trying to interpret some shorthand name (which it can).
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=247000 # NNPDF23_lo_as_0130_qed

scalevariation=1.0
alpsfact=1.0

# -----------------------------------------------------------------------
# Need to arrange several things:
# -- proc_card.dat, this will be determined mostly by the run number.
#    once we have this, we can also initialize the process in madgraph
#
# -- param_card.dat, this is mostly invariant, but does depend on the model scenario
#
# -- run_card.dat, this is also mostly invariant
#
# At that point we can generate the events, arrange the output, and cleanup.
# -----------------------------------------------------------------------
# proc card
# -----------------------------------------------------------------------
# first, figure out what process we should run
processes=[]
pythiachans=[]
if gentype == "WW" or gentype == "ww":
    if decaytype == "lvlv" or decaytype == "llvv":
        processes.append("p p > y, (y > w+ w-, w+ > l+ vl, w- > l- vl~)")
    elif decaytype == "jjjj" or decaytype == "qqqq":
        processes.append("p p > y, (y > w+ w-, w+ > jprime jprime, w- > jprime jprime)")
    elif decaytype == "lvqq" or decaytype == "lvjj":
        processes.append("p p > y, (y > w+ w-, w+ > jprime jprime, w- > l- vl~)")
        processes.append("p p > y, (y > w+ w-, w- > jprime jprime, w+ > l+ vl)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

elif gentype == "ZZ" or gentype == "zz":
    if decaytype == "llll":
        processes.append("p p > y, (y > z z, z > l+ l-, z > l- l+)")
    elif decaytype == "llvv" or gentype == "vvll":
        processes.append("p p > y, (y > z z, z > l+ l-, z > vl vl~)")
    elif decaytype == "jjjj" or decaytype == "qqqq":
        processes.append("p p > y, (y > z z, z > jprime jprime, z > jprime jprime)")
    elif decaytype == "llqq" or decaytype == "lljj":
        processes.append("p p > y, (y > z z, z > jprime jprime, z > l- l+)")
    elif decaytype == "vvqq" or decaytype == "vvjj" or decaytype == "qqvv" or decaytype == "jjvv":
        processes.append("p p > y, (y > z z, z > jprime jprime, z > vl vl~)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

elif gentype == "HH" or gentype == "y":
    processes.append("p p > y, (y > h h)")

    # let pythia decay the Higgs' to get the right BR's
    if decaytype == "qqqq" or decaytype == "jjjj":
        pythiachans.append("25:oneChannel = on 0.569 100 5 -5 ")
        pythiachans.append("25:addChannel = on 0.0287 100 4 -4 ")
    elif decaytype == "bbbb":
        pythiachans.append("25:oneChannel = on 0.569 100 5 -5 ")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

else:
    raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

#if VBF change proccess. Note, inclusive VBF syntax doesn't work since toy VBF model, only this syntax
if isVBF:
  mglog.info("Preparing VBF RSG production")
  for i in range(0,len(processes)):
    processes[i]=processes[i].replace("> y","> y jprime jprime  $$ z w+ w- / g a h",1);
        
# Figure out how to modify the template.
processstring=""
processcount=1
for i in processes:
    if processstring=="":
        processstring="generate %s QED=99 QCD=99 @%d " % (i, processcount)
    else:
        processstring+="\nadd process %s QED=99 QCD=99 @%d " % (i, processcount)
    processcount+=1

# Write the proc card
process_str="""
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model sm

# Define multiparticle labels
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define jprime = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~

import model RS_bulk_ktilda

%s

output -f""" % processstring

# Generate the new process!
if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

# -----------------------------------------------------------------------
# param card
# -----------------------------------------------------------------------
## couplings
ktilde=scenario[2:]
if "0" in ktilde:
  ktilde=str(float(ktilde)*0.1)
params={}
FRBLOCK={}
FRBLOCK["ktilda"]=ktilde
params['FRBLOCK']=FRBLOCK
## masses, decays
masses={
  "25"  : 125., ## SM Higgs
  "39" : float(mass),  ## new particles
}
params['MASS']=masses
decays={
  "39": "DECAY 39 Auto",
}
params['DECAY']=decays

modify_param_card(process_dir=process_dir,params=params)

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed
else:
    raise RuntimeError("No random seed given.")

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)

#---------------------------------------------------------------------------
# run card
#---------------------------------------------------------------------------
## generator cuts
extras = {
  'ptj':"0",
  'ptb':"0",
  'pta':"0",
  'ptl':"0",
  'etaj':"-1",
  'etab':"-1",
  'etaa':"-1",
  'etal':"-1",
  'drjj':"0",
  'drbb':"0",
  'drll':"0",
  'draa':"0",
  'drbj':"0",
  'draj':"0",
  'drjl':"0",
  'drbl':"0",
  'dral':"0",
  'nevents': nevents,
}
if isVBF:
  extras['cut_decays']="F"
  extras['mmjj']=150

## TODO: are the following needed, since fixed_ren_scale and fixed_fac_scale are F by default?
extras["scale"] = mass
extras["dsqrt_q2fact1"] = mass
extras["dsqrt_q2fact2"] = mass

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# -----------------------------------------------------------------------
# Generation
# -----------------------------------------------------------------------
gridpack_mode=False
try:
  generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
  for an_arg in rte.args:
      if 'Gridpack sucessfully created' in an_arg:
          print('Handling exception and exiting')
          theApp.finalize()
          theApp.exit()
  print('Unhandled exception - re-raising')
  raise rte

# -----------------------------------------------------------------------
# Cleanup
# -----------------------------------------------------------------------
try:
    arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)
except:
    raise RuntimeError('Error arranging output dataset!')

mglog.info('All done generating events!!')

# Turn on single-core for pythia
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print(opts)

#--------------------------------------------------------------------------------------------------------------------
# Metadata
#--------------------------------------------------------------------------------------------------------------------
# Some more information
evgenConfig.description = "Bulk RS Graviton Signal Point"
evgenConfig.keywords = ["exotic", "BSM", "RandallSundrum", "warpedED", "graviton"]
evgenConfig.contact = ["Mike Hance <michael.hance@cern.ch>", "Robert Les <robert.les@cern.ch>", "Michiel Veen <michiel.jan.veen@cern.ch>"]
evgenConfig.process = "pp>G*>%s>%s" % (gentype,decaytype) # e.g. pp>G*>WW>qqqq
if isVBF:
    evgenConfig.process = "pp>G*jj>%sjj>%sjj" % (gentype,decaytype) # e.g. pp>G*jj>WWjj>qqqqjj

#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
# Finally, run the parton shower and configure MadGraph+Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += pythiachans
if isVBF:
  genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil = on"]

