### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#keepOutput = True # Debug

### helpers
def StringToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

### parse job options name 
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
additional_options = phys_short.split("_")[6:]
evgenLog.info("SingleStop: Additional options: " + str(additional_options))

randomSeed = runArgs.randomSeed

# Extract job settings/masses etc.
madSpin = False
if 'MS' in additional_options:
    madSpin = True

LepBR = False
if 'Lep' in additional_options:
    LepBR = True

decaymode = phys_short.split('_')[2]

mstop = StringToFloat(phys_short.split('_')[3]) 
mc1   = StringToFloat(phys_short.split('_')[4]) 
mn1   = StringToFloat(phys_short.split('_')[5]) 
masses['1000006'] = mstop
masses['1000024'] = mc1 # C1
masses['1000022'] = mn1 # N1

process = '''
define ljet = g u c d s u~ c~ d~ s~
define bjet = b b~
define susyother = susystrong susyweak / x1- x1+ n1 t1 t1~

define ll- = e- mu- ta-
define ll+ = e+ mu+ ta+
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
'''

if "Cm" in additional_options:
    process += '''
    define c1 = x1- 
    define stop = t1
    '''
    evgenLog.info("Production: stop1 C1m")
elif "Cp" in additional_options:
    process += '''
    define c1 = x1+
    define stop = t1~
    '''
    evgenLog.info("Production: anti-stop1 C1p")
else:
    process += '''
    define c1 = x1- x1+
    define stop = t1 t1~
    '''
    evgenLog.info("stop1 C1m and anti-stop1 C1p production")
    if madSpin:
        raise RunTimeError("ERROR: The bridge mode of MadSpin does not support event files where events do not *all* share the same set of final state particles to be decayed. ")

process +='''
generate p p > stop c1 / susyother
add process p p >  stop c1 ljet / susyother QED=99 QCD=99
add process p p >  stop c1 bjet / susyother t1 t1~ QED=99 QCD=99
add process p p >  stop c1 ljet ljet / susyother QED=99 QCD=99
add process p p >  stop c1 bjet j / susyother t1 t1~ QED=99 QCD=99
'''
evgenLog.info('Production: stop1 C1 production with 0,1,2 jets.')

useguess = True
# max number of jets will be two, unless otherwise specified.
njets = 2

evgenLog.info('generation of single stop production; grid point decoded into m(stop) = %f GeV, m(C1) = %f GeV' % (mstop, mc1))

evgenConfig.contact = [ "jiarong.yuan@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'stop', 'gaugino']
evgenConfig.description = 'Single stop production in simplified model, m_stop1 = %s GeV, m_C1 = %s GeV' % (mstop, mc1)

run_settings["sde_strategy"]=2

# Filter and event multiplier 
evt_multiplier = 2.2

if madSpin:
    # decay chains
    if decaymode == 'tN1':
        randomSeed += 0
        if LepBR:
            decay_chains = ["t1 > n1 t, (t > b w+, w+ > ll+ vl)", "t1~ > n1 t~, (t~ > b~ w-, w- > ll- vl~)", "w+ > ll+ vl", "w- > ll- vl~"]
        else:
            decay_chains = ["t1 > n1 t, (t > b w+, w+ > fu fd~)", "t1~ > n1 t~, (t~ > b~ w-, w- > fu~ fd)", "w+ > fu fd~", "w- > fu~ fd"]
    elif decaymode == 'bC1':
        randomSeed += 3
        if mc1 - mn1 >= 81:
            if LepBR:
                decay_chains = [ "t1 > x1+ b, (x1+ > w+ n1, w+ > ll+ vl)" , "t1~ > x1- b~, (x1- > w- n1, w- > ll- vl~)", "w+ > ll+ vl", "w- > ll- vl~"]
            else:
                decay_chains = [ "t1 > x1+ b, (x1+ > w+ n1, w+ > fu fd~)" , "t1~ > x1- b~, (x1- > w- n1, w- > fu~ fd)", "w+ > fu fd~", "w- > fu~ fd"]
        else:
            decay_chains = [ "t1 > x1+ b" , "t1~ > x1- b~"]
    elif decaymode == 'bWN1':
        randomSeed += 1
        decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~"] 
    elif decaymode == 'bffN1':
        randomSeed += 2
        decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~"]

    else:
        raise RunTimeError("ERROR: did not recognize decaymode {decaymode}")

    if mc1 - mn1 >= 81:
        if LepBR:
            decay_chains += ["x1+ > w+ n1, w+ > ll+ vl", "x1- > w- n1, w- > ll- vl~"]
        else:
            decay_chains += ["x1+ > w+ n1, w+ > fu fd~", "x1- > w- n1, w- > fu~ fd"]
    else:
        if LepBR:
            decays['1000024'] = '''DECAY   1000024  1.704145e-02
                # BR        NDA       ID1       ID2     ID3
                1.078E-01         3     1000022         -11        12   # BR(cha1+ --- chi10 e nu)
                1.078E-01         3     1000022         -13        14   # BR(cha1+ --- chi10 mu nu)
                1.078E-01         3     1000022         -15        16   # BR(cha1+ --- chi10 tau nu)
            '''
        else:
            decays['1000024'] = '''DECAY   1000024  1.704145e-02
                # BR        NDA       ID1       ID2     ID3
                1.078E-01         3     1000022         -11        12   # BR(cha1+ --- chi10 e nu)
                1.078E-01         3     1000022         -13        14   # BR(cha1+ --- chi10 mu nu)
                1.078E-01         3     1000022         -15        16   # BR(cha1+ --- chi10 tau nu)
                3.208525E-01      3     1000022           2        -1   # BR(cha1+ --- chi10 u dbar)
                1.74404E-02       3     1000022           4        -1   # BR(cha1+ --- chi10 c dbar)
                1.74639E-02       3     1000022           2        -3   # BR(cha1+ --- chi10 u sbar)
                3.201099E-01      3     1000022           4        -3   # BR(cha1+ --- chi10 c sbar)
                5.3E-06           3     1000022           2        -5   # BR(cha1+ --- chi10 u bbar)
                5.993E-04         3     1000022           4        -5   # BR(cha1+ --- chi10 c bbar)
            '''

    evgenLog.info('Running w/ MadSpin option')
    madspin_card='madspin_card_test.dat'
    # fixEventWeightsForBridgeMode = True
    mscard = open(madspin_card,'w')
    mscard_to_write = '''
    #************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    #Some options (uncomment to apply)
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    set BW_cut 40 # cut on how far the particle can be off-shell

    #'''

    if randomSeed:
        mscard_to_write += '''
    set seed %i
        '''%(randomSeed)

    mscard_to_write += '''
    set spinmode full
    # specify the decay for the final state particles
    '''
    for decay_chain in decay_chains:
        mscard_to_write += '''
    decay %s
        '''%(decay_chain)
    
    mscard_to_write += '''
    # running the actual code
    launch
    '''
    mscard.write(mscard_to_write)
    mscard.close()

else:
    # Configure our decays
    # stop decay
    if decaymode=='tN1':
        decays['1000006']="""DECAY 1000006 1.0
        #       BR              NDA     ID1       ID2
        1.00000000E+00    2     1000022        6    #t1 -> t + N1
        """
        evgenLog.info('stop1 decay to top N1')
    elif decaymode == 'bWN1':
        decays['1000006']="""DECAY 1000006 1.0
        1.0          3         5        24    1000022  #t1 -> b + W+ + N1
        """
        evgenLog.info('stop1 decay to bWN1')
    elif decaymode == 'bffN1':
        # From https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/507xxx/507131/SUSY_SimplifiedModel_TT_bffN.py
        decays['1000006']="""DECAY 1000006 1.0
        1.078E-01         4     1000022         5       -11        12   # BR(stop1 --- chi10 b e nu)
        1.078E-01         4     1000022         5       -13        14   # BR(stop1 --- chi10 b mu nu)
        1.078E-01         4     1000022         5       -15        16   # BR(stop1 --- chi10 b tau nu)
        3.208525E-01      4     1000022         5         2        -1   # BR(stop1 --- chi10 b u dbar)
        1.74404E-02       4     1000022         5         4        -1   # BR(stop1 --- chi10 b c dbar)
        1.74639E-02       4     1000022         5         2        -3   # BR(stop1 --- chi10 b u sbar)
        3.201099E-01      4     1000022         5         4        -3   # BR(stop1 --- chi10 b c sbar)
        5.3E-06           4     1000022         5         2        -5   # BR(stop1 --- chi10 b u bbar)
        5.993E-04         4     1000022         5         4        -5   # BR(stop1 --- chi10 b c bbar)
        """
        evgenLog.info('stop1 decay to bffN1')
    elif decaymode == 'bC1':
        decays['1000006']="""DECAY 1000006 1.0
        1.00000000E+00    2     1000024        5
        """
        evgenLog.info('stop1 decay to bottom C1')
    elif decaymode == 'tN1bC1':
        decays['1000006']="""DECAY 1000006 1.0
            #       BR              NDA     ID1       ID2
            0.5          2         6   1000022 #t1 -> t + N1
            0.5          2         5   1000024 #t1 -> b + C1
        """
        evgenLog.info('stop1 decay to top N1 or bottom C1')
    else:
        raise RuntimeError("The stop1 decay should be set.")

    # C1 decay
    if mc1 - mn1 >= 81:
        decays['1000024'] = '''DECAY   1000024  1.0
            # BR        NDA       ID1       ID2
            1.0        2     1000022         24
        '''
        evgenLog.info('C1 decay via on-shell W.')
    else:
        decays['1000024'] = '''DECAY   1000024  1.0
            # BR        NDA       ID1       ID2     ID3
            1.078E-01         3     1000022         -11        12   # BR(cha1+ --- chi10 e nu)
            1.078E-01         3     1000022         -13        14   # BR(cha1+ --- chi10 mu nu)
            1.078E-01         3     1000022         -15        16   # BR(cha1+ --- chi10 tau nu)
            3.208525E-01      3     1000022           2        -1   # BR(cha1+ --- chi10 u dbar)
            1.74404E-02       3     1000022           4        -1   # BR(cha1+ --- chi10 c dbar)
            1.74639E-02       3     1000022           2        -3   # BR(cha1+ --- chi10 u sbar)
            3.201099E-01      3     1000022           4        -3   # BR(cha1+ --- chi10 c sbar)
            5.3E-06           3     1000022           2        -5   # BR(cha1+ --- chi10 u bbar)
            5.993E-04         3     1000022           4        -5   # BR(cha1+ --- chi10 c bbar)
        '''
        evgenLog.info('C1 decay via off-shell W.')

# Configure stop mixing
if 'MMIX' in additional_options:
    evgenLog.info("SingleStop: stop is maximally mixed.")
    #common_mixing_matrix('stop_maxmix')
    param_blocks['USQMIX']={}
    # use maximally mixed stop
    param_blocks['USQMIX']['1  1'] = '1.0'  # RRu1x1
    param_blocks['USQMIX']['2  2'] = '1.0'  # RRu2x2
    param_blocks['USQMIX']['3  3'] = ' 0.70710678118' # RRu3x3
    param_blocks['USQMIX']['3  6'] = ' 0.70710678118' # RRu3x6
    param_blocks['USQMIX']['4  4'] = '1.0'  # RRu4x4
    param_blocks['USQMIX']['5  5'] = '1.0'  # RRu5x5
    param_blocks['USQMIX']['6  3'] = ' 0.70710678118' # RRu6x3
    param_blocks['USQMIX']['6  6'] = ' -0.70710678118'# RRu6x6
elif 'LH' in additional_options:
    evgenLog.info("SingleStop: stop1 is LH stop.")
    #common_mixing_matrix('stop_pureL')
    param_blocks['USQMIX']={}
    # stop only left-handed
    param_blocks['USQMIX']['1  1'] = '1.0'  # RRu1x1
    param_blocks['USQMIX']['2  2'] = '1.0'  # RRu2x2
    param_blocks['USQMIX']['3  3'] = '1.0'  # RRu3x3
    param_blocks['USQMIX']['3  6'] = '0.0'  # RRu3x6
    param_blocks['USQMIX']['4  4'] = '1.0'  # RRu4x4
    param_blocks['USQMIX']['5  5'] = '1.0'  # RRu5x5
    param_blocks['USQMIX']['6  3'] = '0.0'  # RRu6x3
    param_blocks['USQMIX']['6  6'] = '1.0'  # RRu6x6
elif 'RH' in additional_options:
    evgenLog.info("SingleStop: stop1 is RH stop.")
    #common_mixing_matrix('stop_pureR')
    param_blocks['USQMIX']={}
    # stop only right-handed
    param_blocks['USQMIX']['1  1'] = '1.0'  # RRu1x1
    param_blocks['USQMIX']['2  2'] = '1.0'  # RRu2x2
    param_blocks['USQMIX']['3  3'] = '0.0'  # RRu3x3
    param_blocks['USQMIX']['3  6'] = '1.0'  # RRu3x6
    param_blocks['USQMIX']['4  4'] = '1.0'  # RRu4x4
    param_blocks['USQMIX']['5  5'] = '1.0'  # RRu5x5
    param_blocks['USQMIX']['6  3'] = '1.0'  # RRu6x3
    param_blocks['USQMIX']['6  6'] = '0.0'  # RRu6x6
else:
    evgenLog.info('SingleStop: stop1 is 30-70 mixed.')

if 'HB' in additional_options:
    # higgsino NLSP C1N2N3 / bino-like LSP N1 / wino N4
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1  1']='0.00E+00'
    param_blocks['VMIX']['1  2']='1.00E+00'
    param_blocks['VMIX']['2  1']='1.00E+00'
    param_blocks['VMIX']['2  2']='0.00E+00'

    param_blocks['UMIX']={}
    param_blocks['UMIX']['1  1']='0.00E+00'
    param_blocks['UMIX']['1  2']='1.00E+00'
    param_blocks['UMIX']['2  1']='1.00E+00'
    param_blocks['UMIX']['2  2']='0.00E+00'

    param_blocks['NMIX']={}
    param_blocks['NMIX']['2  1']='0.00E+00'
    param_blocks['NMIX']['2  2']='0.00E+00'
    param_blocks['NMIX']['2  3']='7.07E-01'
    param_blocks['NMIX']['2  4']='-7.07E-01'
    param_blocks['NMIX']['3  1']='0.00E+00'
    param_blocks['NMIX']['3  2']='0.00E+00'
    param_blocks['NMIX']['3  3']='-7.07E-01'
    param_blocks['NMIX']['3  4']='-7.07E-01'
    param_blocks['NMIX']['4  1']='0.00E+00'
    param_blocks['NMIX']['4  2']='-1.00E+00'
    param_blocks['NMIX']['4  3']='0.00E+00'
    param_blocks['NMIX']['4  4']='0.00E+00'
elif 'Wo' in additional_options:
    # bino N2, wino C1N1
    # Wino scenario
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1 1']='1.00E+00'
    param_blocks['VMIX']['1 2']='0.00E+00'
    param_blocks['VMIX']['2 1']='0.00E+00'
    param_blocks['VMIX']['2 2']='0.00E+00'

    param_blocks['UMIX']={}
    param_blocks['UMIX']['1 1']='1.00E+00'
    param_blocks['UMIX']['1 2']='0.00E+00'
    param_blocks['UMIX']['2 1']='0.00E+00'
    param_blocks['UMIX']['2 2']='0.00E+00'

    param_blocks['NMIX']={}
    param_blocks['NMIX']['1 1']='0.00E+00'
    param_blocks['NMIX']['1 2']='1.00E+00'
    param_blocks['NMIX']['1 3']='0.00E+00'
    param_blocks['NMIX']['1 4']='0.00E+00'
    param_blocks['NMIX']['2 1']='1.00E+00'
    param_blocks['NMIX']['2 2']='0.00E+00'
    param_blocks['NMIX']['2 3']='0.00E+00'
    param_blocks['NMIX']['2 4']='0.00E+00'
    param_blocks['NMIX']['3 1']='0.00E+00'
    param_blocks['NMIX']['3 2']='0.00E+00'
    param_blocks['NMIX']['3 3']='0.00E+00'
    param_blocks['NMIX']['3 4']='0.00E+00'
    param_blocks['NMIX']['4 1']='0.00E+00'
    param_blocks['NMIX']['4 2']='0.00E+00'
    param_blocks['NMIX']['4 3']='0.00E+00'
    param_blocks['NMIX']['4 4']='0.00E+00'
elif 'Ho' in additional_options:
    # bino N3, higgsino C1N2N1
    masses['1000023'] = - mc1
    common_mixing_matrix("higgsino")
elif 'WB' in additional_options:
    # wino NLSP C1N2 /  bino LSP N1
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1 1']='1.00E+00'
    param_blocks['VMIX']['1 2']='0.00E+00'
    param_blocks['VMIX']['2 1']='0.00E+00'
    param_blocks['VMIX']['2 2']='0.00E+00'

    param_blocks['UMIX']={}
    param_blocks['UMIX']['1 1']='1.00E+00'
    param_blocks['UMIX']['1 2']='0.00E+00'
    param_blocks['UMIX']['2 1']='0.00E+00'
    param_blocks['UMIX']['2 2']='0.00E+00'

    param_blocks['NMIX']={}
    param_blocks['NMIX']['2 1']='0.00E+00'
    param_blocks['NMIX']['2 2']='1.00E+00'
    param_blocks['NMIX']['2 3']='0.00E+00'
    param_blocks['NMIX']['2 4']='0.00E+00'
    param_blocks['NMIX']['3 1']='0.00E+00'
    param_blocks['NMIX']['3 2']='0.00E+00'
    param_blocks['NMIX']['3 3']='0.00E+00'
    param_blocks['NMIX']['3 4']='0.00E+00'
    param_blocks['NMIX']['4 1']='0.00E+00'
    param_blocks['NMIX']['4 2']='0.00E+00'
    param_blocks['NMIX']['4 3']='0.00E+00'
    param_blocks['NMIX']['4 4']='0.00E+00'
else:
    evgenLog.info('Default mixing: wino-like c1, bino-like n1')

# post include
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
