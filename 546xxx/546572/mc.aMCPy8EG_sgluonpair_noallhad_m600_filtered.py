import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os

evgenConfig.nEventsPerJob = 5000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents =  4*nevents 
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('9000001 Sgluon 400 (MeV/c) fermion Sgluon 1\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdg_extras.dat'
os.system("get_files %s" % testSeq.TestHepMC.UnknownPDGIDFile)
BasePath = os.getcwd()
process=""" 
import model sgluonsFull_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > sig8 sig8
output -f"""

process_dir = new_process(process)
name='sgluon'
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

extras = { 'lhe_version':'3.0',
        # 'pdlabel'       : "'lhapdf'",
        # 'lhaid'       : lhaid,
        'nevents':int(nevents),
        'event_norm':'sum',
        # 'parton_shower':'PYTHIA8'
}

modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

run_card=process_dir+'/Cards/run_card.dat'


params = {}
# Set masses
# Get the card set up
masses = {}
decays = {}
coupling_NPYLD = {}
coupling_NPYLU = {}
coupling_NPYRD = {}
coupling_NPYRU = {}
masses = {'9000001' : 0.60000000e+03}





# sminputs = {'aEWM1' : 1.323489e+02}
params['MASS']=masses

decays['9000001'] = """DECAY 9000001 AUTO # w8"""

params['DECAY']=decays
### coupling setting ###
coupling_NPYLD['1 1'] = '0.'
coupling_NPYLD['2 2'] = '0.'
coupling_NPYLD['3 3'] = '0.'

coupling_NPYLU['1 1'] = '0.'
coupling_NPYLU['2 2'] = '0.'
coupling_NPYLU['3 3'] = '0.5'

coupling_NPYRD['1 1'] = '0.'
coupling_NPYRD['2 2'] = '0.'
coupling_NPYRD['3 3'] = '0.'

coupling_NPYRU['1 1'] = '0.'
coupling_NPYRU['2 2'] = '0.'
coupling_NPYRU['3 3'] = '0.5'
params['NPYLD']= coupling_NPYLD
params['NPYLU']= coupling_NPYLU
params['NPYRD']= coupling_NPYRD
params['NPYRU']= coupling_NPYRU
params['NPG8G']={
    '1' : 0
}
# modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays})
print ("Final process card:")
print (process)

runName='run_01' 

modify_param_card(process_dir=process_dir,params=params)


mcprod_maddec = "define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~ \ndecay sig8 > t t~, (t > b w+, w+ > wdec wdec), (t~ > b~ w-, w- > wdec wdec) \ndecay t > w+ b, w+ > wdec wdec \ndecay t~ > w- b~, w- > wdec wdec"

# Decay with MadSpin
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w') 
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
#set BW_cut 15                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set seed %i
%s
launch
"""%(runArgs.randomSeed, mcprod_maddec))
mscard.close()



generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)
evgenConfig.description = 'MadGraph_tttt_ttsgluons'
evgenConfig.generators += ["aMcAtNlo","Pythia8","EvtGen"]

evgenConfig.process= "p p ->sig8+sig8-> t+t~+t+t~"
evgenConfig.keywords += ["top","SUSY","jets"]

evgenConfig.contact = ["zongpu.wang@cern.ch"]
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
# genSeq.Pythia8.Commands += [   "TimeShower:globalRecoilMode = 2",
#                                "TimeShower:nMaxGlobalBranch = 1",
#                                "TimeShower:nPartonsInBorn = 2",
#                                "TimeShower:limitPTmaxGlobal = on"]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
### Set lepton filters
if not hasattr(filtSeq, "MultiLeptonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
   lepfilter_leading = MultiLeptonFilter("lepfilter_leading")
   filtSeq += lepfilter_leading
   lepfilter = MultiLeptonFilter("lepfilter")
   filtSeq += lepfilter
if not hasattr(filtSeq, "LeptonPairFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import LeptonPairFilter
   lepPairfilter = LeptonPairFilter("lepPairfilter")
   filtSeq += lepPairfilter


filtSeq.lepfilter_leading.Ptcut = 25000.0 #MeV
filtSeq.lepfilter_leading.Etacut = 2.8
filtSeq.lepfilter_leading.NLeptons = 1 #minimum

filtSeq.lepfilter.Ptcut = 12000.0 #MeV
filtSeq.lepfilter.Etacut = 2.8
filtSeq.lepfilter.NLeptons = 2 #minimum


# no requirement on the OS pairs
filtSeq.lepPairfilter.NSFOS_Max = -1 
filtSeq.lepPairfilter.NSFOS_Min = -1
filtSeq.lepPairfilter.NOFOS_Max = -1
filtSeq.lepPairfilter.NOFOS_Min = -1


filtSeq.lepPairfilter.NSFSS_Min = 0 # at least 0 SFSS pairs with NPairSum_Min which will give at least 1 SS pair
filtSeq.lepPairfilter.NOFSS_Min = 0 # at least 0 OSSS pairs with NPairSum_Min which will give at least 1 SS pair
filtSeq.lepPairfilter.NSFSS_Max = -1 # no requirement on max of SS pairs
filtSeq.lepPairfilter.NOFSS_Max = -1 # no requirement on max of SS pairs

# Count number of SS pair 
filtSeq.lepPairfilter.UseSFSSInSum = True
filtSeq.lepPairfilter.UseOFSSInSum = True
filtSeq.lepPairfilter.UseSFOSInSum = False # no requirement on the OS pairs
filtSeq.lepPairfilter.UseOFOSInSum = False # no requirement on the OS pairs

# At least >=1 SS pair
filtSeq.lepPairfilter.NPairSum_Min = 1 # at least 1 SS pairs
filtSeq.lepPairfilter.NPairSum_Max = -1 # at least 1 SS pairs


# Require the event have leptons from resonant decays but not heavy flavor decays (>20 GeV cut on the resonance...)
# However, it will find the first parent particle 
filtSeq.lepPairfilter.OnlyMassiveParents = False 

filtSeq.lepPairfilter.Ptcut = 12000.0 #MeV
filtSeq.lepPairfilter.Etacut = 2.8

filtSeq.lepPairfilter.NLeptons_Min = 2
filtSeq.lepPairfilter.NLeptons_Max = -1 # No max of leptons 

filtSeq.Expression = "(lepfilter_leading and lepfilter and lepPairfilter)"



testSeq.TestHepMC.EffFailThreshold = 0.96

