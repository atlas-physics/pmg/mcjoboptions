model="LightVector"
fs = "mumu"
mDM1 = 5.0
mDM2 = 1000.0
mZp = 2000.0
mHD = 125.

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_ZpMET_lep.py")

