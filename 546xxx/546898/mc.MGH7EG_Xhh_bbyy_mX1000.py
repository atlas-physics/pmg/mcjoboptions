#---------------------------------------------------------------------------------------------------
# X->HH->bbyy production with MadGraph+Pythia8
#---------------------------------------------------------------------------------------------------
mHH=1000 # resonance mass in GeV
PS="H7EG" # PS&had with Herwig7+EvtGen
evgenConfig.nEventsPerJob = 10000
include("MadGraph5_Xhh_bbyy_updated.py")
