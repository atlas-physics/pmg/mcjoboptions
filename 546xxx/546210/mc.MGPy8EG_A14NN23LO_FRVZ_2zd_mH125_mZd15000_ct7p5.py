params={}
params['mZDinput']  = 1.500000e+01
params['MHSinput']  = 3.000000e+01
params['epsilon']   = 1.000000e-03
params['gXmu']      = 9.500000e-04
params['gXe']       = 9.50000e-04
params['gXpi']      = 1.000000e-11
params['MChi1']     = 1.000000e+01
params['MChi2']     = 4.500000e+01
params['WZp']       = "DECAY 3000001 1.623360e-07 # WZp"
params['mH']        = 125
params['nGamma']    = 2
params['decayMode'] = 'normal'
params['avgtau'] = 7.5




                

include ("MadGraphControl_A14N23LO_FRVZdisplaced_ggF.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    

