import re
import os
import math
import subprocess

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents=runArgs.maxEvents
LHE_EventMultiplier = 2.0
if LHE_EventMultiplier > 0 :
    nevents=runArgs.maxEvents*LHE_EventMultiplier



if el_or_mu == 1 and u_or_c == 1 :
    my_process = """
    import model 4fermitop_up_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- t NP^2==4 @0
    add process p p > e+ e- t~ NP^2==4 @1
    add process p p > e+ e- t j NP^2==4 @2
    add process p p > e+ e- t~ j NP^2==4 @3
    output -f"""
elif el_or_mu == 2 and u_or_c == 1 :
    my_process = """
    import model 4fermitop_up_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- t NP^2==4 @0
    add process p p > mu+ mu- t~ NP^2==4 @1
    add process p p > mu+ mu- t j NP^2==4 @2
    add process p p > mu+ mu- t~ j NP^2==4 @3
    output -f"""
elif el_or_mu == 1 and u_or_c == 2 :
    my_process = """
    import model 4fermitop_charm_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- t NP^2==4 @0
    add process p p > e+ e- t~ NP^2==4 @1
    add process p p > e+ e- t j NP^2==4 @2
    add process p p > e+ e- t~ j NP^2==4 @3
    output -f"""
elif el_or_mu == 2 and u_or_c == 2 :
    my_process = """
    import model 4fermitop_charm_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- t NP^2==4 @0
    add process p p > mu+ mu- t~ NP^2==4 @1
    add process p p > mu+ mu- t j NP^2==4 @2
    add process p p > mu+ mu- t~ j NP^2==4 @3
    output -f"""

  



beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(my_process)
extras = {'ickkw'      : '0',
          'ptj'   : '20.0',
          # 'mmll'   : mmll,
          'xqcut'   : '0.0',
          # 'drjj'   : '0.4',
          'ktdurham'   : '300.0',
          'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
          'nevents'   : nevents,
          'maxjetflavor': '5',
          'asrwgtflavor': '5'}


couplings = {'lambdas': str(Lambda),
          'fsrr'      : str(fsrr),
          'ftrr'      : str(ftrr),
          'fvll'   : str(fvll),
          'fvlr'   : str(fvlr),
          'fvrl'   : str(fvrl),
          'fvrr'   : str(fvrr)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params={}
params['DIM6']=couplings
modify_param_card(process_dir=process_dir,params=params)



print_cards()


generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3, saveProcDir=False)


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

PYTHIA8_nJetMax=1
PYTHIA8_Dparameter=0.4
PYTHIA8_TMS=300
PYTHIA8_nQuarksMerge=5
PYTHIA8_Process="guess"                                                                                                                                                       

include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']


evgenConfig.description = 'Production of tull EFT'
evgenConfig.keywords += ['BSM', 'exotic']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> tjll'
evgenConfig.tune = 'A14 NNPDF23LO'
evgenConfig.contact = ["Yoav Afik <yafik@cern.ch>"]

