##This job option is the SF signal only generation with EFTs=0, and 1 extra jet
##dsid = 100100
from AthenaCommon import Logging
import os
Logging.logging.basicConfig()

from MadGraphControl.MadGraphUtils import *

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

gridpack_mode=True
saveRundetails=True
runPythia=False
if is_gen_from_gridpack():
    saveRundetails = False
    runPythia=True



if is_gen_from_gridpack():
    print("Generating from Gridpack, doing so serially")
    check_reset_proc_number(opts)

if gridpack_mode:
    print("gridpack_mode")
    from MadGraphControl import MadGraphUtils
    MadGraphUtils.MADGRAPH_CATCH_ERRORS=False

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor=1.2
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor


# Note that the model imported here has cpG set to 0
# Note that the syntax of H->ZZ actually does the same!
process="""
set complex_mass_scheme True
set max_npoint_for_channel 4
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTatNLO-NLO_WWscales
generate g g > e+ e- ve ve~ QCD=2 QED=4 NP=0 [noborn=QCD] @0
add process g g > mu+ mu- vm vm~ QCD=2 QED=4 NP=0 [noborn=QCD] @1
add process g g > ta+ ta- vt vt~ QCD=2 QED=4 NP=0 [noborn=QCD] @2
output -f"""

decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width
masses={'25': '1.250000e+02'}  ## Higgs mass

process_dir = new_process(process, keepJpegs=True, usePMGSettings=False)


#Fetch default LO run_card.dat and set parameters
settings = {
            'maxjetflavor': 5,
            'pta':10.0,
            'ptj':5.0,
            'ptb':5.0,
            'ptl': 10.0,
            'ptl2min':10.0, # 5 GeV leeway with selection cuts
            'ptl1min': 20.0, # 7 GeV leeway with selection cuts
            'etaj':5,
            'etal':5,
            'mmnl': 140,
            'dynamical_scale_choice' : '-1', #default value
            'beamEnergy':beamEnergy,
            'nevents':int(nevents),
            'job_strategy': 2
}


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# modify_config_card(process_dir=process_dir,settings=
# {'cluster_type':'condor',
# 'cluster_queue': 'medium'})

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

params={}

## Block dim6
# 1 # Lambda = 1000
# 2 # cpDC = 0
# 3 # cpWB = 0
# 4 # cdp = 0
# 5 # cp = 0
# 6 # cWWW = 0
# 9 # cpW = 0
# 10 # cpBB = 0
c_dim6={'1': 1000, '2': 1e-5, '3' : 1e-5, '4' : 1e-5, '5' : 1e-5, '6' : 1e-5, '9' : 1e-5, '10' : 1e-5} 

## Block dim62f
# 1 # cpl1 = 0
# 2 # cpl2 = 0
# 3 # cpl3 = 0
# 4 # c3pl1 = 0
# 5 # c3pl2 = 0
# 6 # c3pl3 = 0
# 7 # cpe = 0
# 8 # cpmu = 0
# 9 # cpta = 0
# 10 # cpqMi = 0
# 11 # cpq3i = 0
# 12 # cpQ3 = 0
# 13 # cpQM = 0
# 14 # cpu = 0
# 15 # cpt = 0
# 16 # cpd = 0
# 19 # ctp = 0
# 22 # ctZ = 0
# 23 # ctW = 0
# 24 # ctG = 0
c_dim62f={'1' : 1e-5, '2' : 1e-5, '3' : 1e-5, '4' : 1e-5, '5' : 1e-5, '6' : 1e-5, '7' : 1e-5, '8' : 1e-5, '9' : 1e-5, '10' : 1e-5, '11' : 1e-5, '12' : 1e-5, '13' : 1e-5, '14' : 1e-5, '15' : 1e-5, '16' : 1e-5, '19' : 1e-5, '22' : 1e-5, '23' : 1e-5, '24' : 1e-5} 

## Block dim64f
# 1 # cQq83 = 0
# 2 # cQq81 = 0
# 3 # cQu8 = 0
# 4 # ctq8 = 0
# 6 # cQd8 = 0
# 7 # ctu8 = 0
# 8 # ctd8 = 0
# 10 # cQq13 = 0
# 11 # cQq11 = 0
# 12 # cQu1 = 0
# 13 # ctq1 = 0
# 14 # cQd1 = 0
# 16 # ctu1 = 0
# 17 # ctd1 = 0
# 19 # cQQ8 = 0
# 20 # cQQ1 = 0
# 21 # cQt1 = 0
# 23 # ctt1 = 0
# 25 # cQt8 = 0
c_dim64f={'1' : 1e-5, '2' : 1e-5, '3' : 1e-5, '4' : 1e-5, '6' : 1e-5, '7' : 1e-5, '8' : 1e-5, '10' : 1e-5, '11' : 1e-5, '12' : 1e-5, '13' : 1e-5, '14' : 1e-5, '16' : 1e-5, '17' : 1e-5, '19' : 1e-5, '20' : 1e-5, '21' : 1e-5, '23' : 1e-5, '25' : 1e-5} 

##Block dim64f2l 
# 1 # cQlM1 = 0
# 2 # cQlM2 = 0
# 3 # cQl31 = 0
# 4 # cQl32 = 0
# 5 # cQe1 = 0
# 6 # cQe2 = 0
# 7 # ctl1 = 0
# 8 # ctl2 = 0
# 9 # cte1 = 0
# 10 # cte2 = 0
# 13 # cQlM3 = 0
# 14 # cQl33 = 0
# 15 # cQe3 = 0
# 16 # ctl3 = 0
# 17 # cte3 = 0
c_dim64f2l={'1' : 1e-5, '2' : 1e-5, '3' : 1e-5, '4' : 1e-5, '5' : 1e-5, '6' : 1e-5, '7' : 1e-5, '8' : 1e-5, '9' : 1e-5, '10' : 1e-5, '13' : 1e-5, '14' : 1e-5, '15' : 1e-5, '16' : 1e-5, '17' : 1e-5}

##Block dim64f4l 
# 1 # cll1111 = 0
# 2 # cll2222 = 0
# 3 # cll3333 = 0
# 4 # cll1122 = 0
# 5 # cll1133 = 0
# 6 # cll2233 = 0
# 7 # cll1221 = 0
# 8 # cll1331 = 0
# 9 # cll2332 = 0
c_dim64f4l={'1' : 1e-5, '2' : 1e-5, '3' : 1e-5, '4' : 1e-5, '5' : 1e-5, '6' : 1e-5, '7' : 1e-5, '8' : 1e-5, '9' : 1e-5} 

params['dim6'] =c_dim6
params['dim62f']=c_dim62f
params['dim64f'] =c_dim64f
params['dim64f2l']=c_dim64f2l
params['dim64f4l']=c_dim64f4l
params['mass']=masses
params['DECAY']=decays

# Make default card
modify_param_card(process_dir=process_dir,params=params)

###############################################################################################################
######################################### make other cards on the fly #########################################
###############################################################################################################
def modify_block(file_path, block_name, param_number, param_value, param_comment):
    """
    Modify or add a parameter to a specified block in the file.
    
    Args:
        file_path (str): The path to the parameter file.
        block_name (str): The block name (e.g., 'dim6') where the parameter will be added or modified.
        param_number (int): The parameter number to add/modify.
        param_value (float or int): The value of the parameter.
        param_comment (str): The comment to add with the parameter.
    """
    
    # Read the contents of the file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Find the position where the specified block is located
    block_start = None
    for i, line in enumerate(lines):
        if line.strip() == "Block {}".format(block_name):
            block_start = i
            break

    if block_start is not None:
        # New entry to add
        new_entry = "   {}    {}   # {}\n".format(param_number, param_value, param_comment)

        # Check if the parameter already exists, update it if found
        param_exists = False
        insert_position = block_start + 1
        while insert_position < len(lines) and lines[insert_position].strip() and not lines[insert_position].startswith('Block'):
            current_line = lines[insert_position].strip().split()
            if current_line and int(current_line[0]) == param_number:
                # Parameter already exists, update the line
                lines[insert_position] = new_entry
                param_exists = True
                break
            insert_position += 1
        
        if not param_exists:
            # If parameter doesn't exist, insert the new entry at the correct position
            lines.insert(insert_position, new_entry)
    
    # Write the modified content back to the file
    with open(file_path, 'w') as file:
        file.writelines(lines)

# Modify the parameter card to add the new parameters
param_card_path = os.path.join(process_dir, 'Cards/param_card.dat')
param_card_LO_path= os.path.join(process_dir, 'Cards/param_card_LO.dat')


# Make the LO param card (with some parameters that are disabled in NLO enabled)
import shutil
shutil.copy(param_card_path, param_card_path.replace(os.path.basename(param_card_path), os.path.basename(param_card_LO_path)))

modify_block(param_card_LO_path, 'dim6', 7, 1e-5, 'cG')
modify_block(param_card_LO_path, 'dim64f2l', 19, 1e-5, 'ctlS3')
modify_block(param_card_LO_path, 'dim64f2l', 20, 1e-5, 'ctlT3')
modify_block(param_card_LO_path, 'dim64f2l', 21, 1e-5, 'cblS3')

# Make the process specific param cards

# cpG (dim6 8)
param_card_cpG_path= os.path.join(process_dir, 'Cards/param_card_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cpG_path)))
modify_block(param_card_cpG_path, 'dim6', 8, 1, 'cpG')

# ctp_cpG (dim6 8, dim62f 19)
param_card_ctp_cpG_path= os.path.join(process_dir, 'Cards/param_card_ctp_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_ctp_cpG_path)))
modify_block(param_card_ctp_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_ctp_cpG_path, 'dim62f', 19, 1, 'ctp')

# ctG_cpG (dim6 8, dim62f 24)
param_card_ctG_cpG_path= os.path.join(process_dir, 'Cards/param_card_ctG_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_ctG_cpG_path)))
modify_block(param_card_ctG_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_ctG_cpG_path, 'dim62f', 24, 1, 'ctG')

# cpW_cpG (dim6 8, dim6 9)
param_card_cpW_cpG_path= os.path.join(process_dir, 'Cards/param_card_cpW_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cpW_cpG_path)))
modify_block(param_card_cpW_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_cpW_cpG_path, 'dim6', 9, 1, 'cpW')

# cpDc_cpG (dim6 8, dim6 2)
param_card_cpDc_cpG_path= os.path.join(process_dir, 'Cards/param_card_cpDc_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cpDc_cpG_path)))
modify_block(param_card_cpDc_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_cpDc_cpG_path, 'dim6', 2, 1, 'cpDc')

# cdp_cpG (dim6 8, dim6 4)
param_card_cdp_cpG_path= os.path.join(process_dir, 'Cards/param_card_cdp_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cdp_cpG_path)))
modify_block(param_card_cdp_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_cdp_cpG_path, 'dim6', 4, 1, 'cdp')

# c3pl1_cpG (dim6 8, dim62f 4)
param_card_c3pl1_cpG_path= os.path.join(process_dir, 'Cards/param_card_c3pl1_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_c3pl1_cpG_path)))
modify_block(param_card_c3pl1_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_c3pl1_cpG_path, 'dim62f', 4, 1, 'c3pl1')

# c3pl2_cpG (dim6 8, dim62f 5)
param_card_c3pl2_cpG_path= os.path.join(process_dir, 'Cards/param_card_c3pl2_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_c3pl2_cpG_path)))
modify_block(param_card_c3pl2_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_c3pl2_cpG_path, 'dim62f', 5, 1, 'c3pl2')

# cll1221_cpG (dim6 8, dim64f4l 7)
param_card_cll1221_cpG_path= os.path.join(process_dir, 'Cards/param_card_cll1221_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cll1221_cpG_path)))
modify_block(param_card_cll1221_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_cll1221_cpG_path, 'dim64f4l', 7, 1, 'cll1221')

# cp_cpG (dim6 8, dim6 5)
param_card_cp_cpG_path= os.path.join(process_dir, 'Cards/param_card_cp_cpG.dat')
shutil.copy(param_card_LO_path, param_card_LO_path.replace(os.path.basename(param_card_LO_path), os.path.basename(param_card_cp_cpG_path)))
modify_block(param_card_cp_cpG_path, 'dim6', 8, 1, 'cpG')
modify_block(param_card_cp_cpG_path, 'dim6', 5, 1, 'cp')

###############################################################################################################
####################################### end make other cards on the fly #######################################
###############################################################################################################

print_cards()

# #---------------------------------------------------------------------------
# # Reweighting setup
# #---------------------------------------------------------------------------
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""
# content of reweight_card.dat ============

change model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTatNLO-LO_WWscales

change process g g > e+ e- ve ve~ QCD=2 QED=4 NP^2==4 [virt=QCD] @0
change process g g > mu+ mu- vm vm~ QCD=2 QED=4 NP^2==4 [virt=QCD] @1 --add
change process g g > ta+ ta- vt vt~ QCD=2 QED=4 NP^2==4 [virt=QCD] @2 --add

launch --rwgt_name=cpG_squared --rwgt_info=cpG_squared
    {0}

launch --rwgt_name=ctp_cpG --rwgt_info=ctp_cpG
    {1}

launch --rwgt_name=ctG_cpG --rwgt_info=ctG_cpG
    {2}

launch --rwgt_name=cpW_cpG --rwgt_info=cpW_cpG
    {3}

launch --rwgt_name=cpDc_cpG --rwgt_info=cpDc_cpG
    {4}

launch --rwgt_name=cdp_cpG --rwgt_info=cdp_cpG
    {5}

launch --rwgt_name=c3pl1_cpG --rwgt_info=c3pl1_cpG
    {6}

launch --rwgt_name=c3pl2_cpG --rwgt_info=c3pl2_cpG
    {7}

launch --rwgt_name=cll1221_cpG --rwgt_info=cll1221_cpG
    {8}

launch --rwgt_name=cp_cpG --rwgt_info=cp_cpG
    {9}

# ====================================
""".format(param_card_cpG_path, 
           param_card_ctp_cpG_path,
           param_card_ctG_cpG_path,
           param_card_cpW_cpG_path,
           param_card_cpDc_cpG_path,
           param_card_cdp_cpG_path,
           param_card_c3pl1_cpG_path,
           param_card_c3pl2_cpG_path,
           param_card_cll1221_cpG_path,
           param_card_cp_cpG_path))
reweight_card_f.close()

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup
#---------------------------------------------------------------------------

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=saveRundetails)  

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower

if runPythia :
    evgenConfig.description = 'aMcAtNlo_H_WW'
    evgenConfig.keywords+=['Higgs','2lepton', 'WW']
    evgenConfig.contact = ["Zef Wolffs <zewolffs@cern.ch>"]
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_MadGraph.py")
else:
    theApp.finalize()
    theApp.exit()