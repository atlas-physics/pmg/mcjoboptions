#---------------------------------------------------------------------------------------------------
# X->HH->bbyy production with MadGraph+Pythia8
#---------------------------------------------------------------------------------------------------
mHH=500 # resonance mass in GeV
PS="Py8EG" # PS&had with Pythia8+EvtGen
evgenConfig.nEventsPerJob = 10000
include("MadGraph5_Xhh_bbyy.py")
