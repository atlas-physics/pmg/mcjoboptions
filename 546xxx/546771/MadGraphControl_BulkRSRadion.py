#! /usr/bin/env python

import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('MadGraphControl_BulkRadion')

## to run systematic variations on the fly, new in MG2.6.2 (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
## the following setting is needed only if the default base fragments can not be used
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[247000], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[246800,13205,25000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

### ------------------- Main working function --------------------
def BulkRadion_Generation(run_number=100000,
                          gentype="WW",
                          decaytype="lvqq",
                          scenario="k35L1",
                          mass=1000,
                          rand_seed=1234,
                          beamEnergy=6500.,
                          scalevariation=1.0,
                          alpsfact=1.0,
                          pdf='nn23lo',
                          lhaid=247000,
                          isVBF=False):

    ### -------------------- setup proc_card --------------------
    #First, figure out what process we should run
    processes=[]
    if gentype == "WW" or gentype == "ww":
        if decaytype == "lvlv" or decaytype == "llvv":
            processes.append("generate p p > h2, (h2 > w+ w-, w+ > l+ vl, w- > vl~ l-)")
        elif decaytype == "jjjj" or decaytype == "qqqq":
            processes.append("generate p p > h2, (h2 > w+ w-, w+ > jprime jprime, w- > jprime jprime)")
        elif decaytype == "lvqq" or decaytype == "lvjj":
            processes.append("generate p p > h2, (h2 > w+ w-, w+ > l+ vl, w- > jprime jprime)")
            processes.append("add process p p > h2, (h2 > w+ w-, w+ > jprime jprime, w- > l- vl~)")
        else:
            mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
            return -1

    elif gentype == "ZZ" or gentype == "zz":
        if decaytype == "llll":
            processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > l+ l-)")
        elif decaytype == "llvv" or gentype == "vvll":
            processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > vl vl~)")
        elif decaytype == "jjjj" or decaytype == "qqqq":
            processes.append("generate p p > h2, (h2 > z z, z > jprime jprime, z > jprime jprime)")
        elif decaytype == "llqq" or decaytype == "lljj":
            processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > jprime jprime)")
        elif decaytype == "vvqq" or decaytype == "vvjj" or decaytype == "qqvv" or decaytype == "jjvv":
            processes.append("generate p p > h2, (h2 > z z, z > vl vl~, z > jprime jprime)")
        else:
            mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
            return -1

    else:
        mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
        return -1
            
    #if VBF change proccess
    if isVBF:
      mglog.info("Preparing VBF Radion production")
      for i in range(0,len(processes)):
        #processes[i]=processes[i].replace("> h2","> h2 jprime jprime QCD=0",1);
        processes[i]=processes[i].replace("> h2","> h2 jprime jprime  $$ z w+ w- / g a h",1);

    processstring=""
    for i in range(0,len(processes)):
      processstring+=processes[i]+"\n"

    #---------------------------------------------------------------------------
    # Number of Events
    #---------------------------------------------------------------------------
    safefactor = 1.1
    if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0: 
        nevents = int(runArgs.maxEvents*safefactor)
    else:
        nevents = int(evgenConfig.nEventsPerJob*safefactor)

    # Write the default run card
    proc_card_name = "proc_card_mg5.dat"
    fcard = open(proc_card_name,'w')

    process_str="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False
    set gauge unitary
    set complex_mass_scheme False
    import model sm
    
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = u c d s u~ c~ d~ s~ b b~
    define jprime = u c d s u~ c~ d~ s~ b b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    
    import model heft_radion
    
    %s
    
    output -f
    """ % processstring

    fcard.write(process_str)
    fcard.close()


    # Submit Proc_card
    process_dir = new_process(process_str)

    ### -------------------- setup param_card --------------------
    
    # Add values to param card
    kl=scenario.split("L")[0].strip("kl")
    lambda_r=str(float(scenario.split("L")[-1])*1000)
    ## Widths from tool here: https://gitlab.cern.ch/rles/RSCalculator
    widths={200:"0.14",250:"0.395",300:"0.65",400:"2.2",500:"4.6",600:"7.6",700:"12.6",800:"19.0",900:"28.5",1000:"38.0",1100:"48.0",1200:"58.0",1300:"75.33",1400:"92.67",1500:"110.0",1600:"129.0",1700:"161.0",1800:"193.0",1900:"226.5",2000:"260.0",2200:"380.0",2400:"500.0",2600:"640.0",2800:"793.0",3000:"946.0",3500:"1429.0",4000:"2000.0",4500:"2984.0",5000:"4300.0",6000:"6000.0",}
    params={}
    mass_inputs={}
    width_inputs={}
    mass_inputs['mh02']=mass
    mass_inputs['K']=kl
    mass_inputs['LR']=lambda_r
    width_inputs['wh02']=widths[mass]
    #width_inputs['wh02']="Auto" ## let MadGraph calculate the width and decay br's on the fly ?
    params['mass']=mass_inputs
    params['decay']=width_inputs

    str_param_card='param_card.dat'

    ### -------------------- setup run_card --------------------
    # run card
    ## generator cuts
    extras = {
        'ptj':"0",
        'ptb':"0",
        'pta':"0",
        'ptl':"0",
        'etaj':"-1",
        'etab':"-1",
        'etaa':"-1",
        'etal':"-1",
        'drjj':"0",
        'drbb':"0",
        'drll':"0",
        'draa':"0",
        'drbj':"0",
        'draj':"0",
        'drjl':"0",
        'drbl':"0",
        'dral':"0",
        'nevents': nevents,
    }

    ## TODO: are the following needed, since fixed_ren_scale and fixed_fac_scale are F by default?
    extras["scale"] = mass
    extras["dsqrt_q2fact1"] = mass
    extras["dsqrt_q2fact2"] = mass

    ## no need to specify pdfsets, if using the base fragment
    ## (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
    #extras["pdlabel"]=pdf
    #extras["lhaid"]=lhaid

    if isVBF:
      extras['cut_decays']="F"
      extras['mmjj']=150

    # make run card
    modify_param_card(process_dir=process_dir,params=params)

    # update run card
    modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

    ### -------------------- Generation --------------------
    # See if things are where we expected them to be...
    if 1==process_dir:
        mglog.error('Error in process generation!')
        return -1
    if not os.access(process_dir,os.R_OK):
        mglog.error('Could not find process directory '+process_dir+' !!  Bailing out!!')
        return -1
    else:
        mglog.info('Using process directory '+process_dir)

    # Run Generation
    if generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs):
        mglog.error('Error generating events!')
        return -1

    # -------------------- Cleanup --------------------
    outputDS=""
    try:
        arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

    except:
        mglog.error('Error arranging output dataset!')
        return -1

    keepOutput=True
    if not keepOutput:
        mglog.info('Removing process directory...')
        shutil.rmtree(process_dir,ignore_errors=True)

    mglog.info('All done generating events!!')
    return outputDS

### -------------------- Where code actually starts --------------------
## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

# decode dataset short name,
# new r21 format:
# mc.MGPy8EG_A14N23LO_GGF_radion_WW_qqqq_kl35L3_m3000
raddecay=shortname[4]
VVdecay=shortname[5]
scenario=shortname[6]
mass=int(shortname[7][1:])
isVBF=('VBF' in shortname)

# PDF information, in MadGraph's PDF naming scheme.  
# Should probably extract this from shortname[1] in some fancy way.
# For now, specify lhapdf and the ID so that madgraph doesn't get 
# confused trying to interpret some shorthand name (which it can).
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=247000 # NNPDF23_lo_as_0130_qed

# Run MadGraph!
runArgs.inputGeneratorFile=BulkRadion_Generation(999,
                                                 raddecay,              # How the G* decays
                                                 VVdecay,               # How the SM bosons decay
                                                 scenario,              # k/mPl, for instance
                                                 mass,                  # Graviton mass
                                                 runArgs.randomSeed,    # random seed
                                                 runArgs.ecmEnergy/2.,  # beam energy
                                                 1.0,                   # scale variation
                                                 1.0,                   # PS variation
                                                 pdf,                   # PDF information
                                                 lhaid,
                                                 isVBF)

# Some more information
evgenConfig.description = "Bulk RS Radion Signal Point"
evgenConfig.keywords = ["exotic", "BSM", "RandallSundrum", "warpedED"]
evgenConfig.contact = ["Robert Les <robert.les@cern.ch>", "Antonio Giannini <antonio.giannini@cern.ch>", "Michiel Veen <michiel.jan.veen@cern.ch>"]
evgenConfig.process = "pp>phi>%s>%s" % (raddecay,VVdecay) # e.g. pp>phi*>WW>qqqq

# Turn on single-core for pythia
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print (opts)

# Finally, run the parton shower...
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# ...and pull in MadGraph-specific stuff
include("Pythia8_i/Pythia8_MadGraph.py")

