import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=100
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
new_parameter=parameters['NEW'] = {'CV':  '1',  # CV
                     'C2V': '1',  # C2V
                     'C3':  '10'}  # C3

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02'} #MH 

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'nevents':int(nevents)}

#---------------------------------------------------------------------------------------------------
# Generating non-resonant VBF-Only HH process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
import model HHVBF_UFO-5FSwithYukawa
generate p p > h h j j $$ z w+ w- / a j QED=4
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for CV, C2V, C3
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=False)


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description    = "Non-resonant LO di-Higgs production through vector-boson-fusion (VBF) which decays to 4L1tau"
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "tau", "VBF"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 10000

#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# -- #Ele + #Mu >= 1
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- #Ele + #Mu >= 2
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

# -- #Ele + #Mu >= 3
filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
filtSeq.ElecMuThreeFilter.NLeptons = 3
filtSeq.ElecMuThreeFilter.Ptcut = 7000.
filtSeq.ElecMuThreeFilter.Etacut = 2.8

# #Tau >= 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("HadTauOneFilter")
filtSeq.HadTauOneFilter.UseNewOptions = True
filtSeq.HadTauOneFilter.UseMaxNTaus = True
filtSeq.HadTauOneFilter.Nhadtaus = 1
filtSeq.HadTauOneFilter.Ptcuthad = 15000.
filtSeq.HadTauOneFilter.EtaMaxhad = 2.8
filtSeq.HadTauOneFilter.Ptcutlep_lead = 0.

# #Tau >= 2
filtSeq += TauFilter("HadTauTwoFilter")
filtSeq.HadTauTwoFilter.UseNewOptions = True
filtSeq.HadTauTwoFilter.UseMaxNTaus = True
filtSeq.HadTauTwoFilter.Nhadtaus = 2
filtSeq.HadTauTwoFilter.Ptcuthad = 15000.
filtSeq.HadTauTwoFilter.EtaMaxhad = 2.8
filtSeq.HadTauTwoFilter.Ptcutlep_lead = 0.

# #Tau >= 3
filtSeq += TauFilter("HadTauThreeFilter")
filtSeq.HadTauThreeFilter.UseNewOptions = True
filtSeq.HadTauThreeFilter.UseMaxNTaus = True
filtSeq.HadTauThreeFilter.Nhadtaus = 3
filtSeq.HadTauThreeFilter.Ptcuthad = 15000.
filtSeq.HadTauThreeFilter.EtaMaxhad = 2.8
filtSeq.HadTauThreeFilter.Ptcutlep_lead = 0.

# #Tau >= 4
filtSeq += TauFilter("HadTauFourFilter")
filtSeq.HadTauFourFilter.UseNewOptions = True
filtSeq.HadTauFourFilter.UseMaxNTaus = True
filtSeq.HadTauFourFilter.Nhadtaus = 4
filtSeq.HadTauFourFilter.Ptcuthad = 15000.
filtSeq.HadTauFourFilter.EtaMaxhad = 2.8
filtSeq.HadTauFourFilter.Ptcutlep_lead = 0.

# -- Requirement 1: (#Ele + #Mu >= 3) and (#Tau >= 1)
#                   or (#Ele + #Mu == 2) and (#Tau >= 2)
#                   (neglact #Ele + #Mu == 1, since it will be included by 1l3tau)
#                   or (#Ele + #Mu == 0) and (#Tau >= 4)
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="( (ElecMuThreeFilter and HadTauOneFilter) " +\
                    "or (ElecMuTwoFilter and not ElecMuThreeFilter and HadTauTwoFilter) " +\
                    "or (not ElecMuOneFilter and HadTauFourFilter) )" +\
                    "and (LeadingElecMuFilter or not ElecMuOneFilter)"

