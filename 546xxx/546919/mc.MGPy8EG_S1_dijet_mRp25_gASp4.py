mR          = 250
mDM         = 10000
gVSM        = 0.00
gASM        = 0.4
gVDM        = 0.00
gADM        = 1.00
xptj        = 10
filteff     = 1
quark_decays= ['j']

evgenConfig.nEventsPerJob = 10000

include("MadGraphControl_MGPy8EG_DMS1_dijet.py")

evgenConfig.description = "Zprime - mR250 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Chris Delitzsch <chris.malena.delitzsch>, Karol Krizka <kkrizka@cern.ch>"]
