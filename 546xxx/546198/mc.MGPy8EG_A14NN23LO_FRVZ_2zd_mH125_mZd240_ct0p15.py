params={}
params['mZDinput']  = 2.400000e-01
params['MHSinput']  = 2.000000e+00
params['epsilon']   = 1.000000e-03
params['gXmu']      = 1.015000e-00
params['gXe']       = 1.020000e-00
params['gXpi']      = 0.000000e+00
params['MChi1']     = 2.000000e+00
params['MChi2']     = 5.000000e+00
params['WZp']       = "DECAY 3000001 1.000000e-03 # WZp"
params['mH']        = 125
params['nGamma']    = 2
params['decayMode'] = 'normal'
params['avgtau'] = 0.15




                

include ("MadGraphControl_A14N23LO_FRVZdisplaced_ggF.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    

