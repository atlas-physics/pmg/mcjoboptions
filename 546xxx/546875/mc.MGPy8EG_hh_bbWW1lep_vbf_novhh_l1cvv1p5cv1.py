import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------                                                     
safefactor=5.1
nevents = safefactor*runArgs.maxEvents if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1',  # CV
                     'C2V': '1.5',  # C2V
                     'C3':  '1'}  # C3

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'nevents':int(nevents)}

#---------------------------------------------------------------------------------------------------
# Generating non-resonant VBF-Only HH process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~                                                                                                                                                                  
import model HHVBF_UFO-5FSwithYukawa
generate p p > h h j j $$ z w+ w- / a j QED=4
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for CV, C2V, C3
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=False)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators  = ["MadGraph", "Pythia8"]
evgenConfig.description = "Non-resonant LO di-Higgs production through vector-boson-fusion (VBF) which decays to bbWW in the 1-lep final state."
evgenConfig.keywords    = ["hh","SM", "SMHiggs", "nonResonant", "VBF", "WWbb", "bottom"]
evgenConfig.contact     = ['Valerio D\'Amico <Valerio.Damico@cern.ch>']

evgenConfig.nEventsPerJob = 10000
#evgenConfig.tune = "MMHT2014"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 24 -24 ", # WW decay
                             "24:mMin = 0", # W minimum mass
                             "24:mMax = 99999", # W maximum mass
                             "23:mMin = 0", # Z minimum mass
                             "23:mMax = 99999", # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
# Use ParentTwoChildren filter to require:
#   Higgs(25) -> b(5)bbar(-5) AND
#   Higgs(25) -> Wplus(24)Wminus(-24)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentTwoChildrenFilter
filtSeq += ParentTwoChildrenFilter("HiggsToBBWWFilter")
filtSeq.HiggsToBBWWFilter.PDGParent = [25]
filtSeq.HiggsToBBWWFilter.PDGChild = [5,24]

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
filtSeq += DecaysFinalStateFilter("SingleLepFilter", PDGAllowedParents = [ -24, 24 ], NChargedLeptons = 1)

filtSeq.Expression = "HiggsToBBWWFilter and SingleLepFilter"
