import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os, math

#---------------------------------------------------------------------------
# general settings
#---------------------------------------------------------------------------
gridpack_mode = True

#---------------------------------------------------------------------------
# process
# njets: given in the top JO
#---------------------------------------------------------------------------
nJetMax = 2
if math.fabs(njets) > nJetMax:
    raise RuntimeError('Exceeding nJetMax=', nJetMax)

proc_str = ''
prod_str = ''  # for the production
decay_str = 'z > l+ l-' # for the decay, unordered
# jets
jets = ''
if njets >= 0:
    for ij in range(njets): jets += ' j'

if polStat == 'TT':
    prod_str = 'z{T} z{T}'
elif polStat == 'TL' or polStat == 'LT':
    prod_str = 'z{T} z{0}'
elif polStat == 'LL':
    prod_str = 'z{0} z{0}'
elif polStat == 'X0':
    prod_str = 'z z{0}'
else:
    prod_str = 'z z'
    decay_str = 'z > l+ l-, z > l+ l-'

if isZZjj: # ZZjj EW
    proc_str = 'generate p p > %s j j QCD=0 QED=4, %s' % (prod_str, decay_str)
else:
    if njets >= 0:
        proc_str = 'generate p p > %s %s, %s' % (prod_str, jets, decay_str)
    else:
        proc_str = 'generate p p > %s, %s @0\n' % (prod_str, decay_str)
        if math.fabs(njets) > 0:
            proc_str += 'add process p p > %s j, %s @1\n' % (prod_str, decay_str)
        if math.fabs(njets) > 1:
            proc_str += 'add process p p > %s j j, %s @2' % (prod_str, decay_str)

if not is_gen_from_gridpack():
    process = '''
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+
    define l- = e- mu-
    %s
    output -f
    ''' % proc_str

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError('No center of mass energy found.')


#----------------------------------------------------------------------------
# random seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#---------------------------------------------------------------------------
# number of events
#---------------------------------------------------------------------------
safefactor = 1.6
# accounting for CKKW merging efficiency
#if njets==0: safefactor = 1.5
#if njets==1: safefactor = 1.9
# safe factor applied to nevents, to account for the filter efficiency
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob
nevents = int(nevents)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events = runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 run Card
#---------------------------------------------------------------------------
ktdurham = 30

extras = {
    'me_frame'     : '3,4,5,6', ## 4l rest frame
    'asrwgtflavor' : '5',
    'lhe_version'  : '3.0',
    'ptj'          : '0',
    'ptb'          : '0',
    'pta'          : '0',
    'ptl'          : '3',
    'etaj'         : '-1',
    'etab'         : '-1',
    'etal'         : '3.0',
    'drjj'         : '0',
    'draa'         : '0',
    'draj'         : '0',
    'drjl'         : '0',
    'dral'         : '0',
    'drll'         : '0.04',
    'maxjetflavor' : '5' ,
    'cut_decays'   : 'T',
    'auto_ptj_mjj' : 'F',
    'nevents'      : nevents,
}
# for CKKW merging
if not isZZjj:
    extras.update( {
        'ptj'         : ktdurham,
        'ptb'         : ktdurham,
        'ktdurham'    : ktdurham,
        'ickkw'       : '0',
        'dparameter'  : '0.4',
        'xqcut'       : '0',
        }
    )
# for EW ZZjj
if isZZjj:
    extras.update( {
        'ptj'  : '15.',
        'ptb'  : '15.',
        'mmjj' : '10',
        'mmbb' : '10',
        }
    )

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# set up batch jobs
if gridpack_mode and (not is_gen_from_gridpack()):
    modify_config_card(process_dir=process_dir)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

# params is a dictionary of dictionaries (each dictionary is a separate block)
# params={}
# modify_param_card(process_dir=process_dir,params=params)

#---------------------------------------------------------------------------
# MG5 process (lhe) generation
#---------------------------------------------------------------------------

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# helper for resetting process number
check_reset_proc_number(opts)


#---------------------------------------------------------------------------
# shower
#---------------------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

#--------------------------------------------------------------
# evgen
#--------------------------------------------------------------
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.description = 'ZZ polarization study'
evgenConfig.keywords    = ['SM','ZZ']
evgenConfig.process     = 'p p > z z'
evgenConfig.contact     = ['xingyu.wu@cern.ch, lailin.xu@cern.ch']

#---------------------------------------------------------------------------------------------------
# CKKW-L jet matching
#---------------------------------------------------------------------------------------------------
if not isZZjj:
    PYTHIA8_nJetMax      = nJetMax
    PYTHIA8_Dparameter   = float(extras['dparameter'])
    PYTHIA8_Process      = 'pp>e+e-mu+mu-'
    PYTHIA8_TMS          = float(extras['ktdurham'])
    PYTHIA8_nQuarksMerge = int(extras['maxjetflavor'])

    include('Pythia8_i/Pythia8_CKKWL_kTMerge.py')

    # to fix the CKKWL acceptance issue
    genSeq.Pythia8.CKKWLAcceptance = False

# dipole shower
if isZZjj:
    genSeq.Pythia8.Commands += [
        'SpaceShower:dipoleRecoil = on']

# to fix cross section disagreement
genSeq.Pythia8.useRndmGenSvc = False
