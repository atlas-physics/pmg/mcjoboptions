from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
import fileinput


# process options from name
JOName = get_physics_short()
splitConfig = JOName.split('_')

top_decay_option = splitConfig[-1]
flavour_combo = splitConfig[-2]
op_process = splitConfig[-3]

lepton_dict = {
    'ee' : 'e+ e-', 
    'mumu' : 'mu+ mu-', 
    'tautau' : 'ta+ ta-', 
}

up_type_quark = flavour_combo[0]
lep_flavour = flavour_combo[2:]

if top_decay_option == 'had': 
	top_decay_string = 'decay t > w+ b, w+ > j j'
	anti_top_decay_string = 'decay t~ > w- b~, w- > j j'
elif top_decay_option == 'lep': 
	top_decay_string = 'decay t > w+ b, w+ > l+ vl'
	anti_top_decay_string = 'decay t~ > w- b~, w- > l- vl~'
elif top_decay_option == 'incl': 
	top_decay_string = 'decay t > w+ b'
	anti_top_decay_string = 'decay t~ > w- b~'


nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

process = '''
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/Contact/TopFCNC4f-{op}_{flav}
set acknowledged_v3.1_syntax True --no_save
generate p p > t {ll} $$ t~ QCD=1 QED=0 NP=2 [QCD]
add process p p > t~ {ll} $$ t QCD=1 QED=0 NP=2 [QCD]
output -f
'''.format(op=op_process, flav=flavour_combo, ll=lepton_dict[lep_flavour])

process_dir = new_process(process)

# Write the run_card
settings = {
    'nevents':        nevents,
    'parton_shower':  "PYTHIA8",
    'bwcutoff': 50,
    'dynamical_scale_choice' : 3
}



modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

set_top_params(process_dir,mTop=172.5,FourFS=False)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed {seed}
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
set BW_cut 50                 # cut on how far the particle can be off-shell
set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
{t_decay}
{anti_t_decay}
# running the actual code
launch""".format(seed=runArgs.randomSeed, t_decay=top_decay_string, anti_t_decay=anti_top_decay_string))
mscard.close()


# Print and generate
print_cards()
generate(process_dir=process_dir,
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=3)


#### Shower: Py8 with A14 Tune and new MEC
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


# Metadata
evgenConfig.description      = 'aMcAtNlo tll and shower with Pythia'
evgenConfig.keywords        += ['FCNC','top']
evgenConfig.contact          = ['shayma.wahdan@cern.ch', 'dominic.hirschbuehl@cern.ch', 'dominic.matthew.jones@cern.ch']
evgenConfig.generators       = ['aMcAtNlo','Pythia8']
