#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "ggZZ polarization"
evgenConfig.keywords = ["SM","ZZ"]
evgenConfig.contact = [ 'mingyi.liu@cern.ch']
evgenConfig.generators = [ 'MadGraph', 'Pythia8', 'EvtGen' ]
evgenConfig.nEventsPerJob=1000

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# General settings
safefactor = 1.1
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob

gridpack_mode=True
gridpack_dir='madevent/'

if not is_gen_from_gridpack():
    process = """
    import model SM_Loop_ZPolar_NLO
    define p = u c s d b u~ c~ s~ d~ b~ g
    define j = u c s d b u~ c~ s~ d~ b~ g
    define l+ = e+ mu+
    define l- = e- mu-
    generate g g > e+ e- mu+ mu- QED=4 QCD=2 [noborn=QCD] / a z z0 za @1
    add process g g > mu+ mu- mu+ mu- QED=4 QCD=2 [noborn=QCD] / a z z0 za @2
    add process g g > e+ e- e+ e- QED=4 QCD=2 [noborn=QCD] / a z z0 za @3
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

settings = {
            'me_frame' : '3,4,5,6',
            'dynamical_scale_choice' : 3,
            'lhe_version' : '3.0',
            'cut_decays' : 'True',
            'bwcutoff' : 10000000,
            'ptj' : 30,
            'ptb' : 30,
            'pta' : 0,
            'ptl' : 0,
            'etaj' : 6.5,
            'etab' : 6.5,
            'etal' : 3,
            'drjj' : 0,
            'drll' : 0.04,
            'draa' : 0,
            'draj' : 0,
            'drjl' : 0,
            'dral' : 0,
            'mmll' : 45,
            'mmnl' : 130,
            'maxjetflavor' : 5,
            'xqcut' : 0,
            'ickkw' : '0',
            'auto_ptj_mjj' : 'False',
            'event_norm' : 'sum',
            'nevents' : int(nevents)}

settings.update( {
    'dparameter'  : "0.4",
    }
)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

if gridpack_mode and (not is_gen_from_gridpack()):
  modify_config_card(process_dir=process_dir,settings={'cluster_type': "condor",'cluster_queue': None})

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)



# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

with open("pdgid_extras.txt","w") as pdgid_extras_file:
    pdgid_extras_file.write("230\n")
    pdgid_extras_file.write("231")

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.useRndmGenSvc = False
