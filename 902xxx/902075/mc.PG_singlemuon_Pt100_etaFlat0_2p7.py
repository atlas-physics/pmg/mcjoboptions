evgenConfig.description = "Single muon with fixed pT = 100 GeV, flat eta (between -2.7 and 2.7), and flat phi"
evgenConfig.keywords = ["singleParticle", "muon"]
evgenConfig.contact = ["guglielmo.frattari@cern.ch"] 
evgenConfig.generators = ["ParticleGun"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 

genSeq.ParticleGun.sampler.pid = {13, -13}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=100000.0, eta=[-2.7, 2.7])
