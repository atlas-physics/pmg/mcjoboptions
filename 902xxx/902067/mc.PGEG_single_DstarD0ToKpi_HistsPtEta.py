evgenConfig.description = "Single particle gun using 2D histogramsin pT and eta"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact  = [ "mdshapiro@lbl.gov"]
 
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt','2022inclusive_BELLE.dec']
evgenConfig.auxfiles += ['DplusEtaVsPt.root']
evgenConfig.auxfiles += [ 'DstarToD0AndD0ToK3pi.dec']

include("ParticleGun/ParticleGun_Common.py")
include("PtAndEtaHistsParticleSampler.py") 

pid={413,-413}
mass= 2010.2200

histfilePtEta="DplusEtaVsPt.root"
histnamePtEta='DplusEtaVsPt' 

import os
os.system("get_files DplusEtaVsPt.root")

seed = runArgs.randomSeed

# For some reason, the file is not fetched from the JO area without this...
import ROOT
import ParticleGun as PG
#
# ParticleGun does not set the seed for the Root random  engine, so we need to do it here
ROOT.gRandom.SetSeed(seed)

genSeq.ParticleGun.sampler = PtAndEtaHistsParticleSampler(pid=pid,mass=mass,histfilePtEta=histfilePtEta,hPtEtaName=histnamePtEta)

include("EvtGen_i/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.userDecayFile = "DstarToD0AndD0ToK3pi.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

