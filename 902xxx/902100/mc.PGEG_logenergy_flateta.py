evgenConfig.description = "Single particle for calorimeter studies"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["andriish@mpp.mpg.de"] 
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")
genSeq.ParticleGun.sampler.pid =(-11,11,-13,13,-15,15,22,-211,211,111,130,-321,321,2112,-2212,2212)
genSeq.ParticleGun.sampler.mom =PG.EEtaMPhiSampler(energy=PG.LogSampler(2000, 2000000),eta=[-6,6])


include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

genSeq.EvtInclusiveDecay.prohibitFinalStateDecay = False
genSeq.EvtInclusiveDecay.prohibitReDecay = False
genSeq.EvtInclusiveDecay.prohibitRemoveSelfDecay = True

#Debug flags, produce a lot of outputs in the logs
genSeq.EvtInclusiveDecay.printHepMCBeforeEvtGen = False
genSeq.EvtInclusiveDecay.printHepMCAfterEvtGen = False

#Particles to decay
genSeq.EvtInclusiveDecay.whiteList += [ -15, 15, 111 ]



