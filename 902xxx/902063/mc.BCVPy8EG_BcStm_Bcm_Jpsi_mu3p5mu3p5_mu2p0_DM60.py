evgenConfig.description = "Bc*- -> Bc- -> J/psi(mumu) mu- nu_mu sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2_BcStarDM60.py")

genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.EvtInclusiveDecay.userDecayFile = "BcStm_Bcm_Jpsi_mu_inclusive.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"
evgenConfig.auxfiles += [genSeq.EvtInclusiveDecay.pdtFile,genSeq.EvtInclusiveDecay.userDecayFile]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter     
filtSeq += MultiMuonFilter("TwoMuonFilter")
filtSeq += MultiMuonFilter("ThreeMuonFilter")

TwoMuonFilter = filtSeq.TwoMuonFilter
TwoMuonFilter.Ptcut = 3500.
TwoMuonFilter.Etacut = 2.7
TwoMuonFilter.NMuons = 2

ThreeMuonFilter = filtSeq.ThreeMuonFilter
ThreeMuonFilter.Ptcut = 2000.
ThreeMuonFilter.Etacut = 2.7
ThreeMuonFilter.NMuons = 3

