evgenConfig.description = "Single egamma particles (electrons) with fixed ET for MVA calibration"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact  = ["diallo.boye@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# Set flags before including ParticleGun if needed for geometry
from AthenaConfiguration.AllConfigFlags import initConfigFlags
flags = initConfigFlags()
flags.GeoModel.AtlasVersion = "ATLAS-P2-RUN4-03-01-00"
flags.lock()

# Include the ParticleGun configuration
include("ParticleGun/ParticleGun_Common.py")

# Access the ParticleGun algorithm and configure it
genSeq.ParticleGun.sampler.pid = (-11, 11)  # Electron and positron
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=50000, eta=[-2.5, 2.5], phi=[-3.14159, 3.14159], mass=PG.MASSES[11])
