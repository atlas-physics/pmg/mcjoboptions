evgenConfig.description = "Single pion +/- with flat pT (between 1. and 100. GeV), flat eta (between 0.1 and 0.3), flat phi (between -pi and pi), flat z0 (between -150 mm and 150 mm) and flat d0 (between -2 mm and 2 mm)"
evgenConfig.keywords = ["singleParticle","pi+","pi-"]
evgenConfig.contact = ["francesco.castiglioni@pi.infn.it"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]

class MyParticleSampler(PG.ParticleSampler):
    """
    A special sampler to generate single particles flat in pT and in
    impact parameter to the beam, with flat z0 between -150 and +150 mm, flat also in d0 between -2 mm and +2 mm.
    """

    def __init__(self, nParticlesPerEvent=1):
        psamp = PG.PtEtaMPhiSampler(pt=PG.UniformSampler(1000.,100000.),eta=[0.1,0.3],phi=[-PG.PI,PG.PI])
        xsamp = PG.PosSampler(0, 0, [-150,150], 0)
        PG.ParticleSampler.__init__(self, pid={211,-211}, mom=psamp, pos=xsamp)
        self.ip = PG.mksampler([-2,2])
        self.nParticlesPerEvent = nParticlesPerEvent

    def shoot(self):
        "Return a vector of sampled particles"
        output = []
        for i in range(self.nParticlesPerEvent):
            ps = PG.ParticleSampler.shoot(self)
            assert len(ps) == 1
            p = ps[0]
            from math import sqrt
            m = -p.mom.X() / p.mom.Y() #< gradient of azimuthal IP sampling line, perp to mom
            x = self.ip() / sqrt(1 + m**2) #< just decomposing sampled IP into x component...
            y = m*x #< ... and y-component
            p.pos.SetX(x)
            p.pos.SetY(m*x)
            output.append(p)
        return output

genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]
genSeq.ParticleGun.sampler = MyParticleSampler()
