evgenConfig.description = "Single pi with fixed pT = 100 GeV, flat eta (between -2.5 and 2.5), and flat phi"
evgenConfig.keywords = ["singleParticle", "pi+", "pi-"]
evgenConfig.contact = ["guglielmo.frattari@cern.ch"] 
evgenConfig.generators = ["ParticleGun"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 

genSeq.ParticleGun.sampler.pid = {211, -211}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=100000.0, eta=[-2.5, 2.5])
