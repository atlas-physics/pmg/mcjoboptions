evgenConfig.description = "Single pi0 with E in [2.0-5.0] GeV, flat eta (between -4.3 and 4.3), and flat phi"
evgenConfig.keywords = ["singleParticle", "pi0"]
evgenConfig.contact = ["lderamo@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 
evgenConfig.generators += ["ParticleGun"]
       
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=[2000,5000], eta=[-4.3, 4.3])
