evgenConfig.description = "Single electron with fixed pT = 1 GeV, flat eta (between -2.5 and 2.5), and flat phi"
evgenConfig.keywords = ["singleParticle", "electron"]
evgenConfig.contact = ["guglielmo.frattari@cern.ch"] 
evgenConfig.generators = ["ParticleGun"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 

genSeq.ParticleGun.sampler.pid = {11, -11}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=1000.0, eta=[-2.5, 2.5])
