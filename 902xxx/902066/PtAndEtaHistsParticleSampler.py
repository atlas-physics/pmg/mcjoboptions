#! -*- python -*-

# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
# author: Marjorie Shapiro
## ROOT histogram sampling alg with two hists (in ParticleGun.histsampling) 
## based on jobOption.ParticleGun_corrhist.py


import ParticleGun as PG
import ROOT, math, random
import ctypes

class PtAndEtaHistsParticleSampler(PG.ParticleSampler):
    "Particle sampler withpT and eta from a 1D histograms."

    def __init__(self, pid, mass, histfilePtEta, hPtEtaName, num=1):
        self.pid = PG.mksampler(pid)
        print("PtAndEtaHistsParticleSampler using 2D histogram ", hPtEtaName,'from file ',histfilePtEta)
        myfile = ROOT.TFile.Open(histfilePtEta)
        self.histPtEta = myfile.Get(hPtEtaName)
        self.histPtEta.SetDirectory(0)
        myfile.Close()
        self.numparticles = num

    def shoot(self):
        "Return a vector of sampled particles from the provided pT--eta histogram"
        particles = []
        ptrand = ctypes.c_double(0)
        etarand = ctypes.c_double(0)
        for i in range(self.numparticles):
            self.histPtEta.GetRandom2(ptrand, etarand)
            eta = etarand.value
            pt = 1000*ptrand.value # NB. This _particular_ histogram is in GeV, but Athena needs MeV!
            pid = self.pid()
            mom = PG.PtEtaMPhiSampler(pt=pt, eta=eta, mass=mass)
            p = PG.SampledParticle(pid, mom())
            particles.append(p)
        return particles
