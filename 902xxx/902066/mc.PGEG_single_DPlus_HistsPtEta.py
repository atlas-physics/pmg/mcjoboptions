evgenConfig.description = "Single particle gun using 3D histogram in pT and eta"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact  = [ "mdshapiro@lbl.gov"]
 
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt','2022inclusive_BELLE.dec']
evgenConfig.auxfiles += ['DplusEtaVsPt.root']
evgenConfig.auxfiles += [ 'DplusAndMinusToKpipi.dec']

include("ParticleGun/ParticleGun_Common.py")
include("PtAndEtaHistsParticleSampler.py") 

# For some reason, the file is not fetched from the JO area without this.
import os
os.system("get_files DplusEtaVsPt.root")


pid={411,-411}
mass=1869.6300

histfilePtEta="DplusEtaVsPt.root"
histnamePtEta='DplusEtaVsPt' 

seed = runArgs.randomSeed

import ROOT
import ParticleGun as PG
#
# ParticleGun does not set the seed for the Root random  engine, so we need to do it here
ROOT.gRandom.SetSeed(seed)

genSeq.ParticleGun.sampler = PtAndEtaHistsParticleSampler(pid=pid,mass=mass,histfilePtEta=histfilePtEta,hPtEtaName=histnamePtEta)

include("EvtGen_i/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.userDecayFile = "DplusAndMinusToKpipi.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

