evgenConfig.description = "Single muon +/- with pt of 100 GeV, flat eta (between 2.3 and 4.1), and flat phi"
evgenConfig.keywords = ["singleParticle", "muon"]
evgenConfig.contact = ["lderamo@cern.ch"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]

genSeq.ParticleGun.sampler.pid = {-13,13}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=100000, eta=[[-4.1, -2.3], [2.3, 4.1]])
