evgenConfig.description = "Low-pT EPOS inelastic minimum bias events for pileup, using low-pT jet and photon filters. Broad decay widths for resonances."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 50000
evgenConfig.tune = "EPOS LHC"

evgenConfig.saveJets = True
evgenConfig.savePileupTruthParticles = True

include("Epos_i/Epos_Base_Fragment.py")

include ("GeneratorFilters/AddPileupTruthParticles.py")
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
CreateJets(prefiltSeq, 0.6)

AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 8000. ]
filtSeq.DirectPhotonFilter.Etacut = 4.5

filtSeq.Expression = 'not DirectPhotonFilter and QCDTruthJetFilter'

if hasattr(testSeq, "TestHepMC"):
      testSeq.TestHepMC.EnergyDifference = 5000.0

