import os, sys, glob, math

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Wtaunu+0,1,2,3j NLO FxFx HT2-biased, CVetoBVeto, forced leptonic tau decays'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['W','jets','tau']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 30.2%
# CVetoBVeto - eff ~71.5%
# one LHE file contains 55000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

# Hack to restore the W->lnu resonance in the LHE files 
# this allows Pythia8 internal tau decays to handle polarisation
# these will check presence of (24)->(-15)+(16) or (-24)->(15)+(-16)
# if the +-24 particles are not present,
# a new line of an intermediate particle will be added that corresponds to the four-vector sum
restoreResonances = [[-15, 16, 24], [15, -16, -24]]

for infile in glob.glob("*.events"):
    print ("Patching in the resonances", restoreResonances, "into file", infile)

    f1 = open(infile)
    newfile = infile+'.temp'
    f2 = open(newfile,'w')

    startEvent = False
    nParticles = -1
    eventBuffer = []
    
    for line in f1:
        if line.startswith('<event'):
            # flag new event, write out line, go to next line
            startEvent = True
            f2.write(line)
            continue

        if startEvent:
            # start buffering the event, record number of particles, go to next line
            nParticles = float(line.split()[0])
            startEvent = False
            eventBuffer += [line]
            continue

        if nParticles > 0:
            # continue buffering the event, go to next line
            nParticles -= 1
            eventBuffer += [line]
            continue

        if nParticles == 0:
            # we're finish buffering the event, process event and write buffer

            for myRes in restoreResonances:

                imatch = 0
                for l in eventBuffer:
                    if int(l.split()[0]) in myRes:
                        imatch += 1

                if imatch == len(myRes):
                    #print 'Got full resonance', myRes, ' - all ok'
                    break
                elif imatch > 0:
                    #print 'Got partial resonance', myRes, ' - will patch'

                    # event info line patched to have one extra particle in record
                    eventInfo = eventBuffer[0]
                    NUP = int(eventInfo.split()[0])
                    eventInfo = ("%2i" % (NUP+1))+eventInfo[2:]
                    eventBuffer[0] = eventInfo

                    # construct the resonance summing all relevant particles and adding it after the status -1 particles
                    ires = 1
                    pxsum = 0.
                    pysum = 0.
                    pzsum = 0.
                    Esum = 0.
                    iline = -1

                    for l in eventBuffer:
                        lsplit = l.split()
                        iline += 1
                        if int(lsplit[1]) == -1:
                            # count lines before adding the resonance
                            ires += 1
                        elif int(lsplit[0]) in myRes:
                            # add up decay product four-vectors
                            # patch the decay products to originate from resonance
                            pxsum += float(lsplit[6])
                            pysum += float(lsplit[7])
                            pzsum += float(lsplit[8])
                            Esum += float(lsplit[9])
                            pLine = "%9s%3i%5i%5i%5i%5i %s\n" \
                                    % (lsplit[0], int(lsplit[1]), ires, ires, int(lsplit[4]), int(lsplit[5]), ' '.join(lsplit[6:]))
                            eventBuffer[iline] = pLine

                    # add the resonance line
                    Msum = math.sqrt(Esum**2-pxsum**2-pysum**2-pzsum**2)
                    resonance = "%9i  2    1    2    0    0 %+.10e %+.10e %+.10e %.10e %.10e 0.0000e+00 0.0000e+00\n"\
                                % (myRes[-1], pxsum, pysum, pzsum, Esum, Msum)
                    eventBuffer.insert(ires, resonance)
                    break

            # done patching the event (if needed) - write it out
            f2.write(''.join(eventBuffer))

            # clear event buffer
            eventBuffer = []
            nParticles = -1
            # still need to write the last line
            f2.write(line)
            continue

        # this will write any other lines
        f2.write(line)


    f1.close()
    f2.close()
    os.system('rm %s' % (infile) )
    os.system('mv %s %s' % (newfile, infile) )


# Shower/merging settings
parton_shower='PYTHIA8'
nJetMax=3
qCut=20.

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

# force leptonic tau decays
genSeq.Pythia8.Commands += [
    "15:onMode = off",
    "15:onIfAny = 11 13"
]

# CVetoBVeto
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter 

include("GeneratorFilters/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter

filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"
