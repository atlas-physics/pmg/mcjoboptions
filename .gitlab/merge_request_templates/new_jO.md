Make sure that all the items in the checklists are duly followed:  
follow the instructions of each checklist item and report potential issues to the repository mainteners.

## JIRA ticket

<!-- Fill the line below with the JIRA ticket number, e.g. ATLMCPROD-1234 -->

ATLMCPROD-**{\*PLEASE FILL THIS\*}**

## Checklist for approval

This checklist should be normally filled by MC contacts (but it can also be done by the person opening the merge request).   
If something is unclear get in touch with the repository maintainers.

- [ ] I have followed the [guidelines in McSampleRequestProcedure](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McSampleRequestProcedure)
- [ ] I have provided the ATLMCPROD JIRA ticket where the request has been discussed
- [ ] The request has been approved by the PA/CP group
- [ ] The pipeline has run and the status is green (still check the CI job output - if you see any errors report to the repository maintainers)
- [ ] If the pipeline is orange: check the CI job output to sort out the issues. If the issue is unclear contact the repository maintainers
- [ ] CI jobs have not been skipped (if jobs have been skipped, the requesters should provide a reason for skipping and confirmation from the PMG conveners is necessary before merging)
- [ ] Check that no `log.generate.short` files are included in the commit to be merged to master. If such files are present, it indicates that something went wrong in the pipeline. Check the job logs and contact the package maintainers if needed.
- [ ] Check that the title of the MR is descriptive enough (if not ask the requester to modify it)
- [ ] Check that the DSID range assigned is correct (500000-999999) and corresponds to [right generator block](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware#DSID_blocks)
- [ ] Check that the ATLMCPROD link provided in "JIRA ticket" field above works and that a working link pointing to this MR is posted on the corresponding JIRA page. If not contact the package maintainers.

/label ~jobOptions
/label ~"Review required"