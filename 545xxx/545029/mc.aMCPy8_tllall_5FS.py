from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *



nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

process = '''
import model loop_qcd_qed_sm
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
set nlo_mixed_expansion False
generate p p > t j l+ l- $$ h [QCD] 
output -f
'''


process_dir = new_process(process)

# Write the run_card
settings = {
    'nevents':        nevents,
    'parton_shower':  "PYTHIA8",
    'bwcutoff':50,
    'mll_sf':         5,
    'fixed_ren_scale' : True,
    'fixed_fac_scale' : True,
    'muR_ref_fixed' : 172.5,
    'muF_ref_fixed' : 172.5
}


modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

set_top_params(process_dir,mTop=172.5,FourFS=False)

# Print and generate
print_cards()
generate(process_dir=process_dir,
         grid_pack=False,
         gridpack_compile=False,
         required_accuracy=0.005,
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=3)

#### Shower: Py8 with A14 Tune and new MEC
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


# Metadata
evgenConfig.description      = 'aMcAtNlo tllq in the 5FS using HT/6 scale and shower with Pythia with MEC in the decay'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['dominic.hirschbuehl@cern.ch']
evgenConfig.generators       = ['aMcAtNlo']


