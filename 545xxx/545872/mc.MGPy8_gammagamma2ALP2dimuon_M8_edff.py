#import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

 #remove TestHepMC because this configuration is an "unconventional" usage of Madgraph
if hasattr(testSeq, "TestHepMC"): testSeq.remove(TestHepMC())

JOName = get_physics_short()

massstr = JOName.split("_")[2]
mass = float(massstr.strip('M').replace('p', '.'))


safety_factor = 1.1
nevents = int(safety_factor*runArgs.maxEvents)

# beam energy = sqrt(s_NN) / 2 * 208 (GeV)
energy = str ( runArgs.ecmEnergy / 2 * 208  )
#Defaults for run_card.dat
extras = {  'lpp1':'2',     
            'lpp2':'2',
            'ebeam1' : energy ,  # beam 1 energy
            'ebeam2' : energy ,  # beam 2 energy
            'pdlabel': 'edff',
            'nb_proton1' : '82' ,  # 208 Pb 82
            'nb_proton2' : '82' ,  # 208 Pb 82
            'nb_neutron1' : '126' ,  # 208 Pb 82
            'nb_neutron2' : '126' ,  # 208 Pb 82
            'ptl' : '3' ,  # min pT of final state leptons
            'ptlmax': '-1' ,  # max pT of final state leptons (-1 = no cut)
            'etal' : '2.7' ,  # max abs(eta) of final state leptons
            'etalmin' : '-2.7', #! main rap for the charged leptons
            'mmll' : '6.0' ,  # min invariant mass between final state leptons
            'mmllmax' : '-1' ,  # max invariant mass between final state leptons
            'use_syst'  : 'False',
            'systematics_program' : 'None',
            'nevents':str(nevents)
           } 


# Import model
process = """
import model ALP_linear_UFO_WIDTH
generate a a > ax > mu+ mu-
output aatoaxtomumu
"""
#already done: aa>ax>mumu aa>ax>aa
# Define the process and create the run card from a template
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras);
print_cards()

# Set some values in the param card
#  Alp masses
masses={'9000005': mass #Y2
         }

# Decay Width
width = {'9000005':'Auto' #Y2
         }


# Coupling
couplings={'1':1.000000e+03,
'2':0.000000e+00,
'3':0.000000e+00,
'4':1.000000e+01,
'5':1.000000e+01}

#Yukawas
yukawas={'1':0.000000e+00,
'2':0.000000e+00,
'3':0.000000e+00,
'4':0.000000e+00,
'5':0.000000e+00,
'6':0.000000e+00,
'11':0.000000e+00,
'13':1.056600e-01,
'15':0.000000e+00}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir,params={'MASS':masses, 'DECAY':width, 'ALPPARS':couplings, 'YUKAWA':yukawas})
# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3.0)

# These details are important information about the JOs
evgenConfig.description = 'Axion production in yy->mu+mu- process' 
evgenConfig.contact = [ "Karolina Domijan <karolina.domijan@cern.ch>" ]
evgenConfig.keywords += ['2photon','2muon']
evgenConfig.generators += ["MadGraph"]

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

