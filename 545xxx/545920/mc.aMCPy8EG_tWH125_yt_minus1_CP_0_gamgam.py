# os.environ["ATHENA_PROC_NUMBER"] = "64"
evgenConfig.description = 'aMcAtNlo tHW'
evgenConfig.keywords+=['Higgs', 'tHiggs']
evgenConfig.contact = ['haoquan.ren@cern.ch', 'xingyu.wu@cern.ch']
evgenConfig.generators = [ 'aMcAtNlo', 'Pythia8', 'EvtGen' ]
evgenConfig.nEventsPerJob = 20000

from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.DiagramRemoval import do_DRX
from MadGraphControl.DiagramRemoval import do_MadSpin_DRX
import fileinput
######
## number of events to generate + safety margin
nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob
gridpack_mode=False




######

topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''

if not is_gen_from_gridpack():       
    fullproc = """
    import model HC_NLO_X0_UFO_v1p3-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b t
    define q~ = u~ c~ d~ s~ b~ t~
    set acknowledged_v3.1_syntax True --no_save
    set nlo_mixed_expansion False
    generate p p > t w- x0 QCD<=1 QED<=2 QNP<=1[QCD]
    add process p p > t~ w+ x0  QCD<=1 QED<=2 QNP<=1[QCD]
    output -f
    """   
   
    DR_mode = 1
    process_dir = new_process(fullproc)
    # do the removal for overlap with ttH process
    do_DRX(DR_mode,process_dir)

else:    
    process_dir = MADGRAPH_GRIDPACK_LOCATION
extras = {
              'parton_shower'  :'PYTHIA8',
              'nevents'        : int(nevents),
              'lhe_version'            : '3.0',
              'bwcutoff'       : 50.,
              'dynamical_scale_choice' : 10}





print("CUSTOM SCALE SETTINGS: SET TO HT/4")
dyn_scale_fact = 1.0
fileN = process_dir+'/SubProcesses/setscales.f'
mark = '      elseif(dynamical_scale_choice.eq.10.or.dynamical_scale_choice.eq.0) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 10                                   cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           '        write(*,*) "User-defined scale not set"',
           '        stop 1',
           '        temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           '        tmp = 0d0']
flag=0
for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print(line),
    if line.startswith(mark) and flag==0:
        flag +=1
        print("""
c         sum of the transverse mass divide by 4
c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
          tmp=0d0
          do i=3,nexternal
            tmp=tmp+dsqrt(max(0d0,(pp(0,i)+pp(3,i))*(pp(0,i)-pp(3,i))))
          enddo
          tmp=tmp/4d0
          temp_scale_id=\'H_T/4 := sum_i mT(i)/4, i=final state\'""")


modify_param_card(param_card_input="param_card_yt_minus1_CPalpha_0.dat",process_dir=process_dir)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
## do the removal for overlap with ttH process
madspin_card=process_dir+'/Cards/madspin_card.dat'
fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weight 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
launch''')
fMadSpinCard.close()
######
## write madspin card
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
outputDS=arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)





###### 
## shower settings:

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")



genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
]




