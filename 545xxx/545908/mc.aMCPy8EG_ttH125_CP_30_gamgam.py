# os.environ["ATHENA_PROC_NUMBER"] = "64"

evgenConfig.description = 'aMcAtNlo ttH'
evgenConfig.keywords+=['Higgs', 'tHiggs']
evgenConfig.contact = ['haoquan.ren@cern.ch', 'xingyu.wu@cern.ch' ]
evgenConfig.generators = [ 'aMcAtNlo', 'Pythia8', 'EvtGen' ]
evgenConfig.nEventsPerJob = 20000
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
######
## number of events to generate + safety margin
nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob
gridpack_mode=False
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- >  all all'''

if not is_gen_from_gridpack():    
    process ="""
    import model HC_NLO_X0_UFO_v1p3-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b t
    define q~ = u~ c~ d~ s~ b~ t~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    set acknowledged_v3.1_syntax True --no_save
    set nlo_mixed_expansion False
    generate p p > t t~ x0 QCD<=2 QED<=1 QNP<=1 [QCD]
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION
extras = {
              'parton_shower'  :'PYTHIA8',
              'nevents'        : int(nevents),
              'bwcutoff'       : 50.,
              'dynamical_scale_choice' : 3 }



modify_param_card(param_card_input="param_card_CPalpha_30.dat",process_dir=process_dir)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

madspin_card=process_dir+'/Cards/madspin_card.dat'
fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weight 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

### common to all job option
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)




include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")



genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
]

