import os, shutil
from MadGraphControl.MadGraphParamHelpers import set_top_params
import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(2*1.2*runArgs.maxEvents)

process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
set acknowledged_v3.1_syntax True --no_save
generate p p > t t~ a QED=1 QCD=2 [QCD]
output -f"""

dec = """
define w+child = e+ mu+ ta+ ve vm vt u c d~ s~
define w-child = e- mu- ta- ve~ vm~ vt~ u~ c~ d s
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""

settings = {'lhe_version'	:'3.0',
			'maxjetflavor'	:5,
			'parton_shower'	:'HERWIGPP',
			'ptgmin'		:15.,
			'R0gamma'		:0.1,
			'xn'			:2,
			'epsgamma'		:0.1,
			'ptj'			:0.,
			'etal'			:-1.0,
			'etagamma'		:5.0,
			'mll_sf'		:0.,
			'dynamical_scale_choice':'3',
			'nevents'		:nevents

}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
set_top_params(process_dir,mTop=172.5,FourFS=False)
# Decay with MadSpin
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set BW_cut 50
set seed %i
%s
launch
"""%(runArgs.randomSeed, dec))
mscard.close()

# Print cards
print_cards()
# set up
generate(runArgs=runArgs, process_dir=process_dir)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

check_reset_proc_number(opts)

#--------------------------------------------------------------
# EVGEN Configuration
#--------------------------------------------------------------
keyword=['SM','top',  'photon'] #'ttgamma', ttgamma currently not in list of keywords
evgenConfig.keywords += keyword
evgenConfig.generators += ["aMcAtNlo", "Herwig7", "EvtGen"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.description = 'aMcAtNlo+H7_ttgamma_nonallhad_GamFromProd'
evgenConfig.contact = ["jan.joachim.hahn@cern.ch","arpan.ghosal@cern.ch"]

LHEfile=outputDS+".events"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=LHEfile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()
#-------------------------------------------------------------------------------------
#Event filter
#-------------------------------------------------------------------------------------
print ("LEPTON FILTER APPLICATION")
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1

