from MadGraphControl.MadGraphUtils import *
import math
import os
import re
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment

#Parse information from jobConfig
THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]     
jobname = [f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]

## Format of the jobOptions is expected to be
##mc.MGPy8EG_WprimeTbTb_{lep,had}_{LH,RH}_{MASS}_{Coupling}.py

parameters = re.split(r'[._]',jobname)

if not (len(parameters) == 8 ):
        raise RuntimeError('Format of jobOptions not recognised, number of parameters incorrect')

str_channel = parameters[3]
str_chir = parameters[4]
str_mass = parameters[5]
str_coupling = parameters[6]


## Some outputs to allow for easy debugging
print ("Parameters parsed from the jobOptions")
print ("W\' Mass:  ", str_mass) 
print ("decay channel is: ", str_channel)
print ("Coupling factor " , str(int(str_coupling) * 0.1))
print ("Chirality ", str_chir) 
print ("Will attempt to configure the job now")

gSM = (6.483972e-01)
gSF = int(str_coupling) * 0.1
MWp = int(str_mass)

nLepton = 99
if str_channel == 'lep':
    nLepton = -1
elif str_channel == 'had':
    nLepton = 0
else:
    raise RuntimeError('Channel not recognised in jobname, check the jobOptions')

#Chirality (for now we keep it)
if str_chir == 'RH':
    gR = gSM*gSF
    gL = 0.0
    gRL='gR'
elif str_chir == 'LH':
    gR = 0.0
    gL = gSM*gSF
    gRL='gL'
else:
    raise RuntimeError('Chirality not recognised in jobname, check the jobOptions')


##Full process
process="""
import model sm
import model weff_tb_NLO
set acknowledged_v3.1_syntax True --no_save
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t~ b wp+ NP<=1 QED<=0 [QCD]
add process p p > t b~ wp- NP<=1 QED<=0 [QCD]
output -f
"""

process_dir = new_process(process)
           
## Define settings for the run card
nevents = 3.0*runArgs.maxEvents
settings = {
        'parton_shower':'PYTHIA8',
        'bwcutoff':25,
        'nevents':int(nevents),
        'dynamical_scale_choice':2,
        'muR_over_ref':0.333,
        'muF1_over_ref':1,
        'muF2_over_ref':1 
	    }

params = {}
params['MASS']={'mwp': str(MWp),
                'mb': str(4.8)}
params['DECAY']={'wwp': "auto"}
params['WPCOUP']={'gL' : str(gL),'gR' : str(gR)}

####Card modifications
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                                                                    
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set BW_cut 25                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set seed %i
# specify the decay for the final state particles
decay wp+ > t b~, (t > w+ b, w+ > all all)
decay wp- > t~ b, (t~ > w- b~, w- > all all)
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

print_cards()

###########Generating and preparing for Pythia
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)  

#### Shower with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#Information Strings
evgenConfig.description = 'MadGraph NLO tbWprime->tbtb'
evgenConfig.keywords=[ 'BSM', 'Wprime', 'heavyBoson', 'resonance']
evgenConfig.contact=['jason.peter.gombas@cern.ch']

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter   # lep filter
filtSeq += TTbarWToLeptonFilter("TTbarWToLeptonFilter")

TTbarWToLeptonFilter = filtSeq.TTbarWToLeptonFilter

TTbarWToLeptonFilter.NumLeptons = nLepton
TTbarWToLeptonFilter.Ptcut = 0.

filtSeq.Expression = "(TTbarWToLeptonFilter)"
