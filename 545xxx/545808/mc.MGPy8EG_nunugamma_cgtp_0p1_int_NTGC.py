import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# General settings
nevents = runArgs.maxEvents*3 if runArgs.maxEvents>0 else 3*evgenConfig.nEventsPerJob

if not is_gen_from_gridpack():
    process="""
    import model NTGC_9_operators 
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = p
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > vl vl~ a GP==0 GM==0 BB==0 BTW==0 BW==0 WW==0 TBW==0 GPT^2==1 GMT==0
    output -f"""
    process_dir=str(new_process(process))
else:
    process_dir=str(MADGRAPH_GRIDPACK_LOCATION)

# define cuts and other run card settings
settings={
        'dynamical_scale_choice':'3', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'event_norm':'average',
        'lhe_version': '3.0',
        'cut_decays'  : 'F',
        'pta':"0",
        'etaa':'3',
        'ptgmin'      : "140",
        'epsgamma':"0.1",
        'R0gamma' :"0.1", 
        'xn'    :"2",
        'isoEM' :"True", 
        'bwcutoff'    :"15",
        'maxjetflavor': 5,
        'asrwgtflavor': "5",
        'ickkw': "0",
        'xqcut': "0",
        'ktdurham'       : -1
}
#############################################################################################

param_card_settings = {
    'DIM8' : {
        'cg'    : '0.0',
        'cgm'   : '0.0',
        'CtBWL4': '0.0',
        'CBtWL4': '0.0',
        'CBWL4' : '0.0',
        'CBBL4' : '0.0',
        'CWWL4' : '0.0',
        'CGPtL4': '0.1',
        'CGMtL4': '0.0',
    }
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=param_card_settings)

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

#############################################################################################
# add meta data
evgenConfig.description = 'MadGraph nunugamma EFT nTGC'
evgenConfig.keywords+=['SM','Z','photon','diboson']
evgenConfig.contact = ['Danning Liu <danning.liu@cern.ch>']
#############################################################################################

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



