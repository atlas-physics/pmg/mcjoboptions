evgenConfig.description      = 'aMcAtNlo tbarllq with HT/6 scale and shower with Pythia with MEC in the decay and A14 Var3cDown variation'
evgenConfig.keywords        += ['SM','tZ','top']
evgenConfig.contact          = ['dominic.hirschbuehl@cern.ch']
evgenConfig.generators       = ['aMcAtNlo','EvtGen','Pythia8']

evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000


include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
