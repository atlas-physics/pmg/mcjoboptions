### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

keepOutput = False # Debug-off

### helpers
def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

### parse job options name 
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short().split('_')
evgenLog.info("Physics short names: " + str(phys_short))
#additional_options = phys_short.split("_")[4:]
#evgenLog.info("DirectStau: Additional options: " + str(additional_options))

#C1/N2/N3 degenerate
masses['1000025'] = -1.0 * MassToFloat(phys_short[3]) # N3
masses['1000023'] = MassToFloat(phys_short[3]) # N2
masses['1000024'] = MassToFloat(phys_short[3]) # C1
#N1
masses['1000022'] = MassToFloat(phys_short[4]) # N1
mass_splitting = masses['1000024'] - masses['1000022']
if masses['1000022']<0.5: masses['1000022']=0.5

# interpret the generation type.
gentype = phys_short[1] # should be always N2N3

############################
# Updating the parameters
############################

#
# N2,N3 branching ratio
#
decaytype = phys_short[2] #e.g. ZZ,Zh,hh,Zh50

if decaytype == 'ZZ':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->ZN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
elif decaytype == 'Zh':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->hN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
elif decaytype == 'Zh50': 
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->hN1)=50%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     0.50000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.50000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     0.50000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.50000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
elif decaytype == 'hh':
  evgenLog.info('Force Br(N2->hN1)=Br(N3->hN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
elif decaytype == 'hZ':
  evgenLog.info('Just For Reco validation. Force Br(N2->hN1)=Br(N3->ZN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """

# Mixing matrix for the Higgsino NLSP / Bino LSP / decoupled Wino
# Derived from  Spheno
#  1    2.00000000E+02  # M1input
#  2    5.00000000E+03  # M2input
#  3    5.00000000E+03  # M3input
# 23    8.00000000E+02  # Muinput
# 25    1.00000000E+01  # TanBeta
# 26    5.00000000E+03  # MAinput
#
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij i(B,W,H_d,H_u)
param_blocks['NMIX']={}
param_blocks['NMIX']['1 1']=' 1.00E+00' # N_11 bino (N1)
param_blocks['NMIX']['1 2']=' 0.00E+00' # N_12
param_blocks['NMIX']['1 3']=' 0.00E+00' # N_13
param_blocks['NMIX']['1 4']=' 0.00E+00' # N_14
param_blocks['NMIX']['2 1']=' 0.00E+00' # N_21
param_blocks['NMIX']['2 2']=' 0.00E+00' # N_22 
param_blocks['NMIX']['2 3']=' 7.07E-01'  # N_23 higgsino (N2)
param_blocks['NMIX']['2 4']='-7.07E-01' # N_24 higgsino (N2)
param_blocks['NMIX']['3 1']=' 0.00E+00' # N_31
param_blocks['NMIX']['3 2']=' 0.00E+00'  # N_32 
param_blocks['NMIX']['3 3']='-7.07E-01'  # N_33 higgsino (N3)
param_blocks['NMIX']['3 4']='-7.07E-01'  # N_34 higgsino (N3)
param_blocks['NMIX']['4 1']=' 0.00E+00' # N_41
param_blocks['NMIX']['4 2']='-1.00E+00'  # N_42 wino (N4)
param_blocks['NMIX']['4 3']=' 0.00E+00'  # N_43
param_blocks['NMIX']['4 4']=' 0.00E+00' # N_44

#
# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
# https://gitlab.cern.ch/atlas/athena/-/blob/ee38d2687aa7b57481a12bf0b6abf5461dc1680f/Generators/MadGraphControl/python/MadGraphParamHelpers.py#L120
masses['25'] = 125
if ( decaytype == 'Zh' or decaytype == 'hZ' ) and 'hbb' in phys_short :
    decays['25'] = """DECAY  25   6.382339e-03
    #          BR         NDA      ID1       ID2
        1.000E+00 2   5  -5 # H->bb
    """ 
    evgenLog.info( "ControlFile: higgs decays to bb" )
else:
    decays['25'] = """DECAY  25   6.382339e-03
    #          BR         NDA      ID1       ID2
        5.767E-01 2   5  -5 # H->bb
        6.319E-02 2  15 -15 # H->tautau
        2.192E-04 2  13 -13 # H->mumu
        2.462E-04 2   3  -3 # H->ss
        2.911E-02 2   4  -4 # H->cc
        8.569E-02 2  21  21 # H->gg
        2.277E-03 2  22  22 # H->gammagamma
        1.539E-03 2  22  23 # H->Zgamma
        2.146E-01 2  24 -24 # H->WW
        2.641E-02 2  23  23 # H->ZZ
    """ 
    evgenLog.info( "ControlFile: higgs decays inclusively" )

madspindecays=False
if (decaytype == 'ZZ' or decaytype == 'Zh' or decaytype == 'Zh50' or decaytype == 'hh' or decaytype == 'hZ' ) and ('MadSpin' in jofile) :
  madspindecays = True;

print( "ControlFile: gentype ", gentype )
print( "ControlFile: decaytype ", decaytype )
print( "ControlFile: decays ", decays )
print( "ControlFile: masses ", masses )
print( "ControlFile: madspindecays ", madspindecays )

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~
generate p p > n2 n3 $ susystrong @1
add process p p > n2 n3 j $ susystrong @2
add process p p > n2 n3 j j $ susystrong @3
'''

mergeproc="{n2,1000023}{n3,1000025}"

msdecaystring=""
if madspindecays == True:
  if decaytype == 'ZZ':
    msdecaystring="decay n2 > z n1, z > f f \n decay n3 > z n1, z > f f \n "
  elif decaytype == 'Zh':
    msdecaystring="decay n2 > z n1, z > f f \n decay n3 > h01 n1 \n "
  elif decaytype == 'hZ':
    msdecaystring="decay n3 > z n1, z > f f \n decay n2 > h01 n1 \n "
  elif decaytype == 'hh':
    msdecaystring="decay n2 > h01 n1 \n decay n3 > h01 n1 \n "
  elif decaytype == 'Zh50':
    msdecaystring="decay n2 > z n1, z > f f \n decay n2 > h01 n1 \n decay n3 > z n1, z > f f \n decay n3 > h01 n1 \n "


# print the process, just to confirm we got everything right
evgenLog.info( "ControlFile: Final process card " + process )


#--------------------------------------------------------------
# Madspin configuration
#
if madspindecays==True:
  if msdecaystring=="":
    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
  madspin_card='madspin_card.dat'

  mscard = open(madspin_card,'w')

  mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 15                # default for onshell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s


# running the actual code
launch"""%(runArgs.randomSeed,msdecaystring))
  mscard.close()
  mergeproc+="LEPTONS,NEUTRINOS"

# information about this generation
evgenLog.info('Registered generation of ~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh; grid point decoded into mass point ' + str(masses['1000025']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "shion.chen@cern.ch" ]
evgenConfig.keywords += ['gaugino', 'neutralino']
evgenConfig.description = '~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh in simplified model. m_N2N3 = %s GeV, m_N1 = %s GeV'%(masses['1000025'],masses['1000022'])

#--------------------------------------------------------------
# No filter at the moment
evt_multiplier=2
if '1L3Filt' in phys_short:
    # https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/GeneratorFilters/share/common/xAODMultiElecMuTauFilter_Common.py
    # https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/GeneratorFilters/src/xAODMultiElecMuTauFilter.cxx
    evgenLog.info('ControlFile: 1 Lep filter is applied.')

    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    filtSeq.xAODMultiElecMuTauFilter.NLeptons = 1
    filtSeq.xAODMultiElecMuTauFilter.MinPt = 3000.0
    filtSeq.xAODMultiElecMuTauFilter.MaxEta = 2.8
    #filtSeq.xAODMultiElecMuTauFilter.MinVisPtHadTau = 3000.0
    filtSeq.xAODMultiElecMuTauFilter.IncludeHadTaus = False

    filtSeq.Expression = "xAODMultiElecMuTauFilter"    

    # set higher evt_multiplier when using filter
    if mass_splitting <= 200:
        evt_multiplier *= 3
    else:
        evt_multiplier *= 2
elif 'XeFilt' in phys_short:
  
    evgenLog.info('ControlFile: Xe filter is applied.')
    
    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 100000.0
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "xAODMissingEtFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier *= 5
elif '1L20orXeFilt' in phys_short:
    
    evgenLog.info('ControlFile: 1Lep OR Xe filter is applied.')

    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    f1L3Filter = xAODMultiElecMuTauFilter("f1L3Filter")
    filtSeq += f1L3Filter
    filtSeq.f1L3Filter.NLeptons = 1
    filtSeq.f1L3Filter.MinPt = 3000.0
    filtSeq.f1L3Filter.MaxEta = 2.8
    #filtSeq.f1L3Filter.MinVisPtHadTau = 3000.0
    filtSeq.f1L3Filter.IncludeHadTaus = False
    
    f1L20Filter = xAODMultiElecMuTauFilter("f1L20Filter")
    filtSeq += f1L20Filter
    filtSeq.f1L20Filter.NLeptons = 1
    filtSeq.f1L20Filter.MinPt = 20000.0
    filtSeq.f1L20Filter.MaxEta = 2.8
    #filtSeq.f1L20Filter.MinVisPtHadTau = 20000.0
    filtSeq.f1L20Filter.IncludeHadTaus = False

    #include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    #filtSeq.xAODMultiElecMuTauFilter.NLeptons = 1
    #filtSeq.xAODMultiElecMuTauFilter.MinPt = 3000.0
    #filtSeq.xAODMultiElecMuTauFilter.MaxEta = 2.8
    #filtSeq.xAODMultiElecMuTauFilter.MinVisPtHadTau = 3000
    #filtSeq.xAODMultiElecMuTauFilter.IncludeHadTaus = False
    
    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 100000.0
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "f1L3Filter and (f1L20Filter or xAODMissingEtFilter)"

    # set higher evt_multiplier when using filter
    if mass_splitting <= 200:
        evt_multiplier *= 3
    else:
        evt_multiplier *= 2
else:
    evgenLog.info('ControlFile: inclusive processes will be generated')

#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Pythia options
#
#evgenLog.info('Using Merging:Process = guess')
#genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
#genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 

#--------------------------------------------------------------
# Pythia options
# 0.2 is too small to be added, we can comment out these lines but better to keep them.
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]

#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>{n2,1000023}{n3,1000025}",
                                 "1000025:spinType = 1",
                                 "1000023:spinType = 1" ]
