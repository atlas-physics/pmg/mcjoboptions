# using HHVBF UFO

from MadGraphControl.MadGraphUtils import *

# ----------------------------------------------------------------------------
# Adding block for PDF and other variations
# ----------------------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING["alternative_dynamic_scales"] =  [1,2,3,4]

# ----------------------------------------------------------------------------
# Define physics process to be simulated
# ----------------------------------------------------------------------------

mgproc = """
generate p p > w+ w+ h j j QCD=0 @0
"""
runName = 'WpWmhjj_LO_hhvbf_lvqqbb'


gridpack_mode = True
gridpack_compile = True
savePROCdir = False # true only for testing

#MADGRAPH_CATCH_ERRORS=False

physShort = get_physics_short()

#---------------------------------------------------------------------------------------------------
# Number of Events
# The number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 1.1
evgenConfig.nEventsPerJob = 2000
nevents = safefactor * evgenConfig.nEventsPerJob  


def get_KL(physShort):
  import sys
  if sys.version_info >= (3, 0):
    return int(physShort[physShort.find("_kl")+3:].translate(str.maketrans('pm','+-')))
  return int(physShort[physShort.find("_kl")+3:].translate(string.maketrans('pm','+-')))

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat; CX=1 are the SM parameters
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1.0000000',  # CV
                     'C2V': '1.0000000',  # C2V
                     'C3':  get_KL(physShort)} # C3, a.k.a kl

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------

extras = { 'lhe_version':'3.0',
           'ptj': 10.0,
           'drjj': 0.4,
           'etaj': 5.5,
           'auto_ptj_mjj':False,
           'cut_decays': False,
           'systematics_program': 'systematics',
         }


extras["nevents"] = int(nevents)


#---------------------------------------------------------------------------------------------------
# Generating VBS like process plus H with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------

if not is_gen_from_gridpack():

  process="""
  import model HHVBF_UFO-4FSZeroYukawa
  define p = g u c d s u~ c~ d~ s~
  define j = g u c d s u~ c~ d~ s~
  define l+ = e+ mu+
  define l- = e- mu-
  define vl = ve vm
  define vl~ = ve~ vm~
  """ + mgproc + """
  output -f"""

  process_dir = new_process(process)

else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

#---------------------------------------------------------------------------------------------------
# MadSpin settings
#---------------------------------------------------------------------------------------------------

if is_gen_from_gridpack():
  madspin_card=process_dir+'/Cards/madspin_card.dat'
  if os.access(madspin_card,os.R_OK):
      os.unlink(madspin_card)

madspin_card='madspin_card.dat'
mscard = open(madspin_card,'w')
mscard.write("""
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay w+ > l+ vl
decay w+ > j j
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()




#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for CV, C2V, C3
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs, grid_pack=gridpack_mode,gridpack_compile=gridpack_compile)


#Run madspin
add_madspin(madspin_card=madspin_card, process_dir=process_dir)


#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=savePROCdir)

# Helper for resetting process number; multicore only used for gridpack generation
check_reset_proc_number(opts)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "Higgs production through ssWW scattering with Higgs decaying to bb and the Ws leptonically (e,mu) in HHHVBF_UFO."
evgenConfig.keywords = ["Higgs", "SM", "SMHiggs", "VBS", "electroweak", "bottom"]
evgenConfig.contact = ['Lisa Marie Lehmann  <Lisa.Marie.Lehmann@cern.ch>','Karolos Potamianos <karolos.potamianos@cern.ch>']

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["25:onMode = off",
                            "25:oneChannel = 1 1 100 5 -5 "]  # bb decay

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]


