print("Batch has entered job options file.")
from MadGraphControl.MadGraphUtils import *
import math
import os
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

keepOutput=True
gridpack_mode = True
generate_gridpack = False

get_param_card = subprocess.Popen(['get_files', '-data', 'aMcAtNlo_param_card_TRSM_XHS.dat'])
if get_param_card.wait():
        print("Could not get hold of aMcAtNlo_param_card_TRSM_XHS.dat, exiting...")
        sys.exit(2)
get_run_card = subprocess.Popen(['get_files', '-data', 'aMcAtNlo_run_card_TRSM_XHS.dat'])
if get_run_card.wait():
        print("Could not get hold of aMcAtNlo_run_card_TRSM_XHS.dat, exiting...")
        sys.exit(2)


if not generate_gridpack:
   nevents = runArgs.maxEvents*10
else: 
   nevents = runArgs.maxEvents
# Scale up the events to make it more likely pythia will be happy

if hasattr(runArgs, 'ecmEnergy'):
   beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

# H3 = iota0, H2 = eta0 heavy scalars, H=hSM
# M3>M2>M1
# Benchmark scenario 3 in https://link.springer.com/article/10.1140%2Fepjc%2Fs10052-020-7655-x
# Parameters chosen to maximise iota0 production and Br(iota0->H+eta0)
# Sample job options should have name such as mc.MGPy8EG_A14N23LO_TRSM_HHH_bbbbbb_500p0_200p0.py
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
print("PHYSICS SHORT: ", phys_short)

sample_name_list = phys_short.strip('\.py').split('_')

def sample_name_to_parameters(sample_name_list):
  topology = sample_name_list[-4]
  decay = sample_name_list[-3]
  print("============================")  
  print(decay)  
  M3_str = sample_name_list[-2]
  print(M3_str)
  M2_str = sample_name_list[-1].replace(".GRID.tar.gz","")
  print(M2_str)
  return M3_str,M2_str,topology,decay

M3_str,M2_str,topology,decay = sample_name_to_parameters(sample_name_list)
v = 246.
vs = 206.
vx = 288.
M1 = 125.09
M2 = float(M2_str.replace('p','.'))
M3 = float(M3_str.replace('p','.'))
a12= 0.168
a13= -0.266
a23= -0.952

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
99925
99926
""")
pdgfile.close()

# model masses for param card
masses = {'99925':M2,'99926':M3}

# the R mixing matrix given the cosines of the angles
def Rmatrix(a12, a13, a23):
    # define output matrix
    R = [[0. for x in range(3)] for y in range(3)]
    #calculate the cos functions
    c1 = math.cos(a12)
    c2 = math.cos(a13)
    c3 = math.cos(a23)
    # get the sin functions
    s1 = math.sin(a12)
    s2 = math.sin(a13)
    s3 = math.sin(a23)

    # calculate the elements
    R[0][0] = c1 * c2
    R[0][1] = - s1 * c2
    R[0][2] = - s2
    R[1][0] = s1 * c3 - c1 * s2 * s3
    R[1][1] = c1 * c3 + s1 * s2 * s3
    R[1][2] = - c2 * s3
    R[2][0] = c1 * s2 * c3 + s1 * s3
    R[2][1] = c1 * s3 - s1 * s2 * c3
    R[2][2] = c2 * c3
    return R

#calculate the cos functions
c1 = math.cos(a12)
c2 = math.cos(a13)
c3 = math.cos(a23)
# first get the R-matrix 
R = Rmatrix(a12, a13, a23)
R11, R12, R13, R21, R22, R23, R31, R32, R33 = R[0][0], R[0][1], R[0][2], R[1][0], R[1][1], R[1][2], R[2][0], R[2][1], R[2][2]

# Work out triple couplings
kap111 = (M1**2*(R11**3/v + R12**3/vs + R13**3/vx))/2.
kap112 = ((2*M1**2 + M2**2)*((R11**2*R21)/v + (R12**2*R22)/vs + (R13**2*R23)/vx))/2.
kap113 = ((2*M1**2 + M3**2)*((R11**2*R31)/v + (R12**2*R32)/vs + (R13**2*R33)/vx))/2.
kap122 = ((M1**2 + 2*M2**2)*((R11*R21**2)/v + (R12*R22**2)/vs + (R13*R23**2)/vx))/2.
kap123 = (M1**2 + M2**2 + M3**2)*((R11*R21*R31)/v + (R12*R22*R32)/vs + (R13*R23*R33)/vx)
kap133 = ((M1**2 + 2*M3**2)*((R11*R31**2)/v + (R12*R32**2)/vs + (R13*R33**2)/vx))/2.
kap222 = (M2**2*(R21**3/v + R22**3/vs + R23**3/vx))/2.
kap223 = ((2*M2**2 + M3**2)*((R21**2*R31)/v + (R22**2*R32)/vs + (R23**2*R33)/vx))/2.
kap233 = ((M2**2 + 2*M3**2)*((R21*R31**2)/v + (R22*R32**2)/vs + (R23*R33**2)/vx))/2.
kap333 = (M3**2*(R31**3/v + R32**3/vs + R33**3/vx))/2.
# Work out quartic couplings
kap1111 = (0.125*M1**2*R11**6)/v**2 + (0.125*M2**2*R11**4*R21**2)/v**2 + (0.125*M3**2*R11**4*R31**2)/v**2 + (0.125*M1**2*R12**6)/vs**2 + (0.125*M2**2*R12**4*R22**2)/vs**2 + (0.125*M3**2*R12**4*R32**2)/vs**2 + (0.25*M1**2*R11**3*R12**3)/(v*vs) + (0.25*M2**2*R11**2*R12**2*R21*R22)/(v*vs) + (0.25*M3**2*R11**2*R12**2*R31*R32)/(v*vs) + (0.125*M1**2*R13**6)/vx**2 + (0.125*M2**2*R13**4*R23**2)/vx**2 + (0.125*M3**2*R13**4*R33**2)/vx**2 + (0.25*M1**2*R11**3*R13**3)/(v*vx) + (0.25*M2**2*R11**2*R13**2*R21*R23)/(v*vx) + (0.25*M3**2*R11**2*R13**2*R31*R33)/(v*vx) + (0.25*M1**2*R12**3*R13**3)/(vs*vx) + (0.25*M2**2*R12**2*R13**2*R22*R23)/(vs*vx) + (0.25*M3**2*R12**2*R13**2*R32*R33)/(vs*vx)
kap1112 = (0.5*M1**2*R11**5*R21)/v**2 + (0.5*M2**2*R11**3*R21**3)/v**2 + (0.5*M3**2*R11**3*R21*R31**2)/v**2 + (0.5*M1**2*R12**5*R22)/vs**2 + (0.5*M2**2*R12**3*R22**3)/vs**2 + (0.5*M3**2*R12**3*R22*R32**2)/vs**2 + (0.5*M1**2*R11**2*R12**3*R21)/(v*vs) + (0.5*M1**2*R11**3*R12**2*R22)/(v*vs) + (0.5*M2**2*R11*R12**2*R21**2*R22)/(v*vs) + (0.5*M2**2*R11**2*R12*R21*R22**2)/(v*vs) + (0.5*M3**2*R11*R12**2*R21*R31*R32)/(v*vs) + (0.5*M3**2*R11**2*R12*R22*R31*R32)/(v*vs) + (0.5*M1**2*R13**5*R23)/vx**2 + (0.5*M2**2*R13**3*R23**3)/vx**2 + (0.5*M3**2*R13**3*R23*R33**2)/vx**2 + (0.5*M1**2*R11**2*R13**3*R21)/(v*vx) + (0.5*M1**2*R11**3*R13**2*R23)/(v*vx) + (0.5*M2**2*R11*R13**2*R21**2*R23)/(v*vx) + (0.5*M2**2*R11**2*R13*R21*R23**2)/(v*vx) + (0.5*M3**2*R11*R13**2*R21*R31*R33)/(v*vx) + (0.5*M3**2*R11**2*R13*R23*R31*R33)/(v*vx) + (0.5*M1**2*R12**2*R13**3*R22)/(vs*vx) + (0.5*M1**2*R12**3*R13**2*R23)/(vs*vx) + (0.5*M2**2*R12*R13**2*R22**2*R23)/(vs*vx) + (0.5*M2**2*R12**2*R13*R22*R23**2)/(vs*vx) + (0.5*M3**2*R12*R13**2*R22*R32*R33)/(vs*vx) + (0.5*M3**2*R12**2*R13*R23*R32*R33)/(vs*vx)
kap1113 = (0.5*M1**2*R11**5*R31)/v**2 + (0.5*M2**2*R11**3*R21**2*R31)/v**2 + (0.5*M3**2*R11**3*R31**3)/v**2 + (0.5*M1**2*R12**5*R32)/vs**2 + (0.5*M2**2*R12**3*R22**2*R32)/vs**2 + (0.5*M3**2*R12**3*R32**3)/vs**2 + (0.5*M1**2*R11**2*R12**3*R31)/(v*vs) + (0.5*M2**2*R11*R12**2*R21*R22*R31)/(v*vs) + (0.5*M1**2*R11**3*R12**2*R32)/(v*vs) + (0.5*M2**2*R11**2*R12*R21*R22*R32)/(v*vs) + (0.5*M3**2*R11*R12**2*R31**2*R32)/(v*vs) + (0.5*M3**2*R11**2*R12*R31*R32**2)/(v*vs) + (0.5*M1**2*R13**5*R33)/vx**2 + (0.5*M2**2*R13**3*R23**2*R33)/vx**2 + (0.5*M3**2*R13**3*R33**3)/vx**2 + (0.5*M1**2*R11**2*R13**3*R31)/(v*vx) + (0.5*M2**2*R11*R13**2*R21*R23*R31)/(v*vx) + (0.5*M1**2*R11**3*R13**2*R33)/(v*vx) + (0.5*M2**2*R11**2*R13*R21*R23*R33)/(v*vx) + (0.5*M3**2*R11*R13**2*R31**2*R33)/(v*vx) + (0.5*M3**2*R11**2*R13*R31*R33**2)/(v*vx) + (0.5*M1**2*R12**2*R13**3*R32)/(vs*vx) + (0.5*M2**2*R12*R13**2*R22*R23*R32)/(vs*vx) + (0.5*M1**2*R12**3*R13**2*R33)/(vs*vx) + (0.5*M2**2*R12**2*R13*R22*R23*R33)/(vs*vx) + (0.5*M3**2*R12*R13**2*R32**2*R33)/(vs*vx) + (0.5*M3**2*R12**2*R13*R32*R33**2)/(vs*vx)
kap1122 = (0.75*M1**2*R11**4*R21**2)/v**2 + (0.75*M2**2*R11**2*R21**4)/v**2 + (0.75*M3**2*R11**2*R21**2*R31**2)/v**2 + (0.75*M1**2*R12**4*R22**2)/vs**2 + (0.75*M2**2*R12**2*R22**4)/vs**2 + (0.75*M3**2*R12**2*R22**2*R32**2)/vs**2 + (0.25*M1**2*R11*R12**3*R21**2)/(v*vs) + (1.*M1**2*R11**2*R12**2*R21*R22)/(v*vs) + (0.25*M2**2*R12**2*R21**3*R22)/(v*vs) + (0.25*M1**2*R11**3*R12*R22**2)/(v*vs) + (1.*M2**2*R11*R12*R21**2*R22**2)/(v*vs) + (0.25*M2**2*R11**2*R21*R22**3)/(v*vs) + (0.25*M3**2*R12**2*R21**2*R31*R32)/(v*vs) + (1.*M3**2*R11*R12*R21*R22*R31*R32)/(v*vs) + (0.25*M3**2*R11**2*R22**2*R31*R32)/(v*vs) + (0.75*M1**2*R13**4*R23**2)/vx**2 + (0.75*M2**2*R13**2*R23**4)/vx**2 + (0.75*M3**2*R13**2*R23**2*R33**2)/vx**2 + (0.25*M1**2*R11*R13**3*R21**2)/(v*vx) + (1.*M1**2*R11**2*R13**2*R21*R23)/(v*vx) + (0.25*M2**2*R13**2*R21**3*R23)/(v*vx) + (0.25*M1**2*R11**3*R13*R23**2)/(v*vx) + (1.*M2**2*R11*R13*R21**2*R23**2)/(v*vx) + (0.25*M2**2*R11**2*R21*R23**3)/(v*vx) + (0.25*M3**2*R13**2*R21**2*R31*R33)/(v*vx) + (1.*M3**2*R11*R13*R21*R23*R31*R33)/(v*vx) + (0.25*M3**2*R11**2*R23**2*R31*R33)/(v*vx) + (0.25*M1**2*R12*R13**3*R22**2)/(vs*vx) + (1.*M1**2*R12**2*R13**2*R22*R23)/(vs*vx) + (0.25*M2**2*R13**2*R22**3*R23)/(vs*vx) + (0.25*M1**2*R12**3*R13*R23**2)/(vs*vx) + (1.*M2**2*R12*R13*R22**2*R23**2)/(vs*vx) + (0.25*M2**2*R12**2*R22*R23**3)/(vs*vx) + (0.25*M3**2*R13**2*R22**2*R32*R33)/(vs*vx) + (1.*M3**2*R12*R13*R22*R23*R32*R33)/(vs*vx) + (0.25*M3**2*R12**2*R23**2*R32*R33)/(vs*vx)
kap1123 = (1.5*M1**2*R11**4*R21*R31)/v**2 + (1.5*M2**2*R11**2*R21**3*R31)/v**2 + (1.5*M3**2*R11**2*R21*R31**3)/v**2 + (1.5*M1**2*R12**4*R22*R32)/vs**2 + (1.5*M2**2*R12**2*R22**3*R32)/vs**2 + (1.5*M3**2*R12**2*R22*R32**3)/vs**2 + (0.5*M1**2*R11*R12**3*R21*R31)/(v*vs) + (1.*M1**2*R11**2*R12**2*R22*R31)/(v*vs) + (0.5*M2**2*R12**2*R21**2*R22*R31)/(v*vs) + (1.*M2**2*R11*R12*R21*R22**2*R31)/(v*vs) + (1.*M1**2*R11**2*R12**2*R21*R32)/(v*vs) + (0.5*M1**2*R11**3*R12*R22*R32)/(v*vs) + (1.*M2**2*R11*R12*R21**2*R22*R32)/(v*vs) + (0.5*M2**2*R11**2*R21*R22**2*R32)/(v*vs) + (0.5*M3**2*R12**2*R21*R31**2*R32)/(v*vs) + (1.*M3**2*R11*R12*R22*R31**2*R32)/(v*vs) + (1.*M3**2*R11*R12*R21*R31*R32**2)/(v*vs) + (0.5*M3**2*R11**2*R22*R31*R32**2)/(v*vs) + (1.5*M1**2*R13**4*R23*R33)/vx**2 + (1.5*M2**2*R13**2*R23**3*R33)/vx**2 + (1.5*M3**2*R13**2*R23*R33**3)/vx**2 + (0.5*M1**2*R11*R13**3*R21*R31)/(v*vx) + (1.*M1**2*R11**2*R13**2*R23*R31)/(v*vx) + (0.5*M2**2*R13**2*R21**2*R23*R31)/(v*vx) + (1.*M2**2*R11*R13*R21*R23**2*R31)/(v*vx) + (1.*M1**2*R11**2*R13**2*R21*R33)/(v*vx) + (0.5*M1**2*R11**3*R13*R23*R33)/(v*vx) + (1.*M2**2*R11*R13*R21**2*R23*R33)/(v*vx) + (0.5*M2**2*R11**2*R21*R23**2*R33)/(v*vx) + (0.5*M3**2*R13**2*R21*R31**2*R33)/(v*vx) + (1.*M3**2*R11*R13*R23*R31**2*R33)/(v*vx) + (1.*M3**2*R11*R13*R21*R31*R33**2)/(v*vx) + (0.5*M3**2*R11**2*R23*R31*R33**2)/(v*vx) + (0.5*M1**2*R12*R13**3*R22*R32)/(vs*vx) + (1.*M1**2*R12**2*R13**2*R23*R32)/(vs*vx) + (0.5*M2**2*R13**2*R22**2*R23*R32)/(vs*vx) + (1.*M2**2*R12*R13*R22*R23**2*R32)/(vs*vx) + (1.*M1**2*R12**2*R13**2*R22*R33)/(vs*vx) + (0.5*M1**2*R12**3*R13*R23*R33)/(vs*vx) + (1.*M2**2*R12*R13*R22**2*R23*R33)/(vs*vx) + (0.5*M2**2*R12**2*R22*R23**2*R33)/(vs*vx) + (0.5*M3**2*R13**2*R22*R32**2*R33)/(vs*vx) + (1.*M3**2*R12*R13*R23*R32**2*R33)/(vs*vx) + (1.*M3**2*R12*R13*R22*R32*R33**2)/(vs*vx) + (0.5*M3**2*R12**2*R23*R32*R33**2)/(vs*vx)
kap1133 = (0.75*M1**2*R11**4*R31**2)/v**2 + (0.75*M2**2*R11**2*R21**2*R31**2)/v**2 + (0.75*M3**2*R11**2*R31**4)/v**2 + (0.75*M1**2*R12**4*R32**2)/vs**2 + (0.75*M2**2*R12**2*R22**2*R32**2)/vs**2 + (0.75*M3**2*R12**2*R32**4)/vs**2 + (0.25*M1**2*R11*R12**3*R31**2)/(v*vs) + (0.25*M2**2*R12**2*R21*R22*R31**2)/(v*vs) + (1.*M1**2*R11**2*R12**2*R31*R32)/(v*vs) + (1.*M2**2*R11*R12*R21*R22*R31*R32)/(v*vs) + (0.25*M3**2*R12**2*R31**3*R32)/(v*vs) + (0.25*M1**2*R11**3*R12*R32**2)/(v*vs) + (0.25*M2**2*R11**2*R21*R22*R32**2)/(v*vs) + (1.*M3**2*R11*R12*R31**2*R32**2)/(v*vs) + (0.25*M3**2*R11**2*R31*R32**3)/(v*vs) + (0.75*M1**2*R13**4*R33**2)/vx**2 + (0.75*M2**2*R13**2*R23**2*R33**2)/vx**2 + (0.75*M3**2*R13**2*R33**4)/vx**2 + (0.25*M1**2*R11*R13**3*R31**2)/(v*vx) + (0.25*M2**2*R13**2*R21*R23*R31**2)/(v*vx) + (1.*M1**2*R11**2*R13**2*R31*R33)/(v*vx) + (1.*M2**2*R11*R13*R21*R23*R31*R33)/(v*vx) + (0.25*M3**2*R13**2*R31**3*R33)/(v*vx) + (0.25*M1**2*R11**3*R13*R33**2)/(v*vx) + (0.25*M2**2*R11**2*R21*R23*R33**2)/(v*vx) + (1.*M3**2*R11*R13*R31**2*R33**2)/(v*vx) + (0.25*M3**2*R11**2*R31*R33**3)/(v*vx) + (0.25*M1**2*R12*R13**3*R32**2)/(vs*vx) + (0.25*M2**2*R13**2*R22*R23*R32**2)/(vs*vx) + (1.*M1**2*R12**2*R13**2*R32*R33)/(vs*vx) + (1.*M2**2*R12*R13*R22*R23*R32*R33)/(vs*vx) + (0.25*M3**2*R13**2*R32**3*R33)/(vs*vx) + (0.25*M1**2*R12**3*R13*R33**2)/(vs*vx) + (0.25*M2**2*R12**2*R22*R23*R33**2)/(vs*vx) + (1.*M3**2*R12*R13*R32**2*R33**2)/(vs*vx) + (0.25*M3**2*R12**2*R32*R33**3)/(vs*vx)
kap1222 = (0.5*M1**2*R11**3*R21**3)/v**2 + (0.5*M2**2*R11*R21**5)/v**2 + (0.5*M3**2*R11*R21**3*R31**2)/v**2 + (0.5*M1**2*R12**3*R22**3)/vs**2 + (0.5*M2**2*R12*R22**5)/vs**2 + (0.5*M3**2*R12*R22**3*R32**2)/vs**2 + (0.5*M1**2*R11*R12**2*R21**2*R22)/(v*vs) + (0.5*M1**2*R11**2*R12*R21*R22**2)/(v*vs) + (0.5*M2**2*R12*R21**3*R22**2)/(v*vs) + (0.5*M2**2*R11*R21**2*R22**3)/(v*vs) + (0.5*M3**2*R12*R21**2*R22*R31*R32)/(v*vs) + (0.5*M3**2*R11*R21*R22**2*R31*R32)/(v*vs) + (0.5*M1**2*R13**3*R23**3)/vx**2 + (0.5*M2**2*R13*R23**5)/vx**2 + (0.5*M3**2*R13*R23**3*R33**2)/vx**2 + (0.5*M1**2*R11*R13**2*R21**2*R23)/(v*vx) + (0.5*M1**2*R11**2*R13*R21*R23**2)/(v*vx) + (0.5*M2**2*R13*R21**3*R23**2)/(v*vx) + (0.5*M2**2*R11*R21**2*R23**3)/(v*vx) + (0.5*M3**2*R13*R21**2*R23*R31*R33)/(v*vx) + (0.5*M3**2*R11*R21*R23**2*R31*R33)/(v*vx) + (0.5*M1**2*R12*R13**2*R22**2*R23)/(vs*vx) + (0.5*M1**2*R12**2*R13*R22*R23**2)/(vs*vx) + (0.5*M2**2*R13*R22**3*R23**2)/(vs*vx) + (0.5*M2**2*R12*R22**2*R23**3)/(vs*vx) + (0.5*M3**2*R13*R22**2*R23*R32*R33)/(vs*vx) + (0.5*M3**2*R12*R22*R23**2*R32*R33)/(vs*vx)
kap1223 = (1.5*M1**2*R11**3*R21**2*R31)/v**2 + (1.5*M2**2*R11*R21**4*R31)/v**2 + (1.5*M3**2*R11*R21**2*R31**3)/v**2 + (1.5*M1**2*R12**3*R22**2*R32)/vs**2 + (1.5*M2**2*R12*R22**4*R32)/vs**2 + (1.5*M3**2*R12*R22**2*R32**3)/vs**2 + (1.*M1**2*R11*R12**2*R21*R22*R31)/(v*vs) + (0.5*M1**2*R11**2*R12*R22**2*R31)/(v*vs) + (1.*M2**2*R12*R21**2*R22**2*R31)/(v*vs) + (0.5*M2**2*R11*R21*R22**3*R31)/(v*vs) + (0.5*M1**2*R11*R12**2*R21**2*R32)/(v*vs) + (1.*M1**2*R11**2*R12*R21*R22*R32)/(v*vs) + (0.5*M2**2*R12*R21**3*R22*R32)/(v*vs) + (1.*M2**2*R11*R21**2*R22**2*R32)/(v*vs) + (1.*M3**2*R12*R21*R22*R31**2*R32)/(v*vs) + (0.5*M3**2*R11*R22**2*R31**2*R32)/(v*vs) + (0.5*M3**2*R12*R21**2*R31*R32**2)/(v*vs) + (1.*M3**2*R11*R21*R22*R31*R32**2)/(v*vs) + (1.5*M1**2*R13**3*R23**2*R33)/vx**2 + (1.5*M2**2*R13*R23**4*R33)/vx**2 + (1.5*M3**2*R13*R23**2*R33**3)/vx**2 + (1.*M1**2*R11*R13**2*R21*R23*R31)/(v*vx) + (0.5*M1**2*R11**2*R13*R23**2*R31)/(v*vx) + (1.*M2**2*R13*R21**2*R23**2*R31)/(v*vx) + (0.5*M2**2*R11*R21*R23**3*R31)/(v*vx) + (0.5*M1**2*R11*R13**2*R21**2*R33)/(v*vx) + (1.*M1**2*R11**2*R13*R21*R23*R33)/(v*vx) + (0.5*M2**2*R13*R21**3*R23*R33)/(v*vx) + (1.*M2**2*R11*R21**2*R23**2*R33)/(v*vx) + (1.*M3**2*R13*R21*R23*R31**2*R33)/(v*vx) + (0.5*M3**2*R11*R23**2*R31**2*R33)/(v*vx) + (0.5*M3**2*R13*R21**2*R31*R33**2)/(v*vx) + (1.*M3**2*R11*R21*R23*R31*R33**2)/(v*vx) + (1.*M1**2*R12*R13**2*R22*R23*R32)/(vs*vx) + (0.5*M1**2*R12**2*R13*R23**2*R32)/(vs*vx) + (1.*M2**2*R13*R22**2*R23**2*R32)/(vs*vx) + (0.5*M2**2*R12*R22*R23**3*R32)/(vs*vx) + (0.5*M1**2*R12*R13**2*R22**2*R33)/(vs*vx) + (1.*M1**2*R12**2*R13*R22*R23*R33)/(vs*vx) + (0.5*M2**2*R13*R22**3*R23*R33)/(vs*vx) + (1.*M2**2*R12*R22**2*R23**2*R33)/(vs*vx) + (1.*M3**2*R13*R22*R23*R32**2*R33)/(vs*vx) + (0.5*M3**2*R12*R23**2*R32**2*R33)/(vs*vx) + (0.5*M3**2*R13*R22**2*R32*R33**2)/(vs*vx) + (1.*M3**2*R12*R22*R23*R32*R33**2)/(vs*vx)
kap1233 = (1.5*M1**2*R11**3*R21*R31**2)/v**2 + (1.5*M2**2*R11*R21**3*R31**2)/v**2 + (1.5*M3**2*R11*R21*R31**4)/v**2 + (1.5*M1**2*R12**3*R22*R32**2)/vs**2 + (1.5*M2**2*R12*R22**3*R32**2)/vs**2 + (1.5*M3**2*R12*R22*R32**4)/vs**2 + (0.5*M1**2*R11*R12**2*R22*R31**2)/(v*vs) + (0.5*M2**2*R12*R21*R22**2*R31**2)/(v*vs) + (1.*M1**2*R11*R12**2*R21*R31*R32)/(v*vs) + (1.*M1**2*R11**2*R12*R22*R31*R32)/(v*vs) + (1.*M2**2*R12*R21**2*R22*R31*R32)/(v*vs) + (1.*M2**2*R11*R21*R22**2*R31*R32)/(v*vs) + (0.5*M3**2*R12*R22*R31**3*R32)/(v*vs) + (0.5*M1**2*R11**2*R12*R21*R32**2)/(v*vs) + (0.5*M2**2*R11*R21**2*R22*R32**2)/(v*vs) + (1.*M3**2*R12*R21*R31**2*R32**2)/(v*vs) + (1.*M3**2*R11*R22*R31**2*R32**2)/(v*vs) + (0.5*M3**2*R11*R21*R31*R32**3)/(v*vs) + (1.5*M1**2*R13**3*R23*R33**2)/vx**2 + (1.5*M2**2*R13*R23**3*R33**2)/vx**2 + (1.5*M3**2*R13*R23*R33**4)/vx**2 + (0.5*M1**2*R11*R13**2*R23*R31**2)/(v*vx) + (0.5*M2**2*R13*R21*R23**2*R31**2)/(v*vx) + (1.*M1**2*R11*R13**2*R21*R31*R33)/(v*vx) + (1.*M1**2*R11**2*R13*R23*R31*R33)/(v*vx) + (1.*M2**2*R13*R21**2*R23*R31*R33)/(v*vx) + (1.*M2**2*R11*R21*R23**2*R31*R33)/(v*vx) + (0.5*M3**2*R13*R23*R31**3*R33)/(v*vx) + (0.5*M1**2*R11**2*R13*R21*R33**2)/(v*vx) + (0.5*M2**2*R11*R21**2*R23*R33**2)/(v*vx) + (1.*M3**2*R13*R21*R31**2*R33**2)/(v*vx) + (1.*M3**2*R11*R23*R31**2*R33**2)/(v*vx) + (0.5*M3**2*R11*R21*R31*R33**3)/(v*vx) + (0.5*M1**2*R12*R13**2*R23*R32**2)/(vs*vx) + (0.5*M2**2*R13*R22*R23**2*R32**2)/(vs*vx) + (1.*M1**2*R12*R13**2*R22*R32*R33)/(vs*vx) + (1.*M1**2*R12**2*R13*R23*R32*R33)/(vs*vx) + (1.*M2**2*R13*R22**2*R23*R32*R33)/(vs*vx) + (1.*M2**2*R12*R22*R23**2*R32*R33)/(vs*vx) + (0.5*M3**2*R13*R23*R32**3*R33)/(vs*vx) + (0.5*M1**2*R12**2*R13*R22*R33**2)/(vs*vx) + (0.5*M2**2*R12*R22**2*R23*R33**2)/(vs*vx) + (1.*M3**2*R13*R22*R32**2*R33**2)/(vs*vx) + (1.*M3**2*R12*R23*R32**2*R33**2)/(vs*vx) + (0.5*M3**2*R12*R22*R32*R33**3)/(vs*vx)
kap1333 = (0.5*M1**2*R11**3*R31**3)/v**2 + (0.5*M2**2*R11*R21**2*R31**3)/v**2 + (0.5*M3**2*R11*R31**5)/v**2 + (0.5*M1**2*R12**3*R32**3)/vs**2 + (0.5*M2**2*R12*R22**2*R32**3)/vs**2 + (0.5*M3**2*R12*R32**5)/vs**2 + (0.5*M1**2*R11*R12**2*R31**2*R32)/(v*vs) + (0.5*M2**2*R12*R21*R22*R31**2*R32)/(v*vs) + (0.5*M1**2*R11**2*R12*R31*R32**2)/(v*vs) + (0.5*M2**2*R11*R21*R22*R31*R32**2)/(v*vs) + (0.5*M3**2*R12*R31**3*R32**2)/(v*vs) + (0.5*M3**2*R11*R31**2*R32**3)/(v*vs) + (0.5*M1**2*R13**3*R33**3)/vx**2 + (0.5*M2**2*R13*R23**2*R33**3)/vx**2 + (0.5*M3**2*R13*R33**5)/vx**2 + (0.5*M1**2*R11*R13**2*R31**2*R33)/(v*vx) + (0.5*M2**2*R13*R21*R23*R31**2*R33)/(v*vx) + (0.5*M1**2*R11**2*R13*R31*R33**2)/(v*vx) + (0.5*M2**2*R11*R21*R23*R31*R33**2)/(v*vx) + (0.5*M3**2*R13*R31**3*R33**2)/(v*vx) + (0.5*M3**2*R11*R31**2*R33**3)/(v*vx) + (0.5*M1**2*R12*R13**2*R32**2*R33)/(vs*vx) + (0.5*M2**2*R13*R22*R23*R32**2*R33)/(vs*vx) + (0.5*M1**2*R12**2*R13*R32*R33**2)/(vs*vx) + (0.5*M2**2*R12*R22*R23*R32*R33**2)/(vs*vx) + (0.5*M3**2*R13*R32**3*R33**2)/(vs*vx) + (0.5*M3**2*R12*R32**2*R33**3)/(vs*vx)
kap2222 = (0.125*M1**2*R11**2*R21**4)/v**2 + (0.125*M2**2*R21**6)/v**2 + (0.125*M3**2*R21**4*R31**2)/v**2 + (0.125*M1**2*R12**2*R22**4)/vs**2 + (0.125*M2**2*R22**6)/vs**2 + (0.125*M3**2*R22**4*R32**2)/vs**2 + (0.25*M1**2*R11*R12*R21**2*R22**2)/(v*vs) + (0.25*M2**2*R21**3*R22**3)/(v*vs) + (0.25*M3**2*R21**2*R22**2*R31*R32)/(v*vs) + (0.125*M1**2*R13**2*R23**4)/vx**2 + (0.125*M2**2*R23**6)/vx**2 + (0.125*M3**2*R23**4*R33**2)/vx**2 + (0.25*M1**2*R11*R13*R21**2*R23**2)/(v*vx) + (0.25*M2**2*R21**3*R23**3)/(v*vx) + (0.25*M3**2*R21**2*R23**2*R31*R33)/(v*vx) + (0.25*M1**2*R12*R13*R22**2*R23**2)/(vs*vx) + (0.25*M2**2*R22**3*R23**3)/(vs*vx) + (0.25*M3**2*R22**2*R23**2*R32*R33)/(vs*vx)
kap2223 = (0.5*M1**2*R11**2*R21**3*R31)/v**2 + (0.5*M2**2*R21**5*R31)/v**2 + (0.5*M3**2*R21**3*R31**3)/v**2 + (0.5*M1**2*R12**2*R22**3*R32)/vs**2 + (0.5*M2**2*R22**5*R32)/vs**2 + (0.5*M3**2*R22**3*R32**3)/vs**2 + (0.5*M1**2*R11*R12*R21*R22**2*R31)/(v*vs) + (0.5*M2**2*R21**2*R22**3*R31)/(v*vs) + (0.5*M1**2*R11*R12*R21**2*R22*R32)/(v*vs) + (0.5*M2**2*R21**3*R22**2*R32)/(v*vs) + (0.5*M3**2*R21*R22**2*R31**2*R32)/(v*vs) + (0.5*M3**2*R21**2*R22*R31*R32**2)/(v*vs) + (0.5*M1**2*R13**2*R23**3*R33)/vx**2 + (0.5*M2**2*R23**5*R33)/vx**2 + (0.5*M3**2*R23**3*R33**3)/vx**2 + (0.5*M1**2*R11*R13*R21*R23**2*R31)/(v*vx) + (0.5*M2**2*R21**2*R23**3*R31)/(v*vx) + (0.5*M1**2*R11*R13*R21**2*R23*R33)/(v*vx) + (0.5*M2**2*R21**3*R23**2*R33)/(v*vx) + (0.5*M3**2*R21*R23**2*R31**2*R33)/(v*vx) + (0.5*M3**2*R21**2*R23*R31*R33**2)/(v*vx) + (0.5*M1**2*R12*R13*R22*R23**2*R32)/(vs*vx) + (0.5*M2**2*R22**2*R23**3*R32)/(vs*vx) + (0.5*M1**2*R12*R13*R22**2*R23*R33)/(vs*vx) + (0.5*M2**2*R22**3*R23**2*R33)/(vs*vx) + (0.5*M3**2*R22*R23**2*R32**2*R33)/(vs*vx) + (0.5*M3**2*R22**2*R23*R32*R33**2)/(vs*vx)
kap2233 = (0.75*M1**2*R11**2*R21**2*R31**2)/v**2 + (0.75*M2**2*R21**4*R31**2)/v**2 + (0.75*M3**2*R21**2*R31**4)/v**2 + (0.75*M1**2*R12**2*R22**2*R32**2)/vs**2 + (0.75*M2**2*R22**4*R32**2)/vs**2 + (0.75*M3**2*R22**2*R32**4)/vs**2 + (0.25*M1**2*R11*R12*R22**2*R31**2)/(v*vs) + (0.25*M2**2*R21*R22**3*R31**2)/(v*vs) + (1.*M1**2*R11*R12*R21*R22*R31*R32)/(v*vs) + (1.*M2**2*R21**2*R22**2*R31*R32)/(v*vs) + (0.25*M3**2*R22**2*R31**3*R32)/(v*vs) + (0.25*M1**2*R11*R12*R21**2*R32**2)/(v*vs) + (0.25*M2**2*R21**3*R22*R32**2)/(v*vs) + (1.*M3**2*R21*R22*R31**2*R32**2)/(v*vs) + (0.25*M3**2*R21**2*R31*R32**3)/(v*vs) + (0.75*M1**2*R13**2*R23**2*R33**2)/vx**2 + (0.75*M2**2*R23**4*R33**2)/vx**2 + (0.75*M3**2*R23**2*R33**4)/vx**2 + (0.25*M1**2*R11*R13*R23**2*R31**2)/(v*vx) + (0.25*M2**2*R21*R23**3*R31**2)/(v*vx) + (1.*M1**2*R11*R13*R21*R23*R31*R33)/(v*vx) + (1.*M2**2*R21**2*R23**2*R31*R33)/(v*vx) + (0.25*M3**2*R23**2*R31**3*R33)/(v*vx) + (0.25*M1**2*R11*R13*R21**2*R33**2)/(v*vx) + (0.25*M2**2*R21**3*R23*R33**2)/(v*vx) + (1.*M3**2*R21*R23*R31**2*R33**2)/(v*vx) + (0.25*M3**2*R21**2*R31*R33**3)/(v*vx) + (0.25*M1**2*R12*R13*R23**2*R32**2)/(vs*vx) + (0.25*M2**2*R22*R23**3*R32**2)/(vs*vx) + (1.*M1**2*R12*R13*R22*R23*R32*R33)/(vs*vx) + (1.*M2**2*R22**2*R23**2*R32*R33)/(vs*vx) + (0.25*M3**2*R23**2*R32**3*R33)/(vs*vx) + (0.25*M1**2*R12*R13*R22**2*R33**2)/(vs*vx) + (0.25*M2**2*R22**3*R23*R33**2)/(vs*vx) + (1.*M3**2*R22*R23*R32**2*R33**2)/(vs*vx) + (0.25*M3**2*R22**2*R32*R33**3)/(vs*vx)
kap2333 = (0.5*M1**2*R11**2*R21*R31**3)/v**2 + (0.5*M2**2*R21**3*R31**3)/v**2 + (0.5*M3**2*R21*R31**5)/v**2 + (0.5*M1**2*R12**2*R22*R32**3)/vs**2 + (0.5*M2**2*R22**3*R32**3)/vs**2 + (0.5*M3**2*R22*R32**5)/vs**2 + (0.5*M1**2*R11*R12*R22*R31**2*R32)/(v*vs) + (0.5*M2**2*R21*R22**2*R31**2*R32)/(v*vs) + (0.5*M1**2*R11*R12*R21*R31*R32**2)/(v*vs) + (0.5*M2**2*R21**2*R22*R31*R32**2)/(v*vs) + (0.5*M3**2*R22*R31**3*R32**2)/(v*vs) + (0.5*M3**2*R21*R31**2*R32**3)/(v*vs) + (0.5*M1**2*R13**2*R23*R33**3)/vx**2 + (0.5*M2**2*R23**3*R33**3)/vx**2 + (0.5*M3**2*R23*R33**5)/vx**2 + (0.5*M1**2*R11*R13*R23*R31**2*R33)/(v*vx) + (0.5*M2**2*R21*R23**2*R31**2*R33)/(v*vx) + (0.5*M1**2*R11*R13*R21*R31*R33**2)/(v*vx) + (0.5*M2**2*R21**2*R23*R31*R33**2)/(v*vx) + (0.5*M3**2*R23*R31**3*R33**2)/(v*vx) + (0.5*M3**2*R21*R31**2*R33**3)/(v*vx) + (0.5*M1**2*R12*R13*R23*R32**2*R33)/(vs*vx) + (0.5*M2**2*R22*R23**2*R32**2*R33)/(vs*vx) + (0.5*M1**2*R12*R13*R22*R32*R33**2)/(vs*vx) + (0.5*M2**2*R22**2*R23*R32*R33**2)/(vs*vx) + (0.5*M3**2*R23*R32**3*R33**2)/(vs*vx) + (0.5*M3**2*R22*R32**2*R33**3)/(vs*vx)
kap3333 = (0.125*M1**2*R11**2*R31**4)/v**2 + (0.125*M2**2*R21**2*R31**4)/v**2 + (0.125*M3**2*R31**6)/v**2 + (0.125*M1**2*R12**2*R32**4)/vs**2 + (0.125*M2**2*R22**2*R32**4)/vs**2 + (0.125*M3**2*R32**6)/vs**2 + (0.25*M1**2*R11*R12*R31**2*R32**2)/(v*vs) + (0.25*M2**2*R21*R22*R31**2*R32**2)/(v*vs) + (0.25*M3**2*R31**3*R32**3)/(v*vs) + (0.125*M1**2*R13**2*R33**4)/vx**2 + (0.125*M2**2*R23**2*R33**4)/vx**2 + (0.125*M3**2*R33**6)/vx**2 + (0.25*M1**2*R11*R13*R31**2*R33**2)/(v*vx) + (0.25*M2**2*R21*R23*R31**2*R33**2)/(v*vx) + (0.25*M3**2*R31**3*R33**3)/(v*vx) + (0.25*M1**2*R12*R13*R32**2*R33**2)/(vs*vx) + (0.25*M2**2*R22*R23*R32**2*R33**2)/(vs*vx) + (0.25*M3**2*R32**3*R33**3)/(vs*vx)

couplings = {
   '12': str(c1)+'  # ctheta1', 
   '13': str(c2)+'  # ctheta2', 
   '14': str(c3)+'  # ctheta3', 
   '212': str(v)+'  # v',
   '213': str(vs)+'  # vs',
   '214': str(vx)+'  # vx',
   '215': str(R11)+'  # r11',
   '216': str(R21)+'  # r21',
   '217': str(R31)+'  # r31',       
   '15': str(kap111)+'  # kap111',
   '16': str(kap112)+'  # kap112',
   '17': str(kap122)+'  # kap122',
   '18': str(kap222)+'  # kap222',
   '24': str(kap133)+'  # kap133',
   '25': str(kap113)+'  # kap113',
   '26': str(kap123)+'  # kap123',
   '27': str(kap333)+'  # kap333',
   '28': str(kap233)+'  # kap233',
   '29': str(kap223)+'  # kap223', 
   '19': str(kap1111)+'  # kap1111',
   '20': str(kap1112)+'  # kap1112',
   '21': str(kap1122)+'  # kap1122',
   '22': str(kap1222)+'  # kap1222',
   '23': str(kap2222)+'  # kap2222',
   '30': str(kap1113)+'  # kap1113',
   '31': str(kap1133)+'  # kap1133',
   '32': str(kap1333)+'  # kap1333',
   '33': str(kap3333)+'  # kap3333',
   '34': str(kap2223)+'  # kap2223',
   '35': str(kap2333)+'  # kap2333',
   '36': str(kap2233)+'  # kap2233',
   '37': str(kap1233)+'  # kap1233',
   '38': str(kap1223)+'  # kap1223',
   '39': str(kap1123)+'  # kap1123'
}

# calculate total widths for heavy scalars
# Gammas for each scalar mass in grid, credit to TRSM authors
Gamma_SM = { 130.:0.0049, 140.:0.0081, 150.:0.0168, 160.:0.0787, 170.:0.3600, 180.:0.6020, 190.:0.9900, 200.:1.3700, 210.:1.7700, 220.:2.2200, 230.:2.7300, 240.:3.2900, 250.:3.9200, 260.:4.6300, 270.:5.4100, 280.:6.2700, 290.:7.2100, 300.:8.2300, 310.:9.3500, 320.:10.600, 330.:11.900, 340.:13.300, 350.:15.000, 360.:17.300, 370.:19.800, 380.:22.400, 390.:25.200, 400.:28.200, 410.:31.200, 420.:34.400, 430.:37.700, 440.:41.100, 450.:44.600, 455:46.377, 460.:48.200, 470.:52.000, 480.:55.800, 490.:59.800, 500.:64.000, 510.:68.315, 520.:72.734, 530.:77.265, 540.:81.918, 550.:86.700, 560.:91.622, 570.:96.6974, 580.:101.942, 590.:107.371, 600.:113.000, 650.:144.0,700.:179.0, 800.:264.0, 900.:373.0, 1000.:508.0, 1050.:508.0, 1100.:508.0, 1200.:508.0, 1300.:508.0, 1400.:508.0, 1500.:508.0, 575.:99.298, 525.:74.985, 485.:57.769, 475.:53.892, 425.:36.041, 375.:21.082, 325.:11.244, 275.:5.830, 225.:2.468, 285.: 6.730, 2000.:508.0, 2500.:508.0, 3000.:508.0, 4000.:508.0}
# calculate the width h3 -> h2 h1, given the mass, the coupling l123 (in GeV) and the sin(mixing angle)
def Gam_h3_to_h2h1(m1, m2, m3, l123):
  if m3 < m1+m2:
    return 0.
  P = (1./2./m3) * math.sqrt( (m3**2 - (m1+m2)**2) * (m3**2 - (m1-m2)**2) )
  width_h3h2h1 = l123**2 * P / 8 / math.pi / m3**2
  return width_h3h2h1
# calculate the width h2 -> h1 h1, given the mass, the coupling l112 (in GeV) and the sin(mixing angle)
def Gam_h2_to_h1h1(m1, m2, l112):
  if m2 < 2*m1:
    return 0.
  width_h2h1h1 = l112**2 * math.sqrt( 1 - 4 * m1**2 / m2**2 ) / 8 / math.pi / m2
  return width_h2h1h1

H2_width = Gamma_SM[M2] * R21**2  + Gam_h2_to_h1h1(M1, M2, kap112)
H3_width = Gamma_SM[M3] * R31**2  + Gam_h2_to_h1h1(M1, M3, kap113) + Gam_h3_to_h2h1(M1, M2, M3, kap123) + Gam_h2_to_h1h1(M2, M3, kap223)

widths = {
  '25':str(0.0038110045676034245), 
  '99925':str(H2_width),
  '99926':str(H3_width)
}

gentype = 'TRSM'

param_card_old = None
mgproc = None

run_card_extras = {}
runName = 'aMCPy8.TRSM_{0}_{1}_{2}'.format(topology,decay,M3_str,M2_str)

hSMMass = 125.09
xqcut = 0

#generate g g > H eta0 [QCD]
#generate g g > iota0 > eta0 H QED<=4 [QCD]
#generate g g > iota0 > eta0 h QED<=4 [noborn=QCD]
#generate g g > eta0 h QED<=4 [noborn=QCD]
#generate p p > eta0 h $$ eta0 h QED<=4 [noborn=QCD]

mgproc="""
  generate g g > iota0 > eta0 h QED<=4 [noborn=QCD]
 
"""

output = """
output -f
"""

process_str = """
import model loop_sm_twoscalar
define bee = b b~
"""+mgproc+output

m_description = 'MadGraph+Pythia8+A14NNPDF30NLO production pp~>SH~>{0} in TRSM model, m_iota0 = {1} GeV, m_eta0 = {2} GeV'.format(decay, M3_str, M2_str)

evgenConfig.description = m_description
evgenConfig.keywords += ['BSMHiggs']
run_card_extras['fixed_ren_scale'] =  'True' # 
run_card_extras['fixed_fac_scale'] =  'True' #
run_card_extras['scale'] = 125.5 #
run_card_extras['dsqrt_q2fact1'] = 125.5 #
run_card_extras['dsqrt_q2fact2'] = 125.5
run_card_extras['nevents'] = nevents
run_card_extras['cut_decays'] = 'False'

process_dir = new_process(process_str)

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_card_extras)

param_card_extras = {}
param_card_extras['DECAY'] = widths
param_card_extras['BSM'] = couplings
param_card_extras['MASS'] = masses

modify_param_card(param_card_input='aMcAtNlo_param_card_TRSM_'+topology+'.dat',process_dir=process_dir, params=param_card_extras)

#madspin_card=process_dir+'/Cards/madspin_card.dat'
#if os.access(madspin_card,os.R_OK):
#    os.unlink(madspin_card)
#mscard = open(madspin_card,'w')
#mscard.write("""#*********************************
#*      MadSpin  *
#*      *
#*
   
#set seed{0}
#set spinmode none
   
#decay t > b w+ , w+ > l+ vl
#decay t~ > b~ w- , w- > l- vl~
#decay h > b b~   
#decay eta0 > w+ w-, w+ > j j, w- > l- vl~

#decay H > b b~   
#decay w+ > j j
#decay w- > l- vl~

#launch""".format(runArgs.randomSeed))
#mscard.close()


#if not generate_gridpack:
#   madspin_card=process_dir+'/madspin_card.dat'
#   if os.access(madspin_card,os.R_OK):
#      os.unlink(madspin_card)
#   mscard = open(madspin_card,'w')
#   mscard.write("""#*********************************
   #*      MadSpin  *
   #*      *
   #*
   
   #set seed{0}
#   set spinmode none
   
#   decay h > b b~
#   decay eta0 > w+ w-, w+ > j j, w- > l- vl~
   
#   launch""".format(runArgs.randomSeed))
#   mscard.close()


generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=False)

evgenConfig.contact = ['anindya.ghosh@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

if not generate_gridpack:
   genSeq.Pythia8.Commands+=["99925:new = eta0 eta0 1 0 0 {} 0.01 50.0 0.0".format(M2), "99925:isVisible = true", "99925:mayDecay=true"]
   genSeq.Pythia8.Commands += [ 
                                '25:onMode = off',
                                '25:onIfMatch = 15 -15',
                                '99925:onMode = off',
                                '99925:addChannel 1 1.0 100 -5 5',
                              ]


if not generate_gridpack:
  from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
  filtSeq += ParentChildFilter("HbbFilter", PDGParent = [99925], PDGChild = [5])
  filtSeq += ParentChildFilter("HTauTauFilter", PDGParent = [25], PDGChild = [15])

  from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
  filtSeq += XtoVVDecayFilterExtended("TauTauHadHadFilter")
  filtSeq.TauTauHadHadFilter.PDGGrandParent = 25
  filtSeq.TauTauHadHadFilter.PDGParent = 15
  filtSeq.TauTauHadHadFilter.StatusParent = 2
  filtSeq.TauTauHadHadFilter.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
  filtSeq.TauTauHadHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
  
  
  #---------------------------------------------------------------------------------------------------
  # Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had) 
  #---------------------------------------------------------------------------------------------------
  from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
  filtSeq += MultiElecMuTauFilter("TauPtFilter")
  filtSeq.TauPtFilter.IncludeHadTaus = True
  filtSeq.TauPtFilter.NLeptons = 2
  filtSeq.TauPtFilter.MinPt = 13000.
  filtSeq.TauPtFilter.MinVisPtHadTau = 15000.
  filtSeq.TauPtFilter.MaxEta = 3.
  
  
  #---------------------------------------------------------------------------------------------------
  # Leading tau filter
  #---------------------------------------------------------------------------------------------------
  filtSeq += MultiElecMuTauFilter("LeadTauPtFilter")
  filtSeq.LeadTauPtFilter.IncludeHadTaus = True
  filtSeq.LeadTauPtFilter.NLeptons = 1
  filtSeq.LeadTauPtFilter.MinPt = 13000.
  filtSeq.LeadTauPtFilter.MinVisPtHadTau = 35000.
  filtSeq.LeadTauPtFilter.MaxEta = 3.
  
  
  filtSeq.Expression = "HbbFilter and HTauTauFilter and TauTauHadHadFilter and TauPtFilter and LeadTauPtFilter"
  
print("Batch has completed job options file.")
