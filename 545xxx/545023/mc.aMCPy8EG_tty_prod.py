import os, shutil
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *



# General settings
nevents=int(2*1.1*runArgs.maxEvents)

process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
set acknowledged_v3.1_syntax True --no_save
generate p p > t t~ a QED=1 QCD=2 [QCD]
output -f"""
                  
dec = """
define w+child = e+ mu+ ta+ ve vm vt u c d~ s~
define w-child = e- mu- ta- ve~ vm~ vt~ u~ c~ d s
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""

settings = {'lhe_version'	:'3.0',
			'maxjetflavor'	:5,
			'parton_shower'	:'PYTHIA8',
			'ptgmin'		:15.,
			'R0gamma'		:0.1,
			'xn'			:2,
			'epsgamma'		:0.1,
			'ptj'			:0.,
			'etal'			:-1.0,
			'etagamma'		:5.0,
			'mll_sf'		:0.,
			'dynamical_scale_choice':'3',
			'nevents'		:nevents
}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
set_top_params(process_dir,mTop=172.5,FourFS=False)
## Decay with MadSpin
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set BW_cut 50
set seed %i
%s
launch
"""%(runArgs.randomSeed, dec))
mscard.close()

# Print cards
print_cards()
# set up
generate(runArgs=runArgs, process_dir=process_dir)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Go to serial mode for Pythia8
check_reset_proc_number(opts)

# pythia shower
keyword=['SM','top',  'photon'] # 'ttgamma',  removed currently as error caused: generate 13:25:19 Py:Gen_tf           ERROR evgenConfig.keywords contains non-standard keywords: ttgamma. Please check the allowed keywords list and fix.

evgenConfig.keywords += keyword
evgenConfig.generators += ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.description = 'aMcAtNlo+Py8_ttgamma_nonallhad_GamFromProd'
evgenConfig.contact = ["jan.joachim.hahn@cern.ch","arpan.ghosal@cern.ch"]


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
