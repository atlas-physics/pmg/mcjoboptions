import os, shutil
import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

from MadGraphControl.MadGraphUtils import *

# General settings

evgenConfig.nEventsPerJob = 10000
nevents=int(runArgs.maxEvents)
if nevents==-1:
   nevents=evgenConfig.nEventsPerJob
nevents*=1.1


defs = """
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcdec = defs+"""
generate p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > ds uc~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > ds uc~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b), (t~ > l- vl~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > l- vl~ b~ a)
"""

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
"""+mcdec+"""
output -f
"""

settings = {'lhe_version'   :'3.0',
           'maxjetflavor'  :5,
           'cut_decays'    :'T',
           'ptgmin'        :15.,
           'R0gamma'       :0.1,
           'xn'            :2,
           'epsgamma'      :0.1,
           'ptj'           :0.,
           'xptl'          :0.,
           'etal'          :-1.0,
           'etaa'          :5.0,
           'mll_sf'		:0.,
           'dynamical_scale_choice':'3',
           'nevents'    :nevents}




process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
set_top_params(process_dir,mTop=172.5,FourFS=False)
# Print cards
print_cards()
# set up
generate(process_dir=process_dir,runArgs=runArgs)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)


check_reset_proc_number(opts)

#--------------------------------------------------------------
# EVGEN Configuration
#--------------------------------------------------------------
keyword=['SM','top','photon']
evgenConfig.keywords+=keyword
evgenConfig.generators += ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.description = 'MadGraph_ttgamma_nonallhad_GamFromDecay'
evgenConfig.contact = ["jan.joachim.hahn@cern.ch","atlas-generators-madgraphcontrol@cern.ch","arpan.ghosal@cern.ch"]
LHEfile=outputDS+".events"


#copy of fixIDWUTP.py to this place
foundInitTag = False
newlhe=open("temp.lhe",'w')
for line in open(LHEfile):
    newline = line

    if foundInitTag:
        print (line)
        vals=line.split(" ")[:]
        if len(vals) == 10:
            if vals[8] == "4": vals[8] = "3"
            if vals[8] == "-4": vals[8] = "-3"
            newline = " ".join(vals)
        print (newline   )
        foundInitTag = False

    if "<init>" in line:
        foundInitTag = True


    newlhe.write(newline)
newlhe.close()

shutil.move("temp.lhe",LHEfile)


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=LHEfile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()
