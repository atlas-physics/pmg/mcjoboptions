from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import os, shutil

mgmodels='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/'
if os.path.exists('mgmodels_local'):
    shutil.rmtree('mgmodels_local')

os.mkdir( "mgmodels_local" )
sys.path.append(os.getcwd() + '/mgmodels_local')
os.environ['PYTHONPATH'] = os.environ['PYTHONPATH'] + ':' + os.getcwd() + '/mgmodels_local'

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob


#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
splitConfig = jofile.rstrip('.py').split('_')
str_coupling = splitConfig[-1]

#---------------------------------------------------------------------------------------------------
# creating restriction param card
#---------------------------------------------------------------------------------------------------
model="SMEFTsim_U35_MwScheme_UFO"
restricted_model = "mgmodels_local/" + model + "_loc"
shutil.copytree(mgmodels+model,restricted_model)

chwtil_block = {
        '4': '1.5',
        }
mass_block = {
        '5': '0.00',
        '25': '1.250000e+02'
        }
yukawa_block = {
        '5': '0.00',
        }
modify_param_card(param_card_input=restricted_model+"/restrict_SMlimit_massless.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+str_coupling+'.dat',params={'SMEFTcpv': chwtil_block, "MASS": mass_block, "yukawa": yukawa_block})

model_extension = "masslessEFT_"+str_coupling

my_process = """
import model """+model+"""_loc-"""+model_extension+"""
set gauge unitary

define v = a z w+ w-
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~

generate p p > j j h $$ v QCD=0 NP=1, h > a a NP=0
output -f """

process_dir = new_process( my_process )

#---------------------------------------------------------------------------------------------------
# require beam energy to be set as argument
#---------------------------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#---------------------------------------------------------------------------------------------------
# creating run_card.dat
#---------------------------------------------------------------------------------------------------
extras = {
          'lhe_version'   : '3.0',
          'cut_decays'   : 'T',
          'drll'          : '0',
          'bwcutoff'      : '15.0',
          'pta'           : '0.',
		  'ptl'           : '0.',
          'etal'          : '-1.0',
          'drjj'          : '0.0',
          'draa'          : '0.0',
          'etaj'          : '-1.0',
          'draj'          : '0.0',
          'drjl'          : '0.0',
          'dral'          : '0.0',
          'etaa'          : '-1.0',
          'ptj'           : '10.',
          'ptj1min'       : '0.',
          'ptj1max'       : '-1.0',
          'mmjj'          : '0',
          'mmjjmax'       : '-1',
          'nevents'       : nevents,
          'ebeam1': beamEnergy,
          'ebeam2': beamEnergy,
          }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)


print_cards()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3)

#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "VBF 125 GeV Higgs production in the SMEFT model decaying to yy."
evgenConfig.keywords = ["Higgs", "VBF", "BSM", "mH125"]
evgenConfig.generators = ["MadGraph"]
evgenConfig.contact = ['Tong Qiu <tong.qiu@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
