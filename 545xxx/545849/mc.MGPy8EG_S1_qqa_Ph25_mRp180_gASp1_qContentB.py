mR          = 180
mDM         = 10000
gVSM        = 0.00
gASM        = 0.10
gVDM        = 0.00
gADM        = 1.00
filteff     = 0.111000
phminpt     = 25
quark_decays= ['b']

include("MadGraphControl_MGPy8EG_DMS1_dijetgamma_intParams.py")

evgenConfig.description = "Zprime with ISR - mR180 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Alex Gekow <alex.gekow@cern.ch>"]
