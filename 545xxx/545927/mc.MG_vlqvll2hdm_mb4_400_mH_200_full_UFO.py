# Job Options file
# Tells MG which model to import, if you want to use a restrict.dat file, and 
# any extra values/parameters you want to define. 
# THE JOBOPT FILE OVERWRITES RESTRICT AND PARAMETERS.PY FILES

### File location: Anywhere, must line up with run_madgraph.sh --jobConfig=/pathToJobOptsDir/
### Modify: process lines, import model (if using a restrict), under params={} (if changing extra params in this file, do most in restrict file)

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# Load model with no restrict (parameters.py file defaults): 
# import model vlqvll2hdm_full_UFO --modelname 
# Load model with restrict restrictName.dat: 
# import model vlqvll2hdm_full_UFO-restrictName --modelname 

process = """
import model vlqvll2hdm_full_UFO --modelname 
define p = g u c b d s u~ c~ d~ s~ b~
define j = g u c b d s u~ c~ d~ s~ b~
generate p p > vlqb~ vlqb, (vlqb~ > h02 b~, (h02 > b b~)), (vlqb > h02 b, (h02 > b b~))
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = {'nevents'    :int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params={}
# Modify other values here. Codes can be found in restrict file. Example: 
params['MASS'] = { 
#  'pdgID/code' : setValue # Description
  '7' : 400, # Mvlqb 
  '8' : 400, # Mvlqt 
  '35': 200  #Heavy higgs
}

modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# ############################
# # Shower JOs will go here
# evgenConfig.generators = ["MadGraph"]
# theApp.finalize()
# theApp.exit()

#### Shower           
# ***** The actual arguments below may not be correct
# ***** Currently looking into them
evgenConfig.description = 'MadGraph_VLL'
evgenConfig.keywords+=['ttbar','jets']
evgenConfig.contact = ["Rabia Omar <rabia.omar@cern.ch>"]
#evgenConfig.inputfilecheck = runName
#evgenConfig.inputfilecheck = tmp_LHE_events 
#evgenConfig.inputfilecheck="MG_vlqvll2hdm_full_UFO"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
