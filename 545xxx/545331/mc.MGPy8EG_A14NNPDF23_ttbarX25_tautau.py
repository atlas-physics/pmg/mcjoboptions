import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from os import path

safefactor=3.2
mode=0
nevents=runArgs.maxEvents/0.53*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor



process="""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
generate g g > t t~ h3, (h3 > ta+ ta-),(t > W+ b, W+ > j j),(t~ > W- b~, W- > l- vl~)
add process g g > t t~ h3, (h3 > ta+ ta-),(t > W+ b, W+ > l+ vl),(t~ > W- b~, W- > j j)
add process g g > t t~ h3, (h3 > ta+ ta-),(t > W+ b, W+ > l+ vl),(t~ > W- b~, W- > l- vl~)
output -f"""




beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
           'cut_decays':'F',
           'nevents':int(nevents)}


XMass  = { '25':'1.2500e+02',
           '35':'1.2500e+10',
           '36':'25',
           '37':'1.2500e+10',
}



decays={'WH3':'1.801e-02',
}


Params={}

Params['MASS']=XMass
Params['DECAY']=decays




process_dir = new_process(process)
print(dir(MadGraphControl.MadGraphUtils))
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
modify_param_card(process_dir=process_dir,params=Params)
print_cards()

generate(process_dir=process_dir,runArgs=runArgs)


outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)



#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = 'MG5 ttbarX to ta+ ta-, mX = 20 GeV'
evgenConfig.keywords+=['ttbar','jets','tau']
evgenConfig.contact = ['Damiano Vannicola <damiano.vannicola@cern.ch>',' Simone Francescato <simone.francescato@cern.ch>']
evgenConfig.nEventsPerJob=10000



#### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended   # lep filter

filtSeq += XtoVVDecayFilterExtended("XtoVVDecayFilterExtended")

filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 36
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]



