from MadGraphControl.MadGraphUtils import *

# General settings
nevents=runArgs.maxEvents*8.0 if runArgs.maxEvents>0 else 8.0*evgenConfig.nEventsPerJob
gridpack_dir=None
gridpack_mode=False # turn to True to create gridpack
cluster_type='lsf'
cluster_queue='8nh'
mode=2
njobs=132
runName='run_01'

### setting PDF and scale variations
# https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/507xxx/507372/MadGraphControl_NLO_HiggsCaracterisation_topHiggs.py
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
# adding NNPDF31_nlo_as_0118 to the standard 5fs list
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['alternative_pdfs'].append(303400)

### the generate call differs for the cases of using a nominal(SM) or modified(BSM) param card. Param card is set in JOs
### parameter values from: /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/param_card_CPalpha_*.dat

# -----------------------------------------
# MG5 Proc Card
# --

mgproc="generate p p > t w- x0 [QCD] @0\nadd process p p > t~ w+ x0 [QCD] @1\n"
name='twx0_5fl'
process="pp>twx0"
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''

process_string="""
import model HC_NLO_X0_UFO_v1p3-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
"""

extras = {'parton_shower'  :'PYTHIA8',
          'bwcutoff'       : 50.,
          'nevents'        : int(nevents),
          'dynamical_scale_choice' : 3 }

runName = 'madgraph.'+str(runArgs.jobConfig)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

if not is_gen_from_gridpack():
    process_dir = new_process(process_string)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

modify_param_card(process_dir=process_dir, params=parameters)

## DR for tWH
DR_mode = 1
from MadGraphControl.DiagramRemoval import do_DRX
do_DRX(DRmode=DR_mode, process_dir=process_dir)

# -----------------------------------------
# MadSpin Card
# -----------------------------------------

Nevents_for_max_weigth = 2000
max_weight_ps_point = 500
madspin_dir = 'MadSpin'
madspin_card_loc= process_dir+'/Cards/madspin_card.dat'
fMadSpinCard = open(madspin_card_loc,'w')
fMadSpinCard.write('set ms_dir '+process_dir+'/'+madspin_dir+'\n')
fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('#set use_old_dir True\n')

BW_cut=50.0
fMadSpinCard.write('set BW_cut {}\n'.format(BW_cut))
fMadSpinCard.write('set Nevents_for_max_weight {} # number of events for the estimate of the max. weight (default: 75)\n'.format(Nevents_for_max_weigth))
fMadSpinCard.write('set max_weight_ps_point {}  # number of PS to estimate the maximum for each event (default: 400)\n'.format(max_weight_ps_point))
fMadSpinCard.write(topdecay+"\nlaunch\n")
fMadSpinCard.close()
    
subprocess.call(["mv","-v",process_dir+'/Cards/madspin_card.dat','madspin_card.dat'])
subprocess.call(["cp","-r",process_dir,"process_dir_backup"])
# -----------------------------------------
# MG5 Generation
# -----------------------------------------

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)


#MadSpin hack: needed to properly handle diagram removal in madspin 
subprocess.call(["cp","-r","process_dir_backup",process_dir])
subprocess.call(["rm","-rf","process_dir_backup"])

log.info("----> Redecaying tWH events with MadSpin <------")
subprocess.call(["ls","-lahtr"])
subprocess.call(["mv","-v",'madspin_card.dat',process_dir+'/Cards/madspin_card.dat'])
# run MadSpin to create MadSpin files
if os.path.isfile("tmp_LHE_events.events"):
    madspin_on_lhe("tmp_LHE_events.events",madspin_card_loc,runArgs=runArgs,keep_original=True)
else:
    raise RuntimeError("tmp_LHE_events.events not found!")

        

# apply hacks on MadSpin files
from MadGraphControl.DiagramRemoval import do_MadSpin_DRX
#runName = MADGRAPH_RUN_NAME if not is_gen_from_gridpack() else 'GridRun_'+str(runArgs.randomSeed)
full_madspin_dir = process_dir+'/'+madspin_dir
do_MadSpin_DRX(DR_mode, full_madspin_dir)

# Make sure the code is taken from the madspin directory and not overwritten when you redo the decays
msfile=process_dir+'/Cards/madspin_card.dat'
mstilde=process_dir+'/Cards/madspin_card.dat~'
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir True' in line:
            line = line.replace('#','')
        myfile.write(line)
os.remove(mstilde)

# hack to be able to redecay the events
if os.path.isfile("tmp_LHE_events.events"):
    os.remove("tmp_LHE_events.events")
else:
    raise RuntimeError("tmp_LHE_events.events not found!")
do_hack_MS=subprocess.Popen(["cp", "tmp_LHE_events.events.original", "tmp_LHE_events.events"])
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(["rm", "tmp_LHE_events.events.original"])
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(["make","clean"], stdout=subprocess.PIPE, cwd=process_dir)
do_hack_MS.communicate()
log.info("----> First Compilation <------")
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The first compilation attempt fails, with errors written to the log
do_hack_MS.communicate()
log.info("----> Second Compilation <------")
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The second compilation attempt works, producing a new tarfile in the process directory
do_hack_MS.communicate()
log.info("----> Second Compilation completed <------")

# re-run MadSpin on undecayed lhe files with hack applied
if os.path.isfile("tmp_LHE_events.events"):
    madspin_on_lhe("tmp_LHE_events.events",madspin_card_loc,runArgs=runArgs,keep_original=False)
else:
    raise RuntimeError(lheFile+" not found!")

# remove process dir and madspin dir
shutil.rmtree(process_dir,ignore_errors=True)
if os.path.isdir('MGC_LHAPDF/'):
    shutil.rmtree('MGC_LHAPDF/',ignore_errors=True)
shutil.rmtree(madspin_dir,ignore_errors=True)

# -----------------------------------------
# Parton Showering Generation
# -----------------------------------------

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs   serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'aMcAtNlo tx0jb'
evgenConfig.keywords+=['Higgs', 'tHiggs']
#evgenConfig.inputfilecheck = outputDS
evgenConfig.contact = ['maria.giovanna.foti@cern.ch', 'shuo.han@cern.ch']
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("Pythia8_i/Pythia8_aMcAtNlo.py")

## we were asked to add MEC corrections .. Pythia8_aMcAtNlo_decayMEC.py
## Enable MG5_aMC@NLO LHEF reading in Pythia8
include("Pythia8_i/Pythia8_LHEF.py")
evgenConfig.generators += ["aMcAtNlo"]

#aMC@NLO default Pythia8 settings from http://amcatnlo.web.cern.ch/amcatnlo/list_detailed2.htm#showersettings
#plus MEC fix see https://arxiv.org/pdf/2308.06389.pdf
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = on",
                            "TimeShower:MEextended    = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "TimeShower:weightGluonToQuark = 1.",
                            "Check:epTolErr = 1e-2" ]

# -----------------------------------------
# Filters: include bb decay, single lep, hT filter
# -----------------------------------------

# bb decay filter                 
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])

# single lepton filter
# if not hasattr(filtSeq, "LeptonFilter"):
#     from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#     filtSeq += LeptonFilter()
include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 15000.
filtSeq.LeptonFilter.Etacut = 2.8

# ht filter
include("GeneratorFilters/HTFilter.py")
filtSeq.HTFilter.MinJetPt = 20.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 999. # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 200.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 13000.*GeV # Max HT to keep event
# filtSeq.HTFilter.TruthJetContainer = "AntiKt4TruthWZJets" # Which jets to use for HT
filtSeq.HTFilter.UseNeutrinosFromWZTau = False # Include neutrinos from W/Z/tau in the HT
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from W/Z/tau in the HT
