################################################################
# This is job options for 
# Madgraph LO Z->bb + 2 or more jets inclusive sample.
# Designed for VBF H->bb analysis background sample production.
################################################################
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

mgproc = """
import model sm 
"""

mgproc += """
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
"""

mgproc+="""
generate p p > z j j, z > b b~
"""

#define output 
mgproc+="""
output -f
"""
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

# safety factor for event generation
safefactor=200.
nevents=1000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

process_dir = new_process(mgproc)

# 'ickkw': 0 means no merging scheme, setting nevents sets the number of events also satisfied by the safety factor
settings = {'ickkw': 0,'nevents':nevents}

modify_param_card(process_dir=process_dir)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings )

print_cards()
    
generate(process_dir=process_dir,runArgs=runArgs)
################################################################
# Showering (for inclusive sample)
################################################################

#evgenConfig.generators += ["MadGraph", "Pythia8",  "EvtGen"]
evgenConfig.description = "MadGraph QCD samples for Zbb and jets"
#evgenConfig.nEventsPerJob = 5000
evgenConfig.contact = [ "Prajita Bhattarai <prajita.bhattarai@cern.ch>" ]
evgenConfig.keywords = ["QCD"]

arrange_output(process_dir=process_dir,runArgs=runArgs)

################################################################
# VBF Filtering of truth particles
################################################################
# Set up VBF filters
#import the relevant Generator filter
# This is necessary for xAOD filters
include("GeneratorFilters/FindJets.py")
include ("GeneratorFilters/VBFForwardJetsFilter.py")
CreateJets(prefiltSeq, 0.4) ## need to add "WZ" if want WZ jets, the 0.4 is radius Generators/GeneratorFilters/share/common/FindJets.py
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass

filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.JetMinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta=5.0
filtSeq.VBFForwardJetsFilter.NJets=2
filtSeq.VBFForwardJetsFilter.Jet1MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta=5.0
filtSeq.VBFForwardJetsFilter.Jet2MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta=5.0
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

# medium cut:
filtSeq.VBFForwardJetsFilter.MassJJ = 800.*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 3.5
