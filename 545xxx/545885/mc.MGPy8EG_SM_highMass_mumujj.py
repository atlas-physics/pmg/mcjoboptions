# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety

# Here is where we define the commands that will be passed to MadGraph
# Import the physics model
process = """
import model sm
"""

# Define the physics process to be simulated
process += """
generate p p > z | a > mu+ mu- j j QCD=0 weighted<=8 $$ w+ w- / ta+ ta- l+ l- j
"""

# This defines the MadGraph outputs
process += """
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process)
settings = {'ickkw': 0, 
        'nevents':nevents,
        'mmll': 140,
        'mmjj': 500}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir)

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'SM  VBF Z/a -> mu mu mll(140,inf).'
evgenConfig.contact = [ "Diego Baron <diego.baron@cern.ch>" ]
evgenConfig.keywords += ['2muon', 'vbf']

arrange_output(process_dir=process_dir, runArgs=runArgs)