from MadGraphControl.MadGraphUtils import *

# for details about the HC model, please check here: http://feynrules.irmp.ucl.ac.be/attachment/wiki/HiggsCharacterisation/README

# General settings
nevents=runArgs.maxEvents*14.0 if runArgs.maxEvents>0 else 14.0*evgenConfig.nEventsPerJob
gridpack_dir=None
gridpack_mode=False # turn to True to create gridpack
cluster_type='lsf'
cluster_queue='8nh'
mode=2
njobs=132
runName='run_01'

### setting PDF and scale variations
# https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/507xxx/507372/MadGraphControl_NLO_HiggsCaracterisation_topHiggs.py
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
### adding NNPDF31_nlo_as_0118_nf_4 to the standard 4fs list
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING['alternative_pdfs'].append(320500)

### the generate call differs for the cases of using a nominal(SM) or modified(BSM) param card. Param card is set in JOs
### parameter values from: /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/param_card_CPalpha_*.dat

# -----------------------------------------
# MG5 Proc Card
# -----------------------------------------

mgproc="generate p p > x0 t1 b1 j $$ w+ w- [QCD]"
name='tx0jb_4fl'
process="pp>tx0bj"
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''

process_string="""
import model HC_NLO_X0_UFO_v1p3-4Fnoyb
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define t1 = t t~
define b1 = b b~
"""+mgproc+"""
output -f
"""

# -----------------------------------------
# MG5 Run Card
# -----------------------------------------

extras = {'parton_shower'  :'PYTHIA8',
          'bwcutoff'       : 50.,
          'nevents'        : int(nevents),
          'dynamical_scale_choice' : 3 }

runName = 'madgraph.'+str(runArgs.jobConfig)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

if not is_gen_from_gridpack():
    process_dir = new_process(process_string)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

modify_param_card(process_dir=process_dir, params=parameters)

# -----------------------------------------
# MadSpin Card
# -----------------------------------------

madspin_card_loc='madspin_card.dat'

if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir MadSpin\n')
else:
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weigth 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()

# -----------------------------------------
# MG5 Generation
# -----------------------------------------

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# -----------------------------------------
# Parton Showering Generation
# -----------------------------------------

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'aMcAtNlo tx0jb'
evgenConfig.keywords+=['Higgs', 'tHiggs']
#evgenConfig.inputfilecheck = outputDS
evgenConfig.contact = ['maria.giovanna.foti@cern.ch','shuo.han@cern.ch']
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("Pythia8_i/Pythia8_aMcAtNlo.py") 

## we were asked to add MEC corrections .. Pythia8_aMcAtNlo_decayMEC.py
## Enable MG5_aMC@NLO LHEF reading in Pythia8
include("Pythia8_i/Pythia8_LHEF.py")
evgenConfig.generators += ["aMcAtNlo"]

#aMC@NLO default Pythia8 settings from http://amcatnlo.web.cern.ch/amcatnlo/list_detailed2.htm#showersettings
#plus MEC fix see https://arxiv.org/pdf/2308.06389.pdf
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = on",
                            "TimeShower:MEextended    = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "TimeShower:weightGluonToQuark = 1.",
                            "Check:epTolErr = 1e-2" ]


# -----------------------------------------
# Filters: include bb decay, single lep, hT filter
# -----------------------------------------

# bb decay filter                 
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])

# single lepton filter
# if not hasattr(filtSeq, "LeptonFilter"):
#     from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
#     filtSeq += LeptonFilter()
include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 15000.
filtSeq.LeptonFilter.Etacut = 2.8

# ht filter
include("GeneratorFilters/HTFilter.py")
filtSeq.HTFilter.MinJetPt = 20.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 999. # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 200.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 13000.*GeV # Max HT to keep event
# filtSeq.HTFilter.TruthJetContainer = "AntiKt4TruthWZJets" # Which jets to use for HT
filtSeq.HTFilter.UseNeutrinosFromWZTau = False # Include neutrinos from W/Z/tau in the HT
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from W/Z/tau in the HT
