########################################
## Note that this JO is a copy of 504329
## only difference is the addition of the 4l filter
########################################
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphParamHelpers import set_top_params
import os

# General settings
nevents       = int(1.1*runArgs.maxEvents) # safety factor
mllcut        = 5                          # mininum m(ll) mass (to cut off DY)
zdecay        = 'decay z > all all'        # placeholder
ptgmincut     = 20                         # photons with pT>20 GeV
lhe_version   = 3                          # LHE version to save reweighted scales+PDF
gridpack_dir  = 'madevent/'                # to generate from gridpack
gridpack_mode = True                       # we have gridpacks for everything so far
do_systs      = True                       # whether to reweight muR/muF and PDF

beamEnergy    = -999                       # safety check
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# Parse the name of the JO to extract information about the process
jo_name      = get_physics_short()
ttee         = 'ttee' in jo_name
ttmumu       = 'ttmumu' in jo_name
tttautau     = 'tttautau' in jo_name
ttZnunu      = 'ttZnunu' in jo_name
ttZqq        = 'ttZqq' in jo_name
lowmass      = 'lowmass' in jo_name
a14Var1Up    = 'A14Var1Up' in jo_name
a14Var1Down  = 'A14Var1Down' in jo_name
a14Var2Up    = 'A14Var2Up' in jo_name
a14Var2Down  = 'A14Var2Down' in jo_name
a14Var3aUp   = 'A14Var3aUp' in jo_name
a14Var3aDown = 'A14Var3aDown' in jo_name
a14Var3bUp   = 'A14Var3bUp' in jo_name
a14Var3bDown = 'A14Var3bDown' in jo_name
a14Var3cUp   = 'A14Var3cUp' in jo_name
a14Var3cDown = 'A14Var3cDown' in jo_name
pythia       = 'Py8' in jo_name
herwig       = 'H7' in jo_name
herwig721    = 'H721' in jo_name
herwig716    = 'H716' in jo_name
_4lfiltered  = '4lfilt' in jo_name

# Set MadGraph generate commands
if ttee:
    mgproc = '''generate p p > t t~ e+ e- [QCD]'''
    name   = 'ttee_NLO'
elif ttmumu:
    mgproc = '''generate p p > t t~ mu+ mu- [QCD]'''
    name   = 'ttmumu_NLO'
elif tttautau:
    mgproc = '''generate p p > t t~ ta+ ta- $$ h [QCD]'''
    name   = 'tttautau_NLO'
elif ttZnunu:
    mgproc = '''generate p p > t t~ z [QCD]'''
    name   = 'ttZnunu_NLO'
    zdecay = 'decay z > vl vl~'
elif ttZqq:
    mgproc = '''generate p p > t t~ z [QCD]'''
    name   = 'ttZqq_NLO'
    zdecay = 'decay z > q q~'
else:
    raise RuntimeError("If your JO doesn't concern ttee, ttmumu, tttautau, ttZnunu or ttZqq, you've imported the wrong control file, buddy!")

# Increase the intial generated events since we will throw out events in the 4l filter
if(_4lfiltered):
    nevents  = int(35*runArgs.maxEvents)

# Set the parton shower
if pythia:
    psname      = 'PYTHIA8'
elif herwig:
    psname      = 'HERWIGPP'
    do_systs    = False      # Herwig can't handle a reweighted LHE file
    lhe_version = 1          # therefore also pass a different LHE version than usual
else:
    raise RuntimeError("Can't tell whether the parton shower is supposed to be Pythia8 or Herwig7, stopping this madness now!")
    
# Write the proc_card
process = '''
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
'''+mgproc+'''
output -f
'''
process_dir = new_process(process)

# Low mass cuts: 1-5 GeV
if lowmass:
    mllcut = 1
    get_cuts_file = subprocess.Popen(['get_files','-data','ttZ_lowmass_cuts.f'])
    get_cuts_file.wait()
    import os, shutil
    if not os.path.exists('ttZ_lowmass_cuts.f'):
        raise RuntimeError("Cannot find ttZ_lowmass_cuts.f")
    shutil.copy('ttZ_lowmass_cuts.f', os.path.join(process_dir, 'SubProcesses/cuts.f'))

# Write the run_card
settings = {
    'nevents':        nevents,
    'maxjetflavor':   5,
    'parton_shower':  psname,
    'ptgmin':         ptgmincut,
    'mll_sf':         mllcut,
    'reweight_scale': '.false.',
    'reweight_PDF':   '.false.',
    'pdlabel':        'lhapdf',
    'lhaid':          260000,
}
if do_systs:
    settings['reweight_scale'] = '.true.'
    settings['rw_Rscale_down'] =  0.5
    settings['rw_Rscale_up']   =  2.0
    settings['rw_Fscale_down'] =  0.5
    settings['rw_Fscale_up']   =  2.0
    settings['reweight_PDF']   = '.true.'
    settings['PDF_set_min']    = 260001
    settings['PDF_set_max']    = 260100

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=settings)

# Modify the param card (important! this corrects the BR(W->had) issue in previous ttZ samples
set_top_params(process_dir,mTop=172.5,FourFS=False)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
set BW_cut 15                 # cut on how far the particle can be off-shell
set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
# specify the decay for the final state particles
define q = u d s c b
define q~ = u~ d~ s~ c~ b~
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
%s
# running the actual code
launch"""%(runArgs.randomSeed,zdecay))
mscard.close()

# Print and generate
print_cards()
generate(process_dir=process_dir,
         grid_pack=gridpack_mode,
         gridpack_compile=False,  # set to True to generate the gridpack, to False to run from it
         required_accuracy=0.001, # gridpack accuracy, ignored if running OTF
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=lhe_version)

# Metadata
evgenConfig.description      = 'aMcAtNlo_'+str(name)
evgenConfig.keywords        += ['SM','ttZ']
evgenConfig.contact          = ['baptiste.ravina@cern.ch']
evgenConfig.generators       = ['aMcAtNlo','EvtGen']
if pythia:
    evgenConfig.generators  += ['Pythia8']
if herwig:
    evgenConfig.generators  += ['Herwig7']
if a14Var1Up:
    evgenConfig.description += '_A14Var1Up'
if a14Var1Down:
    evgenConfig.description += '_A14Var1Down'
if a14Var2Up:
    evgenConfig.description += '_A14Var2Up'
if a14Var2Down:
    evgenConfig.description += '_A14Var2Down'
if a14Var3aUp:
    evgenConfig.description += '_A14Var3aUp'
if a14Var3aDown:
    evgenConfig.description += '_A14Var3aDown'
if a14Var3bUp:
    evgenConfig.description += '_A14Var3bUp'
if a14Var3bDown:
    evgenConfig.description += '_A14Var3bDown'
if a14Var3cUp:
    evgenConfig.description += '_A14Var3cUp'
if a14Var3cDown:
    evgenConfig.description += '_A14Var3cDown'

# go back to single-thread
check_reset_proc_number(opts)
# shower
if pythia:
    if a14Var1Up:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py")
    elif a14Var1Down:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py")
    elif a14Var2Up:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py")
    elif a14Var2Down:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py")
    elif a14Var3aUp:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py")
    elif a14Var3aDown:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py")
    elif a14Var3bUp:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py")
    elif a14Var3bDown:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py")
    elif a14Var3cUp:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
    elif a14Var3cDown:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py")
    else:
        include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")        
    include("Pythia8_i/Pythia8_aMcAtNlo.py")

    if(_4lfiltered):
        include("GeneratorFilters/FourLeptonInvMassFilter.py")
        filtSeq.FourLeptonInvMassFilter.MinPt   = 4.*GeV
        filtSeq.FourLeptonInvMassFilter.MaxEta  = 3.
        filtSeq.FourLeptonInvMassFilter.MinMass = 100.*GeV 
        filtSeq.FourLeptonInvMassFilter.MaxMass = 170.*GeV
        
        include("GeneratorFilters/FourLeptonMassFilter.py")
        filtSeq.FourLeptonMassFilter.MinPt           = 4000.
        filtSeq.FourLeptonMassFilter.MaxEta          = 4.
        filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
        filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
        filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
        filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
        filtSeq.FourLeptonMassFilter.AllowElecMu     = False
        filtSeq.FourLeptonMassFilter.AllowSameCharge = False

        include("GeneratorFilters/MultiLeptonFilter.py")
        filtSeq.MultiLeptonFilter.Ptcut    = 8000.
        filtSeq.MultiLeptonFilter.Etacut   = 3.
        filtSeq.MultiLeptonFilter.NLeptons = 3

elif herwig:
    evgenConfig.tune = "H7.1-Default"
    runArgs.inputGeneratorFile = outputDS+'.events'
    if herwig721:
        include("Herwig7_i/Herwig72_LHEF.py")
    elif herwig716:
        include("Herwig7_i/Herwig7_LHEF.py")
    Herwig7Config.me_pdf_commands(order="NLO",
                                  name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile,
                                       me_pdf_order="NLO")
    include("Herwig7_i/Herwig71_EvtGen.py")
    Herwig7Config.run()
