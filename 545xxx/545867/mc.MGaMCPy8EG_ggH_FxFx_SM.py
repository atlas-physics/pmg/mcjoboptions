#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "ggH FxFx, point-like coupling in infinite top mass limit, H->yy"
evgenConfig.keywords = ["SM","Higgs"]
evgenConfig.contact = ["ana.cueto@cern.ch", "yi.yu@cern.ch"]
evgenConfig.generators = ["MadGraph","Pythia8"]
evgenConfig.nEventsPerJob = 50000



# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*4 if runArgs.maxEvents>0 else 4*evgenConfig.nEventsPerJob

gridpack_mode=True


if not is_gen_from_gridpack():
    process = """
    import model HC_NLO_X0_UFO_v1p3-heft
    define p = g u c b d s u~ c~ d~ s~ b~
    define j = g u c b d s u~ c~ d~ s~ b~
    set acknowledged_v3.1_syntax True --no_save
    generate p p > x0 / t QED=0 QCD=2 QNP=1 [QCD] @0 
    add process p p > x0 j / t QED=0 QCD=3 QNP=1 [QCD] @1
    add process p p > x0 j j / t QED=0 QCD=4 QNP=1 [QCD] @2
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


#Fetch default LO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'req_acc':0.001,
            'ickkw':3,
            'ptj'  :10,
            'maxjetflavor': 5,
            'mll_sf'        : 10.0,
            #'reweight_scale': 'True',
            'fixed_ren_scale': 'False',
            'fixed_fac_scale': 'False',
            'muR_ref_fixed' : 125.0,
            'muF1_ref_fixed': 125.0,
            'muF2_ref_fixed': 125.0,
            'QES_ref_fixed' : 125.0,
            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
#modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor','cluster_queue':'workday'})

masses={'25': '1.250000e+02'}
frblock= {                  
                            'Lambda': 1000.0,
                            'cosa'  : 1.0,
                            'kSM'   : 1.0,
                            'kHgg'  : 1.0,
                            'kAgg'  : 0.0,
                            'kHll'  : 0.0,
                            'kAll'  : 0.0,
                            'kHaa'  : 0.0,
                            'kAaa'  : 0.0,
                            'kHza'  : 0.0,
                            'kAza'  : 0.0,
                            'kHzz'  : 0.0,
                            'kAzz'  : 0.0,
                            'kHww'  : 0.0,
                            'kAww'  : 0.0,
                            'kHda'  : 0.0,
                            'kHdz'  : 0.0,
                            'kHdwR' : 0.0,
                            'kHdwI' : 0.0,
}

params={}
params['MASS']=masses
params['frblock']=frblock
modify_param_card(process_dir=process_dir,params=params)

# #input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'
# madspin_card=process_dir+'/Cards/madspin_card.dat'
# #if os.access(madspin_card,os.R_OK):
# #    os.unlink(madspin_card)
# fMadSpinCard = open(madspin_card,'w')
# #fMadSpinCard.write('import '+input_events+'\n')
# #fMadSpinCard.write('set ms_dir '+process_dir+'/MadSpin\n')
# fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
# fMadSpinCard.write('''set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight (default: 75)
# set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
# set spinmode none
# decay x0 > a a
# launch''')
# fMadSpinCard.close()


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Helper for resetting process number
check_reset_proc_number(opts)

############################
# Shower JOs will go here
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")

# Matching settings
PYTHIA8_nJetMax=2
PYTHIA8_qCut=20.
include("Pythia8_i/Pythia8_FxFx_A14mod.py")

genSeq.Pythia8.Commands += [
    "JetMatching:qCutME           = 10.0",   # Should match ptj.
    # "JetMatching:doShowerKt       = off",
    # "JetMatching:jetAllow         = 1",
    # "JetMatching:nJet             = 0",
    # "JetMatching:exclusive        = 1",     # Exclusive - all PS jets must match HS jets.
    # "JetMatching:etaJetMax        = 4.5",
    # "JetMatching:clFact           = 1.0",
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
]

#avoid ME photons to decay y->ffbar in the shower
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
genSeq.Pythia8.useRndmGenSvc = False
