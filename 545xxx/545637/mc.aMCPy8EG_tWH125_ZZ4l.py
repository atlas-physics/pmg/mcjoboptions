include("MadGraphControl_tWH125_Common.py")

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")

genSeq.Pythia8.Commands += [
    'SLHA:useDecayTable = off',
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 23 23',
    '23:onMode = off',
    '23:mMin = 2.0',
    '23:onIfAny = 11 13 15'
]
