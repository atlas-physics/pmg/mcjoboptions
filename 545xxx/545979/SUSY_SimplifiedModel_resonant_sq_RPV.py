include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtils import *
JOName = get_physics_short()

# setup mass information
masses['1000006'] = float(JOName.split('_')[5]) #stop L
masses['1000022'] = float(JOName.split('_')[6]) #chi10

# setup process
process = '''
import model RPVMSSM_UFO
generate p p > t1~
add process p p > t1
''' 

decays["1000006"] = """DECAY   1000006   2.06904927E+00   # neutralino1 decays
#          BR       NDA       ID1       ID2
1.00000000E+00       2         6        1000022    # BR(~t1 -- t n1)"""

decays["1000022"] = """DECAY   1000022   1   # neutralino1 decays
#          BR       NDA       ID1       ID2      ID3
1.66666666E-01       3          1         3        6  # BR(~chi_10 -- d s t)
1.66666666E-01       3          1         5        6  # BR(~chi_10 -- d b t)
1.66666666E-01       3          3         5        6  # BR(~chi_10 -- s b t)
1.66666666E-01       3         -1        -3       -6  # BR(~chi_10 -- d~ s~ t~)
1.66666666E-01       3         -1        -5       -6  # BR(~chi_10 -- d~ b~ t~)
1.66666666E-01       3         -3        -5       -6  # BR(~chi_10 -- s~ b~ t~)"""

tR = '1'
tL = '0'

param_blocks['usqmix']={}
param_blocks['usqmix']['1 1']='1.00000000E+00'
param_blocks['usqmix']['2 2']='1.00000000E+00'
param_blocks['usqmix']['3 3']=tL
param_blocks['usqmix']['3 6']=tR
param_blocks['usqmix']['4 4']='1.00000000E+00'
param_blocks['usqmix']['5 5']='1.00000000E+00'
param_blocks['usqmix']['6 3']='-'+tR
param_blocks['usqmix']['6 6']=tL

# Chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='9.72557835E-01'     # V_11
param_blocks['VMIX']['1 2']='-2.32661249E-01'    # V_12
param_blocks['VMIX']['2 1']='2.32661249E-01'     # V_21
param_blocks['VMIX']['2 2']='9.72557835E-01'     # V_22
# Chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='9.16834859E-01'     # U_11
param_blocks['UMIX']['1 2']='-3.99266629E-01'    # U_12
param_blocks['UMIX']['2 1']='3.99266629E-01'     # U_21
param_blocks['UMIX']['2 2']='9.16834859E-01'     # U_22
# Neutralino mixing matrix
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']='9.86364430E-01'    # N_11
param_blocks['NMIX']['1  2']='-5.31103553E-02'   # N_12
param_blocks['NMIX']['1  3']='1.46433995E-01'    # N_13
param_blocks['NMIX']['1  4']='-5.31186117E-02'   # N_14
param_blocks['NMIX']['2  1']='9.93505358E-02'    # N_21
param_blocks['NMIX']['2  2']='9.44949299E-01'    # N_22
param_blocks['NMIX']['2  3']='-2.69846720E-01'   # N_23
param_blocks['NMIX']['2  4']='1.56150698E-01'    # N_24
param_blocks['NMIX']['3  1']='-6.03388002E-02'   # N_31
param_blocks['NMIX']['3  2']='8.77004854E-02'    # N_32
param_blocks['NMIX']['3  3']='6.95877493E-01'    # N_33
param_blocks['NMIX']['3  4']='7.10226984E-01'    # N_34
param_blocks['NMIX']['4  1']='-1.16507132E-01'   # N_41
param_blocks['NMIX']['4  2']='3.10739017E-01'    # N_42
param_blocks['NMIX']['4  3']='6.49225960E-01'    # N_43
param_blocks['NMIX']['4  4']='-6.84377823E-01'   # N_44

flavourScheme = 5

# setup basic information
evgenConfig.contact  = [ "minlin.wu@cern.ch" ]
evgenConfig.description = 'Resonance squark production with RPV'
evgenConfig.keywords += ['simplifiedModel','stop','RPV']

evt_multiplier = 2
njets = 0

include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

pythia = genSeq.Pythia8

genSeq.Pythia8.Commands += ["24:mMin = 0.2"]
genSeq.Pythia8.Commands += ["23:mMin = 0.2"]

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
