include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:addChannel = 1 0.002 100 313 22',
                             '313:mMax = 5'
                             ]

evgenConfig.process        = 'ttH H->KStar0Gamma'
evgenConfig.description    = 'aMcAtNloPythia8 ttH allhad, H to KStar0'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs','photon' ]
evgenConfig.contact = ["Govindraj Singh Virdee <g.s.virdee@cern.ch>"]
evgenConfig.generators  = [ "Pythia8","aMcAtNlo" ]
evgenConfig.inputFilesPerJob = 2 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000
