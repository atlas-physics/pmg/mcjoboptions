include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:addChannel = 1 0.002 100 423 22',
                             '25:addChannel = 1 0.002 100 -423 22',
                             '421:oneChannel = 1 1 100 -321 211',
                             '-421:oneChannel = 1 1 100 321 -211',
                             ]


evgenConfig.process        = 'ttH H->DStar0Gamma'
evgenConfig.description    = 'aMcAtNloPythia8 ttH allhad, H to DStar0'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs','photon' ]
evgenConfig.contact = ["Govindraj Singh Virdee <g.s.virdee@cern.ch>"]
evgenConfig.generators  = [ "Pythia8","aMcAtNlo" ]
evgenConfig.inputFilesPerJob = 2 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000

