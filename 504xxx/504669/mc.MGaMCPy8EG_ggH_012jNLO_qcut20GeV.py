#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators   += ["MadGraph", "Pythia8"]
evgenConfig.keywords      = ["SM","Higgs"]
evgenConfig.description   = 'ggf H->WW events with up to 2 partons and FxFx merging in infinite top mass limit'
evgenConfig.contact	  = ['Dominik Duda <dominik.duda@cern.ch>']
evgenConfig.nEventsPerJob = 1000

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*20.0 if runArgs.maxEvents>0 else 20.0*evgenConfig.nEventsPerJob

gridpack_mode=True
Compile_Gridpack=False

if Compile_Gridpack:
    process = """
    import model HC_NLO_X0_UFO-heft
    define p = g u c b d s u~ c~ d~ s~ b~
    define j = g u c b d s u~ c~ d~ s~ b~
    generate p p > x0 / t a [QCD] @0
    add process p p > x0 j / t a [QCD] @1
    add process p p > x0 j j / t a [QCD] @2
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

#Fetch default run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'req_acc':0.001,
            'ickkw'  :3,
            'ptl'    :7.0,
            'etal'   :3.0,
            'ptj'    :8,
            'jetradius' : 1.0,
            'mll_sf' :10.0,
            'nevents':int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

masses    = {'25': '1.250000e+02'}
frblock   = {               'kAza'  : 0.0,
                            'kAgg'  : 0.0,
                            'kAaa'  : 0.0,
                            'cosa'  : 1.0,
                            'kHdwR' : 0.0,
                            'kHaa'  : 0.0,
                            'kAll'  : 0.0,
                            'kHll'  : 0.0,
                            'kAzz'  : 0.0,
                            'kSM'   : 1.0,
                            'kHdwI' : 0.0,
                            'kHdz'  : 0.0,
                            'kAww'  : 0.0,
                            'kHgg'  : 1.0,
                            'kHda'  : 0.0,
                            'kHza'  : 0.0,
                            'kHww'  : 0.0,
                            'kHzz'  : 0.0,
                            'Lambda': 1000.0
}

params={}
params['MASS']=masses
params['frblock']=frblock
if Compile_Gridpack:
    modify_param_card(process_dir=process_dir,params=params)

if Compile_Gridpack:
   madspin_card=process_dir+'/Cards/madspin_card.dat'
   fMadSpinCard = open(madspin_card,'w')
   fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
   fMadSpinCard.write('''set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
   set spinmode none
   define l- = e- mu- ta-
   define l+ = e+ mu+ ta+
   decay x0 > w+ w- > l+ vl l- vl~   
   launch''')
   fMadSpinCard.close()

generate(process_dir=process_dir,runArgs=runArgs,gridpack_compile=Compile_Gridpack,grid_pack=gridpack_mode,required_accuracy=0.001)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = 20.0",
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 8.0",
                            "JetMatching:nJetMax          = 2",
                            "JetMatching:jetAlgorithm     = 2",
                            "JetMatching:slowJetPower     = 1", 
                            "JetMatching:nQmatch          = 5", 
                            "JetMatching:eTjetMin         = 20.0", 
                            "SpaceShower:rapidityOrder    = off", # FxFx + A14 prescription
                            "SpaceShower:pTmaxFudge       = 1.0", # FxFx + A14 prescription
]

genSeq.Pythia8.UserHooks += [ 'JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS    = True

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiElectronFilter("MultiElectronFilter")
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq += MultiLeptonFilter("MultiLeptonFilter")

MultiElectronFilter = filtSeq.MultiElectronFilter
MultiElectronFilter.Ptcut      = 7000.
MultiElectronFilter.Etacut     = 3.0
MultiElectronFilter.NElectrons = 1

MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut  = 7000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 1

MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut    = 16000.
MultiLeptonFilter.Etacut   = 3.0
MultiLeptonFilter.NLeptons = 1

filtSeq.Expression = "(MultiElectronFilter) and (MultiMuonFilter) and (MultiLeptonFilter)"

