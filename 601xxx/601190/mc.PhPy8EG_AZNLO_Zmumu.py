# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Zmumu production, AZNLO"
evgenConfig.keywords = ["SM", "Z", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 10000
filterMultiplier = 1.1

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Z_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
# AZNLO config
PowhegConfig.no_ew=1
PowhegConfig.ptsqmin=4
PowhegConfig.mass_low=60
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

#Gmu EW scheme inputs matching what used in the Powheg V1 Z samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

# Powheg integration and fold settings (samples are supposed to be used with integtation grids)
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 400000
PowhegConfig.ncall2       = 400000
PowhegConfig.nubound      = 1000000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 1
PowhegConfig.storemintupb = 0 # smaller grids



# historic CT10 10800 as central value for AZNLO tune, keep all default Powheg PDFs
PowhegConfig.PDF = [10800] + PowhegConfig.PDF
# add a selection of historic PDF central values
PowhegConfig.PDF += [325100, 13000,25300,61200,42560,303200] # NNPDF31_nnlo_as_0118_luxqed, CT14, MMHT14, HERAPDF2.0, ABMP16 5FS, NNPDF30_nnlo_as_0118_hessian
PowhegConfig.PDF += [11200, 21100, 21200, 10550] #CT10nnlo, MSTW2008nlo68cl, MSTW2008nnlo68cl, CTEQ6.6
# full error sets for latest PDFs
PowhegConfig.PDF += range(304401, 304500+1) # NNPDF31_nnlo_as_0118_hessian error set
PowhegConfig.PDF += range(331600, 331652+1) # NNPDF40_nnlo_hessian_pdfas (central as=0.118)
PowhegConfig.PDF += range(14001, 14058+1) # CT18NNLO error set
PowhegConfig.PDF += range(27401, 27464+1) # MSHT20nnlo_as118 error set


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
# this is needed to approximately reproduce low pT(V) in Pythia8.245 and Pythia8.3
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 1.4"]

# next level of Photos
genSeq.Photospp.CreateHistory = True # restore Photospp_i default that changed in 22.6
genSeq.Photospp.ZMECorrection = True
genSeq.Photospp.WMECorrection = False
genSeq.Photospp.PhotonSplitting = True
genSeq.Photospp.WtInterference = 4.0 # increase Photos upper limit for ME corrections
