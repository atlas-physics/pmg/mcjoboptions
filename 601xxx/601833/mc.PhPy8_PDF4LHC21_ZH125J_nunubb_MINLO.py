#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "ZH, Z->nunu, H->bb"
evgenConfig.description    = "POWHEG+PYTHIA8+EVTGEN, ZH+jet, Z->vv, H->bb"
evgenConfig.keywords       = [ "SM", "ZHiggs", "SMHiggs", "bbbar", "mH125" ]
evgenConfig.contact        = [ 'yiming.abulaiti@cern.ch' ]

##--------------------------------------------------------------
## Pythia8 showering
##--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']

else:
        genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

##--------------------------------------------------------------
## H->bb decay
##--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
        '25:onIfMatch = 5 -5' ]
        
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 140



#if runArgs.maxEvents > 0:
#    nevents=runArgs.maxEvents*1.5



