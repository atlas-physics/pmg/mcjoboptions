#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')


#--------------------------------------------------------------
mh=150


# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
#PowhegConfig.mass_H  = 125
#PowhegConfig.width_H = 0.00407
PowhegConfig.mass_H  = mh
PowhegConfig.width_H = 0.01
#PowhegConfig.mass_H  = mh
#PowhegConfig.width_H = 0.1


# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
#PowhegConfig.bwshape = 1
PowhegConfig.complexpolescheme = 0 # do not use CPS


# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
#PowhegConfig.runningscale = 2

# EW correction
#if PowhegConfig.mass_H <= 1000.:
#  PowhegConfig.ew = 1
#else:
#  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact (disabled for the time being!)
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
#PowhegConfig.masswindow = masswindow
#PowhegConfig.hfact = 38

# Additonal setting for MSSM group
#PowhegConfig.storeinfo_rwgt = 1
#PowhegConfig.hdecaymode = 0

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 15.

### Control generator systematics:
#PowhegConfig.PDF = range(90400,90433) + range(260000,260101) + [11068] + [25200] + [13165]
PowhegConfig.PDF = range(260000,260101) + range(90400,90433) + [11068] + [25200] + [13165]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]


#PowhegConfig.generateRunCard()
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
#include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update   / do we want this? ? /
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15'
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MSSM VBF Higgs mass 150GeV narrow width H->tautau->hh"
evgenConfig.keywords    = [ "BSMHiggs", "2tau" ]
evgenConfig.contact  = ["Pawel Bruckman de Renstrom <pawel.bruckman.de.renstrom@cern.ch>"]
evgenConfig.nEventsPerJob=1000


# ... Filter H->VV->Children
include("GeneratorFilters/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
#filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]

# Filter tau kinematics:
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  had30had20filter = TauFilter("had30had20filter")
  filtSeq += had30had20filter

filtSeq.had30had20filter.UseNewOptions = True
filtSeq.had30had20filter.Ntaus = 2
filtSeq.had30had20filter.Nleptaus = 0
filtSeq.had30had20filter.Nhadtaus = 2
filtSeq.had30had20filter.EtaMaxlep = 2.6
filtSeq.had30had20filter.EtaMaxhad = 2.6
filtSeq.had30had20filter.Ptcutlep = 10000.0 #MeV
filtSeq.had30had20filter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.had30had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.had30had20filter.Ptcuthad_lead = 30000.0 #MeV
