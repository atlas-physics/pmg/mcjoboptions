#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = 'Powheg+Herwig7 H+Wm+jet->4l+all production'
evgenConfig.keywords      = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.process       = "qq->WmH, H->4l, W->all"
evgenConfig.contact       = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators     += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7UE"
evgenConfig.inputFilesPerJob = 11
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC21_40_pdfas")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# only consider H->ZZ devays
Herwig7Config.add_commands("""
# force H->ZZ decays
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
# print out Higgs decays modes and branching ratios to the terminal/log.generate
do /Herwig/Particles/h0:PrintDecayModes
# force Z->ee/mumu decays
do /Herwig/Particles/Z0:SelectDecayModes Z0->e-,e+; Z0->mu-,mu+;
# print out Z decays modes and branching ratios to the terminal/log.generate
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()
