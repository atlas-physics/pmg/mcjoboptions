#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG gg->2e2mu 'no higgs' production."
evgenConfig.keywords = ["SM", "Higgs"]
evgenConfig.contact = ["andrej.saibel@cern.ch", "guglielmo.frattari@cern.ch"]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob    = 200

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bblvlv process
# -----------------------------------------------------------
include("PowhegControl/PowhegControl_gg4l_Common.py")
PowhegConfig.mass_b = 0 
PowhegConfig.proc = "ZZ"
PowhegConfig.contr = "no_h"
PowhegConfig.vdecaymodeV1 = 11
PowhegConfig.vdecaymodeV2 = 13
PowhegConfig.mllmin = 4
PowhegConfig.mllmax = 130
PowhegConfig.m4lmin = 80
PowhegConfig.m4lmax = 220

#Integration settings
PowhegConfig.ncall1 = 200000
PowhegConfig.itmx1 = 2
PowhegConfig.ncall2 = 200000
PowhegConfig.itmx2 = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2
PowhegConfig.foldphi = 5
PowhegConfig.nubound = 75000
PowhegConfig.icsimax = 2
PowhegConfig.iymax = 2
PowhegConfig.xupbound = 2
PowhegConfig.storemintupb = 1
PowhegConfig.fastbtlbound = 1
PowhegConfig.ubexcess_correct = 1
PowhegConfig.ncall1btlbrn = 50000
PowhegConfig.ncall2btlbrn = 100000
PowhegConfig.manyseeds = 1
PowhegConfig.parallelstage = 4
PowhegConfig.allrad = 1
PowhegConfig.withdamp = 1
PowhegConfig.m4l_sampling = 2
PowhegConfig.massiveloops = 1

PowhegConfig.mu_F         = 1.0
PowhegConfig.mu_R         = 1.0
PowhegConfig.PDF          = 260000


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()
