#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# H->mumu decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggF with MiNLO and A14 tune, H->mumu mH=125 GeV"
evgenConfig.keywords    = [ "SMHiggs", "gluonFusionHiggs", "2muon"]
evgenConfig.contact     = [ 'Yanlin Liu <yanlin.liu@cern.ch>' ]
evgenConfig.generators  = [ 'Powheg','Pythia8']
evgenConfig.inputFilesPerJob = 11
evgenConfig.nEventsPerJob = 20000
