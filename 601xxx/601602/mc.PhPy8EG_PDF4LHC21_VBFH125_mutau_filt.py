evgenConfig.process     = "VBF H->mutau"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->mutau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "mH125" ]
evgenConfig.contact     = [ 'antonio.de.maria@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.inputFilesPerJob = 2 
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:oneChannel = 1 0.5 100 15 -13',
	                     '25:addChannel = 1 0.5 100 13 -15' ]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lfvfilter = TauFilter("lfvfilter")
  filtSeq += lfvfilter

filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 1
filtSeq.lfvfilter.Nleptaus = 0
filtSeq.lfvfilter.Nhadtaus = 0
filtSeq.lfvfilter.EtaMaxlep = 2.6
filtSeq.lfvfilter.EtaMaxhad = 2.6
filtSeq.lfvfilter.Ptcutlep = 10000.0 #MeV
filtSeq.lfvfilter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 20000.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 20000.0 #MeV


