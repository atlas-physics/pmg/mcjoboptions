#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> inv + l+l- production
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_bbH_Common.py")

PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

PowhegConfig.runningscales = 2 #https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCforbbH 
PowhegConfig.toploop = 1 #https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCforbbH
PowhegConfig.msbar = 1 #https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCforbbH 

#PDFs in 4FS
PowhegConfig.PDF = list(range(93700,93743)) + list(range(92000,92031)) + list(range(260400,260501)) + [27610] + [334300]

# scale variations: first pair is the nominal setting
PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] 
PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

#improve default integration parameters using the numbers of 13 TeV production
PowhegConfig.itmx2     = 8
PowhegConfig.itmx1     = 8
PowhegConfig.ncall1    = 10000
PowhegConfig.ncall1rm  = 10000
PowhegConfig.ncall2    = 100000
PowhegConfig.ncall2rm  = 100000
PowhegConfig.nubound   = 500000
PowhegConfig.foldy = 5 # reduces fractions of negative weights
PowhegConfig.foldphi = 1
PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG  bbH  production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [  'ana.cueto@cern.ch']
evgenConfig.process = "bb->H"
evgenConfig.nEventsPerJob   = 11000
evgenConfig.generators= [ "Powheg"]
