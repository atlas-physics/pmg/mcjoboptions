#---------------------------------------------------------------
# LHE files of ggH used as inputs 
# POWHEG+Pythia8 ggH, H-> gam gam_d, mH=600GeV, my_d=0
#---------------------------------------------------------------
#Inputs are given in --inputGeneratorFile XXX
#Gen_tf.py --ecmEnergy=13000.0 --randomSeed=123456 --jobConfig=999999 --inputGeneratorFile=TXT.16395251._000001.events --outputEVNTFile=test.EVNT.root --maxEvents=10000
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 
#--------------------------------------------------------------
# H->y+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 600', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 22 4900022', # H->y+yd
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            ]
#--------------------------------------------------------------
# PhotonFilter for ggH->yyd
#--------------------------------------------------------------
if not hasattr( filtSeq, "PhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
    filtSeq += PhotonFilter()
    pass
PhotonFilter = filtSeq.PhotonFilter
PhotonFilter.NPhotons = 1
PhotonFilter.PtMin = 120000.
PhotonFilter.EtaCut = 2.5
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ggH production: H->y+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'gluonFusionHiggs' , 'photon' , 'darkPhoton' ]
evgenConfig.contact     = [ 'hassnae.el.jarrari@cern.ch' ]
evgenConfig.process = "gg->H, H->y+yd"
