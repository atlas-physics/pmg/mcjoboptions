include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_Powheg_Main31.py")
evgenConfig.description = 'Powheg+Pythia8+EvtGen MiNNLO WW->lvlv'
evgenConfig.generators  = [ "Powheg","Pythia8", "EvtGen" ]
evgenConfig.keywords+=['SM','diboson','WW','electroweak']
evgenConfig.contact     = [ 'hannes.mildner@cern.ch']
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000
