# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF opposite-sign WW production with AZNLO CTEQ6L1 tune."
evgenConfig.keywords = ["SM", "diboson", "WW", "VBF"]
evgenConfig.contact = ["christophe.pol.a.roland@cern.ch"]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune = "H7.1-Default"
evgenConfig.nEventsPerJob = 1000
# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_osWW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_osWW_Common.py")

PowhegConfig.decay_mode = "w+ w- > tau+ vt mu- vm~"

# Continuum only
PowhegConfig.ww_res_type = 1
PowhegConfig.bornsuppfact = 1


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate(use_XML_reweighting=False)

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")

Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()

dipoleShowerCommands = """
     cd /Herwig/EventHandlers
     set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
     """
Herwig7Config.add_commands(dipoleShowerCommands)


# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


