#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->vv ccar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs"]
evgenConfig.contact     = [ 'stephen.jiggins@cern.ch' ]
#evgenConfig.minevents = 50 #3 No longer supported in 21.6
evgenConfig.nEventsPerJob=10000
evgenConfig.process = "gg->ZH, H->cc, Z->vv"
evgenConfig.inputFilesPerJob=2

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']

#--------------------------------------------------------------
# Higgs->ccar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 4 4' ]
