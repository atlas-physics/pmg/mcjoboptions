#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# H->mumu decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603207.Ph_PDF4LHC21_ggZH125_Zincl_HZZ_batch2_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125", "ZHiggs" ]
evgenConfig.process     = "gg->ZH, Z->inc, H->mumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, gg->H+Z, Z->inc, H->mumu"
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob    = 100000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ 'Yanlin Liu <yanlin.liu@cern.ch>', 'michiel.jan.veen@cern.ch' ]
