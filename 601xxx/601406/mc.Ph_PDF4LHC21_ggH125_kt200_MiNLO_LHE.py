# EVGEN configuration
# Note:  This JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg ggF MiNLO PDF4LHC21'
evgenConfig.keywords       = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact        = [ 'andrea.sciandra@cern.ch' ]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob  = 2000

#--------------------------------------------------------------
# Powheg ggF setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Hj_Common.py')
PowhegConfig.mass_H = 125.0
PowhegConfig.width_H = 0.00407

# Filter 
PowhegConfig.bornktmin = 200

PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]
PowhegConfig.generate()

## Example Showering setup
##--------------------------------------------------------------
## Pythia8 showering with the A14 NNPDF 2.3 tune
##--------------------------------------------------------------
#include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
## note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
##include('Pythia8_i/Pythia8_Powheg_Main31.py')
#
#genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
#
##--------------------------------------------------------------
## Pythia8 corrections to BR for Higgs inclusive decay
##--------------------------------------------------------------
##--------------------------------------------------------------
## Higgs at Pythia8
##--------------------------------------------------------------
#genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
#        '25:onIfMatch = 5 -5' ]
##        '23:onMode = off', #decay of Z
##        '23:mMin = 2.0',
##        '23:onIfMatch = 11 11',
##        '23:onIfMatch = 13 13' ]



