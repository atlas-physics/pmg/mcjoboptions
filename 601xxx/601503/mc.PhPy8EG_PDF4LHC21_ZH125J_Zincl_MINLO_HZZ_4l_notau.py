#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603259.Ph_PDF4LHC21_ZH125J_Zincl_MINLO_HZZ_batch1_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "4lepton", "mH125", "ZHiggs" ]
evgenConfig.process     = "qq->ZH, Z->inc, H->ZZ->4l (no tau)"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MiNLO H+Z+jet production, Z->inc, H->ZZ->4l (no tau)"
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 10000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch', 'michiel.jan.veen@cern.ch' ]

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
  genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13']
