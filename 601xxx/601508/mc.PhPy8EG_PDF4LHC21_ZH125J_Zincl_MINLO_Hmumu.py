#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# H->mumu decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603260.Ph_PDF4LHC21_ZH125J_Zincl_MINLO_HZZ_batch2_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125", "ZHiggs" ]
evgenConfig.process     = "qq->ZH, Z->inc, H->mumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MiNLO H+Z+jet production, Z->inc, H->mumu"
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 10000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ 'Yanlin Liu <yanlin.liu@cern.ch>', 'michiel.jan.veen@cern.ch' ]
