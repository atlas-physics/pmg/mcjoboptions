evgenConfig.description = "Pythia8 jet gamma gamma -> qqbar, direct x resolved, using nuclear photon flux and NCTEQ15 PDFs with A14 tune"
evgenConfig.keywords = ["QED","QCD","coherent","jets"]
evgenConfig.contact = ["angerami@cern.ch"]
evgenConfig.nEventsPerJob   = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from Pythia8_i.Pythia8_iConf import UserPhotonFlux
photonFluxTool=UserPhotonFlux()
photonFluxTool.Process=3
photonFluxTool.NuclearCharge=82;
photonFluxTool.MinimumB=6.62;


from AthenaCommon.AppMgr import ToolSvc
ToolSvc += UserPhotonFlux()
genSeq.Pythia8.CustomInterface = photonFluxTool

#options to set up gamma* p collisions
genSeq.Pythia8.Beam1='ELECTRON'
genSeq.Pythia8.Beam2='ELECTRON'
genSeq.Pythia8.Commands += ["Photon:sampleQ2 = off",
                            "PDF:lepton2gamma = on",
                            "PDF:lepton2gammaSet = 2",
                            "PDF:lepton2gammaApprox = 2",
                            "PhaseSpace:pTHatMin = 5.0",
                            "Photon:Wmin  = 10.0"] #Wmin = 2 x pThatmin


#direct x resolved
genSeq.Pythia8.Commands +=["Photon:ProcessType = 3",
                           "PhotonParton:all = on"]

if hasattr(testSeq, "TestHepMC"):
    testSeq.TestHepMC.TachyonsTest=False


