# based on DSID=450694
evgenConfig.description = "Dijet truth jet slice JZ3, with the A14 NNPDF23 LO tune, filtered for 4 jets (2 b-jets)"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["bill.balunas@cern.ch", "mswiatlo@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection = on",
                            "PhaseSpace:pTHatMin = 50."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if not hasattr( filtSeq, "MultiBjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
  filtSeq += MultiBjetFilter()
  print ("MultiBjetFilter added ...")
  pass

# Example configuration
"""
MultiBjetFilter = filtSeq.MultiBjetFilter
MultiBjetFilter.LeadJetPtMin = 0
MultiBjetFilter.LeadJetPtMax = 50000
MultiBjetFilter.BottomPtMin = 5.0
MultiBjetFilter.BottomEtaMax = 3.0
MultiBjetFilter.JetPtMin = 15000
MultiBjetFilter.JetEtaMax = 2.7
MultiBjetFilter.DeltaRFromTruth = 0.3
MultiBjetFilter.TruthContainerName = "AntiKt4TruthJets"
"""


filtSeq.MultiBjetFilter.JetPtMin = 15000.
filtSeq.MultiBjetFilter.NJetsMin = 4
filtSeq.MultiBjetFilter.NBJetsMin = 2

CreateJets(prefiltSeq, 0.6 )
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(3,filtSeq)

evgenConfig.nEventsPerJob = 1000

