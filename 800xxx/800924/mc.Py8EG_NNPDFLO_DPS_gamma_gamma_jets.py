##############################################################
# JO  fragment for DPS 2(gamma + jets)
##############################################################
evgenConfig.description = "pp -> gamma + gamma (DPS) and jj "
evgenConfig.keywords = ["electroweak","MPI", "W"]
#evgenConfig.keywords = ["EM","MPI", "gamma"]
evgenConfig.contact = ["ranjit.nayak@cern.ch"]
evgenConfig.nEventsPerJob = 2000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ['TimeShower:QEDshowerByGamma = off']
genSeq.Pythia8.Commands += ['PromptPhoton:qqbar2ggamma = on']
genSeq.Pythia8.Commands += ['PromptPhoton:qg2qgamma = on'] 
genSeq.Pythia8.Commands += ['PromptPhoton:gg2ggamma = on']
genSeq.Pythia8.Commands += ['SecondHard:generate = on']
genSeq.Pythia8.Commands += ['SecondHard:PhotonAndJet = on']
genSeq.Pythia8.Commands += ['PhaseSpace:sameForSecond = off'] 
genSeq.Pythia8.Commands += ['PhaseSpace:pTHatMin = 35.']
genSeq.Pythia8.Commands += ['PhaseSpace:pTHatMinSecond = 30.']
