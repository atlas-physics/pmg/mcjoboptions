include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ1, with the A14 NNPDF23 LO tune, with b hadron filter "
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch","alex.emerman@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 5."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)    

if not hasattr( filtSeq, "MultiBjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
  filtSeq += MultiBjetFilter()
  print ("MultiBjetFilter added ...")
  pass

filtSeq.MultiBjetFilter.NBJetsMin = 1 
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(1,filtSeq)  
