include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ReducedShowerWeights.py")

evgenConfig.description = "Pythia8 dijet events with prompt (fragmentation) photons in 1000 < pT_ylead < 1500."
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 100
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch"]

## Configure Pythia
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PhaseSpace:Bias2Selection=on", 
                            "PhaseSpace:pTHatMin = 500",
]

genSeq.Pythia8.UserHooks = ["EnhanceSplittings"]
genSeq.Pythia8.Commands += [
                            "EnhanceSplittings:isr:Q2QA = 5",
                            "EnhanceSplittings:fsr:Q2QA = 5",
]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 1000000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 1500000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
