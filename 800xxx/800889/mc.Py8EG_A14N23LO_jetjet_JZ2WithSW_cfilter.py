include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ2, with the A14 NNPDF23 LO tune, with c hadron filter "
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch","alex.emerman@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 15."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)    

if not hasattr( filtSeq, "MultiCjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiCjetFilter
  filtSeq += MultiCjetFilter()
  print ("MultiCjetFilter added ...")
  pass

filtSeq.MultiCjetFilter.NCJetsMin = 1 
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(2,filtSeq)  
