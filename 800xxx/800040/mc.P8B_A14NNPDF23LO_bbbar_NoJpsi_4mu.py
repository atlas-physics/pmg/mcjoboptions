evgenConfig.description = "Pythia8B b b-bar -> 4 muon witout J/Psi"
evgenConfig.process = "b b-bar -> 4 mu witout J/Psi"
evgenConfig.keywords = ["4Muon","SM","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["yue.xu@cern.ch"]
evgenConfig.nEventsPerJob = 50

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
#genSeq.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
genSeq.Pythia8B.QuarkPtCut = 6.0
genSeq.Pythia8B.AntiQuarkPtCut = 6.0
genSeq.Pythia8B.QuarkEtaCut = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 7  # 1 (old value)

# Bc
genSeq.Pythia8B.Commands += ['541:onMode = off']
genSeq.Pythia8B.Commands += ['541:onIfAny = 13']
genSeq.Pythia8B.Commands += ['541:onIfAny = 15']
# B0
genSeq.Pythia8B.Commands += ['511:onMode = off']
genSeq.Pythia8B.Commands += ['511:onIfAny = 13']
genSeq.Pythia8B.Commands += ['511:onIfAny = 15']
# B+/-
genSeq.Pythia8B.Commands += ['521:onMode = off']
genSeq.Pythia8B.Commands += ['521:onIfAny = 13']
genSeq.Pythia8B.Commands += ['521:onIfAny = 15']
# Bs
genSeq.Pythia8B.Commands += ['531:onMode = off']
genSeq.Pythia8B.Commands += ['531:onIfAny = 13']
genSeq.Pythia8B.Commands += ['531:onIfAny = 15']
#LambdaB
genSeq.Pythia8B.Commands += ['5122:onMode = off']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 13']
genSeq.Pythia8B.Commands += ['5122:onIfAny = 15']
#Xi_b+-
genSeq.Pythia8B.Commands += ['5132:onMode = off']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 13']
genSeq.Pythia8B.Commands += ['5132:onIfAny = 15']
#Xi_b0
genSeq.Pythia8B.Commands += ['5232:onMode = off']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 13']
genSeq.Pythia8B.Commands += ['5232:onIfAny = 15']
#Omega_b
genSeq.Pythia8B.Commands += ['5332:onMode = off']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 13']
genSeq.Pythia8B.Commands += ['5332:onIfAny = 15']

#D0
genSeq.Pythia8B.Commands += ['421:onMode = off']
genSeq.Pythia8B.Commands += ['421:onIfAny = 13']
genSeq.Pythia8B.Commands += ['421:onIfAny = 15']
#D+/-
genSeq.Pythia8B.Commands += ['411:onMode = off']
genSeq.Pythia8B.Commands += ['411:onIfAny = 13']
genSeq.Pythia8B.Commands += ['411:onIfAny = 15']
#Ds
genSeq.Pythia8B.Commands += ['431:onMode = off']
genSeq.Pythia8B.Commands += ['431:onIfAny = 13']
genSeq.Pythia8B.Commands += ['431:onIfAny = 15']
#Lambda_c +/-
genSeq.Pythia8B.Commands += ['4122:onMode = off']
genSeq.Pythia8B.Commands += ['4122:onIfAny = 13']
genSeq.Pythia8B.Commands += ['4122:onIfAny = 15']

### Set lepton filters
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5, 2.5, 1.5] #GeV
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2, 3, 4]

### Set lepton filters
if not hasattr(filtSeq, "M4MuIntervalFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import M4MuIntervalFilter
   M4MuIntervalfilter = M4MuIntervalFilter("M4MuIntervalfilter")
   filtSeq += M4MuIntervalfilter

filtSeq.M4MuIntervalfilter.MinPt = 1500 #MeV
filtSeq.M4MuIntervalfilter.MaxEta = 2.7
filtSeq.M4MuIntervalfilter.LowM4muProbability = 1.0
filtSeq.M4MuIntervalfilter.MediumMj4muProbability = 0.5
filtSeq.M4MuIntervalfilter.HighM4muProbability = 0.1
filtSeq.M4MuIntervalfilter.LowM4mu = 11000.
filtSeq.M4MuIntervalfilter.HighM4mu = 25000.
filtSeq.M4MuIntervalfilter.ApplyReWeighting = True

