from MadGraphControl.MadGraphUtils import *

# Generate VH(hh), h->bb, Z->ll or Z->vv or W->lv

# parse the job arguments to get mA,mH and Z decay mode
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
mH = float(phys_short.split('_')[2][2:])
decay = (phys_short.split('_')[1][0:3])

width = 0.0

print "mH ",mH
print "Z decay ",decay

# a safe margin for the number of generated events
nevents=int(runArgs.maxEvents*1.1) 
mode=0 

# create the process string to be copied to proc_card_mg5.dat
process="""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~ a
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
# Define multiparticle labels
# Specify process(es) to run
"""
if decay=="Zll":
  process += """
generate     p p > z > z h2 / h3, z > l- l+, (h2 > h1 h1)
output -f
"""

if decay=="Zvv":
  process += """
generate     p p > z > z h2 / h3, z > vl vl~, (h2 > h1 h1)
output -f
"""
if decay=="Wlv":
  process += """generate     p p > w+ > l+ vl h2 / h3 h+ h- l+ l- t t~, (h2 > h1 h1) \n"""
  process += """add process  p p > w- > l- vl~ h2 / h3 h+ h- l+ l- t t~, (h2 > h1 h1) \n"""
  process += """output -f"""
print 'process string: ',process

#---------------------------------------------------------------------------------------------------
# Set masses and widths in param_card.dat
#---------------------------------------------------------------------------------------------------
mh1=125
mh2=mH
masses ={'25':mh1,  
         '35':mh2}
decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e-03 # Wh2'}
params = {}
params['mass'] = masses
params['decay'] = decays

print masses

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters (extras)
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':"'lhapdf'",
           'lhaid':247000} # NNPDF23_lo_as_0130_qed
extras['nevents'] = nevents

runName='run_01'     

# set up process
process_dir = new_process(process) 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)
    
print_cards()

# and the generation
generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)


#### Pythia8 Showering with A14_NNPDF23LO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
   
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'mass VH(%s GeV)->hh->bbbb' % (mH)

evgenConfig.keywords+=['BSMHiggs','bottom']
evgenConfig.inputfilecheck = runName
evgenConfig.contact = [ 'matthew.henry.klein@cern.ch' ]
evgenConfig.inputfilecheck = ""

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 -5' ]
