from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


# MadgGaph configuration
process = """
import model DMsimp_s_spin1

define l = e- e+ mu- mu+
define v = ve ve~ vm vm~ vt vt~

generate p p > y1, y1 > l v j j
add process p p > y1, y1 > l l j j
output -f"""


process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters


modify_run_card(process_dir=process_dir,
                settings={
                'nevents':runArgs.maxEvents*1.1,
                'lhe_version':'3.0',
                'cut_decays' :'True'
                }
                )


print_cards()

params = { 'DMINPUTS' : {'gVXc' : gVXc,
			 'gVXd' : gVXd,
			 'gAXd' : gAXd,
			 'gVd11' : gVd11,
			 'gVu11' : gVu11,
			 'gVd22' : gVd22,
			 'gVu22' : gVu22,
			 'gVd33' : gVd33,
			 'gVu33' : gVu33,
			 'gVl11' : gVl11,
			 'gVl22' : gVl22,
			 'gVl33' : gVl33,
			 'gAd11' : gAd11,
			 'gAu11' : gAu11,
			 'gAd22' : gAd22,
			 'gAu22' : gAu22,
			 'gAd33' : gAd33,
			 'gAu33' : gAu33,
			 'gAl11' : gAl11,
			 'gAl22' : gAl22,
			 'gAl33' : gAl33,
			 'gnu11' : gnu11,
			 'gnu22' : gnu22,
			 'gnu33' : gnu33,
			 'gVh' : gVh
			},
	   'MASS' : { 'MY1' : MY1,
		      'MXd' : MXd,
                      'MXr' : MXr,
                      'MXc' : MXc },
	   'DECAY' : { '5000001' : str(WY1) }
	 }

modify_param_card(process_dir=process_dir, params=params)

runName='run_01'

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)                                                                          
                                                                                                                                                                        
#### Shower                                                                                                                                                             
evgenConfig.description = 'p p > y1, y1 > l l/v j j - model DMsimp_s_spin1'
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = 'p p > y1, y1 > l l/v j j'
evgenConfig.contact = ['huan.yu.meng@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



