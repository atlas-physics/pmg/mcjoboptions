'''
Stop 3-body MG control with updated filters. 
Otherwise identical to https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/543xxx/543234/MadGraphControl_SimplifiedModel_directTT_bWN1.py
'''
from os import environ
from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')

madSpin = False
if 'MS' in tokens:
    madSpin = True
    tokens.remove('MS')

LepBR = False
if 'LepBR' in tokens:
    LepBR = True
    tokens.remove('LepBR')

try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]
    
productionmode = str(tokens[2])
if productionmode != 'TT':
    raise RunTimeError("ERROR: did not recognize productionmode {productionmode}")

decaymode = str(tokens[3])
stopMass = float(tokens[4])
neutralinoMass = float(tokens[5])


# Option
randomSeed = runArgs.randomSeed


masses['1000006'] = stopMass
masses['1000022'] = neutralinoMass #N1

njets = 2
# __And now production. Taking cues from higgsinoRPV.py generator__
process = '''
define susyall = go ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ h01 h2 h3 h+ sve svm svt el- mul- ta1- er- mur- ta2- h- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+ n1 n2 n3 n4 x1+ x2+ x1- x2-
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define c1 = x1+ x1-
define ll- = e- mu- ta-
define ll+ = e+ mu+ ta+
'''

process += '''
define exclude = susyall / t1 t1~ n1
generate    p p > t1 t1~     / exclude QED=0 @1
add process p p > t1 t1~ j   / exclude QED=0 @2
add process p p > t1 t1~ j j / exclude QED=0 @3
'''

if madSpin:
    # decay chains
    if decaymode == 'tN1':
        randomSeed += 0
        if LepBR:
            decay_chains = ["t1 > n1 t", "t1~ > n1 t~", "t > w+ b", "t~ > w- b~", "w+ > ll+ vl", "w- > ll- vl~"]
        else:
            decay_chains = ["t1 > n1 t", "t1~ > n1 t~", "t > w+ b", "t~ > w- b~", "w+ > fu fd~", "w- > fu~ fd"]
    elif decaymode == 'bWN1':
        randomSeed += 1
        if LepBR:
            decay_chains = [ "t1 > n1 ll+ vl b" , "t1~ > n1 ll- vl~ b~" ]
        else:
            decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~"] 
    elif decaymode == 'bffN1':
        randomSeed += 2
        if LepBR:
            decay_chains = [ "t1 > n1 ll+ vl b" , "t1~ > n1 ll- vl~ b~" ]
        else: 
            decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~"]
    elif decaymode == 'bC1':
        masses['1000024'] = neutralinoMass+0.5 #C1
        randomSeed += 0
        decay_chains = [ "t1 > x1+ b" , "t1~ > x1- b~"  ]
    else:
        raise RunTimeError("ERROR: did not recognize decaymode {decaymode}")

    evgenLog.info('Running w/ MadSpin option')
    madspin_card='madspin_card_test.dat'
    # fixEventWeightsForBridgeMode = True
    mscard = open(madspin_card,'w')
    mscard_to_write = '''
    #************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    #Some options (uncomment to apply)
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    #'''

    if randomSeed:
        mscard_to_write += '''
    set seed %i
        '''%(randomSeed)

    mscard_to_write += '''
    set spinmode none
    # specify the decay for the final state particles
    '''

    for decay_chain in decay_chains:
        mscard_to_write += '''
        decay %s
        '''%(decay_chain)
    
    mscard_to_write += '''
    # running the actual code
    launch
    '''
    mscard.write(mscard_to_write)
    mscard.close()

else: #not Madspin
    branchingRatiosC1 = '''
        # BR        NDA       ID1       ID2
         1.0          2        24   1000022 #C1 -> W + N1
    '''
    
    if decaymode == 'tN1':
        branchingRatiosT = '''
            # BR        NDA       ID1       ID2
             1.0          2         6   1000022 #t1 -> t + N1
        '''
    elif decaymode == 'bWN1':
        branchingRatiosT = '''
            # BR        NDA       ID1       ID2        ID3
             1.0          3         5        24    1000022  #t1 -> b + W+ + N1
        '''
    elif decaymode == 'bffN1':
        # From https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/507xxx/507131/SUSY_SimplifiedModel_TT_bffN.py
        branchingRatiosT = '''
            # BR            NDA         ID1       ID2       ID3       ID4
            1.078E-01         4     1000022         5       -11        12   # BR(stop1 --- chi10 b e nu)
            1.078E-01         4     1000022         5       -13        14   # BR(stop1 --- chi10 b mu nu)
            1.078E-01         4     1000022         5       -15        16   # BR(stop1 --- chi10 b tau nu)
            3.208525E-01      4     1000022         5         2        -1   # BR(stop1 --- chi10 b u dbar)
            1.74404E-02       4     1000022         5         4        -1   # BR(stop1 --- chi10 b c dbar)
            1.74639E-02       4     1000022         5         2        -3   # BR(stop1 --- chi10 b u sbar)
            3.201099E-01      4     1000022         5         4        -3   # BR(stop1 --- chi10 b c sbar)
            5.3E-06           4     1000022         5         2        -5   # BR(stop1 --- chi10 b u bbar)
            5.993E-04         4     1000022         5         4        -5   # BR(stop1 --- chi10 b c bbar)
            '''
    elif decaymode == 'bC1':
        branchingRatiosT = '''
            # BR        NDA       ID1       ID2
             1.0          2         5   1000024 #t1 -> b + C1
        '''
    elif decaymode == 'tN1bC1':
        branchingRatiosT = '''
            # BR        NDA       ID1       ID2
             0.5          2         6   1000022 #t1 -> t + N1
             0.5          2         5   1000024 #t1 -> b + C1
        '''
    else:
        raise RunTimeError("ERROR: did not recognize decaymode {decaymode}")

    branchingRatiosC1 = '''
        # BR        NDA       ID1       ID2     ID3
        1.078E-01         3     1000022         -11        12   # BR(cha1+ --- chi10 e nu)
        1.078E-01         3     1000022         -13        14   # BR(cha1+ --- chi10 mu nu)
        1.078E-01         3     1000022         -15        16   # BR(cha1+ --- chi10 tau nu)
        3.208525E-01      3     1000022           2        -1   # BR(cha1+ --- chi10 u dbar)
        1.74404E-02       3     1000022           4        -1   # BR(cha1+ --- chi10 c dbar)
        1.74639E-02       3     1000022           2        -3   # BR(cha1+ --- chi10 u sbar)
        3.201099E-01      3     1000022           4        -3   # BR(cha1+ --- chi10 c sbar)
        5.3E-06           3     1000022           2        -5   # BR(cha1+ --- chi10 u bbar)
        5.993E-04         3     1000022           4        -5   # BR(cha1+ --- chi10 c bbar)
    '''
    decays['1000006'] = 'DECAY   1000006  1.0' + branchingRatiosT
    decays['1000024'] = 'DECAY   1000024  1.0' + branchingRatiosC1


if njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])

if event_filter == '1L20orMET100':
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    if 'HEPMCVER' in environ and int(environ['HEPMCVER'])>=3:
        include ("GeneratorFilters/CreatexAODSlimContainers.py")
        createxAODSlimmedContainer("TruthMET",prefiltSeq)
        prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'
        from GeneratorFilters.GeneratorFiltersConf import xAODMETFilter
        xAODMissingEtFilter = xAODMETFilter("xAODMissingEtFilter")  
        filtSeq += xAODMissingEtFilter
        filtSeq.xAODMissingEtFilter.METCut = 100*GeV
        filtSeq.Expression = "LeptonFilter or xAODMissingEtFilter"
    elif 'HEPMCVER' in environ and int(environ['HEPMCVER']) < 3:
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
        filtSeq += MissingEtFilter()
        filtSeq.MissingEtFilter.METCut = 100*GeV
        filtSeq.Expression = "LeptonFilter or MissingEtFilter"

    masssplitting = stopMass - neutralinoMass
    evt_multiplier = 5. if masssplitting <= 10 else 3.
    
elif event_filter == '1L20':
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    masssplitting = stopMass - neutralinoMass
    evt_multiplier = 5. if masssplitting <= 10 else 3.
elif event_filter == 'MET100':
    if 'HEPMCVER' in environ and int(environ['HEPMCVER'])>=3:
        include ("GeneratorFilters/CreatexAODSlimContainers.py")
        createxAODSlimmedContainer("TruthMET",prefiltSeq)
        prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'
        from GeneratorFilters.GeneratorFiltersConf import xAODMETFilter
        xAODMissingEtFilter = xAODMETFilter("xAODMissingEtFilter")  
        filtSeq += xAODMissingEtFilter
        filtSeq.xAODMissingEtFilter.METCut = 100*GeV
    elif 'HEPMCVER' in environ and int(environ['HEPMCVER']) < 3:
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
        filtSeq += MissingEtFilter()
        filtSeq.MissingEtFilter.METCut = 100*GeV
    evt_multiplier = 13.
elif event_filter == '2L15':
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter()
    filtSeq.MultiElecMuTauFilter.MinPt  = 15000.
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.MultiElecMuTauFilter.NLeptons = 2
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
    evt_multiplier = 10. if LepBR else 50.

evgenConfig.contact = ["jmontejo@cern.ch", "tsano@cern.ch","mrimoldi@cern.ch","fdesanti@cern.ch","skandel@cern.ch","jwuerzin@cern.ch"]
evgenConfig.keywords +=['SUSY', 'gluino', 'neutralino']
if productionmode == 'TT' :
    evgenConfig.description = 'TT pair production'

if productionmode == 'TT' :
    evgenConfig.description += 'Stop pair production decay to qN1, m_T = %.1f GeV, m_N1 = %.1f GeV'%(masses['1000006'], masses['1000022'])
else:
    evgenConfig.description += 'SUSY strong pair production, m_parent = %.1f GeV, m_child = %.1f GeV'%(float(tokens[4]), float(tokens[5]))

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

