from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

prod  = shortname[4]
mAstr = shortname[5]
mHstr = shortname[6]
wAstr = shortname[7]

#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['mt'] = 172.5 # The top quark mass
THDMparams['mh2'] = float(mHstr[2:]) # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = float(mAstr[2:]) # The mass of the heavy pseudoscalar mass eigenstate $A$.
THDMparams['mhc'] = THDMparams['mh3'] # The mass of the charged scalar eigenstate $H^\pm$.
THDMparams['Awidth'] = float(wAstr[2:]) # A width: 0 means narrow width (1e-3), >0 means percentage of the mass

initialGluons = prod=='ggF' # Determines initial state to generate from. True --> top loop induced production (gluon fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details
decayChannel = 'llbb'

# Reweighting in tanb
reweight = False
reweights = [
]

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob = 10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------
include('MadGraphControl_Py8EG_2HDMII_AZH_llbb_common.py')
