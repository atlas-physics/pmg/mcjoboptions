mR          = 2000
mDM         = 10000
gVSM        = 0.00
gASM        = 0.25
gVDM        = 0.00
gADM        = 1.00
filteff     = 1.0
quark_decays= ['b']

include("MadGraphControl_MGPy8EG_DMS1_dijet_intParams.py")

evgenConfig.description = "Zprime to bbbar - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Bingxuan Liu <bingxuan.liu@cern.ch>"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.process = "p p --> zprime(b bbar) j j"
