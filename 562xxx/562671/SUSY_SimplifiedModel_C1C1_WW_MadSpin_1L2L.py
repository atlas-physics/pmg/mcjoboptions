### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

keepOutput = False # Debug

def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)
#------------------------

# C1/N2 denegenerate
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
joName = get_physics_short()
jobConfigParts = joName.split("_")
print("jobConfigParts",jobConfigParts )
  
dM=StringToFloat(jobConfigParts[3])-StringToFloat(jobConfigParts[4])
masses['1000023'] = StringToFloat(jobConfigParts[3])
masses['1000024'] = StringToFloat(jobConfigParts[3])
masses['1000022'] = StringToFloat(jobConfigParts[4])
print("masses",masses['1000023'] ,masses['1000024'] ,masses['1000022'] )
print(jobConfigParts[0])
if masses['1000022']<0.5: masses['1000022']=0.5

try:
    param_blocks.update(common_mixing_matrix('winobino'))
except:
    # Chargino mixing matrix V
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1 1']='9.72557835E-01'     # V_11
    param_blocks['VMIX']['1 2']='-2.32661249E-01'    # V_12
    param_blocks['VMIX']['2 1']='2.32661249E-01'     # V_21
    param_blocks['VMIX']['2 2']='9.72557835E-01'     # V_22
    # Chargino mixing matrix U
    param_blocks['UMIX']={}
    param_blocks['UMIX']['1 1']='9.16834859E-01'     # U_11
    param_blocks['UMIX']['1 2']='-3.99266629E-01'    # U_12
    param_blocks['UMIX']['2 1']='3.99266629E-01'     # U_21
    param_blocks['UMIX']['2 2']='9.16834859E-01'     # U_22
    # Neutralino mixing matrix
    param_blocks['NMIX']={}
    param_blocks['NMIX']['1  1']='9.86364430E-01'    # N_11
    param_blocks['NMIX']['1  2']='-5.31103553E-02'   # N_12
    param_blocks['NMIX']['1  3']='1.46433995E-01'    # N_13
    param_blocks['NMIX']['1  4']='-5.31186117E-02'   # N_14
    param_blocks['NMIX']['2  1']='9.93505358E-02'    # N_21
    param_blocks['NMIX']['2  2']='9.44949299E-01'    # N_22
    param_blocks['NMIX']['2  3']='-2.69846720E-01'   # N_23
    param_blocks['NMIX']['2  4']='1.56150698E-01'    # N_24
    param_blocks['NMIX']['3  1']='-6.03388002E-02'   # N_31
    param_blocks['NMIX']['3  2']='8.77004854E-02'    # N_32
    param_blocks['NMIX']['3  3']='6.95877493E-01'    # N_33
    param_blocks['NMIX']['3  4']='7.10226984E-01'    # N_34
    param_blocks['NMIX']['4  1']='-1.16507132E-01'   # N_41
    param_blocks['NMIX']['4  2']='3.10739017E-01'    # N_42
    param_blocks['NMIX']['4  3']='6.49225960E-01'    # N_43
    param_blocks['NMIX']['4  4']='-6.84377823E-01'   # N_44


madspindecays=False
if "MadSpin" in jobConfigParts :#and dM<100.:
  madspindecays=True

# Set Gen and Decay types 
gentype = str(jobConfigParts[1])
decaytype = str(jobConfigParts[2])
print("gentype",gentype)
print("decaytype",decaytype)

#----------------------------
# MadGraph options

decays['1000024'] = """DECAY   1000024     2.10722495E-01   # chargino1 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10   W )
  #
  """

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~
generate p p > x1+ x1- $ susystrong @1
add process p p > x1+ x1- j $ susystrong @2
add process p p > x1+ x1- j j $ susystrong @3
'''

mergeproc="{x1+,1000024}{x1-,-1000024}"
if dM > 130:
    msdecaystring="decay x1+ > w+ n1, w+ > f f \n decay x1- > w- n1, w- > f f \n "
else:
    raise RuntimeError("The Control file is not designed for the off-shell case.")
    msdecaystring="decay x1+ > l+ vl n1 \n decay x1- > l- vl~ n1 \n"


# print the process, just to confirm we got everything right
evgenLog.info( "ControlFile: Final process card " + process )


#--------------------------------------------------------------
# Madspin configuration
#
if madspindecays==True:
  if msdecaystring=="":
    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
  evgenLog.info('Active MadSpin')
  madspin_card='madspin_card.dat'
  mscard = open(madspin_card,'w')

  mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 15                # cut on how far the particle can be off-shell         
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s

# running the actual code
launch"""%(runArgs.randomSeed,msdecaystring))
  mscard.close()
  mergeproc+="LEPTONS,NEUTRINOS"


#
#--------------------------------------------------------------


# information about this generation
evgenLog.info('Registered generation of ~chi1+/~chi1- production, decay via WW; grid point '+str(jobConfigParts)+' decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))
evgenConfig.contact = [ "Valentina.Tudorache@cern.ch","yuchen.cai@cern.ch"]
evgenConfig.keywords += ['SUSY', 'gaugino', 'chargino', 'slepton', 'sneutrino']
evgenConfig.description = '~chi1+/~chi1- production, decay via WW in simplified model, m_C1C1 = %s GeV, m_N1 = %s GeV'%(masses['1000023'],masses['1000022'])


#--------------------------------------------------------------
#Filter and event multiplier 
evt_multiplier = 2

if '1L7not2L7' in jobConfigParts:
    evgenLog.info('ControlFile: Exact 1Lep Filter is applied.')
    evt_multiplier *= 2
    if dM==10.: evt_multiplier = 150
    
    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    f1L7Filter = xAODMultiElecMuTauFilter("f1L7Filter")
    filtSeq += f1L7Filter
    filtSeq.f1L7Filter.NLeptons = 1
    filtSeq.f1L7Filter.MinPt = 7000.0
    filtSeq.f1L7Filter.MaxEta = 2.8
    filtSeq.f1L7Filter.IncludeHadTaus = False
    
    f2L0Filter = xAODMultiElecMuTauFilter("f2L0Filter")
    filtSeq += f2L0Filter
    filtSeq.f2L0Filter.NLeptons = 2
    filtSeq.f2L0Filter.MinPt = 7000.0
    filtSeq.f2L0Filter.MaxEta = 2.8
    filtSeq.f2L0Filter.IncludeHadTaus = False

    filtSeq.Expression = "f1L7Filter and (not f2L0Filter)"

elif '1L7' in jobConfigParts:
    evgenLog.info('ControlFile: At least 1Lep Filter is applied.')
    evt_multiplier *= 2
    if dM==10.: evt_multiplier = 150
    
    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    f1L7Filter = xAODMultiElecMuTauFilter("f1L7Filter")
    filtSeq += f1L7Filter
    filtSeq.f1L7Filter.NLeptons = 1
    filtSeq.f1L7Filter.MinPt = 7000.0
    filtSeq.f1L7Filter.MaxEta = 2.8
    filtSeq.f1L7Filter.IncludeHadTaus = False
    filtSeq.Expression = "f1L7Filter"

elif '2L7' in jobConfigParts:
    evgenLog.info('ControlFile: At least 2Lep Filter is applied.')
    if dM==10.: evt_multiplier = 150
    elif dM <= 600.: evt_multiplier *= 10
    else: evt_multiplier *= 6
    
    include("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    filtSeq.xAODMultiElecMuTauFilter.NLeptons = 2
    filtSeq.xAODMultiElecMuTauFilter.MinPt = 7000.
    filtSeq.xAODMultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.xAODMultiElecMuTauFilter.IncludeHadTaus = 0
    filtSeq.Expression = "xAODMultiElecMuTauFilter"
    
elif '1L202L3' in jobConfigParts:
    evgenLog.info('1Lepton20 and 2Lepton3 filter is applied')

    if dM==10.: evt_multiplier = 150
    
    
    include("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    filtSeq += xAODMultiElecMuTauFilter("DiLeptonFilter")
    filtSeq += xAODMultiElecMuTauFilter("SingleLeptonFilter")
    
    xAODMultiElecMuTauFilter2 = filtSeq.DiLeptonFilter
    xAODMultiElecMuTauFilter2.NLeptons = 2
    xAODMultiElecMuTauFilter2.MinPt = 3000.
    xAODMultiElecMuTauFilter2.MaxEta = 2.8
    xAODMultiElecMuTauFilter2.IncludeHadTaus = 0

    xAODMultiElecMuTauFilter1 = filtSeq.SingleLeptonFilter
    xAODMultiElecMuTauFilter1.NLeptons = 1
    xAODMultiElecMuTauFilter1.MinPt = 20000.
    xAODMultiElecMuTauFilter1.MaxEta = 2.8
    xAODMultiElecMuTauFilter1.IncludeHadTaus = 0
    
    filtSeq.Expression = "SingleLeptonFilter and DiLeptonFilter"


elif '1L20orXe' in jobConfigParts:

    if dM==10.: evt_multiplier = 150
    
    evgenLog.info('ControlFile: 1Lep OR Xe filter is applied.')

    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    f1L3Filter = xAODMultiElecMuTauFilter("f1L3Filter")
    filtSeq += f1L3Filter
    filtSeq.f1L3Filter.NLeptons = 1
    filtSeq.f1L3Filter.MinPt = 3000.0
    filtSeq.f1L3Filter.MaxEta = 2.8
    filtSeq.f1L3Filter.MinVisPtHadTau = 3000.0
    filtSeq.f1L3Filter.IncludeHadTaus = False
    
    f1L20Filter = xAODMultiElecMuTauFilter("f1L20Filter")
    filtSeq += f1L20Filter
    filtSeq.f1L20Filter.NLeptons = 1
    filtSeq.f1L20Filter.MinPt = 20000.0
    filtSeq.f1L20Filter.MaxEta = 2.8
    filtSeq.f1L20Filter.MinVisPtHadTau = 20000.0
    filtSeq.f1L20Filter.IncludeHadTaus = False

  
    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 100000.0
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "f1L3Filter and (f1L20Filter or xAODMissingEtFilter)"
    if dM < 300:
        evt_multiplier *= 2


#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Pythia options
#
#evgenLog.info('Using Merging:Process = guess')
#genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
#genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 

#--------------------------------------------------------------
# Pythia options
# 0.2 is too small to be added, we can comment out these lines but better to keep them.
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]

#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>{x1+,1000024}{x1-,-1000024}",
                                 "1000024:spinType = 1",
                                 "-1000024:spinType = 1" ]
