from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

mAstr = shortname[5]
mHstr = shortname[6]
wAstr = shortname[7]

#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 5 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['lam3'] = 6.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 0.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 0.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.2 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['mt'] = 172.5 # The top quark mass
THDMparams['MXd'] = 10 # The dark matter mass
THDMparams['mh2'] = float(mHstr[2:]) # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = float(mAstr[2:]) # The mass of the heavy pseudoscalar mass eigenstate $A$.
THDMparams['mhc'] = THDMparams['mh3'] # The mass of the charged scalar eigenstate $H^\pm$.
THDMparams['mh4'] = 100 # The mass of the additional pseudoscalar mediator $a$
THDMparams['Awidth'] = float(wAstr[2:]) # A width: 0 means narrow width (1e-3), >0 means percentage of the mass

initialGluons = True # Determines initial state to generate from. True --> top loop induced production (gluon fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details
decayChannel = 'llbb'

# Reweighting in tanb
reweight = False
reweights = [
]

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob = 10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------
include('MadGraphControl_Py8EG_2HDMaI_AZH_llbb_ggF_common.py')
