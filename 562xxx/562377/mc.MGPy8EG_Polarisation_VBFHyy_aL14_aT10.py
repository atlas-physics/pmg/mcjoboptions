#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "VBF 125 GeV Higgs decaying to yy with modified couplings to polarised vector bosons (aL = 1.4 and aT = 1.0)"
evgenConfig.keywords    = ["Higgs", "VBF", "BSM", "mH125"]
evgenConfig.generators  = ["MadGraph"]
evgenConfig.contact     = ['Dominik Duda <dominik.duda@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 10000
