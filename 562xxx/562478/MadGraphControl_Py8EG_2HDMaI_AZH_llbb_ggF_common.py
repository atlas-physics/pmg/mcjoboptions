import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
# For the gluon-gluon fusion production use the 4FS
process = '''
import model Pseudoscalar_2HDMI-aligned
define p = g d u s c d~ u~ s~ c~
define j = g d u s c d~ u~ s~ c~
generate g g > h2 z aS=2 aEW=2 [QCD]
output -f
'''

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RuntimeError('No center of mass energy found.')

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------

nevents = evgenConfig.nEventsPerJob
# to account for the filter efficiency + safety margin
multiplier = 1.5

nevents = runArgs.maxEvents*multiplier if runArgs.maxEvents > 0 else nevents*multiplier
nevents = int(nevents)

skip_events = 0
if hasattr(runArgs, 'skipEvents'): skip_events = runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'maxjetflavor'  : 4,
    'asrwgtflavor'  : 4,
    'lhe_version'   : '3.0',
    'cut_decays'    : 'F',
    'nevents'       : nevents,
}
# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir = process_dir, runArgs = runArgs, settings = extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params = {}
## blocks might be modified
dict_blocks = {
'mass': ['mt', 'MXd', 'mh2', 'mh3', 'mhc', 'mh4'],
'DMINPUTS' : ['gPXd'],
'FRBlock': ['tanbeta', 'sinbma'],
'Higgs': ['lam3', 'laP1', 'laP2', 'sinp'],
}

for bl in dict_blocks.keys():
    for pa in dict_blocks[bl]:
        if pa in THDMparams.keys():
            if bl not in params: params[bl] = {}
            if pa=='MB': 
                params[bl]['5']=THDMparams[pa]
            else:
                params[bl][pa] = THDMparams[pa]

## auto calculation of decay width
THDMparams_decay = {
'25': 'Auto',
'35': 'Auto',
'37': 'Auto',
'55': 'Auto',
}
THDMparams_decay['36'] = '1e-3' if THDMparams['Awidth'] == 0 else str(params['mass']['mh3']*THDMparams['Awidth']/100.)

params['decay'] = THDMparams_decay

print('Updating parameters:')
print(params)

modify_param_card(process_dir = process_dir, params = params)

# Build reweight_card.dat
if reweight:
    print(params)
    # Create reweighting card
    reweight_card_loc = process_dir + '/Cards/reweight_card.dat'
    rwcard = open(reweight_card_loc, 'w')

    for rw_name in reweights:
        params_rwt = params.copy()
        for param in rw_name.split('-'):
            param_name, value = param.split('_')
            if param_name == 'SINP': params_rwt['HIGGS']['SINP'] = value
            elif param_name == 'TANB': params_rwt['FRBLOCK']['TANBETA'] = value

        param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
        shutil.copy(process_dir + '/Cards/param_card.dat', param_card_reweight)
        param_card_rwt_new = process_dir + '/Cards/param_card_rwt_%s.dat' % rw_name
        modify_param_card(param_card_input = param_card_reweight, process_dir = process_dir, params = params_rwt, output_location = param_card_rwt_new)

        rwcard.write('launch --rwgt_info=%s\n' % rw_name)
        rwcard.write('%s\n' % param_card_rwt_new)
    rwcard.close()

# --------------------------------------------------------------
# madspin_card
# --------------------------------------------------------------
madspin_command = '''
decay z > l+ l-
decay h2 > b b~
'''

madspin_card = process_dir + '/Cards/madspin_card.dat'
if os.access(madspin_card, os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card, 'w')
mscard.write('''#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                 # cut on how far the particle can be off-shell
# set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles
%s
# running the actual code
launch''' % (runArgs.randomSeed, madspin_command))
mscard.close()

print_cards()

#---------------------------------------------------------------------------
# Generate the events
#---------------------------------------------------------------------------
generate(process_dir = process_dir, runArgs = runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version = 3, saveProcDir = False)

#---------------------------------------------------------------------------
# Metadata
#---------------------------------------------------------------------------

evgenConfig.process = 'g g > h2 z aS=2 aEW=2 [QCD]'
initialStateString = 'gluon fusion'
decayString = 'll b bbar'

evgenConfig.description = '2HDMa type-I for ZH(->%s) %s initiated production with mA = %s, mH = %s, ma = %s' % (decayString, initialStateString, str(THDMparams['mh3']), str(THDMparams['mh2']), str(THDMparams['mh4']))
evgenConfig.keywords = ['BSMHiggs','ZHiggs']
evgenConfig.contact = ['tetiana.moskalets@cern.ch']

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

# Use lepton filter
include('GeneratorFilters/MultiElecMuTauFilter.py')
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = False
filtSeq.MultiElecMuTauFilter.NLeptons = 1
filtSeq.MultiElecMuTauFilter.MinPt = 18000.
filtSeq.MultiElecMuTauFilter.MaxEta = 5.
