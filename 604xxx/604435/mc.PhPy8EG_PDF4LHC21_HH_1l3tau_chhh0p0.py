#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to 1l3tau, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "tau"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 200
evgenConfig.inputFilesPerJob  = 69


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# -- #Ele + #Mu >= 1
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- #Ele + #Mu >= 2
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import TauFilter
# #Tau >= 3
filtSeq += TauFilter("HadTauThreeFilter")
filtSeq.HadTauThreeFilter.UseNewOptions = True
filtSeq.HadTauThreeFilter.UseMaxNTaus = True
filtSeq.HadTauThreeFilter.Nhadtaus = 3
filtSeq.HadTauThreeFilter.Ptcuthad = 15000.
filtSeq.HadTauThreeFilter.EtaMaxhad = 2.8
filtSeq.HadTauThreeFilter.Ptcutlep_lead = 0.


# -- Requirement 1: (#Ele + #Mu == 1) and (#Tau >= 3)
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="( (ElecMuOneFilter and not ElecMuTwoFilter and HadTauThreeFilter) )" +\
                    "and (LeadingElecMuFilter or not ElecMuOneFilter)"

