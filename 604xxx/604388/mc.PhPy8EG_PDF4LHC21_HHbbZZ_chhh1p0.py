#--------------------------------------------------------------                                                                                                                                                                                                           
# EVGEN configuration                                                                                                                                                                                                                                             
#--------------------------------------------------------------                                                                                                                                                                                               
evgenConfig.description    = "diHiggs production with chhh=1.0, using Powheg-Box-V2 at NLO + full top mass, with PDF4LHC21 PDF. Decay to bbZZ and PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant"]
evgenConfig.contact = [ "giuseppe.callea@cern.ch" ]
evgenConfig.nEventsPerJob  = 20

# --------------------------------------------------------------                                                                                                                                                                                                     
# Configuring Powheg                                                                                                                                                                                                                                                
# --------------------------------------------------------------                                                                                                                                                                                                      
### Load ATLAS defaults for the Powheg ggF_HH process                                                                                                                                                                                                                  
include("PowhegControl/PowhegControl_ggF_HH_Common.py")
### important parameters                                                                                                                                                                                                                                                
PowhegConfig.mtdep = 3 # full theory                                                                                                                                                                                                                          
PowhegConfig.mass_H = 125 # Higgs boson mass - need to stick to that value as it is hardcoded in the ME                                                                                                                                                     
PowhegConfig.mass_t = 173 # top-quark mass - need to stick to that value as it is hardcoded in the ME                                                                                                                                             
PowhegConfig.hdamp = 250
### Modify coupling                                                                                                                                                                                                                                           
PowhegConfig.chhh = 1.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]                                                                                                                                                                                            
### accounting for Pythia8 and filter efficiencies                                                                                                                                                                                                                    
PowhegConfig.nEvents *= 5
### scales and PDF sets                                                                                                                                                                                                                                     
PowhegConfig.PDF = list(range(93300,93343)) # PDF4LHC21_40_pdfas with error sets                                                                                                                                                                                              
PowhegConfig.PDF += list(range(90400,90433)) # PDF4LHC15_nlo_30_pdfas with error sets                                                                                                                                                                                   
PowhegConfig.PDF += list(range(325300,325403)) # NNPDF31_nnlo_as_0118_mc_hessian_pdfas with error sets                                                                                                                                                             
PowhegConfig.PDF += [27100,14400,331700] # MSHT20nlo_as118, CT18NLO, NNPDF40_nlo_as_01180 nominal sets                                                                                                                                                            
PowhegConfig.mu_F = [1.0, 0.5, 0.5, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0]
### Generate events                                                                                                                                                                                                                                                  
PowhegConfig.generate()
# Configuring Pythia8                                                                                                                                                                                                                                                          
#--------------------------------------------------------------                                                                                                                                                                                                                
### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine                                                                                                                                                                                                          
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
### main31 routine for showering Powheg events with Powheg                                                                                                                                                                                                                     
include("Pythia8_i/Pythia8_Powheg_Main31.py")
### 2 particles in the final state                                                                                                                                                                                                                                          
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

### selectin Higgs decays                                                                                                                                                                                                                                                 
genSeq.Pythia8.Commands += ["25:oneChannel = 1 0.5 100 23 23",  
                            "25:addChannel = 1 0.5 100 5 -5",
                            '23:oneChannel = 1 0.2354 100 11 -11',
                            '23:addChannel = 1 0.2354 100 13 -13',
                            '23:addChannel = 1 0.0292 100 15 -15',
                            '23:addChannel = 1 0.5 100 12 -12',                
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "24:mMin = 0", # W minimum mass
                            "24:mMax = 99999", # W maximum mass
                            "23:mMin = 0", # Z minimum mass
                            "23:mMax = 99999", # Z maximum mass
                            "TimeShower:mMaxGamma = 0"  # Z/gamma* combination scale
                            ]


### Dipole shower option                                                                                                                                                                                                                                                 
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------                                                                                                                                                                           
# Filter for bbZZ                                                                                                                                                                                                                                         
#---------------------------------------------------------------------------------------------------                                                                                                                                                                         
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HZZFilter", PDGParent = [25], PDGChild = [23])
filtSeq += ParentChildFilter("ZvvFilter", PDGParent = [23], PDGChild = [12])
filtSeq += ParentChildFilter("ZllFilter", PDGParent = [23], PDGChild = [11,13,15])
filtSeq.Expression = "HbbFilter and HZZFilter and ZvvFilter and ZllFilter"


