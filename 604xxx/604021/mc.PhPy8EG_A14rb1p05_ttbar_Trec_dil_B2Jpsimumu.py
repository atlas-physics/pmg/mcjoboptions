# original JO: 601357
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune with rb=1.05, with the recoil-to-top user hook for diepton events, B->Jpsi->mumu filter.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'aknue@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 10


#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")  
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# A14rb tune
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]

# set the user hook for recoil to top
genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured=off' ]
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.UserHooks += ['TopRecoilHook']

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
           
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#--------------------------------------------------------------
# Special decay of B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['B2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'B2Jpsimumu.DEC'

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
