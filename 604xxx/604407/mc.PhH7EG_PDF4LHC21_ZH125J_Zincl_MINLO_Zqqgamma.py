#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC21_40_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# only consider H->gammagamma devays
Herwig7Config.add_commands("""
  # force H->yy decays
  do /Herwig/Particles/h0:SelectDecayModes h0->Z0,gamma;
  # force Z->qq decays
  do /Herwig/Particles/Z0:SelectDecayModes Z0->d,dbar; Z0->s,sbar; Z0->b,bbar; Z0->u,ubar; Z0->c,cbar;
  # print out decays modes and branching ratios to the terminal/log.generate 
  do /Herwig/Particles/h0:PrintDecayModes
  do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'POWHEG+HERWIG7+EVTGEN, Zh Zincl MINLO, H->Z gam, Z->qq mh=125 GeV'
evgenConfig.keywords       = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact        = [ 'tong.qiu@cern.ch' ]
evgenConfig.generators     = [ 'Powheg', 'Herwig7' ]
evgenConfig.tune    = "H7.2-Default"
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 10000
