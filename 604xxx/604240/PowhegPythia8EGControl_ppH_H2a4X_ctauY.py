#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------
import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

###Run Number Encoding and decoding
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
print(get_physics_short())
tokens = get_physics_short().replace(".py","").split('_')

ctau = float(tokens[-2].split('ctau')[-1])
ma   = float(tokens[-4].split('a')[-1])

decayProcess = str(tokens[-3])
prodMode = str(tokens[-6])

filtered = True

print(str(tokens[-1]))

if  str(tokens[-1]) == "filtered" :
    filtered = True
elif str(tokens[-1]) == "unfiltered" :
    filtered = False

print(f"Filtered flag is {filtered}")
    
print('#############################################################')
print('ma ='+str(ma))
print('ctau ='+str(ctau))
print('decayProcess ='+str(decayProcess))
print('#############################################################')

if decayProcess == "4b":
    adecay= 5
elif decayProcess == "4u":
    adecay= 2
elif decayProcess == "4c":
    adecay= 4
elif decayProcess == "4tau":
    adecay= 15
elif decayProcess == "4g":
    adecay= 21
else:
    print("Decay process not availabe. Recheck Job specific JO name or rewrite general JO code")

if adecay==21:
    adecay1 = adecay
    adecay2 = adecay
else:
    adecay1 = adecay
    adecay2 = -adecay

m_ma=ma-0.5
p_ma=ma+0.5
width=(1.9732699E-13)/ctau
nProcess=1 


if prodMode == "WpH":
    nProcess = 0 
if prodMode == "WmH":
    nProcess = 1
if prodMode == "ZllH":
    nProcess = 2 
if prodMode == "ggZllH":
    nProcess = 3
if prodMode == "VBF":
    nProcess = 4
if prodMode == "ggF":
    nProcess = 5

print(prodMode, nProcess)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if nProcess == 3: #ggZH
    genSeq.Pythia8.Commands += ['Powheg:NFinal = -1'] # let Pythia figure out the number of final state particles
    genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
else:
    genSeq.Pythia8.Commands += ['Powheg:NFinal = -1'] # let Pythia figure out the number of final state particles
    genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
    #--------------------------------------------------------------
    # Higgs->bbar at Pythia8
    #--------------------------------------------------------------
genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    '35:m0 = 125',
    '35:mWidth = 0.00407',
    '35:doForceWidth = on',
    '35:onMode = off',
    '35:onIfMatch = 36 36', # h->aa

    '36:oneChannel = 1 1.0 101 {0} {1}'.format(adecay1,adecay2),
    '36:m0=%.1f' % ma,
    '36:mMin=%.1f' %m_ma,
    '36:mMax = %.1f' %p_ma,
    '36:mWidth= %.7g' % width,
    '36:tau0 %.1f' % ctau,
]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
    'ParticleDecays:tau0Max = 100000.0',
    'ParticleDecays:limitTau0 = off'
]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

if nProcess==0:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vbbbarbbbar production"
        evgenConfig.process = "WpH, H->2a->4b, W->lv"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vtau+tau-tau+tau- production"
        evgenConfig.process = "WpH, H->2a->4tau, W->lv"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vuubaruubar production"
        evgenConfig.process = "WpH, H->2a->4u, W->lv"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vccbarccbar production"
        evgenConfig.process = "WpH, H->2a->4c, W->lv"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vgggg production"
        evgenConfig.process = "WmH, H->2a->4g, W->lv"
elif nProcess==1:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vbbbarbbbar production"
        evgenConfig.process = "WmH, H->2a->4b, W->lv"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vtau+tau-tau+tau- production"
        evgenConfig.process = "WmH, H->2a->4tau, W->lv"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vuubaruubar production"
        evgenConfig.process = "WmH, H->2a->4u, W->lv"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vccbarccbar production"
        evgenConfig.process = "WmH, H->2a->4c, W->lv"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vgggg production"
        evgenConfig.process = "WmH, H->2a->4g, W->lv"
elif nProcess==2:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "ZH, H->2a->4b, Z->ll"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "ZH, H->2a->4tau, Z->ll"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "ZH, H->2a->4u, Z->ll"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "ZH, H->2a->4c, Z->ll"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process = "ZH, H->2a->4g, Z->ll"
elif nProcess==3:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "ggZH, H->2a->4b, Z->ll"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "ggZH, H->2a->4tau, Z->ll"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "ggZH, H->2a->4u, Z->ll"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "ggZH, H->2a->4c, Z->ll"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->H+Z+jet->l+l-gggg production"
        evgenConfig.process = "ggZH, H->2a->4g, Z->ll"
elif nProcess==4:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqbbbarbbbar production"
        evgenConfig.process = "VBF, H->2a->4b"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqtau+tau-tau+tau- production"
        evgenConfig.process = "VBF, H->2a->4tau"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qquubaruubar production"
        evgenConfig.process = "VBF, H->2a->4u"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqccbarccbar production"
        evgenConfig.process = "VBF, H->2a->4c"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqgggg production"
        evgenConfig.process = "VBF, H->2a->4g"

elif nProcess==5:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqbbbarbbbar production"
        evgenConfig.process = "ggF, H->2a->4b"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqtau+tau-tau+tau- production"
        evgenConfig.process = "ggF, H->2a->4tau"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qquubaruubar production"
        evgenConfig.process = "ggF, H->2a->4u"
    elif adecay==4:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqccbarccbar production"
        evgenConfig.process = "ggF, H->2a->4c"
    elif adecay==21:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 qq->H+qq->qqgggg production"
        evgenConfig.process = "ggF, H->2a->4g"
        
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact     = [ 'connor.waits@cern.ch','melissa.yexley@cern.ch','elena.pompa.pacchi@cern.ch' ]

################################################################
# Filtering of truth particles
################################################################
# Set up VBF filters
if filtered :
    if nProcess==4:
        #import the relevant Generator filter
        # This is necessary for xAOD filters
        include("GeneratorFilters/FindJets.py")
        include ("GeneratorFilters/VBFForwardJetsFilter.py")
        CreateJets(prefiltSeq, 0.4) ## need to add "WZ" if want WZ jets, the 0.4 is radius Generators/GeneratorFilters/share/common/FindJets.py
        AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)

        if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
            from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
            filtSeq += VBFForwardJetsFilter()
            pass

        filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
        filtSeq.VBFForwardJetsFilter.JetMinPt=40.*GeV
        filtSeq.VBFForwardJetsFilter.JetMaxEta=5.0
        filtSeq.VBFForwardJetsFilter.NJets=2
        filtSeq.VBFForwardJetsFilter.Jet1MinPt=40.*GeV
        filtSeq.VBFForwardJetsFilter.Jet1MaxEta=5.0
        filtSeq.VBFForwardJetsFilter.Jet2MinPt=40.*GeV
        filtSeq.VBFForwardJetsFilter.Jet2MaxEta=5.0
        filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
        filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
        filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
        filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
        filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

        # medium cut:
        filtSeq.VBFForwardJetsFilter.MassJJ = 800.*GeV
        filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 3.5

    # Set up ggF filters
    if nProcess==5:
        include("GeneratorFilters/xAODJetFilter_Common.py")
        filtSeq.xAODJetFilter.JetThreshold = 110000
