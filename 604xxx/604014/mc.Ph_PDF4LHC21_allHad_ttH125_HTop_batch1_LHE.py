#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg ttH allhad production PDF4LHC21'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'sabidi@cern.ch' ]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob = 10000


#--------------------------------------------------------------
# Powheg ttH setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ttH_Common.py')
PowhegConfig.decay_mode   = "t t~ > b j j b~ j j" 
PowhegConfig.hdamp        = 352.5

PowhegConfig.runningscales = 1 ## dynamic scale
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate()


