#---------------------------------------------------------------------------------
# EVGEN configuration
#---------------------------------------------------------------------------------
evgenConfig.process     = "qq->ZH Z->inc H->inc"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MiNLO, ZH Z->inc H->inc   mh=125GeV"
evgenConfig.keywords    = ["SM", "Higgs", "SMHiggs", "mH125"]
evgenConfig.inputFilesPerJob = 55
evgenConfig.nEventsPerJob    = 5000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ "binbin.dong@cern.ch","rongqian.qian@cern.ch" ]


#---------------------------------------------------------------------------------
# Pythia8 showing with the A14 NNPDF 2.3 tune
#---------------------------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

    

