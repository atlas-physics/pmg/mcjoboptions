#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune and ATLCR0 for dilepton events.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'lucia.keszeghova@cern.ch' ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000


#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


#--------------------------------------------------------------
# Colour reconnection model
#--------------------------------------------------------------

genSeq.Pythia8.Commands += ["ColourReconnection:mode=0",
                            "MultipartonInteractions:pT0Ref = 2.15",
                            "MultipartonInteractions:expPow = 1.81",
                            "ColourReconnection:range = 2.92"
                           ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------

include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.