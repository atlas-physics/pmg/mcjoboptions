#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to 3L0tau, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 1000
evgenConfig.inputFilesPerJob  = 69


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale


#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
# -- #Ele + #Mu >= 3
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
filtSeq.ElecMuThreeFilter.NLeptons = 3
filtSeq.ElecMuThreeFilter.Ptcut = 7000.
filtSeq.ElecMuThreeFilter.Etacut = 2.8

# -- #Ele + #Mu >= 4
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuFourFilter")
filtSeq.ElecMuFourFilter.NLeptons = 4
filtSeq.ElecMuFourFilter.Ptcut = 7000.
filtSeq.ElecMuFourFilter.Etacut = 2.8

# #Tau >= 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("HadTauOneFilter")
filtSeq.HadTauOneFilter.UseNewOptions = True
filtSeq.HadTauOneFilter.Nhadtaus = 1
filtSeq.HadTauOneFilter.Ptcuthad = 15000.
filtSeq.HadTauOneFilter.EtaMaxhad = 2.8
filtSeq.HadTauOneFilter.Ptcutlep_lead = 0.


# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

# -- Requirement 1: #Ele + #Mu == 3, and #Tau == 0
# -- Requirement 2: Leading lepton (Ele or Mu) pt > 13 GeV
filtSeq.Expression="(ElecMuThreeFilter and not HadTauOneFilter and not ElecMuFourFilter) and LeadingElecMuFilter"



