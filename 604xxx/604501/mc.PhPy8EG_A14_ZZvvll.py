#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = "z z > v v~ l+ l-"
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.runningwidth = 1
PowhegConfig.ncall1 = 100000
PowhegConfig.ncall2 = 100000
PowhegConfig.nubound = 100000
PowhegConfig.xupbound = 4
PowhegConfig.generate()

# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Diboson ZZ->vvll production with A14 tune and mllmin4'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '2lepton', 'neutrino']
evgenConfig.contact     = [ 'jan.kretzschmar@cern.ch' ]
evgenConfig.nEventsPerJob   = 10000
