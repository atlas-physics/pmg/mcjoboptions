from MadGraphControl.MadGraphUtils import *

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')


# read parameters from jobOption name
# mc.PhPy8EG_AZNLO_ggH125_mA0p1_Cyy0p01_Czh1p0_Ctau0p0.py
# but since we are using really simplified model, the coupling it's self is also not important
# mc.PhPy8EG_AZNLO_ggH125_mA0p1_Ctau0p0.py
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short


tokens = get_physics_short().split('_')

isggH = False
if 'ggH' in tokens:
  isggH = True

A_Mass = float(tokens[3].replace('mA','').replace('p','.'))
A_Ctau = float(tokens[4].replace('Ctau','').replace('p','.'))

print('=================================================================')
print('Mass of ALP: ',A_Mass, 'Ctau: ',A_Ctau)
print('=================================================================')
  



#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if isggH:
  genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ] # why 2 for ggH?
  genSeq.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2'] # Weak boson processes, only pure Z^0 contribution. More details see from https://pythia.org/latest-manual/ElectroweakProcesses.html
  # need this because h->Za with Z included?
  # but why VBF one does not need this?? Because VBF will have VV->H, we have to consider the off-shell case also
else:
  genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ] 

genSeq.Pythia8.Commands += [
  '35:new = a0 a0 1 0 0 '+str(A_Mass)+' 0 0 0 '+str(A_Ctau), # id:new = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
  '35:oneChannel = 1 1.0 0 22 22',
  '35:isResonance = 0', # mainly for the large mass point
  '25:onMode = off',
  '25:addChannel = 0 0.1 103 23 35',
  '25:onIfMatch = 23 35',
  '25:m0 = 125.',
  '25:mWidth = 0.00407',
  '25:doForceWidth = on',
  '23:onMode = off',
  '23:onIfAny = 11 13 15',
  'ParticleDecays:tau0Max = 1000000.0', #some very large value special for llp
  'ParticleDecays:limitTau0 = off',
  ]

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV


# use xAOD filter
include ("GeneratorFilters/CreatexAODSlimContainers.py")
createxAODSlimmedContainer("TruthLightLeptons",prefiltSeq)
prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

from GeneratorFilters.GeneratorFiltersConf import xAODDiLeptonMassFilter # change dilep filter into xAOD filter
xAODDiLeptonMassFilter = xAODDiLeptonMassFilter("xAODDiLeptonMassFilter")
filtSeq += xAODDiLeptonMassFilter

filtSeq.xAODDiLeptonMassFilter.MinPt = 10000.
filtSeq.xAODDiLeptonMassFilter.MaxEta = 2.7
filtSeq.xAODDiLeptonMassFilter.MinMass = 76000.
filtSeq.xAODDiLeptonMassFilter.MaxMass = 106000.
filtSeq.xAODDiLeptonMassFilter.MinDilepPt = 0.
filtSeq.xAODDiLeptonMassFilter.AllowSameCharge = False

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->a0(yy)Z(ll), mH=125 GeV, mA0="+str(A_Mass)+" GeV, lifetime="+str(A_Ctau)+" ns"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","Z", "mH125" ]
evgenConfig.contact     = [ 'liuya@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]

