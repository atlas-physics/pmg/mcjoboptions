#--------------------------------------------------------------
# Pythia8 Configuration for Prompt HNL Decay Analysis
#--------------------------------------------------------------

from AthenaCommon.Include import include
import os

# Extract the DSID directory from runArgs.jobConfig
if hasattr(runArgs, "jobConfig") and runArgs.jobConfig:
    dsid_dir = runArgs.jobConfig[0]  # The DSID directory passed via --jobConfig
else:
    raise ValueError("Could not find jobConfig in runArgs. Ensure the DSID directory is specified correctly.")

# Dynamically locate the job option file inside the DSID directory
job_option_file = None
if os.path.isdir(dsid_dir):
    for file in os.listdir(dsid_dir):
        if file.endswith(".py") and file.startswith("mc.PhPy8EG"):
            job_option_file = os.path.join(dsid_dir, file)
            break
else:
    raise ValueError(f"DSID directory not found or invalid: {dsid_dir}")

if not job_option_file:
    raise ValueError(f"No valid job option file found in directory: {dsid_dir}")

# Extract the job option file name
jo_name = os.path.basename(job_option_file)

print(f"Job Option Name: {jo_name}")  # Debugging print
params = jo_name.split('_')  # Split the name into parameters

# Ensure the correct number of parameters are present
if len(params) < 5:
    raise ValueError(
        f"Unexpected job option file name format: {jo_name}. "
        f"Expected format: mc.PhPy8EG_A14N23LO_HNL_ctau<LIFETIME>_mass<HNL_MASS>_<LEADING_LEPTON>.py"
    )

# Extract mass, lifetime, and leading lepton type
ctau_hnl = float(params[3][4:].replace('p', '.'))  # Extract ctau (e.g., "ctau0" -> 0.0)
mass_hnl = float(params[4][4:].replace('p', '.'))  # Extract mass (e.g., "mass3p0" -> 3.0)
leading_lepton = params[5].strip('.py')  # Extract leading lepton type (e.g., "electron" or "muon")

# Print parsed parameters for verification
print(f"Parsed Parameters: ctau = {ctau_hnl} mm, mass = {mass_hnl} GeV, leading lepton = {leading_lepton}")

# Ensure the decay is prompt
if ctau_hnl != 0:
    raise ValueError("This CO is for prompt decays only (ctau must be 0).")

# Load the common settings for Pythia8
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
else:
    genSeq.Pythia8.UserModes += ['Main31:NFinal = 3']


# Pythia8 Commands for HNL and Higgs
genSeq.Pythia8.Commands += [
    # Define the new particle N2 (HNL)
    f'99904:new = N2 N2 2 0 0 {mass_hnl:.2f} 0.0 0.0 0.0 0.0 0 1 0 1 0',
    '99904:mayDecay = on',
    '99904:isResonance = false',
]

# Add decay channels based on the leading lepton
if leading_lepton == 'electron':
    genSeq.Pythia8.Commands += [
        '99904:addChannel = 1 0.16 23 11 -11 12',
        '99904:addChannel = 1 0.16 23 11 -13 14',
        '99904:addChannel = 1 0.16 23 11 -15 16',
        '99904:addChannel = 1 0.16 23 -11 11 -12',
        '99904:addChannel = 1 0.16 23 -11 13 -14',
        '99904:addChannel = 1 0.16 23 -11 15 -16',
    ]
elif leading_lepton == 'muon':
    genSeq.Pythia8.Commands += [
        '99904:addChannel = 1 0.16 23 13 -11 12',
        '99904:addChannel = 1 0.16 23 13 -13 14',
        '99904:addChannel = 1 0.16 23 13 -15 16',
        '99904:addChannel = 1 0.16 23 -13 11 -12',
        '99904:addChannel = 1 0.16 23 -13 13 -14',
        '99904:addChannel = 1 0.16 23 -13 15 -16',
    ]
else:
    raise ValueError(f"Unknown leading lepton type: {leading_lepton}")

# Higgs decay to HNL
genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    '25:onMode = off',  # Turn off all other Higgs decay modes
    f'25:addChannel = 1 1.0 103 14 99904',  # H -> nu_mu + N2
    '25:onIfMatch = 14 99904',  # Match for H -> nu_mu + N2
]

# Event Generation Metadata
evgenConfig.description = "POWHEG+Pythia8 VBF production of H->nuN with mHNL={mass_hnl} GeV, ctau={ctau_hnl} mm, leading lepton={leading_lepton}"
evgenConfig.keywords = ['BSM', 'Higgs', 'BSMHiggs']
evgenConfig.process = "pp->Hjj, H->nu+N"

evgenConfig.contact = ["ska6969@nyu.edu"]



def arrange_output(process_dir, runArgs, lhe_version, saveProcDir):
    if not hasattr(runArgs, 'outputEVNTFile'):
        raise ValueError("outputEVNTFile not defined in runArgs.")

    # Set the output path
    output_file = runArgs.outputEVNTFile

    # (Optional) Save process directory for debugging
    if saveProcDir:
        os.makedirs(process_dir, exist_ok=True)
        print(f"Process directory saved at {process_dir}")

    # Example log output
    print(f"Output file configured: {output_file} with LHE version {lhe_version}")
