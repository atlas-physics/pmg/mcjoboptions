#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to 4L1tau, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "tau"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 1000
evgenConfig.inputFilesPerJob  = 80


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# -- #Ele + #Mu >= 1
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- #Ele + #Mu >= 2
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

# -- #Ele + #Mu >= 3
filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
filtSeq.ElecMuThreeFilter.NLeptons = 3
filtSeq.ElecMuThreeFilter.Ptcut = 7000.
filtSeq.ElecMuThreeFilter.Etacut = 2.8

# #Tau >= 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("HadTauOneFilter")
filtSeq.HadTauOneFilter.UseNewOptions = True
filtSeq.HadTauOneFilter.UseMaxNTaus = True
filtSeq.HadTauOneFilter.Nhadtaus = 1
filtSeq.HadTauOneFilter.Ptcuthad = 15000.
filtSeq.HadTauOneFilter.EtaMaxhad = 2.8
filtSeq.HadTauOneFilter.Ptcutlep_lead = 0.

# #Tau >= 2
filtSeq += TauFilter("HadTauTwoFilter")
filtSeq.HadTauTwoFilter.UseNewOptions = True
filtSeq.HadTauTwoFilter.UseMaxNTaus = True
filtSeq.HadTauTwoFilter.Nhadtaus = 2
filtSeq.HadTauTwoFilter.Ptcuthad = 15000.
filtSeq.HadTauTwoFilter.EtaMaxhad = 2.8
filtSeq.HadTauTwoFilter.Ptcutlep_lead = 0.

# #Tau >= 3
filtSeq += TauFilter("HadTauThreeFilter")
filtSeq.HadTauThreeFilter.UseNewOptions = True
filtSeq.HadTauThreeFilter.UseMaxNTaus = True
filtSeq.HadTauThreeFilter.Nhadtaus = 3
filtSeq.HadTauThreeFilter.Ptcuthad = 15000.
filtSeq.HadTauThreeFilter.EtaMaxhad = 2.8
filtSeq.HadTauThreeFilter.Ptcutlep_lead = 0.

# #Tau >= 4
filtSeq += TauFilter("HadTauFourFilter")
filtSeq.HadTauFourFilter.UseNewOptions = True
filtSeq.HadTauFourFilter.UseMaxNTaus = True
filtSeq.HadTauFourFilter.Nhadtaus = 4
filtSeq.HadTauFourFilter.Ptcuthad = 15000.
filtSeq.HadTauFourFilter.EtaMaxhad = 2.8
filtSeq.HadTauFourFilter.Ptcutlep_lead = 0.

# -- Requirement 1: (#Ele + #Mu >= 3) and (#Tau >= 1)
#                   or (#Ele + #Mu == 2) and (#Tau >= 2)
#                   (neglact #Ele + #Mu == 1, since it will be included by 1l3tau)
#                   or (#Ele + #Mu == 0) and (#Tau >= 4)
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="( (ElecMuThreeFilter and HadTauOneFilter) " +\
                    "or (ElecMuTwoFilter and not ElecMuThreeFilter and HadTauTwoFilter) " +\
                    "or (not ElecMuOneFilter and HadTauFourFilter) )" +\
                    "and (LeadingElecMuFilter or not ElecMuOneFilter)"

