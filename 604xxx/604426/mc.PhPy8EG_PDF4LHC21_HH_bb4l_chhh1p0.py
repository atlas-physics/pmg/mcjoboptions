#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to bb4l, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar", "bottom"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 5000
evgenConfig.inputFilesPerJob = 23


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

### selectin Higgs decays
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.99   100 23 23  ",   # ZZ decay
                             "25:addChannel = on 0.01   100 5 -5   ",   # bb decay 
                             "23:onMode = off",
                             "23:onIfAny= 11 13 15",
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale
### Dipole shower option
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]


#---------------------------------------------------------------------------------------------------
# Filter for bbZZ
#---------------------------------------------------------------------------------------------------                                                           
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HZZFilter", PDGParent = [25], PDGChild = [23])

# from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# # -- #Ele + #Mu >= 4
# filtSeq += MultiLeptonFilter("ElecMuFourFilter")
# filtSeq.ElecMuFourFilter.NLeptons = 4
# filtSeq.ElecMuFourFilter.Ptcut = 7000.
# filtSeq.ElecMuFourFilter.Etacut = 2.8

# # -- leading lepton pt filter
# from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
# filtSeq += LeptonFilter("LeadingElecMuFilter")
# filtSeq.LeadingElecMuFilter.Ptcut = 13000.
# filtSeq.LeadingElecMuFilter.Etacut = 2.8

# # -- Requirement 1: (#Ele + #Mu >= 4) and (Hbb)
# # -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
# #                   Otherwise, we require pt > 13 GeV for leading light lepton
# # filtSeq.Expression = "(HbbFilter and ElecMuFourFilter) and LeadingElecMuFilter"
# filtSeq.Expression = "(HbbFilter and HZZFilter and ElecMuFourFilter) and LeadingElecMuFilter"
filtSeq.Expression = "HbbFilter and HZZFilter"
