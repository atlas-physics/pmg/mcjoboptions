#--------------------------------------------------------------
# Job Option for Specific HNL Mass and Leading Lepton
#--------------------------------------------------------------

evt_multiplier = 1.2  # Adjust event multiplier for the generation


# Configure the number of events per job and input files per job
evgenConfig.nEventsPerJob = 10000  # Number of events to generate per job
evgenConfig.inputFilesPerJob = 15  # Number of input LHE files per job

# Include the control objective
include("Pythia8_HNL_METll_PromptDecay_control.py")
