
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603258.Ph_PDF4LHC21_ZH125J_Zincl_MINLO_HWW_batch2_LHE.evgen.TXT.e8557
evgenConfig.description = "POWHEG+MiNLO+Herwig7 Z+H+jet->Zincl+WW->Zincl+qqlv production"
evgenConfig.generators  = ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'callum.mccracken@cern.ch' ]
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 10000
evgenConfig.process = "qq->ZH, Z->inclusive, H->WW, WW->qqlv"
evgenConfig.tune = "H71-Default"

#--------------------------------------------------------------
# POWHEG+MiNLO+Herwig7 qq->ZH, Z->inclusive, H->WW, WW->qqlv production
#--------------------------------------------------------------

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# H->WW, with  W+ -> qq,  W- -> lv

Herwig7Config.add_commands("""
# force H->WW decays
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
# W decay options
set /Herwig/Particles/W-:Synchronized Not_synchronized
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
set /Herwig/Particles/W+:Synchronized Not_synchronized
do /Herwig/Particles/W+:SelectDecayModes W+->u,dbar; W+->sbar,u; W+->c,dbar; W+->c,sbar; W+->bbar,c;
""")

# run Herwig7
Herwig7Config.run()
