#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, two-lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 601230 LHE files with alternative color reconnection in the W decay.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'federica.fabbri@cern.ch','andrea.helen.knue@cern.ch','chris.young@cern.ch']
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000


#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


#--------------------------------------------------------------
# Early resonance decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'PartonLevel:earlyResDec = 1' ]
genSeq.Pythia8.Commands += [ 'ColourReconnection:range = 5.2' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------

include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.