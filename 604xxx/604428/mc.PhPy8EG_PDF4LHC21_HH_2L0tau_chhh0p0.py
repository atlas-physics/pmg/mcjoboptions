#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to 2L0tau, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "multilepton"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 5000
evgenConfig.inputFilesPerJob = 85


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0"]             # Z/gamma* combination scale

### Dipole shower option
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
# -- #Ele + #Mu >= 2
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

# -- #Ele + #Mu >= 3
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
filtSeq.ElecMuThreeFilter.NLeptons = 3
filtSeq.ElecMuThreeFilter.Ptcut = 7000.
filtSeq.ElecMuThreeFilter.Etacut = 2.8

# #Tau >= 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("HadTauOneFilter")
filtSeq.HadTauOneFilter.UseNewOptions = True
filtSeq.HadTauOneFilter.Nhadtaus = 1
filtSeq.HadTauOneFilter.Ptcuthad = 15000.
filtSeq.HadTauOneFilter.EtaMaxhad = 2.8
filtSeq.HadTauOneFilter.Ptcutlep_lead = 0.


# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

# -- Requirement 1: #Ele + #Mu == 2, and #Tau == 0
# -- Requirement 2: Leading lepton (Ele or Mu) pt > 13 GeV
filtSeq.Expression="(ElecMuTwoFilter and not HadTauOneFilter and not ElecMuThreeFilter) and LeadingElecMuFilter"
