#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG MiNNLO W"
evgenConfig.keywords    = [ "SM", "W", "0jet", "1jet", "2jet"]
evgenConfig.generators  = [ "Powheg" ]
evgenConfig.contact     = [ "hannes.mildner@cern.ch" ]
evgenConfig.nEventsPerJob = 500

include("PowhegControl/PowhegControl_Wj_MiNNLO_Common.py")
# Integration settings, MiNNLO settings, PDF settings included here 
include("PowhegControl_MiNNLO_DY.py")

# EW input settings
setMassAndWidth()

# decay channel
PowhegConfig.decay_mode = "w- > mu- vm~"

# run powheg
PowhegConfig.generate()
