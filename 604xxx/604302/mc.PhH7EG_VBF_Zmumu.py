# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
# modelled after 603009/mc.PhPy8EG_A14NNPDF23_VBF_Zmumu_v2.py 

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Herwig7 VBF Z(mumu)jj production, mll>40 GeV"
evgenConfig.keywords    = ["SM", "VBF", "Z"]
evgenConfig.contact     = ["lu.zheng@cern.ch", "oliver.rieger@nikhef.nl", "jan.kretzschmar@cern.ch"]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.nEventsPerJob = 2000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_Z process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_Z_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.mll_gencut = 40
PowhegConfig.ptj_gencut = 20

PowhegConfig.ncall1 = 1000000
PowhegConfig.ncall2 = 2000000
PowhegConfig.itmx1 = 5
PowhegConfig.itmx2 = 10

PowhegConfig.nubound = 10000000
PowhegConfig.xupbound = 8

PowhegConfig.foldphi = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------
include("Herwig7_i/Herwig7_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
