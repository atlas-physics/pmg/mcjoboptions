#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 W MiNNLO with modified A14 tune and Photos"
evgenConfig.keywords    = [ "SM", "W", "0jet", "1jet", "2jet"]
evgenConfig.contact     = [ "hannes.mildner@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 21

# Config for Py8 with A14 tune
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# Pythia shower modifications
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 2.0"] # (default 1.8) higher values improves low pT behaviour, anti-correlated with Q0

# Powheg-Pythia matching settings https://pythia.org/latest-manual/POWHEGMatching.html
genSeq.Pythia8.Commands += ["POWHEG:NFinal = 2"] # -> KEEP THIS VALUE: NNLO = 2 jets
genSeq.Pythia8.Commands += ["POWHEG:veto = 1"]   # -> RUN VETOED SHOWER: SHOWER INCLUSIVELY BUT VETO EMISSIONS WITH PT > PTHARD
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"]  # -> PT DEFINITION: 1 = The POWHEG ISR pT and FSR d_ij definitions.
genSeq.Pythia8.Commands += ["POWHEG:pTHard = 2"] # -> HARDNESS DEFINITION: 1 = The pT of all final-state partons is tested against all other incoming and outgoing partons, with the minimal value chosen. 

# Photos (copied from Powheg sample)
include('Pythia8_i/Pythia8_Photospp.py')
genSeq.Photospp.CreateHistory = True # restore Photospp_i default that changed in 22.6
genSeq.Photospp.ZMECorrection = False
genSeq.Photospp.WMECorrection = True
genSeq.Photospp.PhotonSplitting = True
genSeq.Photospp.WtInterference = 4.0 # increase Photos upper limit for ME corrections

