#--------------------------------------------------------------
# PowHeg+Pythia bbH with H->tautau
#--------------------------------------------------------------

evgenConfig.description    = "Powheg+Pythia8 for bbH, H->tautau"
evgenConfig.keywords       = [ "SM", "bbHiggs", "SMHiggs", "mH125" , "2tau" ]
evgenConfig.contact        = [ "huanguo.li@cern.ch" ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 10000 

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15']

