#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to 3L1tau, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "tau"]
evgenConfig.contact = ["cen.mo@cern.ch"]
evgenConfig.nEventsPerJob  = 2000
evgenConfig.inputFilesPerJob  = 43


#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### 2 particles in the final state
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.706 100 24 -24 " , # WW decay
                             "25:addChannel = on 0.086 100 23 23  " , # ZZ decay
                             "25:addChannel = on 0.207 100 15 -15 " , # tautau decay
                             "24:mMin = 0"                          , # W minimum mass
                             "24:mMax = 99999"                      , # W maximum mass
                             "23:mMin = 0"                          , # Z minimum mass
                             "23:mMax = 99999"                      , # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ]             # Z/gamma* combination scale


#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
# # -- #Ele + #Mu + #Tau >= 3
# from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
# filtSeq += MultiElecMuTauFilter("ElecMuTauThreeFilter")
# filtSeq.ElecMuTauThreeFilter.IncludeHadTaus = True
# filtSeq.ElecMuTauThreeFilter.NLeptons = 3
# filtSeq.ElecMuTauThreeFilter.MinPt = 7000.
# filtSeq.ElecMuTauThreeFilter.MinVisPtHadTau = 15000.
# filtSeq.ElecMuTauThreeFilter.MaxEta = 2.8

# # -- #Ele + #Mu + #Tau >= 4
# filtSeq += MultiElecMuTauFilter("ElecMuTauFourFilter")
# filtSeq.ElecMuTauFourFilter.IncludeHadTaus = True
# filtSeq.ElecMuTauFourFilter.NLeptons = 4
# filtSeq.ElecMuTauFourFilter.MinPt = 7000.
# filtSeq.ElecMuTauFourFilter.MinVisPtHadTau = 15000.
# filtSeq.ElecMuTauFourFilter.MaxEta = 2.8

# # -- #Ele + #Mu >= 3
# from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
# filtSeq.ElecMuThreeFilter.NLeptons = 3
# filtSeq.ElecMuThreeFilter.Ptcut = 7000.
# filtSeq.ElecMuThreeFilter.Etacut = 2.8

# -- leading lepton pt filter
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeadingElecMuFilter")
filtSeq.LeadingElecMuFilter.Ptcut = 13000.
filtSeq.LeadingElecMuFilter.Etacut = 2.8

# # -- #Ele + #Mu >= 1
# filtSeq += MultiLeptonFilter("ElecMuOneFilter")
# filtSeq.ElecMuOneFilter.NLeptons = 1
# filtSeq.ElecMuOneFilter.Ptcut = 7000.
# filtSeq.ElecMuOneFilter.Etacut = 2.8

# # -- Requirement 1: #Ele + #Mu + #Tau == 3
# # -- Requirement 2: #Tau >= 1
# # -- Requirement 3: If there is no light lepton, then no requirement on the leading lepton is required.
# #                   Otherwise, we require pt > 13 GeV for leading light lepton
# filtSeq.Expression="(ElecMuTauThreeFilter and not ElecMuTauFourFilter) and not ElecMuThreeFilter and (LeadingElecMuFilter or not ElecMuOneFilter)"

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# -- #Ele + #Mu >= 1
filtSeq += MultiLeptonFilter("ElecMuOneFilter")
filtSeq.ElecMuOneFilter.NLeptons = 1
filtSeq.ElecMuOneFilter.Ptcut = 7000.
filtSeq.ElecMuOneFilter.Etacut = 2.8

# -- #Ele + #Mu >= 2
filtSeq += MultiLeptonFilter("ElecMuTwoFilter")
filtSeq.ElecMuTwoFilter.NLeptons = 2
filtSeq.ElecMuTwoFilter.Ptcut = 7000.
filtSeq.ElecMuTwoFilter.Etacut = 2.8

# -- #Ele + #Mu >= 3
filtSeq += MultiLeptonFilter("ElecMuThreeFilter")
filtSeq.ElecMuThreeFilter.NLeptons = 3
filtSeq.ElecMuThreeFilter.Ptcut = 7000.
filtSeq.ElecMuThreeFilter.Etacut = 2.8

# #Tau == 1
from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("HadTauEQOneFilter")
filtSeq.HadTauEQOneFilter.UseNewOptions = True
filtSeq.HadTauEQOneFilter.UseMaxNTaus = True
filtSeq.HadTauEQOneFilter.Nhadtaus = 1
filtSeq.HadTauEQOneFilter.MaxNhadtaus = 1
filtSeq.HadTauEQOneFilter.Ptcuthad = 15000.
filtSeq.HadTauEQOneFilter.EtaMaxhad = 2.8
filtSeq.HadTauEQOneFilter.Ptcutlep_lead = 0.

# #Tau == 2
filtSeq += TauFilter("HadTauEQTwoFilter")
filtSeq.HadTauEQTwoFilter.UseNewOptions = True
filtSeq.HadTauEQTwoFilter.UseMaxNTaus = True
filtSeq.HadTauEQTwoFilter.Nhadtaus = 2
filtSeq.HadTauEQTwoFilter.MaxNhadtaus = 2
filtSeq.HadTauEQTwoFilter.Ptcuthad = 15000.
filtSeq.HadTauEQTwoFilter.EtaMaxhad = 2.8
filtSeq.HadTauEQTwoFilter.Ptcutlep_lead = 0.

# #Tau == 3
filtSeq += TauFilter("HadTauEQThreeFilter")
filtSeq.HadTauEQThreeFilter.UseNewOptions = True
filtSeq.HadTauEQThreeFilter.UseMaxNTaus = True
filtSeq.HadTauEQThreeFilter.Nhadtaus = 3
filtSeq.HadTauEQThreeFilter.MaxNhadtaus = 3
filtSeq.HadTauEQThreeFilter.Ptcuthad = 15000.
filtSeq.HadTauEQThreeFilter.EtaMaxhad = 2.8
filtSeq.HadTauEQThreeFilter.Ptcutlep_lead = 0.

# -- Requirement 1: (#Ele + #Mu == 2) and (#Tau == 1)
#                   or (#Ele + #Mu == 1) and (#Tau == 2)
#                   or (#Ele + #Mu == 0) and (#Tau == 3)
# -- Requirement 2: If there is no light lepton, then no requirement on the leading lepton is required.
#                   Otherwise, we require pt > 13 GeV for leading light lepton
filtSeq.Expression="( (ElecMuTwoFilter and not ElecMuThreeFilter and HadTauEQOneFilter) " +\
                    "or (ElecMuOneFilter and not ElecMuTwoFilter and HadTauEQTwoFilter) " +\
                    "or (not ElecMuOneFilter and HadTauEQThreeFilter) )" +\
                    "and (LeadingElecMuFilter or not ElecMuOneFilter)"
