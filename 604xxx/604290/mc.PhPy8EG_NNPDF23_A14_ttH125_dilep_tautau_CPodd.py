#--------------------------------------------------------------
# POWHEG+Pythia8 ttH Htautau CPodd production
# LHE input: mc21_13p6TeV.601452.Ph_PDF4LHC21_dilep_ttH125_LHE.evgen.TXT.e8453
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Inclusive Higgs decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'HiggsH1:parity = 2', # decay of Higgs
                             '25:onMode = off',
                             '25:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# TTbarWToLeptonFilter
# Working when input lhe files are inclusive and if LHE input already added the filter this would have no effect
#--------------------------------------------------------------
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process        = "ttH dilep H->tautau CPodd"
evgenConfig.description    = 'POWHEG+Pythia8.230 ttH (dilep) production with A14 NNPDF2.3 tune'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'runze.li@yale.edu' ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob  = 10000
evgenConfig.generators     = [ 'Powheg', 'Pythia8', 'EvtGen' ]

