#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = "w+ w- > l+ vl j j / j j l- vl~"
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.runningwidth = 1
PowhegConfig.ncall1 = 100000
PowhegConfig.ncall2 = 100000
PowhegConfig.nubound = 100000
PowhegConfig.xupbound = 2
PowhegConfig.generate()

# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Diboson WW->lvqq production with A14 tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', 'lepton', 'neutrino', '2jet' ]
evgenConfig.contact     = [ 'jan.kretzschmar@cern.ch' ]
evgenConfig.nEventsPerJob   = 10000
