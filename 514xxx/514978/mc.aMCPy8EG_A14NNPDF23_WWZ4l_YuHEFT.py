
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.contact = ["patrick.dougan@cern.ch"]
runName = 'WWZ4l'
nevents = runArgs.maxEvents*2.2 if runArgs.maxEvents>0 else 2.2*evgenConfig.nEventsPerJob

#for SM
#import model SM_Ltotal_Ind5v2020v2_UFO
#Note need to inlcude pdgid_extras.txt file in working directory 
#with PDG IDs for new particles
# import model Radion_BKK
process = """
import model YqHEFT_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define wpm = w+ w-
define VB = w+ w- z
define l = e- mu- e+ mu+
define v = ve ve~ vm vm~
generate p p > wpm wpm z QCD=1 QED=4 NP==1
output -f"""

process_dir = new_process(process)

#Fetch default NLO run_card.dat and set parameters
settings = { #'lhe_version':'2.0',
             #'cut_decays' : 'F',
            # 'parton_shower':'PYTHIA8',
            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

param_card_name = 'param_card_LQYC_mod_u.dat'
modify_param_card(param_card_input=param_card_name,process_dir=process_dir)

#print cards
# print_cards(run_card=modify_run_card,param_card=modify_param_card)

generate(process_dir=process_dir,runArgs=runArgs)
outDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


#### Shower
evgenConfig.generators = ["aMcAtNlo"]
evgenConfig.description = 'aMcAtNlo_WWZ4l'
evgenConfig.keywords+=["triboson"]
runArgs.inputGeneratorFile=outDS

# SHOWERING:
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#LO shower
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ['24:onMode = off', #decay of W
                            '24:onIfAny = 11 12 13 14',
                            '23:onMode = off', #decay of Z
                            '23:onIfAny = 11 13']


#NLO shower
#include("Pythia8_i/Pythia8_aMcAtNlo.py")

#### Finalize
#theApp.finalize()
#theApp.exit()

