import os

from AthenaCommon.Include import include
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment


#reading the name of the other file that contains the info of the process

phys_short = get_physics_short()
model_string = phys_short.split('_')[2]
mhnl_input = phys_short.split('_')[2].replace("HNL","")
mhnl_str= ''
for entry in mhnl_input:
    if entry =='p':
        mhnl_str=mhnl_str+'.'
    else:
        mhnl_str =mhnl_str+entry
mhnl = float(mhnl_str)


channel = phys_short.split('_')[4]


lep = [None]*3
digit = 0

for i in range(3):
    if channel[digit:digit+2]=='mu':
        lep[i] = 'mu'
        digit+=2
    elif channel[digit:digit+2]=='ta':
        lep[i] = 'ta'
        digit+=2
    elif channel[digit:digit+1]=='e':
        lep[i] = 'e'
        digit+=1
    else:
        evgenLog.error("lepton %i does not have correct type, "%i)

if len(channel[digit:])!=0:
    evgenLog.error("channel name too long, check")

ctaustring = phys_short.split('_')[3].replace("ctau","") # ctau is the lifetime of the HNL

ctau_str= ''
for entry in ctaustring:
    if entry =='p':
        ctau_str=ctau_str+'.'
    else:
        ctau_str =ctau_str+entry
ctau = float(ctau_str)

if ctau > 0:
    tofoption = 0
else:
    tofoption= -1

evgenLog.info('physics short: %s' %phys_short)

evgenLog.info('Processing model with HNL mass and average lifetime: (mhnl, ctau) = (%e,%e)' %(mhnl, ctau))

genline1 = "import model SM_HeavyN_NLO"
genline2 = "define p = u c d s b u~ c~ d~ s~ b~ g"
genline3 = "define j = p"
genline4 = "define mu = mu+ mu-"
genline5 = "define e = e+ e-"
genline6 = "define ta = ta+ ta-"
genline7 = "define vvm = vm vm~"
genline8 = "define vve = ve ve~"
genline9 = "define vvt = vt vt~"
genline10 = "define vv =vm vm~ ve ve~ vt vt~"
# Next if takes into account if you have a tau in any of the 3 spots, if you have one at 1 and one at 2, and if you don't have any

if lep[0]=="mu": 
    genline11 = "generate p p > mu n1 [QCD]"
if lep[0]=="e":
    genline11 = "add process p p > e n1 [QCD]"
if lep[0]=="ta":
    genline11 = "add process p p > ta n1 [QCD]"   
genline12 = "output -f"
gen_process = genline1+"\n"+genline2+"\n"+genline3+"\n"+genline4+"\n"+genline5+"\n"+genline6+"\n"+genline7+"\n"+genline8+"\n"+genline9+"\n"+genline10+"\n"+genline11+"\n"+genline12

# General settings
gridpack_mode=False

    
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=500*evt_multiplier

run_settings = {'lhe_version':'3.0',
                #'pdlabel'    : "'lhapdf'",
                #'lhaid'      : 260000,
                'ickkw'      : '0',
                'maxjetflavor':5, # 5 flavor scheme
                'bwcutoff': 15.0,
                'time_of_flight': tofoption,
                'parton_shower':'PYTHIA8'
}

#run_settings['use_syst']='F'
run_settings['nevents'] = nevents
run_settings['time_of_flight']= 6.66666666667e-12

# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# this is a dictionary
params = {}

params['mass']={'72':mhnl,'74':1e10,'76':1e10}#, '15':1.777e0}
params['numixing']={'1':0,'2':0,'3':0,'4':0,'5':0,'6':0,'7':0,'8':0,'9':0}

if ctau > 0:
    width= 197.4635212e-15/ctau # hbar*c [GeV*mm] / c*tau [mm]                 
    params['DECAY']={'72':'DECAY 72 {} # WN1'.format(width)} #we will need to add decay of tau
else:
    params['DECAY']={'72':'DECAY  72 Auto # WN1'}

params['sminputs']={'1':1.325070e+02,'2':1.166390e-05 ,'3':1.180000e-01}
params['numixing']={'1':1,'2':0,'3':0,'4':1,'5':0,'6':0,'7':1,'8':0,'9':0}


modify_param_card(process_dir=process_dir,params=params)

print('Modifying SKS Param Card, printing old stuff')
with open('/'.join([process_dir, 'Cards', 'FKS_params.dat']), 'r+') as fks:
    retlines = []
    for line in fks:
        if '1.0d-5' in line and not line.startswith('!'): # at the time of writing, only param with this value                                               
            retlines.append('-1.0d0\n')
        else:
            retlines.append(line)

    fks.seek(0)                 # set "cursor" at beginning of file            
    fks.write(''.join(retlines))
    fks.truncate()              # and cut off the end                          

    print_cards()

# Set up the MadSpin card
madline1 = "set BW_cut 15"
madline2 = "set spinmode onshell"
madline3 = "set max_weight_ps_point 400  # number of PS to estimate the maximum for each event"
madline4 = "set seed %i"

if lep[1]=="e" and lep[2]=="e":
    madline5 = "decay n1 > e e vv"
if lep[1]=="e" and lep[2]=="mu":
    madline5 = "decay n1 > e mu vv"
if lep[1]=="e" and lep[2]=="ta":
    madline5 = "decay n1 > e ta vv"
if lep[1]=="mu" and lep[2]=="mu":
    madline5="decay n1 > mu mu vv"
if lep[1]=="mu" and lep[2]=="e":
    madline5 = "decay n1 > mu e vv"
if lep[1]=="mu" and lep[2]=="ta":
    madline5 = "decay n1 > mu ta vv"
if lep[1]=="ta" and lep[2]=="ta":
    madline5 = "decay n1 > ta ta vv"
if lep[1]=="ta" and lep[2]=="e":
    madline5 = "decay n1 > ta e vv"
if lep[1]=="ta" and lep[2]=="mu":
    madline5 = "decay n1 > ta mu vv"


madline6 = "launch"
mad_process = madline1+"\n"+madline2+"\n"+madline3+"\n"+madline4+"\n"+madline5+"\n"+madline6
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write(mad_process%runArgs.randomSeed)
mscard.close()

# Generate the events                                                          
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
print('grid_pack?',gridpack_mode)

if ctau>=1:
    min_disp = 2.0 # [mm]fiducial volume is 4-300mm                            
    tau = (min_disp)/(1000.0*3e8) # in seconds, [mm] / [mm/m] * [m/s]          
    #Add min displacement cut                                                  
    add_lifetimes(process_dir=process_dir,threshold=tau)


# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Set the evgen metadata
evgenConfig.description = 'HNL %s (with matching), m_hnl = %s GeV, channel = %s '%(model_string, mhnl, channel)
evgenConfig.keywords = ["exotic","BSM","neutrino"]
evgenConfig.contact = ["Monika Wielers <monika.wielers@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

testSeq.TestHepMC.MaxTransVtxDisp = 1000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 4000000 #in mm
