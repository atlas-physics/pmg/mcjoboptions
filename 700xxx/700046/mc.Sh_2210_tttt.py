include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.10 tttt production with all decay modes."
evgenConfig.keywords = ["SM", "top", "tttt" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""
(run){
  SCALES VAR{H_T2/16}
  EXCLUSIVE_CLUSTER_MODE 1;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  NLO_CSS_PSMODE=1

  %decay settings
  HARD_DECAYS On;
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
  HARD_SPIN_CORRELATIONS 1;
  SOFT_SPIN_CORRELATIONS=1
  SPECIAL_TAU_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process : 93 93 ->  6 -6 6 -6;
  NLO_QCD_Mode 3;
  ME_Generator Amegic;
  RS_ME_Generator Comix;
  Loop_Generator LOOPGEN;
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptttt_ew", "pptttt" ]
