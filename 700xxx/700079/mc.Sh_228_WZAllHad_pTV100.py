include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa WZ"
evgenConfig.keywords = ["SM", "WZ", "all-hadronic", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  % NLO EWK
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
  EXCLUSIVE_CLUSTER_MODE=1

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  NUM_ACCURACY=1e-6
  DIPOLE_AMIN=1.e-8

  % Force W->qq' decay
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2

  % Force Z->qq decay
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,1,-1]=2
  HDH_STATUS[23,2,-2]=2
  HDH_STATUS[23,3,-3]=2
  HDH_STATUS[23,4,-4]=2
  HDH_STATUS[23,5,-5]=2

  % Negative weight reduction
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 23 -24 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET}
  Enhance_Observable VAR{ log10(max(PPerp(p[2]),PPerp(p[3]))) }|2|3.3 {2,3,4,5}
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.99 {3,4,5};
  Selector_File *|(selectorWminusZ){|}(selectorWminusZ)
  End process;

  Process 93 93 -> 23 24 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET}
  Enhance_Observable VAR{ log10(max(PPerp(p[2]),PPerp(p[3]))) }|2|3.3 {2,3,4,5}
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.99 {3,4,5};
  Selector_File *|(selectorWplusZ){|}(selectorWplusZ)
  End process;
}(processes)

(selectorWminusZ){
  MinSelector {
    "PT"  23 100.0,E_CMS [PT_UP]
    "PT" -24 100.0,E_CMS [PT_UP]
  }
}(selectorWminusZ)

(selectorWplusZ){
  MinSelector {
    "PT" 23 100.0,E_CMS [PT_UP]
    "PT" 24 100.0,E_CMS [PT_UP]
  }
}(selectorWplusZ)


"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0","WIDTH[23]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 16
