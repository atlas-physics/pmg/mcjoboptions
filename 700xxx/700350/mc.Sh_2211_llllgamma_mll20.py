include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa llllgamma+0j@NLO+1,2j@LO offshell production with mll>20"
evgenConfig.keywords = ["SM", "electron", "muon", "tau", "neutrino", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  % tags for process setup
  NJET:=2; LJET:=5; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 22 90 90 90 90 93{NJET}
  Order (*,5); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  91  2.0  E_CMS
  Mass 11 -11 20.0 E_CMS
  Mass 13 -13 20.0 E_CMS
  Mass 15 -15 20.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 128

genSeq.Sherpa_i.OpenLoopsLibs += [ "pplllla" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=Process/OpenLoops" ]
