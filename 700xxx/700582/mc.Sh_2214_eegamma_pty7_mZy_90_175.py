include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/EW_scheme_sinthetaW_mZ.py")

evgenConfig.description = "Sherpa eegamma + 0j@NLO + 1,2,3j@LO with 7<pT_y"
evgenConfig.keywords = ["SM", "electron", "neutrino", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch", "ana.cueto@cern.ch" ]
evgenConfig.nEventsPerJob = 5000

genSeq.Sherpa_i.RunCard="""
(run){

  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  # tags for process setup
  NJET:=3; LJET:=3; QCUT:=20;

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1; 

  # speed and neg weight fraction improvements
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  METS_BBAR_MODE=5
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 11 -11 22 93{NJET}
  Enhance_Function VAR{pow(max(PPerp(p[2]+p[3]),PPerp(p[4]))/20.0,2)}
  Associated_Contributions EW|LO1 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6,7}
  End process
}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  90  10.0  E_CMS
  "Calc(Mass(p[0]+p[1]+p[2])>90.0&&Mass(p[0]+p[1]+p[2])<175.0)" 11,-11,22 1,1
}(selector)
"""


genSeq.Sherpa_i.NCores = 32
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplla","ppllaj","pplla_ew","ppllaj_ew"]

#factor alpha(0)/alpha(mZ) for the photon, using Sherpa default values
#KFACTOR VAR{128.802/137.03599976};
genSeq.Sherpa_i.CrossSectionScaleFactor = 0.939913601
