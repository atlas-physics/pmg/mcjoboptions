include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")
# include("Sherpa_i/Lund_Hadronisation.py")

evgenConfig.description = "Sherpa ttW+0,1j@NLO+2j@LO with Sherpa 2.2.14, EW correction weights (muQ x2)"
evgenConfig.keywords = ["ttW", "SM", "multilepton"]
evgenConfig.contact = ["atlas-generators-sherpa@cern.ch"]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""
(run){
    # perturbative scales
    FSF:=1.; RSF:=1.; QSF:=4.;
    SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;

    # merging setup
    QCUT:=30.;
    LJET:=3,4; NJET:=2; 
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR=0.05;

    # EW corrections
    ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3
    NLO_CSS_PSMODE=1

    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0;
    STABLE[6] 0;

    SPECIAL_TAU_SPIN_CORRELATIONS=1
    SOFT_SPIN_CORRELATIONS=1

}(run)

(processes){
    Process 93 93 -> 6 -6 24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;

    Process 93 93 -> 6 -6 -24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0", "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1"]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]

genSeq.Sherpa_i.NCores = 20
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppwtt", "ppwttj", "ppwtt_ew", "ppwttj_ew" ]
