
include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa mumugammagamma + 0j@NLO + 1,2j@LO."
evgenConfig.keywords = ["SM", "2muon", "2photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 200


genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=2; LJET:=4; QCUT:=20;

  % ME generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  # EW corrections setup
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  # speed and neg weight fraction improvements
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 22 22 13 -13 93{NJET}
  Order (*,4); CKKW sqr(QCUT/E_CMS)
  Associated_Contributions EW|LO1 {LJET};
  Enhance_Observable VAR{log10(max(max(PPerp(p[4]),PPerp(p[5])),Mass(p[4]+p[5])))}|1|2.7 {4,5,6}
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  PSI_ItMin 20000 {5}
  Integration_Error 0.99 {5}
  PSI_ItMin 50000 {6}
  Integration_Error 0.99 {6}
  End process
}(processes)

(selector){
  "PT"  22  7.0,E_CMS:7.0,E_CMS [PT_UP]
  "DR"  22,90  0.1,1000:0.1,1000:0.1,1000:0.1,1000
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO 22 22 0.2 1000
  Mass  13  -13  10.0  E_CMS
}(selector)
"""
genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
