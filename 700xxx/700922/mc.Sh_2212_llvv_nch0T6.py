evgenConfig.description = "2 oppositely charged leptons + 2 neutrinos with 0,1j@NLO + 2,3j@LO and mll>4GeV, pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "oldrich.kepka@cern.ch" ]
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 23

if runArgs.trfSubstepName == 'generate' :
   print ("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   #--------------------------------------------------------------
   include('GeneratorFilters/MultiLeptonFilter.py')
   ### Default cut params
   filtSeq.MultiLeptonFilter.Ptcut = 3500.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   include('GeneratorFilters/ChargedTrackWeightFilter.py')
   filtSeq.ChargedTracksWeightFilter.NchMin = 0
   filtSeq.ChargedTracksWeightFilter.NchMax = 6
   filtSeq.ChargedTracksWeightFilter.Ptcut = 500
   filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
   filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 12, 20, 27 ]
   filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.07, 0.07, 0.15, 0.2, 0.23, 0.23 ]

   postSeq.CountHepMC.CorrectRunNumber = True

