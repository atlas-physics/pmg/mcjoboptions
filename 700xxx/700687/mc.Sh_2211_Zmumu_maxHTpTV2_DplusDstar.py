evgenConfig.description = "Sherpa Z/gamma* -> e e + 0,1,2j@NLO + 3,4,5j@LO with c-jet filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "mdshapiro@lbl.gov","MihaMuskinja@lbl.gov" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 14

if runArgs.trfSubstepName == 'generate' :
   print("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include("EvtGen_i/EvtGen_Fragment.py")
   evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
   genSeq.EvtInclusiveDecay.readExisting = True
   genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
   genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
   genSeq.EvtInclusiveDecay.userDecayFile = "DplusAndMinusToKpipiAndD0ToKpi.dec"
   evgenConfig.auxfiles += [ 'DplusAndMinusToKpipiAndD0ToKpi.dec']

# Don't some states with mass mismatches between EvtGen and Sherpa
# These are all strongly decaying states, so it is fine to use Sherpa decays
   genSeq.EvtInclusiveDecay.whiteList.remove(10411)
   genSeq.EvtInclusiveDecay.whiteList.remove(-10411)
   genSeq.EvtInclusiveDecay.whiteList.remove(10421)
   genSeq.EvtInclusiveDecay.whiteList.remove(-10421)
   genSeq.EvtInclusiveDecay.whiteList.remove(-10423)
   genSeq.EvtInclusiveDecay.whiteList.remove(10423)
   genSeq.EvtInclusiveDecay.whiteList.remove(10431)
   genSeq.EvtInclusiveDecay.whiteList.remove(-10431)
   genSeq.EvtInclusiveDecay.whiteList.remove(-10433)
   genSeq.EvtInclusiveDecay.whiteList.remove(10433)
   genSeq.EvtInclusiveDecay.whiteList.remove(-20433)
   genSeq.EvtInclusiveDecay.whiteList.remove(20433)

   include("GeneratorFilters/MultiLeptonFilter.py")
   filtSeq.MultiLeptonFilter.Ptcut = 15000.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
   DplusHadronFilter = HeavyFlavorHadronFilter("DplusHadronFilter")
   filtSeq += DplusHadronFilter
   DplusHadronFilter.RequestSpecificPDGID=True
   DplusHadronFilter.RequestCharm = False
   DplusHadronFilter.RequestBottom = False
   DplusHadronFilter.Request_cQuark = False
   DplusHadronFilter.Request_bQuark = False
   DplusHadronFilter.PDGPtMin=7.0*GeV
   DplusHadronFilter.PDGEtaMax=2.3
   DplusHadronFilter.PDGID=411
   DplusHadronFilter.PDGAntiParticleToo=True

   DstarHadronFilter = HeavyFlavorHadronFilter("DstarHadronFilter")
   filtSeq += DstarHadronFilter
   DstarHadronFilter.RequestCharm = False
   DstarHadronFilter.RequestBottom = False
   DstarHadronFilter.Request_cQuark = False
   DstarHadronFilter.Request_bQuark = False
   DstarHadronFilter.RequestSpecificPDGID = True
   DstarHadronFilter.RequireTruthJet = False
   DstarHadronFilter.RequestCharm=False
   DstarHadronFilter.PDGPtMin=7.0*GeV
   DstarHadronFilter.PDGEtaMax=2.3
   DstarHadronFilter.PDGID=413
   DstarHadronFilter.PDGAntiParticleToo=True

   filtSeq.Expression="MultiLeptonFilter and ((DplusHadronFilter) or (DstarHadronFilter))"
   
   postSeq.CountHepMC.CorrectRunNumber = True

