# aftertburner of 345718 
evgenConfig.description = "Sherpa+OpenLoops gg->(WW->)llvv + 0,1j, cf. arXiv:1309.0500, including the gg->h diagrams (+interference)."
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino" ]
evgenConfig.contact  = [ "oldrich.kepka@cern.ch" ]

evgenConfig.nEventsPerJob = 50000
evgenConfig.inputFilesPerJob = 17

if runArgs.trfSubstepName == 'generate' :
   print ("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   #--------------------------------------------------------------
   #--------------------------------------------------------------
   include('GeneratorFilters/MultiLeptonFilter.py')
   ### Default cut params
   filtSeq.MultiLeptonFilter.Ptcut = 3500.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   include('GeneratorFilters/ChargedTrackFilter.py')
   filtSeq.ChargedTracksFilter.NTracks = 21
   filtSeq.ChargedTracksFilter.NTracksMax = -1
   filtSeq.ChargedTracksFilter.Ptcut = 500
   filtSeq.ChargedTracksFilter.Etacut= 2.5

   postSeq.CountHepMC.CorrectRunNumber = True

