include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa W(qq)+gamma + 0,1j@NLO + 2,3j@LO with pT_y>140 GeV, pT_W>140 GeV"
evgenConfig.keywords = ["SM", "W", "quark", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){

  # Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  # HT prime scale 
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])+MPerp(p[2]))/4};

  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % Onshell W's
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2

}(run)

(processes){
  Process 93 93 -> 22 24 93{NJET}
  Enhance_Function VAR{max(PPerp2(p[2]),PPerp2(p[3]))/400.0}
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {3}
  Integration_Error 0.99 {3}
  PSI_ItMin 50000 {4,5,6,7}
  Integration_Error 0.99 {4,5,6,7}
  End process;

  Process 93 93 -> 22 -24 93{NJET};
  Enhance_Function VAR{max(PPerp2(p[2]),PPerp2(p[3]))/400.0}
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  PSI_ItMin 20000 {3}
  Integration_Error 0.99 {3}
  PSI_ItMin 50000 {4,5,6,7}
  Integration_Error 0.99 {4,5,6,7}
  End process;

}(processes)

(selector){
  PTNLO 22 140.0 E_CMS
  PTNLO 24 140.0 E_CMS
  PTNLO -24 140.0 E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  90  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 128 
