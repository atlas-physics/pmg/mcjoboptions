evgenConfig.description = "Sherpa Z -> ee + 0,1,2j@NLO + 3,4,5j@LO with 3 lepton filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "muon", "jets", "NLO", "3leptons" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 11

if runArgs.trfSubstepName == 'generate' :
   print("ERROR: These JO require an input file.  Please use the --afterburn option")

if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]

   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False

   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include("GeneratorFilters/MultiElecMuTauFilter.py")
   filtSeq.MultiElecMuTauFilter.NLeptons  = 3
   filtSeq.MultiElecMuTauFilter.MaxEta = 10.0
   filtSeq.MultiElecMuTauFilter.MinPt = 4000.0
   filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 10000.0

   include("GeneratorFilters/FourLeptonMassFilter.py")
   filtSeq.FourLeptonMassFilter.MinPt = 4000.
   filtSeq.FourLeptonMassFilter.MaxEta = 3.
   filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
   filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
   filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
   filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
   filtSeq.FourLeptonMassFilter.AllowElecMu = True
   filtSeq.FourLeptonMassFilter.AllowSameCharge = True

   filtSeq.Expression = "(not FourLeptonMassFilter) and (MultiElecMuTauFilter)"

   postSeq.CountHepMC.CorrectRunNumber = True
