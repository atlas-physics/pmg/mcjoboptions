include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.14 gamma gamma + 0,1j@NLO + 2,3j@LO with m_yy<50."
evgenConfig.keywords = ["SM", "diphoton", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "orcun.kolay@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  CORE_SCALE VAR{Abs2(p[2]+p[3])};
  EXCLUSIVE_CLUSTER_MODE 1;

  ALPHAQED_DEFAULT_SCALE=0.0;

  % tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=10;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN Internal;
  LOOPGEN:=OpenLoops;

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5;

  # NWF improvements
  NLO_CSS_PSMODE=1;
  PP_RS_SCALE VAR{0.25*sqr(PPerp(p[2])+PPerp(p[3]))};

  NLO_SUBTRACTION_SCHEME=2;
  CSS_KIN_SCHEME=0;
  NLO_CSS_KIN_SCHEME=0;

}(run)

(processes){
  Process 21 21 -> 22 22
  ME_Generator Internal;
  Loop_Generator gg_yy
  Scales VAR{FSF*Abs2(p[2]+p[3])}{RSF*Abs2(p[2]+p[3])}{QSF*Abs2(p[2]+p[3])};
  End process;

  Process 93 93 -> 22 22 93{NJET};
  Order (*,2);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]));
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  PSI_ItMin 50000 {3}
  Integration_Error 0.99 {3}
  PSI_ItMin 100000 {4,5}
  Integration_Error 0.02 {4}
  Integration_Error 0.05 {5}
  End process;
}(processes)

(selector){
  "PT" 22 20,E_CMS:18,E_CMS [PT_UP]
  RapidityNLO  22  -2.7  2.7
  IsolationCut  22  0.1  2  0.10;
  DeltaRNLO 22 22 0.2 1000.0;
  Mass 22 22  3.7  50;
}(selector)
"""

genSeq.Sherpa_i.NCores = 64
