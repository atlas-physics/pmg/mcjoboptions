include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")


evgenConfig.description = "Sherpa ttll+0,1j@NLO+1j@LO with Sherpa 2.2.12 and EW correction weights"
#evgenConfig.keywords = ["ttll", "SM", "multilepton"]
evgenConfig.contact = ["rohin.narayan@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.nEventsPerJob = 50

genSeq.Sherpa_i.RunCard="""
(run){
    #perturbative scales
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;

    # merging setup
    QCUT:=30.;
    LJET:=4,5; NJET:=1;
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR 0.5

    # EW corrections
    #ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3
    NLO_CSS_PSMODE=1

    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0;
    STABLE[6] 0;
}(run)
(processes){
    Process 93 93 -> 6 -6 90 90 93{NJET};
    Order (*,2);
    Print_Graphs feyn;
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;
}(processes)
(selector){
    Mass 11 -11  5 E_CMS;
    Mass 13 -13  5 E_CMS;
    Mass 15 -15  5 E_CMS;
}(selector)
"""

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
filtSeq.ElecMuTauFilter.NLeptons = 3
filtSeq.ElecMuTauFilter.IncludeHadTaus = True
filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
filtSeq.ElecMuTauFilter.MaxEta = 2.8

if not hasattr(filtSeq, "TwoLeptonFilter"):
  from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
  OneLeptonFilter = MultiElecMuTauFilter(name="TwoLeptonFilter")

filtSeq += MultiElecMuTauFilter("TwoLeptonFilter")
filtSeq.TwoLeptonFilter.NLeptons = 2
filtSeq.TwoLeptonFilter.IncludeHadTaus = True
filtSeq.TwoLeptonFilter.MinVisPtHadTau = 7000.
filtSeq.TwoLeptonFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
filtSeq.TwoLeptonFilter.MaxEta = 2.8

filtSeq.Expression = "(not ElecMuTauFilter) and (TwoLeptonFilter)"

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0", "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1"]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
#genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplltt", "ppllttj", "pplltt_ew", "ppllttj_ew" ]
