include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa yybb 90<myy<175 with NLO+PS in the 4FS"
evgenConfig.keywords = ["SM", "2photon", "heavyFlavour", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "heberth.torres@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  QSF:=1;
  SCALES VAR{H_TM2/4}{0.25*sqrt(MPerp(p[2])*MPerp(p[3])*MPerp(p[4])*MPerp(p[5]))}{sqr(QSF)*H_TM2/4}
  CORE_SCALE VAR{MU_F2}{MU_R2}{sqr(QSF)*H_TM2/4}
  EXCLUSIVE_CLUSTER_MODE 1;
  CSS_RESPECT_Q2=1
  CSS_KMODE=34
  CSS_KIN_SCHEME=0

  # massive b parameters
  MASSIVE[5]=1
  MASS[5]=4.75
  CSS_SCALE_SCHEME 20
  CSS_EVOLUTION_SCHEME 30

  NLO_CSS_PSMODE=1
}(run)

(processes){
  Process 93 93 -> 22 22 5 -5
  Order (*,2);
  NLO_QCD_Mode MC@NLO
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  PSI_ItMin 20000
  Integration_Error 0.99
  End process
}(processes)

(selector){
  "PT" 22 30,E_CMS:20,E_CMS [PT_UP]
  RapidityNLO  22  -2.7  2.7
  IsolationCut  22  0.1  2  0.10
  Mass 22 22  90.0 175.0
  DeltaRNLO 22 22 0.2 1000.0
}(selector)
"""

genSeq.Sherpa_i.NCores = 128

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4, "WZ")

from GeneratorFilters.GeneratorFiltersConf import DiBjetFilter
filtSeq += DiBjetFilter()
filtSeq.DiBjetFilter.LeadJetPtMin = 15000.0
filtSeq.DiBjetFilter.LeadJetPtMax = 50000000.0
filtSeq.DiBjetFilter.BottomPtMin = 5000.0
filtSeq.DiBjetFilter.BottomEtaMax = 3.0
filtSeq.DiBjetFilter.JetPtMin = 15000.0
filtSeq.DiBjetFilter.JetEtaMax = 3.0
filtSeq.DiBjetFilter.DeltaRFromTruth = 0.3
filtSeq.DiBjetFilter.TruthContainerName = "AntiKt4WZTruthJets" 
filtSeq.DiBjetFilter.AcceptSomeLightEvents = False
