evgenConfig.description = "0L Filter for muQ025 variation of Sherpa ttW+0,1j@NLO+2j@LO with Sherpa 2.2.14"
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.nEventsPerJob = 20000
evgenConfig.inputFilesPerJob = 8 

if runArgs.trfSubstepName == 'generate' :
   print("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())
   
   from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
   filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
   filtSeq.ElecMuTauFilter.NLeptons = 1
   filtSeq.ElecMuTauFilter.IncludeHadTaus = True
   filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
   filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
   filtSeq.ElecMuTauFilter.MaxEta = 2.8

   if not hasattr(filtSeq, "ZeroLeptonFilter"):
      from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
      ZeroLeptonFilter = MultiElecMuTauFilter(name="ZeroLeptonFilter")

   filtSeq += MultiElecMuTauFilter("ZeroLeptonFilter")
   filtSeq.ZeroLeptonFilter.NLeptons = 0
   filtSeq.ZeroLeptonFilter.IncludeHadTaus = True
   filtSeq.ZeroLeptonFilter.MinVisPtHadTau = 7000.
   filtSeq.ZeroLeptonFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
   filtSeq.ZeroLeptonFilter.MaxEta = 2.8

   filtSeq.Expression = "(not ElecMuTauFilter) and (ZeroLeptonFilter)"

   postSeq.CountHepMC.CorrectRunNumber = True