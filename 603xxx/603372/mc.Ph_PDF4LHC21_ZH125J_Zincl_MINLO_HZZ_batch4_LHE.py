#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production: Z->all"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch', 'ana.cueto@cern.ch']
evgenConfig.process = "qq->ZH, H->incl, Z->all"
evgenConfig.generators   = ['Powheg']
evgenConfig.nEventsPerJob = 100


#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> inv + l+l- production
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_HZj_Common.py")

PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode = "z > all" # Z->all
PowhegConfig.mass_Z_low = 10


PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#NNPDF3.0 and PDF4LHC PDF variations
#PDF4LHC21 PDF variations (keeping also variation for PDF4LHC15 correlation studies and NNPDF3.0 for correlation with bkg samples if needed)                                                                                                                             
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]

# scale variations: first pair is the nominal setting
PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] 
PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate()



