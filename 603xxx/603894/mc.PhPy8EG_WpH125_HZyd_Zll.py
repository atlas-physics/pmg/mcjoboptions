#---------------------------------------------------------------
# LHE files of WpH used as inputs 
# POWHEG+Pythia8 WpH(Winclusive), H->Z+yd, Z->ll, mH=125GeV, m_yd=0
# W->qq filter
#---------------------------------------------------------------

# input LHE dataset:
#mc15_13TeV.345039.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Wincl_MINLO.evgen.TXT.e5590
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 58

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# H->Z+yd decay, Z->ll
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 23 4900022', # H->Z+yd
                            '25:onIfMatch = 23 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            '23:onMode = off',  # Z decay off
                            '23:onIfAny = 11 13 15' # Z decay to charged leptons
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py') 
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#--------------------------------------------------------------
# Filter for W->qq
#--------------------------------------------------------------
include('GeneratorFilters/DecaysFinalStateFilter.py')
DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ 24, -24 ]
DecaysFinalStateFilter.NQuarks = 2
DecaysFinalStateFilter.NChargedLeptons = 0


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 WpH production: H->Z+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'WHiggs' , 'Z' , 'darkPhoton' ]
evgenConfig.contact     = [ 'rachid.mazini@cern.ch' ]
evgenConfig.process = "WpH, Wp->qq, H->Z+yd, Z->ll"
