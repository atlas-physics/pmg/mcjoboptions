# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

# set the common Powheg parameters of W and Z Powheg+Pythia AZNLO generation at LO EW

PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

# AZNLO config
PowhegConfig.ptsqmin=4

# LO EW mode
PowhegConfig.no_ew=1
PowhegConfig.PHOTOS_enabled = False # this disables running Photos directly on LHE files

#Gmu EW scheme inputs matching what used in the Powheg V1 Z samples
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
JOName = get_physics_short()
if any(Z in JOName for Z in ['Zee', 'Zmumu', 'Ztautau']):
    PowhegConfig.scheme=0
    PowhegConfig.alphaem=0.00781653
    PowhegConfig.mass_W=79.958059
    # assume the usual m>60 GeV!
    PowhegConfig.mass_low=60

if "20M60" in JOName:
    PowhegConfig.mass_low=20
    PowhegConfig.mass_high=60


# Powheg integration and fold settings (samples are supposed to be used with integration grids)
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 400000
PowhegConfig.ncall2       = 400000
PowhegConfig.nubound      = 1000000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 1
PowhegConfig.ubexcess_correct = 0 # disable excess weights (tested)
PowhegConfig.storemintupb = 0 # much smaller grids

# update PDF list to most useful for W,Z precision analysis
# historic CT10 10800 as central value for AZNLO tune
PowhegConfig.PDF = [10800]
# latest 'nominal' global fits & PDF4LHC21  with full error sets
PowhegConfig.PDF += range(304400, 304500+1) # NNPDF31_nnlo_as_0118_hessian
PowhegConfig.PDF += range(331600, 331652+1) # NNPDF40_nnlo_hessian_pdfas (central as=0.118)
PowhegConfig.PDF += range(14000, 14058+1) # CT18NNLO
PowhegConfig.PDF += range(27400, 27464+1) # MSHT20nnlo_as118
PowhegConfig.PDF += range(93300, 93342+1) #PDF4LHC21_40_pdfas
# ATLAS-specifics & ABMP16
PowhegConfig.PDF += range(14200, 14258+1) # CT18ANNLO
PowhegConfig.PDF += range(65700, 65752+1) # ATLASpdf21_T3
PowhegConfig.PDF += range(42560, 42589+1) # ABMP16
#PowhegConfig.PDF += range(11200, 11250+1) # CT10nnlo - no space
PowhegConfig.PDF += [11200] # CT10nnlo - central only
# add a selection of special and historic PDF central values
PowhegConfig.PDF += [325100, 260000, 303200, 90400] # NNPDF31_nnlo_as_0118_luxqed, NNPDF30_nlo_as_0118, NNPDF30_nnlo_as_0118_hessian, PDF4LHC15_nlo_30_pdfas
PowhegConfig.PDF += [29100, 29250] # MSHT20an3lo_as118, MSHT20an3lo_as118_Kcorr
PowhegConfig.PDF += [13000, 25300, 61200] # CT14, MMHT14, HERAPDF2.0
PowhegConfig.PDF += [21100, 21200, 10550] # MSTW2008nlo68cl, MSTW2008nnlo68cl, CTEQ6.6

