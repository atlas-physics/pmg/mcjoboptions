#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune. + nuclear pdfs"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["mhoppesc@cern.ch"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.generators = ["Powheg"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_jj_Common.py")
PowhegConfig.bornktmin =75
PowhegConfig.bornsuppfact =1800

PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          =  [14000, 304400, 25300, 14200, 14100, 14300, 14400, 25100, 91500, 42560, 317500, 303600, 319300, 319500, 322500, 322700, 322900, 323100, 323300, 323500, 323700, 323900, 27400, 107100, 103100, 115650, 119150, 3201200, 3206200, 30025400, 260000, 901300, 13165, 904400, 14668]  
PowhegConfig.rwl_group_events = 10000

PowhegConfig.ncall1       = 80000
PowhegConfig.ncall2       = 80000
PowhegConfig.nubound      = 4800000
PowhegConfig.itmx1        = 20
PowhegConfig.itmx2        = 20

### Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 2
PowhegConfig.foldy        = 2

PowhegConfig.nEvents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob

# PDF variations with nominal scale variation:
# CT18NNLO,	 
# NNPDF31_nnlo_as_0118_hessian, 
# MMHT2014nnlo68cl, 
# CT18ANNLO, 
# CT18ZNNLO, 
# CT18XNNLO, 
# CT18NLO, 
# MMHT2014nlo68cl, 
# PDF4LHC15_nnlo_mc, 
# ABMP16_5_nnlo, 
# NNPDF31_nnlo_as_0118_nojets, 
# NNPDF31_nnlo_as_0118
# NNPDF31_nnlo_as_0116
# NNPDF31_nnlo_as_0120
# NNPDF31_nnlo_as_0108
# NNPDF31_nnlo_as_0110
# NNPDF31_nnlo_as_0112
# NNPDF31_nnlo_as_0114
# NNPDF31_nnlo_as_0117
# NNPDF31_nnlo_as_0119
# NNPDF31_nnlo_as_0122
# NNPDF31_nnlo_as_0124
# MSHT20nnlo_as118
# nCTEQ15npFullNuc_208_82
# nCTEQ15FullNuc_208_82
# nCTEQ15WZ_FullNuc_208_82
# nCTEQ15WZSIH_FullNuc_208_82
# TUJU19_nlo_208_82
# TUJU19_nnlo_208_82
# nNNPDF30_nlo_as_0118_A208_Z82
# NNPDF30_nlo_as_0118
# EPPS16nlo_CT14nlo_Pb208
# CT14nlo_as_0118
# EPPS21nlo_CT18Anlo_Pb208
# CT18ANLO_as_0118

PowhegConfig.PDF.extend(range(14001, 14059))                            # Include the CT18NNLO error set
PowhegConfig.PDF.extend(range(304401, 304501))                           # Include the NNPDF31_nnlo_as_0118_hessian error set
#PowhegConfig.PDF.extend(range(25301, 25351))                             # Include the MMHT2014nnlo68cl error set
PowhegConfig.PDF.extend(range(27401, 27465))                            # Include the MSHT20nnlo_as118 error set
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()
