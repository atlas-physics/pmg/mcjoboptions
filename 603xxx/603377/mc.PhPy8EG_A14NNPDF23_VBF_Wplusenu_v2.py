# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF W+(enu) production with A14 NNPDF2.3 tune. Clone of 600934 with std PDFs"
evgenConfig.keywords = ["SM", "VBF", "W"]
evgenConfig.contact = ["oliver.rieger@cern.ch", "jan.kretzschmar@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_W process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_W_Common.py")

PowhegConfig.decay_mode = "w+ > e+ ve"
#PowhegConfig.ptj_gencut = 20

PowhegConfig.ncall1 = 2000000
PowhegConfig.ncall2 = 2000000
PowhegConfig.itmx1 = 5
PowhegConfig.itmx2 = 10

PowhegConfig.nubound = 1000000
PowhegConfig.xupbound = 4

# fold (2,2,2) gives <1% neg weights
PowhegConfig.foldphi = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2

### default PDFs 

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
