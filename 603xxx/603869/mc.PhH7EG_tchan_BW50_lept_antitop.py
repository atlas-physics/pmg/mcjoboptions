#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Herwig7.3 single-top-quark t-channel (2->3) production (top),MadSpin.'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.generators  = ['EvtGen', 'Herwig7', 'Powheg']  
evgenConfig.contact     = [ 'lukas.kretschmann@cern.ch' ]
evgenConfig.tune = "H7-Default"
evgenConfig.inputFilesPerJob = 20
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------                                                                                                                                          
# Herwig7 (H7UE) showering                                                                                                                                                                               
#--------------------------------------------------------------                                                                                                                                         

# initialize Herwig7 generator configuration for showering of LHE files                                                                                                                                  
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7                                                                                                                                                                                      
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                                                                             
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7                                                                                                                                                                                            
Herwig7Config.run()
