# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# This is an example joboption to generate events with Powheg
# using ATLAS' interface. Users should optimise and carefully
# validate the settings before making an official sample request.
#--------------------------------------------------------------
import os
import re
from MCJobOptionUtils.JOsupport import get_physics_short

#get JO name
job_option_name = get_physics_short()
print(job_option_name)

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Drell-Yan Vector LeptoQuark production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["BSM", "leptoquark"]
evgenConfig.contact = ["tpelzer@cern.ch"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Drell-Yan Vector LeptoQuark process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_DY_VLQ_Common.py")

# --------------------------------------------------------------
# Relevant parameters for this process
# --------------------------------------------------------------
# 4321 Model Parameters
PowhegConfig.g4 = 1.414214 # SU(4) coupling strength g4

# Get betaL33 from jo file
matches_L33 = re.search("33L([0-9]_[0-9]+).*", job_option_name)
if matches_L33 is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    L33=0.0
else:
    L33= float(matches_L33.group(1).replace("_", "."))
PowhegConfig.betaL3x3 = L33 # Relative coupling strength to left-handed fermions (3rd generation)

# Get betaR33 from jo file
matches_R33 = re.search("33R([0-9]_[0-9]+).*", job_option_name)
if matches_R33 is None:
    print("Cannot find betaR 33 string in job option name: {:s}.".format(job_option_name))
    R33=0.0
else:
    R33= float(matches_R33.group(1).replace("_", "."))
PowhegConfig.betaR3x3 = R33 # Relative coupling strength to right-handed fermions (3rd generation)

# Get MU from jo file
matches_MU = re.search("MU([0-9]+).*", job_option_name)
if matches_MU is None:
    print("Cannot find MU string in job option name: {:s}.".format(job_option_name))
    MU=0.0
else:
    MU= float(matches_MU.group(1).replace("_", "."))
PowhegConfig.MU1 = MU # Mass of vector leptoquark U1
PowhegConfig.MGp = MU # Mass of the coloron Gp, take MGp=MU now, can also take MGp=2.5MU

# General Leptoquark (LQ) Parameters
PowhegConfig.SM = 0 # Include SM contribution
PowhegConfig.LQ = 1 # Include basic LQ contributions
PowhegConfig.LQ_Int = 1 # Include the interference between the SM and the LQ contributions
PowhegConfig.bornonly = 0 # NOT Include NLO
PowhegConfig.mass_t = 172.5 # top-quark (running) mass

# Get mass_low from jo file
matches_slice = re.search("SL([0-9]+).*", job_option_name)
if matches_slice is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    slice_low=0.0
else:
    slice_low= float(matches_slice.group(1))
PowhegConfig.mass_low = slice_low # lower limit for dilepton mass
if(slice_low==0.):
    PowhegConfig.mass_low = -1 # upper limit for dilepton mass
    PowhegConfig.mass_high = -1 # upper limit for dilepton mass
elif(slice_low==200.):
    PowhegConfig.mass_high = 500 # upper limit for dilepton mass
elif(slice_low==500.):
    PowhegConfig.mass_high = 1000 # upper limit for dilepton mass
elif(slice_low==2000.):
    PowhegConfig.mass_high = -1 # upper limit for dilepton mass
else:
    PowhegConfig.mass_high = slice_low + 1000. # upper limit for dilepton mass
#PowhegConfig.runningscale = 1
#PowhegConfig.new_damp = 1
#PowhegConfig.hnew_damp = 0.5
#PowhegConfig.hdamp = 1.0
PowhegConfig.decay_mode = "ta+ ta-"

# --------------------------------------------------------------
# Integration settings
# --------------------------------------------------------------
PowhegConfig.ncall1 		= 10000
PowhegConfig.ncall2 		= 10000
PowhegConfig.nubound 		= 10000
PowhegConfig.smartsig       = 0
PowhegConfig.bornzerodamp   = 0
PowhegConfig.hfact          = -1

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
# Setting the appropriate number of final state particles for the main31 routine
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
