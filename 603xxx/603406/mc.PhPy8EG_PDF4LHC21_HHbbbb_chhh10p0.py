#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to bbbb, with Powheg-Box-V2, at NLO + full top mass, with PDF4LHC21 PDF, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom"]
evgenConfig.contact = ["yizhou.cai@cern.ch"]
evgenConfig.nEventsPerJob  = 500

# --------------------------------------------------------------
# Configuring Powheg
# --------------------------------------------------------------

### Load ATLAS defaults for the Powheg ggF_HH process
include("PowhegControl/PowhegControl_ggF_HH_Common.py")

PowhegConfig.hdamp = 250
### Modify couplings
PowhegConfig.chhh = 10.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]

### scales and PDF sets
PowhegConfig.PDF = list(range(93300,93343)) # PDF4LHC21_40_pdfas with error sets
PowhegConfig.PDF += list(range(90400,90433)) # PDF4LHC15_nlo_30_pdfas with error sets
PowhegConfig.PDF += list(range(325300,325403)) # NNPDF30_nnlo_as_0118 with error sets
PowhegConfig.PDF += [27100,14400,331700] # MSHT20nlo_as118, CT18NLO, NNPDF40_nlo_as_01180 nominal sets
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0]

### Generate events
PowhegConfig.generate()

#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")
### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### selectin Higgs decays
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 5 -5 ", # bb decay
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale



