#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production with chhh=5.0 using Powheg-Box-V2 at NLO + full top mass, with PDF4LHC21 PDF. Decay to bbyy and PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom", "photon", "diphoton"]
evgenConfig.contact = [ "tpelzer@cern.ch", "brendon.aurele.bullard@cern.ch" ]
evgenConfig.tune    = "H7.2-Default"
evgenConfig.nEventsPerJob  = 200
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]

# --------------------------------------------------------------
# Configuring Powheg
# --------------------------------------------------------------

### Load ATLAS defaults for the Powheg ggF_HH process
include("PowhegControl/PowhegControl_ggF_HH_Common.py")

### important parameters
PowhegConfig.mtdep = 3 # full theory
PowhegConfig.mass_H = 125 # Higgs boson mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.mass_t = 173 # top-quark mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.hdamp = 250

### Modify coupling
PowhegConfig.chhh = 5.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]

### accounting for Herwig7 and filter efficiencies
PowhegConfig.nEvents *= 2.5

### scales and PDF sets
PowhegConfig.PDF += [27100,14400,331700] # MSHT20nlo_as118, CT18NLO, NNPDF40_nlo_as_01180 nominal sets
PowhegConfig.mu_F = 1.0
PowhegConfig.mu_R = 1.0

### Generate events
PowhegConfig.generate()

#--------------------------------------------------------------
# Configuring Herwig7
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC21_40_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# use H->bb and H->gammagamma decays
Herwig7Config.add_commands("""
  do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->b,bbar;
  do /Herwig/Particles/h0:PrintDecayModes
  set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
  set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
  # print out decays modes and branching ratios to the terminal/log.generate 
  set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
  set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes 
  set /Herwig/Partons/RemnantDecayer:AllowTop No
""")

# run Herwig7
Herwig7Config.run()

#---------------------------------------------------------------------------------------------------
# Filter for bbyy
#---------------------------------------------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "HbbFilter and HyyFilter"

