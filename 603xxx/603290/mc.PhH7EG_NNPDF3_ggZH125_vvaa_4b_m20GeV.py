evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125" ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = "PowhegBox/Herwig7 H+Z+jet->v+v-bbbarbbbar production"
evgenConfig.process     = "hSM->a1a2->4b, Z->vv"
evgenConfig.contact     = [ 'rachel.smith@cern.ch' ]
evgenConfig.tune        = "H7.1-Default"
#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob, math, random, ROOT, re

#--------------------------------------------------------------
# Standard Parameters
#--------------------------------------------------------------

ma1 = 20. #GeV
ma2 = 20. #GeV
mH = 125. #GeV
mb = 4.8 #GeV

evgenLog.info("mh = %.2f"%mH)
evgenLog.info("ma1 = %.2f"%ma1)
evgenLog.info("ma2 = %.2f"%ma2)

def triangleFunction(m1, m2, m3):
    return math.sqrt(math.pow(m1,4.)+math.pow(m2,4.)+math.pow(m3,4.)-2.*(math.pow(m1,2)*math.pow(m2,2)+math.pow(m1,2)*math.pow(m3,2)+math.pow(m2,2)*math.pow(m3,2)))/(2.*m3)

def decayScalar(higgs, ma1, ma2):

    mH = higgs.M()
    betaH = higgs.BoostToCM()
    boostH = ROOT.Math.Boost(-betaH.X(),-betaH.Y(),-betaH.Z())

    thetaa = random.uniform(0,math.pi)
    phia = random.uniform(0,2.*math.pi)
    pa = triangleFunction(ma1, ma2, mH)
    ea1 = math.sqrt(pa*pa+ma1*ma1)
    ea2 = math.sqrt(pa*pa+ma2*ma2)
    a1 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(pa*math.sin(thetaa)*math.cos(phia),pa*math.sin(thetaa)*math.sin(phia),pa*math.cos(thetaa),ea1)
    a2 = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(-pa*math.sin(thetaa)*math.cos(phia),-pa*math.sin(thetaa)*math.sin(phia),-pa*math.cos(thetaa),ea2)
    a1_lab = boostH(a1)
    a2_lab = boostH(a2)

    return [a1_lab,a2_lab]

for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    buffer = ''
    numParticles = -1
    for line in f1:
        newEvent = re.search(r'(\d+)  10001', line)
        if line.startswith('     2212     2212  6.50000E+03  6.50000E+03     -1     -1     -1     -1     -4      1'):
            f2.write('     2212     2212  6.50000E+03  6.50000E+03     -1     -1     -1     -1     -1      1\n')
        elif newEvent is not None:
            numParticles = int(newEvent.group(1))
            newline = '{:6d}'.format(numParticles+6) + line[7:]
            f2.write(newline)
        elif line.startswith('      25     1'):
            evtinfo = line.split()
            higgs = ROOT.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')(float(evtinfo[6]),float(evtinfo[7]),float(evtinfo[8]),float(evtinfo[9]))
            a1,a2 = decayScalar(higgs, ma1, ma2)
            b11,b12 = decayScalar(a1, mb, mb)
            b21,b22 = decayScalar(a2, mb, mb)
            f2.write('{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(25,2,1,2,0,0,higgs.Px(),higgs.Py(),higgs.Pz(),higgs.E(),higgs.M(),0.,9.))
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(25,2,3,3,0,0,a1.Px(),a1.Py(),a1.Pz(),a1.E(),a1.M(),0.,9.)
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(25,2,3,3,0,0,a2.Px(),a2.Py(),a2.Pz(),a2.E(),a2.M(),0.,9.)
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(5,1,numParticles+1,numParticles+1,521,0,b11.Px(),b11.Py(),b11.Pz(),b11.E(),b11.M(),0.,9.)
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-5,1,numParticles+1,numParticles+1,0,521,b12.Px(),b12.Py(),b12.Pz(),b12.E(),b12.M(),0.,9.)
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(5,1,numParticles+2,numParticles+2,522,0,b21.Px(),b21.Py(),b21.Pz(),b21.E(),b21.M(),0.,9.)
            buffer = buffer + '{:8d} {:5d} {:5d} {:5d} {:5d} {:5d} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 16.9E} {: 12.5E} {: 10.3E}\n'.format(-5,1,numParticles+2,numParticles+2,0,522,b22.Px(),b22.Py(),b22.Pz(),b22.E(),b22.M(),0.,9.)
        elif line.startswith('<rwgt>'):
            f2.write(buffer)
            buffer = ''
            f2.write(line)
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

# initialize Herwig7 generator configuration for showering of LHE files

include("Herwig7_i/Herwig7_LHEF.py")


# configure Herwig7

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename='merged_lhef._0.events', me_pdf_order="NLO")


include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
