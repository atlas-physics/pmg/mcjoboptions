# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 W+munu production, AZNLO, new PDF selection"
evgenConfig.keywords = ["SM", "W", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 10000
filterMultiplier = 1.1

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg W_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_W_EW_Common.py")

PowhegConfig.decay_mode = "w+ > mu+ vm"

include("PowhegControl_WorZ_AZNLO_LOEW.py")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
# this is needed to approximately reproduce low pT(V) in Pythia8.245 and Pythia8.3
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 1.4"]

# next level of Photos
genSeq.Photospp.CreateHistory = True # restore Photospp_i default that changed in 22.6
genSeq.Photospp.ZMECorrection = False
genSeq.Photospp.WMECorrection = True
genSeq.Photospp.PhotonSplitting = True
genSeq.Photospp.WtInterference = 4.0 # increase Photos upper limit for ME corrections
