#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune for dilepton events.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators  = ['EvtGen', 'Herwig7', 'Powheg']  

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 8

evgenConfig.tune = "H7-Default"

#--------------------------------------------------------------                                                                                                                                          
# Herwig7 (H7UE) showering                                                                                                                                                                               
#--------------------------------------------------------------                                                                                                                                         
   
# initialize Herwig7 generator configuration for showering of LHE files                                                                                                                                  
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7                                                                                                                                                                                      
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                                                                             
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7                                                                                                                                                                                            
Herwig7Config.run()



#--------------------------------------------------------------                                                                                                                                        
# Event filter                                                                                                                                                                                           
#--------------------------------------------------------------                                                                                                                                          
  
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
