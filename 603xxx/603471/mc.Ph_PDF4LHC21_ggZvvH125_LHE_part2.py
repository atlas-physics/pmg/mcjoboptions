#--------------------------------------------------------------
# POWHEG ZH+jet production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode_Z = "z > v v~"

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + [260000] + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.kappa_ghz = [1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0]
PowhegConfig.kappa_ght = [1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0]
PowhegConfig.kappa_ghb = [1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0]

PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG ggZ(->vv)H+jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators   = ['Powheg']
evgenConfig.process = "ggZH, Z->vv"
evgenConfig.nEventsPerJob = 550
