
import math
#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 1200
PowhegConfig.width_H = 40

# Turn on the heavy quark effect
#PowhegConfig.use_massive_b = True
#PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 1

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
#PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = 38

# Additonal setting for MSSM group
#PowhegConfig.storeinfo_rwgt = 1
#PowhegConfig.hdecaymode = 0

# Increase number of events requested to compensate for filter efficiency:
calc_factor = lambda n, br : (5*math.sqrt(n)+n)/(n*br)
PowhegConfig.nEvents = int( calc_factor(PowhegConfig.nEvents, 0.42) * PowhegConfig.nEvents )

PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ]

#--------------------------------------------------------------
# Pythia8 matching and Higgs
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = -1',
                             '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MSSM ggH Higgs mass 1200 GeV width 40 GeV H->tautau->lh"
evgenConfig.keywords    = [ "Higgs", "BSMHiggs", "2tau" ]
evgenConfig.contact  = ["Adrian Salvador Salas <adrian.salvador.salas@cern.ch>"]
evgenConfig.nEventsPerJob=10000

# ... Filter H->VV->Children
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended

filtSeq += XtoVVDecayFilterExtended("XtoVVDecayFilterExtended")

filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]

filtSeq.Expression = "(XtoVVDecayFilterExtended)"

