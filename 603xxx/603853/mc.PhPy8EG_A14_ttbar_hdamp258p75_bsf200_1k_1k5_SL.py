#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune for single lepton events, BSF 200, HT slice 1000-1500 GeV.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'nhidic@cern.ch' ]
evgenConfig.nEventsPerJob = 2000

include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.decay_mode = "t t~ > all"

# Initial settings
PowhegConfig.hdamp          = 258.75                                        # 1.5 * mtop
PowhegConfig.bornsuppfact 	= 200.0
PowhegConfig.doublefsr 		= 1

PowhegConfig.mu_F           = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R           = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales

filterMultiplier = 1.8/(0.006458) # safety factor
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier
PowhegConfig.rwl_group_events = 50000

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]



#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#--------------------------------------------------------------
# HT filter
#--------------------------------------------------------------
include("GeneratorFilters/xAODHTFilter_Common.py")
filtSeq.xAODHTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.xAODHTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.xAODHTFilter.MinHT = 1000.*GeV # Min HT to keep event
filtSeq.xAODHTFilter.MaxHT = 1500.*GeV # Max HT to keep event
filtSeq.xAODHTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.xAODHTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.xAODHTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

filtSeq.Expression = "(TTbarWToLeptonFilter and xAODHTFilter)"
