
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 pThard2 bblvlv_beta dilepton with all dilep flav combinations, hdamp 1.5 mtop"
evgenConfig.keywords = [ 'SM', 'top', 'WWbb', 'lepton']
evgenConfig.contact = ["katharin@cern.ch"]
# starting from nominal bb4l-dl LHE files (200 events per file, DSID: 602496)
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, BB4L UserHook and Py8 Splitting Kernel Var. Weights
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ "TimeShower:recoilStrategyRF = 3" ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print ('UserHook present')
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ldlsl']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 2"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"] #default 0
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"] #default 0
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"] #default 0

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]                                                                                                                           
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 1.2"] # change from default 0.8 to 1.2 to have same as in HERWIG setting


