
#--------------------------------------------------------------                                           
# Pythia8 showering with the A14 NNPDF2.3 tune                                                            
#--------------------------------------------------------------                                           
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                               
include("Pythia8_i/Pythia8_Powheg_Main31.py")


genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']


#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',#decay of Z
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 12 13 14 15 16' ]



#--------------------------------------------------------------
# ZZ->llvv filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended

filtSeq += XtoVVDecayFilterExtended()
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 23
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13,15]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [12,14,16]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description      = "POWHEG+PYTHIA8+EVTGEN, qq->ZH, Z->ll, H->ZZ->llvv, mH=125 GeV"
evgenConfig.keywords         = [ "Higgs", "SM", "ZZ", "mH125" ]
evgenConfig.contact          = [ 'xinmeng.ye@cern.ch' ]
evgenConfig.nEventsPerJob    = 2000
evgenConfig.inputFilesPerJob = 90
