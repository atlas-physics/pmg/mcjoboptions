#--------------------------------------------------------------
# JO to be used with this input TXT container:
# mc23_13p6TeV.601454.Ph_PDF4LHC21_allHad_ttH125_LHE.evgen.TXT.e8537
#--------------------------------------------------------------

#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# TTbarWToLeptonFilter
# l+jets ttbar decay - input lhe files are inclusive
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'POWHEG+HERWIG7 ttH (allhad) production'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'linghua.guo@cern.ch' ]
evgenConfig.generators     = [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.inputFilesPerJob = 5
evgenConfig.nEventsPerJob    = 20000