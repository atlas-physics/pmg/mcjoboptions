# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF Z(mumu) production with A14 NNPDF2.3 tune. m(ll)>105 GeV"
evgenConfig.keywords = ["SM", "VBF", "Z"]
evgenConfig.contact = ["oliver.rieger@nikhef.nl", "jan.kretzschmar@cern.ch"]
evgenConfig.nEventsPerJob = 2000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_Z process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_Z_Common.py")

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.mll_gencut = 105
PowhegConfig.ptj_gencut = 20

PowhegConfig.ncall1 = 1000000
PowhegConfig.ncall2 = 2000000
PowhegConfig.itmx1 = 5
PowhegConfig.itmx2 = 10

PowhegConfig.nubound = 10000000
PowhegConfig.xupbound = 8

PowhegConfig.foldphi = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2

### default PDFs are fine

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
