# original JO: 600987
#---------------------------------------------------------------------------------------------------
# Common generator script for b-filtered non-standard top mass samples
#---------------------------------------------------------------------------------------------------
def parseargs(runArgs):
    # function for parsing the Generate_tf.py arguments
    options = {}
    if hasattr(runArgs,'ecmEnergy'):
        options['beamEnergy'] = runArgs.ecmEnergy / 2.
    else: 
        raise RuntimeError('No center of mass energy found.')

    if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:
        options['nevents']=runArgs.maxEvents
    else:
        raise RuntimeError('No maxEvents provided.')

    if hasattr(runArgs,'randomSeed'):
        options['randomSeed'] = runArgs.randomSeed
    else:
        raise RuntimeError('No random seed provided.')

    if hasattr(runArgs,'inputGeneratorFile'):
        options['lheFile'] = runArgs.inputGeneratorFile
    else:
        options['lheFile'] = ''
    return options

# Original function defined in MCJobOptionsUtils/JOsupport.py
def get_physics_short():
    FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
    jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]
    if len(jofiles)==0:
        raise RuntimeError('No job options found in '+FIRST_DIR)
    joparts = os.path.basename(jofiles[0]).split('.')
    if len(joparts)<2:
        raise RuntimeError('Malformed job options file name: '+jofiles[0])
    return joparts[1]

def convert_string_to_float(string):
    if "mt" in string:
        return float( (string.strip("mt")).replace("p",".") )
    elif "wt" in string:
        return float( (string.strip("wt")).replace("p",".") )
    elif "p" in string:
        return float( string.replace("p",".") )
    else:
        raise SyntaxWarning("WARNING: Cannot convert string")

# get arguments
options = parseargs(runArgs=runArgs)

shortname = get_physics_short()

print "Here is the physics short %s" % (shortname)
shortnameSplit = shortname.split("_")

if len(shortnameSplit) < 5:
    raise RuntimeError('Physics short name should have 5 qualifiers... [Generators]_[PhysicsProcess]_mt[masstop]_wt[widthtop]_[Bfilter]')

# parse the physics short name to get relevant qualifies
generators = shortnameSplit[0]
physicsProcess = shortnameSplit[1]
mtop = convert_string_to_float(shortnameSplit[2])
widthtop = convert_string_to_float(shortnameSplit[3])
bFilter = shortnameSplit[4]
bFilterDecayCard = bFilter + '.DEC'

pythiaNames = [ 'pythia', 'pythia8', 'py' ]
herwigNames = [ 'herwig', 'herwig7', 'h7' ]

# get showering algorithm based on generator name
isHerwig, isPythia = False, False
if any(pyAbb in generators.lower() for pyAbb in pythiaNames): isPythia = True
if any(hwgAbb in generators.lower() for hwgAbb in herwigNames): isHerwig = True

if not isHerwig and not isPythia:
  raise RuntimeError('Generator names {0} does not contain pythia or herwig qualifier'.format(generators))

print "########## Here is the job option configuration ##########"
print "########## Generators %s " % (generators)
print "########## Physics Process %s    " % (physicsProcess)
print "########## top mass %f GeV, width top %f GeV.    " % (mtop, widthtop)
print "########## B-filter type %s  " % (bFilter)
print "##########################################################"

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'Jpsi' ]
evgenConfig.contact     = [ 'derue@lpnhe.in2p3.fr', 'burton@utexas.edu', 'dpanchal@utexas.edu', 'asada@hepl.phys.nagoya-u.ac.jp' ]
evgenConfig.generators += [ 'Powheg', 'EvtGen' ]

if isPythia:
    evgenConfig.generators += ['Pythia8']
    evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, \
                               A14 tune with rb=1.05, A14 NNPDF23 LO, ME NNPDF30 NLO, at least one lepton, \
                               Jpsi->mumu filter.'
if isHerwig:
    evgenConfig.generators += ['Herwig7']
    evgenConfig.tune        = "MMHT2014"
    evgenConfig.description = 'POWHEG+Herwig713 ttbar production with Powheg hdamp equal 1.5*top mass, \
                               H7UE tune, single lepton filter, ME NNPDF30 NLO, H7UE MMHT2014 LO at \
                               least one lepton, Jpsi->mumu filter.'

#--------------------------------------------------------------
# Powheg matrix element
#--------------------------------------------------------------
if options['lheFile'] is '':
    include('PowhegControl/PowhegControl_tt_Common.py')
    # Initial settings
    PowhegConfig.decay_mode = "t t~ > all" # inclusive decay
    PowhegConfig.mass_t     = mtop
    PowhegConfig.width_t    = widthtop
    PowhegConfig.hdamp      = 1.5*mtop
    PowhegConfig.PDF        = 260000 # NNPDF30
    PowhegConfig.nEvents   *= 10.    # compensate filter efficiency
    PowhegConfig.generate()

#--------------------------------------------------------------
# Showering
#--------------------------------------------------------------
if isPythia: # Pythia8 (A14rb) showering
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    include("Pythia8_i/Pythia8_Powheg_Main31.py")
    genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
    genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
    genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
    genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
    genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
    genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
    genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
    # A14rb tune
    genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]

elif isHerwig: # Herwig7 (H7UE) showering
    include("Herwig7_i/Herwig7_LHEF.py")
    include('Herwig7_i/Herwig7_EvtGen.py')

    # Redefine the deltaM parameters to turn off b-mixing
    # Does not work with the current Herwig setup (Herwig 7.2.1)
#     Herwig7Config.add_commands("""
# set /Herwig/Particles/B0:DeltaM 0.0
# set /Herwig/Particles/Bbar0:DeltaM 0.0
# set /Herwig/Particles/B_s0:DeltaM 0.0
# set /Herwig/Particles/B_sbar0:DeltaM 0.0
#     """)

    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
    # With AthGeneration release 21.2.76 (Herwig 7.2), these commands do not work
#     Herwig7Config.add_commands("""
# set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
# set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED""")
    Herwig7Config.run()

#--------------------------------------------------------------
# Special decay of (anti-)B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += [ bFilterDecayCard ]
genSeq.EvtInclusiveDecay.userDecayFile = bFilterDecayCard

#--------------------------------------------------------------
# Event filters
#--------------------------------------------------------------
# apply a non all-hadronic decay filter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.

