evgenConfig.description = "POWHEG-BOX-RES/OpenLoops+Herwig7+EvtGen ttbb (4FS), mur=1/2*[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], dileptonic channel, hdamp=HT/2, H7.1-Default tune, decays with Powheg"
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'bbbar', 'jets']
evgenConfig.contact = ["lfaldaul@cern.ch"]
evgenConfig.generators += ['Powheg']

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 20
evgenConfig.tune = "H7.2-Default"

testSeq.TestHepMC.EffFailThreshold = 0.90

#--------------------------------------------------------------
# Shower lhe files with Herwig7 7.2.3
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile,me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.enable_angularShowerScaleVariations(True)

# add EvtGen
include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
