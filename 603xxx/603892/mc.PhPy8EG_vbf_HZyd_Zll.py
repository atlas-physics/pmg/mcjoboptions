#---------------------------------------------------------------
# LHE files of VBF used as inputs, DSID 345916 
# POWHEG+Pythia8 VBF H, H->Z+yd, mH=125GeV, m_yd=0, Z->l+l-
#---------------------------------------------------------------
# input dataset
# mc15_13TeV.345916.Powheg_NNPDF30_VBFH125_LHE.evgen.TXT.e6901

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 4

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# H->Z+yd decay, Z->l+l- 
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 23 4900022', # H->Z+yd
                            '25:onIfMatch = 23 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            '23:onMode = off',  # Z decay off
                            '23:onIfAny = 11 13 15' # Z decay to charged leptons
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py') 
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF H production: H->Z+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'VBFHiggs' , 'Z' , 'darkPhoton' ]
evgenConfig.contact     = [ 'rachid.mazini@cern.ch' ]
evgenConfig.process = "VBF->H, H->Z+yd, Z->l+l-"
