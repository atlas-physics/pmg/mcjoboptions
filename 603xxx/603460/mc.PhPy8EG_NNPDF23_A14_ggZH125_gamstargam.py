#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

evgenConfig.process     = "ggZH H->gam*gam, gam*->ll"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggZH H->gam*gam, gam*->ll mh=125 GeV CPS"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2lepton", "photon", "mH125" ]
evgenConfig.contact     = [ 'danning.liu@cern.ch' ]
evgenConfig.inputFilesPerJob = 10
evgenConfig.nEventsPerJob    = 20000


#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22',
                             'TimeShower:mMaxGamma = 90',         #Increase maximum allowed gamma* mass -n.b no interferece between gamma * and Z)
                             'TimeShower:nGammaToQuark = 0',     #Turn off gamma* to quarks
                             'TimeShower:nGammaToLepton = 2']    #Turn off gamma* to taus

# Repeat time showers in Pythia8 to improve efficiency
from Pythia8_i.Pythia8_iConf import HllgamRepeatTimeShower
hllgamRepeatTimeShower = HllgamRepeatTimeShower( name = "HllgamRepeatTimeShower" ) 
ToolSvc += hllgamRepeatTimeShower
genSeq.Pythia8.CustomInterface = hllgamRepeatTimeShower
