#---------------------------------------------------------------
# LHE files of qqZH used as inputs 
# POWHEG+Pythia8 qqZH(Zinclusive), H->Z+yd, Z->inclusive, mH=125GeV, m_yd=0
# Using lepton filter for ll+yd final state
#---------------------------------------------------------------

# input LHE dataset:
# mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 92

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']

#--------------------------------------------------------------
# H->Z+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 23 4900022', # H->Z+yd
                            '25:onIfMatch = 23 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py') 
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 qqZH production: H->Z+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'ZHiggs' , 'Z' , 'darkPhoton' ]
evgenConfig.contact     = [ 'rachid.mazini@cern.ch' ]
evgenConfig.process = "qq->ZH, H->Z+yd"
