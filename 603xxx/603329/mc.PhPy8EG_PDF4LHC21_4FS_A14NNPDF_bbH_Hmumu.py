#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603199.PhPy8EG_PDF4LHC21_4FS_A14NNPDF_bbH_HZZ_batch2_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125", "bbHiggs" ]
evgenConfig.process     = "bbH H->mumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production via bbH with MiNLO and A14 tune, H->mumu"
evgenConfig.inputFilesPerJob = 10
evgenConfig.nEventsPerJob    = 100000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact          = [ 'xinmeng.ye@cern.ch', 'michiel.jan.veen@cern.ch' ]

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3'  ]


#--------------------------------------------------------------
# H->mumu decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']
