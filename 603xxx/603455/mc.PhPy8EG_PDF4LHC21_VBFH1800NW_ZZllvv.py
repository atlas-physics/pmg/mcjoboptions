#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 1800
PowhegConfig.width_H = 0.00407

#PDF4LHC21 PDF variations (keeping also variation for PDF4LHC15 correlation studies and NNPDF3.0 for correlation with bkg samples if needed)
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]                                        
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0] 

# width settings for Higgs 
PowhegConfig.higgsfixedwidth = 0
PowhegConfig.complexpolescheme = 0 # not CPS

# Increase number of events requested to compensate for filter efficiency
# ZZ->llvv is 44% of ZZ decays (not counting Z->qq) (with 1.15x safety factor)
PowhegConfig.nEvents *= 1.15/0.44

PowhegConfig.generate()

#--------------------------------------------------------------                                           
# Pythia8 showering with the A14 NNPDF2.3 tune                                                            
#--------------------------------------------------------------                                           
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                               
include("Pythia8_i/Pythia8_Powheg_Main31.py")


genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']


#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',#decay of Z
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 12 13 14 15 16' ]

#--------------------------------------------------------------
### Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']



#--------------------------------------------------------------
# ZZ->llvv filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended

filtSeq += XtoVVDecayFilterExtended()
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 23
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13,15]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [12,14,16]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ZZ->llvv mh=1800 GeV NWA"
evgenConfig.keywords    = [ "Higgs", "BSMHiggs", "ZZ" ]
evgenConfig.contact     = [ 'tairan.xu@cern.ch','michiel.jan.veen@cern.ch' ]

