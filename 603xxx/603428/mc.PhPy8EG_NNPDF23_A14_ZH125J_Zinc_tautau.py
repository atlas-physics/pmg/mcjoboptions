#--------------------------------------------------------------
# POWHEG+Pythia8 qq->HZ-> Zinc + Htautau production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8, H+Z+jet, Z->all, H->tautau"
evgenConfig.process     = "qq->ZH, Z->all, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'huanguo.li@cern.ch' ]
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob = 5000

