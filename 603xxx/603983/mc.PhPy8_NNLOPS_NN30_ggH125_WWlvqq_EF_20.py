#--------------------------------------------------------------

# EVGEN configuration

#--------------------------------------------------------------

evgenConfig.process     = "gg->H->WW*->lvlv"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->WW->lvlv"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.contact     = [ 'kathrin.becker@cern.ch' ]
#evgenConfig.minevents   = 10
#evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputfilecheck = "events" # This is in the very specific case where we input a list of TXT files seperated with commas in --inputGeneratorFile
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 80000

#--------------------------------------------------------------

# Pythia8 showering

# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py

#--------------------------------------------------------------

include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------

# Pythia8 main31 update

#--------------------------------------------------------------

#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#replaced above line based on example here: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/421xxx/421121/mc.PhPy8EG_A14_ttbar_hdamp172p5_nonallhad_v2.py#L31-36

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------

# Higgs at Pythia8

#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:m0 = 125',        # Higgs mass (just in case)
                             '25:mWidth = 0.00407',# Higgs width (just in case)
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16' ]

#include("GeneratorFilters/XtoVVDecayFilterExtended.py")

# replaced the above based on JIRA ticket: https://its.cern.ch/jira/browse/ATLMCPROD-10955

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended   

filtSeq += XtoVVDecayFilterExtended("XtoVVDecayFilterExtended")

filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25               
filtSeq.XtoVVDecayFilterExtended.PDGParent = 24                     
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22                 
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]   
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]
