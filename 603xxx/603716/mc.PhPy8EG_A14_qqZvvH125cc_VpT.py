#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 ZH+jet->vvbarccar with VpT enhancement showering"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch', 'stephen.jiggins@cern.ch', 'miha.muskinja@cern.ch' ]
evgenConfig.generators   = ['Powheg','Pythia8']
evgenConfig.process = "qqZH, H->cc, Z->vv"
evgenConfig.inputFilesPerJob = 20
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs->ccar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 4 4' ]
