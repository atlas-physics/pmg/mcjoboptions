
#------------------------------------------------------------------
# EVGEN configuration
# Note:  This JO are designed to run Powheg + showering in one shot 
#------------------------------------------------------------------

evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+Z+jet MiNLO, Z->all, H->gamgam mH=70 GeV"
evgenConfig.keywords    = [ "Higgs", "ZHiggs", "diphoton" ]
evgenConfig.contact     = [ 'leonardo.carminati@cern.ch']
evgenConfig.process     = "qq->ZH, H->gamgam, Z->all"
evgenConfig.generators  = ['Powheg','Pythia8']

evgenConfig.nEventsPerJob = 100

#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> inv + l+l- production
#--------------------------------------------------------------

include("PowhegControl/PowhegControl_HZj_Common.py")

PowhegConfig.mass_H  = 70.
PowhegConfig.width_H = 0.00407

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode = "z > all" # Z->all
PowhegConfig.mass_Z_low = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.PDF = list(range(93300,93343)) + [27100] +  [14400] + [331700]

# scale variations: first pair is the nominal setting

PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] 
PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8 (H->yy, dipole recoil, removing Dalitz decays)
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',     # decay of Higgs
                             '25:onIfMatch = 22 22' ]

genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]

