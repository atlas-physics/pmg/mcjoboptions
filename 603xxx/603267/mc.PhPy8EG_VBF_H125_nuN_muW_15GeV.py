#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
        genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

if hasattr(testSeq, "TestHepMC"):
  testSeq.TestHepMC.MaxTransVtxDisp = 200000 #in mm
  testSeq.TestHepMC.MaxVtxDisp = 500000 #in mm

#--------------------------------------------------------------
# H->vl + v4 decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [

                            '99904:new = N2 N2 2 0 0 15.0 0.0 0.0 0.0 0.1 0 1 0 1 0', # define new particle 40 GeV & 0.1 mm
                            '99904:mayDecay = on',
                            '99904:isResonance = false',
                            
                             '99904:addChannel = 1 0.16 23 13 -11 12',
                             '99904:addChannel = 1 0.16 23 13 -13 14',
                             '99904:addChannel = 1 0.16 23 13 -15 16',
                             '99904:addChannel = 1 0.16 23 -13 11 -12',
                             '99904:addChannel = 1 0.16 23 -13 13 -14',
                             '99904:addChannel = 1 0.16 23 -13 15 -16',
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1.0 103 14 99904', # H -> nu_mu N
                            '25:onIfMatch = 14 99904',
                            'ParticleDecays:limitTau0 = off', # switch off decaying lifetime limits
                            'ParticleDecays:tau0Max = 600.0'
                            ]
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

evgenConfig.description = "POWHEG+Pythia8 VBF production: H->nuN, 15 GeV"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs']
evgenConfig.process = "pp->Hjj, H->nu+N"

evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ['mohamed.belfkir@cern.ch', 'mai.el.sawy@cern.ch']