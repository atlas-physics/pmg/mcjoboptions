#--------------------------------------------------------------
# POWHEG H+W+jet production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.decay_mode = "w- > l- vl~"


PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + [260000] + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.define_event_weight_group( group_name='kappas_var', parameters_to_vary=['kappa_ghw','kappa_ght', 'kappa_ghb'] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.1, khb.1', parameter_values=[0.5,1.,1.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.0, khb.1', parameter_values=[1.,0.,1.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.1, khb.0', parameter_values=[1.,1.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.0, khb.0', parameter_values=[1.,0.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.1, khb.0', parameter_values=[0.5,1.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.0, khb.1', parameter_values=[0.5,0.,1.] )
PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG W(-)(->l(-)v)H+jet production with VpT bias"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs"]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators   = ['Powheg']
evgenConfig.process = "WmH, W->lv"
evgenConfig.nEventsPerJob = 500
