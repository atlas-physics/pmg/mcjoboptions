#---------------------------------------------------------------
# LHE files of ggH used as inputs 
# POWHEG+Pythia8 ggH, H->Z+yd, mH=125GeV, m_yd=0, Z->l+l-
#---------------------------------------------------------------

# Input dataset
# mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 10

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# H->Z+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 23 4900022', # H->Z+yd
                            '25:onIfMatch = 23 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                            '23:onMode = off',  # Z decay off
                            '23:onIfAny = 11 13 15' # Z decay to charged leptons
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py') 
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ggH production: H->Z+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'gluonFusionHiggs' , 'Z' , 'darkPhoton' ]
evgenConfig.contact     = [ 'Rachid.Mazini@cern.ch' ]
evgenConfig.process = "gg->H, H->Z+yd, Z->l+l-"
