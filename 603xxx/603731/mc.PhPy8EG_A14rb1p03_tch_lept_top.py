#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = 'POWHEG+Pythia8+EvtGen single-top-quark t-channel (2->3) production (top),MadSpin, A14 tune with rb=1.03, ME NNPDF3.04f NLO, A14 NNPDF23 LO'
evgenConfig.keywords      = [ 'SM', 'top', 'lepton' ]
evgenConfig.contact       = [ 'marcus.de.beurs@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 20 # H.A. optional? to be adjusted.

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py') # H.A. updated for JO in 21.6, before -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
include("Pythia8_i/Pythia8_Powheg_Main31.py") # H.A. updated for JO in 21.6, before -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# A14rb tune
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.03' ]
