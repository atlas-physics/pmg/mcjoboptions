# original JO: 602059
evgenConfig.description = 'POWHEG+Pythia8+EvtGen tW production (top), DR scheme, dynamic scale, inclusive, hdamp equal 1.5*top mass and m(top)=176.00 GeV, A14 tune with rb=1.05, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt' ]
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch', 'jens.roggel@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8', 'EvtGen' ]

#--------------------------------------------------------------
# Powheg tW setup - V2
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_Wt_DR_modified_Common.py")

PowhegConfig.decay_mode_top = 't > all'
PowhegConfig.decay_mode_W = 'w > all'
PowhegConfig.mass_t  = 176.00
PowhegConfig.width_t = 1.415
PowhegConfig.hdamp   = 1.5*176.00                                        # 1.5 * mtop        


# q: how do I generate tW with positive tops?   
# a: change the decay mode of the top quark to t > b l+ vl

PowhegConfig.nEvents     *= 1.1     # Add safety factor
PowhegConfig.fulloffshell=1
PowhegConfig.numwidth_W=80.4
PowhegConfig.mass_W_low=40.
PowhegConfig.runningscales=1
PowhegConfig.btlscalect=1
PowhegConfig.btlscalereal=1
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# A14rb tune
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]

