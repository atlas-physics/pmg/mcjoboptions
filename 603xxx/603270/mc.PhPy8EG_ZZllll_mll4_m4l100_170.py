#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->llll production with PDF4LHC21 PDF variations and A14 tune and mllmin4 and m4l=[100,170] GeV'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '4lepton' ]
evgenConfig.contact     = [ 'chiara.arcangeletti@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.nEventsPerJob = 1000
#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = "z z > l+ l- l'+ l'-"
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 30.
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

include("GeneratorFilters/FourLeptonInvMassFilter.py")
filtSeq.FourLeptonInvMassFilter.MinPt = 3.*GeV
filtSeq.FourLeptonInvMassFilter.MaxEta = 5.
filtSeq.FourLeptonInvMassFilter.MinMass = 100.*GeV
filtSeq.FourLeptonInvMassFilter.MaxMass = 170.*GeV
