evgenConfig.process          = "qq->WpH, W->all, H->tautau"
evgenConfig.description      = "POWHEG+PYTHIA8, H+Wp+jet, W->all, H->tautau"
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2tau", "mH125" ]
evgenConfig.contact          = [ 'antonio.de.maria@cern.ch' ]
evgenConfig.inputFilesPerJob = 10
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]


