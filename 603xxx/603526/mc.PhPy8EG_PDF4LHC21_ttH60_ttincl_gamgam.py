
#------------------------------------------------------------------
# EVGEN configuration
# Note:  This JO are designed to run Powheg + showering in one shot 
#------------------------------------------------------------------

evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ttH NLO tt~->all, H->gamgam mh=60 GeV"
evgenConfig.keywords    = [ "Higgs", "top", "diphoton" ]
evgenConfig.contact     = [ 'leonardo.carminati@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]

evgenConfig.nEventsPerJob = 20000

#--------------------------------------------------------------
# Powheg ttH setup starting from ATLAS defaults
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_ttH_Common.py')

PowhegConfig.decay_mode   = "t t~ > all" 
PowhegConfig.hdamp        = 352.5

PowhegConfig.mass_H = 60.0     
PowhegConfig.width_H = 0.00407  

PowhegConfig.runningscales = 1 ## dynamic scale
#PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.PDF = list(range(93300,93343)) + [27100] +  [14400] + [331700]	
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Higgs at Pythia8 (H->yy, dipole recoil, removing Dalitz decays)
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',     # decay of Higgs
                             '25:onIfMatch = 22 22' ]

genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
