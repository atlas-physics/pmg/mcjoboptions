#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
  infile = f
  f1 = open( infile )
  newfile = infile+'.temp'
  f2 = open(newfile,'w')
  counter = 0
  for line in f1:
      if line.startswith('      25     1'):
          f2.write(line.replace('      25     1','      35     1'))
          counter += 1
      else:
          f2.write(line)
  print ('Found %d events in LHE, replaced 25 with 35' % counter)
  f1.close()
  f2.close()
  os.system('mv %s %s '%(infile, infile+'.old') )
  os.system('mv %s %s '%(newfile, infile) )

# JO listing: PhPy8EG_ggH125_n1n2_n1a_mm_2_1_63_2LMET75.py
# JO listing: PhPy8EG_VBFH125_n1n2_n1a_tt_2_1_63_2LMET75.py

def getParameters():
  runconf=jofile.rstrip(".py").split("_")
  process_string=runconf[1][:-3]
  fs_string=runconf[4]
  A_Mass=runconf[5].replace("p",".")
  N1_Mass=runconf[6].replace("p",".")
  N2_Mass=runconf[7].replace("p",".")
  filteroptions_string=runconf[8]

  return process_string, fs_string, A_Mass, N1_Mass, N2_Mass, filteroptions_string


process, decay, ma, mn1, mn2, filteroptions = getParameters()

if decay == 'mm':
    # Muons 
    axino_decay = '13 -13'
elif decay == 'tt':
    # Taus
    axino_decay = '15 -15'
elif decay == 'bb':
    # B quarks
    axino_decay = '5 -5'
else:
    # Not supported
    raise RuntimeError("Invalid final state string, exiting!") 


print ("-----------------------------------------------")
print ("Parsed JobOption configuration:")
print ("DECAY STRING = " + str(decay))
print ("A MASS = " + str(ma))
print ("N1 MASS = " + str(mn1))
print ("N2 MASS = " + str(mn2))
print ("PYTHIA DECAY MODE = " + axino_decay)
print ("FILTER OPTIONS = " + filteroptions)
print ("-----------------------------------------------")


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if process=="ggH":
  genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']
elif process=="VBFH":
  genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
  genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
H_Mass = 125.0
H_Width = 0.00407
A_Width = ( float(ma) / 100. ) * 0.001 #1 MeV width for 100 GeV a
A_MassMin = float(ma) - 100*float(A_Width)
A_MassMax = float(ma) + 100*float(A_Width)
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',
                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:oneChannel = 1 1 100 1000023 1000022',
                             '1000023:onMode = off',
                             '1000023:oneChannel = 1 1 100 36 1000022',
                             '1000023:m0 = '+str(mn2),
                             '1000023:tau0 0',  
                             '1000022:onMode = off',
                             '1000022:m0 = '+str(mn1),
                             '1000022:tau0 0', 
                             '36:onMode = off', # decay of the a
                             '36:onIfAny = '+axino_decay, # decay a->xx
                             '36:m0 = '+str(ma), #scalar mass 
                             '36:mWidth = '+str(A_Width), # narrow width
                             '36:mMin = '+str(A_MassMin), # narrow width
                             '36:mMax = '+str(A_MassMax), # narrow width
                             '36:tau0 0', #lifetime 
                          ]
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
filters=[]

if '2L' in filteroptions:
    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")
    ElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    ElecMuTauFilter.MinPt = 2000.
    ElecMuTauFilter.MaxEta = 2.8
    ElecMuTauFilter.NLeptons = 2
    ElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
    filters.append("MultiElecMuTauFilter")


# MET filter, interpreted from jobOptions
if 'MET' in filteroptions:
    filtSeq += MissingEtFilter("MissingEtFilter")
    filtSeq += MissingEtFilter("MissingEtFilterUpperCut")

    m = re.search('MET(\d+)', filteroptions)
    lowercut = int(m.group(1))
    print ("Met lower cut = " + str(lowercut))
    print ("Met upper cut = " + str(100000))

    filtSeq.MissingEtFilter.METCut          = lowercut*GeV
    filtSeq.MissingEtFilterUpperCut.METCut  = 100000*GeV
    filters.append("MissingEtFilter and not MissingEtFilterUpperCut")


for i in filters:
    if filtSeq.Expression == "":
        filtSeq.Expression=i
    else:
        filtSeq.Expression = "(%s) and (%s)" % (filtSeq.Expression,i)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
if process=="VBFH":
  evgenConfig.description = "POWHEG+Pythia8 VBF H125 production, H->N1N2 ; N2->N1a ; a->%s, ma(%s), mN2(%s), mN1(%s)" % ( decay, ma, mn2, mn1 )
  evgenConfig.process     = "VBF H->N1N2->N1N1a->%s + MET" % (decay)
elif process=="ggH":
  evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->N1N2 ; N2->N1a ; a->%s, ma(%s), mN2(%s), mN1(%s)" % ( decay, ma, mn2, mn1 )
  evgenConfig.process     = "ggH H->N1N2->N1N1a->%s + MET" % (decay)

evgenConfig.keywords    = [ "BSM", "Higgs", "MSSM", "mH125" ]
evgenConfig.contact     = [ 'christopher.hayes@cern.ch' , 'ben.carlson@cern.ch']

evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob = 5000
