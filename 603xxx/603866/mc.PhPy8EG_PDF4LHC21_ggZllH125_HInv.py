#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 gg->Z+H->l+l-Inv production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia showering with A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs->ZZ->4v at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14',
                             '23:onIfMatch = 16 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603984.Ph_PDF4LHC21_ggZllH125_HZZ_batch4_LHE.evgen.TXT.e8557
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->Z+H->l+l-Inv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'eesha.lodhi@cern.ch' ]
evgenConfig.inputFilesPerJob = 9
evgenConfig.nEventsPerJob    = 90000
evgenConfig.process = "gg->ZH, Z->ll, H->Inv"
#evgenConfig.inputfilecheck = "TXT"
