#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with MadSpin decay, Powheg hdamp equal 1.5*top mass, A14 tune and single lepton filter.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.nEventsPerJob = 10000

import os, sys, glob

include('PowhegControl/PowhegControl_tt_Common.py')

# Initial settings
PowhegConfig.decay_mode = "t t~ > undecayed"

PowhegConfig.hdamp        = 258.75
#PowhegConfig.mu_F         = 1.0
#PowhegConfig.mu_R         = 1.0
#PowhegConfig.PDF          = 260000

PowhegConfig.MadSpin_taus_are_leptons = True
PowhegConfig.MadSpin_decays= ["decay t > w+ B, w+ > all all", "decay t~ > w- B~, w- > all all"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_mode = "full"
PowhegConfig.MadSpin_model = "/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/sm_specialb"
PowhegConfig.MadSpin_nFlavours = 5
os.system('get_files -jo param_card_sm_specialb.dat')
PowhegConfig.MadSpin_paramcard = "./param_card_sm_specialb.dat"

# new line added to match MC@NLO config
PowhegConfig.bwcutoff = 50

PowhegConfig.nEvents     *= 10 # safety factor
PowhegConfig.generate()

for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith(' 100000005'):
            f2.write(line.replace(' 100000005','        5'))
        elif line.startswith(' -100000005'):
            f2.write(line.replace(' -100000005','       -5'))
        else:
            f2.write(line)

    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


