#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->bb"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [ 'matthew.henry.klein@cern.ch' ]
evgenConfig.nEventsPerJob    = 2000
evgenConfig.inputFilesPerJob = 100

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 -5' ]

################################################################
# VBF Filtering of truth particles
################################################################
# Set up VBF filters
include("GeneratorFilters/FindJets.py")
include ("GeneratorFilters/VBFForwardJetsFilter.py")
CreateJets(prefiltSeq, 0.4) ## need to add "WZ" if want WZ jets, the 0.4 is radius Generators/GeneratorFilters/share/common/FindJets.py
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass

filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.JetMinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta=5.0
filtSeq.VBFForwardJetsFilter.NJets=2
filtSeq.VBFForwardJetsFilter.Jet1MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta=5.0
filtSeq.VBFForwardJetsFilter.Jet2MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta=5.0
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

# medium cut:
filtSeq.VBFForwardJetsFilter.MassJJ = 800.*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 3.5
