#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

# In 20.7.9.9.6, LHE merging means this is no longer needed
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["b"]   = 5
dict_pdgIds["mu"]  = 13
dict_pdgIds["tau"] = 15
dict_pdgIds["g"]   = 21
dict_pdgIds["y"]   = 22


def decaywidth(cAA=1, MALP=1):
    import math

    sw2 = 0.23
    cBB = cAA * (1 - sw2)
    cWW = cAA * sw2
    aEWM1 = 127.9
    aEW = 1 / aEWM1
    ee = 2 * math.sqrt(aEW) * math.sqrt(math.pi)
    Lambda = 1000

    return (pow(MALP,2)*((8*cBB*cBB*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (16*cBB*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2) + (8*cWW*cWW*pow(ee,4)*pow(MALP,4))/pow(Lambda,2)))/(32.*math.pi*pow(MALP,3))


def lifetime(width=1):
    return 3.0E11 * 6.582E-16 / float(width) / 1E9  # mm
    

def getParameters():
    import re

    #--- Read parts of the job option
    jonamelist = jofile.rstrip(".py").split("_")
    process = jonamelist[1]
    ma = float(jonamelist[3].split("a")[-1].replace("p", "."))
    decayChan = str(jonamelist[4])
    partFilter = str(jonamelist[5])
    Cdecay = None
    for param in jonamelist[5:]:
        if param.startswith('filter'):
            partFilter = str(param)

    #--- list of decays, e.g. [mu, tau] for 2mu2tau
    decayProducts = []
    for part in dict_pdgIds.keys():
        decay = re.findall("[1-4]%s" % part, decayChan)
        if len(decay)>0:
            decayProducts.append(decay[0][1:]) # remove the number in front of the letter
    process = re.sub(r'\d+', '', process)

    return process, ma, decayChan, decayProducts, Cdecay, partFilter

#    MC15.999999.PowhegPy8EG_ggH_H125_a60a60_2mu2tau_[filtXXX].py
process, ma, decayChan, decayProducts, Cdecay, partFilter = getParameters()
print("Parameters: ")
print(process, ma, decayChan, decayProducts, Cdecay, partFilter)

if Cdecay is None:
    Atau0 = 0
else:
    Awidth0 = decaywidth(Cdecay, ma)
    Atau0 = lifetime(Awidth0)

print("Parameters: ")
print(process, ma, Atau0, decayChan, decayProducts, partFilter)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg_Main31.py")
print(dir(genSeq.Pythia8))
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


#--------------------------------------------------------------
# Higgs->aa at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            '35:onMode = off',
                            '35:onIfMatch = 36 36', # h->aa
                            '36:onMode = off',
                            ]

#--------------------------------------------------------------
# a->XX at Pythia8
#--------------------------------------------------------------

if len(decayProducts)==1: # a->4X
    genSeq.Pythia8.Commands += [
                                '36:onIfAny = %d' % dict_pdgIds[decayProducts[0]], # decay a->XX
                                '36:m0 = %.2f' % ma, #scalar mass
                                '36:mMin = 0',
                                '36:tau0 = %.6f' % Atau0,
                                ]
    if Atau0 > 0:
        testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
        testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->aa->4tau mh=125 GeV"
evgenConfig.process     = "ggH H->aa->4tau"
evgenConfig.contact     = [ 'rachel.smith@cern.ch', 'huacheng.cai@cern.ch', 'roger.caminal.armadans@cern.ch' ]
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 5000

#--------------------------------------------------------------
# FILTERS 
#--------------------------------------------------------------
#if partFilter=="filter2taulep2tauhad":
# from GeneratorFilters.GeneratorFiltersConf import FourTauLepLepHadHadFilter
# filtSeq += FourTauLepLepHadHadFilter()
from GeneratorFilters.GeneratorFiltersConf import TauFilter
lfvfilter = TauFilter("lfvfilter")
filtSeq += lfvfilter
filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 4
filtSeq.lfvfilter.Nleptaus = 2
filtSeq.lfvfilter.Nhadtaus = 2
filtSeq.lfvfilter.EtaMaxlep = 2.7
filtSeq.lfvfilter.EtaMaxhad = 2.7
filtSeq.lfvfilter.Ptcutlep = 3000.0 #MeV
filtSeq.lfvfilter.Ptcutlep_lead = 3000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 12500.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 12500.0 #MeV
