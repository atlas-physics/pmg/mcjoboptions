
#------------------------------------------------------------------
# EVGEN configuration
# Note:  This JO are designed to run Powheg + showering in one shot 
#------------------------------------------------------------------

evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH MINLO, H->gamgam mh=110 GeV"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'leonardo.carminati@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]

evgenConfig.nEventsPerJob    = 5000

#--------------------------------------------------------------
# Powheg ggF setup starting from ATLAS defaults
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_Hj_Common.py')

PowhegConfig.mass_H = 110.0
PowhegConfig.width_H = 0.00407

#PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.PDF = list(range(93300,93343)) + [27100] +  [14400] + [331700]	
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]

PowhegConfig.nEvents = evgenConfig.nEventsPerJob*1.1 if evgenConfig.nEventsPerJob>0 else  runArgs.maxEvents

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8 (H->yy, dipole recoil, removing Dalitz decays)
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ '25:onMode = off',     # decay of Higgs
                             '25:onIfMatch = 22 22' ]

genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]

