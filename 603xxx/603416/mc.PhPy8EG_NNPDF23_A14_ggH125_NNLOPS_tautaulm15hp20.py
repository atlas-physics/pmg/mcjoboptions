#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H->tautau->l-h+ production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = off', # decay of taus
                             '15:onPosIfAny = 11 13', # particle
                             '15:onNegIfAny = 111 130 211 221 223 310 311 321 323' ] # antiparticle

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8 ggF H->tautau"
evgenConfig.process     = "gg->H->tautau->l-h+"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'huanguo.li@cern.ch' ]
evgenConfig.inputFilesPerJob = 60
evgenConfig.nEventsPerJob = 5000

#--------------------------------------------------------------
# Set up tau filters
#--------------------------------------------------------------
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep15had20filter = TauFilter("lep15had20filter")
  filtSeq += lep15had20filter

filtSeq.lep15had20filter.UseNewOptions = True
filtSeq.lep15had20filter.Ntaus = 2
filtSeq.lep15had20filter.Nleptaus = 1
filtSeq.lep15had20filter.Nhadtaus = 1
filtSeq.lep15had20filter.EtaMaxlep = 2.6
filtSeq.lep15had20filter.EtaMaxhad = 2.6
filtSeq.lep15had20filter.Ptcutlep = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcutlep_lead = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad_lead = 20000.0 #MeV
filtSeq.lep15had20filter.filterEventNumber = 1 # keep odd EventNumber events

