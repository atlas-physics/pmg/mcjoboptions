evgenConfig.description = "3L filter for muQ4 variation of Sherpa ttll+0,1j@NLO with Sherpa 2.2.14, EW correction weights"
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.nEventsPerJob = 50000
evgenConfig.inputFilesPerJob = 17

if runArgs.trfSubstepName == 'generate':
   print("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
   filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
   filtSeq.ElecMuTauFilter.NLeptons = 3
   filtSeq.ElecMuTauFilter.IncludeHadTaus = True
   filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
   filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
   filtSeq.ElecMuTauFilter.MaxEta = 2.8

   if not hasattr(filtSeq, "FourLeptonFilter"):
      from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
      OneLeptonFilter = MultiElecMuTauFilter(name="FourLeptonFilter")

   filtSeq += MultiElecMuTauFilter("FourLeptonFilter")
   filtSeq.FourLeptonFilter.NLeptons = 4
   filtSeq.FourLeptonFilter.IncludeHadTaus = True
   filtSeq.FourLeptonFilter.MinVisPtHadTau = 7000.
   filtSeq.FourLeptonFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
   filtSeq.FourLeptonFilter.MaxEta = 2.8

   filtSeq.Expression = "(not FourLeptonFilter) and (ElecMuTauFilter)"

   postSeq.CountHepMC.CorrectRunNumber = True
