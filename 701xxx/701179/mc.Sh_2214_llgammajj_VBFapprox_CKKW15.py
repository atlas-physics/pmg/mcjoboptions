include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Electroweak llgammajj+1j@LO with a t-channel requirement to suppress triboson final states."
evgenConfig.keywords = ["SM", "diboson", "lepton", "photon", "VBS"]
evgenConfig.contact  = ["atlas-generators-sherpa@cern.ch", "chris.g@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  MASSIVE[5]=1; 

  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  % simplified setup as long as only 2->6 taken into account:
  % SCALES=VAR{FSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSCF*Abs2(p[2]+p[3]+p[4]+p[5])};

  %tags for process setup
  NJET:=1; QCUT:=15.;

  EXCLUSIVE_CLUSTER_MODE=1;
  SOFT_SPIN_CORRELATIONS=1

  ME_QED_CLUSTERING_THRESHOLD=5;

  % improve colour-flow treatment
  CSS_CSMODE=1
  
  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;

  KFACTOR=VAR{132.17/137.03599976}
}(run)

(processes){
  Process 93 93 -> 22 90 90 93 93 93{NJET};
  Order (*,5); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1;
  End process;
}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 7

