evgenConfig.description = "1L Filter for Sherpa ttW+0,1j@NLO+2j@LO with Sherpa 2.2.14, EW correction weights"
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch" ]
evgenConfig.nEventsPerJob = 20000
evgenConfig.inputFilesPerJob = 31

if runArgs.trfSubstepName == 'generate' :
   print("ERROR: These JO require an input file.  Please use the --afterburn option")

if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]

   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False

   ## Disable TestHepMC for the time being, cf.
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   include ("GeneratorFilters/CreatexAODSlimContainers.py")
   createxAODSlimmedContainer("TruthGen",prefiltSeq)
   prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

   from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
   filtSeq += xAODMultiElecMuTauFilter("ElecMuTauFilter")
   filtSeq.ElecMuTauFilter.NLeptons = 2
   filtSeq.ElecMuTauFilter.IncludeHadTaus = True
   filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
   filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV
   filtSeq.ElecMuTauFilter.MaxEta = 2.8

   if not hasattr(filtSeq, "OneLeptonFilter"):
      from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
      OneLeptonFilter = xAODMultiElecMuTauFilter(name="OneLeptonFilter")

   filtSeq += xAODMultiElecMuTauFilter("OneLeptonFilter")
   filtSeq.OneLeptonFilter.NLeptons = 1
   filtSeq.OneLeptonFilter.IncludeHadTaus = True
   filtSeq.OneLeptonFilter.MinVisPtHadTau = 7000.
   filtSeq.OneLeptonFilter.MinPt = 7000. #Each lepton must be at least 7GeV
   filtSeq.OneLeptonFilter.MaxEta = 2.8

   filtSeq.Expression = "(not ElecMuTauFilter) and (OneLeptonFilter)"

   postSeq.CountHepMC.CorrectRunNumber = True
