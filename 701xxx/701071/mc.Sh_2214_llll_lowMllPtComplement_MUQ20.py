include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "4 charged leptons with 0j@NLO + 1,2j@LO and mll>2*ml+250MeV, (pTl1>20GeV OR MET>50GeV) AND (pTl2<5GeV OR ONE M(SFOS)<4GeV)."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){
  # scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=4.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # tags for process setup
  NJET:=2; LJET:=4; QCUT:=20.;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  # EW corrections setup
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  METS_BBAR_MODE=5

  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0
  SOFT_SPIN_CORRELATIONS=1

  EW_SCHEME=3
  GF=1.166397e-5
  ME_QED_CLUSTERING_THRESHOLD=5.

  PP_RS_SCALE VAR{0.25*sqr(PPerp(p[2])+PPerp(p[3])+PPerp(p[4])+PPerp(p[5]))};

  # NWF improvements
  NLO_CSS_PSMODE=1

  # settings for MAXMLLPT selector
  SHERPA_LDADD=SherpaMAXMLLPT
}(run)

(processes){
  Process 93 93 -> 90 90 90 90 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {4,5,6};
  End process;
}(processes)

(selector){
  "PT" 90 2.0,E_CMS:2.0,E_CMS [PT_UP]
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  MAXMLLPT 4.0
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=write_parameters=1 ew_renorm_scheme=1 ew_scheme=2" ]
genSeq.Sherpa_i.Parameters += ["OL_PREFIX=Process/OpenLoops"]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllll" ,"ppllll_ew", "pp4lj", "pp4lj_ew"]
genSeq.Sherpa_i.NCores = 20

genSeq.Sherpa_i.PluginCode=r"""
#include "ATOOLS/Org/CXXFLAGS_PACKAGES.H"
#define USING__FASTJET "1"
#define USING__FASTJET__3 "1"
#include "ATOOLS/Phys/Particle_List.H"
#include "ATOOLS/Phys/Fastjet_Helpers.H"
#include "PHASIC++/Selectors/Selector.H"

namespace PHASIC {
  class MAXMLLPT : public Selector_Base {
    double m_MLLmax2;

  public:
    MAXMLLPT(int nin, int nout,ATOOLS::Flavour * fl, double MLLmax);

    ~MAXMLLPT();


    bool   NoJetTrigger(const ATOOLS::Vec4D_Vector &);
    bool   Trigger(const ATOOLS::Vec4D_Vector &);
    bool   JetTrigger(const ATOOLS::Vec4D_Vector &,
		      ATOOLS::NLO_subevtlist *const subs);

    void   BuildCuts(Cut_Data *) {}
  };
}

#include "PHASIC++/Process/Process_Base.H"
#include "PHASIC++/Main/Process_Integrator.H"
#include "ATOOLS/Org/Run_Parameter.H"
#include "ATOOLS/Org/Message.H"
#include "ATOOLS/Org/Exception.H"
#include "ATOOLS/Org/MyStrStream.H"
#include "ATOOLS/Org/Data_Reader.H"


using namespace PHASIC;
using namespace ATOOLS;

/*---------------------------------------------------------------------

  General form - flavours etc are unknown, will operate on a Particle_List.

  --------------------------------------------------------------------- */

MAXMLLPT::MAXMLLPT(int nin, int nout,ATOOLS::Flavour * fl, double MLLmax2) :
  Selector_Base("MAXMLLPT"), m_MLLmax2(MLLmax2)
{

  m_fl         = fl;
  m_smin       = 0.;
  m_smax       = sqr(rpa->gen.Ecms());

  m_nin        = nin;
  m_nout       = nout;

  m_sel_log    = new Selector_Log(m_name);
}


MAXMLLPT::~MAXMLLPT() { }


bool MAXMLLPT::NoJetTrigger(const Vec4D_Vector &p)
{
  double s=(p[0]+p[1]).Abs2();
  return (s>m_smin*4.);
}

bool MAXMLLPT::Trigger(const Vec4D_Vector &p)
{
  // accept event if any SFOS pairs are below maximum Mll cut
  bool mll_trigger(false);
  Vec4D MET(0., 0., 0., 0.);
  std::vector<fastjet::PseudoJet> leps;
  for (size_t i(m_nin);i<p.size();++i) {
    if (Flavour(kf_lepton).Includes(m_fl[i])) {
      leps.push_back(MakePseudoJet(m_fl[i], p[i]));
      int pdg1 = m_fl[i];
      for (size_t j(i+1); j < p.size(); ++j) {
        int pdg2 = m_fl[j];
        if (abs(pdg1) == abs(pdg2) && pdg1 * pdg2 < 0.) {
          // same-flavour opposite-charge lepton pair
          double mll = (p[i] + p[j]).Abs2();
          if (mll < m_MLLmax2)  mll_trigger = true;
        }
      }
    }
    else if(Flavour(kf_neutrino).Includes(m_fl[i])) {
      fastjet::PseudoJet neutrino = MakePseudoJet(m_fl[i], p[i]);
      MET += Vec4D(neutrino.E(), neutrino.px(), neutrino.py(), neutrino.pz());
    }
  }

  bool pt1_trigger(false), pt2_trigger(false), met_trigger(false);
  Vec4D p1(leps[0].E(),leps[0].px(),leps[0].py(),leps[0].pz());
  Vec4D p2(leps[1].E(),leps[1].px(),leps[1].py(),leps[1].pz());
  if (p1.PPerp()  > 20.)  pt1_trigger = true;
  if (p2.PPerp()  <  5.)  pt2_trigger = true;
  if (MET.PPerp() > 50.)  met_trigger = true;

  bool trigger = (mll_trigger || pt2_trigger) && (pt1_trigger || met_trigger);


  return (1-m_sel_log->Hit(1-trigger));
}

bool MAXMLLPT::JetTrigger(const Vec4D_Vector &p,
                            ATOOLS::NLO_subevtlist *const subs)
{

  // accept event if any SFOS pairs are below maximum Mll cut
  bool mll_trigger(false);
  Vec4D MET(0., 0., 0., 0.);
  std::vector<fastjet::PseudoJet> leps;
  for (size_t i(m_nin); i < subs->back()->m_n; ++i) {
    if(Flavour(kf_lepton).Includes(subs->back()->p_fl[i])) {
      leps.push_back(MakePseudoJet(subs->back()->p_fl[i], p[i]));
      int pdg1 = subs->back()->p_fl[i];
      for (size_t j(i+1); j < subs->back()->m_n; ++j) {
        int pdg2 = subs->back()->p_fl[j];
        if (abs(pdg1) == abs(pdg2) && pdg1 * pdg2 < 0.) {
          // same-flavour opposite-charge lepton pair
          double mll = (p[i] + p[j]).Abs2();
          if (mll < m_MLLmax2) mll_trigger = true;
        }
      }
    }
    else if(Flavour(kf_neutrino).Includes(subs->back()->p_fl[i])) {
      fastjet::PseudoJet neutrino = MakePseudoJet(subs->back()->p_fl[i], p[i]);
      MET +=  Vec4D(neutrino.E(), neutrino.px(), neutrino.py(), neutrino.pz());
    }
  }

  bool pt1_trigger(false), pt2_trigger(false), met_trigger(false);
  Vec4D p1(leps[0].E(),leps[0].px(),leps[0].py(),leps[0].pz());
  Vec4D p2(leps[1].E(),leps[1].px(),leps[1].py(),leps[1].pz());
  if (p1.PPerp()  > 20.)  pt1_trigger = true;
  if (p2.PPerp()  <  5.)  pt2_trigger = true;
  if (MET.PPerp() > 50.)  met_trigger = true;

  bool trigger = (mll_trigger || pt2_trigger) && (pt1_trigger || met_trigger);

  return (1-m_sel_log->Hit(1-trigger));
}


DECLARE_ND_GETTER(MAXMLLPT,"MAXMLLPT",Selector_Base,Selector_Key,true);

Selector_Base *ATOOLS::Getter<Selector_Base,Selector_Key,MAXMLLPT>::
operator()(const Selector_Key &key) const
{
  if (key.empty() || key.front().size()<1) THROW(critical_error,"Invalid syntax");

  double maxMll2 = ToType<double>(key.p_read->Interpreter()->Interprete(key[0][0]));
  maxMll2 *= maxMll2;

  MAXMLLPT *jf(new MAXMLLPT(key.p_proc->NIn(),key.p_proc->NOut(),
                                (Flavour*)&key.p_proc->Process()->Flavours().front(), maxMll2));
  jf->SetProcess(key.p_proc);
  return jf;
}


void ATOOLS::Getter<Selector_Base,Selector_Key,MAXMLLPT>::
PrintInfo(std::ostream &str,const size_t width) const
{
  str<<"MAXMLLPT MLLmax";
}

"""

