include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Electroweak Sherpa W/gamma* ->us+ jets@LO using Min_N_TChannels option."
evgenConfig.keywords = ["SM", "W", "electroweak", "jets", "VBF" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "prajita.bhattarai@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;

  % tags for process setup
  NJET:=1; LJET:=1,2; QCUT:=15.;

  % improve colour-flow treatment
  CSS_CSMODE=1

  % Force W->ud decay
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[-24,-2,1]=2
}(run)

(processes){
  Process 93 93 -> 24 93 93 93{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;

  Process 93 93 -> -24 93 93 93{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  PT 24 10.0 E_CMS
  PT -24 10.0 E_CMS
  NJetFinder 2 15.0 0.0 0.4 -1
}(selector)

(model){
  MASSIVE[5]=1
}(model)
"""
genSeq.Sherpa_i.NCores = 32
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

################################################################
# VBF Filtering of truth particles
################################################################
# Set up VBF filters
#import the relevant Generator filter
# This is necessary for xAOD filters
include("GeneratorFilters/FindJets.py")
include ("GeneratorFilters/VBFForwardJetsFilter.py")
CreateJets(prefiltSeq, 0.4) ## need to add "WZ" if want WZ jets, the 0.4 is radius Generators/GeneratorFilters/share/common/FindJets.py
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass

filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.JetMinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta=5.0
filtSeq.VBFForwardJetsFilter.NJets=2
filtSeq.VBFForwardJetsFilter.Jet1MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta=5.0
filtSeq.VBFForwardJetsFilter.Jet2MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta=5.0
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

# medium cut:
filtSeq.VBFForwardJetsFilter.MassJJ = 800.*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 3.5
