include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "4 charged leptons with 0,1j@NLO, polarization states are included."
evgenConfig.keywords = ["SM", "diboson", "4lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "m.yuan@cern.ch", "tairan.xu@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""

# collider setup

# settings matrix-element generation
ME_GENERATORS:
  - Comix
  - Amegic
  - OpenLoops
COMIX_DEFAULT_GAUGE: 0

# scale setting
MEPS:
  CORE_SCALE: VAR{0.25*sqr(91.153+91.153)}

# width 0 for the stable vector bosons in the hard matrix element
PARTICLE_DATA:
  24:
    Width: 0
  23:
    Width: 0

WIDTH_SCHEME: Fixed

# speed and neg weight fraction improvements
NLO_CSS_PSMODE: 2

# vector boson decays
HARD_DECAYS:
  Enabled: true
  Channels:
    23,11,-11: {Status: 2}
    23,13,-13: {Status: 2}
# settings for polarized cross sections 
  Pol_Cross_Section:
    Enabled: true
    Reference_System: [Lab, COM]

# vector boson production part pp -> ZZ
PROCESSES:
# leading order
- 93 93 -> 23 23 93{1}:
    Order: {QCD: 0, EW: 2}
    CKKW: 20
    # NLO QCD corrections
    NLO_Mode: MC@NLO
    NLO_Order: {QCD: 1, EW: 0}
    ME_Generator: Amegic
    RS_ME_Generator: Comix
    Loop_Generator: OpenLoops
"""
