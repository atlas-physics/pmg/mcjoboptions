include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "ZZZ (-> 4 charged leptons + 2 neutrinos) +O,1j@NLO+2,3j@LO."
evgenConfig.keywords = ["SM", "triboson", "multilepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]

evgenConfig.nEventsPerJob = 5000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  %tags for process setup
  NJET:=2; LJET:=3; QCUT:=30

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  %EW corrections setup
  %subleading contributions are zero for pure ew cores, but might not be in the +1j
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;

  METS_BBAR_MODE=5
  SOFT_SPIN_CORRELATIONS=1
  SPECIAL_TAU_SPIN_CORRELATIONS=1
  AMEGIC_CUT_MASSIVE_VECTOR_PROPAGATORS=0

  % NWF improvements
  NLO_CSS_PSMODE=1

  % massive b to exclude it from 93 container
  MASSIVE[5]=1

  %decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,11,-11]=2
  HDH_STATUS[23,13,-13]=2
  HDH_STATUS[23,15,-15]=2
  HDH_STATUS[23,12,-12]=2
  HDH_STATUS[23,14,-14]=2
  HDH_STATUS[23,16,-16]=2
}(run);

(processes){
  Process 93 93 -> 23 23 23 93{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[23]=0" ]

include("GeneratorFilters/DecaysFinalStateFilter.py")
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 23 ]
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 4
filtSeq.DecaysFinalStateFilter.NNeutrinos = 2
