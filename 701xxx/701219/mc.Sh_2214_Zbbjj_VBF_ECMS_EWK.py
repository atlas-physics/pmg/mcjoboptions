include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Electroweak Sherpa Z/gamma* ->bb+ jets@LO using Min_N_TChannels option."
evgenConfig.keywords = ["SM", "Z", "electroweak", "jets", "VBF" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 500

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;

  % tags for process setup
  NJET:=1; LJET:=1,2; QCUT:=15.;

  % improve colour-flow treatment
  CSS_CSMODE=1

  % Force Z->bb decay
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,5,-5]=2
}(run)

(processes){
  Process 93 93 -> 23 93 93 93{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  PT 23 10.0 E_CMS
}(selector)
"""
genSeq.Sherpa_i.NCores = 32
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

################################################################
# VBF Filtering of truth particles
################################################################
# Set up VBF filters
#import the relevant Generator filter
# This is necessary for xAOD filters
include("GeneratorFilters/FindJets.py")
include ("GeneratorFilters/VBFForwardJetsFilter.py")
CreateJets(prefiltSeq, 0.4) ## need to add "WZ" if want WZ jets, the 0.4 is radius Generators/GeneratorFilters/share/common/FindJets.py
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass

filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.JetMinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta=5.0
filtSeq.VBFForwardJetsFilter.NJets=2
filtSeq.VBFForwardJetsFilter.Jet1MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta=5.0
filtSeq.VBFForwardJetsFilter.Jet2MinPt=40.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta=5.0
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

# medium cut:
filtSeq.VBFForwardJetsFilter.MassJJ = 800.*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 3.5
