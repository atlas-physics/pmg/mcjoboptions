include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
# include("Sherpa_i/Lund_Hadronisation.py")

evgenConfig.description = "Sherpa ttll+0,1j@NLO with Sherpa 2.2.14 and EW correction weights (QSF = 4)"
evgenConfig.keywords = ["SM", "multilepton"]
evgenConfig.contact = ["atlas-generators-sherpa@cern.ch"]
evgenConfig.nEventsPerJob = 50

genSeq.Sherpa_i.RunCard="""
(run){
    # perturbative scales
    FSF:=1.; RSF:=1; QSF:=4;
    SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;

    # merging setup
    QCUT:=30.;
    LJET:=4,5; NJET:=1;
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR 0.5

    # EW corrections
    ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3
    NLO_CSS_PSMODE=1

    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0;
    STABLE[6] 0;
}(run)
(processes){
    Process 93 93 -> 6 -6 90 90 93{NJET};
    Order (*,2);
    # Print_Graphs feyn;
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET};
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;
}(processes)
(selector){
    Mass 11 -11  5 E_CMS;
    Mass 13 -13  5 E_CMS;
    Mass 15 -15  5 E_CMS;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0", "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1"]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]
genSeq.Sherpa_i.Parameters += [ "PDF_VARIATIONS=NNPDF30_nnlo_as_0118_hessian[all] NNPDF30_nnlo_as_0117 NNPDF30_nnlo_as_0119 MSHT20nnlo_as118 CT18NNLO_as_0118 PDF4LHC21_40_pdfas[all] NNPDF31_nnlo_as_0118_hessian NNPDF40_nnlo_as_01180_hessian CT18ANNLO CT18XNNLO CT18ZNNLO NNPDF30_nlo_as_0118_hessian[all]" ] # Add NLO PDF wrt NNPDF30NNLO.py

genSeq.Sherpa_i.NCores = 128
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplltt", "ppllttj", "pplltt_ew", "ppllttj_ew" ]
