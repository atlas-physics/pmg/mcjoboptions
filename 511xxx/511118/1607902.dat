# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  21:32
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.16835667E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.03939620E+03  # scale for input parameters
    1    3.40094845E+02  # M_1
    2    1.54169969E+03  # M_2
    3    1.18708124E+03  # M_3
   11    2.52573382E+03  # A_t
   12   -1.91562716E+03  # A_b
   13    1.70981875E+03  # A_tau
   23    6.34111489E+02  # mu
   25    3.05432542E+01  # tan(beta)
   26    7.51443983E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.81176969E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.28560631E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.08811319E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.03939620E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.03939620E+03  # (SUSY scale)
  1  1     8.38886469E-06   # Y_u(Q)^DRbar
  2  2     4.26154326E-03   # Y_c(Q)^DRbar
  3  3     1.01344224E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.03939620E+03  # (SUSY scale)
  1  1     5.14871371E-04   # Y_d(Q)^DRbar
  2  2     9.78255604E-03   # Y_s(Q)^DRbar
  3  3     5.10591062E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.03939620E+03  # (SUSY scale)
  1  1     8.98488192E-05   # Y_e(Q)^DRbar
  2  2     1.85778862E-02   # Y_mu(Q)^DRbar
  3  3     3.12449483E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.03939620E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.52573370E+03   # A_t(Q)^DRbar
Block Ad Q=  3.03939620E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.91562806E+03   # A_b(Q)^DRbar
Block Ae Q=  3.03939620E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.70981886E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.03939620E+03  # soft SUSY breaking masses at Q
   1    3.40094845E+02  # M_1
   2    1.54169969E+03  # M_2
   3    1.18708124E+03  # M_3
  21   -5.90561724E+03  # M^2_(H,d)
  22   -2.24535909E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.81176969E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.28560631E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.08811319E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22060083E+02  # h0
        35     7.51506605E+02  # H0
        36     7.51443983E+02  # A0
        37     7.58970334E+02  # H+
   1000001     1.01216632E+04  # ~d_L
   2000001     1.00978228E+04  # ~d_R
   1000002     1.01213026E+04  # ~u_L
   2000002     1.01013036E+04  # ~u_R
   1000003     1.01216679E+04  # ~s_L
   2000003     1.00978303E+04  # ~s_R
   1000004     1.01213072E+04  # ~c_L
   2000004     1.01013051E+04  # ~c_R
   1000005     2.12401259E+03  # ~b_1
   2000005     2.82959426E+03  # ~b_2
   1000006     2.82513324E+03  # ~t_1
   2000006     3.26990924E+03  # ~t_2
   1000011     1.00201500E+04  # ~e_L-
   2000011     1.00086248E+04  # ~e_R-
   1000012     1.00193880E+04  # ~nu_eL
   1000013     1.00201709E+04  # ~mu_L-
   2000013     1.00086629E+04  # ~mu_R-
   1000014     1.00194079E+04  # ~nu_muL
   1000015     1.00193977E+04  # ~tau_1-
   2000015     1.00262537E+04  # ~tau_2-
   1000016     1.00251031E+04  # ~nu_tauL
   1000021     1.49403819E+03  # ~g
   1000022     3.40268466E+02  # ~chi_10
   1000023     6.42643903E+02  # ~chi_20
   1000025     6.44546711E+02  # ~chi_30
   1000035     1.63737606E+03  # ~chi_40
   1000024     6.40302813E+02  # ~chi_1+
   1000037     1.63733870E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.30242729E-02   # alpha
Block Hmix Q=  3.03939620E+03  # Higgs mixing parameters
   1    6.34111489E+02  # mu
   2    3.05432542E+01  # tan[beta](Q)
   3    2.43220408E+02  # v(Q)
   4    5.64668060E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.92780579E-01   # Re[R_st(1,1)]
   1  2     1.19944661E-01   # Re[R_st(1,2)]
   2  1    -1.19944661E-01   # Re[R_st(2,1)]
   2  2    -9.92780579E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.31008661E-02   # Re[R_sb(1,1)]
   1  2     9.99914180E-01   # Re[R_sb(1,2)]
   2  1    -9.99914180E-01   # Re[R_sb(2,1)]
   2  2     1.31008661E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.39529091E-01   # Re[R_sta(1,1)]
   1  2     9.70889188E-01   # Re[R_sta(1,2)]
   2  1    -9.70889188E-01   # Re[R_sta(2,1)]
   2  2     2.39529091E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.93659188E-01   # Re[N(1,1)]
   1  2     3.53335814E-03   # Re[N(1,2)]
   1  3    -9.82483129E-02   # Re[N(1,3)]
   1  4     5.45545861E-02   # Re[N(1,4)]
   2  1     1.08115320E-01   # Re[N(2,1)]
   2  2     5.88365055E-02   # Re[N(2,2)]
   2  3    -7.01627886E-01   # Re[N(2,3)]
   2  4     7.01831641E-01   # Re[N(2,4)]
   3  1     3.07903386E-02   # Re[N(3,1)]
   3  2    -2.41863423E-02   # Re[N(3,2)]
   3  3    -7.05308053E-01   # Re[N(3,3)]
   3  4    -7.07818851E-01   # Re[N(3,4)]
   4  1    -2.10975450E-03   # Re[N(4,1)]
   4  2     9.97968337E-01   # Re[N(4,2)]
   4  3     2.46196763E-02   # Re[N(4,3)]
   4  4    -5.87249404E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.47922210E-02   # Re[U(1,1)]
   1  2     9.99394567E-01   # Re[U(1,2)]
   2  1     9.99394567E-01   # Re[U(2,1)]
   2  2     3.47922210E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -8.30450071E-02   # Re[V(1,1)]
   1  2     9.96545798E-01   # Re[V(1,2)]
   2  1     9.96545798E-01   # Re[V(2,1)]
   2  2     8.30450071E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01697265E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.87432807E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.16204732E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.42461547E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37913415E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.87123401E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.31164767E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01033128E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     7.67911393E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05155980E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03062302E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.84770369E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.22559423E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.61384011E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.35328341E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.37981967E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.86732548E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.55295584E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.64976593E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00884554E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     7.67531508E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04856727E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.27957580E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.21042277E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.08791443E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.97649667E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.23681301E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.03049082E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.49841015E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.53343822E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.03869325E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.58104939E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.52121246E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.57525743E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.53246784E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.17532239E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37917343E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.10965113E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.23633966E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02361984E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.37450651E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01642995E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37985874E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.10514495E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.23374957E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02212480E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.86623915E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01346087E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.57314840E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.99097421E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.59334181E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.65247434E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.26448155E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.27935010E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.68164374E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.22975648E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92678083E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.92670983E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.62191857E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.71727226E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.19860199E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.45443374E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.56419956E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.68203029E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.22963831E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92628937E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.92694124E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.62208455E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.71715367E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.23892106E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.45419628E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.56398188E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.19622342E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.97716142E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     7.24257560E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.28174647E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.44243766E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     6.90728844E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.98082639E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.59276003E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.28924897E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.32289682E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.77639101E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.58547438E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.70159800E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.86454052E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     5.41323460E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     9.63078464E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.85331431E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.82966591E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.33041358E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.71343168E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.92660661E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.49735403E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.01872762E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.71004283E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.82911854E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.40034873E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.56396629E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.85338765E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.82964114E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.35309789E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.71334215E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.92683761E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.49732815E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.03865718E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.70992470E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.04222285E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.40011274E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.56374858E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.96099394E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.19265303E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.30136252E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.31437120E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.89794757E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.73393528E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     5.64879409E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.27094843E-02    2     1000021         4   # BR(~t_1 -> ~g c)
     5.68953941E-01    2     1000021         6   # BR(~t_1 -> ~g t)
     1.06573141E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     6.50669507E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.04505269E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.39756356E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.92583772E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.01009658E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.01973727E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.33595929E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.69202146E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     8.46131581E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.47571276E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.84345369E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98305630E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.69048357E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.16831493E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.61654192E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.68230648E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.69604174E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.41810243E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     7.54115149E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     8.40866370E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.67423002E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     7.75644570E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.65229364E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     7.67882192E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.28593064E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.56023800E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.22017600E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.54352928E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     9.92091406E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     9.00745323E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     7.31690908E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.04430918E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.95520392E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.38808383E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.52843592E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.52843592E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.89525534E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     6.89525534E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.19194946E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.32386282E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.33454191E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     6.55058242E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     5.75180104E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.40057369E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.28170600E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.13530855E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     9.63063040E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     6.26274726E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     7.23159948E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.96339300E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.77262798E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.96518745E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.96518745E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.25075226E-03   # ~g
#    BR                NDA      ID1      ID2
     4.19401848E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.12232200E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.42599954E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.28444548E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.28439495E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.71966786E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.37802683E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.09370490E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.21890817E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.09115876E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.21978602E-01    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.20080082E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.20080082E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.28244779E-03   # Gamma(h0)
     2.51106028E-03   2        22        22   # BR(h0 -> photon photon)
     1.34631374E-03   2        22        23   # BR(h0 -> photon Z)
     2.24038051E-02   2        23        23   # BR(h0 -> Z Z)
     1.96369460E-01   2       -24        24   # BR(h0 -> W W)
     7.97596308E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.39454206E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.39957148E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.91835991E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.41112538E-07   2        -2         2   # BR(h0 -> Up up)
     2.73862100E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.26850671E-07   2        -1         1   # BR(h0 -> Down down)
     2.26717015E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.00572473E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     9.20223520E+00   # Gamma(HH)
     8.86989140E-08   2        22        22   # BR(HH -> photon photon)
     5.57282953E-08   2        22        23   # BR(HH -> photon Z)
     1.80239435E-05   2        23        23   # BR(HH -> Z Z)
     3.16632306E-05   2       -24        24   # BR(HH -> W W)
     9.19299337E-05   2        21        21   # BR(HH -> gluon gluon)
     1.13175275E-08   2       -11        11   # BR(HH -> Electron electron)
     5.03695747E-04   2       -13        13   # BR(HH -> Muon muon)
     1.45438429E-01   2       -15        15   # BR(HH -> Tau tau)
     1.97724501E-13   2        -2         2   # BR(HH -> Up up)
     3.83445049E-08   2        -4         4   # BR(HH -> Charm charm)
     2.10492754E-03   2        -6         6   # BR(HH -> Top top)
     9.85831775E-07   2        -1         1   # BR(HH -> Down down)
     3.56567101E-04   2        -3         3   # BR(HH -> Strange strange)
     8.51119554E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.45150965E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.88877922E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.04298825E+00   # Gamma(A0)
     1.13367641E-07   2        22        22   # BR(A0 -> photon photon)
     2.85517642E-08   2        22        23   # BR(A0 -> photon Z)
     1.46226471E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.13066611E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.03212068E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.45302147E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.81158435E-13   2        -2         2   # BR(A0 -> Up up)
     3.51301668E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.47553157E-03   2        -6         6   # BR(A0 -> Top top)
     9.84896881E-07   2        -1         1   # BR(A0 -> Down down)
     3.56228964E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.50362198E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     8.23915908E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.93671137E-05   2        23        25   # BR(A0 -> Z h0)
     2.53721145E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     9.59602137E+00   # Gamma(Hp)
     1.31299294E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.61345469E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.58778796E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.00340434E-06   2        -1         2   # BR(Hp -> Down up)
     1.66384373E-05   2        -3         2   # BR(Hp -> Strange up)
     9.61429932E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.82095536E-08   2        -1         4   # BR(Hp -> Down charm)
     3.61649444E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.34633889E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.15317045E-07   2        -1         6   # BR(Hp -> Down top)
     5.17023359E-06   2        -3         6   # BR(Hp -> Strange top)
     8.38890266E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.88945196E-05   2        24        25   # BR(Hp -> W h0)
     3.25528889E-09   2        24        35   # BR(Hp -> W HH)
     3.39403018E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.01813307E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    9.32872244E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    9.32890377E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99980562E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.09137482E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.07193731E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.01813307E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    9.32872244E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    9.32890377E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999913E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.73263067E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999913E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.73263067E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26098459E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.74405734E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.35652230E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.73263067E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999913E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.81412742E-04   # BR(b -> s gamma)
    2    1.58672946E-06   # BR(b -> s mu+ mu-)
    3    3.52374219E-05   # BR(b -> s nu nu)
    4    2.10266445E-15   # BR(Bd -> e+ e-)
    5    8.98222222E-11   # BR(Bd -> mu+ mu-)
    6    1.87324979E-08   # BR(Bd -> tau+ tau-)
    7    6.93503262E-14   # BR(Bs -> e+ e-)
    8    2.96259473E-09   # BR(Bs -> mu+ mu-)
    9    6.25497648E-07   # BR(Bs -> tau+ tau-)
   10    8.63902724E-05   # BR(B_u -> tau nu)
   11    8.92378440E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42353201E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93351048E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15937532E-03   # epsilon_K
   17    2.28169091E-15   # Delta(M_K)
   18    2.47908613E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28827198E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.37407088E-16   # Delta(g-2)_electron/2
   21    1.44252523E-11   # Delta(g-2)_muon/2
   22    4.08404202E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.99947123E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.49598403E-01   # C7
     0305 4322   00   2    -1.03140591E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.61176453E-01   # C8
     0305 6321   00   2    -1.21687929E-03   # C8'
 03051111 4133   00   0     1.61706810E+00   # C9 e+e-
 03051111 4133   00   2     1.61767381E+00   # C9 e+e-
 03051111 4233   00   2     9.49732820E-05   # C9' e+e-
 03051111 4137   00   0    -4.43976020E+00   # C10 e+e-
 03051111 4137   00   2    -4.43661876E+00   # C10 e+e-
 03051111 4237   00   2    -7.07732126E-04   # C10' e+e-
 03051313 4133   00   0     1.61706810E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61767372E+00   # C9 mu+mu-
 03051313 4233   00   2     9.49596736E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43976020E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43661885E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.07718730E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50467062E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.53368585E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50467062E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.53371524E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50467062E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.54199562E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85805249E-07   # C7
     0305 4422   00   2     2.36004283E-05   # C7
     0305 4322   00   2    -2.35719467E-07   # C7'
     0305 6421   00   0     3.30467710E-07   # C8
     0305 6421   00   2    -4.79393212E-05   # C8
     0305 6321   00   2    -1.11668686E-06   # C8'
 03051111 4133   00   2     4.28306413E-07   # C9 e+e-
 03051111 4233   00   2     5.06976918E-06   # C9' e+e-
 03051111 4137   00   2     5.99055428E-07   # C10 e+e-
 03051111 4237   00   2    -3.79716143E-05   # C10' e+e-
 03051313 4133   00   2     4.28304862E-07   # C9 mu+mu-
 03051313 4233   00   2     5.06976820E-06   # C9' mu+mu-
 03051313 4137   00   2     5.99057218E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.79716173E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.07199630E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     8.22866084E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.07199570E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     8.22866084E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.07182399E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     8.22866081E-06   # C11' nu_3 nu_3
