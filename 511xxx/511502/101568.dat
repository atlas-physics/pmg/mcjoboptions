# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  10:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.17615983E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.98002701E+03  # scale for input parameters
    1    1.26246538E+03  # M_1
    2    1.93220996E+03  # M_2
    3    2.33322577E+03  # M_3
   11   -1.25109525E+03  # A_t
   12    1.43184439E+03  # A_b
   13    6.60910183E+01  # A_tau
   23    1.52743473E+02  # mu
   25    2.08667200E+01  # tan(beta)
   26    2.03412137E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.12958563E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.73288441E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.11969027E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.98002701E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.98002701E+03  # (SUSY scale)
  1  1     8.39399453E-06   # Y_u(Q)^DRbar
  2  2     4.26414922E-03   # Y_c(Q)^DRbar
  3  3     1.01406196E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.98002701E+03  # (SUSY scale)
  1  1     3.51967949E-04   # Y_d(Q)^DRbar
  2  2     6.68739103E-03   # Y_s(Q)^DRbar
  3  3     3.49041915E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.98002701E+03  # (SUSY scale)
  1  1     6.14209809E-05   # Y_e(Q)^DRbar
  2  2     1.26999108E-02   # Y_mu(Q)^DRbar
  3  3     2.13591608E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.98002701E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.25109539E+03   # A_t(Q)^DRbar
Block Ad Q=  2.98002701E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.43184438E+03   # A_b(Q)^DRbar
Block Ae Q=  2.98002701E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     6.60910158E+01   # A_tau(Q)^DRbar
Block MSOFT Q=  2.98002701E+03  # soft SUSY breaking masses at Q
   1    1.26246538E+03  # M_1
   2    1.93220996E+03  # M_2
   3    2.33322577E+03  # M_3
  21    3.99054489E+06  # M^2_(H,d)
  22    1.46279465E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.12958563E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.73288441E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.11969027E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.20201959E+02  # h0
        35     2.03413605E+03  # H0
        36     2.03412137E+03  # A0
        37     2.03738947E+03  # H+
   1000001     1.01084956E+04  # ~d_L
   2000001     1.00857315E+04  # ~d_R
   1000002     1.01081299E+04  # ~u_L
   2000002     1.00888116E+04  # ~u_R
   1000003     1.01084983E+04  # ~s_L
   2000003     1.00857352E+04  # ~s_R
   1000004     1.01081326E+04  # ~c_L
   2000004     1.00888132E+04  # ~c_R
   1000005     3.18860450E+03  # ~b_1
   2000005     3.19257531E+03  # ~b_2
   1000006     2.77712741E+03  # ~t_1
   2000006     3.19775064E+03  # ~t_2
   1000011     1.00192002E+04  # ~e_L-
   2000011     1.00084791E+04  # ~e_R-
   1000012     1.00184370E+04  # ~nu_eL
   1000013     1.00192100E+04  # ~mu_L-
   2000013     1.00084982E+04  # ~mu_R-
   1000014     1.00184468E+04  # ~nu_muL
   1000015     1.00138789E+04  # ~tau_1-
   2000015     1.00219801E+04  # ~tau_2-
   1000016     1.00212035E+04  # ~nu_tauL
   1000021     2.74350932E+03  # ~g
   1000022     1.62901871E+02  # ~chi_10
   1000023     1.67676668E+02  # ~chi_20
   1000025     1.27692468E+03  # ~chi_30
   1000035     2.04423833E+03  # ~chi_40
   1000024     1.65345529E+02  # ~chi_1+
   1000037     2.04420389E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.61722015E-02   # alpha
Block Hmix Q=  2.98002701E+03  # Higgs mixing parameters
   1    1.52743473E+02  # mu
   2    2.08667200E+01  # tan[beta](Q)
   3    2.43267475E+02  # v(Q)
   4    4.13764975E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.37427877E-02   # Re[R_st(1,1)]
   1  2     9.96487404E-01   # Re[R_st(1,2)]
   2  1    -9.96487404E-01   # Re[R_st(2,1)]
   2  2     8.37427877E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.50618945E-01   # Re[R_sb(1,1)]
   1  2     9.88591894E-01   # Re[R_sb(1,2)]
   2  1    -9.88591894E-01   # Re[R_sb(2,1)]
   2  2     1.50618945E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.32210980E-02   # Re[R_sta(1,1)]
   1  2     9.99448027E-01   # Re[R_sta(1,2)]
   2  1    -9.99448027E-01   # Re[R_sta(2,1)]
   2  2     3.32210980E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     2.95781565E-02   # Re[N(1,1)]
   1  2    -3.12549216E-02   # Re[N(1,2)]
   1  3     7.11924207E-01   # Re[N(1,3)]
   1  4    -7.00936649E-01   # Re[N(1,4)]
   2  1     2.10319099E-02   # Re[N(2,1)]
   2  2    -2.47037764E-02   # Re[N(2,2)]
   2  3    -7.02210901E-01   # Re[N(2,3)]
   2  4    -7.11229381E-01   # Re[N(2,4)]
   3  1     9.99338103E-01   # Re[N(3,1)]
   3  2     3.92396211E-03   # Re[N(3,2)]
   3  3    -6.28049700E-03   # Re[N(3,3)]
   3  4     3.56161970E-02   # Re[N(3,4)]
   4  1     2.47932170E-03   # Re[N(4,1)]
   4  2    -9.99198407E-01   # Re[N(4,2)]
   4  3    -4.93247243E-03   # Re[N(4,3)]
   4  4     3.96493109E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.96965409E-03   # Re[U(1,1)]
   1  2     9.99975712E-01   # Re[U(1,2)]
   2  1     9.99975712E-01   # Re[U(2,1)]
   2  2     6.96965409E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -5.60401140E-02   # Re[V(1,1)]
   1  2     9.98428518E-01   # Re[V(1,2)]
   2  1     9.98428518E-01   # Re[V(2,1)]
   2  2     5.60401140E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.86671838E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.03550002E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.56832712E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.98633769E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.33704127E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.23196639E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01654616E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05855167E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.87314749E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.23618370E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.80998236E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.97318332E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.58616577E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.33736410E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.00481751E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.78249740E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.22974841E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01582151E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05709615E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.69480808E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.00914460E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.67356504E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.25707816E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.81753433E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.35722448E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.16088659E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.42752767E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.20399342E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.16933413E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.68060889E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.82357534E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.67094641E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.33707160E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.29881224E-04    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.25743619E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.96355898E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.03224327E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.07033698E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.03914122E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.33739440E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.29705770E-04    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.25641275E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.96140483E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.03151487E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.31005623E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.03769062E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42840463E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.83401050E-04    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.98631492E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     8.39289369E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.83927982E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.55753341E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.65485715E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.88097445E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.91240720E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92076701E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.08833494E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.61330077E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.05648070E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.01197860E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.46500828E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.88115665E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.91220131E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92050913E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.08846391E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.61327594E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.05640168E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.01196291E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.46487628E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.52406001E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.13553014E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.05356287E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.47445856E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.98313038E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.53084183E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     6.03646450E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.82920712E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     1.32162401E-03    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.04829572E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.64050314E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.70485442E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.14575353E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.56232139E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     6.01873848E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.22330090E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.63983001E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.96875983E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     7.04735445E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.09124694E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69045262E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.08819870E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.76204621E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.04742291E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.45347601E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.00882376E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.46474721E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.04742854E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.09121558E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69035233E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.08832736E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.76202394E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.04734486E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.56423583E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.00880804E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.46461516E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.16070493E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.37901333E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.44505596E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.31053830E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.84467869E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.04749035E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.02184301E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.11414076E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.38987222E-03    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.76848339E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     7.44499328E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.43664602E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.44228927E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.02105037E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.10710932E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.79697290E-11   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     5.15978449E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.33130512E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.71997312E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.70405753E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     8.48797455E-03    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.87732665E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.37352170E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.44452191E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.48366291E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41211021E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.40685215E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.42039958E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.50549597E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.00644567E-02    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.97848660E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.82805186E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     6.45943613E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     5.36127980E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.94883240E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     6.87415813E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     6.84259698E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.55131933E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.54010163E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.15382769E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     9.19282675E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     5.21948862E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     5.21948862E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.15853886E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.15853886E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.73987958E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.73987958E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.72221074E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.72221074E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     3.68865086E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.41883122E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.41883122E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.70761606E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.61877787E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.40241934E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.20855115E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.70754673E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     2.65320517E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.39606623E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.39606623E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.93937978E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.33944568E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.33944568E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.67525437E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.47371328E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.42871669E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.22834461E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.19567780E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.05660900E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.23814611E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     6.02943915E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.29553368E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     9.18914969E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.18914969E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.01804105E-01   # ~g
#    BR                NDA      ID1      ID2
     2.50337556E-03    2     1000022        21   # BR(~g -> chi^0_1 g)
     2.78995104E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
     2.20524370E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.62950015E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.27180774E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.58405715E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.28081099E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.29977174E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     3.78179685E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     6.46519817E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.48799905E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.48799905E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.02595536E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.02595536E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.97960191E-03   # Gamma(h0)
     2.60226318E-03   2        22        22   # BR(h0 -> photon photon)
     1.23348156E-03   2        22        23   # BR(h0 -> photon Z)
     1.92261280E-02   2        23        23   # BR(h0 -> Z Z)
     1.74917074E-01   2       -24        24   # BR(h0 -> W W)
     8.44002376E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.50116893E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.44698933E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.05484019E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.56318426E-07   2        -2         2   # BR(h0 -> Up up)
     3.03356093E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.41213768E-07   2        -1         1   # BR(h0 -> Down down)
     2.31911365E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.16259391E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.22044957E+01   # Gamma(HH)
     5.10747163E-08   2        22        22   # BR(HH -> photon photon)
     3.41756693E-08   2        22        23   # BR(HH -> photon Z)
     1.04307394E-05   2        23        23   # BR(HH -> Z Z)
     5.39725888E-06   2       -24        24   # BR(HH -> W W)
     1.02371707E-05   2        21        21   # BR(HH -> gluon gluon)
     1.06199109E-08   2       -11        11   # BR(HH -> Electron electron)
     4.72791675E-04   2       -13        13   # BR(HH -> Muon muon)
     1.36539509E-01   2       -15        15   # BR(HH -> Tau tau)
     7.94923625E-13   2        -2         2   # BR(HH -> Up up)
     1.54193254E-07   2        -4         4   # BR(HH -> Charm charm)
     1.11671830E-02   2        -6         6   # BR(HH -> Top top)
     8.17159740E-07   2        -1         1   # BR(HH -> Down down)
     2.95572723E-04   2        -3         3   # BR(HH -> Strange strange)
     7.73091198E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.56606697E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.41571921E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.87766867E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.75808060E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.80662971E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.37463488E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.82329874E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.19776813E+01   # Gamma(A0)
     2.27005262E-07   2        22        22   # BR(A0 -> photon photon)
     9.22705496E-08   2        22        23   # BR(A0 -> photon Z)
     5.58853089E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.05999454E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.71904409E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.36283723E-01   2       -15        15   # BR(A0 -> Tau tau)
     7.65736459E-13   2        -2         2   # BR(A0 -> Up up)
     1.48526187E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.10959769E-02   2        -6         6   # BR(A0 -> Top top)
     8.15627806E-07   2        -1         1   # BR(A0 -> Down down)
     2.95018404E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.71648608E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.86927360E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.76768884E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.38622098E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.55944878E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     8.48384237E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.70394911E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.44023005E-05   2        23        25   # BR(A0 -> Z h0)
     1.36625482E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.38635326E+01   # Gamma(Hp)
     1.15091273E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.92051135E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.39179927E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.70583812E-07   2        -1         2   # BR(Hp -> Down up)
     1.29482722E-05   2        -3         2   # BR(Hp -> Strange up)
     8.29559541E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.34399389E-08   2        -1         4   # BR(Hp -> Down charm)
     2.77876563E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.16167992E-03   2        -5         4   # BR(Hp -> Bottom charm)
     8.26847123E-07   2        -1         6   # BR(Hp -> Down top)
     1.84340966E-05   2        -3         6   # BR(Hp -> Strange top)
     7.93341753E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.56366372E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.97705205E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.51295193E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.25208683E-05   2        24        25   # BR(Hp -> W h0)
     3.57581062E-11   2        24        35   # BR(Hp -> W HH)
     3.65721103E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.29730699E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.35490273E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.35420004E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00016138E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.13525031E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.29663312E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.29730699E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.35490273E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.35420004E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997061E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.93903361E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997061E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.93903361E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26713484E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.74970721E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.00675610E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.93903361E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997061E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25198937E-04   # BR(b -> s gamma)
    2    1.58947216E-06   # BR(b -> s mu+ mu-)
    3    3.52450496E-05   # BR(b -> s nu nu)
    4    2.67790051E-15   # BR(Bd -> e+ e-)
    5    1.14396801E-10   # BR(Bd -> mu+ mu-)
    6    2.39438075E-08   # BR(Bd -> tau+ tau-)
    7    9.00538037E-14   # BR(Bs -> e+ e-)
    8    3.84709350E-09   # BR(Bs -> mu+ mu-)
    9    8.15875610E-07   # BR(Bs -> tau+ tau-)
   10    9.61339602E-05   # BR(B_u -> tau nu)
   11    9.93027004E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42504235E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93805727E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15946161E-03   # epsilon_K
   17    2.28168064E-15   # Delta(M_K)
   18    2.47910115E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28844270E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.22701623E-16   # Delta(g-2)_electron/2
   21    9.52120286E-12   # Delta(g-2)_muon/2
   22    2.69412038E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.76204968E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.92258243E-01   # C7
     0305 4322   00   2    -2.03450616E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.07369130E-01   # C8
     0305 6321   00   2    -2.99344121E-04   # C8'
 03051111 4133   00   0     1.61611453E+00   # C9 e+e-
 03051111 4133   00   2     1.61703136E+00   # C9 e+e-
 03051111 4233   00   2     9.50704604E-05   # C9' e+e-
 03051111 4137   00   0    -4.43880664E+00   # C10 e+e-
 03051111 4137   00   2    -4.43641881E+00   # C10 e+e-
 03051111 4237   00   2    -7.13091571E-04   # C10' e+e-
 03051313 4133   00   0     1.61611453E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61703131E+00   # C9 mu+mu-
 03051313 4233   00   2     9.50703446E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43880664E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43641886E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.13091500E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50483350E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.54564039E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50483350E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.54564061E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50483350E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.54570456E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823309E-07   # C7
     0305 4422   00   2     7.71456184E-06   # C7
     0305 4322   00   2     7.14083318E-07   # C7'
     0305 6421   00   0     3.30483180E-07   # C8
     0305 6421   00   2    -3.92878816E-06   # C8
     0305 6321   00   2     1.12067678E-07   # C8'
 03051111 4133   00   2     3.71813183E-07   # C9 e+e-
 03051111 4233   00   2     2.09034083E-06   # C9' e+e-
 03051111 4137   00   2    -7.82373342E-07   # C10 e+e-
 03051111 4237   00   2    -1.56862243E-05   # C10' e+e-
 03051313 4133   00   2     3.71812579E-07   # C9 mu+mu-
 03051313 4233   00   2     2.09034063E-06   # C9' mu+mu-
 03051313 4137   00   2    -7.82372805E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.56862249E-05   # C10' mu+mu-
 03051212 4137   00   2     1.86173114E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.40002306E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.86173130E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.40002306E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.86177720E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.40002306E-06   # C11' nu_3 nu_3
