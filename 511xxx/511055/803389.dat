# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:40
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.12254125E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.73109998E+03  # scale for input parameters
    1   -1.42637342E+02  # M_1
    2   -1.28925485E+02  # M_2
    3    3.69857718E+03  # M_3
   11    4.10881445E+03  # A_t
   12   -6.67611385E+02  # A_b
   13    1.75175389E+03  # A_tau
   23    1.56291085E+03  # mu
   25    1.06475898E+01  # tan(beta)
   26    3.44759223E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.98053859E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.37150973E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.58081788E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.73109998E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.73109998E+03  # (SUSY scale)
  1  1     8.42126848E-06   # Y_u(Q)^DRbar
  2  2     4.27800439E-03   # Y_c(Q)^DRbar
  3  3     1.01735687E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.73109998E+03  # (SUSY scale)
  1  1     1.80181033E-04   # Y_d(Q)^DRbar
  2  2     3.42343963E-03   # Y_s(Q)^DRbar
  3  3     1.78683124E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.73109998E+03  # (SUSY scale)
  1  1     3.14429079E-05   # Y_e(Q)^DRbar
  2  2     6.50139611E-03   # Y_mu(Q)^DRbar
  3  3     1.09342787E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.73109998E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.10881392E+03   # A_t(Q)^DRbar
Block Ad Q=  4.73109998E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -6.67611513E+02   # A_b(Q)^DRbar
Block Ae Q=  4.73109998E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.75175390E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.73109998E+03  # soft SUSY breaking masses at Q
   1   -1.42637342E+02  # M_1
   2   -1.28925485E+02  # M_2
   3    3.69857718E+03  # M_3
  21    9.28098642E+06  # M^2_(H,d)
  22   -1.86785841E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.98053859E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.37150973E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.58081788E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24039252E+02  # h0
        35     3.44738483E+03  # H0
        36     3.44759223E+03  # A0
        37     3.44876494E+03  # H+
   1000001     1.01137000E+04  # ~d_L
   2000001     1.00886798E+04  # ~d_R
   1000002     1.01133402E+04  # ~u_L
   2000002     1.00912892E+04  # ~u_R
   1000003     1.01137000E+04  # ~s_L
   2000003     1.00886797E+04  # ~s_R
   1000004     1.01133402E+04  # ~c_L
   2000004     1.00912893E+04  # ~c_R
   1000005     3.74235426E+03  # ~b_1
   2000005     5.05902751E+03  # ~b_2
   1000006     4.41937617E+03  # ~t_1
   2000006     5.06481145E+03  # ~t_2
   1000011     1.00212830E+04  # ~e_L-
   2000011     1.00094739E+04  # ~e_R-
   1000012     1.00205156E+04  # ~nu_eL
   1000013     1.00212835E+04  # ~mu_L-
   2000013     1.00094728E+04  # ~mu_R-
   1000014     1.00205154E+04  # ~nu_muL
   1000015     1.00092141E+04  # ~tau_1-
   2000015     1.00213687E+04  # ~tau_2-
   1000016     1.00204561E+04  # ~nu_tauL
   1000021     4.22867059E+03  # ~g
   1000022     1.43770623E+02  # ~chi_10
   1000023     1.47186993E+02  # ~chi_20
   1000025     1.58855284E+03  # ~chi_30
   1000035     1.58913905E+03  # ~chi_40
   1000024     1.47362956E+02  # ~chi_1+
   1000037     1.59011398E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.90171855E-02   # alpha
Block Hmix Q=  4.73109998E+03  # Higgs mixing parameters
   1    1.56291085E+03  # mu
   2    1.06475898E+01  # tan[beta](Q)
   3    2.42775964E+02  # v(Q)
   4    1.18858922E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -8.04654559E-02   # Re[R_st(1,1)]
   1  2     9.96757398E-01   # Re[R_st(1,2)]
   2  1    -9.96757398E-01   # Re[R_st(2,1)]
   2  2    -8.04654559E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.13785767E-03   # Re[R_sb(1,1)]
   1  2     9.99995077E-01   # Re[R_sb(1,2)]
   2  1    -9.99995077E-01   # Re[R_sb(2,1)]
   2  2     3.13785767E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.10145312E-01   # Re[R_sta(1,1)]
   1  2     9.93915494E-01   # Re[R_sta(1,2)]
   2  1    -9.93915494E-01   # Re[R_sta(2,1)]
   2  2     1.10145312E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.97823273E-01   # Re[N(1,1)]
   1  2     6.10142086E-02   # Re[N(1,2)]
   1  3     2.50196261E-02   # Re[N(1,3)]
   1  4     8.47402557E-06   # Re[N(1,4)]
   2  1    -5.96662667E-02   # Re[N(2,1)]
   2  2     9.96889923E-01   # Re[N(2,2)]
   2  3    -5.14820346E-02   # Re[N(2,3)]
   2  4     1.34761915E-04   # Re[N(2,4)]
   3  1    -1.98565590E-02   # Re[N(3,1)]
   3  2     3.51706532E-02   # Re[N(3,2)]
   3  3     7.05903223E-01   # Re[N(3,3)]
   3  4     7.07155840E-01   # Re[N(3,4)]
   4  1     1.98587282E-02   # Re[N(4,1)]
   4  2    -3.53662684E-02   # Re[N(4,2)]
   4  3    -7.05991684E-01   # Re[N(4,3)]
   4  4     7.07057706E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     9.97512690E-01   # Re[U(1,1)]
   1  2    -7.04871152E-02   # Re[U(1,2)]
   2  1     7.04871152E-02   # Re[U(2,1)]
   2  2     9.97512690E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99999977E-01   # Re[V(1,1)]
   1  2     2.15675574E-04   # Re[V(1,2)]
   2  1     2.15675574E-04   # Re[V(2,1)]
   2  2     9.99999977E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02712396E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.95690112E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.56013157E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.74842374E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.74909975E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44828559E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.07441400E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.83499950E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.74633410E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.77402627E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05830719E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     2.87589468E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02872328E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.95373440E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.55944231E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.53977978E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.54062398E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.58245723E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44836576E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.07435495E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.83484426E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.02176914E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.04951839E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05797215E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     2.87573565E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.59310668E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     8.87924602E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.42914357E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.92625039E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.92615341E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.32026638E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.60572601E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.45958381E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.09311123E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.77138868E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.37919846E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     8.38563042E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.92325368E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.45981199E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44832449E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     6.78285826E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.22160424E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     6.06514386E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     6.11716392E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08792734E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44840455E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     6.78248319E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.22142610E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     6.06480847E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     6.11682565E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08759359E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47097058E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.67839416E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.17198770E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.97172978E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     6.02294864E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.99497103E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.53207174E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.46732186E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.02530371E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.89702553E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.78461003E+02   # ~d_L
#    BR                NDA      ID1      ID2
     8.92779131E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.65744082E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.30518107E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.20150460E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.01206579E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.46736638E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.02529562E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.89694475E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.78466742E+02   # ~s_L
#    BR                NDA      ID1      ID2
     8.92773745E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.65738541E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.30516998E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.25312674E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.01199800E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.27283292E+00   # ~b_1
#    BR                NDA      ID1      ID2
     3.93719953E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.02149291E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.50932099E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.50920273E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.01912395E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.98387057E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.98167268E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.53418805E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.13896287E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.71033330E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.71232110E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.22684712E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.27573488E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.82958803E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.73555683E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.01674430E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.72624371E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.63887062E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.97747479E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.42216317E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.60053061E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.78444068E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.73691763E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.37950408E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.31168414E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.01173919E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.63894029E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.97742553E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.42214556E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.60041163E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.78449775E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.73688605E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.37945017E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.31167327E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.01167134E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.48607089E+02   # ~t_1
#    BR                NDA      ID1      ID2
     6.53927964E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.04971115E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.31230904E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.33620542E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.67806223E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.61169415E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.85511258E-03    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.02312394E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.57774148E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.06066171E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.09040629E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.07351539E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.18673533E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54694394E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.82038957E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.97818625E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.85262046E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.58579669E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.75100875E-11   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     4.32317099E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.36686484E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.44104954E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.43483099E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     4.33946775E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.12658523E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.14125851E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.72822307E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.92442436E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.95281149E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.22310216E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.64495900E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.44923654E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.81415756E-14   # chi^0_2
#    BR                NDA      ID1      ID2
     9.35544171E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.11855236E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.12269067E-03    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.42030326E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.40871871E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.03796117E-03    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.95395948E-03    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.78654743E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.13131001E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.92375711E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.92375711E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.67260164E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.56240927E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     3.30755077E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.57656460E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.41296726E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     6.72433843E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.16968890E-02    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.16968890E-02    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.21663183E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.71872255E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.71872255E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.42438232E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.43965839E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.39154230E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.09331680E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.28655394E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     6.16979577E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.66825898E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.66825898E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.37309301E+00   # ~g
#    BR                NDA      ID1      ID2
     4.61452415E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.61452415E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.73913778E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.67053670E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     3.58015495E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.48539416E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.60916723E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.42494614E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.45353687E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.50562421E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     1.50562421E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     1.50562452E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     1.50562452E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     4.63035147E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     4.63035147E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.43778663E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.43778663E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.53167451E-03   # Gamma(h0)
     2.48127585E-03   2        22        22   # BR(h0 -> photon photon)
     1.50079945E-03   2        22        23   # BR(h0 -> photon Z)
     2.69135989E-02   2        23        23   # BR(h0 -> Z Z)
     2.26635069E-01   2       -24        24   # BR(h0 -> W W)
     7.79402925E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.11664134E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.27596833E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.56231881E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.45599344E-07   2        -2         2   # BR(h0 -> Up up)
     2.82559927E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.93053233E-07   2        -1         1   # BR(h0 -> Down down)
     2.14493296E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.70206949E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.44780045E+01   # Gamma(HH)
     4.29557259E-09   2        22        22   # BR(HH -> photon photon)
     2.66790205E-08   2        22        23   # BR(HH -> photon Z)
     6.02445520E-06   2        23        23   # BR(HH -> Z Z)
     1.52211957E-06   2       -24        24   # BR(HH -> W W)
     3.14563123E-06   2        21        21   # BR(HH -> gluon gluon)
     1.83212755E-09   2       -11        11   # BR(HH -> Electron electron)
     8.15785480E-05   2       -13        13   # BR(HH -> Muon muon)
     2.35613979E-02   2       -15        15   # BR(HH -> Tau tau)
     1.78491033E-12   2        -2         2   # BR(HH -> Up up)
     3.46267073E-07   2        -4         4   # BR(HH -> Charm charm)
     2.45608778E-02   2        -6         6   # BR(HH -> Top top)
     1.30230748E-07   2        -1         1   # BR(HH -> Down down)
     4.71037998E-05   2        -3         3   # BR(HH -> Strange strange)
     1.13630542E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.19415552E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.53133374E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.53133374E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.09107950E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.02719310E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.24904061E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.40460136E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.06809936E-01   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.92390648E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.30008670E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.29941519E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     7.56119881E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     7.33319197E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.35463942E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.81113920E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.42891086E+01   # Gamma(A0)
     2.04895903E-08   2        22        22   # BR(A0 -> photon photon)
     1.04140967E-08   2        22        23   # BR(A0 -> photon Z)
     1.63874655E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.78541820E-09   2       -11        11   # BR(A0 -> Electron electron)
     7.94995551E-05   2       -13        13   # BR(A0 -> Muon muon)
     2.29609733E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.67867154E-12   2        -2         2   # BR(A0 -> Up up)
     3.25646911E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.33170453E-02   2        -6         6   # BR(A0 -> Top top)
     1.26910859E-07   2        -1         1   # BR(A0 -> Down down)
     4.59030050E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.10734692E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.25152313E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.54558531E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.54558531E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.62487853E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.05476248E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.26481696E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.07460039E-01   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.41183851E-01   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.94646589E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.31952859E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.32227644E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.03164635E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.29008632E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.40820110E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.10203499E-05   2        23        25   # BR(A0 -> Z h0)
     3.62910236E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.50320977E+01   # Gamma(Hp)
     2.05145850E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     8.77062580E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.48083213E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.26630473E-07   2        -1         2   # BR(Hp -> Down up)
     2.14075139E-06   2        -3         2   # BR(Hp -> Strange up)
     1.27073812E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.40182607E-08   2        -1         4   # BR(Hp -> Down charm)
     4.60200829E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.77949417E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.98291777E-06   2        -1         6   # BR(Hp -> Down top)
     4.33019543E-05   2        -3         6   # BR(Hp -> Strange top)
     1.50388611E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.76063716E-07   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.55417653E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.37652819E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.68535580E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.49680102E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.29270423E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.49678966E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.95086998E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.08027483E-05   2        24        25   # BR(Hp -> W h0)
     1.94584039E-13   2        24        35   # BR(Hp -> W HH)
     8.62008058E-14   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.03892635E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.13467276E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.13371169E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00084772E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.97286159E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.82058475E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.03892635E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.13467276E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.13371169E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99978599E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.14006283E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99978599E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.14006283E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27485152E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.64871421E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.16975910E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.14006283E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99978599E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.26176219E-04   # BR(b -> s gamma)
    2    1.58961513E-06   # BR(b -> s mu+ mu-)
    3    3.52549795E-05   # BR(b -> s nu nu)
    4    2.59025551E-15   # BR(Bd -> e+ e-)
    5    1.10652782E-10   # BR(Bd -> mu+ mu-)
    6    2.31644784E-08   # BR(Bd -> tau+ tau-)
    7    8.71354507E-14   # BR(Bs -> e+ e-)
    8    3.72242393E-09   # BR(Bs -> mu+ mu-)
    9    7.89573817E-07   # BR(Bs -> tau+ tau-)
   10    9.67404000E-05   # BR(B_u -> tau nu)
   11    9.99291296E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42012463E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93661651E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15733335E-03   # epsilon_K
   17    2.28165835E-15   # Delta(M_K)
   18    2.48004518E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29062746E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.39369802E-17   # Delta(g-2)_electron/2
   21   -2.30597726E-12   # Delta(g-2)_muon/2
   22   -6.52446549E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.20927741E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.95006509E-01   # C7
     0305 4322   00   2    -1.15696555E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.02854756E-01   # C8
     0305 6321   00   2    -1.47029356E-04   # C8'
 03051111 4133   00   0     1.63206624E+00   # C9 e+e-
 03051111 4133   00   2     1.63223289E+00   # C9 e+e-
 03051111 4233   00   2     2.64152899E-05   # C9' e+e-
 03051111 4137   00   0    -4.45475834E+00   # C10 e+e-
 03051111 4137   00   2    -4.45332033E+00   # C10 e+e-
 03051111 4237   00   2    -1.93469524E-04   # C10' e+e-
 03051313 4133   00   0     1.63206624E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63223288E+00   # C9 mu+mu-
 03051313 4233   00   2     2.64152882E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45475834E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45332034E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.93469525E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50504097E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.17840706E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50504097E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.17840709E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50504097E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.17841417E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85814331E-07   # C7
     0305 4422   00   2     2.69675647E-07   # C7
     0305 4322   00   2    -1.28642621E-07   # C7'
     0305 6421   00   0     3.30475490E-07   # C8
     0305 6421   00   2    -2.12134146E-06   # C8
     0305 6321   00   2    -1.03399626E-07   # C8'
 03051111 4133   00   2     7.64124933E-07   # C9 e+e-
 03051111 4233   00   2     5.18958544E-07   # C9' e+e-
 03051111 4137   00   2     1.95842292E-07   # C10 e+e-
 03051111 4237   00   2    -3.80205372E-06   # C10' e+e-
 03051313 4133   00   2     7.64124665E-07   # C9 mu+mu-
 03051313 4233   00   2     5.18958535E-07   # C9' mu+mu-
 03051313 4137   00   2     1.95842568E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.80205375E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.68460941E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     8.21140025E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.68460797E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     8.21140025E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.68419783E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     8.21140023E-07   # C11' nu_3 nu_3
