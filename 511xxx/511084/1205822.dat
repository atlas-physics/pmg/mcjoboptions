# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:11
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.53597579E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.50178711E+03  # scale for input parameters
    1    1.55724948E+02  # M_1
    2    1.46179584E+02  # M_2
    3    3.27178160E+03  # M_3
   11    3.68040455E+03  # A_t
   12    1.50580618E+02  # A_b
   13    1.19318223E+03  # A_tau
   23   -1.79709477E+03  # mu
   25    2.43924557E+01  # tan(beta)
   26    3.16774194E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.80170932E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.09762575E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.04789709E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.50178711E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.50178711E+03  # (SUSY scale)
  1  1     8.39141496E-06   # Y_u(Q)^DRbar
  2  2     4.26283880E-03   # Y_c(Q)^DRbar
  3  3     1.01375033E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.50178711E+03  # (SUSY scale)
  1  1     4.11311612E-04   # Y_d(Q)^DRbar
  2  2     7.81492062E-03   # Y_s(Q)^DRbar
  3  3     4.07892232E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.50178711E+03  # (SUSY scale)
  1  1     7.17768840E-05   # Y_e(Q)^DRbar
  2  2     1.48411832E-02   # Y_mu(Q)^DRbar
  3  3     2.49604286E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.50178711E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.68040455E+03   # A_t(Q)^DRbar
Block Ad Q=  3.50178711E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.50580615E+02   # A_b(Q)^DRbar
Block Ae Q=  3.50178711E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.19318223E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.50178711E+03  # soft SUSY breaking masses at Q
   1    1.55724948E+02  # M_1
   2    1.46179584E+02  # M_2
   3    3.27178160E+03  # M_3
  21    6.75340507E+06  # M^2_(H,d)
  22   -2.99431073E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.80170932E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.09762575E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.04789709E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24140195E+02  # h0
        35     3.16732376E+03  # H0
        36     3.16774194E+03  # A0
        37     3.16934275E+03  # H+
   1000001     1.01039898E+04  # ~d_L
   2000001     1.00789916E+04  # ~d_R
   1000002     1.01036224E+04  # ~u_L
   2000002     1.00819433E+04  # ~u_R
   1000003     1.01039923E+04  # ~s_L
   2000003     1.00789943E+04  # ~s_R
   1000004     1.01036245E+04  # ~c_L
   2000004     1.00819443E+04  # ~c_R
   1000005     3.16709523E+03  # ~b_1
   2000005     3.86921636E+03  # ~b_2
   1000006     3.16276626E+03  # ~t_1
   2000006     3.87714803E+03  # ~t_2
   1000011     1.00214114E+04  # ~e_L-
   2000011     1.00090459E+04  # ~e_R-
   1000012     1.00206421E+04  # ~nu_eL
   1000013     1.00214235E+04  # ~mu_L-
   2000013     1.00090569E+04  # ~mu_R-
   1000014     1.00206500E+04  # ~nu_muL
   1000015     1.00120617E+04  # ~tau_1-
   2000015     1.00250046E+04  # ~tau_2-
   1000016     1.00228788E+04  # ~nu_tauL
   1000021     3.72761970E+03  # ~g
   1000022     1.57280961E+02  # ~chi_10
   1000023     1.65013812E+02  # ~chi_20
   1000025     1.81893049E+03  # ~chi_30
   1000035     1.81910622E+03  # ~chi_40
   1000024     1.65202991E+02  # ~chi_1+
   1000037     1.82031170E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.94991185E-02   # alpha
Block Hmix Q=  3.50178711E+03  # Higgs mixing parameters
   1   -1.79709477E+03  # mu
   2    2.43924557E+01  # tan[beta](Q)
   3    2.43084705E+02  # v(Q)
   4    1.00345890E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.72627962E-02   # Re[R_st(1,1)]
   1  2     9.95258734E-01   # Re[R_st(1,2)]
   2  1    -9.95258734E-01   # Re[R_st(2,1)]
   2  2    -9.72627962E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -2.23391743E-02   # Re[R_sb(1,1)]
   1  2     9.99750450E-01   # Re[R_sb(1,2)]
   2  1    -9.99750450E-01   # Re[R_sb(2,1)]
   2  2    -2.23391743E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.26582694E-01   # Re[R_sta(1,1)]
   1  2     9.45168633E-01   # Re[R_sta(1,2)]
   2  1    -9.45168633E-01   # Re[R_sta(2,1)]
   2  2    -3.26582694E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99691105E-01   # Re[N(1,1)]
   1  2    -1.66987621E-03   # Re[N(1,2)]
   1  3     2.47716119E-02   # Re[N(1,3)]
   1  4     1.12827103E-03   # Re[N(1,4)]
   2  1    -5.81457130E-04   # Re[N(2,1)]
   2  2     9.99038608E-01   # Re[N(2,2)]
   2  3     4.37894005E-02   # Re[N(2,3)]
   2  4     2.00239573E-03   # Re[N(2,4)]
   3  1     1.67530697E-02   # Re[N(3,1)]
   3  2    -2.95315487E-02   # Re[N(3,2)]
   3  3     7.06307313E-01   # Re[N(3,3)]
   3  4    -7.07090660E-01   # Re[N(3,4)]
   4  1     1.83491363E-02   # Re[N(4,1)]
   4  2    -3.23567369E-02   # Re[N(4,2)]
   4  3     7.06115313E-01   # Re[N(4,3)]
   4  4     7.07119167E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.98085310E-01   # Re[U(1,1)]
   1  2    -6.18523566E-02   # Re[U(1,2)]
   2  1     6.18523566E-02   # Re[U(2,1)]
   2  2    -9.98085310E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99996017E-01   # Re[V(1,1)]
   1  2     2.82235024E-03   # Re[V(1,2)]
   2  1     2.82235024E-03   # Re[V(2,1)]
   2  2     9.99996017E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02649660E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99422067E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.62582958E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.14992809E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44814541E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.73756776E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03653842E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.20669690E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.44905279E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06525587E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     2.17931897E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03470528E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.97794224E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.68496555E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.20594527E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     8.11347381E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44855789E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.73510883E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03568323E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.62064431E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.86214310E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06353609E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     2.17870139E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.24567767E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.63174999E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.47720704E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.59230658E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.54931215E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.29296642E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.11340101E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.47513319E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.11796403E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.62275439E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.19051561E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     4.21437181E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.23597337E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.82819460E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44818483E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.62806810E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.04000091E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.21769265E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     5.06236038E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08786679E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44859667E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.62562193E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.03913903E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.21649732E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     5.06092567E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08615239E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.86896642E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.56470905E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.98731825E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.81424010E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.90459024E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     4.68655369E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.63879805E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     7.39638876E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.98438164E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.39247196E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90601986E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.29944599E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.89382586E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.07895629E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.21321913E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     4.36404685E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.15425555E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.98461288E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.39212410E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90564144E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.29959755E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.89379730E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.07883480E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.21319420E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     4.41084867E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.15408953E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.11672500E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.58608531E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.51327430E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.09757372E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.09550068E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     8.94850966E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.08622245E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.22565164E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.30674459E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.38104404E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.18044891E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.18096887E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.74111472E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.91596963E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.28755705E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     1.08918362E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.80865568E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.66365109E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.15600533E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.65331806E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.63445674E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.29928098E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.96620982E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.07635285E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.21785189E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.15395652E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.15607422E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.65327740E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.63434999E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.29943178E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.96616954E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.07622835E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.21782762E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.15379041E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.50747878E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.06248360E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.82371685E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.22174623E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.25091513E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.68463723E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.40943464E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.25942286E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.99439159E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.33024582E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.88800553E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.87603062E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.68074136E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.46272871E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.62266230E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.38860264E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.33672984E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.32136292E-13   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.59574474E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.17521493E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.15797309E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.15680636E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.08290721E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
     4.51450678E-04    3     1000023       -11        12   # BR(chi^+_1 -> chi^0_2 e^+ nu_e)
     1.45565678E-04    3     1000023       -13        14   # BR(chi^+_1 -> chi^0_2 mu^+ nu_mu)
DECAY   1000037     1.31626927E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     9.44679555E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.90768848E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.90345551E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.96028846E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.48046234E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.18860333E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.03453218E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.39903714E-13   # chi^0_2
#    BR                NDA      ID1      ID2
     6.53895665E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     5.35446184E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     3.72554951E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     6.85428905E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     6.84160394E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.40895238E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.40223012E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     6.26044648E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     8.39730206E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.41172598E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.71476533E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.71476533E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.75167140E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.48206120E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.03488947E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.78666165E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.94561683E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     6.98179008E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.16648223E-02    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.16648223E-02    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.31255322E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.92066717E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.92066717E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.25689101E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.32677442E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.62451335E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.64103941E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.95170914E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     7.04329934E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.06167326E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.06167326E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.77504144E+01   # ~g
#    BR                NDA      ID1      ID2
     2.48718858E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.48718858E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.40506273E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.40506273E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.51592617E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.71295677E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     2.06880444E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.48663065E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.30284055E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.89470324E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.30446916E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.89558776E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     4.55165337E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     4.55165337E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.96996391E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.96996391E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.43871851E-03   # Gamma(h0)
     2.56474665E-03   2        22        22   # BR(h0 -> photon photon)
     1.56088419E-03   2        22        23   # BR(h0 -> photon Z)
     2.79981795E-02   2        23        23   # BR(h0 -> Z Z)
     2.35290162E-01   2       -24        24   # BR(h0 -> W W)
     8.02613670E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.01845120E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.23229225E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.43639697E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.42697586E-07   2        -2         2   # BR(h0 -> Up up)
     2.76940276E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.81649969E-07   2        -1         1   # BR(h0 -> Down down)
     2.10369131E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.59832336E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     5.12122728E+01   # Gamma(HH)
     8.25026142E-09   2        22        22   # BR(HH -> photon photon)
     4.70558278E-08   2        22        23   # BR(HH -> photon Z)
     9.03229666E-07   2        23        23   # BR(HH -> Z Z)
     2.54098665E-07   2       -24        24   # BR(HH -> W W)
     4.44047871E-06   2        21        21   # BR(HH -> gluon gluon)
     5.40823598E-09   2       -11        11   # BR(HH -> Electron electron)
     2.40806189E-04   2       -13        13   # BR(HH -> Muon muon)
     6.95483395E-02   2       -15        15   # BR(HH -> Tau tau)
     2.54700732E-13   2        -2         2   # BR(HH -> Up up)
     4.94099780E-08   2        -4         4   # BR(HH -> Charm charm)
     4.43190692E-03   2        -6         6   # BR(HH -> Top top)
     4.13704812E-07   2        -1         1   # BR(HH -> Down down)
     1.49654161E-04   2        -3         3   # BR(HH -> Strange strange)
     5.49062591E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.00581235E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.13687898E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.13687898E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.00405673E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.73067174E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.65642179E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.66897177E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     8.17681985E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.61973836E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.60672612E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.50085329E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.99385252E+01   # Gamma(A0)
     6.27333188E-09   2        22        22   # BR(A0 -> photon photon)
     6.49781173E-09   2        22        23   # BR(A0 -> photon Z)
     1.58864551E-05   2        21        21   # BR(A0 -> gluon gluon)
     5.32560692E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.37125352E-04   2       -13        13   # BR(A0 -> Muon muon)
     6.84853574E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.32541021E-13   2        -2         2   # BR(A0 -> Up up)
     4.51100786E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.12417226E-03   2        -6         6   # BR(A0 -> Top top)
     4.07375176E-07   2        -1         1   # BR(A0 -> Down down)
     1.47364143E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.40663655E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.06126274E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.16634969E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.16634969E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.03178715E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.88976698E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.82025292E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.79853777E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.40531219E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.64961214E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.66043542E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.56926232E-06   2        23        25   # BR(A0 -> Z h0)
     1.77212711E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.51442899E+01   # Gamma(Hp)
     6.11250929E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.61328863E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     7.39186682E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.03379035E-07   2        -1         2   # BR(Hp -> Down up)
     6.81408289E-06   2        -3         2   # BR(Hp -> Strange up)
     6.05544710E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.09388191E-08   2        -1         4   # BR(Hp -> Down charm)
     1.45436422E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.47979347E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.28712972E-07   2        -1         6   # BR(Hp -> Down top)
     5.20225243E-06   2        -3         6   # BR(Hp -> Strange top)
     5.75246374E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.02918513E-10   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.06362616E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.06985905E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     3.02115953E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.05970034E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.05945954E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.42407799E-06   2        24        25   # BR(Hp -> W h0)
     8.28141436E-13   2        24        35   # BR(Hp -> W HH)
     2.59511906E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.29371371E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.95062524E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.94991895E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00011871E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.56198997E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.68069516E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.29371371E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.95062524E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.94991895E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997827E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.17332162E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997827E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.17332162E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26781005E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.56330839E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.80114469E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.17332162E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997827E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.22872003E-04   # BR(b -> s gamma)
    2    1.58952563E-06   # BR(b -> s mu+ mu-)
    3    3.52441861E-05   # BR(b -> s nu nu)
    4    2.51634926E-15   # BR(Bd -> e+ e-)
    5    1.07495417E-10   # BR(Bd -> mu+ mu-)
    6    2.24933547E-08   # BR(Bd -> tau+ tau-)
    7    8.53926750E-14   # BR(Bs -> e+ e-)
    8    3.64796643E-09   # BR(Bs -> mu+ mu-)
    9    7.73413534E-07   # BR(Bs -> tau+ tau-)
   10    9.64701930E-05   # BR(B_u -> tau nu)
   11    9.96500160E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41958586E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93630665E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15721856E-03   # epsilon_K
   17    2.28165746E-15   # Delta(M_K)
   18    2.47935924E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28896569E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.06736788E-16   # Delta(g-2)_electron/2
   21   -4.56335310E-12   # Delta(g-2)_muon/2
   22   -1.29260405E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.94236503E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.91099883E-01   # C7
     0305 4322   00   2    -1.02925074E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.00733173E-01   # C8
     0305 6321   00   2    -1.39984465E-04   # C8'
 03051111 4133   00   0     1.62185662E+00   # C9 e+e-
 03051111 4133   00   2     1.62206511E+00   # C9 e+e-
 03051111 4233   00   2     1.25709336E-04   # C9' e+e-
 03051111 4137   00   0    -4.44454872E+00   # C10 e+e-
 03051111 4137   00   2    -4.44207875E+00   # C10 e+e-
 03051111 4237   00   2    -9.36181738E-04   # C10' e+e-
 03051313 4133   00   0     1.62185662E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62206504E+00   # C9 mu+mu-
 03051313 4233   00   2     1.25709282E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44454872E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44207882E+00   # C10 mu+mu-
 03051313 4237   00   2    -9.36181739E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50481698E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.02656911E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50481698E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.02656920E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50481698E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.02659380E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85849738E-07   # C7
     0305 4422   00   2    -4.00592035E-06   # C7
     0305 4322   00   2     3.28228635E-07   # C7'
     0305 6421   00   0     3.30505818E-07   # C8
     0305 6421   00   2     6.50259592E-06   # C8
     0305 6321   00   2     3.08261533E-07   # C8'
 03051111 4133   00   2     1.03220749E-06   # C9 e+e-
 03051111 4233   00   2     2.51517836E-06   # C9' e+e-
 03051111 4137   00   2     7.57955068E-07   # C10 e+e-
 03051111 4237   00   2    -1.87335284E-05   # C10' e+e-
 03051313 4133   00   2     1.03220611E-06   # C9 mu+mu-
 03051313 4233   00   2     2.51517812E-06   # C9' mu+mu-
 03051313 4137   00   2     7.57956530E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.87335292E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.36404882E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     4.05528192E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.36404808E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     4.05528192E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.36383727E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     4.05528191E-06   # C11' nu_3 nu_3
