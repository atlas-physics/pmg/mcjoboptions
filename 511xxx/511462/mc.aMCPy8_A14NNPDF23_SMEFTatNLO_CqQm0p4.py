import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os 

nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob 
nevents*=1.1

process="""
   import model SMEFTatNLO-NLO
   generate p p > t j $$ W- QED=2 QCD=0 NP=2 [QCD]
   output -f"""

beamEnergy=-999 
if hasattr(runArgs,'ecmEnergy'):
     beamEnergy = runArgs.ecmEnergy / 2. 
else:
     raise RuntimeError("No center of mass energy found.")

settings_run = {   'parton_shower': 'PYTHIA8',
                   'fixed_ren_scale':'True',
                   'fixed_fac_scale':'True',
                   'mur_ref_fixed':'172.5',
                   'muf_ref_fixed':'172.5',
                   'jetalgo': '-1.0',
                   'jetradius': '0.4',
                   'ptj': '0.0',
                   'etaj': '0.0',
                   'etal': '0.0',
                   'mll_sf': '0.0',
                   'ptgmin': '0.0',
                   'nevents':int(nevents), }


params =dict()
#Set all EFT parameters to zero
params['dim6']=dict()
for i in range(2,7):
  params['dim6'][str(i)]=0.0
params['dim6']['9']=0.0
params['dim6']['10']=0.0

params['dim62f']=dict()
for i in range(1,17):
  params['dim62f'][str(i)]=0.0
for i in [11,12,23]:
  params['dim62f'][str(i)]= 1.0e-13 
for i in [19,22,24]:
  params['dim62f'][str(i)]= 0.0

params['dim64f']=dict()
for i in [1,2,3,4,6,7,8,11,12,13,14,16,17,19,20,21,23,25]:
  params['dim64f'][str(i)]=0.0
params['dim64f']['10']=-0.4 #The EFT parameter of interest

params['dim64f2l']=dict()
for i in range(1,11):
  params['dim64f2l'][str(i)]=0.0
for i in range(13,18):
  params['dim64f2l'][str(i)]=0.0


params['dim64f4l']=dict()
for i in range(1,10):
  params['dim64f4l'][str(i)]=0.0


process_dir = new_process(process) 
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings_run) 
modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs) 
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.contact          = ['maren.stratmann@cern.ch'] 
evgenConfig.generators       = ['MadGraph','Pythia8','EvtGen'] 
evgenConfig.description = 'Single-top t-channel production with four-fermion EFT operator' 

evgenConfig.nEventsPerJob = 10000
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
