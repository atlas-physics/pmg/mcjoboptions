# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.21722537E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.61600956E+03  # scale for input parameters
    1    1.18443566E+02  # M_1
    2   -6.93961907E+02  # M_2
    3    1.93377159E+03  # M_3
   11    2.78298763E+03  # A_t
   12   -4.01242717E+02  # A_b
   13    1.32433787E+03  # A_tau
   23   -2.74730205E+02  # mu
   25    2.12186416E+01  # tan(beta)
   26    1.76497979E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.92198841E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.40789697E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.69213593E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.61600956E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.61600956E+03  # (SUSY scale)
  1  1     8.39367816E-06   # Y_u(Q)^DRbar
  2  2     4.26398851E-03   # Y_c(Q)^DRbar
  3  3     1.01402374E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.61600956E+03  # (SUSY scale)
  1  1     3.57890473E-04   # Y_d(Q)^DRbar
  2  2     6.79991899E-03   # Y_s(Q)^DRbar
  3  3     3.54915203E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.61600956E+03  # (SUSY scale)
  1  1     6.24545047E-05   # Y_e(Q)^DRbar
  2  2     1.29136108E-02   # Y_mu(Q)^DRbar
  3  3     2.17185689E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.61600956E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.78298746E+03   # A_t(Q)^DRbar
Block Ad Q=  3.61600956E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -4.01242696E+02   # A_b(Q)^DRbar
Block Ae Q=  3.61600956E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.32433793E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.61600956E+03  # soft SUSY breaking masses at Q
   1    1.18443566E+02  # M_1
   2   -6.93961907E+02  # M_2
   3    1.93377159E+03  # M_3
  21   -8.81106187E+04  # M^2_(H,d)
  22    1.86963025E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.92198841E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.40789697E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.69213593E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23169893E+02  # h0
        35     1.76984559E+02  # H0
        36     1.76497979E+02  # A0
        37     2.00720066E+02  # H+
   1000001     1.01174419E+04  # ~d_L
   2000001     1.00923482E+04  # ~d_R
   1000002     1.01170775E+04  # ~u_L
   2000002     1.00958707E+04  # ~u_R
   1000003     1.01174434E+04  # ~s_L
   2000003     1.00923504E+04  # ~s_R
   1000004     1.01170791E+04  # ~c_L
   2000004     1.00958716E+04  # ~c_R
   1000005     2.76349365E+03  # ~b_1
   2000005     2.96900781E+03  # ~b_2
   1000006     2.96961905E+03  # ~t_1
   2000006     4.40309849E+03  # ~t_2
   1000011     1.00214170E+04  # ~e_L-
   2000011     1.00086429E+04  # ~e_R-
   1000012     1.00206471E+04  # ~nu_eL
   1000013     1.00214230E+04  # ~mu_L-
   2000013     1.00086543E+04  # ~mu_R-
   1000014     1.00206530E+04  # ~nu_muL
   1000015     1.00118749E+04  # ~tau_1-
   2000015     1.00231281E+04  # ~tau_2-
   1000016     1.00223204E+04  # ~nu_tauL
   1000021     2.33470354E+03  # ~g
   1000022     1.16741787E+02  # ~chi_10
   1000023     2.82783266E+02  # ~chi_20
   1000025     2.95647876E+02  # ~chi_30
   1000035     7.58037129E+02  # ~chi_40
   1000024     2.83195298E+02  # ~chi_1+
   1000037     7.58143330E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.11531433E-01   # alpha
Block Hmix Q=  3.61600956E+03  # Higgs mixing parameters
   1   -2.74730205E+02  # mu
   2    2.12186416E+01  # tan[beta](Q)
   3    2.43042337E+02  # v(Q)
   4    3.11515366E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99440869E-01   # Re[R_st(1,1)]
   1  2     3.34357566E-02   # Re[R_st(1,2)]
   2  1    -3.34357566E-02   # Re[R_st(2,1)]
   2  2    -9.99440869E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.48926901E-03   # Re[R_sb(1,1)]
   1  2     9.99954976E-01   # Re[R_sb(1,2)]
   2  1    -9.99954976E-01   # Re[R_sb(2,1)]
   2  2    -9.48926901E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -5.65766480E-02   # Re[R_sta(1,1)]
   1  2     9.98398259E-01   # Re[R_sta(1,2)]
   2  1    -9.98398259E-01   # Re[R_sta(2,1)]
   2  2    -5.65766480E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.82501553E-01   # Re[N(1,1)]
   1  2     5.33275955E-03   # Re[N(1,2)]
   1  3    -1.74849653E-01   # Re[N(1,3)]
   1  4    -6.39520093E-02   # Re[N(1,4)]
   2  1     7.95909931E-02   # Re[N(2,1)]
   2  2     1.25939086E-01   # Re[N(2,2)]
   2  3     7.04766902E-01   # Re[N(2,3)]
   2  4    -6.93619661E-01   # Re[N(2,4)]
   3  1     1.68266681E-01   # Re[N(3,1)]
   3  2    -5.24188976E-02   # Re[N(3,2)]
   3  3     6.85556414E-01   # Re[N(3,3)]
   3  4     7.06364627E-01   # Re[N(3,4)]
   4  1     6.50359579E-03   # Re[N(4,1)]
   4  2    -9.90637758E-01   # Re[N(4,2)]
   4  3     5.23795471E-02   # Re[N(4,3)]
   4  4    -1.25900432E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.41127453E-02   # Re[U(1,1)]
   1  2    -9.97249869E-01   # Re[U(1,2)]
   2  1    -9.97249869E-01   # Re[U(2,1)]
   2  2     7.41127453E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.78419822E-01   # Re[V(1,1)]
   1  2     9.83954454E-01   # Re[V(1,2)]
   2  1     9.83954454E-01   # Re[V(2,1)]
   2  2    -1.78419822E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02735513E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.65358079E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.32666503E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.82734145E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43418940E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.64362992E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     8.71401972E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.31122398E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.96291933E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.37324337E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04753382E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03399067E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.64106855E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.64542763E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.85456361E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.54939386E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43452219E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.64233819E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.82693544E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.39757468E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.96224006E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.37246284E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04613457E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.93206579E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.05819032E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     7.37903281E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.08061865E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.51150574E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.37625089E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.30821390E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.52562442E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.25438003E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.78621555E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.08283918E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.78634615E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.78327481E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.68347763E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43423769E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.29846266E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.13661112E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     6.21720034E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.00444121E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.95478997E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.88669541E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43457044E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.29654270E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.13611679E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     6.21576192E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.00374613E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.97734676E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.88534614E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.52837739E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.78862374E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.00534341E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.83523238E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.81986567E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     7.94466997E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.52839920E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.19273668E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.55980121E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.21433589E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92168838E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.49340011E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.50818809E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.53947937E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.55254992E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.09973098E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     5.75078739E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.03119136E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.42791084E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.19292346E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.56000001E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.27466728E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92143340E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.49353080E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.50849911E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.59357227E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.60376425E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.09965622E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     5.79234163E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.03117702E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.42778269E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.29087785E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.19854919E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.02939200E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.74378958E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.87718698E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.03789716E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.67166053E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.42992812E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.35318559E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.32189990E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.79244472E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.61037920E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.31956626E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.24132338E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.81951651E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.52267254E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.36484981E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.95428375E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.93619018E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     8.65272580E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69396991E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.49328337E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.70377280E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.02767378E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.07582181E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.33289710E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.00385563E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.42765484E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.36492319E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.95425647E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.95998517E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     8.67734485E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69387395E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.49341382E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.70376449E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.02972849E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.07575098E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.34369603E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.00384097E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.42752667E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.33355824E+02   # ~t_1
#    BR                NDA      ID1      ID2
     4.60299366E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.16992610E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.22939913E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.64028281E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.44660263E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.62897422E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.40324111E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.31722842E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.92621513E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.51915677E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.08792186E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.13127421E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.29441586E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.18309705E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.63202274E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.56445726E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.66855246E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.29890822E-04    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     1.32965932E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.49983064E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.21016639E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99845542E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.53461723E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.19712422E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     4.99414834E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.48460707E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.35044099E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     5.01370021E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.23056769E-01    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.90717129E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.38580772E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.09100686E-01    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.38630222E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     9.56990626E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.21148276E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     9.89041647E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.84603702E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.45888016E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.20280774E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.77896060E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.59673881E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.99959170E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     1.19988959E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.98131252E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.53352233E-04    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     5.70544568E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.30648985E-01    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.34416332E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.33298619E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.33298619E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.06158952E-01    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     1.06158952E-01    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     3.07832418E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.86152710E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.09421390E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.48712134E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.75301113E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.31166054E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.92309165E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.07797121E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.98858632E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     3.73161132E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     6.79545806E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     1.04171862E-01    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.08866942E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.08866942E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.13663127E-01   # ~g
#    BR                NDA      ID1      ID2
     2.44844831E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     2.93420284E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.24780446E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.18730857E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.18731021E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     5.55165407E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.56747406E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.53554005E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.16666341E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.55909662E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     4.82027210E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.33657308E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.33657610E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     2.81695567E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.34261710E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.34261799E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     3.69000581E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.12340923E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.12340923E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.20415166E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.20415166E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.20415650E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.20415650E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     3.59018521E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.59018521E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     1.50224934E-02   # Gamma(h0)
     5.78973936E-04   2        22        22   # BR(h0 -> photon photon)
     3.25431090E-04   2        22        23   # BR(h0 -> photon Z)
     5.63444236E-03   2        23        23   # BR(h0 -> Z Z)
     4.82872225E-02   2       -24        24   # BR(h0 -> W W)
     1.54187040E-02   2        21        21   # BR(h0 -> gluon gluon)
     7.06485266E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.14255859E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.06053654E-02   2       -15        15   # BR(h0 -> Tau tau)
     3.08943183E-08   2        -2         2   # BR(h0 -> Up up)
     5.99591734E-03   2        -4         4   # BR(h0 -> Charm charm)
     8.22727604E-07   2        -1         1   # BR(h0 -> Down down)
     2.97572984E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.32541254E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.56492655E+00   # Gamma(HH)
     3.33769557E-07   2        22        22   # BR(HH -> photon photon)
     3.72664424E-07   2        22        23   # BR(HH -> photon Z)
     6.03408695E-05   2        23        23   # BR(HH -> Z Z)
     1.48001806E-03   2       -24        24   # BR(HH -> W W)
     7.54312354E-04   2        21        21   # BR(HH -> gluon gluon)
     7.90236481E-09   2       -11        11   # BR(HH -> Electron electron)
     3.51548190E-04   2       -13        13   # BR(HH -> Muon muon)
     1.01429421E-01   2       -15        15   # BR(HH -> Tau tau)
     4.77935074E-12   2        -2         2   # BR(HH -> Up up)
     9.27859776E-07   2        -4         4   # BR(HH -> Charm charm)
     8.65792457E-07   2        -1         1   # BR(HH -> Down down)
     3.13157794E-04   2        -3         3   # BR(HH -> Strange strange)
     8.95608694E-01   2        -5         5   # BR(HH -> Bottom bottom)
DECAY        36     1.57895067E+00   # Gamma(A0)
     2.31964384E-07   2        22        22   # BR(A0 -> photon photon)
     7.12751767E-09   2        22        23   # BR(A0 -> photon Z)
     6.72459194E-04   2        21        21   # BR(A0 -> gluon gluon)
     7.90617757E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.51718022E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.01519364E-01   2       -15        15   # BR(A0 -> Tau tau)
     6.83117087E-13   2        -2         2   # BR(A0 -> Up up)
     1.32438338E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.66586111E-07   2        -1         1   # BR(A0 -> Down down)
     3.13444919E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.97140767E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.00103189E-06   2        23        25   # BR(A0 -> Z h0)
DECAY        37     3.06259906E-01   # Gamma(Hp)
     5.32820579E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     2.27797313E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     6.44240144E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.01139032E-06   2        -1         2   # BR(Hp -> Down up)
     8.12595976E-05   2        -3         2   # BR(Hp -> Strange up)
     5.49853049E-05   2        -5         2   # BR(Hp -> Bottom up)
     2.68893021E-07   2        -1         4   # BR(Hp -> Down charm)
     1.80692867E-03   2        -3         4   # BR(Hp -> Strange charm)
     7.69929537E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.16882926E-07   2        -1         6   # BR(Hp -> Down top)
     7.07085751E-06   2        -3         6   # BR(Hp -> Strange top)
     3.43685658E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.94296976E-05   2        24        25   # BR(Hp -> W h0)
     2.91998520E-05   2        24        35   # BR(Hp -> W HH)
     3.24050592E-05   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    5.58974169E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.45641010E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.50230751E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.89805801E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.24152819E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.22108329E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    5.58974169E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.45641010E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.50230751E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.95853500E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.14650015E-03        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.95853500E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.14650015E-03        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.09997651E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.35997368E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    7.00675839E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.14650015E-03        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.95853500E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.56076115E-04   # BR(b -> s gamma)
    2    1.62106866E-06   # BR(b -> s mu+ mu-)
    3    3.52676660E-05   # BR(b -> s nu nu)
    4    2.81481881E-15   # BR(Bd -> e+ e-)
    5    1.20245627E-10   # BR(Bd -> mu+ mu-)
    6    2.51583574E-08   # BR(Bd -> tau+ tau-)
    7    9.59424372E-14   # BR(Bs -> e+ e-)
    8    4.09864946E-09   # BR(Bs -> mu+ mu-)
    9    8.68859710E-07   # BR(Bs -> tau+ tau-)
   10    3.79586949E-05   # BR(B_u -> tau nu)
   11    3.92098786E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42293231E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92432171E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16028706E-03   # epsilon_K
   17    2.28177456E-15   # Delta(M_K)
   18    2.48078769E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29240494E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.99680846E-16   # Delta(g-2)_electron/2
   21    8.53700013E-12   # Delta(g-2)_muon/2
   22    2.41639575E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.76464278E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -4.06665487E-01   # C7
     0305 4322   00   2    -4.22654206E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.91008683E-01   # C8
     0305 6321   00   2    -3.75310740E-03   # C8'
 03051111 4133   00   0     1.62289867E+00   # C9 e+e-
 03051111 4133   00   2     1.62346277E+00   # C9 e+e-
 03051111 4233   00   2    -2.21491408E-04   # C9' e+e-
 03051111 4137   00   0    -4.44559078E+00   # C10 e+e-
 03051111 4137   00   2    -4.44533105E+00   # C10 e+e-
 03051111 4237   00   2     1.65141528E-03   # C10' e+e-
 03051313 4133   00   0     1.62289867E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62346273E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.21680822E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44559078E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44533108E+00   # C10 mu+mu-
 03051313 4237   00   2     1.65160531E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50529588E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -3.57403818E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50529588E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -3.57362682E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50529588E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -3.45770482E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825504E-07   # C7
     0305 4422   00   2     6.42679674E-06   # C7
     0305 4322   00   2     4.40476146E-07   # C7'
     0305 6421   00   0     3.30485060E-07   # C8
     0305 6421   00   2     2.14115110E-05   # C8
     0305 6321   00   2     4.97789210E-07   # C8'
 03051111 4133   00   2     9.52392331E-07   # C9 e+e-
 03051111 4233   00   2     2.77582864E-06   # C9' e+e-
 03051111 4137   00   2    -6.28821015E-08   # C10 e+e-
 03051111 4237   00   2    -2.06252974E-05   # C10' e+e-
 03051313 4133   00   2     9.52390968E-07   # C9 mu+mu-
 03051313 4233   00   2     2.77582840E-06   # C9' mu+mu-
 03051313 4137   00   2    -6.28807127E-08   # C10 mu+mu-
 03051313 4237   00   2    -2.06252981E-05   # C10' mu+mu-
 03051212 4137   00   2     4.88653273E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     4.46374682E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     4.88653943E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     4.46374682E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     4.88843952E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     4.46374680E-06   # C11' nu_3 nu_3
