# Settings
saveProcDir = False
gridpack_mode = True

MWR = 4000.

# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MCJobOptionUtils.JOsupport import get_physics_short

phys_short = get_physics_short()
MDD = float(phys_short.split('_')[4].replace('DD', ''))
MN1 = MN2 = MN3 = float(phys_short.split('_')[3].replace('N', ''))

# Some includes that are necessary to interface MadGraph with Pythia
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

# Number of events to produce
safety = 1.3  # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob * safety

# Import the LRSM NLO UFO model and define the process
if not is_gen_from_gridpack():
    process = '''
    import model mlrsm-nu-loop
    define n = n1 n2 n3
    generate g g > n n [noborn=QCD]
    output -f
    '''
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

# Set some values in the run_card
settings = {
    'ickkw': 0,
    'nevents': nevents,
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Set BSM particle masses in the param_card
masses = {
    '60': MN1,  # MN1
    '62': MN2,  # MN2
    '64': MN3,  # MN3
    '34': MWR,  # MWR
    '45': MDD   # MDD
}

# BSM particle width set to auto (only used when not generating from gridpack)
decays = {
    '60': 'auto',  # WN1
    '62': 'auto',  # WN2
    '64': 'auto',  # WN3
    '45': 'auto',  # WDD
    '25': 'auto'   # WH
}

# Modify the SM h <-> Delta mixing
thetamix = {'3': '1.000000e-01'}

# Create the param card and modify some parameters from their default values
params = {'MASS': masses, 'LRSMINPUTS': thetamix}
if not is_gen_from_gridpack(): params['DECAY'] = decays
modify_param_card(process_dir=process_dir, params=params)

# Do the event generation
generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)

# Add lifetimes
add_lifetimes(process_dir=process_dir, threshold=1e-25)

# These details are important information about the JOs
evgenConfig.description = 'Process g g > n n'
evgenConfig.contact = ['Blaž Leban <blaz.leban@cern.ch>']
evgenConfig.keywords += ['BSM', 'exotic']
evgenConfig.generators = ['MadGraph', 'Pythia8', 'EvtGen']

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=saveProcDir)

testSeq.TestHepMC.MaxTransVtxDisp = 1e+08  # in mm
testSeq.TestHepMC.MaxVtxDisp = 1e+08  # in mm
testSeq.TestHepMC.MaxNonG4Energy = 1e+08  # in MeV
