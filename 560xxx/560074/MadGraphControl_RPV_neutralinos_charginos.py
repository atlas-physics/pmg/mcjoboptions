from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#nevents = runArgs.maxEvents*25 if runArgs.maxEvents>0 else 20000

#Read filename of jobOptions to obtain: realgentype, decaytype, neutralino mass, lifetime.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')
realgentype = str(tokens[2])
realdecaytype = str(tokens[3])
neutralinoMass = float(tokens[4])
lifetime = str(tokens[5])
neutralinoLifetime = lifetime.replace("ns","").replace(".py","").replace("p","0.")
hbar = 6.582119514e-16
decayWidth = hbar/float(neutralinoLifetime)
decayWidthStr = '%e' % decayWidth

if realgentype == 'N1C1p' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > n1 x1+     RPV=0 QED<=2 / susystrong  @1
    add process p p > n1 x1+ j   RPV=0 QED<=2 / susystrong  @2
    add process p p > n1 x1+ j j RPV=0 QED<=2 / susystrong  @3
    '''
elif realgentype == 'N1C1m' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > n1 x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 x1- j j RPV=0 QED<=2 / susystrong @3
    '''
elif realgentype == 'N1N2' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > n1 n2     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 n2 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 n2 j j RPV=0 QED<=2 / susystrong @3
    '''
elif realgentype == 'N2C1p' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > n2 x1+     RPV=0 QED<=2 / susystrong @1
    add process p p > n2 x1+ j   RPV=0 QED<=2 / susystrong @2
    add process p p > n2 x1+ j j RPV=0 QED<=2 / susystrong @3
    '''
elif realgentype == 'N2C1m' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > n2 x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > n2 x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > n2 x1- j j RPV=0 QED<=2 / susystrong @3
    '''
elif realgentype == 'C1C1' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    generate    p p > x1+ x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > x1+ x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > x1+ x1- j j RPV=0 QED<=2 / susystrong @3
    '''
elif realgentype == 'C1N2N1' :
    process = '''
    import model RPVMSSM_UFO
    define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
    define c1 = x1+ x1-
    define X = n1 c1
    define Y = n2 c1
    generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
    add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
    add process p p > X y j j RPV=0 QED<=2 / susystrong @3
    '''
else:
    raise RunTimeError("ERROR: did not recognize realgentype arg")

headerN1 = 'DECAY   1000022  ' + decayWidthStr
headerC1 = 'DECAY   1000024  ' + decayWidthStr
headerN2 = 'DECAY   1000023  ' + decayWidthStr
evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)

run_settings.update({'time_of_flight':'1E-2'})

# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino 
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14 
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21 
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino 
param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31 
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32 
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44

if realdecaytype == "rpvdem":
        branchingRatiosN1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.05555556      3    2  1  3
        0.05555556      3    2  1  5
        0.05555556      3    2  3  5
        0.05555556      3    4  1  3
        0.05555556      3    4  1  5
        0.05555556      3    4  3  5
        0.05555556      3    6  1  3
        0.05555556      3    6  1  5
        0.05555556      3    6  3  5
        0.05555556      3   -2 -1 -3
        0.05555556      3   -2 -1 -5
        0.05555556      3   -2 -3 -5
        0.05555556      3   -4 -1 -3
        0.05555556      3   -4 -1 -5
        0.05555556      3   -4 -3 -5
        0.05555556      3   -6 -1 -3
        0.05555556      3   -6 -1 -5
        0.05555556      3   -6 -3 -5
        #'''
        branchingRatiosC1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.05555556      3    2  1  2
        0.05555556      3    2  1  4
        0.05555556      3    2  1  6
        0.05555556      3    2  3  2
        0.05555556      3    2  3  4
        0.05555556      3    2  3  6
        0.05555556      3    2  5  2
        0.05555556      3    2  5  4
        0.05555556      3    2  5  6
        0.05555556      3    4  1  4
        0.05555556      3    4  1  6
        0.05555556      3    4  3  4
        0.05555556      3    4  3  6
        0.05555556      3    4  5  4
        0.05555556      3    4  5  6
        0.05555556      3    6  1  6
        0.05555556      3    6  3  6
        0.05555556      3    6  5  6
        '''
elif realdecaytype == "rpvLF":
        branchingRatiosN1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.2500  3    2  1  3
        0.2500  3    4  1  3
        0.2500  3   -2 -1 -3
        0.2500  3   -4 -1 -3
        #'''
        branchingRatiosC1 = '''                                        
        #          BR         NDA      ID1       ID2                                                    
        0.2500000    3     2     1     4       # BR(~chi_1+ -> u d c)                              
        0.2500000    3     2     1     2       # BR(~chi_1+ -> u d u)                              
        0.2500000    3     2     3     4       # BR(~chi_1+ -> u s c)      
        0.2500000    3     2     3     2       # BR(~chi_1+ -> u s u)                       
        #'''
elif realdecaytype == "rpvHF":
        branchingRatiosN1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.50003   3    6  3  5
        0.50003   3   -6 -3 -5
        #'''
        branchingRatiosC1 = '''
        #          BR          NDA       ID1       ID2       ID3
        1.00000   3   -3 -5 -5
        #'''

else:
    raise RunTimeError("ERROR: realdecaytype is not rpvdem, rpvHF, or rpvLF")

decays['1000022'] = headerN1 + branchingRatiosN1
decays['1000024'] = headerC1 + branchingRatiosC1
decays['1000023'] = headerN2 + branchingRatiosN1

njets = 2
masses['1000022'] = float(neutralinoMass) #N1
masses['1000023'] = float(neutralinoMass+1) #N2
masses['1000024'] = float(neutralinoMass+1) #C1

evgenConfig.contact = ["mglisic@cern.ch"]
evgenConfig.keywords +=['SUSY', 'RPV', 'neutralino']

if realgentype == 'N1C1p' :
    evgenConfig.description = 'chargino+ - neutralino'
elif realgentype == 'N1C1m' :
    evgenConfig.description = 'chargino- - neutralino'
elif realgentype == 'N1N2' :
    evgenConfig.description = 'neutralino1 - neutralino2'
elif realgentype == 'C1C1' :
    evgenConfig.description = 'chargino+ - chargino-'
elif realgentype == 'N2C1p' :
    evgenConfig.description = 'chargino+ - neutralino2'
elif realgentype == 'N2C1m' :
    evgenConfig.description = 'chargino- - neutralino2'
elif realgentype == 'C1N2N1' :
    evgenConfig.description = 'inclusive of all higgsino production modes,'
evgenConfig.description += ' production and decays via RPV coupling (%s), m_N1 = %s GeV, m_C1 = %s GeV, m_N2 = %s GeV'%(realdecaytype, masses['1000022'],masses['1000024'],masses['1000023'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
   genSeq.Pythia8.Commands += ["Merging:Process = guess"]
   genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

testSeq.TestHepMC.MaxVtxDisp = 1e8 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1e8 #in mm
