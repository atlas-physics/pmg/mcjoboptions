###########################################################                                                                                                                                           
# Emerging Jets Event Generation                                                                                                                                                                         
# Pythia 8: Phi --> Qd Qd_bar --> 2EJ                                                                                                                                                                     
# contact: Danielle Wilson-Edwards (danielle.joan.wilson@cern.ch)                                                                                                                                              
#==========================================================                                                                                                                                               

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re
import math, cmath
import os


# set sample / model parameters automatically based on jo name
print("ARGS: ", runArgs.jobConfig[0])
print("JO ARGS: ", jofile.rstrip('.py').split('_'))


Ld = jofile.rstrip('.py').split('_')[2]
if Ld == "Ld10":
    L_d = 10.0
elif Ld == "Ld4":
    L_d = 4.0
elif Ld == "Ld20":
    L_d = 20.0
elif Ld == "Ld40":
    L_d = 40.0
elif Ld == "Ld1p6":
    L_d = 1.6
print("LAMBDA DARK: %s " % L_d)
rho = jofile.rstrip('.py').split('_')[3]
if rho == "rho20":
    m_rho_d = 20.0
elif rho == "rho8":
    m_rho_d = 8.0
elif rho == "rho40":
    m_rho_d = 40.0
elif rho == "rho80":
    m_rho_d = 80.0
elif rho == "rho3p2":
    m_rho_d = 3.2
print("DARK RHO MASS: %f " % m_rho_d)
pi = jofile.rstrip('.py').split('_')[4]
if pi == "pi5":
    m_pi_d = 5.0
elif pi == "pi2":
    m_pi_d = 2.0
elif pi == "pi10":
    m_pi_d = 10.0
elif pi == "pi20":
    m_pi_d = 20.0
elif pi == "pi0p8":
    m_pi_d = 0.8
print("DARK PION MASS: %f " % m_pi_d)
Xd = jofile.rstrip('.py').split('_')[5]
if Xd == "Xd600":
    m_Xd = 600.0
elif Xd == "Xd800":
    m_Xd = 800.0
elif Xd == "Xd1000":
    m_Xd = 1000.0
elif Xd == "Xd1200":
    m_Xd = 1200.0
elif Xd == "Xd1500":
    m_Xd = 1500.0
elif Xd == "Xd1800":
    m_Xd = 1800.0
elif Xd == "Xd2000":
    m_Xd = 2000.0
elif Xd == "Xd2200":
    m_Xd = 2200.0
elif Xd == "Xd2600":
    m_Xd = 2600.0
elif Xd == "Xd3000":
    m_Xd = 3000.0
elif Xd == "Xd3500":
    m_Xd = 3500.0
print("SCALAR MEDIATOR MASS: %f " % m_Xd)
lpi = jofile.rstrip('.py').split('_')[6]
if lpi == "l1":
    ctau_pi_d = 1.0
elif lpi == "l5":
    ctau_pi_d = 5.0
elif lpi == "l10":
    ctau_pi_d = 10.0
elif lpi == "l50":
    ctau_pi_d = 50.0
elif lpi == "l100":
    ctau_pi_d = 100.0
elif lpi == "l500":
    ctau_pi_d = 500.0
elif lpi == "l1000":
    ctau_pi_d = 1000.0
print("DARK PION LIFETIME: %f " % ctau_pi_d)
print("PT MIN FSR: %f " % (m_pi_d*2*1.1))


##  MG process generation 

safefactor=15
nevents=runArgs.maxEvents*safefactor

nevents_lhe = nevents


my_process = """
import model darkQCD_fv_down

define qd = dDark1 dDark1~

generate p p > qd qd @0
add process p p > qd qd j @1
add process p p > qd qd j j @2

output -f"""

## mediator and dark quark masses

masses = {'4900001':m_Xd,
          '4900101':m_pi_d*2}

print("MASS 4900001: ", m_Xd)
print("MASS 4900101: ", m_pi_d*2)


run_settings = {
    'ickkw' : '1', # MLM
    'cut_decays'  : 'F', 
    'drjj'        : 0.0,
    'maxjetflavor': 5,
    'xqcut'       : 20,
    'nevents':int(nevents_lhe)
}

PYTHIA8_nJetMax = 2
PYTHIA8_qCut = 20
PYTHIA8_nQmatch = 5
PYTHIA8_coneRadius = 0.4

process_dir = new_process(my_process)

modify_param_card(process_dir=process_dir,params={'MASS':masses})
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)


generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs)  


check_reset_proc_number(opts)


evgenConfig.description = "emerging jet events from dark quarks pair-produced via the t-channel"
evgenConfig.nEventsPerJob = 10000
evgenConfig.keywords = ["exotic", "hiddenValley", "2jet", "tChannel"]
evgenConfig.process = "p p --> Phi --> Qd Qd_bar --> 2EJ"
evgenConfig.contact = ["danielle.joan.wilson@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_MLM_ClassicalMatch.py")

genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on"]


## OVERRIDE STANDARD ATLAS TAU0 LIMIT ##
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"]

# settings for dark sector 
genSeq.Pythia8.Commands += ["HiddenValley:spinFV = 0",
                            "HiddenValley:Ngauge = 3", # n dark QCD colors
                            "HiddenValley:alphaFSR = 0.7"] # fixed dark coupling

# Model settings
genSeq.Pythia8.Commands += ["4900101:m0 = " + str(m_pi_d*2), # qd mass
                            "4900111:m0 = " + str(m_pi_d), # pi_d mass
                            "4900113:m0 = " + str(m_pi_d*4), # rho_d mass
                            "4900211:m0 = " + str(m_pi_d), # pi_d off-diag mass
                            "4900213:m0 = " + str(m_pi_d*4), # rho_d off-diag mass
                            "HiddenValley:Lambda = " + str(L_d),
                            "HiddenValley:separateFlav = off",			#do not allow seperate properties for dark quarks and pions
                            "HiddenValley:pTminFSR = " + str(m_pi_d*2*1.1)] # pT cutoff for dark shower


# dark pion lifetime
genSeq.Pythia8.Commands += ["4900111:tau0 = " + str(ctau_pi_d)] # pi_d lifetime -- variable
# off-diagonal dark pion lifetime
genSeq.Pythia8.Commands += ["4900211:tau0 = " + str(ctau_pi_d)]

mWidth = m_Xd / 5.6 / 9 * 3

#bifundamental mediator 
genSeq.Pythia8.Commands += ["4900001:m0 = " + str(m_Xd),
			                "4900001:mWidth = " + str(mWidth),
                            "4900001:0:meMode = 103"]


genSeq.Pythia8.Commands += ["4900001:oneChannel on 0.333 103 1  4900101",
                            "4900001:addChannel on 0.333 103 3  4900101",
                            "4900001:addChannel on 0.333 103 5  4900101"]

# dark meson decays
genSeq.Pythia8.Commands += ["4900111:0:all on 1.0 102 1 -1", # dark pion to down quarks
                            "4900113:0:all on 0.999 102 4900111 4900111", # dark vector to dark pions 99.9%
                            "4900113:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%
# dark meson off-diagonal decays
genSeq.Pythia8.Commands += ["4900211:oneChannel on 1.0 91 1 -1", # dark pion to down quarks
                            "4900213:oneChannel on 0.999 102 4900211 4900211", # dark vector to dark pions 99.9%
                            "4900213:addchannel on 0.001 102 1 -1"] # dark vector to down quarks 0.1%


# dark QCD coupling (alphaHV) running
genSeq.Pythia8.Commands += ["HiddenValley:alphaOrder = 1",
                            "HiddenValley:nFlav = 7"]

# HV parton shower settings
genSeq.Pythia8.Commands += ["HiddenValley:FSR = on",          
                            "HiddenValley:fragment = on"]

# workarounds for TestHepMC
testSeq.TestHepMC.MaxVtxDisp=5000000.
testSeq.TestHepMC.MaxTransVtxDisp = 5000000.

#Add the pdgids to a .txt file
whitelist = open('pdg_extras.dat','w')
whitelist.write('4900101\n')
whitelist.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'

## JET FILTERING ##
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if not hasattr( filtSeq, "TruthJetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()
    pass

filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.TruthJetFilter.Njet = 2
filtSeq.TruthJetFilter.NjetMinPt = 125
filtSeq.TruthJetFilter.NjetMaxEta = 2.4

