###############
# LO TEMPLATE
###############

      double precision function user_dynamical_scale(P)
c allow to define your own dynamical scale, need to set dynamical_scale_choice to 0 (or 10) to use it
      implicit none
      include 'nexternal.inc'
      double precision P(0:3, nexternal)
c Commmon to have access to all variable defined in the run_card
      include 'genps.inc'
      include 'run.inc'
      integer i
      real*8 pt1pt2, pt
      integer njets
      user_dynamical_scale = 1.0 ! your scale definition here
      pt1pt2 = 1.0
      njets = 0
      do i=nexternal-1,nexternal
        pt1pt2 = pt1pt2*pt(P(0,i))
        njets = njets+1
      enddo
      if(njets.ne.2) write(*,*) 'WARNING:  njets= ',njets
      user_dynamical_scale=dsqrt(pt1pt2)
      return
      end