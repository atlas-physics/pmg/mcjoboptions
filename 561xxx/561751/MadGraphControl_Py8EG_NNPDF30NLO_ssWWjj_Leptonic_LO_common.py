import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import *
import os

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'VBS ssWWjj'
evgenConfig.keywords = ['SM', 'electroweak', 'VBS', 'WW', 'SameSign', '2lepton', '2jet']
evgenConfig.contact = ['yi.yu@cern.ch', 'ana.rosario.cueto.gomez@cern.ch']

physShort=get_physics_short()
isLL = physShort.find("_LL") != -1
isTL = physShort.find("_TL") != -1
isLT = physShort.find("_LT") != -1
isTT = physShort.find("_TT") != -1
isWWcmFrame = physShort.find("_WWcmf_") != -1

isEWK = physShort.find("EWK") != -1
isINT = physShort.find("INT") != -1
isQCD = physShort.find("QCD") != -1

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
MADGRAPH_PDFSETTING['alternative_dynamic_scales'] = [1,2,3,4]

# General settings
nevents = int(runArgs.maxEvents*1.2 if runArgs.maxEvents>0 else 1.2*evgenConfig.nEventsPerJob)
gridpack_mode=True

# Processes
if isLL:
  gen = """
        generate p p > w+{0} w+{0} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{0} w-{0} j j QCD=0, w- > l- vl~ @0
        """
elif isTT:
  gen = """
        generate p p > w+{T} w+{T} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{T} w-{T} j j QCD=0, w- > l- vl~ @0
        """
elif isTL:
  gen = """  
        generate p p > w+{T} w+{0} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{T} w-{0} j j QCD=0, w- > l- vl~ @0
        """
elif isLT:
  gen = """  
        generate p p > w+{0} w+{T} j j QCD=0, w+ > l+ vl @0
        add process p p > w-{0} w-{T} j j QCD=0, w- > l- vl~ @0
        """
elif isINT:
  gen = """
        generate p p > l+ vl l+ vl j j QCD^2==2 @1
        add process p p > l- vl~ l- vl~ j j QCD^2==2 @1
        """
elif isQCD:
  gen = """
        generate p p > l+ vl l+ vl j j QED=4 QCD=2 @1
        add process p p > l- vl~ l- vl~ j j QED=4 QCD=2 @1
        """
elif isEWK:
  gen = """
        generate p p > l+ vl l+ vl j j QED==6 QCD=0 @1
        add process p p > l- vl~ l- vl~ j j QED==6 QCD=0 @1
        """
else:
  gen = """
        generate p p > w+ w+ j j QCD=0, w+ > l+ vl @0
        add process p p > w- w- j j QCD=0, w- > l- vl~ @0
        """

if not is_gen_from_gridpack():
  process = """
            import model sm-no_b_mass
            define l+ = e+ mu+ ta+
            define vl = ve vm vt
            define l- = e- mu- ta-
            define vl~ = ve~ vm~ vt~
            define p = g u c d s u~ c~ d~ s~ b b~
            define j = g u c d s u~ c~ d~ s~ b b~
            set group_subprocesses False
            {}
            output -f""".format(gen)

  process_dir = new_process(process)

else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

# Copy custom scale file
scaleToCopy      = 'custom_dynscale.f' # sqrt(ptj1*ptj2)
scaleDestination = process_dir+'/../custom_dynscale.f'
scalefile        = subprocess.Popen(['get_files','-data',scaleToCopy])
scalefile.wait()

if not os.access(scaleToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(scaleToCopy))

# Fetch default NLO run_card.dat and set parameters
settings = { 
            'lhe_version'  :'3.0',
            'bwcutoff'     :'15',
            'nevents'      :nevents,
            'dynamical_scale_choice': 0,
            'custom_fcts': scaleDestination,
            'maxjetflavor': 5, 
            'ptl': 4.0, 
            'ptj': 15.0, 
            'etal': 3.0, 
            'etaj': 5.5, 
            'drll': 0.2, 
            'asrwgtflavor': 5, 
            'auto_ptj_mjj': False, 
            'cut_decays': True, 
            'ptb': 15.0, 
            'etab': 5.5, 
            'dral': 0.1, 
            'drbb': 0.2, 
            'drbj': 0.2, 
            'drbl': 0.2, 
            'drjj': 0.2,
            'drjl': 0.2, 
            'mmll': 0.0,
            'mmjj' : 110.0,
            'sde_strategy': 2
        }

if isWWcmFrame:
  settings.update({ 'me_frame' : '3, 4, 5, 6' })

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode,required_accuracy=0.01)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Helper for resetting process number
check_reset_proc_number(opts)

############################
# Shower JOs will go here
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")  
include("Pythia8_i/Pythia8_MadGraph.py")        
include("Pythia8_i/Pythia8_ShowerWeights.py")   

# for VBS processes 
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
