import MadGraphControl.MadGraphUtils
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
  
evgenConfig.nEventsPerJob = 10000
  
  
if not is_gen_from_gridpack():
  process="""
  import model QAll_5_Aug21v2 
  define l = e+ mu+ e- mu-
  define v = ve ve~ vm vm~
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > l v a a QCD=0 T2^2==2
  output -f"""
  process_dir = new_process(process)
else:
  process_dir = str(MADGRAPH_GRIDPACK_LOCATION)






param_card_settings={'anoinputs':{
            'FS0'   : 0.0,
            'FS1'   : 0.0,
            'FS2'   : 0.0,
            'FM0'   : 0.0,
            'FM1'   : 0.0,
            'FM2'   : 0.0,
            'FM3'   : 0.0,
            'FM4'   : 0.0,
            'FM5'   : 0.0,
            'FM6'  : 0.0,
            'FM7'  : 0.0,
            'FT0'  : 0.0,
            'FT1'  : 0.0,
            'FT2'  : 9e-11,
            'FT3'  : 0.0,
            'FT4'  : 0.0,
            'FT5'  : 0.0,
            'FT6'  : 0.0,
            'FT7'  : 0.0,
            'FT8'  : 0.0,
            'FT9'  : 0.0}
}

include("Control_MGPy8EG_Wgg.py")
            
  