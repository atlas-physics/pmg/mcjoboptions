#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate per job.

evgenConfig.nEventsPerJob=10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------

include("MadGraphControl_Py8EG_NNPDF30LO_WbbChargedHiggs_LO_common.py")
