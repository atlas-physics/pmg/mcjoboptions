#---------------------------------------------------
# on-the-fly generation of th+ MG5 events
#---------------------------------------------------

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *
import math

evgenConfig.description = 'aMcAtNlo single top with charged Higgs at NLO'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['Adrian Berrocal <adrian.berrocal.guardia@cern.ch>']
nevents=1.1*(runArgs.maxEvents)
mode=0

#Charged Higgs mass is parsed via JO file.

model_pars_str = str(jofile)[:-3]

for s in model_pars_str.split("_"):

    if 'mhc' in s:
        ss = s.replace("mhc","")  
        if ss.isdigit():    
            mhc = int(ss)

#Process.

process="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme True
    import model 2HDMtypeII
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    """

if("Hminus" in model_pars_str):

    process+="""
        generate p p > t h- b~ [QCD]
        output -f
        """

else:
    
    process+="""
        generate p p > t~ h+ b [QCD]
        output -f
        """

#Set the mass for h1, h2 and h3.

mh1=1.250e+02                 
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

masses = {'25':str(mh1),
          '35':str(mh2),
          '36':str(mh3),
          '37':str(mhc)}

params = {}
params['mass'] = masses

runName='run_01'

extras = {'nevents'               :int(nevents), # Numebr of events to generate. 
           'lhe_version'           :'3.0',          
           'fixed_ren_scale'       :'F',         # Dynamical factorisation scale.          
           'fixed_fac_scale'       :'F',         # Dynamical factorisation scale.          
           'dynamical_scale_choice':2,           # Sum of the transverse mass as reference scale.  
           'muR_over_ref'          :0.3333,      # Renormalization scale is 0.3333 times the reference scale.
           'muF1_over_ref'         :0.3333,      # Factorization scale 1 is 0.3333 times the reference scale.
           'muF2_over_ref'         :0.3333,      # Factorization scale 2 is 0.3333 times the reference scale.  
           'parton_shower'         :'PYTHIA8',   # Parton shower Pythia8.
	    } 

#Parameters for madspin.

bwcut = 15

process_dir = new_process(process)

#Run card.

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#Parameters card.

modify_param_card(process_dir=process_dir,params=params)     

#MadSpin card.

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'

mscard = open(madspin_card_loc,'w')

mscard.write("""
    #************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    set BW_cut %i
    set seed %i
    define j = g u c d s b u~ c~ d~ s~ b~
    define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
    """%(bwcut, runArgs.randomSeed))

if("Hminus" in model_pars_str):
    
    mscard.write("""
    decay t > b w+, w+ > wdec wdec
    launch
    """)
    
else:

    mscard.write("""
    decay t~ > b~ w-, w- > wdec wdec
    launch
    """)

mscard.close()

print_cards()

#Generate the process.

generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

runArgs.inputGeneratorFile=outputDS

#Pythia 8 for the showering.

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#Use Pythia to simulate the decays of charged Higgs.

if("cb" in model_pars_str):

    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                                "37:oneChannel = 1 1. 0 4 -5" #Turn off all decays of H+ and switch on H+ to cb.
                            ]

else:
    
    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
			    			    "37:oneChannel = 1 1. 0 4 -3" #Turn off all decays of H+ and switch on H+ to cs.
						       ]