#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate per job.

evgenConfig.nEventsPerJob=10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------

include('MadGraphControl_Py8EG_NNPDF30NLO_tbChargedHiggs_NLO_common.py')
