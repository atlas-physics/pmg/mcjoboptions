#----------------------------------------------------------
# on-the-fly generation of ttbar->tChargedHiggsb MG5 events
#----------------------------------------------------------

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
import math

evgenConfig.description = 'aMcAtNlo ttbar going to charged Higgs at NLO'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['Adrian Berrocal <adrian.berrocal.guardia@cern.ch>']

nevents=1.1*(runArgs.maxEvents)
mode=0

#Charged Higgs mass is parsed via JO file.

model_pars_str = str(jofile)[:-3]

for s in model_pars_str.split("_"):

    if 'mhc' in s:
        ss = s.replace("mhc","")  
        if ss.isdigit():    
            mhc = int(ss)

#Process.

process="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model 2HDMtypeII-nobmass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
    generate p p > t t~ [QCD]
    output -f
    """

#Set the mass for h1, h2 and h3.

mh1=1.250e+02                 
mh2=math.sqrt(math.pow(mhc,2)+math.pow(8.0399e+01,2)) 
mh3=mh2

masses = {'25':str(mh1),
          '35':str(mh2),
          '36':str(mh3),
          '37':str(mhc)}

params = {}
params['mass'] = masses

#For masses greater than 100 GeV, Madspin seems to ignore the decay of the top quark into charged Higgs and b.
#This problem is solved computing the BRs of the top decays according to the model and adding them by hand.
#This is only done for masses below 140 GeV, because over 140 GeV other processes without two resonant tops are used.

if(140 > mhc > 100):

    #In order to compute the BRs the following parameters are necessary:
    # -mb: Mass of the b quark.
    # -mt: Mass of the top quark.
    # -mZ: Mass of the Z boson.
    # -Gf: Fermi's constant
    # -aEW: Fine estructure constant.
    # -beta: An angle that diagonalises one of the squared-matrices.

    mb = 4.7
    mt = 172
    mZ = 91.18760

    Gf = 0.000011663900000000002
    aEW = 1/127.9
    beta = 20

    #From the previous parameters, the following ones are calculated:
    # -mW: Mass of the W boson.
    # -ee: Coupling of the electromagnatic vertex.
    # -sw: Sinus of the Weinberg angle.     
    # -gw: Coupling of the electroweak vertex.
    # -vev: Vaccuum expectation value.
    # -I1a33: Coupling of the vertex between the charged Higgs with the top and bottom quarks.
    # -I2a33: Coupling of the vertex between the charged Higgs with the top and bottom quarks.

    mW = math.sqrt(mZ**2/2. + math.sqrt(mZ**4/4. - (aEW*math.pi*mZ**2)/(Gf*math.sqrt(2))))

    ee = math.sqrt(4*math.pi*aEW)

    sw = math.sqrt(1-mW**2/mZ**2)
    gw = ee/sw

    vev = 2*mW*sw/ee
    I1a33 = math.sqrt(2)*mb*math.tan(beta)/vev
    I2a33 = math.sqrt(2)*mt/(math.tan(beta)*vev)

    #With the previous parameters, the following partial decay widths are computed:
    # -BR_1: Partial width of t going to a charged H and a b.
    # -BR_2: Partial width of t going to a W and a b.

    BR_1 = (((3*gw**2*mb**2)/2. + (3*gw**2*mt**2)/2. + (3*gw**2*mb**4)/(2.*mW**2) - (3*gw**2*mb**2*mt**2)/mW**2 + (3*gw**2*mt**4)/(2.*mW**2) - 3*gw**2*mW**2)*math.sqrt(mb**4 - 2*mb**2*mt**2 + mt**4 - 2*mb**2*mW**2 - 2*mt**2*mW**2 + mW**4))/(96.*math.pi*abs(mt)**3)
    BR_2 = 1/(96*math.pi*mt**3)*(3*(mb**2+mt**2-mhc**2)*(I1a33**2+I2a33**2) - 6*mb*mt*2*I1a33*I2a33)*math.sqrt((mb**2-mhc**2)**2-2*(mb**2+mhc**2)*mt**2+mt**4)

    BR_1 = BR_1
    BR_2 = BR_2

    #From partial widths, the total one is computed and the BRs are obtained.

    WT = BR_1+BR_2 

    BR_1 = BR_1/WT
    BR_2 = BR_2/WT

    #The total width and BRs are added in a dictionary that will be given to Madgraph.

    decays={'6':"""DECAY  6 """ + str(WT) + """ #WT
            #  BR             NDA  ID1    ID2   ...
            """ + str(BR_1) + """   2   5  24 
            """ + str(BR_2) + """   2   5  37 """,
            '-6':"""DECAY  -6 """ + str(WT) + """ #WT
            #  BR             NDA  ID1    ID2   ...
            """ + str(BR_1) + """   2   -5  -24 
            """ + str(BR_2) + """   2   -5  -37 """}

    params['DECAY'] = decays

runName='run_01'

extras = {'nevents'               :int(nevents), # Numebr of events to generate. 
           'lhe_version'           :'3.0',          
           'fixed_ren_scale'       :'F',         # Dynamical factorisation scale.          
           'fixed_fac_scale'       :'F',         # Dynamical factorisation scale.          
           'dynamical_scale_choice':2,           # Sum of the transverse mass as reference scale.  
           'muR_over_ref'          :0.5,         # Renormalization scale is 0.5 times the reference scale.
           'muF1_over_ref'         :1,           # Factorization scale 1 is 1 times the reference scale.
           'muF2_over_ref'         :1,           # Factorization scale 2 is 1 times the reference scale.  
           'parton_shower'         :'PYTHIA8',   # Parton shower Pythia8.
	    } 

#Parameters for madspin.

bwcut = 15

process_dir = new_process(process)

#Run card.

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#Parameters card.

modify_param_card(process_dir=process_dir,params=params)     

#MadSpin card.

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'

mscard = open(madspin_card_loc,'w')

mscard.write("""
    #************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    set BW_cut %i
    set seed %i
    define j = g u c d s b u~ c~ d~ s~ b~
    define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
    """%(bwcut, runArgs.randomSeed))

if("Hminus" in model_pars_str):

    mscard.write("""
    decay t~ > h- b~ 
    decay t > b w+, w+ > wdec wdec
    launch
    """)

else:
    
    mscard.write("""
    decay t > h+ b 
    decay t~ > b~ w-, w- > wdec wdec
    launch
    """)


mscard.close()

print_cards()

#Generate the process.

generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)
        
runArgs.inputGeneratorFile=outputDS
#Pythia 8 for the showering.

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#Use Pythia to simulate the decays of charged Higgs.

if("cb" in model_pars_str):

    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
                                "37:oneChannel = 1 1. 0 4 -5" #Turn off all decays of H+ and switch on H+ to cb.
                            ]

else:
    
    genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
			    			    "37:oneChannel = 1 1. 0 4 -3" #Turn off all decays of H+ and switch on H+ to cs.
						       ]