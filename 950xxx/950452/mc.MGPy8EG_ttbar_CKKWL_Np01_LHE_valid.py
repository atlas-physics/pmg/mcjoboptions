# fixed version of 950176
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*2.5 if runArgs.maxEvents>0 else 2.5*evgenConfig.nEventsPerJob

process_def = """
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > t t~ @0
add process p p > t t~ j @1
output -f"""

process_dir = new_process(process_def)

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version' : '3.0', 
             'cut_decays'  : 'F', 
             'pdlabel'     : "'nn23lo1'",
             'ickkw'       : 0,
             'drjj'        : 0.0,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'use_syst'    : 'False',
             'xqcut'       : 0,
             'pdgs_for_merging_cut' : '1, 2, 3, 4, 5, 21', # Ensure we do not include top quarks here!
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords+=['ttbar','jets']
evgenConfig.contact  = [ "chris.g@cern.ch" ]
evgenConfig.generators = ['MadGraph']

