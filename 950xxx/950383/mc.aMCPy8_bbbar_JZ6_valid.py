evgenConfig.description = "pp->bbar NLO QCD"
evgenConfig.keywords = ["SM","bbbar", "jets"]
evgenConfig.generators = ["aMcAtNlo", "Pythia8", "EvtGen"]
evgenConfig.contact = ["luciakeszeghova@gmail.com", "ana.cueto@cern.ch", "javier.llorente.merino@cern.ch"]

from MadGraphControl.MadGraphUtils import *

# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

# log file efficiency is 1.3% at 13 TeV, 0.7% at 7 TeV
evgenConfig.nEventsPerJob = 500
multiplier = 85

ptjmin = '700.0'
if hasattr(runArgs,'ecmEnergy') and runArgs.ecmEnergy < 13000.:
    ptjmin = '800.0'
    multiplier *= 2

nevents = runArgs.maxEvents*multiplier if runArgs.maxEvents>0 else multiplier*evgenConfig.nEventsPerJob

process="""
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > b b~ [QCD]
output -f"""

process_dir = new_process(process)

#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'nevents':int(nevents),
            'ptj':ptjmin,
            'jetAlgo':'-1',
            'jetRadius':'0.6',
            'maxjetflavor':'5'}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(6, filtSeq)
