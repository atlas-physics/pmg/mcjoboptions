
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
### Shower
evgenConfig.description = 'Pythia8_ttbar'
evgenConfig.keywords+=['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["Pythia8"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1