import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment 
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
gridpack_mode=False

process_def = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > mu+ mu-
output -f"""

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'mmll':40,
             'sde_strategy':1,
             'nevents':int(nevents)}
    
process_dir = new_process(process_def)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
# saveProcDir=True for testing only
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.description = 'MadGraph_Zmumu'
evgenConfig.keywords+=['Z','jets']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
