#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "Herwig7 internal BSM model for Lepto-Quark Pair-Production for validation purposes"
evgenConfig.keywords      = ["BSM", "exotic", "leptoquark"]
evgenConfig.generators    = ['Herwig7']
evgenConfig.contact       = ["lukas.kretschmann@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune          = "H7.2-Default"
#--------------------------------------------------------------
# Setup Herwig in BSM running mode for internal BSM models 
#--------------------------------------------------------------
# Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")

# --------------------------------------------------------------
# Configure generator              
# -------------------------------------------------------------- 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()

# add the BSM model specific commands
Herwig7Config.add_commands("""
##################################################
# Example generator for the Leptoquark model
# in hadron collisions
#
# The first section loads the model file which 
# does not contain anything that users need to touch.
#
# The second section contains the user settings.
#
# Model descriptions can be found here:
# https://herwig.hepforge.org/tutorials/bsm/internal.html
###################################################

# Set emission to POWHEG for radiation in decays
set /Herwig/Shower/ShowerHandler:HardEmission POWHEG

# read model
read Leptoquark.model
cd /Herwig/NewPhysics

##################################################
#
# This section contains the user defined settings
#
##################################################
# Example hard process: Incoming proton, outgoing leptoquarks

insert HPConstructor:Incoming 0 /Herwig/Particles/u
insert HPConstructor:Incoming 1 /Herwig/Particles/ubar
insert HPConstructor:Incoming 2 /Herwig/Particles/d
insert HPConstructor:Incoming 3 /Herwig/Particles/dbar
insert HPConstructor:Incoming 4 /Herwig/Particles/s
insert HPConstructor:Incoming 5 /Herwig/Particles/sbar
insert HPConstructor:Incoming 6 /Herwig/Particles/c
insert HPConstructor:Incoming 7 /Herwig/Particles/cbar
insert HPConstructor:Incoming 8 /Herwig/Particles/g

insert HPConstructor:Outgoing 0 /Herwig/Particles/S0
insert HPConstructor:Outgoing 1 /Herwig/Particles/S0bar

""")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
Herwig7Config.run()
