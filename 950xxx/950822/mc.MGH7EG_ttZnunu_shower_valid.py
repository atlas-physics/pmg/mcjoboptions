#### Shower
evgenConfig.description = 'Herwig7_ttbar'
evgenConfig.keywords += ['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.tune = "H7.2-Default"

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

Herwig7Config.me_pdf_commands(order="NLO",name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
#lhe_filename = outputDS.split(".tar.gz")[0] + ".events"
lhe_filename = "LHE_events.events"
Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename,me_pdf_order="NLO")
include("Herwig7_i/Herwig71_EvtGen.py")
Herwig7Config.run()