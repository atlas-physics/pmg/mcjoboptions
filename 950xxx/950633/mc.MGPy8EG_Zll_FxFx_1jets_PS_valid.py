#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Zll+1Np FxFx from LHE files'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","hannes.mildner@cern.ch"]
evgenConfig.keywords+=['Z','jets']

# General settings
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1

nJetMax=1
qCut=20.

#### Shower: Py8 with A14 Tune, with FxFx prescription
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

