#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "Herwig7 internal BSM model for resonant production of a graviton with the Randall-Sundrum model for a Anti-de-Sitter space time (ADSST) for validation purposes"
evgenConfig.keywords      = ["exotic", "BSM", "RandallSundrum", "warpedED", "graviton", "extraDimensions"]
evgenConfig.generators    = ['Herwig7']
evgenConfig.contact       = ["lukas.kretschmann@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune          = "H7.2-Default"

#--------------------------------------------------------------
# Setup Herwig in BSM running mode for internal BSM models 
#--------------------------------------------------------------
# Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")

# --------------------------------------------------------------
# Configure generator              
# -------------------------------------------------------------- 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()

# add the BSM model specific commands
Herwig7Config.add_commands("""
##################################################
# Example generator for the Randall-Sundrum model
# in hadron collisions
# As with the ADD model in the Randell-Sundrum 
# model only gravity propagates in the extra 
# dimension, however due the warped extra 
# dimension in this case graviton resonances 
# are produced.
#
# The first section loads the model file which 
# does not contain anything that users need to touch.
#
# The second section contains the user settings.
#
# Model descriptions can be found here:
# https://herwig.hepforge.org/tutorials/bsm/internal.html
###################################################

# Set emission to POWHEG for radiation in decays
set /Herwig/Shower/ShowerHandler:HardEmission POWHEG

read RS.model
cd /Herwig/NewPhysics

##################################################
#
# This section contains the user defined settings
#
##################################################
# Example hard process: Incoming proton,resonant graviton

insert ResConstructor:Incoming 0 /Herwig/Particles/g
insert ResConstructor:Incoming 0 /Herwig/Particles/u
insert ResConstructor:Incoming 0 /Herwig/Particles/ubar
insert ResConstructor:Incoming 0 /Herwig/Particles/d
insert ResConstructor:Incoming 0 /Herwig/Particles/dbar

insert ResConstructor:Intermediates 0 /Herwig/Particles/Graviton

insert ResConstructor:Outgoing 0 /Herwig/Particles/e+
insert ResConstructor:Outgoing 1 /Herwig/Particles/W+
insert ResConstructor:Outgoing 2 /Herwig/Particles/Z0
insert ResConstructor:Outgoing 3 /Herwig/Particles/gamma

# coupling
set RS/Model:Lambda_pi 10000*GeV
""")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
Herwig7Config.run()
