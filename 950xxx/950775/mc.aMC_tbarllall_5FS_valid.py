from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *



nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

process = '''
import model loop_qcd_qed_sm
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
set nlo_mixed_expansion False
generate p p > t~ j l+ l- $$ h [QCD] 
output -f
'''


process_dir = new_process(process)

# Write the run_card
settings = {
    'nevents':        nevents,
    'parton_shower':  "PYTHIA8",
    'bwcutoff':50,
    'fixed_ren_scale' : True,
    'fixed_fac_scale' : True,
    'muR_ref_fixed' : 172.5,
    'muF_ref_fixed' : 172.5
}


modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

param_card_settings = {
        'mass' : {
            '5':  "0.000000",
            '6':  "172.5",
            '15': "0",
            '23': "9.118760e+01",
            '24': "8.039900e+01",
            '25': "1.250000e+02",
                    },
        'yukawa' : { '6': "1.725000e+02 # ymt" },
        'DECAY' : {
            '5'  : """DECAY  5   0.000000e+00""",
            '15' : """DECAY  15   0.000000e+00""",
            '23' : """DECAY  23   2.495200e+00""",
            '24': """DECAY  24   2.085000e+00
                3.377000e-01   2   -1   2
                3.377000e-01   2   -3   4
                1.082000e-01   2  -11  12
                1.082000e-01   2  -13  14
                1.082000e-01   2  -15  16""",
                   }
        }
    
modify_param_card(process_dir=process_dir,params=param_card_settings)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
set BW_cut 50                 # cut on how far the particle can be off-shell
set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
# running the actual code
launch"""%(runArgs.randomSeed))
mscard.close()


# Print and generate
print_cards()
generate(process_dir=process_dir,
         grid_pack=False,
         gridpack_compile=False,
         required_accuracy=0.005,
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=3)


#### Shower: Py8 with A14 Tune and new MEC
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


# Metadata
evgenConfig.description      = 'aMcAtNlo_tZ'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['dominic.hirschbuehl@cern.ch']
evgenConfig.generators       = ['aMcAtNlo']


