evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, single lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with splitting kernel shower weights'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk', 'mdshapiro@lbl.gov', 'm.fenton@cern.ch']
##evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

# CMS REF: https://cmssdt.cern.ch/lxr/source/Configuration/Generator/python/PSweightsPythia/PythiaPSweightsSettings_cfi.py?v=CMSSW_11_2_1

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# New Shower Weights which need to overwrite the existing ones so duplicate for now
# Var3c - A14 tune variation
# Red - reduced sqrt(2)
# Def - default 2
# Con - conservative 4
# Splitting with default 2
# \- g2gg, g2qq, q2qg, x2xg; where x is b,t or heavier if nFlavQ = 4
# cNS - non-singular terms, relevant if no matrix element correction (MEC)
genSeq.Pythia8.Commands += ["UncertaintyBands:List = {\
Var3cUp isr:muRfac=0.549241,\
Var3cDown isr:muRfac=1.960832,\
isr:PDF:plus=1,\
isr:PDF:minus=2,\
isrRedHi isr:muRfac=0.707,\
fsrRedHi fsr:muRfac=0.707,\
isrRedLo isr:muRfac=1.414,\
fsrRedLo fsr:muRfac=1.414,\
isrDefHi isr:muRfac=0.5,\
fsrDefHi fsr:muRfac=0.5,\
isrDefLo isr:muRfac=2.0,\
fsrDefLo fsr:muRfac=2.0,\
isrConHi isr:muRfac=0.25,\
fsrConHi fsr:muRfac=0.25,\
isrConLo isr:muRfac=4.0,\
fsrConLo fsr:muRfac=4.0,\
isr_cNS_up isr:cNS=2.0,\
isr_cNS_dn isr:cNS=-2.0,\
fsr_cNS_up fsr:cNS=2.0,\
fsr_cNS_dn fsr:cNS=-2.0,\
fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,\
fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,\
fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,\
fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,\
fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,\
fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,\
fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,\
fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,\
fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,\
fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,\
fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,\
fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,\
fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,\
fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,\
fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,\
fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,\
isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,\
isr_G2GG_muR_up isr:G2GG:muRfac=2.0,\
isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,\
isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,\
isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,\
isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,\
isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,\
isr_X2XG_muR_up isr:X2XG:muRfac=2.0,\
isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,\
isr_G2GG_cNS_up isr:G2GG:cNS=2.0,\
isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,\
isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,\
isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,\
isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,\
isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,\
isr_X2XG_cNS_up isr:X2XG:cNS=2.0\
}"]

genSeq.Pythia8.ShowerWeightNames = ["Var3cUp",
                                    "Var3cDown",
                                    "isr:PDF:plus",
                                    "isr:PDF:minus",
                                    "isrRedHi", 
                                    "fsrRedHi", 
                                    "isrRedLo", 
                                    "fsrRedLo", 
                                    "isrDefHi", 
                                    "fsrDefHi", 
                                    "isrDefLo", 
                                    "fsrDefLo", 
                                    "isrConHi", 
                                    "fsrConHi", 
                                    "isrConLo", 
                                    "fsrConLo", 
                                    "isr_cNS_up",
                                    "isr_cNS_dn",
                                    "fsr_cNS_up",                                    
                                    "fsr_cNS_dn",
                                    "fsr_G2GG_muR_dn", 
                                    "fsr_G2GG_muR_up", 
                                    "fsr_G2QQ_muR_dn", 
                                    "fsr_G2QQ_muR_up", 
                                    "fsr_Q2QG_muR_dn", 
                                    "fsr_Q2QG_muR_up", 
                                    "fsr_X2XG_muR_dn", 
                                    "fsr_X2XG_muR_up", 
                                    "fsr_G2GG_cNS_dn", 
                                    "fsr_G2GG_cNS_up", 
                                    "fsr_G2QQ_cNS_dn", 
                                    "fsr_G2QQ_cNS_up", 
                                    "fsr_Q2QG_cNS_dn", 
                                    "fsr_Q2QG_cNS_up", 
                                    "fsr_X2XG_cNS_dn", 
                                    "fsr_X2XG_cNS_up", 
                                    "isr_G2GG_muR_dn", 
                                    "isr_G2GG_muR_up", 
                                    "isr_G2QQ_muR_dn", 
                                    "isr_G2QQ_muR_up", 
                                    "isr_Q2QG_muR_dn", 
                                    "isr_Q2QG_muR_up", 
                                    "isr_X2XG_muR_dn", 
                                    "isr_X2XG_muR_up", 
                                    "isr_G2GG_cNS_dn", 
                                    "isr_G2GG_cNS_up", 
                                    "isr_G2QQ_cNS_dn", 
                                    "isr_G2QQ_cNS_up", 
                                    "isr_Q2QG_cNS_dn", 
                                    "isr_Q2QG_cNS_up", 
                                    "isr_X2XG_cNS_dn", 
                                    "isr_X2XG_cNS_up"]

# New settings
genSeq.Pythia8.Commands += ['UncertaintyBands:nFlavQ = 4', # define X=bottom/top in X2XG variations
                            'UncertaintyBands:MPIshowers = on',
                            'UncertaintyBands:overSampleFSR = 10.0',
                            'UncertaintyBands:overSampleISR = 10.0',
                            'UncertaintyBands:FSRpTmin2Fac = 20',
                            'UncertaintyBands:ISRpTmin2Fac = 1']


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
