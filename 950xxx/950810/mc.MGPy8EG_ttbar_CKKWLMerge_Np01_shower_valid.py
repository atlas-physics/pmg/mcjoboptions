#### Shower 
settings = { 'lhe_version' : '3.0',
             'cut_decays'  : 'F',
             'ickkw'       : 0,
             'drjj'        : 0.0,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'xqcut'       : 0,
             'pdgs_for_merging_cut' : '1, 2, 3, 4, 5, 21' # Ensure we do not include top quarks here!
}

evgenConfig.description = 'Pythia_ttbar'
evgenConfig.keywords += ['ttbar','jets']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["Pythia8"]

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

PYTHIA8_nJetMax=1
PYTHIA8_Process='pp>tt~'
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
#Needed for ttbar:
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
