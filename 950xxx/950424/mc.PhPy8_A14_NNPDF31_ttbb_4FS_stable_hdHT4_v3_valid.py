evgenConfig.description = "POWHEG-BOX-RES/OpenLoops+Pythia8+EvtGen ttbb (4FS), mur=1/2*[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], stable tops, bornzerodamp=5, hdamp=HT/4, A14 NNPDF23 LO tune"
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'bbbar', '1lepton']
evgenConfig.contact = ["lars.ferencz@cern.ch"]
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob = 1000
del testSeq.TestHepMC

#--------------------------------------------------------------
# Powheg
#--------------------------------------------------------------
# Load ATLAS defaults for the Powheg ttbb process
include('PowhegControl/PowhegControl_ttbb_Common.py')

### integration was done with ATHENA_PROC_NUMBER=10
PowhegConfig.ncall1       = "400000"
PowhegConfig.ncall2       = "400000"
PowhegConfig.ncall2rm     = "600000"
PowhegConfig.nubound      = "1000000"

### muR/muF recommendation from LHC Higgs WG
PowhegConfig.runningscales = 3

### bornzerodamp
PowhegConfig.bornzerodampcut = 5 

### hdamp settings
PowhegConfig.hdamp      = -1   # fixed hdamp off
PowhegConfig.dynhdamp   = 1    # 0=off, 1=on
PowhegConfig.dynhdampPF = 0.25 # prefactor of dynhdamp, typically 0.25, 0.5 or 1.0

### Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = "5"
PowhegConfig.foldphi      = "5"
PowhegConfig.foldy        = "5"
PowhegConfig.for_reweighting = 1

### decay handled by PowHeg
PowhegConfig.decay_mode   = "t t~ > undecayed"

#Dynamical scale variations
PowhegConfig.mu_F         = [ 1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0, 0.25, 0.5,  0.25, 0.25, 1.0  ] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [ 1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5, 0.5,  0.25, 0.25, 1.0,  0.25 ] # List of renormalisation scales


### Set specific PDF set
PowhegConfig.PDF          = [320900, 260400, 13191, 25410, 92000, 265400, 266400, 320500 ]


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
### Stable top setup
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ] 
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
genSeq.Pythia8.Commands += [ '6:mayDecay = off']
genSeq.Pythia8.Commands += [ '-6:mayDecay = off']
genSeq.Pythia8.Commands += [ 'HadronLevel:all = off']
genSeq.Pythia8.Commands += [ 'PartonLevel:MPI = off']
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = off']
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByQ = off']
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByGamma = off']
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByL = off']
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByOther = off']
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByQ = off']
genSeq.Pythia8.Commands += [ '5:m0 = 4.75']
genSeq.Pythia8.Commands += [ '-5:m0 = 4.75']
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2']
genSeq.Pythia8.Commands += [ 'TimeShower:pTmaxMatch = 2']

