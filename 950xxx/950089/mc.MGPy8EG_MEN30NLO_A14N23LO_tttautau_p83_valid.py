import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment 
from MadGraphControl.MadGraphUtils import *

# General settings
evgenConfig.nEventsPerJob = 1000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
gridpack_mode=False

# Z decay setting
zdecay="decay z > all all"

process_def = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > t t~ ta+ ta- $$ h [QCD]
output -f
"""
process_dir = new_process(process_def)

#Fetch default LO run_card.dat and set parameters
settings = { 'maxjetflavor'  : 5,
             'parton_shower' : 'PYTHIA8',
             'ptgmin'        : 20,
             'mll_sf'        : 5}

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'

mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
define q = u d s c b
define q~ = u~ d~ s~ c~ b~
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
%s
# running the actual code
launch"""%(runArgs.randomSeed,zdecay))
mscard.close()

# set shower starting scale to old scale s hat
mccounter_path=os.path.join(process_dir, 'SubProcesses/montecarlocounter.f')
mccounter_file=open(mccounter_path,'r')
mccounter_content=mccounter_file.read().replace('parameter(i_scale=1)','parameter(i_scale=0)')
mccounter_file.close()
mccounter_file=open(mccounter_path,'w')
mccounter_file.write(mccounter_content)
mccounter_file.close()

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Set the W decays explicitly
param_settings = {'DECAY':{
  '24':"""DECAY  24   2.085000e+00                                                
#  BR             NDA  ID1    ID2   ...                                 
      3.337000e-01   2   -1   2                                         
      3.337000e-01   2   -3   4                                         
      1.082000e-01   2  -11  12                                         
      1.082000e-01   2  -13  14                                         
      1.082000e-01   2  -15  16                                         
"""} }
modify_param_card(process_dir=process_dir,params=param_settings)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode,gridpack_compile=True)
# saveProcDir=True only for debugging
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower
evgenConfig.description = 'aMcAtNlo_tttautau_NLO'
evgenConfig.keywords+=['SM','ttZ','2lepton']
evgenConfig.contact = ['mcfayden@cern.ch', 'ponyisi@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Reset to serial processing
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_aMcAtNlo.py")
