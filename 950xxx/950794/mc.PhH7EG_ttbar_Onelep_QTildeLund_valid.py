#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig73 ttbar single lepton production with QTilde/angular ordered shower and lund hadronisation'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact = ["betsy.may.cunnett@cern.ch, joshua.angus.mcfayden@cern.ch"] 
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1
evgenConfig.tune = "H7-Default"



#--------------------------------------------------------------                                                                                                                                          
# Herwig7 showering                                                                                                                                                                               
#--------------------------------------------------------------                                                                                                                                         
   
# initialize Herwig7 generator configuration for showering of LHE files                                                                                                                                  
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7                                                                                                                                                                                      
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                                                                             
include("Herwig7_i/Herwig7_EvtGen.py")

#String hadronisation  
os.environ['THEP8I_PATH'] = '/cvmfs/atlas.cern.ch/repo/sw/software/23.6/sw/lcg/releases/LCG_104c_ATLAS_5/MCGenerators/thep8i'
os.environ['PYTHIA8DATA']=os.environ['PY8PATH']+'/share/Pythia8/xmldoc'
include("Herwig7_i/Herwig7_TheP8I.py")


# run Herwig7                                                                                                                                                                                            
Herwig7Config.run()



#--------------------------------------------------------------                                                                                                                                        
# Event filter                                                                                                                                                                                           
#--------------------------------------------------------------                                                                                                                                          
  
include('GeneratorFilters/xAODTTbarWToLeptonFilter_Common.py')
filtSeq.xAODTTbarWToLeptonFilter_Common.NumLeptons = 1
filtSeq.xAODTTbarWToLeptonFilter_Common.Ptcut = 0.
