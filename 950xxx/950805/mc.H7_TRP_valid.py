#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "Herwig7 internal BSM model for transplankian scattering for validation purposes"
evgenConfig.keywords      = ["exotic", "BSM", "graviton", "extraDimensions"]
evgenConfig.generators    = ['Herwig7']
evgenConfig.contact       = ["lukas.kretschmann@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune          = "H7.2-Default"

#--------------------------------------------------------------
# Setup Herwig in BSM running mode for internal BSM models 
#--------------------------------------------------------------
# Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")

# --------------------------------------------------------------
# Configure generator              
# -------------------------------------------------------------- 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()

# add the BSM model specific commands
Herwig7Config.add_commands("""
##################################################
# Example generator based on LHC parameters
# A model for transplankian scattering which 
# only requireas a new MatrixElement for the 
# scattering process.
#
# Model descriptions can be found here:
# https://herwig.hepforge.org/tutorials/bsm/internal.html
##################################################

cd /Herwig/Particles
create ThePEG::ParticleData graviton
setup graviton 39 graviton 0.0 0.0 0.0 0.0 0 0 0 1
                                        
##################################################
# Matrix Elements for hadron-hadron collisions 
##################################################
cd /Herwig/MatrixElements/
create Herwig::METRP2to2 METransplanck HwTransplanck.so

insert SubProcess:MatrixElements[0] METransplanck
#set METransplanck:Process 6
set METransplanck:NumberExtraDimensions 6
set METransplanck:PlanckMass 1500
""")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
Herwig7Config.run()
