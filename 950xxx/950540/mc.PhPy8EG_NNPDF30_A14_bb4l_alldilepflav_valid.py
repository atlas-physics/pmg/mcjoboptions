import os
os.environ["POWHEGPATH"] = os.getcwd()

evgenConfig.generators   += ["Powheg","Pythia8","EvtGen"]
evgenConfig.description   = 'Powheg bb4l test production with A14 tune, NNPDF30 PDF, really all flavour dilep combinations, mtop=172.5 GeV, Gammatop=1.32 GeV'
evgenConfig.keywords     += [ 'SM', 'top', 'WW', 'lepton']
evgenConfig.contact       = [ 'Simone Amoroso <simone.amoroso@cern.ch>', 'jan.kretzschmar@cern.ch' ]
evgenConfig.nEventsPerJob = 2000

# -------------------- # Load ATLAS defaults for the Powheg bblvlv process # ----------------------------
include("PowhegControl/PowhegControl_bblvlv_Common.py")
PowhegConfig.width_t         = 1.32
PowhegConfig.twidth_phsp     = 1.32
PowhegConfig.tmass_phsp      = 172.5
PowhegConfig.mass_t          = 172.5
PowhegConfig.hdamp           = 258.75

# enabling for_reweighting=1 will speed up event generation at expense of some spread in weights
PowhegConfig.for_reweighting = 0

### PDFs as in main hvq sample + NNPDF3.0 replica + a few newer ones
PowhegConfig.PDF             = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
# save on weight calculation
#PowhegConfig.PDF.extend(range(260001, 260101))         # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend([303600, 14400, 14000, 14200, 27100, 27400, 331500,331100, 93300]) # NNPDF31_nnlo_as_0118, CT18NLO, CT18NNLO, CT18ANNLO, MSHT20nlo_as118, MSHT20nnlo_as118, NNPDF40_nnlo_as_01180_hessian, NNPDF40_nlo_as_01180, PDF4LHC21

## enable all different-flavour lepton combinations
## will only work in new release >=21.6.74
PowhegConfig.decay_mode = "b l+ vl b~ l- vl~"

### Optimised fold parameters: (2,2,2) gives 3.9% negative weights
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 2
PowhegConfig.foldy        = 2

### turn up integration parameters
PowhegConfig.ncall1       = 120000*2
PowhegConfig.ncall2       = 180000*2
PowhegConfig.nubound      = 100000*2
PowhegConfig.itmx1        = 2
PowhegConfig.itmx2        = 8
PowhegConfig.xupbound     = 3.

PowhegConfig.nEvents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.1

# --------------------- # Generate events # --------------------------------------------------------------
PowhegConfig.generate()


################################################################
#                      adding UserHook                         #
################################################################         
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleFSR=10' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:FSRpTmin2Fac=8' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleISR=10' ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print ('UserHook present')
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ltms']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"]
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"]

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:onlyDistance1 = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoAtPL = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:dryRunFSR = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:veto = 0"]
#genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:excludeFSRConflicting = false"]                                                                                                                              
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 0.8"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:DEBUG = 0"]

