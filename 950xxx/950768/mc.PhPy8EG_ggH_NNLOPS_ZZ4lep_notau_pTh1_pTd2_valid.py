#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "ggH H->ZZ->llll"
evgenConfig.description      = 'POWHEG+Pythia8 ggF NNLOPS production with A14 NNPDF2.3 tune'
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125"  ]
evgenConfig.contact          = [ 'sabidi@cern.ch', 'xinmeng.ye@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen'  ]
evgenConfig.inputFilesPerJob = 20
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 1'  ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3'  ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2'  ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1'  ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3'  ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0'  ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0'  ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0'  ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13']
