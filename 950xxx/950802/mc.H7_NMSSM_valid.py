#--------------------------------------------------------------
# Import generic modules
#--------------------------------------------------------------
import os

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "Herwig7 internal BSM model for next to Minimal Supersymmetric SM for validation purposes"
evgenConfig.keywords      = ["SUSY"]
evgenConfig.generators    = ['Herwig7']
evgenConfig.contact       = ["lukas.kretschmann@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune          = "H7.2-Default"
#--------------------------------------------------------------
# Setup Herwig in BSM running mode for internal BSM models 
#--------------------------------------------------------------
# Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")

# --------------------------------------------------------------
# Configure generator              
# -------------------------------------------------------------- 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()

# add the BSM model specific commands
Herwig7Config.add_commands("""
##################################################
# Example generator for the NMSSM
# The Next-to-Minimal Supersymmetric Standard
# Model (NMSSM) includes an additional Higgs 
# singlet field. The implementation inherits from 
# that for the MSSM.
#
# The first section loads the model file which 
# does not contain anything that users need to touch.
#
# The second section contains the user settings.
#
# Model descriptions can be found here:
# https://herwig.hepforge.org/tutorials/bsm/internal.html
###################################################

# Set emission to POWHEG for radiation in decays
set /Herwig/Shower/ShowerHandler:HardEmission POWHEG

# read model
read NMSSM.model
cd /Herwig/NewPhysics

##################################################
#
# This section contains the user defined settings
#
##################################################
# --- Hard Process ----
# The particle name can be found in the relevant model file
# by searching for its PDG code and noting the text 
# '/Herwig/Particles/###' where the hashes denote the name

# Switch to decide whether to include EW diagrams in the 
# hard process (On by default)
set HPConstructor:IncludeEW No

# Example hard process: Incoming proton, outgoing squarks
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar

insert HPConstructor:Outgoing 0 /Herwig/Particles/~u_L
insert HPConstructor:Outgoing 1 /Herwig/Particles/~u_Lbar
insert HPConstructor:Outgoing 2 /Herwig/Particles/~d_L
insert HPConstructor:Outgoing 3 /Herwig/Particles/~d_Lbar
""")

# Get the correct file-path for the spectrum file
path = os.path.join(os.environ['HERWIG7_PATH'], 'share/Herwig')

Herwig7Config.add_commands(f"""
# --- Perturbative Decays ---
# Read in the spectrum file and optional decay table.
# If a decay table is in a separate file
# then add another 'setup' line with that
# file as the argument. The provided
# spectrum file is an example using NMHDecay-1.2.1
setup NMSSM/Model {path}/NMSSM.spc
""")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
Herwig7Config.run()
