import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords += ['ttbar']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators = ["MadGraph"]

evgenConfig.nEventsPerJob = 10000

nevents = runArgs.maxEvents*2.5 if runArgs.maxEvents>0 else 2.5*evgenConfig.nEventsPerJob

process_def = """
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > t t~ @0
add process p p > t t~ j @1
output -f"""

process_dir = new_process(process_def)

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version' : '3.0', 
             'cut_decays'  : 'F', 
             'ickkw'       : 0,
             'drjj'        : 0.0,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'xqcut'       : 0,
             'pdgs_for_merging_cut' : '1, 2, 3, 4, 5, 21', # Ensure we do not include top quarks here!
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True only for local testing!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Reset to serial processing
check_reset_proc_number(opts)