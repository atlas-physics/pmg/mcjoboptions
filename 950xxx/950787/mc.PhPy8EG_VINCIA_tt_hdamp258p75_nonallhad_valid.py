evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, at least one lepton filter, ME NNPDF30 NLO, VINCIA shower, DSID 410450 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch','m.fenton@cern.ch']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ]

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


genSeq.Pythia8.Commands += ['PartonShowers:model = 2']
genSeq.Pythia8.Commands += ['Vincia:pTmaxMatch = 2']  # recommended in https://arxiv.org/pdf/2106.10987.pdf
genSeq.Pythia8.Commands += ['Vincia:ewMode = 0']
genSeq.Pythia8.Commands += ['Vincia:interleaveResDec= on']
genSeq.Pythia8.Commands += ['Vincia:helicityShower = off']

genSeq.Pythia8.Commands += ['BeamRemnants:primordialKThard    = 0.4']
genSeq.Pythia8.Commands += ['ColourReconnection:range           = 1.75']

genSeq.Pythia8.Commands += ['MultipartonInteractions:alphaSvalue = 0.119']

genSeq.Pythia8.Commands += ['MultipartonInteractions:pT0Ref         = 2.24']
genSeq.Pythia8.Commands += ['SigmaProcess:alphaSvalue              = 0.119']

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
