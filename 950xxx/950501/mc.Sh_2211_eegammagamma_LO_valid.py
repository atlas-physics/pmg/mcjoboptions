include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa eegammagamma + 0,1,2j@LO."
evgenConfig.keywords = ["SM", "2electron", "2photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=2; LJET:=0; QCUT:=20;

  % ME generator settings
  ME_SIGNAL_GENERATOR Comix Amegic;

  # EW corrections setup
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

}(run)

(processes){
  Process 93 93 -> 22 22 11 -11 93{NJET}
  Order (*,4); CKKW sqr(QCUT/E_CMS)
  Enhance_Function VAR{(max(PPerp(p[4]),PPerp(p[5])))/400.0}
  PSI_ItMin 20000 {4}
  PSI_ItMin 20000 {5}
  Integration_Error 0.99 {5}
  PSI_ItMin 50000 {6}
  Integration_Error 0.99 {6.7}
  End process
}(processes)

(selector){
  "PT"  22  7.0,E_CMS:7.0,E_CMS [PT_UP]
  "DR"  22,90  0.1,1000:0.1,1000:0.1,1000:0.1,1000
  IsolationCut  22  0.1  2  0.10
  Mass  11  -11  10.0  E_CMS
}(selector)
"""
genSeq.Sherpa_i.NCores = 128
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

