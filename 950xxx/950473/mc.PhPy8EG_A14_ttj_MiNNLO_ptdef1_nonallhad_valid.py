# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 tt+jet production with MiNNLO and A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "top", "1jet"]
evgenConfig.contact = ["dominic.hirschbuehl@cern.ch,tpelzer@cern.ch"]
evgenConfig.nEventsPerJob = 5000

if not os.path.exists("/afs/cern.ch/atlas/offline/external/powhegbox"):
    os.environ['OpenLoopsPath'] = os.environ['POWHEGPATH'] + "/External/OpenLoops-2.1.1"


# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg ttj_MiNNLO process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_ttj_MiNNLO_Common.py")

# --------------------------------------------------------------
# Settings
# --------------------------------------------------------------
PowhegConfig.PDF   = 261000 

# define the decay mode
PowhegConfig.decay_mode = "t t~ > all [MadSpin]"
PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > all all", "decay t~ > w- b~, w- > all all"]

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.nEvents     *= 3. # compensate filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

