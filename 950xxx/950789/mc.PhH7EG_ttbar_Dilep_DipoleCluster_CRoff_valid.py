#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig73 ttbar dilepton production with dipole shower and cluster hadronisation with colour reconnection off'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact = ["betsy.may.cunnett@cern.ch, joshua.angus.mcfayden@cern.ch"] 
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1
evgenConfig.tune =  "H7-Default"

#--------------------------------------------------------------                                                                                                                                          
# Herwig7 showering                                                                                                                                                                               
#--------------------------------------------------------------                                                                                                                                         
   
# initialize Herwig7 generator configuration for showering of LHE files                                                                                                                                  
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7                                                                                                                                                                                      
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                                                                             
include("Herwig7_i/Herwig7_EvtGen.py")


# turn CR off
Herwig7Config.add_commands("""
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.0
""")


# Dipole shower

command = """
set /Herwig/Generators/EventGenerator:EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
read snippets/DipoleShowerFiveFlavours.in
set /Herwig/DipoleShower/DipoleShowerHandler:PowhegDecayEmission No



cd /Herwig/EventHandlers
insert /Herwig/DipoleShower/DipoleShowerHandler:OffShellInShower 0 6
insert /Herwig/DipoleShower/DipoleShowerHandler:OffShellInShower 1 5



"""
Herwig7Config.add_commands(command) 

# run Herwig7                                                                                                                                                                                            
Herwig7Config.run()



#--------------------------------------------------------------                                                                                                                                        
# Event filter                                                                                                                                                                                           
#--------------------------------------------------------------                                                                                                                                          
  
include('GeneratorFilters/xAODTTbarWToLeptonFilter_Common.py')
filtSeq.xAODTTbarWToLeptonFilter_Common.NumLeptons = 2
filtSeq.xAODTTbarWToLeptonFilter_Common.Ptcut = 0.

