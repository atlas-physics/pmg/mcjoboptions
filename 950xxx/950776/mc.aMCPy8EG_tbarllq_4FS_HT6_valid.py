from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

import fileinput

nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

process = '''
import model loop_qcd_qed_sm-with_b_mass
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
set nlo_mixed_expansion False
generate p p > t~ b j l+ l- $$ w+ w- h [QCD]
output -f
'''


process_dir = new_process(process)

# Write the run_card
settings = {
    'nevents':        nevents,
    'parton_shower':  "PYTHIA8",
    'bwcutoff':50,
    'ptgmin':         20,
    'mll_sf':         5,
    'dynamical_scale_choice' : 10,
}


print("CUSTOM SCALE SETTINGS: SET TO HT/6")
dyn_scale_fact = 1.0
fileN = process_dir+'/SubProcesses/setscales.f'
mark = '      elseif(dynamical_scale_choice.eq.10.or.dynamical_scale_choice.eq.0) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 10                                   cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           '        write(*,*) "User-defined scale not set"',
           '        stop 1',
           '        temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           '        tmp = 0d0']
flag=0
for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
            toKeep = False
            break
    if toKeep:
        print(line),
    if line.startswith(mark) and flag==0:
        flag +=1
        print("""
c         sum of the transverse mass divide by 6
c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
          tmp=0d0
          do i=3,nexternal
            if ( (idup(i,1) .ge. 5) .or. (idup(i,1) .le. -5) ) then
              tmp=tmp+dsqrt(max(0d0,(pp(0,i)+pp(3,i))*(pp(0,i)-pp(3,i))))
            endif
          enddo
          tmp=tmp/6d0
          temp_scale_id='H_T/6 := sum_i mT(i)/6, i=t,H,b'
        """)
    if line.startswith("      common/ctemp_scale_id/temp_scale_id"):
        print("""
           integer idup(nexternal,maxproc)
           common /c_leshouche_inc/idup
        """)


modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)



param_card_settings = {
        'mass' : {
            '5':  "4.950000e+00",
            '6':  "172.5",
            '15': "0",
            '23': "9.118760e+01",
            '24': "8.039900e+01",
            '25': "1.250000e+02",
                    },
        'yukawa' : { '6': "1.725000e+02 # ymt" },
        'DECAY' : {
            '5'  : """DECAY  5   0.000000e+00""",
            '15' : """DECAY  15   0.000000e+00""",
            '23' : """DECAY  23   2.495200e+00""",
            '24': """DECAY  24   2.085000e+00
                3.377000e-01   2   -1   2
                3.377000e-01   2   -3   4
                1.082000e-01   2  -11  12
                1.082000e-01   2  -13  14
                1.082000e-01   2  -15  16""",
                   }
        }

modify_param_card(process_dir=process_dir,params=param_card_settings)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
set BW_cut 50                 # cut on how far the particle can be off-shell
set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
# running the actual code
launch"""%(runArgs.randomSeed))
mscard.close()


# Print and generate
print_cards()
generate(process_dir=process_dir,
         grid_pack=False,
         gridpack_compile=False,
         required_accuracy=0.0005,
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=3)

#### Shower: Py8 with A14 Tune and new MEC
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


# Metadata
evgenConfig.description      = 'aMcAtNlo_tbarllq with HT/6 scale and new MEC'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['dominic.hirschbuehl@cern.ch']
evgenConfig.generators       = ['aMcAtNlo']



