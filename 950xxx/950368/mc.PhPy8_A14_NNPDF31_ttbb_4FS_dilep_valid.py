evgenConfig.description = "POWHEG-BOX-RES/OpenLoops+Pythia8+EvtGen ttbb (4FS), mur=1/2*[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], dileptonic channel, hdamp=HT/2, bornzerodamp=5,A14 NNPDF23 LO tune, decays with Powheg"
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'bbbar', '2lepton']
evgenConfig.contact = ["lars.ferencz@cern.ch"]
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.nEventsPerJob = 1000


#--------------------------------------------------------------
# Powheg
#--------------------------------------------------------------
# Load ATLAS defaults for the Powheg ttbb process
include('PowhegControl/PowhegControl_ttbb_Common.py')

### integration was done with ATHENA_PROC_NUMBER=10
PowhegConfig.ncall1       = "200000"
PowhegConfig.ncall2       = "200000"
PowhegConfig.ncall2rm     = "400000"
PowhegConfig.nubound      = "10000"

### muR/muF recommendation from LHC Higgs WG
PowhegConfig.runningscales = 3
PowhegConfig.bornzerodamp = 5
PowhegConfig.for_reweighting = 1
### Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = "5"
PowhegConfig.foldphi      = "5"
PowhegConfig.foldy        = "5"

### decay mode handled by Powheg
PowhegConfig.decay_mode   = "t t~ > b l+ vl b~ l- vl~"

#Dynamical scale variations
PowhegConfig.mu_F         = [ 1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0, 0.25, 0.5,  0.25, 0.25, 1.0  ] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [ 1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5, 0.5,  0.25, 0.25, 1.0,  0.25 ] # List of renormalisation scales



### Set specific PDF set
PowhegConfig.PDF          = [320900, 260400, 13191, 25410, 92000, 265400, 266400, 320500 ]

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
### Change pTdef value
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 1' ] 
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
