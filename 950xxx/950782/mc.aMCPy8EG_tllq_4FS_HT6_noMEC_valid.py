# Metadata
evgenConfig.description      = 'aMcAtNlo_tllq with HT/6 scale and no MEC'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['dominic.hirschbuehl@cern.ch']
evgenConfig.generators       = ['aMcAtNlo']
evgenConfig.nEventsPerJob    = 20000
evgenConfig.inputFilesPerJob = 2

#### Shower: Py8 with A14 Tune 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")



