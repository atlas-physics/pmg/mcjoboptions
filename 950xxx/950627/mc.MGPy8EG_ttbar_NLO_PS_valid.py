### Metadata
evgenConfig.description = 'MG5_aMC@NLO+MadSpin+Pythia8+EvtGen ttbar dilepton production from LHE files with shower weights added'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.keywords+=['ttbar','jets']
evgenConfig.contact  = ["jens.roggel@cern.ch" ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
