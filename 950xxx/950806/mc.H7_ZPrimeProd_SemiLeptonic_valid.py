#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "Herwig7 internal BSM model for ZPrime to ttbar production (semi-leptonic) for validation purposes"
evgenConfig.keywords      = ["BSM","Zprime","resonance","ttbar"]
evgenConfig.generators    = ['Herwig7']
evgenConfig.contact       = ["lukas.kretschmann@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune          = "H7-Default"

#--------------------------------------------------------------
# Setup Herwig in BSM running mode for internal BSM models 
#--------------------------------------------------------------
# Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")

# --------------------------------------------------------------
# Configure generator              
# -------------------------------------------------------------- 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()

# add the BSM model specific commands
Herwig7Config.add_commands("""
##################################################
# Example generator for the Z' model
# in hadron collisions
# A model including a Z′ boson.
#
# The first section loads the model file which 
# does not contain anything that users need to touch.
#
# The second section contains the user settings.
#
# Model descriptions can be found here:
# https://herwig.hepforge.org/tutorials/bsm/internal.html
###################################################

# Set emission to POWHEG for radiation in decays
set /Herwig/Shower/ShowerHandler:HardEmission POWHEG

# read model
read Zprime.model

set /Herwig/Particles/t:Synchronized Not_synchronized
set /Herwig/Particles/tbar:Synchronized Not_synchronized

# Here we select the different top-decay modes
# In this JO we enable a semi-leptonic decay
set /Herwig/Particles/t/t->b,bbar,c;:Active No
set /Herwig/Particles/t/t->b,c,dbar;:Active No
set /Herwig/Particles/t/t->b,c,sbar;:Active No
set /Herwig/Particles/t/t->b,sbar,u;:Active No
set /Herwig/Particles/t/t->b,u,dbar;:Active No
set /Herwig/Particles/t/t->nu_e,e+,b;:Active Yes
set /Herwig/Particles/t/t->nu_mu,mu+,b;:Active Yes
set /Herwig/Particles/t/t->nu_tau,tau+,b;:Active Yes
set /Herwig/Particles/tbar/tbar->b,bbar,cbar;:Active Yes
set /Herwig/Particles/tbar/tbar->bbar,cbar,d;:Active Yes
set /Herwig/Particles/tbar/tbar->bbar,cbar,s;:Active Yes
set /Herwig/Particles/tbar/tbar->bbar,s,ubar;:Active Yes
set /Herwig/Particles/tbar/tbar->bbar,ubar,d;:Active Yes
set /Herwig/Particles/tbar/tbar->nu_ebar,e-,bbar;:Active No
set /Herwig/Particles/tbar/tbar->nu_mubar,mu-,bbar;:Active No
set /Herwig/Particles/tbar/tbar->nu_taubar,tau-,bbar;:Active No

##################################################
#
# This section contains the user defined settings
#
##################################################
# Example hard process: Incoming proton, outgoing leptoquarks
cd /Herwig/NewPhysics

set HPConstructor:Processes Exclusive

insert HPConstructor:Incoming 0 /Herwig/Particles/u
insert HPConstructor:Incoming 1 /Herwig/Particles/ubar
insert HPConstructor:Incoming 2 /Herwig/Particles/d
insert HPConstructor:Incoming 3 /Herwig/Particles/dbar
insert HPConstructor:Incoming 4 /Herwig/Particles/s
insert HPConstructor:Incoming 5 /Herwig/Particles/sbar
insert HPConstructor:Incoming 6 /Herwig/Particles/c
insert HPConstructor:Incoming 7 /Herwig/Particles/cbar
insert HPConstructor:Incoming 8 /Herwig/Particles/g

# Comment out the following line if Axigluon model is selected

insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/g
insert /Herwig/NewPhysics/HPConstructor:Excluded 1 /Herwig/Particles/t
insert /Herwig/NewPhysics/HPConstructor:Excluded 2 /Herwig/Particles/tbar


insert HPConstructor:Outgoing 0 /Herwig/Particles/t
insert HPConstructor:Outgoing 1 /Herwig/Particles/tbar

""")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
Herwig7Config.run()
