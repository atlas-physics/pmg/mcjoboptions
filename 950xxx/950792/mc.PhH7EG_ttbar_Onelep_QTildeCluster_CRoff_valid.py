#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig73 ttbar single lepton production with QTilde/angular ordered shower and cluster hadronisation with colour reconnection off'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact = ["betsy.may.cunnett@cern.ch, joshua.angus.mcfayden@cern.ch"] 
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 1
evgenConfig.tune = "H7-Default"


#--------------------------------------------------------------                                                                                                                                          
# Herwig7 showering                                                                                                                                                                               
#--------------------------------------------------------------                                                                                                                                         
   
# initialize Herwig7 generator configuration for showering of LHE files                                                                                                                                  
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7                                                                                                                                                                                      
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# add EvtGen                                                                                                                                                                                             
include("Herwig7_i/Herwig7_EvtGen.py")

# turn CR off
Herwig7Config.add_commands("""
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.0
""")

# run Herwig7                                                                                                                                                                                            
Herwig7Config.run()




#--------------------------------------------------------------                                                                                                                                        
# Event filter                                                                                                                                                                                           
#--------------------------------------------------------------                                                                                                                                          
  
include('GeneratorFilters/xAODTTbarWToLeptonFilter_Common.py')
filtSeq.xAODTTbarWToLeptonFilter_Common.NumLeptons = 1
filtSeq.xAODTTbarWToLeptonFilter_Common.Ptcut = 0.

