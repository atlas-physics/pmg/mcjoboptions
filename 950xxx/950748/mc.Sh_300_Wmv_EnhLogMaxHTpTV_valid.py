include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "Sherpa W+/W- -> munu + 0,1,2j@NLO + 3,4j@LO"
evgenConfig.keywords = ["SM", "W", "muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch","chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""

# me generator settings
ME_SIGNAL_GENERATOR:
  - Comix
  - Amegic
  - OpenLoops

# EW corrections setup
OL_PARAMETERS:
  ew_scheme: 2
  ew_renorm_scheme: 1
  write_paraemeters: 1

ASSOCIATED_CONTRIBUTIONS_VARIATIONS:
  - [EW]
  - [EW, LO1]
  - [EW, LO1, LO2]
  - [EW, LO1, LO2, LO3]

EW_SCHEME: 3
GF: 1.166397e-5
METS_BBAR_MODE: 5

# speed and neg weight fraction improvements
PP_RS_SCALE: VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
NLO_CSS_PSMODE: 1

PROCESSES:
  - 93 93 -> 13 -14 93{4}:
      Order: {QCD: 0, EW: 2}
      CKKW: 20
      2->2-4:
        Associated_Contributions: [EW, LO1, LO2, LO3]
        Enhance_Observable: VAR{log10(max(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3]),MPerp(p[2]+p[3])))}|1|3.3 {3,4,5,6,7}
        NLO_QCD_Mode: MC@NLO
        ME_Generator: Amegic
        RS_ME_Generator: Comix
        Loop_Generator: OpenLoops
      2->6:
        Max_N_Quarks: 4
        Max_Epsilon: 0.01
      2->2-6:
        Integration_Error 0.99

  - 93 93 -> -13 14 93{4}:
      Order: {QCD: 0, EW: 2}
      CKKW: 20
      2->2-4:
        Associated_Contributions: [EW, LO1, LO2, LO3]
        Enhance_Observable: VAR{log10(max(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3]),MPerp(p[2]+p[3])))}|1|3.3 {3,4,5,6,7}
        NLO_QCD_Mode: MC@NLO
        ME_Generator: Amegic
        RS_ME_Generator: Comix
        Loop_Generator: OpenLoops
      2->6:
        Max_N_Quarks: 4
        Max_Epsilon: 0.01
      2->2-6:
        Integration_Error 0.99

SELECTORS:
  - [Mass, 13, -14, 2.0, E_CMS]
  - [Mass, -13, 14, 2.0, E_CMS]
"""

genSeq.Sherpa_i.NCores = 24

