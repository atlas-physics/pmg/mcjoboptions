## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.description = "Herwig7 dijet LO, JZ4 with R04 slicing"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "yoran.yeh@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4 )
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4,filtSeq)

command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 150.0*GeV

##  - - -   bias to high pt.
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert /Herwig/MatrixElements/SubProcess:Preweights 0  MPreWeight
set MPreWeight:MaxPTPower 4
##

"""

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()
