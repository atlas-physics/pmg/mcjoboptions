import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

evgenConfig.description     = "Non-resonant production of two b-jets two additional jets and a photon"
evgenConfig.keywords        = ['charm', 'photon', 'QCD']
evgenConfig.contact         = ["carolyn.mei.gee@cern.ch"]
evgenConfig.generators      = ['MadGraph', 'Pythia8']
evgenConfig.nEventsPerJob   = 10000

gridpack_mode = True
gridpack_dir = 'madevent/'
minMjj       = 500

if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*3)
else: nevents = int(evgenConfig.nEventsPerJob*3)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Process type:
#---------------------------------------------------------------------------

process="""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > c c~ j j a $ z h
output -f"""

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+process+"""
output -f
""")
fcard.close()

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf'      : 93300, # PDF4LHC21_40_pdfas 
    'pdf_variations'   : [93300], 
     # NNPDF31 hessian, CT14, MMHT14, CT18, CT18Z, CT18A, HERAPDF2.0, ABMP16 5FS, NNPDF30 hessian, MSHT20, NNPDF40_nnlo_as_01180_hessian, PPDF4LHC15_nlo_mc
    'alternative_pdfs' : [304400,13000,25300,14000,14100,14200,61200,42560,303200,27400,331500,90500],
    'scale_variations' :[0.5,1,2], 
    }

extras = { 'nevents'      : nevents,
           'lhe_version'  : '3.0',
           'cut_decays'   : 'F',
           'maxjetflavor' : '5', 
           'ptj'          : '15.0',
           'ptb'          : '15.0',
           'pta'          : '20.0',
           'etaj'         : '-1.0',
           'etaa'         : '3.0',
           'drjj'         : '0.2',
           'drbb'         : '0.2',
           'drbj'         : '0.2',
           'drab'         : '0.2',
           'draj'         : '0.2'
           }

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
               settings=extras)

print_cards()

generate(process_dir=process_dir,
         grid_pack=gridpack_mode, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3)

# Shower
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Filter
include('GeneratorFilters/FindJets.py')
CreateJets(prefiltSeq, 0.4, "WZ") 

#https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/GeneratorFilters/share/common/VBFForwardJetsFilter.py

# Set up VBF filters
if not hasattr(filtSeq, "VBFForwardJetsFilter" ):
	from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
	filtSeq += VBFForwardJetsFilter()

filtSeq.VBFForwardJetsFilter.JetMinPt = 0.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MinPt = 0.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MinPt = 0.*GeV
filtSeq.VBFForwardJetsFilter.TruthJetContainer = "AntiKt4WZTruthJets"
filtSeq.VBFForwardJetsFilter.MassJJ = minMjj*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 0.1
