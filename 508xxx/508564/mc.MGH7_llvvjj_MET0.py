import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *


#import fileinput
#import shutil
#import subprocess
#import os

mode=0

#gridpack_dir='madevent/'
#gridpack_mode=True
gridpack_dir=None
gridpack_mode=False

masses={'25': '1.250000e+02'}        ## Higgs mass 
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width
extras_cuts = {} ## mass filters
#os.environ['MADPATH'] = "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/sw/lcg/external/MCGenerators_lcgcmt67c/madgraph5amc/2.6.2.atlas/x86_64-slc6-gcc47-opt"



# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
model="import model sm"
nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
process_str="""
"""+model+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = ve vm vt
define v~ = ve~ vm~ vt~
generate p p > W+ W- j j QCD=0, w+ > l+ v,  w- > l- v~
output -f
"""

name = "osWW"

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


maxjetflavor = 4
dynamic=-1
#dynamic=1 #Total transverse energy of the event.
#dynamic=2 #sum of the transverse mass
#dynamic=3 #sum of the transverse mass divide by 2
#dynamic=4 #\sqrt(s), partonic energy
#dynamic=5 #\decaying particle mass, for decays



#process_dir = new_process(grid_pack="madevent/")
process_dir = new_process(process_str)

extras = { 
'asrwgtflavor':"5",     
'lhe_version':"3.0",     
'etal':"5",    
'drll':"0",    
'drjl':"0",    
'maxjetflavor':"5" ,    
'auto_ptj_mjj': 'F',   
'nevents'      : nevents
 }

extras.update(extras_cuts)
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut=0,nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
print_cards()
params={}
params['MASS']=masses 
params['DECAY']=decays
modify_param_card(process_dir=process_dir,params=params)
#os.system("cp setscales.f  "+process_dir+"/SubProcesses/setscales.f")

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig72ConfigLHEF import Hw72ConfigLHEF

genSeq += Herwig7()
Herwig7Config = Hw72ConfigLHEF(genSeq, runArgs)

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
lhe_filename = runArgs.inputGeneratorFile.split('.tar.gz')[0]+'.events'
Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")
  
dipoleShowerCommands = """
cd /Herwig/EventHandlers
set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
cd /Herwig/DipoleShower
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=2.0 2.0 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=1.0 2.0 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=0.5 2.0 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=2.0" 1.0 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.5" 1.0 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=2.0" 0.5 2.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=1.0 0.5 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=0.5 0.5 0.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.75_fsr:muRfac=1.0 1.75 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.5_fsr:muRfac=1.0 1.5 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.25_fsr:muRfac=1.0 1.25 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.625_fsr:muRfac=1.0" 0.625 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.75_fsr:muRfac=1.0 0.75 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=0.875_fsr:muRfac=1.0 0.875 1.0 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.75 1.0 1.75 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.5 1.0 1.5 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.25 1.0 1.25 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.625 1.0 0.625 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.75 1.0 0.75 All
do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.875 1.0 0.85 All
"""
print dipoleShowerCommands
Herwig7Config.add_commands(dipoleShowerCommands)
  
# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


## Provide config information
evgenConfig.generators += ["MadGraph", "Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "MG5aMCatNLO/Herwig7 LHEF"
evgenConfig.keywords    = ['SM','diboson','VBS','WW','electroweak','2lepton','2jet']
evgenConfig.contact = ["rhayes@cern.ch"]


