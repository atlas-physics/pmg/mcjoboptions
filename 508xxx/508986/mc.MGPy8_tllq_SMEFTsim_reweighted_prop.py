selected_operators = ['cHQ1','cHQ3']

process_definition = 'generate p p > t j l+ l- $$ w+ w- QCD=1 QED=4 NP=0 NPprop=2 / w1+ w1- h1, (t > w+ b NP=0 NPprop=0,  w+ > l+ vl NP=0 NPprop=0) @0 SMHLOOP=0\n'
process_definition+= 'add process p p > t~ j l+ l- $$ w+ w- QCD=1 QED=4 NP=0 NPprop=2 / w1+ w1- h1, (t~ > w- b~ NP=0 NPprop=0, w- > l- vl~ NP=0 NPprop=0) @1 SMHLOOP=0'

fixed_scale = 263.5 # ~ m(top)+m(Z)

gridpack = False

evgenConfig.description = 'SMEFTsim 3.0 t+ll, top model, trilepton, reweighted, no EFT vertices, propagator correction'

# for NPprop>0, whitelist dummy fields used for propagator corrections
whitelist = open("pdgid_extras.txt", "w")
for pid in range(9000005,9000008+1):
    whitelist.write(str(pid)+"\n-"+str(pid)+"\n")
whitelist.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

include("../../508xxx/508772/Common_SMEFTsim_topmW_topX_reweighted.py")