params={}
params['mZDinput']  = 9.000000e-01
params['MHSinput']  = 2.000000e+00
params['epsilon']   = 1.000000e-03
params['gXmu']      = 6.500000e-04
params['gXe']       = 6.500000e-04
params['gXpi']      = 1.000000e-11
params['MChi1']     = 2.000000e+00
params['MChi2']     = 5.000000e+00
params['WZp']       = "DECAY 3000001 8.243660e-09 # WZp"
params['mH']        = 125
params['nGamma']    = 2
params['avgtau']    = 10
params['decayMode'] = 'normal'




                

include ("MadGraphControl_A14N23LO_FRVZ_ggF.py")

evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    
