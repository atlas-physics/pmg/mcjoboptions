import subprocess
retcode = subprocess.Popen(['get_files', '-jo', 'SMWeinbergCommon.py'])
if retcode.wait() != 0:
    raise IOError('could not locate SMWeinbergCommon.py')

import SMWeinbergCommon

SMWeinbergCommon.process = SMWeinbergCommon.available_processes['mumuchannel']
SMWeinbergCommon.parameters_paramcard['nuphysics']['Lambda'] = 200e3
SMWeinbergCommon.parameters_paramcard['nuphysics']['Cmm'] = 1.0

SMWeinbergCommon.run_evgen(runArgs, evgenConfig, opts)
