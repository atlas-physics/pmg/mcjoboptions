from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

evgenConfig.contact = ["Pingchuan Zhao <pingchuan.zhao@cern.ch>"]

process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
import model ZAAA
generate p p > z,z > a a a
output -f"""

nevents = 15000

process_dir = new_process(process)
#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version':'3.0', 
            'cut_decays' :'F',
            'nevents'    :int(nevents)}

params={}
params["npcoupl"] = {'1': '1.450000e-10', '2': '1.450000e-10'}

modify_param_card(process_dir=process_dir,params=params)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 

evgenConfig.generators = ["MadGraph"]

#### Shower 
evgenConfig.description = 'MadGraph_ZAAA'
evgenConfig.keywords+=['SM','photon','Z']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

