#include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' ) 
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )



#### done by Arka, second time

def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

### parse job options name 
# N.B. 60 chars limit on PhysicsShort name...
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
additional_options = phys_short.split("_")[-1]
evgenLog.info("GGM Higgsino: Additional options: " + str(additional_options))

# MC15.900001.MGPy8EG_A14N23LO_C1N2N1_GGMHino_95_noFilter.py
# C1/N2/N1 roughly mass denegenerate  mC1=mN2=mN1+1 GeV

print("HERE:", runArgs.jobConfig, runArgs.jobConfig[0].split('_'))
print("HERE2:", phys_short.split('_'))
#masses['1000022'] = float(runArgs.jobConfig[0].split('_')[4])
#masses['1000023'] = -(float(runArgs.jobConfig[0].split('_')[4])+1.0)
#masses['1000024'] = float(runArgs.jobConfig[0].split('_')[4])+1.0
masses['1000022'] = StringToFloat(phys_short.split('_')[4])
masses['1000023'] = -(StringToFloat(phys_short.split('_')[4])+1.0)
masses['1000024'] = StringToFloat(phys_short.split('_')[4])+1.0
masses['1000039'] = 0.001
masses['35'] = 1.25000000E+02

# Set Gen and Decay types 
#gentype = str(runArgs.jobConfig[0].split('_')[2])
#decaytype = str(runArgs.jobConfig[0].split('_')[3])

#BEGIN MOD
# Set Gen and Decay types 
# Configure our mixing for higgsinos
param_blocks['NMIX'] = {}
param_blocks['NMIX'][ '1   1' ] = '0.0' 
param_blocks['NMIX'][ '1   2' ] = '-0.0' 
param_blocks['NMIX'][ '1   3' ] = '0.707'
param_blocks['NMIX'][ '1   4' ] = '-0.707'
param_blocks['NMIX'][ '2   1' ] = '0.0' 
param_blocks['NMIX'][ '2   2' ] = '-0.0' 
param_blocks['NMIX'][ '2   3' ] = '-0.707'
param_blocks['NMIX'][ '2   4' ] = '-0.707'
param_blocks['NMIX'][ '3   1' ] = '1.0' 
param_blocks['NMIX'][ '3   2' ] = '0.0' 
param_blocks['NMIX'][ '3   3' ] = '0.0'
param_blocks['NMIX'][ '3   4' ] = '-0.0'
param_blocks['NMIX'][ '4   1' ] = '0.0' 
param_blocks['NMIX'][ '4   2' ] = '-1.0' 
param_blocks['NMIX'][ '4   3' ] = '-0.0'
param_blocks['NMIX'][ '4   4' ] = '0.0'
param_blocks['UMIX'] = {}
param_blocks['UMIX'][ '1   1' ] = '-0.0' 
param_blocks['UMIX'][ '1   2' ] = '1.0' 
param_blocks['UMIX'][ '2   1' ] = '1.0'
param_blocks['UMIX'][ '2   2' ] = '0.0'
param_blocks['VMIX'] = {}
param_blocks['VMIX'][ '1   1' ] = '-0.0' 
param_blocks['VMIX'][ '1   2' ] = '1.0' 
param_blocks['VMIX'][ '2   1' ] = '1.0'
param_blocks['VMIX'][ '2   2' ] = '0.0'
# Configure our decays
decays['1000022'] = """DECAY   1000022     1.00000000E-03   # N1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
     0.50000000E+00    2     1000039        25         # N1 to grav higgs
     0.50000000E+00    2     1000039        23         # N1 to grav Z
"""
# decays['1000022'] = """DECAY   1000022  3.24412243E+00   # N1 decays 
# #          BR         NDA          ID1       ID2       ID3       ID4
#      0.50000000E+00    2     1000039        25         # BR(~chi_10 -> ~G h)
#      0.50000000E+00    2     1000039        35         # BR(~chi_10 -> ~G H) and the H is forced to decay to photons
# """
decays['35'] = """DECAY        35  4.55911893E-03   # H decays -- force to decay to photons
#          BR         NDA          ID1       ID2       ID3       ID4
     1.00000000E+00    2          22        22   # BR(H1 -> gamma gamma)  
"""
decays['1000039'] = """DECAY   1000039     1.00000000E-07   # Gravitino decays 
"""
decays['1000023'] = """DECAY   1000023  1.00000000E-03   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
     1.37284605E-01    3     1000022        -2         2   # N2 to N1 uubar
     1.84624124E-01    3     1000022        -1         1   # N2 to N1 ddbar
     1.37284605E-01    3     1000022        -4         4   # N2 to N1 ccbar
     1.84624124E-01    3     1000022        -3         3   # N2 to N1 ssbar
     3.98007010E-02    3     1000022       -11        11   # N2 to N1 ee
     3.98007010E-02    3     1000022       -13        13   # N2 to N1 mumu
     3.98835450E-02    3     1000022       -15        15   # N2 to N1 tautau
     7.88991980E-02    3     1000022       -12        12   # N2 to N1 nue nue 
     7.88991980E-02    3     1000022       -14        14   # N2 to N1 num num 
     7.88991980E-02    3     1000022       -16        16   # N2 to N1 nut nut
"""
decays['1000024'] = """DECAY   1000024  1.00000000E-03   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
     3.64604242E-01    3     1000022         2        -1   # C1 to N1 udbar
     3.64604242E-01    3     1000022         4        -3   # C1 to N1 csbar
     1.21534824E-01    3     1000022       -11        12   # C1 to N1 e nu
     1.21534824E-01    3     1000022       -13        14   # C1 to N1 mu nu
     2.77218666E-02    3     1000022       -15        16   # C1 to N1 tau nu
"""
# decays['25'] = """DECAY        25  4.55911893E-03   # h decays (Taken from LHCWG YellowReportB2014)
# #          BR         NDA          ID1       ID2       ID3       ID4 
#      6.32000000E-02    2          15       -15   # H to tautau
#      5.77000000E-01    2           5        -5   # H to bbbar
#      2.15000000E-01    2          24       -24   # H to WW
#      2.62000000E-02    2          23        23   # H to ZZ
#      2.28000000E-03    2          22        22   # H to yy
#      2.19000000E-04    2          13       -13   # H to mu mu
#      2.91000000E-02    2           4        -4   # H to ccbar
#      2.46000000E-04    2           3        -3   # H to ssbar
#      8.57000000E-02    2          21        21   # H to guon
# """
#END MOD

# x1+x1- + 2 partons inclusive
process = '''
generate p p > x1+ x1- / susystrong @1
add process p p > x1+ x1- j / susystrong @2
add process p p > x1+ x1- j j / susystrong @3
add process p p > x1+ n1 / susystrong @1
add process p p > x1+ n1 j / susystrong @2
add process p p > x1+ n1 j j / susystrong @3
add process p p > x1- n1 / susystrong @1
add process p p > x1- n1 j / susystrong @2
add process p p > x1- n1 j j / susystrong @3
add process p p > x1+ n2 / susystrong @1
add process p p > x1+ n2 j / susystrong @2
add process p p > x1+ n2 j j / susystrong @3
add process p p > x1- n2 / susystrong @1
add process p p > x1- n2 j / susystrong @2
add process p p > x1- n2 j j / susystrong @3
add process p p > n1 n2 / susystrong @1
add process p p > n1 n2 j / susystrong @2
add process p p > n1 n2 j j / susystrong @3
'''
njets = 1
#evgenLog.info('Registered generation of ~hino pair production, decay to gravitino; grid point '+str(runArgs.runNumber)+' decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))
evgenLog.info('Registered generation of ~hino pair production, decay to gravitino; decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))

evgenConfig.contact += [ "santra.arka@gmail.com" ]
evgenConfig.keywords += ['SUSY', 'chargino', 'neutralino', 'gravitino']
evgenConfig.description = '~hino pair production, decay to gravitino simplified model, m_C1N2 = %s GeV, m_N1 = %s GeV'%(masses['1000023'],masses['1000022'])

# Filter and event multiplier 
evt_multiplier = 4
### this is added to explicitly turn off systematic variations + push in the proper event_norm setting, this is to check the bug

#extras['event_norm']='sum'
#extras['use_syst']='F'

if '4L4' in runArgs.jobConfig[0].split('_')[-1]:
  evgenLog.info('4lepton4 filter is applied')
  
  #include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
  include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
  filtSeq.MultiElecMuTauFilter.NLeptons  = 4
  filtSeq.MultiElecMuTauFilter.MinPt = 4000.         # pt-cut on the lepton
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
  filtSeq.Expression = "MultiElecMuTauFilter"
  
  if masses['1000022'] >= 600 :
    evt_multiplier = 100
  elif masses['1000022'] >= 200 :
    evt_multiplier = 170
  elif masses['1000022'] >= 130 :
    evt_multiplier = 210
  else:
    evt_multiplier = 250

    
elif '2L8' in runArgs.jobConfig[0].split('_')[-1]:
  evgenLog.info('2lepton8 filter is applied')
  
  #include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
  include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
  filtSeq.MultiElecMuTauFilter.NLeptons  = 2
  filtSeq.MultiElecMuTauFilter.MinPt = 8000.         # pt-cut on the lepton
  #filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.   # pt-cut on the hadronic taus
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus
  filtSeq.Expression = "MultiElecMuTauFilter"
  
  evt_multiplier = 50

elif '3L4' in runArgs.jobConfig[0].split('_')[-1]:
  evgenLog.info('3lepton4 filter is applied')
  
  #include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
  include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
  filtSeq.MultiElecMuTauFilter.NLeptons  = 3
  filtSeq.MultiElecMuTauFilter.MinPt = 4000.         # pt-cut on the lepton
  filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.   # pt-cut on the hadronic taus
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 1    # include hadronic taus
  filtSeq.Expression = "MultiElecMuTauFilter"
  
  evt_multiplier = 30

elif '4LT4' in runArgs.jobConfig[0].split('_')[-1]:
  evgenLog.info('4leptontau4 filter is applied')
  
  #include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
  include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
  filtSeq.MultiElecMuTauFilter.NLeptons  = 4
  filtSeq.MultiElecMuTauFilter.MinPt = 4000.         # pt-cut on the lepton
  filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.   # pt-cut on the hadronic taus
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 1    # include hadronic taus
  filtSeq.Expression = "MultiElecMuTauFilter"

  if masses['1000022'] >= 500 :
    evt_multiplier = 55
  elif masses['1000022'] >= 400 :
    evt_multiplier = 65
  elif masses['1000022'] >= 300 :
    evt_multiplier = 90
  elif masses['1000022'] >= 200 :
    evt_multiplier = 110
  elif masses['1000022'] >= 130 :
    evt_multiplier = 170
  elif masses['1000022'] >= 110 :
    evt_multiplier = 260
  elif masses['1000022'] >= 95 :
    evt_multiplier = 280
  else:
    evt_multiplier = 100

  minevents=2000     # only do 2k events at once in prod system
  
  
#### the code snippet needed for GMSB Higgsino NLSP signal point ####

elif '2L3' in runArgs.jobConfig[0].split('_')[-1]:
  
  evgenLog.info('2leptonMET50 filter is applied')
  
  #include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
  include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
  filtSeq.MultiElecMuTauFilter.NLeptons  = 2         # need 2 leptons
  filtSeq.MultiElecMuTauFilter.MinPt = 3000.         # pt-cut on the lepton
  filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
  filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # do not include hadronic taus
  
  include("MC15JobOptions/MissingEtFilter.py")
  filtSeq.MissingEtFilter.METCut = 50*GeV            # MET > 50 GeV
  
  filtSeq.Expression = "(MultiElecMuTauFilter) and (MissingEtFilter)"

  
  if masses['1000022'] >= 600 :
    evt_multiplier = 12
  elif masses['1000022'] >= 300 :
    evt_multiplier = 16
  elif masses['1000022'] >= 150 :
    evt_multiplier = 20
  else:
    evt_multiplier = 20
  
    
#elif '2G15' in runArgs.jobConfig[0].split('_')[-1]:
elif '2G15' in additional_options:
  evgenLog.info('2photon15 filter is applied')
  
  #include ( 'MC15JobOptions/DirectPhotonFilter.py' )
  #include ( 'GeneratorFilters/DirectPhotonFilter.py' )
  #filtSeq.DirectPhotonFilter.NPhotons  = 2
  #filtSeq.DirectPhotonFilter.Ptmin = 15000.         # pt-cut on the photon
  #filtSeq.DirectPhotonFilter.Etacut = 2.8          # eta cut on the photon
  #filtSeq.Expression = "DirectPhotonFilter"

  #from GeneratorFilters.GeneratorFiltersConf import SUSYGGMHiggsinoPhotonFilter
  #filtSeq += SUSYGGMHiggsinoPhotonFilter()
  #filtSeq.SUSYGGMHiggsinoPhotonFilter.NPhotons  = 2
  #filtSeq.SUSYGGMHiggsinoPhotonFilter.Ptmin = 15000.         # pt-cut on the photon
  #filtSeq.SUSYGGMHiggsinoPhotonFilter.Etacut = 2.8          # eta cut on the photon
  #filtSeq.Expression = "SUSYGGMHiggsinoPhotonFilter"

  evt_multiplier = 4

#include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# Bugfix from Zach
# Only N1 is modified by MG, put it back
decays['1000022'] = """DECAY   1000022     1.00000000E-03   # N1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
     0.50000000E+00    2     1000039        35         # N1 to grav higgs
     0.50000000E+00    2     1000039        23         # N1 to grav Z
"""
# decays['1000022'] = """DECAY   1000022  3.24412243E+00   # N1 decays 
# #          BR         NDA          ID1       ID2       ID3       ID4
#      0.50000000E+00    2     1000039        25         # BR(~chi_10 -> ~G h)
#      0.50000000E+00    2     1000039        35         # BR(~chi_10 -> ~G H) and the H is forced to decay to photons
# """

import os
os.rename(runArgs.inputGeneratorFile+'.events', runArgs.inputGeneratorFile+'_old.events')
modify_param_card(param_card_input=runArgs.inputGeneratorFile+'_old.events',params={'DECAY':decays},output_location=runArgs.inputGeneratorFile+'.events')
os.unlink(runArgs.inputGeneratorFile+'_old.events.old_to_be_deleted')

# if njets>0:
#     genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
    
#     ### needed to use "guess" option of Pythia.
#     if "UserHooks" in genSeq.Pythia8.__slots__.keys():
#         genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
#     else:
#         genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'

useguess = True
# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
