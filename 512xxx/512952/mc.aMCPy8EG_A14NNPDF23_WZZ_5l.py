import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# WZZ->5l
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ["thomas.glyn.hitchings@cern.ch"]
runName = 'test'
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model SM_Ltotal_Ind5v2020v2_UFO
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > w+ z z NP=1 QCD=1 QED=3, w+ > l+ vl, z > l+ l-, z > l+ l-
add process p p > w- z z NP=1 QCD=1 QED=3, w- > l- vl~, z > l+ l-, z > l+ l-
output -f"""
process_dir = new_process(process)


settings = {'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


#update param card to include new couplings
param_card_name = 'param_card_mod.dat'
modify_param_card(param_card_input=param_card_name,process_dir=process_dir)


generate(process_dir=process_dir,runArgs=runArgs)
outDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.generators = ["aMcAtNlo"]
evgenConfig.description = 'aMcAtNlo_WWZ'
evgenConfig.keywords+=['lepton']
#evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=outDS #'test_lhe_events.events'

# SHOWERING
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#LO shower
include("Pythia8_i/Pythia8_MadGraph.py")

#### Finalize
#theApp.finalize()
#theApp.exit:()
