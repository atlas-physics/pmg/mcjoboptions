from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraphUtils
import re
import subprocess
import sys

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':263400,
    # 'pdf_variations':[263400],
    # 'alternative_pdfs':[262400,13202],
    # 'scale_variations':[0.5,1.,2.],
    'use_syst':False,
}

vlq_ids = {'X':"6000005", 
           'T':"6000006", 
           'B':"6000007", 
           'Y':"6000008"}


get_vlqcoupling = subprocess.Popen(['get_files', '-jo', 'VLQCouplingCalculator.py'])
if get_vlqcoupling.wait():
    print("Could not copy VLQCouplingCalculator.py")
    sys.exit(2)

from VLQCouplingCalculator import *

#### Some Variables
MAX_TRIAL = 2     ## Maximum number of trials allowed for failures in event generation or reweighting
SAFE_FACTOR = 1.1  ## Number of events generated = SAFE_FACTOR * max_events
runName='run_01'   ## Run name for event generation


if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

extras = { 'nevents': evgenConfig.nEventsPerJob * SAFE_FACTOR,
           'iseed': str(runArgs.randomSeed),
           'xqcut': "0.",
           'lhe_version'   : '3.0',
           'cut_decays'    : 'F',
           'bwcutoff'      : '10000',
           'event_norm'    : 'average',
           'drjj'          :  -1.0,
           'drll'          :  -1.0,
           'draa'          :  -1.0,
           'draj'          :  -1.0,
           'drjl'          :  -1.0,
           'dral'          :  -1.0,
           'etal'          :  -1.0,
           'etaj'          :  -1.0,
           'etaa'          :  -1.0,
}

process = '''set zerowidth_tchannel False
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/VLQ_v4_4FNS_UFO-3rd
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define bb = b b~
define WW = w+ w-
define tt = t t~
define ferm = ve vm vt ve~ vm~ vt~ mu- ta- e- mu+ ta+ e+ u c d s b u~ c~ d~ s~ b~
define TPTP = tp tp~
define BPBP = bp bp~
define XX = x x~
define YY = y y~
define lep = e- e+ mu- mu+
add process p p > j z bb bb / tp x y VLQ=2 QED=1 QCD=1, (z > lep lep)
output -f 
'''

runArgs.vlqmode, runArgs.prodmode, runArgs.decaymode, runArgs.vlqprocess, runArgs.chirality, runArgs.mass, runArgs.kappa, runArgs.dosig, runArgs.dosigbar, runArgs.dorwt = 'B', 'Z', 'Z', 'ZBZb', 'LH', 2100.0, 0.5, True, True, False

def paramdictmaker():
    chiralityIndex = runArgs.chirality.replace('H','')
    all_blocks = ["k"+a+b+c for a in ["t","b"] for b in ["l","r"] for c in ["w", "h", "z"]] + ["k"+a+b+"w" for a in ["y","x"] for b in ["l","r"]]
    all_vars = ["K"+a+b+c+d for a in ["T","B"] for b in ["L","R"] for c in ["w", "h", "z"] for d in ["1","2","3"]] + ["K"+a+b+d for a in ["Y","X"] for b in ["L","R"] for d in ["1","2","3"]]

    paramdict = {}
    for block in ['mass', 'decay'] + all_blocks:
        paramdict[block] = {}

    
    for vlqmode in vlq_ids.keys():
        paramdict['mass'][vlq_ids[vlqmode]] = str(runArgs.mass)
        paramdict['decay'][vlq_ids[vlqmode]] = str(runArgs.gamma)

    if runArgs.vlqmode in ['X', 'Y']:
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'w']['K' + runArgs.vlqmode + chiralityIndex + '3'] = str(runArgs.kw)
    else:
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'w']['K' + runArgs.vlqmode + chiralityIndex + 'w3'] = str(runArgs.kw)
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'z']['K' + runArgs.vlqmode + chiralityIndex + 'z3'] = str(runArgs.kz)
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'h']['K' + runArgs.vlqmode + chiralityIndex + 'h3'] = str(runArgs.kh)

    return paramdict


def Vars():
    chiralityIndex = runArgs.chirality.replace('H','')
    all_vars = ["K"+a+b+c+d for a in ["T","B"] for b in ["L","R"] for c in ["w", "h", "z"] for d in ["1","2","3"]] + ["K"+a+b+d for a in ["Y","X"] for b in ["L","R"] for d in ["1","2","3"]]
    if runArgs.vlqmode in ['X', 'Y']:
        vars_to_change = ['M' + runArgs.vlqmode,
                          'W' + runArgs.vlqmode,
                          'K' + runArgs.vlqmode + chiralityIndex + '3']
    else:
        vars_to_change = ['M' + runArgs.vlqmode + 'P',
                          'W' + runArgs.vlqmode + 'P',
                          'K' + runArgs.vlqmode + chiralityIndex + 'w3',
                          'K' + runArgs.vlqmode + chiralityIndex + 'z3',
                          'K' + runArgs.vlqmode + chiralityIndex + 'h3']
    return  vars_to_change


me_exec = os.environ['MADPATH'] + '/bin/mg5_aMC' ## MadGraph executable

if not os.access(me_exec, os.R_OK):
    print ("mg5_aMC not located. Aborting!!")
    sys.exit(2)

c = VLQCouplingCalculator(runArgs.mass, runArgs.vlqmode)

if runArgs.vlqmode in ['X', 'Y']:
    c.setKappaxi(runArgs.kappa, 1.0, 0.0)
else:
    c.setKappaxi(runArgs.kappa, 0.5, 0.25)

[runArgs.kw, runArgs.kz, runArgs.kh, ks] = c.getKappas()
runArgs.gamma = c.getGamma()

print ("VLQ:   ", runArgs.vlqmode)
print ("Mass:  ", runArgs.mass)
print ("Kappa: ", runArgs.kappa)
print ("Process:  ", runArgs.vlqprocess)

paramlist = Vars()
params = paramdictmaker()

process_dir = new_process(process)
modify_run_card(process_dir=process_dir, settings=extras)
modify_param_card(process_dir=process_dir, params=params)
MadGraphControl.MadGraphUtils.MADGRAPH_RUN_NAME = runName

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN and A15NNPDF23LO for VLQ single " + runArgs.vlqmode + " to " + runArgs.vlqprocess[2:] + " while produced via " + runArgs.prodmode
evgenConfig.keywords = ["BSM", "BSMtop", "exotic"]
evgenConfig.process = runArgs.vlqprocess
evgenConfig.contact =  ['avik.roy@cern.ch']

