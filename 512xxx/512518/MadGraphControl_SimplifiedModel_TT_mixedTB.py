include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

flavourScheme = 5

#get the file name and split it on _ to extract relavant information

jobConfigParts = JOName.split("_")

if "MadSpin" in JOName:
    mstop=float(jobConfigParts[5])
    mneutralino=float(jobConfigParts[6].split('.')[0])
else:
    mstop=float(jobConfigParts[4])
    mneutralino=float(jobConfigParts[5].split('.')[0])

masses['1000006'] = mstop
masses['1000022'] = mneutralino
masses['1000024'] = mneutralino+1 # Chargino mass is N1 + 1GeV

process = '''
generate p p > t1 t1~ $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
'''

decays['1000006'] = """DECAY   1000006     1.34259598E-01   # stop1 decays
     0.50000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     0.50000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1pm b )
"""

decays['1000024'] = """DECAY   1000024     1.34259598E-01   # Chi_pm decays
     1.0000000E+00    2     1000022         24   # BR(~chi_1pm -> ~chi_10 W )
     0.00000000E+00    3     1000022       11        12  # BR(~chi_1+ -> ~chi_10 e+ nu_e), dummy decay for Madgraph, https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/505xxx/505109/SUSY_SimplifiedModel_higgsino.py#L119-121
"""


njets = 2

if "MadSpin" in JOName.split('_')[-1]:
    print JOName.split('_')[-1]
    if "MET" in JOName.split('_')[-2]:
        print JOName.split('_')[-2]
        include ( 'GeneratorFilters/MissingEtFilter.py' )

        metFilter = JOName.split('_')[-2]
        print metFilter
        metFilter = int(metFilter.split("MET")[1])
        print metFilter

        print "Using MET Filter: " + str(metFilter)
        filtSeq.MissingEtFilter.METCut = metFilter*GeV
        evt_multiplier = metFilter / 10

elif "MET" in JOName.split('_')[-1]:
      include ( 'GeneratorFilters/MissingEtFilter.py' )

      metFilter = JOName.split('_')[-1]
      metFilter = int(metFilter.split("MET")[1].split(".")[0])

      print "Using MET Filter: " + str(metFilter)
      filtSeq.MissingEtFilter.METCut = metFilter*GeV
      evt_multiplier = metFilter / 10

else:
      print "No MET Filter applied"


evgenLog.info('Registered generation of stop pair production, stop to b+NLSP and t+LSP; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point mstop=' + str(masses['1000006']) + ', mlsp='+str(masses['1000022'])+ ", mnlsp="+str(masses['1000024']))

evgenConfig.contact  = [ "john.kenneth.anders@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','stop']
evgenConfig.description = 'stop direct pair production, st->t+LSP and st-> b+nLSP in simplified model'

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]
genSeq.Pythia8.Commands += ["24:mMin = 0.2","-24:mMin = 0.2"]

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
