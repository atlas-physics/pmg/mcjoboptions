import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import math
import os


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


nevents=int(3.0*runArgs.maxEvents)

model_pars_str = str(jofile) 
mtaup=int(0)
vllm=int(0)
vlle=int(0)

##                generate p p > taup taup~, taup > w- vm, taup~ > w vm~
for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'MVLL' in s:
        ss=s.replace("MVLL","")  
        print(ss)
        if ss.isdigit():
            mtaup = int(ss) 
    if 'mu' in s:
        vllm = 0.1
        vlle = 0.
    if 'el' in s:
        vllm = 0.
        vlle = 0.1


process_string=str(0)
process_string = """
                import model sm
                import model VLLS_NLO --modelname
                define p = g u c d s u~ c~ d~ s~
                define j = g u c d s u~ c~ d~ s~
                define l+ = e+ mu+
                define l- = e- mu-
                define vl = ve vm 
                define vl~ = ve~ vm~
                generate p p > taup taup~ [QCD]
                output -f
                """
process_dir = new_process(process_string)


# Set up the run card with mostly default values
run_card_extras = {
            'lhe_version':'3.0',
            # 'pdlabel':"'lhapdf'",
            # 'lhaid':' 260400',
            'nevents' :int(nevents)
}
params = {}
# Set masses
params['MASS'] = {'17':mtaup}
params['Couplings'] = {'1':vlle,'2':vllm,'3':'0'}
# params['VECTORLIKE'] = {'1','0.04'}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras)


# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
# if os.access(madspin_card,os.R_OK):
#     os.unlink(madspin_card)
mscard = open(madspin_card,'w')
if vllm > 0:
    mscard.write("""
    # set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    # set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
    set seed %i
    decay w+ > all all
    decay w- > all all
    decay z > all all
    decay taup > z mu-
    decay taup~ > z mu+
    decay taup > h mu-
    decay taup~ > h mu+
    decay taup > w- vm
    decay taup~ > w+ vm~
    launch""" % (runArgs.randomSeed))
if vlle > 0:
    mscard.write("""
    # set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    # set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
    set seed %i
    decay w+ > all all
    decay w- > all all
    decay z > all all
    decay taup > z e-
    decay taup~ > z e+
    decay taup > h e-
    decay taup~ > h e+
    decay taup > w- ve
    decay taup~ > w+ ve~
    launch""" % (runArgs.randomSeed))
mscard.close()

print_cards()
modify_param_card(process_dir=process_dir,params=params) #param_card_extras
generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")

# Shower
evgenConfig.description = 'MadGraph5+Pythia8 for Vectorlike leptons of mass {} GeV, inc lepton final state'.format(mtaup)
evgenConfig.contact = ['Merve Nazlim Agaras <merve.nazlim.agaras@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton']
evgenConfig.generators += ['MadGraph','Pythia8']
# runArgs.inputGeneratorFile=output_DS

# include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
# include("Pythia8_i/Pythia8_MadGraph.py")

# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut = 15000.0#MeV



