from MadGraphControl.MadGraphUtils import *

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# General settings
nevents = runArgs.maxEvents*3.0 if runArgs.maxEvents>0 else 3.0*evgenConfig.nEventsPerJob

gridpack_mode=False

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
if not is_gen_from_gridpack():
    process="""
    import model NTGC_Celine 
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > a l+ l- QCD=0
    output -f""" 
    process_dir=str(new_process(process))
else:
    process_dir=str(MADGRAPH_GRIDPACK_LOCATION)

#---------------------------------------------------------------------------
# lhapdf id for pdf weight 
#---------------------------------------------------------------------------
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING = {
    'central_pdf': 315000,
    #'alternative_pdfs': [262000],#NNPDF30_lo_as_0118
    'scale_variations': [0.5,1.,2.],
}

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'nevents' : int(nevents),
    'asrwgtflavor':"5",
    'lhe_version':"3.0",
    'ptj' :-1,
    'pta' :"150",
    'ptl' :"10",
    'etaj':"5",
    'etal':"3.0",
    'drll':"0",
    'mmjj':-1,
    'mmll':"10",
    'maxjetflavor':"5" ,
    }

#---------------------------------------------------------------------------
# coupling values 
#---------------------------------------------------------------------------
param_card_settings = {'dim8' : {
        'CBBL4' : '0.0',
        'CBtWL4': '2.0',
        'CBWL4' : '0.0',
        'CWWL4' : '0.0'}
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=extras)
modify_param_card(process_dir=process_dir, params=param_card_settings)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""launch --rwgt_info=CBtWL4_0p3
set CBtWL4 0.3
launch --rwgt_info=CBtWL4_0p6
set CBtWL4 0.6
launch --rwgt_info=CBtWL4_0p9
set CBtWL4 0.9
launch --rwgt_info=CBtWL4_1p2
set CBtWL4 1.2
launch --rwgt_info=CBtWL4_1p5
set CBtWL4 1.5
launch --rwgt_info=CBtWL4_1p8
set CBtWL4 1.8
launch --rwgt_info=CBtWL4_2p1
set CBtWL4 2.1
launch --rwgt_info=CBtWL4_2p4
set CBtWL4 2.4
launch --rwgt_info=CBtWL4_2p7
set CBtWL4 2.7
launch --rwgt_info=CBtWL4_3
set CBtWL4 3.0
launch --rwgt_info=CBtWL4_m0p3
set CBtWL4 -0.3
launch --rwgt_info=CBtWL4_m0p6
set CBtWL4 -0.6
launch --rwgt_info=CBtWL4_m0p9
set CBtWL4 -0.9
launch --rwgt_info=CBtWL4_m1p2
set CBtWL4 -1.2
launch --rwgt_info=CBtWL4_m1p5
set CBtWL4 -1.5
launch --rwgt_info=CBtWL4_m1p8
set CBtWL4 -1.8
launch --rwgt_info=CBtWL4_m2p1
set CBtWL4 -2.1
launch --rwgt_info=CBtWL4_m2p4
set CBtWL4 -2.4
launch --rwgt_info=CBtWL4_m2p7
set CBtWL4 -2.7
launch --rwgt_info=CBtWL4_m3
set CBtWL4 -3.0
set CBBL4  0.0
set CBWL4  0.0
set CWWL4  0.0""")
reweight_card_f.close()

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True)

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph aTGC leptonic decay final state'
evgenConfig.keywords+=['SM','Z']
evgenConfig.contact = ['Danning Liu<danning.liu@cern.ch>']
