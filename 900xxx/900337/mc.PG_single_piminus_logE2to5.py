evgenConfig.description = "Single Pi- with flat eta-phi and E in [2, 5] GeV"
evgenConfig.keywords = ["singleParticle"] 
evgenConfig.contact  = ["matthew.gignac@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler.pid = -211
topSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=[2000,5000], eta=[-2.8, 2.8])

