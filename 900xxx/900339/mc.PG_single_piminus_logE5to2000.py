evgenConfig.description = "Single Pi- with flat eta-phi and log E in [5, 2000] GeV"
evgenConfig.keywords = ["singleParticle"] 
evgenConfig.contact  = ["matthew.gignac@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler.pid = -211
topSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(5000, 2000000.), eta=[-2.8, 2.8])
