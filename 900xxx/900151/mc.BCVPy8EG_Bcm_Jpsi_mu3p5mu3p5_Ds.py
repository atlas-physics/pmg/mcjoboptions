evgenConfig.description = "Bc- -> J/psi(mu3p5mu3p5) Ds-(phi(K+ K-) pi-)"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive", "2muon", "Jpsi"]
evgenConfig.nEventsPerJob = 5000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.userDecayFile = "Bcm_Jpsi_mumu_Ds.dec"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG20.pdt','Bcm_Jpsi_mumu_Ds.dec']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"

include("GeneratorFilters/BSignalFilter.py")

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL1MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.8
filtSeq.BSignalFilter.LVL2MuonCutPT  = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.8
