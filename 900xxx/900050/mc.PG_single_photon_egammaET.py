evgenConfig.description = "Single egamma particles (photons) with ET spectrum shaped for MVA calibration"
evgenConfig.keywords = ["singleParticle"]	
evgenConfig.contact  = ["chris.g@cern.ch"]


include("ParticleGun/ParticleGun_Common.py")
# include("GeneratorUtils/StdEvgenSetup.py")    # sets EvtMax to 100 in production. Zach Marshall suggested removing this include

def dbnFermiDirac(x,mu,kT):
   import math
   arg = (x-mu)/kT
   if arg < -20 :	# avoid numerical underflows
      result = 1
   elif arg > 20 :	# avoid numerical overflows
      result = 0
   else :
      div = math.exp(arg)+1
      result = 1/div
   return result

import ParticleGun as PG

class ETSpectrumParticleSampler(PG.ParticleSampler):

    def __init__(self, pid, num=1):
        self.pid = PG.mksampler(pid)
        self.numparticles = num

        # Parameters for the MVA-shaped spectrum : higher density in the < 100 GeV range
        mu1 = 0.5		# mu1,kT1 : smooth but steep ramp-up from 0 to 1 GeV (requested by TauCP)
        kT1 = 0.1
        mu2 = 200		# mu2,kT2 : smooth, slow ramp-down in the 100-300 GeV range
        kT2 = 20
        y0 = 0.005		# y0 : baseline for low-density at high ET up to 3 TeV

        # Create and fill a very fine-grained histogram 
        from ROOT import TH1D
        etSpectrumFullRange =  TH1D("ETSpectrumFullRange" , "Reference ET spectrum for egamma MVA calib" , 30000 , 0 , 3000)
        for i in range(etSpectrumFullRange.GetNbinsX()):
             x = etSpectrumFullRange.GetBinCenter(i+1)
             y1 = dbnFermiDirac(x,mu1,kT1)
             y2 = dbnFermiDirac(x,mu2,kT2)
             y = y0 - y1 + y2
             etSpectrumFullRange.SetBinContent(i+1,y)
        self.hist = PG.TH1(etSpectrumFullRange) #< wrap *after* populating

    def shoot(self):
        "Return a vector of sampled particles from the provided ET-histogram"
        particles = []
        for i in range(self.numparticles):
             ptrand = self.hist.GetRandom()
             ptrand *= 1000 # NB. This _particular_ histogram is in GeV, but Athena needs MeV
             pid = self.pid()
             m = PG.MASSES[abs(pid)]
             mom = PG.PtEtaMPhiSampler(pt=ptrand, eta=[-2.5, 2.5], mass=m)
             p = PG.SampledParticle(pid, mom())
             p.mass = m
             particles.append(p)
        return particles

topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler = ETSpectrumParticleSampler(22)

evgenConfig.nEventsPerJob = 10000
