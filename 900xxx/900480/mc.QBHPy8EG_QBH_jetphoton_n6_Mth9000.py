evgenConfig.description = "Quantum black holes (n=6, M_th = 9.0 TeV) decaying to jet + photon"
evgenConfig.process = "QBH -> jet gamma"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.tune = "A14"
evgenConfig.notes = "CTEQ6L1"
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("Pythia8_i/Pythia8_LHEF.py")

# Increase tolerance on displaced vertieces due to highly boosted heavy flavours.
testSeq.TestHepMC.MaxVtxDisp = 100000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 100000*1000 #in mm
