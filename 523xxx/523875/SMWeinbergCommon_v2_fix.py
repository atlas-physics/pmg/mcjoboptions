import os

from AthenaCommon.Include import include
from MadGraphControl.MadGraphUtils import *

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING["central_pdf"] = 324900 #NNPDF31_nlo_as_0118_luxqed
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING["pdf_variations"] = [324900, 93300]  #NNPDF31_nlo_as_0118_luxqed, PDF4LHC21_40_pdfas

print(MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING)

# General settings
gridpack_mode=True
process_dir=''

stringy = 'aMCPy8EG_NNPDF31NLOluxqed_SMWeinberg_ssWW_NLO'

# Merging settings
maxjetflavor=5
ickkw=0
nJetMax=4
ktdurham=30
dparameter=0.4

process = None                  # set later explicitly

available_processes = {
    'pythia':'p p > mu+- mu+- j j',   # for pythia
    'mumuchannel':'''
import model SMWeinbergNLO

define p = u c d s b u~ c~ d~ s~ b~ g
define j = p
define ell+ = mu+
define ell- = mu-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

#For sanity checks:
#generate    p p > mu+ vm QED=2 QCD=0 [QCD]

output -f --noeps=True
    ''',
    'eechannel':'''
import model SMWeinbergNLO

define p = u c d s u~ c~ d~ s~ g
define j = p
define ell+ = e+
define ell- = e-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

output -f --noeps=True
    ''',
    'tautauchannel':'''
import model SMWeinbergNLO

define p = u c d s u~ c~ d~ s~ g
define j = p
define ell+ = ta+
define ell- = ta-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

output -f --noeps=True
    ''',
    'emuchannel':'''
import model SMWeinbergNLO

define p = u c d s u~ c~ d~ s~ g
define j = p
define ell+ = e+ mu+
define ell- = e- mu-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

output -f --noeps=True
    ''',
    'etauchannel':'''
import model SMWeinbergNLO

define p = u c d s u~ c~ d~ s~ g
define j = p
define ell+ = e+ ta+
define ell- = e- ta-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

output -f --noeps=True
    ''',
    'mutauchannel':'''
import model SMWeinbergNLO

define p = u c d s u~ c~ d~ s~ g
define j = p
define ell+ = mu+ ta+
define ell- = mu- ta-

generate    p p > ell+ ell+ j j QED=4 QCD=0 $$ w+ w- [QCD]
add process p p > ell- ell- j j QED=4 QCD=0 $$ w+ w- [QCD]

output -f --noeps=True
    '''
}




parameters_runcard = {
    'parton_shower' :'PYTHIA8',
    'req_acc_fo'    : 0.001,
    'jetalgo'       :-1,
    'jetradius'     :0.4,
    'etaj'          :5.5,    # generate until slightly out of detector acceptance
    'ptj'           :15
}

parameters_paramcard = {
    'nuphysics':{
        'Lambda':1e12,
        'Cee':0,
        'Cem':0,
        'Cet':0,
        'Cmm':0,
        'Cmt':0,
        'Ctt':0,
    }
}

# modify_run_card(
#     process_dir=process_dir,
#     runArgs=runArgs,
#     settings=extras_runcard
# )
# modify_config_card(
#     process_dir=process_dir
# )

# generate(
#     process_dir=process_dir,
#     grid_pack=gridpack_mode,
#     runArgs=runArgs
#     # nevents=nevents
# )


def run_evgen(runArgs, evgenConfig, opts):
    # scale up number of events so Pythia won't run out of events later on
    nevents=runArgs.maxEvents if runArgs.maxEvents > 0 else 5000
    nevents=1.5*nevents
    parameters_runcard['nevents']=int(nevents)


    if not is_gen_from_gridpack():
        process_dir = new_process(process)
    else:
        process_dir = MADGRAPH_GRIDPACK_LOCATION
        
        
    evgenConfig.description = 'test run'
    evgenConfig.keywords+=['BSM','exotic','neutrino', 'VBS'] # list of allowed keywords: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/blob/master/common/evgenkeywords.txt
    evgenConfig.generators += ["aMcAtNlo", "Pythia8", "EvtGen"]
    evgenConfig.process = 'pp -> mu mu j j'
    evgenConfig.inputconfcheck=""
    evgenConfig.contact = ["Richard Ruiz <richard.physics@gmail.com>", "Karolos Potamianos <karolos.potamianos@cern.ch"]
    runArgs.inputGeneratorFile=''.join(['tmp_', stringy, '._00001.events.tar.gz'])

    beamEnergy=-999
    if hasattr(runArgs,'ecmEnergy'):
        beamEnergy = runArgs.ecmEnergy / 2.
    else: 
        raise RuntimeError("No center of mass energy found.")
        
    modify_run_card(
        run_card_input=get_default_runcard(process_dir),
        process_dir=process_dir,
        runArgs=runArgs,
        settings=parameters_runcard
    )
    
    modify_param_card(
        param_card_input='/'.join([process_dir, 'Cards', 'param_card.dat']),
        process_dir=process_dir,
        params=parameters_paramcard
    )
    
    # modify FKS card to ignore IRPoleCheckThreshold
    print 'Modifying SKS Param Card, printing old stuff'
    with open('/'.join([process_dir, 'Cards', 'FKS_params.dat']), 'r+') as fks:
        retlines = []
        for line in fks:
            if '1.0d-5' in line and not line.startswith('!'): # at the time of writing, only param with this value
                retlines.append('-1.0d0\n')
            else:
                retlines.append(line)

        fks.seek(0)                 # set "cursor" at beginning of file
        fks.write(''.join(retlines))
        fks.truncate()              # and cut off the end
                
                
    print_cards()
    
    generate(
        process_dir=process_dir,
        grid_pack=gridpack_mode,
        runArgs=runArgs
    )
    
    arrange_output(
        process_dir=process_dir,
        runArgs=runArgs,
        lhe_version=3,
        saveProcDir=True
    )
    
    # back to single core
    check_reset_proc_number(opts)
    
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_aMcAtNlo.py")  
    # include("Pythia8_aMcAtNlo_VBSshowerTune.py")
