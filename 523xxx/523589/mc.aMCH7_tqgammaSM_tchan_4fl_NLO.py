import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import os,subprocess,fileinput
# from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

# General settings
evgenConfig.nEventsPerJob = 5000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

print(runArgs)


name = 'tqgammaSM_tchan_4fl_NLO'
runName='mc.aMC_HW7_'+str(name)

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t b~ j a $$ w+ w- [QCD]
add process p p > t~ b j a $$ w+ w- [QCD]
output -f"""

process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '3.0',
           'parton_shower' :'HERWIGPP',
           'ptgmin'        :10,
           'r0gamma'       :0.2,
           'maxjetflavor'  :4,
           'dynamical_scale_choice': '3', #sum of the transverse mass divided by 2
           'ptl'           :0.0,
           'ptj'           :0.0,
           'etal'          :5.0,
           'etagamma'      :5.0,
           'etaj'          :-1,
           'drll'          :0.0,
            # 'store_rwgt_info':True,
           'nevents':int(nevents),
           }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
set_top_params(process_dir,mTop=172.5,FourFS=True)

WT = 1.320000e+00
WW = 2.085000e+00

decays = {'6':"""DECAY  6  """+str(WT)+""" #WT
        #  BR             NDA  ID1    ID2   ...
        1.000000e+00   2   5  24 # 1.32
        #""",
        '24':"""DECAY  24  """+str(WW)+""" #WW
        #  BR             NDA  ID1    ID2   ...
        3.377000e-01   2    -1  2
        3.377000e-01   2    -3  4
        1.082000e-01   2   -11 12
        1.082000e-01   2   -13 14
        1.082000e-01   2   -15 16
        #"""}

modify_param_card(process_dir=process_dir,params={'DECAY':decays})

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
 set BW_cut 50                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.keywords+= ['SM', 'top',  'photon','singleTop','lepton']
evgenConfig.contact = ['Nils Abicht <nils.julius.abicht@cern.ch>']
evgenConfig.tune = "H7.2-Default"
runArgs.inputGeneratorFile=outputDS+".events"

check_reset_proc_number(opts)
include("Herwig7_i/Herwig72_LHEF.py") 

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=4)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
