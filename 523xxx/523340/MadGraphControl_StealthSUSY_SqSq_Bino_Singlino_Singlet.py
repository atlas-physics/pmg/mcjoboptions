# SUSY MadGraph+Pythia8 jobOptions for simplified model squark pair generation
import os, sys
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtils import new_process, modify_param_card
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphParamHelpers import get_masses, get_block

process = '''
import model mssm_stealth
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susyweak = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
generate p p > susylq susylq~ $ go susyweak @1
add process p p > susylq susylq~ j $ go susyweak @2
add process p p > susylq susylq~ j j $ go susyweak @3
'''

njets=2


JOName = get_physics_short()
joparts = JOName.split('_')

gentype = joparts[3]
decaytype = joparts[4]

mSquark = float(joparts[5])
mBino   = float(joparts[6])


# Do we want 4FS or 5FS? 4 is default
# * 5-flavor scheme always should use nQuarksMerge=5 [5FS -> nQuarksMerge=5]
# * 4-flavor scheme with light-flavor MEs (p p > go go j , with j = g d u s c)
#       should use nQuarksMerge=4 [4FS -> nQuarksMerge=4]
# * 4-flavor scheme with HF MEs (p p > go go j, with j = g d u s c b) should
#       use nQuarksMerge=5 [4FS + final state b -> nQuarksMerge=5]
flavourScheme = 5



# Set up the process
param_blocks["QNUMBERS"] = {"BLOCK QNUMBERS":
'''Block QNUMBERS 3000001 # singlino singlinobar
        1 0  # 3 times electric charge
        2 2  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 6: sextet, 8: octet)
        4 1  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 3000002 # singlet singletbar
        1 0  # 3 times electric charge
        2 1  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 6: sextet, 8: octet)
        4 1  # Particle/Antiparticle distinction (0=own anti)'''}

masses['1000001'] = mSquark # ~d_L
masses['2000001'] = mSquark # ~d_R
masses['1000002'] = mSquark # ~u_L
masses['2000002'] = mSquark # ~u_R
masses['1000003'] = mSquark # ~s_L
masses['2000003'] = mSquark # ~s_R
masses['1000004'] = mSquark # ~c_L
masses['2000004'] = mSquark # ~c_R

masses['1000022'] = mBino   # mN_1

masses['3000001'] = 100.0     # m(singlino)
masses['3000002'] =  90.0     # m(singlet)
masses['1000039'] =   1.0E-09 # m(~gravitino)
masses['1000021'] =   1.0E+06 # m(gluino)

masses['1000005'] =   1.0E+06 # ~b_1
masses['2000005'] =   1.0E+06 # ~b_2
masses['1000006'] =   1.0E+06 # ~t_1
masses['2000006'] =   1.0E+06 # ~t_2



#                                 PDG            Width
decays['1000002'] = '''DECAY   1000002     1.00000000E+00   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
'''
decays['2000002'] = '''DECAY   2000002     1.00000000E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
'''
decays['1000001'] = '''DECAY   1000001     1.00000000E+00   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
'''
decays['2000001'] = '''DECAY   2000001     1.00000000E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
'''
decays['1000004'] = '''DECAY   1000004     1.00000000E+00   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
'''
decays['2000004'] = '''DECAY   2000004     1.00000000E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
'''
decays['1000003'] = '''DECAY   1000003     1.00000000E+00   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
'''
decays['2000003'] = '''DECAY   2000003     1.00000000E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
'''


#decays['1000022'] = '''DECAY   1000022     1.00000000E-04   # chi0_1 decays
decays['1000022'] = '''DECAY   1000022     1.00000000E+00   # chi0_1 decays
     1.00000000E+00    2     3000001        22        # BR(chi0_1 -> gamma singlino)
'''
decays['3000001'] = '''DECAY   3000001     1.00000000E-03   # singlino decays
#           BR         NDA      ID1       ID2
     1.00000000E+00    2     3000002   1000039        # BR(singlino -> singlet ~gravitino)
'''
decays['3000002'] = '''DECAY   3000002     1.00000000E-03   # singlet decays
#           BR         NDA      ID1       ID2
     1.00000000E+00    2          21        21        # BR(singlet -> g g)
'''
#     0.99600000E+00    2          21        21        # BR(singlet -> g g)
#     0.00400000E+00    2          22        22        # BR(singlet -> gamma gamma)
#'''
#~q -> q (Bino -> ph (singlino -> gravitino (singlet -> gamma-gamma/gluon-gluon ) ) )

##---------------------------------------------------------------------------

#evgenLog.info('Registered generation of squark grid '+str(runArgs.runNumber))
evgenLog.info('Registered generation of squark grid decaying to a bino in a Stealth SUSY model')

evgenConfig.contact  = [ "joaquin.hoya@cern.ch", "njkang@ucsc.edu" ]
evgenConfig.keywords += ['simplifiedModel', 'squark', 'bino']
evgenConfig.description = 'squark production, sq->q+bino(->gamma+Singlino) in simplified model, m_sq = %s GeV, m_N1 = %s GeV'%(masses[squarks[0]],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

import os
os.rename(runArgs.inputGeneratorFile+'.events', runArgs.inputGeneratorFile+'_old.events')
modify_param_card(param_card_input=runArgs.inputGeneratorFile+'_old.events',params={'DECAY':decays},output_location=runArgs.inputGeneratorFile+'.events')
os.unlink(runArgs.inputGeneratorFile+'_old.events.old_to_be_deleted')


if njets>0:
    genSeq.Pythia8.Commands += ['Merging:Process = guess']
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
