from MadGraphControl.MadGraphUtils import *
import math, subprocess, os, shutil

safefactor=1.1
nevents=runArgs.maxEvents*safefactor

#==================================================================================

# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

process = '''
import model SMEFTsim_U35_MwScheme_UFO-massless_Htautau_CPV
generate p p > h j j QCD=0 /a NP<=1
output -f
'''
process_dir = new_process(process)


#==================================================================================

#Fetch default LO run_card.dat and set parameters
extras = { 
            'nevents'     : nevents,
            'lhe_version' : '3.0', 
            'cut_decays'  : 'F', 
            'pdlabel'     : 'lhapdf',
            'lhaid'       : 260000,
#            'ickkw'       : ickkw,
            'ptj'         : 20.,
            'ptb'         : 20.,
            'use_syst'    : "False" 
         }

#==================================================================================

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#==================================================================================
#Building param_card setting to 0 c_is that are not of interest

parameters={'smeftcpv':
    {
    'cHWtil'       : 0.0,
    'cHBtil'       : 0.3,
    'cHWBtil'      : 0.0,
    }
}

modify_param_card(process_dir=process_dir,params=parameters)

#==================================================================================

rcard = open('reweight_card.dat','w')

reweightCommand="""

launch --rwgt_name=cHBtil_m_5p0
set smeftcpv 5 -5.0

launch --rwgt_name=cHBtil_m_3p0
set smeftcpv 5 -3.0

launch --rwgt_name=cHBtil_m_2p0
set smeftcpv 5 -2.0

launch --rwgt_name=cHBtil_m_1p5
set smeftcpv 5 -1.5

launch --rwgt_name=cHBtil_m_1p0
set smeftcpv 5 -1.0

launch --rwgt_name=cHBtil_m_0p75
set smeftcpv 5 -0.75

launch --rwgt_name=cHBtil_m_0p5
set smeftcpv 5 -0.5

launch --rwgt_name=cHBtil_m_0p25
set smeftcpv 5 -0.25

launch --rwgt_name=cHBtil_m_0p1
set smeftcpv 5 -0.1

launch --rwgt_name=cHBtil_p_0p1
set smeftcpv 5 0.1

launch --rwgt_name=cHBtil_p_0p25
set smeftcpv 5 0.25

launch --rwgt_name=cHBtil_p_0p5
set smeftcpv 5 0.5

launch --rwgt_name=cHBtil_p_0p75
set smeftcpv 5 0.75

launch --rwgt_name=cHBtil_p_1p0
set smeftcpv 5 1.0

launch --rwgt_name=cHBtil_p_1p5
set smeftcpv 5 1.5

launch --rwgt_name=cHBtil_p_2p0
set smeftcpv 5 2.0

launch --rwgt_name=cHBtil_p_3p0
set smeftcpv 5 3.0

launch --rwgt_name=cHBtil_p_5p0
set smeftcpv 5 5.0

"""

rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

#==================================================================================

print_cards()

#==================================================================================

generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================

evgenConfig.description = 'MadGraph_SMEFT_VBF_Htautau_cHBtil_reweight'
evgenConfig.keywords    = ['Higgs']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['huanguo.li@cern.ch']
evgenConfig.nEventsPerJob = 10000 


#==================================================================================
# Pythia 8 showering setup

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Dipole option Pythia8
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]



