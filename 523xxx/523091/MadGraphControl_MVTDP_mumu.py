import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re
import math, cmath
import os

safefactor=1.2
nevents=runArgs.maxEvents*safefactor


pid_list = ["60000","60001"]
whitelist_file = open("pdgid_extras.txt", "w")
for pid in pid_list:
    whitelist_file.write(pid+"\n-"+pid+"\n")
whitelist_file.close()

testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'


print "ARGS: ", runArgs.jobConfig[0]
MT = float(jofile.rstrip('.py').split('_')[2])
print("MVLT ", MT)

decay_60001="""DECAY   60001   1.000000e+00 # WVLT
     1   2   6   60000
"""

decay_60000="""DECAY   60000   1.000000e-01 # WAD
     1   2   13   -13
"""


param_card_extras = {}
param_card_extras['MASS'] = {'60001':MT,'60000':0.5}
param_card_extras['DECAY'] = {'60001':decay_60001,'60000':decay_60000}


my_process = """
import model mavtop_ufo
#import model /afs/cern.ch/user/d/dkar/work/MavTopNew/UFO/mavtop_ufo
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d b s u~ c~ d~ b~ s~ 
define vltvlt = vlt vlt~
generate p p > vltvlt j
output -f
"""

settings = {'nevents':nevents, 'maxjetflavor':5} 
process_dir = new_process(my_process)
runName='run_01'

modify_param_card(process_dir=process_dir,params=param_card_extras) 
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'You run with an athena MP-like whole-node setup. Re-configureing to run remainder of the job serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts


# Shower 
evgenConfig.description = "Top Partner with Dark Photon"
evgenConfig.keywords+=['BSM']
evgenConfig.generators+=["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact  = ['deepak.kar@cern.ch']
evgenConfig.process = "p p --> vltvlt j"
PYTHIA8_nQuarksMerge=settings['maxjetflavor']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands+=["6:onMode = off"]
genSeq.Pythia8.Commands+=["6:onIfAny = 5 24"]
genSeq.Pythia8.Commands+=["24:onMode = off"]
genSeq.Pythia8.Commands+=["24:onIfAny = 1 2 3 4 5 -1 -2 -3 -4 -5"]

