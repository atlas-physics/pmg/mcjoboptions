# definitions that will be used for process
definitions="""
"""
#import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTsim_A_U35_MwScheme_UFO  

# process (EFTORDER: NP<=1)
processes={
           'eva' : 'generate p p > e+ ve a EFTORDER\n add process p p > e- ve~ a EFTORDER\n',
           'muva' : 'generate p p > mu+ vm a EFTORDER\n add process p p > mu- vm~ a EFTORDER\n',
           'lva' : 'generate p p > mu+ vm a EFTORDER\n add process p p > mu- vm~ a EFTORDER\n add process p p > e+ ve a EFTORDER\n add process p p > e- ve~ a EFTORDER\n',
}

# cuts and other run card settings
settings={
          'dynamical_scale_choice':'4',
          'ptl':'22', 
          'etal':'3',
          'pta':'22',
}

# param card settings 
params={}

# EFT parameters with non-zero values for initial sample
smeftpars_bsm={'cW': 1,
               'cHWB': 1, 
               'cWtil': 1, 
               'cHWBtil': 1}

# parameters used in reweighting:
relevant_coeffs = 'cW cHWB cWtil cHWBtil'.split()

evgenConfig.description = 'lva production with SMEFTsim'
evgenConfig.keywords+=['SM','diboson','Wgamma','electroweak']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Antonio Costa <antonio.jacques.costa@cern.ch>']

include("MadGraphControl_SMEFT_lva.py")
