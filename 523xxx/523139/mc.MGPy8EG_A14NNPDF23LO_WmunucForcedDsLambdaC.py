evgenConfig.description = "aMC@NLO W+/W- -> enu +charm forced D+,D0 decays"
evgenConfig.keywords = ["SM", "W", "electron", "jets", "NLO" ]
evgenConfig.contact  = [ "mdshapiro@lbl.gov" ]
evgenConfig.nEventsPerJob = 10000

isPythia = True
useCKM = True
We = False
Wmu = True

include("MadGraphControl_Wc2.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.userDecayFile = "DsToPhiPiLambdaCTopkpi.dec"
evgenConfig.auxfiles += [ 'DsToPhiPiLambdaCTopkpi.dec']


include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 15000.
filtSeq.LeptonFilter.Etacut = 2.7

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
DsHadronFilter = HeavyFlavorHadronFilter("DsHadronFilter")
filtSeq +=    DsHadronFilter
DsHadronFilter.RequestSpecificPDGID=True
DsHadronFilter.RequestCharm = False
DsHadronFilter.RequestBottom = False
DsHadronFilter.Request_cQuark = False
DsHadronFilter.Request_bQuark = False
DsHadronFilter.PDGPtMin=7.0*GeV
DsHadronFilter.PDGEtaMax=2.3
DsHadronFilter.PDGID=431
DsHadronFilter.PDGAntiParticleToo=True

DpHadronFilter = HeavyFlavorHadronFilter("DpHadronFilter")
filtSeq +=    DpHadronFilter
DpHadronFilter.RequestSpecificPDGID=True
DpHadronFilter.RequestCharm = False
DpHadronFilter.RequestBottom = False
DpHadronFilter.Request_cQuark = False
DpHadronFilter.Request_bQuark = False
DpHadronFilter.PDGPtMin=7.0*GeV
DpHadronFilter.PDGEtaMax=2.3
DpHadronFilter.PDGID=411
DpHadronFilter.PDGAntiParticleToo=True

LambdaCHadronFilter = HeavyFlavorHadronFilter("LambdaCHadronFilter")
filtSeq +=    LambdaCHadronFilter
LambdaCHadronFilter.RequestCharm = False
LambdaCHadronFilter.RequestBottom = False
LambdaCHadronFilter.Request_cQuark = False
LambdaCHadronFilter.Request_bQuark = False
LambdaCHadronFilter.RequestSpecificPDGID = True
LambdaCHadronFilter.RequireTruthJet = False
LambdaCHadronFilter.RequestCharm=False
LambdaCHadronFilter.PDGPtMin=7.0*GeV
LambdaCHadronFilter.PDGEtaMax=2.3
LambdaCHadronFilter.PDGID=4122
LambdaCHadronFilter.PDGAntiParticleToo=True

filtSeq.Expression="LeptonFilter and ((DpHadronFilter) or (DsHadronFilter) or (LambdaCHadronFilter))"

