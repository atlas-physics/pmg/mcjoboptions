from MadGraphControl import MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from os import path


process = """

import model Type-II_seesaw_mix_UFO
define p = g u c d s b t  u~ c~ d~ s~ b~ t~
define j = g u c d s u~ c~ d~ s~
define EXCL = H ha h
define Hpp = H++ H--
define Hp = H+ H-
define w =  w+ w-
define z0 = z
define l = e+ mu+ e- mu-
define n = vl
output -f """
  

process_dir = new_process()

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F',
	    
             }



pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
9000055
-9000055
9000037
-9000037
""")
pdgfile.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'



############################
# Shower JOs will go here
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.generators = ["MadGraph"]


evgenConfig.description = 'Type-II_seesaw_mix'
evgenConfig.keywords+=['BSM']
evgenConfig.contact +=['Sulman Younas<sulman.younas@cern.ch>','Tudorache Alexandra<alexandra.tudorache@cern.ch>']



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.NLeptons = 1
MultiLeptonFilter.Etacut = 2.7



