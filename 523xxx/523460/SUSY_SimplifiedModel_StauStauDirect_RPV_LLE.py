#change BRs and width
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

### helpers
def StringToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
splits=phys_short.split('_')

#keepOutput = True

# 2: Stau or StauRPV
rpvmodeint=0
if splits[2]=="StauRPV":
    rpvmodeint=1
elif splits[2]=="StauRPCRPV":
    rpvmodeint=2

# 3,4: mass
mslep      = StringToFloat(splits[3])
mn1        = StringToFloat(splits[4])
masses['1000011'] = mslep
masses['1000013'] = mslep
masses['1000015'] = mslep
masses['2000011'] = mslep
masses['2000013'] = mslep
masses['2000015'] = mslep
masses['1000022'] = mn1
evgenLog.info('DirectStau: generation of stau-pair production; grid point decoded into m(stau) = %f GeV, m(N1) = %f GeV' % (mslep, mn1))

# 5: lambda 133/233
rpvtype=splits[5]
evgenLog.info("RPV coupling parameter: "+ rpvtype)

try :
    longlived = True
    neutralinoLifetime = StringToFloat(splits[6].replace("ns",""))
    additional_options = splits[7:]
except :
    longlived = False
    additional_options = splits[6:]

if (longlived):
    hbar = 6.582119514e-16
    decayWidth = hbar/float(neutralinoLifetime)
    decayWidthStr = '%e' % decayWidth
    decayStringHeader = 'DECAY   1000022  '
    header = decayStringHeader + decayWidthStr 
    evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)

evgenLog.info("DirectStau: Additional options: " + str(additional_options))

process = '''
import model RPVMSSM_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate    p p > ta1- ta1+      RPV=0  $  susystrong  @1
add process p p > ta2- ta2+      RPV=0  $  susystrong  @2
add process p p > ta1- ta1+ j    RPV=0  $  susystrong  @3
add process p p > ta2- ta2+ j    RPV=0  $  susystrong  @4
add process p p > ta1- ta1+ j j  RPV=0 QED=2 QCD=99 $  susystrong  @5
add process p p > ta2- ta2+ j j  RPV=0 QED=2 QCD=99 $  susystrong  @6
'''
useguess = True

###

if rpvmodeint==2: # stau half rpv, half rpc; n1 prv prompt decay.
    if rpvtype == "133":
        decays['1000015'] = """DECAY   1000015     1.0  # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2    1000022  15
            0.5000  2    15  -12 #  stauL > tau nu_e
        """
        decays['2000015'] = """DECAY   2000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2    1000022  15
            0.2500  2   11   16 # stauR > e nu_tau
            0.2500  2   12   15 # stauR > nu_e tau
        """
        decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
        #          BR          NDA       ID1       ID2       ID3
            0.25000  3    15    -15     12  # tau tau nu_e
            0.25000  3    15    -15    -12  # tau tau nu_e
            0.25000  3    15    -16    -11  # tau nu_tau e
            0.25000  3    -15    16     11  # tau nu_tau e
        """
    if rpvtype == "233":
        decays['1000015'] = """DECAY   1000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2    1000022  15
            0.5000  2    15  -14 #  stauL > tau nu_mu
        """
        decays['2000015'] = """DECAY   2000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2    1000022  15
            0.2500  2   13   16 # stauR > mu nu_tau
            0.2500  2   14   15 # stauR > nu_mu tau
        """
        decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
        #          BR          NDA       ID1       ID2       ID3
            0.25000  3    15    -15     14  # tau tau nu_mu
            0.25000  3    15    -15    -14  # tau tau nu_mu
            0.25000  3    15    -16    -13  # tau nu_tau mu
            0.25000  3    -15    16     13  # tau nu_tau mu
        """

###
if rpvmodeint==1: # stau rpv decay
    if rpvtype == "133":
        decays['1000015'] = """DECAY   1000015     1.0  # stau decays
        #          BR          NDA       ID1       ID2       ID3
            1.000  2    15  -12 #  stauL > tau nu_e
        """
        decays['2000015'] = """DECAY   2000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2   11   16 # stauR > e nu_tau
            0.5000  2   12   15 # stauR > nu_e tau
        """
    if rpvtype == "233":
        decays['1000015'] = """DECAY   1000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            1.000  2    15  -14 #  stauL > tau nu_mu
        """
        decays['2000015'] = """DECAY   2000015     1.0   # stau decays
        #          BR          NDA       ID1       ID2       ID3
            0.5000  2   13   16 # stauR > mu nu_tau
            0.5000  2   14   15 # stauR > nu_mu tau
        """

if rpvmodeint==0: # n1 rpv decay
    decays['1000015'] = """DECAY   1000015     1.483273e-01   # stau decays
    #          BR          NDA       ID1       ID2       ID3
        1.0000  2    1000022  15
    """
    decays['2000015'] = """DECAY   2000015     2.699061e-01   # stau decays
    #          BR          NDA       ID1       ID2       ID3
        1.0000  2    1000022  15
    """
    if rpvtype == "133":
        branchingRatios = """
        #          BR          NDA       ID1       ID2       ID3
            0.25000  3    15    -15     12  # tau tau nu_e
            0.25000  3    15    -15    -12  # tau tau nu_e
            0.25000  3    15    -16    -11  # tau nu_tau e
            0.25000  3    -15    16     11  # tau nu_tau e
        #"""         
    if rpvtype == "233":
        branchingRatios = """
        #          BR          NDA       ID1       ID2       ID3
            0.25000  3    15    -15     14  # tau tau nu_mu
            0.25000  3    15    -15    -14  # tau tau nu_mu
            0.25000  3    15    -16    -13  # tau nu_tau mu
            0.25000  3    -15    16     13  # tau nu_tau mu
        #"""         
    if not longlived:
        header="DECAY   1000022     1.0  # neutralino decays"
    evgenLog.info("DirectStau: Lifetime header: " + header)
    decays['1000022'] = header + branchingRatios

# Set up a default event multiplier
#evt_multiplier = 2

evgenLog.info('DirectStau: No filter is applied')

param_blocks['selmix'] = {}
if 'mmix' in additional_options:
    # use maximally mixed staus
    evgenLog.info("DirectStau: Maxmix scenario selected.")
    param_blocks['selmix'][ '3   3' ] = '0.70710678118' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.70710678118' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '-0.70710678118' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '0.70710678118' # # RRl6x6
else:
    # No mixing
    param_blocks['selmix'][ '3   3' ] = '1.0' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.0' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '0.0' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '1.0' # # RRl6x6

njets=2

usePMGSettings = True

evgenConfig.contact  = ["jiarong.yuan@cern.ch"]

if not longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'stau', 'simplifiedModel', 'neutralino']
  evgenConfig.description = 'stau pair production and decay via RPV lamba133, or decay to tau and neutralino, which then decays via RPV lambda133, m_stau= %s GeV, m_N1 = %s GeV'%(masses['1000015'],masses['1000022'])

if longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'stau', 'simplifiedModel', 'neutralino', 'longLived']
  evgenConfig.description = 'stau pair production and decay to taus and long-lived neutralino, which then decays via RPV lambda133, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])
  testSeq.TestHepMC.MaxVtxDisp = 1e8 # in mm
  testSeq.TestHepMC.MaxTransVtxDisp = 1e8

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
