import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os,subprocess,fileinput

# General settings 

evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*1.1
#gridpack_mode=True
#gridpack_dir='madevent/'

name = 'tqgammaSM_tchan_4fl'
runName='mc.MadGraph_'+str(name)

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~ 
define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
define vl = ve vm vt 
define vl~ = ve~ vm~ vt~
generate p p > t b~ j a $$ w+, (t > l+ vl b)
add process p p > t b~ j $$ w+, (t > l+ vl b a)
add process p p > t~ b j a $$ w-, (t~ > l- vl~ b~)
add process p p > t~ b j $$ w-, (t~ > l- vl~ b~ a) 
output -f"""

# process_dir = new_process()#grid_pack=gridpack_dir)
process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters

extras = { 'cut_decays'    : 'T', 
           #'parton_shower' :'PYTHIA8',
           'pta'           :10,
           'maxjetflavor'  :4,
           'drab'          :0.2, 
           'draj'          :0.2, 
           'dral'          :0.2,
           'dynamical_scale_choice': '3', #sum of the transverse mass divided by 2
           'ptl'           :0.0,
           'ptj'           :0.0,
           'etal'          :5.0,
           'etaa'          :5.0,
           'etaj'          :-1,
           'etab'          :-1,
           'drjj'          :0.0,
           'drjl'          :0.0,
           'drll'          :0.0,
           'draa'          :0.0,
           'nevents'       : nevents,
           }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


#### Shower
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.keywords+= ['SM', 'top',  'photon','singleTop','lepton'] 
evgenConfig.contact = ['nils.julius.abicht@cern.ch']
runArgs.inputGeneratorFile=outputDS+".events"


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")



if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): 
        mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print(opts)

include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")