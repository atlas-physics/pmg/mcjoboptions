import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os
import re

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
nevents=runArgs.maxEvents*1.1

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""6100242
-6100242
""")
pdgfile.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

#JOname = runArgs.jobConfig[0]
JOname = job_option_name  
     
#LQs in the model
lqType = ""
lqPDGID = 0
yue = 0
yumu = 0
yuta = 0
yce = 0
ycmu = 0
ycta = 0
yte = 0
ytmu = 0
ytta = 0 
if "_S1_" in JOname:
    lqType = 's1'  
    lqPDGID = 6100242
else:
    raise RuntimeError("Cannot find LQ type in JO name.")

if not lqType:
    raise RuntimeError("No LQ type set.")
if not lqPDGID:
    raise RuntimeError("No LQ PDG ID set.")

matchesVlqMass = re.search("Mlq([0-9]+).py", JOname)
print "matchesVlqMass", matchesVlqMass
if matchesVlqMass is None:
    raise RuntimeError("Cannot find vlq mass string in JO name.")     
else:
    lqmass = float(matchesVlqMass.group(1))

print "lqmass", lqmass

evgenConfig.description = ('Single production of scalar leptoquark, {0}, mLQ={1:d}').format(lqType,int(lqmass))

fcard = open('proc_card_mg5.dat','w')
process="""
import model sm
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/sleptoquark_top_UFO
set zerowidth_tchannel False
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0

define ee = e+ e-
define mm = mu+ mu-
define tt = t t~
define lq = s1 s1~

define q = u c d s b
define q~ = u~ c~ d~ s~ b~
define bb = b b~
define ww = w+ w-
define qq = q q~
define vv = ve vm vt ve~ vm~ vt~
define ll = e+ mu+ ta+ e- mu- ta-
define Dec = ve vm vt ve~ vm~ vt~ u c  d s  u~ c~  d~ s~ e- mu- ta- e+ mu+ ta+

generate p p > ee mm tt, ( tt > ww bb, ww > Dec Dec)
output -f

"""

fcard.write(process)
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


process_dir = new_process(process,keepJpegs=True)

settings = {
    'nevents'      : nevents,      
 #   'pdlabel'      :'lhapdf',
 #   'lhaid'        :'260000',
    'scale'        :lqmass,
    'dsqrt_q2fact1':lqmass,
    'dsqrt_q2fact2':lqmass,
    'hard_survey'  : 1
#    'fixed_ren_scale' : True,
#    'fixed_fac_scale' : True,
#    'lhe_version': 3.0,
#    'clusinfo': True,
#    'ickkw': 0,
#    'alpsfact': 1.00,
#    'chcluster': False,
#    'asrwgtflavor': 4,
#    'auto_ptj_mjj': False,
#    'xqcut': 0.000000, 
#    'ptgmin': 0.0,
#    'R0gamma': 0.4,
#    'xn': 1.0,
#    'epsgamma': 1.0,
#    'isoEM': True,
#    'ktdurham': -1.0,
#    'dparameter': 0.4,
#    'ptlund': -1.0,
#    'pdgs_for_merging_cut': "1 2 3 4 5 6 21 1000 1000", 
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'MASS':{
        '6100242' : '{:e}'.format(lqmass),
    },
    'FRBLOCK' :{
      '1'  :  '{:e}'.format(lqmass) + ' # mlq',
      '2'  :  '0.050000e+00 # yue',
      '3'  :  '0.500000e+00 # yumu',
      '4'  :  '0.000000e+00 # yuta',
      '5'  :  '0.000000e+00 # yce',
      '6'  :  '0.000000e+00 # ycmu',
      '7'  :  '0.000000e+00 # ycta',
      '8'  :  '5.000000e+00 # yte',
      '9'  :  '50.00000e+00 # ytmu',
      '10' :  '0.000000e+00 # ytta'

    },
    'DECAY':{
        '6': 'Auto',
        '24': 'Auto',
        '6100242' : 'Auto'
    }
}

modify_param_card(process_dir=process_dir, params=parameters)

print_cards(run_card='run_card.dat', param_card='param_card.dat')
      
runName='run_01'     

generate(process_dir=process_dir, runArgs=runArgs)

arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = ('Single production of s1 scalar leptoquarks, {0}, mLQ={1:d}').format(lqType,int(lqmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark', 'top']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> mu tau t'
evgenConfig.contact = ["Jacob Kempster <jacob.julian.kempster@cern.ch>,Mahsana Haleem <mahsana.haleem@cern.ch>"]

