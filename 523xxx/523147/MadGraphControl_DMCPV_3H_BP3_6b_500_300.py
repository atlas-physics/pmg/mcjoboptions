##################### MY PARTS GO HERE
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import *
import math
import os
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

keepOutput=True
gridpack_mode = True
generate_gridpack = False

## Can edit this with Safe-Factor Method
if not generate_gridpack:
   nevents = runArgs.maxEvents*4
else: 
   nevents = runArgs.maxEvents
# Scale up the events to make it more likely pythia will be happy

if hasattr(runArgs, 'ecmEnergy'):
   beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

# H3 = iota0, H2 = eta0 heavy scalars, H=hSM
# M3>M2>M1
# Benchmark scenario 1 (BP1)
# Parameters chosen to maximise iota0 production and Br(iota0->H+eta0) (Potentially, need to still epxlore)
# Sample job options should have name such as mc.MGPy8EG_A14N23LO_TRSM_HHH_bbbbbb_500p0_200p0.py
# Above line is just a template. Will be changed later

# only works for local running
# f_list = os.listdir(runArgs.jobConfig[0])
# for i in f_list:
#   if "mc" in os.path.splitext(i)[0] : 
#     JO = i

# works for local/grid running
# ### get MC job-options filename
# FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
# jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]
# jobConfig=jofiles[0]

# #Parsing JO file in the same fashion as before
# sample_name_list = jobConfig.strip('\.py').split('_')


# ### get MC job-options filename
joName = get_physics_short()
# #Parsing JO file in the same fashion as before
sample_name_list = joName.split('_')


def sample_name_to_parameters(sample_name_list):
  topology = sample_name_list[-5]
  Benchmark = sample_name_list[-4]
  decay = sample_name_list[-3]
  M3_str = sample_name_list[-2]
  M2_str = sample_name_list[-1].replace(".GRID.tar.gz","")
  return M3_str,M2_str,Benchmark,topology,decay

M3_str,M2_str,Benchmark,topology,decay = sample_name_to_parameters(sample_name_list)

#Here we want to just import the pre-defined model stand-alone, without setting parameters in the parameter card.

mgproc = None

run_card_extras = {}
runName = 'aMCPy8.DMCPV_{0}_{1}_{2}_{3}_{4}'.format(Benchmark,topology,decay,M3_str,M2_str)


mgproc="""
  generate g g > h3 > h2 h  [QCD]

"""

output = """
output -f
"""

process_str = """
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/DMCPV_NLO/
"""+mgproc+output

m_description = 'MadGraph+Pythia8+A14NNPDF30NLO production pp~>hhh~>bbbbbb in DM-CPV {0} model, m_X = {1} GeV, m_S = {2} GeV'.format(Benchmark, M3_str, M2_str)

evgenConfig.description = m_description
evgenConfig.keywords += ['BSMHiggs']
run_card_extras['fixed_ren_scale'] =  'True' #
run_card_extras['fixed_fac_scale'] =  'True' #
run_card_extras['scale'] = 125.5 #
run_card_extras['dsqrt_q2fact1'] = 125.5 #
run_card_extras['dsqrt_q2fact2'] = 125.5
run_card_extras['nevents'] = nevents
run_card_extras['cut_decays'] = 'False'

process_dir = new_process(process_str)

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_card_extras)

### Here we set the couplings needed for each signal model point
M2 = 300
M3 = 500
vs = 241
delta2 = -3.27 
lamX = 1.0973358347
msq = -51313.63587
lam = 0.142284329
Rdelta1 = 1058.242127
Idelta1 = 993.2925074
Rb1 = -131726.6351
Ib1 = -25654.35376
Rd1 = -2.824227101
Id1 = 1.494670099
d2 =  7.532479454

# model masses for param card
masses = {'35':M2,'36':M3}
# model NP's for param card
NPs = {'15':vs,'4':delta2,'14':lamX}
# model FRBlock's for param card
FRBlock = {'1':msq,'2':lam}
# model Depedent for param card
Dependent = {'5':Rdelta1,'6':Idelta1,'1':Rb1,'2':Ib1,'3':Rd1,'4':Id1,'7':d2}

### Now we set the parameter card features we need for each model production
param_card_extras = {}
param_card_extras['DECAY'] = {'35':'auto','36':'auto'}
param_card_extras['MASS'] = masses
param_card_extras['NP'] = NPs
param_card_extras['FRBlock'] = FRBlock
param_card_extras['Dependent'] = Dependent

modify_param_card(param_card_input='aMcAtNlo_param_card_DMCPV_HHH.dat',process_dir=process_dir, params=param_card_extras)

generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True)

evgenConfig.contact = ['nkyriaco@umich.edu']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
if not generate_gridpack:
   genSeq.Pythia8.Commands += [
        'Higgs:useBSM = on',
        '25:onMode = off',
        '35:onMode = off',
        '25:onIfMatch = 5 -5',
        '35:onIfMatch = 25 25'] #to specify the decay pathway

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
if not hasattr( filtSeq, "MultiBjetFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
    filtSeq += MultiBjetFilter()
    pass
MultiBjetFilter = filtSeq.MultiBjetFilter
MultiBjetFilter.NJetsMin = 4
MultiBjetFilter.NBJetsMin = 4
MultiBjetFilter.JetPtMin = 25*GeV
