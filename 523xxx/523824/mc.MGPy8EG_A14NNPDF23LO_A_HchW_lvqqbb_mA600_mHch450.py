import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------

tanb = 2.  # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
cosba = 0. # The cosine of the difference of the mixing angles $\cos (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.


A_width = 17.25
Hp_width = 4.503


mH2 =450. # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
mA =600. # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
mHp=450.  # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.

m12=mA*mA*tanb/(1 + tanb*tanb)   #defined as such in....

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------


# Make sure 2HDM PDG IDs are known to TestHepMC:
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
35
36
37
-37
""")
pdgfile.close()

process='''
import model 2HDM_TypeI_EFF
define p = g d u s c d~ u~ s~ c~
define j = g d u s c d~ u~ s~ c~
define lv = e- e+ mu- mu+ ve ve~ vm vm~


generate p p > h3 @0, (h3 > h+ w-, (h+ > t b~, (t > w+ b, (w+ > j j))), (w- > lv lv))
add process p p > h3 @0, (h3 > h+ w-, (h+ > t b~, (t > w+ b, (w+ > lv lv))), (w- > j j))
add process p p > h3 @0, (h3 > h- w+, (h- > t~ b,(t~ > w- b~, (w- > j j))), (w+ > lv lv))
add process p p > h3 @0, (h3 > h- w+, (h- > t~ b,(t~ > w- b~, (w- > lv lv))), (w+ > j j))

add process p p > h3 j @1, (h3 > h+ w-, (h+ > t b~,(t > w+ b, (w+ > j j))), (w- > lv lv))
add process p p > h3 j @1, (h3 > h+ w-, (h+ > t b~,(t > w+ b, (w+ > lv lv))), (w- > j j))
add process p p > h3 j @1, (h3 > h- w+, (h- > t~ b,(t~ > w- b~, (w- > j j))), (w+ > lv lv))
add process p p > h3 j @1, (h3 > h- w+, (h- > t~ b,(t~ > w- b~, (w- > lv lv))), (w+ > j j))

output -f
'''

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RuntimeError('No center of mass energy found.')

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------

nevents=evgenConfig.nEventsPerJob
multiplier=1.1 
nevents = runArgs.maxEvents*multiplier if runArgs.maxEvents>0 else nevents*multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

extras = {
    'maxjetflavor'  : 4,
    'lhe_version'	: '3.0',
    'cut_decays'  : 'F',
    'nevents'  : nevents,
    'xqcut' :mA*0.5,
    'etal' : 10,
    'etaj':10,
    'ptj':0,
    'ickkw' : 1,
    'auto_ptj_mjj' : 'False'
}


#
# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

thdm={'1':tanb,'2':cosba,'3':m12,}

widths={'36':"""DECAY 36 %g # WHA""" % A_width,'37':"""DECAY 37 %g # WHp""" % Hp_width}

masses={'35':mH2, '36':mA, '37':mHp}


modify_param_card(process_dir=process_dir,params={'MASS':masses,'THDMBLOCK':thdm,'DECAY':widths})


#---------------------------------------------------------------------------
# Generate the events
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#---------------------------------------------------------------------------
# Metadata
#---------------------------------------------------------------------------

evgenConfig.process = 'g g > h3 > h+- w-+ '
initialStateString = 'gluon fusion'

evgenConfig.description = '2HDM for A->WH(->WWbb) %s initiated process with mA = %s, mH = %s' % (initialStateString, str('mA'), str('mH'))
evgenConfig.keywords = ['BSMHiggs','WHiggs']
evgenConfig.contact = ['rachel.ashby.pickering@cern.ch']

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------

PYTHIA8_merge=1
PYTHIA8_scheme=1
PYTHIA8_nJetMax=1
PYTHIA8_qcut=mA*0.75
PYTHIA8_nQmatch=4
PYTHIA8_doShowerKt=0

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

