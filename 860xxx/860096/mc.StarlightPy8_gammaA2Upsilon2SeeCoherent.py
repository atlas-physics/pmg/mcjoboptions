evgenConfig.description = "Starlight+Pythia8 gamma + A UPC collisions at 5020 GeV to Upsilon(2S) -> 2 e, breakup mode 5, pT(e)>2.2GeV, |eta(e)|<2.6"
evgenConfig.keywords = ["coherent","2lepton"]
evgenConfig.contact = ["jakub.kremer@cern.ch"]
evgenConfig.nEventsPerJob = 10000

if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

from Starlight_i.Starlight_iConf import Starlight_i
# The name "ParticleDecayer" is needed due to removal of dummy event when running with Pythia8 (GenModule feature)
genSeq += Starlight_i("ParticleDecayer")
genSeq.ParticleDecayer.McEventKey = "GEN_EVENT"

evgenConfig.generators += ["Starlight"]

from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT"]
del _evgenstream


genSeq.ParticleDecayer.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beam1Gamma 2705",   #Gamma of the colliding ion1, for sqrt(nn)=5.02 TeV
     "beam2Gamma 2705",   #Gamma of the colliding ion2, for sqrt(nn)=5.02 TeV
     "maxW -1", #Max value of w
     "minW -1", #Min value of w
     "nmbWBins 400", #Bins n w
     "maxRapidity 9.", #max y
     "nmbRapidityBins 1000", #Bins n y
     "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 2.2", #Minimum pT in GeV
     "maxPt 100.0", #Maximum pT in GeV
     "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -2.6", #Minimum pseudorapidity
     "maxEta 2.6", #Maximum pseudorapidity
     "productionMode 2", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 554011", #Channel of interest
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 0", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]

genSeq.ParticleDecayer.lheOutput = True
genSeq.ParticleDecayer.maxevents = int(1.1*runArgs.maxEvents)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = 1' ]
genSeq.Pythia8.Commands += [ 'TauDecays:mode = 0' ]



