evgenConfig.description = "Processes HEPMC files for photonuclear events generated with overlaid STARlight processes of "\
                            "two UPC-muons (m>7.5 GeV, 3.5<pT(mu), |eta(mu)|<2.6) and "\
                            "a pion pair from 1)rho0->pi,pi or 2) direct pi,pi including interference between the processes (pT(pi)>0.05 GeV, |eta(pi)|<2.6)."\
                            "The CS for the latter process is 2.714b."\
                            "Note that this CS doesnot correspond to the combined (overlayed) process."\
                            "Therefore, this sample should only be used for tracking eff calculation,"\
                            " and not for physics signal/background estimates"
evgenConfig.keywords      = ["QED","coherent"]
evgenConfig.tune          = "none"
evgenConfig.contact       = ["soumya.mohapatra@cern.ch"]
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 1

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]

del testSeq.TestHepMC
