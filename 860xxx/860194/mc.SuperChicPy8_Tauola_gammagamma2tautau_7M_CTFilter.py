"""
Bassed on: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/860xxx/860106/mc.SuperChicPy8_gammagamma2mumu_7M20.py
"""

from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun

evgenConfig.description = 'Superchic gamma + gamma to mu mu Pb+Pb UPC collisions'
evgenConfig.keywords = ['2photon', '2lepton']
# evgenConfig.weighting = 0
evgenConfig.contact = ['kartik.deepak.bhide@cern.ch']
evgenConfig.generators += ['Superchic']
evgenConfig.nEventsPerJob = 2000

scConfig = SuperChicConfig(runArgs)

safety_factor = 3
nEvents = runArgs.maxEvents*safety_factor

scConfig.isurv = 4
scConfig.PDFname = 'MMHT2015qed_nnlo'
scConfig.PDFmember = 0
scConfig.proc = 58
scConfig.beam = 'ion'
scConfig.sfaci = True
scConfig.diff = 'el'
scConfig.genunw = True
scConfig.ymin = -9.0
scConfig.ymax = 9.0
scConfig.mmin = 7
scConfig.mmax = 200
scConfig.gencuts = True
scConfig.scorr = True
scConfig.fwidth = True
scConfig.ptxmax = 10000000000
scConfig.ptamin = 0.0
scConfig.ptbmin = 0.0
scConfig.etaamin = -5.0
scConfig.etaamax = 5.0
scConfig.etabmin = -5.0
scConfig.etabmax = 5.0
scConfig.acoabmax = 100000000000
scConfig.mcharg = 120
scConfig.mneut = 100
scConfig.nev = nEvents

SuperChicRun(scConfig, genSeq)

from MCJobOptionUtils import LheConverterUpc as LC
lc = LC.LheConverterUpc(generator='Superchic', mode='Tauolapp')
lc.fileName = "evrecs/evrecout.dat"
genSeq += lc

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += ["15:onMode = off"]
from Tauolapp_i.Tauolapp_iConf import TauolaPP
genSeq += TauolaPP()
TauolaPP.decay_mode_same = 0
TauolaPP.decay_mode_opposite = 0
TauolaPP.decay_particle = 15
TauolaPP.tau_mass = 1.77684
TauolaPP.spin_correlation=True
TauolaPP.setRadiation=False
evgenConfig.generators += [ "TauolaPP" ] 
include("Pythia8_i/Pythia8_Photospp.py")
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = 1' ]
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByL = 1' ]

include('GeneratorFilters/ChargedTrackFilter.py')
filtSeq.ChargedTracksFilter.NTracks = 1
filtSeq.ChargedTracksFilter.NTracksMax = -1
filtSeq.ChargedTracksFilter.Ptcut = 1500.0
filtSeq.ChargedTracksFilter.Etacut= 2.6
