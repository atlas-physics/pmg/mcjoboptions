# JOs based on 420000
###############################################################
#
# Job options file for Hijing generation of central
# Pb + Pb collisions at 5020 GeV/(colliding nucleon pair)
#
# Flow with new LHC physics flow full v2-v6
# using function jjia_minbias_new
#
# impact oarameter > 12 fm
# =========================================
# Version for > FlowAfterburner-00-02-00
# =========================================
#
#==============================================================

# use common fragment
include("Hijing_i/Hijing_Common.py")

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
genSeq += AddFlowByShifting()

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["minBias"]
evgenConfig.contact = ["Tomasz.Bold@cern.ch"]
evgenConfig.nEventsPerJob = 10000

if hasattr(runArgs, 'ecmEnergy'):
    ecm=f"efrm {runArgs.ecmEnergy}"
else:
  raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = [ecm, "frame CMS", "proj A", "targ A",
                    "iap 208", "izp 82", "iat 208", "izt 82",
# impact parameter bigger than 12 fm
                    "bmin 12",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]

AddFlowByShifting = genSeq.AddFlowByShifting
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "GEN_EVENT"

#----------------------
# Flow Parameters
#----------------------
AddFlowByShifting.FlowFunctionName = "jjia_minbias_new"
AddFlowByShifting.FlowImplementation = "exact"

AddFlowByShifting.RandomizePhi  = 0


