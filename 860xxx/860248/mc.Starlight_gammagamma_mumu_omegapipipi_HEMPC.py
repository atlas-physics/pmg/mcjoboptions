evgenConfig.description = "Processes HEPMC files for photonuclear events generated with overlaid STARlight processes of "\
                            "two UPC-muons (m>7.5 GeV, 3.5<pT(mu), |eta(mu)|<2.6) and "\
                            "a pion triplet from 1)omega->pi,pi,pi."\
                            "The CS for the latter process is unknown."\
                            "Therefore, this sample should only be used for tracking eff calculation,"\
                            " and not for physics signal/background estimates"
evgenConfig.keywords      = ["QED","coherent"]
evgenConfig.tune          = "none"
evgenConfig.contact       = ["soumya.mohapatra@cern.ch"]
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 1

from TruthIO.TruthIOConf import HepMCReadFromFile
genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]
#evgenConfig.auxfiles += [ 'inclusive.pdt', 'inclusive.dec' ]

del testSeq.TestHepMC
