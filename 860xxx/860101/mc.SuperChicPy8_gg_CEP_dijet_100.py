evgenConfig.description = "SuperChic4 MC gg dijet production at 13000 GeV"
#evgenConfig.keywords = ["2gluons","2jets","cep"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["maura.barros@cern.ch"]
evgenConfig.nEventsPerJob = 1000 
evgenConfig.tune = "none"

if not os.path.exists('inputs'):
    os.makedirs('inputs')
if not os.path.exists('evrecs'):
    os.makedirs('evrecs')
if not os.path.exists('outputs'):
    os.makedirs('outputs')

from Superchic_i.SuperChicUtils import *

#class with the superchic initialization parameters.  Please see SuperChicUtils for a complete list of tunable parameters.
Init = SuperChicConfig()

hasECM = hasattr(runArgs,"ecmEnergy")
if hasECM:
    ecm = int(runArgs.ecmEnergy)
    Init.rts = str(ecm)+"d0" # set the COM collision energy (in fortran syntax)
Init.isurv = "4"                  # Model of soft survival (from 1 -> 4, corresponding to arXiv:1306.2149)
Init.intag = "'in13'"              # for input files
Init.PDFname = "'cteq6l1'" # PDF set name
Init.PDFmember = "0"              # PDF member
Init.proc = "3"                  # Process number (59 = gg->gg, 56 = gg->ee, 68 = gg->a->gg ); Please consult Superchic Manual https://superchic.hepforge.org/
Init.beam = "'prot'"              # Beam type ('prot', 'ion')
Init.outtg = "'out'"              # for output file name
Init.sfaci = ".true."             # Include soft survival effects
Init.diff = "'el'"                # interaction: elastic ('el'), single ('sd','sda','sdb') and double ('dd') dissociation.
hasSeed = hasattr(runArgs,"randomSeed")
if hasSeed:
    Init.iseed  = str(int(runArgs.randomSeed))
Init.genunw  = ".true."
hasNev = hasattr(runArgs,"maxEvents")
if hasNev:
    nevents = str(int(runArgs.maxEvents*1.1))
else:
    nevents = str(int(evgenConfig.nEventsPerJob*1.1))

Init.nev = str(nevents)    

Init.ymin  = "-5.0d0"              # Minimum object rapidity Y_X
Init.ymax  = "5.0d0"               # Maximum object rapidity Y_X
Init.mmin  = "100d0"                 # Minimum object mass M_X
Init.mmax  = "2000d0"               # Maximum object mass M_X
Init.gencuts  = ".true."           # Generate cuts below
Init.scorr  = ".true."           # Include spin correlations
Init.fwidth  = ".true."           # Include finite width
Init.ptxmax  = "1d10"         #cut on proton pt
Init.ptamin  = "100.0d0"             # Minimum pT of outgoing object a - pT 50 GeV
Init.ptbmin  = "100.0d0"             # Minimum pT of outgoing object b - pT 50 GeV
Init.etaamin  = "-5.0d0"           # Minimum eta of outgoing object a
Init.etaamax   = "5.0d0"           # Maximum eta of outgoing object a
Init.etabmin  = "-5.0d0"           # Minimum eta of outgoing object b
Init.etabmax   = "5.0d0"           # Maximum eta of outgoing object b
Init.acoabmax  = "10d10"

# --- Added for dijet production by Maura ----

Init.rjet = "0.4"
Init.jalg = "'antikt'"

# --------------------------------------------

SuperChicRun(Init)


## Base config for Pythia8
from Pythia8_i.Pythia8_iConf import Pythia8_i
genSeq += Pythia8_i("Pythia8")
evgenConfig.generators += ["Pythia8"]

## Control storing LHE in the HepMC record
if "StoreLHE" in genSeq.Pythia8.__slots__.keys():
    print "Pythia8_Base_Fragment.py: DISABLING storage of LHE record in HepMC by default. Please re-enable storage if desired"
    genSeq.Pythia8.StoreLHE = False

genSeq.Pythia8.Commands += [
    "PartonLevel:ISR = off",
    "PartonLevel:MPI = off",
    "PartonLevel:Remnants = off",
    "Check:event = off",
    "LesHouches:matchInOut = off"
]

assert hasattr(genSeq, "Pythia8")
fileName = "evrecs/evrec"+Init.outtg[1:-1]+".dat"
genSeq.Pythia8.LHEFile = fileName
genSeq.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)

testSeq.TestHepMC.MaxTransVtxDisp = 1000000
testSeq.TestHepMC.MaxVtxDisp      = 1000000000
