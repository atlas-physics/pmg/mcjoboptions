evgenConfig.description = "SuperChic4 MC gamma + gamma pp collisions at 13000 GeV to 2 muons + X"
evgenConfig.contact = ["maura.barros@cern.ch"]
evgenConfig.generators += ['Superchic']
evgenConfig.tune = "none"

from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun


# Retrieve mass that we want from the JO name
name_jobfile = jofiles
config_names = jofile.split('.')[1].split('_')
mX = float(config_names[2][0:])

#class with the superchic initialization parameters.  Please see SuperChicUtils for a complete list of tunable parameters.
scConfig = SuperChicConfig(runArgs)

#trying to generate more events than the ones requested to apply the filter
scConfig.nev = 5*runArgs.maxEvents
scConfig.isurv = 4                  # Model of soft survival (from 1 -> 4, corresponding to arXiv:1306.2149)
scConfig.PDFname = 'MSHT20qed_nnlo' # PDF set name
scConfig.PDFmember = 0              # PDF member
scConfig.proc = 83                  # Please consult Superchic Manual https://superchic.hepforge.org/
scConfig.beam = 'prot'              # Beam type ('prot', 'ion')
scConfig.sfaci = True               # Include soft survival effects
scConfig.diff = 'el'                # interaction: elastic ('el'), single ('sd','sda','sdb') and double ('dd') dissociation.
scConfig.genunw  = True
scConfig.ymin  = -5.0               # Minimum object rapidity Y_X
scConfig.ymax  = 5.0                # Maximum object rapidity Y_X
scConfig.mmin  = 10                 # Minimum object mass M_X
scConfig.mmax  = 5000               # Maximum object mass M_X
scConfig.gencuts  = False           # Generate cuts below
scConfig.scorr  = True              # Include spin correlations
scConfig.fwidth  = True             # Include finite width
scConfig.ptxmax  = 10000000000      #cut on proton pt 
scConfig.acoabmax  = 100000000000
scConfig.tau = 0.04                 # Mass distribution decay constant (GeV^-1)
scConfig.mxs = mX                   # Mass of MX

SuperChicRun(scConfig, genSeq)
include('Pythia8_VX_Common.py')

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
from AthenaCommon.SystemOfUnits import GeV
######################
# Proton filter side A
######################

filtSeq += ForwardProtonFilter("ProtonSideA")

filtSeq.ProtonSideA.xi_min = 0.00
filtSeq.ProtonSideA.xi_max = 0.15
filtSeq.ProtonSideA.beam_energy = 6500.*GeV
filtSeq.ProtonSideA.pt_min = 0.0*GeV
filtSeq.ProtonSideA.pt_max = 1.5*GeV
filtSeq.ProtonSideA.single_tagA = True

######################
# Proton filter side C
######################

filtSeq += ForwardProtonFilter("ProtonSideC")

filtSeq.ProtonSideC.xi_min = 0.00
filtSeq.ProtonSideC.xi_max = 0.15
filtSeq.ProtonSideC.beam_energy = 6500.*GeV
filtSeq.ProtonSideC.pt_min = 0.0*GeV
filtSeq.ProtonSideC.pt_max = 1.5*GeV
filtSeq.ProtonSideC.single_tagC = True

filtSeq.Expression = "ProtonSideA and ProtonSideC"

#-------------------------------------------------------------- 

