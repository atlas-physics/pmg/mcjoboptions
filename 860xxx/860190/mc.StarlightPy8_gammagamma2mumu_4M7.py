evgenConfig.description = "Starlight+Pythia8 gamma + gamma Pb+Pb UPC collisions to continuum -> mumu, 4 < m < 7 GeV, 0.75 < pT(mu), |eta(mu)| < 2.6"
evgenConfig.keywords = ["2photon","2lepton"]
evgenConfig.contact = ["mateusz.dyndalG@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# Gamma calculation for Pb208
ecmEnergy=5020.
if hasattr(runArgs,'ecmEnergy'):
    ecmEnergy = runArgs.ecmEnergy
else:
    print("No center of mass energy found, default to 5020 GeV")
# gamma = E(Pb208) / (m(Pb208)/amu * amu/GeV)
gamma = 208*0.5*ecmEnergy / (207.9766359 * 0.931494)
gamma = int(gamma)

from Starlight_i.Starlight_iConf import Starlight_i
# The name "ParticleDecayer" is needed due to removal of dummy event when running with Pythia8 (GenModule feature)
genSeq += Starlight_i("ParticleDecayer")
genSeq.ParticleDecayer.McEventKey = "GEN_EVENT"

evgenConfig.generators += ["Starlight"]

from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT"]
del _evgenstream


genSeq.ParticleDecayer.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beam1Gamma %i" % gamma, #Gamma of the colliding ion1
     "beam2Gamma %i" % gamma, #Gamma of the colliding ion2
     "maxW 7.", #Max value of w
     "minW 4.", #Min value of w
     "nmbWBins 400", #Bins n w
     "maxRapidity 3.", #max y
     "nmbRapidityBins 300", #Bins n y
     "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 0.75", #Minimum pT in GeV
     "maxPt 200.", #Maximum pT in GeV
     "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -2.6", #Minimum pseudorapidity
     "maxEta 2.6", #Maximum pseudorapidity
     "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 13", #Channel of interest
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 0", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]

genSeq.ParticleDecayer.lheOutput = True
genSeq.ParticleDecayer.maxevents = int(1.1*runArgs.maxEvents)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = 1' ]


