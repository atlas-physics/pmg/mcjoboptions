evgenConfig.description = "Starlight+Pythia8+Tauolapp+Photospp gamma + gamma Pb+Pb UPC collisions to continuum -> 2 tau, breakup mode 5 (no selection) with pT>1.5 GeV charged particle filter"
evgenConfig.keywords = ["2photon","2lepton"]
evgenConfig.contact = ["mateusz.dyndal@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# Gamma calculation for Pb208
ecmEnergy=5020.
if hasattr(runArgs,'ecmEnergy'):
    ecmEnergy = runArgs.ecmEnergy
else:
    print("No center of mass energy found, default to 5020 GeV")
# gamma = E(Pb208) / (m(Pb208)/amu * amu/GeV)
gamma = 208*0.5*ecmEnergy / (207.9766359 * 0.931494)
gamma = int(gamma)


from Starlight_i.Starlight_iConf import Starlight_i
# The name "ParticleDecayer" is needed due to removal of dummy event when running with Pythia8 (GenModule feature)
genSeq += Starlight_i("ParticleDecayer")
genSeq.ParticleDecayer.McEventKey = "GEN_EVENT"

evgenConfig.generators += ["ParticleGenerator"]

from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT"]
del _evgenstream


genSeq.ParticleDecayer.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     "beam1Gamma %i" % gamma, #Gamma of the colliding ion1
     "beam2Gamma %i" % gamma, #Gamma of the colliding ion2
     "maxW 7", #Max value of w
     "minW -1", #Min value of w
     "nmbWBins 400", #Bins n w
     "maxRapidity 9.", #max y
     "nmbRapidityBins 1000", #Bins n y
     "accCutPt 0", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 2.0", #Minimum pT in GeV
     "maxPt 100.0", #Maximum pT in GeV
     "accCutEta 0", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -5", #Minimum pseudorapidity
     "maxEta 5", #Maximum pseudorapidity
     "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 15", #Channel of interest
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 0", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]

genSeq.ParticleDecayer.lheOutput = True
genSeq.ParticleDecayer.maxevents = int(40.*runArgs.maxEvents)
genSeq.ParticleDecayer.doTauolappLheFormat = True


include("Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += ["15:onMode = off"]
from Tauolapp_i.Tauolapp_iConf import TauolaPP
genSeq += TauolaPP()
TauolaPP.decay_mode_same = 0
TauolaPP.decay_mode_opposite = 0
TauolaPP.decay_particle = 15
TauolaPP.tau_mass = 1.77684
TauolaPP.spin_correlation=True
TauolaPP.setRadiation=False
evgenConfig.generators += [ "TauolaPP" ] 
include("Pythia8_i/Pythia8_Photospp.py")
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = 1' ]
genSeq.Pythia8.Commands += [ 'TimeShower:QEDshowerByL = 1' ]


include('GeneratorFilters/ChargedTrackFilter.py')
filtSeq.ChargedTracksFilter.NTracks = 1
filtSeq.ChargedTracksFilter.NTracksMax = -1
filtSeq.ChargedTracksFilter.Ptcut = 1500.0
filtSeq.ChargedTracksFilter.Etacut= 2.6


