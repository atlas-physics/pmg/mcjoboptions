evgenConfig.description = "Single anti-proton with log energy distribution"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["christopher.young@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (-2212)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(1000, 2000000.), eta=[-2.5, 2.5])


