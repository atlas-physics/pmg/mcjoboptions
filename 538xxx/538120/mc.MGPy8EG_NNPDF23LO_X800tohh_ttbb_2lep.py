import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------------------------------                                                                                                
# Setting mHH and WHH for param_card.dat                                                                                                                                                          
#---------------------------------------------------------------------------------------------------
mhh=8.000000e+02
whh=1.000000e-02

params_bsm={'1560':mhh, #MHH
            '1561':whh}

params_mass={'25':1.250000e+02, #MH
             '1560':mhh} 

process = """
import model sm
import model HeavyHiggsTHDM
"""

# Define multi-particle representations
process += """
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
"""

#---------------------------------------------------------------------------------------------------                                                                                   
# Generating di-higgs through Heavy Higgs resonance with MadGraph                                                                                                                          
#---------------------------------------------------------------------------------------------------
# Define process to be simulated
process += """
generate p p > hh > h h
"""

# Define MadGraph outputs
process += """
output -f
"""

#Bonus file to add 1560 to pdg
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('1560\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

safefactor=10
nevents=20000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor


extras = { 'nevents':nevents,
           'ebeam1':beamEnergy,
           'ebeam2':beamEnergy,
           'fixed_ren_scale': 'True', # 
           'fixed_fac_scale': 'True', #
           'scale':mhh/2, #
           'dsqrt_q2fact1':mhh/2, #
           'dsqrt_q2fact2':mhh/2 #
         }

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------                                                                                  
# Modify the param_ and run_cards to work with the mass paramaters                                                                                                              
# Using the values given in "extras" above for the selected parameters when setting up the run_card                                                                     
# If not set in "extras", default values are used                                                                                                                                  
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params={'MASS':params_mass,'BSM':params_bsm})

modify_run_card(process_dir=process_dir,settings=extras)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs)


#--------------------------------------------------------------                                                                                                                      
# Pythia8 showering setup                                                                                                                                                                 
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#---------------------------------------------------------------------------------------------------                                                                          
# EVGEN Configuration                                                                                                                                                                  
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Di-Higgs production through 800 GeV Heavy Higgs resonance which decays to ttbb in the two lepton channel."
evgenConfig.keywords = ["hh"]
evgenConfig.contact = ['giuseppe.callea@cern.ch']
evgenConfig.nEventsPerJob = 5000

#--------------------------------------------------------------                                                                                                                    
# Pythia8 showering                                                                                                                                                               
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5", # bb decay
                            "25:addChannel = on 0.5 100 15 -15", # tt decay
                            "24:mMin = 0", # W minimum mass
                            "24:mMax = 99999", # W maximum mass
                            "23:mMin = 0", # Z minimum mass
                            "23:mMax = 99999", # Z maximum mass
                            "15:onMode = off",
                            "15:onIfAny = 11 13",
                            "TimeShower:mMaxGamma = 0"]

#--------------------------------------------------------------                                                                                                               
# Dipole option Pythia8                                                                                                                                   
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------                                                                                       
# Generator Filters - ttbb events                                                                                                     
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HttFilter", PDGParent = [25], PDGChild = [15])

filtSeq.Expression = "HbbFilter and HttFilter"

