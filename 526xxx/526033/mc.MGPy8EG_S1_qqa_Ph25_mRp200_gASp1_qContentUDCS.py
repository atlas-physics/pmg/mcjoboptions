mR          = 200
mDM         = 10000
gVSM        = 0.00
gASM        = 0.10
gVDM        = 0.00
gADM        = 1.00
filteff     = 0.111000
phminpt     = 25
quark_decays= ['u', 'd', 's', 'c']

include("MadGraphControl_MGPy8EG_DMS1_dijetgamma_intParams.py")

evgenConfig.description = "Zprime to qqbar with ISR - mR250 - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Alex Gekow <alex.gekow@cern.ch>"]
