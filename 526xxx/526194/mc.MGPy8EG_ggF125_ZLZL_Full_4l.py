#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "ggF HZZ"
evgenConfig.keywords = [ 'Higgs', 'SMHiggs' ]
evgenConfig.contact = [ 'mingyi.liu@cern.ch', 'xingyu.wu@cern.ch' ]
evgenConfig.generators = [ 'MadGraph', 'Pythia8', 'EvtGen' ]



# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True
gridpack_dir='madevent/'

if not is_gen_from_gridpack():
    process = """
    import model SM_Loop_ZPolar_NLO
    define p = u c s d b u~ c~ s~ d~ b~ g
    define j = u c s d b u~ c~ s~ d~ b~ g
    define l+ = e+ mu+
    define l- = e- mu-
    generate g g > h > l+ l- l+ l- / a z za zt QED=4 QCD=2 [noborn=QCD] @0
    add process g g > h g > l+ l- l+ l- g / a z za zt QED=4 QCD=3 [noborn=QCD] @1
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

ktdurham=30

settings = {
            'me_frame' : '3,4,5,6',
            'dynamical_scale_choice' : 3,
            'lhe_version' : '3.0',
            'cut_decays' : 'True',
            'ebeam1' : 6500,
            'ebeam2' : 6500,
            'ptj' : 0,
            'ptb' : 0,
            'pta' : 0,
            'ptl' : 0,
            'etaj' : -1,
            'etab' : -1,
            'etal' : -1,
            'drjj' : 0,
            'drll' : 0,
            'draa' : 0,
            'draj' : 0,
            'drjl' : 0,
            'dral' : 0,
            'mmll' : 0,
            'mmllmax' : -1,
            'mmnl' : 0,
            'maxjetflavor' : 5,
            'xqcut' : 0,
            'ickkw' : '0',
            'auto_ptj_mjj' : 'False',
            'event_norm' : 'sum',
            'parton_shower' : 'PYTHIA8',
            'bwcutoff' : 10000000,
            'nevents' : int(nevents)}

settings.update( {
    'ptj': ktdurham,
    'ptb': ktdurham,
    'ktdurham'    : ktdurham,
    'ickkw'       : '0',
    'dparameter'  : "0.4",
    'xqcut'       : "0",
    }
)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Param Card
params={}
mass={}
mass['25']=125
params['MASS']=mass
sminputs={}
sminputs['1']=1.323489e+02 # aewm1
sminputs['2']=1.166370e-05 # gf
params['SMINPUTS']=sminputs

modify_param_card(process_dir=process_dir,params=params)

#os.environ["ATHENA_PROC_NUMBER"] = "96"
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

outputDS=arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)



# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

with open("pdgid_extras.txt","w") as pdgid_extras_file:
    pdgid_extras_file.write("230\n")
    pdgid_extras_file.write("231")

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
PYTHIA8_nJetMax=1
PYTHIA8_Dparameter=float(settings['dparameter'])
PYTHIA8_Process="pp>e+e-mu+mu-"
PYTHIA8_TMS=float(settings['ktdurham'])
PYTHIA8_nQuarksMerge=int(settings['maxjetflavor'])

include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

#genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
# to fix the CKKWL acceptance issue
genSeq.Pythia8.CKKWLAcceptance = False
