#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "VH HZZ"
evgenConfig.keywords = [ 'Higgs', 'SMHiggs' ]
evgenConfig.contact = [ 'mingyi.liu@cern.ch', 'xingyu.wu@cern.ch' ]
evgenConfig.generators = [ 'MadGraph', 'Pythia8', 'EvtGen' ]



# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True
gridpack_dir='madevent/'

if not is_gen_from_gridpack():
    process = """
    define p = u c s d b u~ c~ s~ d~ b~ g
    define j = u c s d b u~ c~ s~ d~ b~ g
    define l+ = e+ mu+
    define l- = e- mu-
    define la+ = e+ mu+ ta+
    define la- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > h la+ la- EW=3 QCD=0, (h > z{0} l+ l-, z > l+ l-) @0
    add process p p > h vl vl~ EW=3 QCD=0, (h > z{0} l+ l-, z > l+ l-) @1
    add process p p > h la+ vl EW=3 QCD=0, (h > z{0} l+ l-, z > l+ l-) @2
    add process p p > h la- vl~ EW=3 QCD=0, (h > z{0} l+ l-, z > l+ l-) @3
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

settings = {
            'me_frame' : '3,4,5,6',
            'dynamical_scale_choice' : 3,
            'lhe_version' : '3.0',
            'cut_decays' : 'True',
            'ebeam1' : 6500,
            'ebeam2' : 6500,
            'ptj' : 0,
            'ptb' : 0,
            'pta' : 0,
            'ptl' : 0,
            'etaj' : -1,
            'etab' : -1,
            'etal' : -1,
            'drjj' : 0,
            'drll' : 0,
            'draa' : 0,
            'draj' : 0,
            'drjl' : 0,
            'dral' : 0,
            'mmll' : 0,
            'mmllmax' : -1,
            'mmnl' : 0,
            'maxjetflavor' : 5,
            'xqcut' : 0,
            'ickkw' : '0',
            'auto_ptj_mjj' : 'False',
            'event_norm' : 'sum',
            'parton_shower' : 'PYTHIA8',
            'bwcutoff' : 10000000,
            'nevents' : int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#os.environ["ATHENA_PROC_NUMBER"] = "96"
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

outputDS=arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
