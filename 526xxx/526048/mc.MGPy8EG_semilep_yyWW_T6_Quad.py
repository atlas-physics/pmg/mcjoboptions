from MadGraphControl.MadGraphUtils import *

# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

nevents = runArgs.maxEvents*2 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

#Quadratic
gen_string = "T6==1"

process="""
import model QAll_5_Aug21v2
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~"""

process += "\ngenerate a a > w+ w- > l+ vl j j %s" %(gen_string)
process += "\nadd process a a > w+ w- > l- vl~ j j %s" %(gen_string)


process += "\noutput -f"

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0',
              'cut_decays':'F',
              'lpp1': '2',
              'lpp2': '2',
	            'fixed_fac_scale1' : 'T',
              'fixed_fac_scale2' : 'T',
              'ptl': 5.0,
              'etal': 4.5,
       	      'drll': 0.2,
              'drjj': 0.2,
              'nevents':int(nevents),
              'hard_survey':1,
              'dynamical_scale_choice':3
           }
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

anoinputs={}
anoinputs['FS0']="0e-8"
anoinputs['FS1']="0e-8"
anoinputs['FS2']="0e-8"
anoinputs['FM0']="0e-8"
anoinputs['FM1']="0e-8"
anoinputs['FM2']="0e-8"
anoinputs['FM3']="0e-8"
anoinputs['FM4']="0e-8"
anoinputs['FM5']="0e-8"
anoinputs['FM6']="0e-8"
anoinputs['FM7']="0e-8"
anoinputs['FT0']="0e-8"
anoinputs['FT1']="0e-8"
anoinputs['FT2']="0e-8"
anoinputs['FT3']="0e-8"
anoinputs['FT4']="0e-8"
anoinputs['FT5']="0e-8"
anoinputs['FT6']="1e-8"
anoinputs['FT7']="0e-8"
anoinputs['FT8']="0e-8"
anoinputs['FT9']="0e-8"

params={}
params['anoinputs'] = anoinputs
modify_param_card(process_dir=process_dir, params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# MG5 + Pythia8
include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py") 
include("Pythia8_i/Pythia8_MadGraph.py")

# Pythia8 options
include("Pythia8_i/Pythia8_ShowerWeights.py")

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 4.5
MultiLeptonFilter.NLeptons = 1

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyWW'
evgenConfig.keywords+=['photon','WW']
evgenConfig.contact = ['Varsiha Sothilingam  <varsiha.sothilingam@cern.ch>']
