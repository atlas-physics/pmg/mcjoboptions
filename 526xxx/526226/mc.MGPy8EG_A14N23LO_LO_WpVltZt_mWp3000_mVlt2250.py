import os, sys
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

nevents = runArgs.maxEvents*1.1

pdgfile = open('pdgid_extras.txt', 'w+')
pdgfile.write("""
34
4000005
4000006
4000007
4000008
-34
-4000005
-4000006
-4000007
-4000008
""")
pdgfile.close()

params = {
    'MASS': {
        'MTH': 2250.0,
        'MT53': 902.0,
        'MT23': 903.0,
        'MBH': 2250.0,
        'MKKW': 3000.0,
    },
    'DECAY': {
        '34': 'Auto', 
        '4000005': 'Auto', 
        '4000006': 'Auto', 
        '4000007': 'Auto', 
        '4000008': 'Auto', 
    }
}

process = f"""
import model ./Wprime_vlq_UFO
generate p p > kkW+, (kkW+ > Th b~, (Th > t z))
add process p p > kkW-, (kkW- > Th~ b, (Th~ > t~ z))
output -f
"""

print('New process')
process_dir = new_process(process)

print('Process dir: ', process_dir)
cardsDir=os.path.join(process_dir, 'Cards')

#Fetch default LO run_card.dat and set parameters
settings = { 'ickkw': 0,
             'nevents':int(nevents) }

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=settings)

modify_param_card(param_card_input=os.path.join(cardsDir, 'param_card.dat'), 
                  param_card_backup=os.path.join(cardsDir, 'param_card.dat.backup'), 
                  process_dir=process_dir, 
                  params=params, 
                  output_location=os.path.join(cardsDir,'param_card.dat') )

generate(process_dir=process_dir, runArgs=runArgs)

evgenConfig.generators = [ 'MadGraph' ]
evgenConfig.process = 'WpVlqTb'
evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN and A15NNPDF23LO for W' decaying into T+b"
evgenConfig.keywords = ['BSM', 'Wprime' ]
evgenConfig.contact =  [ 'Takanori Kono <takanori.kono@cern.ch>' ]

outDS = arrange_output(process_dir=process_dir,
                       runArgs=runArgs)
#lhe_version=3,
#saveProcDir=True)

