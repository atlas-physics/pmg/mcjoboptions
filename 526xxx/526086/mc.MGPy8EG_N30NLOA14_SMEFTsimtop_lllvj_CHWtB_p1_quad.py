import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260400, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
}
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphUtils import check_reset_proc_number
#include('MadGraphControl/setupEFT.py')
 

# create dictionary of processes this JO can create
process="""
import model SMEFTsim_top_MwScheme_UFO-topCPV_massless
define p = g u c d s u~ c~ d~ s~
define j = p
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define W = W+ W-
generate p p > l+ l- l+ vl NP==1 
add process p p > l- l+ l- vl~ NP==1 
add process p p > l+ l- l+ vl j NP==1 
add process p p > l- l+ l- vl~ j NP==1 
output -f"""

# define cuts
settings={
    'ptl':'10', #min charged lepton pt
    'etal':'2.7', #max charged lepton abs eta
    'drll':'0.2', #min dr charged leptons
  # needed for jet merging
    'ickkw':'0',
    'dparameter':'0.4',
    'ktdurham':'25',
    'maxjetflavor':'4',
    'pdgs_for_merging_cut' : '1, 2, 3, 4, 21',
    'xqcut': -1,
    'drjj': 0.0,
    'nevents': 1.8*(runArgs.maxEvents if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob)
}

params=dict()
params['SMEFTcpv']=dict()
params['SMEFTcpv']['cWtil']=0
params['SMEFTcpv']['cHWBtil']=1.

process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  

# add meta data
evgenConfig.description = 'merged lllv and lllvj production with SMEFTSIM'
evgenConfig.keywords+=['diboson']
evgenConfig.contact = ['Juan Rivera <juan.cristobal.rivera.vergara@cern.ch>', 'Maheyer J. Shroff <maheyer.jamshed.shroff@cern.ch>']

check_reset_proc_number(opts)

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=off"]
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

nJetMax = 1
PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=0.4
PYTHIA8_Process="pp>LEPTONS,NEUTRINOS"
PYTHIA8_TMS=25
PYTHIA8_nQuarksMerge=4

include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

evgenConfig.nEventsPerJob = 100000
