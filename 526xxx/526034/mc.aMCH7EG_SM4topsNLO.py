from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

import fileinput

# Make some excess of events - make sure we protect against maxEvents=-1
evgenConfig.nEventsPerJob = 1000
nevents=1.1*runArgs.maxEvents if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

bwcut = 15.0
topdecay = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \n"

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
gridpack_mode=False
req_acc=0.001

if not is_gen_from_gridpack():
    process="""
    import model loop_sm-no_b_mass
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    generate p p > t t~ t t~ [QCD]
    output -f"""

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION
#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
#Fetch default NLO run_card.dat and set parameters
settings = {
    'maxjetflavor'  : 5,
    'bwcutoff'  : bwcut,
    'nevents':int(nevents),
    'parton_shower' : 'HERWIGPP'
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
# Modify the param card
# set_top_params(process_dir,mTop=172.5,FourFS=False)

#---------------------------------------------------------------------------
# Cook the setscales file for the user defined dynamical scale
#---------------------------------------------------------------------------
# The default scale (-1) gets modified to be the sum of the transverse mass divided by 4
# This avoid the problems with the custom scales introduced in 3.5.X
fileN = process_dir+'/SubProcesses/setscales.f'
for line in fileinput.input(fileN, inplace=1):
    if line.startswith('c         sum of the transverse mass divide by 2'):
        print('c         sum of the transverse mass divide by 4')
    elif line.startswith('          tmp=tmp/2d0'):
        print('          tmp=tmp/4d0')
    elif line.startswith("          temp_scale_id='H_T/2 := sum_i mT(i)/2, i=final state'"):
        print("          temp_scale_id='H_T/4 := sum_i mT(i)/4, i=final state'")
    else:
        print(line.strip('\n'))

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set Nevents_for_max_weight 75
set BW_cut %i
set seed %i
%s
launch
"""%(bwcut, runArgs.randomSeed, topdecay))
mscard.close()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

#--------------------------------------------------------------
# Herwig7.2 showering
#--------------------------------------------------------------
#from MadGraphControl.MadGraphUtils import check_reset_proc_number
#check_reset_proc_number(opts)

include("Herwig7_i/Herwig7_LHEF.py")
include("Herwig7_i/Herwig7_EvtGen.py")
runArgs.inputGeneratorFile = outputDS+'.events'
Herwig7Config.me_pdf_commands(order="NLO",
                                name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile,
                                    me_pdf_order="NLO")

# run Herwig7
Herwig7Config.run()

# Metadata
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.contact = ['mmiralle@cern.ch']
evgenConfig.description = "Standard-Model 4tops production at NLO with MadGraph5 and Herwig7"
evgenConfig.keywords = ['SM', 'top', '4top', 'NLO']
evgenConfig.tune = "H7.2-Default"

