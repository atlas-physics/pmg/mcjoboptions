import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260400, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
}
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphUtils import check_reset_proc_number
#include('MadGraphControl/setupEFT.py')
 
veto_couplings=['NPcbW', 'NPcHG', 'NPcbj1', 'NPctu8', 'NPcleQt1', 'NPceW', 'NPctu1', 'NPcju1', 'NPcHl3', 'NPcQQ1', 'NPcHj1', 'NPclebQ', 'NPcHbox', 'NPcdd8', 'NPcbu8', 'NPcH', 'NPcjj11', 'NPcHbq', 'NPcQe', 'NPcuu8', 'NPcll1', 'NPceu', 'NPcQu1', 'NPctH', 'NPcll', 'NPctj1', 'NPcbl', 'NPced', 'NPcQb1', 'NPcHtb', 'NPcQl1', 'NPcbd8', 'NPctd1', 'NPcbB', 'NPcQtQb8', 'NPctb8', 'NPctt', 'NPctl', 'NPcle1', 'NPcQl3', 'NPcdd1', 'NPcuu1', 'NPcQd8', 'NPcQj38', 'NPcbu1', 'NPcHQ1', 'NPcbe', 'NPcbd1', 'NPcjj31', 'NPcjj38', 'NPcud8', 'NPcHj3', 'NPcHd', 'NPcHQ3', 'NPcQt8', 'NPcld', 'NPcW', 'NPcQj18', 'NPctd8', 'NPcHB', 'NPctW', 'NPcbG', 'NPclj3', 'NPcte', 'NPcud1', 'NPctB', 'NPcQj11', 'NPcQQ8', 'NPcG', 'NPcHW', 'NPcje', 'NPctG', 'NPcHt', 'NPcleQt3', 'NPceB', 'NPclu', 'NPcHWB', 'NPctb1', 'NPcHl1', 'NPcjd8', 'NPcbH', 'NPcju8', 'NPcQtQb1', 'NPctj8', 'NPcHDD', 'NPcbb', 'NPcHe', 'NPceH', 'NPcjd1', 'NPcHu', 'NPcQu8', 'NPcjj18', 'NPcQt1', 'NPcQb8', 'NPcQd1', 'NPcQj31', 'NPcbj8', 'NPcle', 'NPcee', 'NPclj1']

veto_couplings.remove('NPcHWB')


# create dictionary of processes this JO can create
process="""
import model SMEFTsim_top_MwScheme_UFO-massless
define p = g u c d s u~ c~ d~ s~
define j = p
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define W = W+ W-
generate p p > l+ l- l+ vl NP==1 {0}
add process p p > l- l+ l- vl~ NP==1 {0}
add process p p > l+ l- l+ vl j NP==1 {0}
add process p p > l- l+ l- vl~ j NP==1 {0}
output -f""".format(' '.join([x+'==0' for x in veto_couplings]))

# define cuts
settings={
    'ptl':'10', #min charged lepton pt
    'etal':'2.7', #max charged lepton abs eta
    'drll':'0.2', #min dr charged leptons
  # needed for jet merging
    'ickkw':'0',
    'dparameter':'0.4',
    'ktdurham':'25',
    'maxjetflavor':'4',
    'pdgs_for_merging_cut' : '1, 2, 3, 4, 21',
    'xqcut': -1,
    'drjj': 0.0,
    'nevents': 1.8*(runArgs.maxEvents if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob)
}

params=dict()
params['SMEFT']=dict()
params['SMEFT']['9']=1.0

process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
default_card='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTsim_top_MwScheme_UFO/param_card_massless.dat'
import shutil
shutil.copy(default_card,process_dir+'/Cards/param_card.dat')

modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  

# add meta data
evgenConfig.description = 'merged lllv and lllvj production with SMEFTSIM'
evgenConfig.keywords+=['diboson']
evgenConfig.contact = ['Juan Rivera <juan.cristobal.rivera.vergara@cern.ch>', 'Maheyer J. Shroff <maheyer.jamshed.shroff@cern.ch>']

check_reset_proc_number(opts)

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=off"]
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

nJetMax = 1
PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=0.4
PYTHIA8_Process="pp>LEPTONS,NEUTRINOS"
PYTHIA8_TMS=25
PYTHIA8_nQuarksMerge=4

include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

evgenConfig.nEventsPerJob = 100000

