evgenConfig.keywords    = ['Higgs']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.description = """WH (W->jj, onshell)  125 GeV Higgs FxFx merged production decaying to gammagamma."""



import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings                                                                                                                                       
nevents = runArgs.maxEvents*3 if runArgs.maxEvents>0 else 3*evgenConfig.nEventsPerJob
gridpack_mode=True
if not is_gen_from_gridpack():
    process = """                                                                                                                                        
    import model loop_sm-no_b_mass                                                                                                                       
    define p = g u c b d s u~ c~ d~ s~ b~                                                                                                                
    define j = g u c b d s u~ c~ d~ s~ b~                                                                                                                
    define l+ = e+ mu+ ta+                                                                                                                               
    define l- = e- mu- ta-                                                                                                                               
    define vl~ = ve~ vm~ vt~                                                                                                                             
    define vl = ve vm vt                                                                                                                                 
    generate p p > h w+  [QCD] @0                                                                                                                          
    add process p p > h w-  [QCD] @0                                                                                                                          
    add process p p >  h w+ j [QCD] @1                                                                                                                    
    add process p p >  h w- j [QCD] @1                                                                                                                    
    output -f                                                                                                                                            
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION
settings= {'ickkw'         : 3,
          'ptj'           : 10,
          'jetradius'     : 1.0,
          'maxjetflavor'  : 5,
          'parton_shower' :'PYTHIA8',
          'mll_sf'        : 10.0,
          'muR_ref_fixed' : 125.0,
          'muF1_ref_fixed': 125.0,
          'muF2_ref_fixed': 125.0,
          'QES_ref_fixed' : 125.0,
          'nevents'      :int(nevents)
          }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')   
mscard.write("""#************************************************************                                                                            
#*                        MadSpin                           *                                                                                            
#*                                                          *                                                                                            
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                            
#*                                                          *                                                                                            
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                            
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                            
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                            
#*                                                          *                                                                                            
#************************************************************                                                                                            
#Some options (uncomment to apply)                                                                                                                       
#                                                                                                                                                        
# set seed 1                                                                                                                                             
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight                                                                   
# set BW_cut 15                # cut on how far the particle can be off-shell                                                                            
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event                                                                      
#                                                                                                                                                        
set seed %i                                                                                                                                              
# specify the decay for the final state particles                                                                                                        
decay w+ > j j                                                                                                                                        
decay w- > j j                                                                                                                                        
# running the actual code                                                                                                                                
launch"""%runArgs.randomSeed) 

mscard.close()

masses={'25': '1.250000e+02'}
params={}
params['MASS']=masses
modify_param_card(process_dir=process_dir,params=params)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

print "Now performing parton showering ..."
######################################################################                                                                                   
# End of event generation, start configuring parton shower here.                                                                                         
######################################################################                                                                                   
'''                                                                                                                                                      
if 'ATHENA_PROC_NUMBER' in os.environ:                                                                                                                   
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs ser\
ially.'                                                                                                                                                  
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')                                                                                                         
    # Try to modify the opts underfoot                                                                                                                   
    if not hasattr(opts,'nprocs'): print 'Did not see option!'                                                                                           
else: opts.nprocs = 0                                                                                                                                    
    print opts                                                                                                                                           
'''

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

if hasattr(genSeq.Pythia8, "UserHook"):
    genSeq.Pythia8.UserHook = 'JetMatchingMadgraph'
else:
    genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']

genSeq.Pythia8.FxFxXS = True
genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:nQmatch          = 5",     # Should match maxjetflavour in 'mg5'.                                                                       
    "JetMatching:jetAlgorithm     = 2",
    "JetMatching:slowJetPower     = 1",
    "JetMatching:clFact           = 1.0",
    "JetMatching:eTjetMin         = 40.0",  # Should match qcut.                                                                                         
    "JetMatching:coneRadius       = 1.0",   # Default.                                                                                                   
    "JetMatching:etaJetMax        = 4.5",
    "JetMatching:exclusive        = 1",     # Exclusive - all PS jets must match HS jets.                                                                
    "JetMatching:nJetMax          = 1",     # *** Must match highest Born-level jet multiplicity. ***                                                    
    "JetMatching:nJet             = 0",
    "JetMatching:jetAllow         = 1",
    "JetMatching:doShowerKt       = off",
    "JetMatching:qCut             = 40.0",  # Merging scale for FxFx.  
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = 10.0"   # Should match ptj.                                                                                          
    ]
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ] #Decay                                                                         

