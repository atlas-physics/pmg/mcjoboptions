# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:40
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.77396509E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.93434000E+03  # scale for input parameters
    1    6.18916944E+01  # M_1
    2   -7.34261076E+02  # M_2
    3    2.14138085E+03  # M_3
   11   -1.29247063E+03  # A_t
   12   -1.88019474E+02  # A_b
   13   -1.30798683E+02  # A_tau
   23    1.84720039E+02  # mu
   25    5.76423130E+01  # tan(beta)
   26    2.35997362E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.92006701E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.91230656E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.72590492E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.93434000E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.93434000E+03  # (SUSY scale)
  1  1     8.38563374E-06   # Y_u(Q)^DRbar
  2  2     4.25990194E-03   # Y_c(Q)^DRbar
  3  3     1.01305191E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.93434000E+03  # (SUSY scale)
  1  1     9.71309277E-04   # Y_d(Q)^DRbar
  2  2     1.84548763E-02   # Y_s(Q)^DRbar
  3  3     9.63234439E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.93434000E+03  # (SUSY scale)
  1  1     1.69500572E-04   # Y_e(Q)^DRbar
  2  2     3.50473424E-02   # Y_mu(Q)^DRbar
  3  3     5.89438643E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.93434000E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.29247071E+03   # A_t(Q)^DRbar
Block Ad Q=  3.93434000E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.88019466E+02   # A_b(Q)^DRbar
Block Ae Q=  3.93434000E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.30798681E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.93434000E+03  # soft SUSY breaking masses at Q
   1    6.18916944E+01  # M_1
   2   -7.34261076E+02  # M_2
   3    2.14138085E+03  # M_3
  21    5.66937781E+06  # M^2_(H,d)
  22    3.23533338E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.92006701E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.91230656E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.72590492E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22933839E+02  # h0
        35     2.35792082E+03  # H0
        36     2.35997362E+03  # A0
        37     2.36262057E+03  # H+
   1000001     1.01167800E+04  # ~d_L
   2000001     1.00918142E+04  # ~d_R
   1000002     1.01164138E+04  # ~u_L
   2000002     1.00948292E+04  # ~u_R
   1000003     1.01167850E+04  # ~s_L
   2000003     1.00918233E+04  # ~s_R
   1000004     1.01164188E+04  # ~c_L
   2000004     1.00948299E+04  # ~c_R
   1000005     3.74112479E+03  # ~b_1
   2000005     3.94120795E+03  # ~b_2
   1000006     3.90692626E+03  # ~t_1
   2000006     3.96194609E+03  # ~t_2
   1000011     1.00211893E+04  # ~e_L-
   2000011     1.00091201E+04  # ~e_R-
   1000012     1.00204175E+04  # ~nu_eL
   1000013     1.00212143E+04  # ~mu_L-
   2000013     1.00091681E+04  # ~mu_R-
   1000014     1.00204423E+04  # ~nu_muL
   1000015     1.00226892E+04  # ~tau_1-
   2000015     1.00283915E+04  # ~tau_2-
   1000016     1.00274514E+04  # ~nu_tauL
   1000021     2.57654039E+03  # ~g
   1000022     5.88603283E+01  # ~chi_10
   1000023     2.02945460E+02  # ~chi_20
   1000025     2.14674053E+02  # ~chi_30
   1000035     8.02518089E+02  # ~chi_40
   1000024     2.02820926E+02  # ~chi_1+
   1000037     8.02615274E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.73795850E-02   # alpha
Block Hmix Q=  3.93434000E+03  # Higgs mixing parameters
   1    1.84720039E+02  # mu
   2    5.76423130E+01  # tan[beta](Q)
   3    2.42954841E+02  # v(Q)
   4    5.56947549E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.89890020E-01   # Re[R_st(1,1)]
   1  2     8.07483600E-01   # Re[R_st(1,2)]
   2  1    -8.07483600E-01   # Re[R_st(2,1)]
   2  2     5.89890020E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.26141419E-02   # Re[R_sb(1,1)]
   1  2     9.99920439E-01   # Re[R_sb(1,2)]
   2  1    -9.99920439E-01   # Re[R_sb(2,1)]
   2  2     1.26141419E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.67485095E-01   # Re[R_sta(1,1)]
   1  2     9.85874608E-01   # Re[R_sta(1,2)]
   2  1    -9.85874608E-01   # Re[R_sta(2,1)]
   2  2     1.67485095E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.71107246E-01   # Re[N(1,1)]
   1  2    -7.08272219E-03   # Re[N(1,2)]
   1  3    -2.28253690E-01   # Re[N(1,3)]
   1  4     6.92878445E-02   # Re[N(1,4)]
   2  1     1.14676697E-01   # Re[N(2,1)]
   2  2     9.25305216E-02   # Re[N(2,2)]
   2  3    -7.02208290E-01   # Re[N(2,3)]
   2  4    -6.96556440E-01   # Re[N(2,4)]
   3  1     2.09212152E-01   # Re[N(3,1)]
   3  2    -5.74642033E-02   # Re[N(3,2)]
   3  3    -6.73935996E-01   # Re[N(3,3)]
   3  4     7.06214142E-01   # Re[N(3,4)]
   4  1     5.49982898E-03   # Re[N(4,1)]
   4  2    -9.94025051E-01   # Re[N(4,2)]
   4  3    -2.47799055E-02   # Re[N(4,3)]
   4  4    -1.06159810E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.51358398E-02   # Re[U(1,1)]
   1  2     9.99382546E-01   # Re[U(1,2)]
   2  1    -9.99382546E-01   # Re[U(2,1)]
   2  2    -3.51358398E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.50396369E-01   # Re[V(1,1)]
   1  2     9.88625780E-01   # Re[V(1,2)]
   2  1     9.88625780E-01   # Re[V(2,1)]
   2  2    -1.50396369E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02863156E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.43093892E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.31415205E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.37346095E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43231170E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.51547462E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     7.27786367E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.07223968E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.98620801E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     7.59730722E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.07279635E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07753169E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.34266671E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.53883128E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.54994458E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.80720302E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43476211E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.50984401E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.10635361E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.68015432E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.98112592E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     7.58435088E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06244025E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.89476146E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.75709110E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.85803646E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.66248523E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.21260691E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.59437341E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.58877235E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     2.11913144E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.92446179E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.64140888E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.59139796E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.98620549E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.37911264E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.03475036E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43235699E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.06201051E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.00799919E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.81032429E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02138972E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.39183491E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.94211449E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43480717E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.04826316E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.00286996E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.79530095E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01623784E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.55976843E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.93200312E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.12591175E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.43568385E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.02809831E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.94022973E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.03716122E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.34742453E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.01041548E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.01751725E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.57100252E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.05631607E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     3.51220550E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91971636E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.31649753E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.47261095E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.78302795E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.80048392E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.23446308E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.32095054E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.05612251E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.39680061E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.01888799E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.57459189E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.53477475E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     3.95237757E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91779053E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.31722212E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.47676506E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.18772827E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.17302703E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.23401472E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.36371851E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.05603202E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.39607438E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.20690520E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.56628475E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.53593761E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.41159934E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.09496599E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.98445573E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.69789253E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.02454736E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.37572438E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.18205315E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.09446705E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.23693891E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.57616379E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.11343054E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.37233191E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.18924668E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.95693941E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.12037555E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.37125584E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.68646376E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.31637118E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.73486509E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.82542276E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.21376011E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.42042856E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.03348725E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.39653991E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.18931981E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.95691184E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.14490558E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.37376855E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.68636566E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.31709540E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.73473592E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.84612434E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.21331321E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.50223931E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.03339877E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.39581370E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.98758604E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.73714828E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.39681797E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.07234045E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.25715525E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.18167449E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.19957578E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.28852180E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.24034478E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.83929676E-02    2     1000021         4   # BR(~t_1 -> ~g c)
     3.57777268E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     2.34339368E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.84647816E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.35551663E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.40611425E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.56415812E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.65724220E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.90701482E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.97447910E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.27582701E-01    2     1000021         6   # BR(~t_2 -> ~g t)
DECAY   1000024     9.38663159E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999637E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     6.72053289E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     8.37422543E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.56506322E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.42316584E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47961645E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37485596E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.62242021E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.02637931E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.98947694E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.62906186E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.38704310E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     7.86798933E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.13166642E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     7.26814209E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     4.61477407E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.38497229E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     7.64113692E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.27053366E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.27053366E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.85317794E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.16332998E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.58324350E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.09241632E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.49768141E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.60555624E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.64964979E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.42664862E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.23187470E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.23187470E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.36532731E-01   # ~g
#    BR                NDA      ID1      ID2
     2.40766065E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.92369490E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.55001660E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.55001941E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     8.90974844E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.66810422E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.07733958E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.21790228E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.13030212E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.11900229E-01    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.95778343E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.95778347E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     1.02965308E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.96519767E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.96519777E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     1.29167408E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.32839683E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.32839683E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.79734264E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.79734264E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.79734714E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.79734714E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     1.23478585E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.23478585E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.38906183E-03   # Gamma(h0)
     2.50025183E-03   2        22        22   # BR(h0 -> photon photon)
     1.41573218E-03   2        22        23   # BR(h0 -> photon Z)
     2.43301306E-02   2        23        23   # BR(h0 -> Z Z)
     2.09509357E-01   2       -24        24   # BR(h0 -> W W)
     7.91630217E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.10136816E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.26916838E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.54254406E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.44859989E-07   2        -2         2   # BR(h0 -> Up up)
     2.81127637E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.92221448E-07   2        -1         1   # BR(h0 -> Down down)
     2.14192325E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.69248472E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.98529795E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.12886528E+02   # Gamma(HH)
     4.92833516E-08   2        22        22   # BR(HH -> photon photon)
     7.99354116E-08   2        22        23   # BR(HH -> photon Z)
     1.02515359E-07   2        23        23   # BR(HH -> Z Z)
     4.29992476E-08   2       -24        24   # BR(HH -> W W)
     1.36692035E-05   2        21        21   # BR(HH -> gluon gluon)
     9.48527329E-09   2       -11        11   # BR(HH -> Electron electron)
     4.22297182E-04   2       -13        13   # BR(HH -> Muon muon)
     1.21959860E-01   2       -15        15   # BR(HH -> Tau tau)
     1.54284338E-14   2        -2         2   # BR(HH -> Up up)
     2.99271382E-09   2        -4         4   # BR(HH -> Charm charm)
     2.09678268E-04   2        -6         6   # BR(HH -> Top top)
     7.12758007E-07   2        -1         1   # BR(HH -> Down down)
     2.57805414E-04   2        -3         3   # BR(HH -> Strange strange)
     6.47612911E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.35980528E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.63053169E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.63053169E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.27718280E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.65126378E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.17060935E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.92631680E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.72432517E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.03823423E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.01133648E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.26148953E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.41790160E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.12036139E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.45055579E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     5.46329939E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.05738603E+02   # Gamma(A0)
     1.22170836E-09   2        22        22   # BR(A0 -> photon photon)
     1.52834947E-08   2        22        23   # BR(A0 -> photon Z)
     2.01134628E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.29105220E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.13651207E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.19463207E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.44503941E-14   2        -2         2   # BR(A0 -> Up up)
     2.80288327E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.01068138E-04   2        -6         6   # BR(A0 -> Top top)
     6.98086517E-07   2        -1         1   # BR(A0 -> Down down)
     2.52498930E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.34304888E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.69795322E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.08444036E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.08444036E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.67445614E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.90562577E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.16147621E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     8.09442116E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.00944683E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.06131864E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.52891176E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.63216816E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.85932969E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.18403274E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     8.48674590E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.55806626E-07   2        23        25   # BR(A0 -> Z h0)
     3.78546172E-13   2        23        35   # BR(A0 -> Z HH)
     7.68960442E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.25562436E+02   # Gamma(Hp)
     1.03739278E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.43517812E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.25452004E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.75425796E-07   2        -1         2   # BR(Hp -> Down up)
     1.13694616E-05   2        -3         2   # BR(Hp -> Strange up)
     7.06534718E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.15918131E-08   2        -1         4   # BR(Hp -> Down charm)
     2.43419427E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.89401178E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.48901926E-08   2        -1         6   # BR(Hp -> Down top)
     6.82595157E-07   2        -3         6   # BR(Hp -> Strange top)
     6.66394529E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.20466882E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.75242884E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.19709359E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.13180659E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     5.17773841E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.47785890E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.03125020E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.28274474E-09   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.31786023E-07   2        24        25   # BR(Hp -> W h0)
     2.48457233E-11   2        24        35   # BR(Hp -> W HH)
     1.40796617E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00380317E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.32263244E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.32263625E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99998855E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.02110462E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.00965837E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00380317E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.32263244E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.32263625E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.08623761E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.08623761E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26528812E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.22242167E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.68870544E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.08623761E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.22696042E-04   # BR(b -> s gamma)
    2    1.58935496E-06   # BR(b -> s mu+ mu-)
    3    3.52380583E-05   # BR(b -> s nu nu)
    4    3.23851328E-15   # BR(Bd -> e+ e-)
    5    1.38341199E-10   # BR(Bd -> mu+ mu-)
    6    2.87093985E-08   # BR(Bd -> tau+ tau-)
    7    1.05991542E-13   # BR(Bs -> e+ e-)
    8    4.52783479E-09   # BR(Bs -> mu+ mu-)
    9    9.53445507E-07   # BR(Bs -> tau+ tau-)
   10    9.37829591E-05   # BR(B_u -> tau nu)
   11    9.68742063E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41976442E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93452761E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15753869E-03   # epsilon_K
   17    2.28166439E-15   # Delta(M_K)
   18    2.47908694E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28829983E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.77112743E-16   # Delta(g-2)_electron/2
   21   -2.46737813E-11   # Delta(g-2)_muon/2
   22   -7.00865610E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.71751430E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.89671572E-01   # C7
     0305 4322   00   2    -1.55967983E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.04554350E-01   # C8
     0305 6321   00   2    -2.53784197E-04   # C8'
 03051111 4133   00   0     1.62737134E+00   # C9 e+e-
 03051111 4133   00   2     1.62767686E+00   # C9 e+e-
 03051111 4233   00   2     6.66255423E-04   # C9' e+e-
 03051111 4137   00   0    -4.45006344E+00   # C10 e+e-
 03051111 4137   00   2    -4.44714342E+00   # C10 e+e-
 03051111 4237   00   2    -4.92426500E-03   # C10' e+e-
 03051313 4133   00   0     1.62737134E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62767661E+00   # C9 mu+mu-
 03051313 4233   00   2     6.66252086E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45006344E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44714367E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.92426344E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50472028E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.06464734E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50472028E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.06464796E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50472028E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.06482401E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819695E-07   # C7
     0305 4422   00   2    -9.19581315E-06   # C7
     0305 4322   00   2     6.87664332E-07   # C7'
     0305 6421   00   0     3.30480085E-07   # C8
     0305 6421   00   2    -2.27538686E-05   # C8
     0305 6321   00   2    -1.22347219E-07   # C8'
 03051111 4133   00   2     4.65551872E-07   # C9 e+e-
 03051111 4233   00   2     1.40671427E-05   # C9' e+e-
 03051111 4137   00   2     5.79566195E-07   # C10 e+e-
 03051111 4237   00   2    -1.03983333E-04   # C10' e+e-
 03051313 4133   00   2     4.65545097E-07   # C9 mu+mu-
 03051313 4233   00   2     1.40671348E-05   # C9' mu+mu-
 03051313 4137   00   2     5.79573763E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.03983357E-04   # C10' mu+mu-
 03051212 4137   00   2    -9.88422077E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.24816480E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.88418613E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.24816480E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.87468610E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.24816479E-05   # C11' nu_3 nu_3
