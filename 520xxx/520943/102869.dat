# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  14:53
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.67231745E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.09543683E+03  # scale for input parameters
    1    4.35685252E+02  # M_1
    2    8.57315383E+02  # M_2
    3    3.41331308E+03  # M_3
   11   -2.48221277E+02  # A_t
   12   -1.55879567E+03  # A_b
   13    8.69555143E+02  # A_tau
   23    1.30356511E+03  # mu
   25    1.59787200E+01  # tan(beta)
   26    4.73170230E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.02844576E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.96212267E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.50671583E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.09543683E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.09543683E+03  # (SUSY scale)
  1  1     8.40077546E-06   # Y_u(Q)^DRbar
  2  2     4.26759393E-03   # Y_c(Q)^DRbar
  3  3     1.01488115E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.09543683E+03  # (SUSY scale)
  1  1     2.69737676E-04   # Y_d(Q)^DRbar
  2  2     5.12501584E-03   # Y_s(Q)^DRbar
  3  3     2.67495251E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.09543683E+03  # (SUSY scale)
  1  1     4.70711969E-05   # Y_e(Q)^DRbar
  2  2     9.73283062E-03   # Y_mu(Q)^DRbar
  3  3     1.63690200E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.09543683E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.48221279E+02   # A_t(Q)^DRbar
Block Ad Q=  3.09543683E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.55879564E+03   # A_b(Q)^DRbar
Block Ae Q=  3.09543683E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.69555140E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.09543683E+03  # soft SUSY breaking masses at Q
   1    4.35685252E+02  # M_1
   2    8.57315383E+02  # M_2
   3    3.41331308E+03  # M_3
  21    2.05097121E+07  # M^2_(H,d)
  22   -1.42847895E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.02844576E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.96212267E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.50671583E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19495384E+02  # h0
        35     4.73118873E+03  # H0
        36     4.73170230E+03  # A0
        37     4.73369780E+03  # H+
   1000001     1.00961728E+04  # ~d_L
   2000001     1.00719205E+04  # ~d_R
   1000002     1.00958045E+04  # ~u_L
   2000002     1.00752440E+04  # ~u_R
   1000003     1.00961746E+04  # ~s_L
   2000003     1.00719225E+04  # ~s_R
   1000004     1.00958064E+04  # ~c_L
   2000004     1.00752455E+04  # ~c_R
   1000005     3.13788665E+03  # ~b_1
   2000005     3.60871668E+03  # ~b_2
   1000006     3.04973533E+03  # ~t_1
   2000006     3.14182319E+03  # ~t_2
   1000011     1.00209716E+04  # ~e_L-
   2000011     1.00086023E+04  # ~e_R-
   1000012     1.00202083E+04  # ~nu_eL
   1000013     1.00209781E+04  # ~mu_L-
   2000013     1.00086121E+04  # ~mu_R-
   1000014     1.00202138E+04  # ~nu_muL
   1000015     1.00113907E+04  # ~tau_1-
   2000015     1.00228260E+04  # ~tau_2-
   1000016     1.00217815E+04  # ~nu_tauL
   1000021     3.83881436E+03  # ~g
   1000022     4.39380823E+02  # ~chi_10
   1000023     9.18704431E+02  # ~chi_20
   1000025     1.32176700E+03  # ~chi_30
   1000035     1.33038509E+03  # ~chi_40
   1000024     9.18390023E+02  # ~chi_1+
   1000037     1.33020550E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.97854169E-02   # alpha
Block Hmix Q=  3.09543683E+03  # Higgs mixing parameters
   1    1.30356511E+03  # mu
   2    1.59787200E+01  # tan[beta](Q)
   3    2.43242294E+02  # v(Q)
   4    2.23890067E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.27569443E-01   # Re[R_st(1,1)]
   1  2     9.91829641E-01   # Re[R_st(1,2)]
   2  1    -9.91829641E-01   # Re[R_st(2,1)]
   2  2     1.27569443E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99887247E-01   # Re[R_sb(1,1)]
   1  2     1.50164599E-02   # Re[R_sb(1,2)]
   2  1    -1.50164599E-02   # Re[R_sb(2,1)]
   2  2     9.99887247E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.58571528E-01   # Re[R_sta(1,1)]
   1  2     9.87347492E-01   # Re[R_sta(1,2)]
   2  1    -9.87347492E-01   # Re[R_sta(2,1)]
   2  2     1.58571528E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99114151E-01   # Re[N(1,1)]
   1  2    -2.85925180E-03   # Re[N(1,2)]
   1  3     3.91236508E-02   # Re[N(1,3)]
   1  4    -1.52341100E-02   # Re[N(1,4)]
   2  1    -8.70644627E-03   # Re[N(2,1)]
   2  2    -9.89487877E-01   # Re[N(2,2)]
   2  3     1.17222764E-01   # Re[N(2,3)]
   2  4    -8.42422821E-02   # Re[N(2,4)]
   3  1     1.68044363E-02   # Re[N(3,1)]
   3  2    -2.36003188E-02   # Re[N(3,2)]
   3  3    -7.06288563E-01   # Re[N(3,3)]
   3  4    -7.07330971E-01   # Re[N(3,4)]
   4  1    -3.75861969E-02   # Re[N(4,1)]
   4  2     1.42648485E-01   # Re[N(4,2)]
   4  3     6.97054252E-01   # Re[N(4,3)]
   4  4    -7.01679455E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.85796210E-01   # Re[U(1,1)]
   1  2     1.67945918E-01   # Re[U(1,2)]
   2  1     1.67945918E-01   # Re[U(2,1)]
   2  2     9.85796210E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.92599395E-01   # Re[V(1,1)]
   1  2     1.21434929E-01   # Re[V(1,2)]
   2  1     1.21434929E-01   # Re[V(2,1)]
   2  2     9.92599395E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.00932179E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.98282883E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.73697733E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.36858861E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42588745E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.68485097E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.00601233E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.48481686E-03    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.91161064E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.68405102E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01296886E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.97558162E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.54700548E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.54400839E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.52843165E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.42607075E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.68376054E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.00564583E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.27638499E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.54634128E-03    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.91085475E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.68383572E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.26386818E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.85189014E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.71001602E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.95453771E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.28797073E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     5.37555695E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.15301714E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.45546283E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.09759402E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.83889362E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.84778465E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.51257635E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.57115318E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.44157697E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42592757E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.87202705E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.94970468E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.16937605E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     7.90420156E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.99284609E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     8.80351388E-03    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42611071E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.87089262E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.94932755E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.16897091E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     7.90319116E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.99211655E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     8.92657474E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47774236E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.56229733E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.84673978E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.05876085E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.62833614E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.79366472E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.24023641E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.86409664E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.53561715E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90447927E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.15615937E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.02167025E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.96096922E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.33730434E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.18704503E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.38250637E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.14901893E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.86419998E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.53546462E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90430803E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.15624751E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.02165691E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.96090676E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.34073805E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.18703127E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.38733782E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.14892099E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.09904286E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.49925362E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.39997340E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.86542214E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.03841404E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.81619798E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.33796039E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     9.90352476E+00   # ~b_2
#    BR                NDA      ID1      ID2
     1.97270679E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.79083689E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.94512910E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.90740019E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.80151324E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.81776792E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.68000413E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     9.70242052E-03    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     4.98596986E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     9.15085826E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.03543217E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.70719915E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.62864216E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.15598857E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.89576478E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.99836357E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.10828579E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.20346901E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.76841863E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.14871518E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.03550385E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.70715566E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.62852971E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.15607651E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.89574298E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.99829412E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.11070633E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.20345648E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.77529694E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.14861719E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.79346303E+01   # ~t_1
#    BR                NDA      ID1      ID2
     7.32829857E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.17661190E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.29697986E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.18050437E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.43371415E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.42835001E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     8.16526728E+01   # ~t_2
#    BR                NDA      ID1      ID2
     5.61002615E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.32397671E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.66806206E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.74586436E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.62802377E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.77962023E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
DECAY   1000024     1.72072502E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.91780855E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     8.21804546E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     7.76906479E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.19480919E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.03534467E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.89017694E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.83123503E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.10468975E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.51212399E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.02065922E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.56722725E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     8.80884145E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     9.11030231E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.71377292E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     1.04907689E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.34710054E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.34710054E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.86635274E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.24562594E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.48514660E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.68920633E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.52336260E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.35762142E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.35762142E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     7.45056046E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.91349007E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.91349007E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.11472556E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.39794884E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.53056916E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.87014293E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.15292778E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.11339352E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.11339352E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.11907352E+01   # ~g
#    BR                NDA      ID1      ID2
     1.72703511E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.72703511E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.56560321E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.56560321E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.51853804E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.51853804E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.86593631E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.86593631E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.62751811E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.52327326E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.27888063E-03   # Gamma(h0)
     2.30669218E-03   2        22        22   # BR(h0 -> photon photon)
     1.04179968E-03   2        22        23   # BR(h0 -> photon Z)
     1.58510029E-02   2        23        23   # BR(h0 -> Z Z)
     1.46222684E-01   2       -24        24   # BR(h0 -> W W)
     7.54209415E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.80478556E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.58203726E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.44408604E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.66539001E-07   2        -2         2   # BR(h0 -> Up up)
     3.23137663E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.77391686E-07   2        -1         1   # BR(h0 -> Down down)
     2.44996450E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.51898203E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.66436876E+01   # Gamma(HH)
     4.42514148E-08   2        22        22   # BR(HH -> photon photon)
     9.10314977E-08   2        22        23   # BR(HH -> photon Z)
     7.09262754E-07   2        23        23   # BR(HH -> Z Z)
     1.24290767E-07   2       -24        24   # BR(HH -> W W)
     3.00181852E-07   2        21        21   # BR(HH -> gluon gluon)
     3.08393121E-09   2       -11        11   # BR(HH -> Electron electron)
     1.37330073E-04   2       -13        13   # BR(HH -> Muon muon)
     3.96654462E-02   2       -15        15   # BR(HH -> Tau tau)
     6.59233759E-13   2        -2         2   # BR(HH -> Up up)
     1.27895490E-07   2        -4         4   # BR(HH -> Charm charm)
     8.36002920E-03   2        -6         6   # BR(HH -> Top top)
     2.10837308E-07   2        -1         1   # BR(HH -> Down down)
     7.62612946E-05   2        -3         3   # BR(HH -> Strange strange)
     1.84613222E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.03665316E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.25671344E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.25671344E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     3.08358068E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.19987748E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.24149056E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.12613383E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.75975737E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.08392471E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.37266488E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.08254807E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.10821484E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.38055627E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     3.26097731E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.66130084E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.60016565E+01   # Gamma(A0)
     7.35691805E-08   2        22        22   # BR(A0 -> photon photon)
     1.13725288E-07   2        22        23   # BR(A0 -> photon Z)
     6.12591776E-06   2        21        21   # BR(A0 -> gluon gluon)
     2.99136037E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.33207932E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.84748611E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.95092865E-13   2        -2         2   # BR(A0 -> Up up)
     1.15449036E-07   2        -4         4   # BR(A0 -> Charm charm)
     7.54049009E-03   2        -6         6   # BR(A0 -> Top top)
     2.04506163E-07   2        -1         1   # BR(A0 -> Down down)
     7.39711919E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.79071466E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.41542621E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.24408987E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.24408987E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     6.39964845E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.52168724E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.77927201E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.93048419E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.97510108E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.94862387E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     8.70254740E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.30334028E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.40810082E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.63344966E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     6.05595835E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.84522735E-06   2        23        25   # BR(A0 -> Z h0)
     2.85562418E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.70041037E+01   # Gamma(Hp)
     3.26736163E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.39689915E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.95122678E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.94981615E-07   2        -1         2   # BR(Hp -> Down up)
     3.30785658E-06   2        -3         2   # BR(Hp -> Strange up)
     1.98616042E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.48098618E-08   2        -1         4   # BR(Hp -> Down charm)
     7.03915195E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.78133768E-04   2        -5         4   # BR(Hp -> Bottom charm)
     6.21537881E-07   2        -1         6   # BR(Hp -> Down top)
     1.36565701E-05   2        -3         6   # BR(Hp -> Strange top)
     1.96826713E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.91191383E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.50030615E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.28956263E-07   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.34528746E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.26459745E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.05795680E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.27807223E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.82023422E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.82037886E-06   2        24        25   # BR(Hp -> W h0)
     2.02087404E-12   2        24        35   # BR(Hp -> W HH)
     6.43005061E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.15070733E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.55404422E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.55319493E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00033264E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.58402221E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.91666139E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.15070733E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.55404422E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.55319493E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99992622E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.37829196E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99992622E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.37829196E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26919320E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.04999012E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.61987895E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.37829196E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99992622E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.20581554E-04   # BR(b -> s gamma)
    2    1.58974695E-06   # BR(b -> s mu+ mu-)
    3    3.52419504E-05   # BR(b -> s nu nu)
    4    2.57871834E-15   # BR(Bd -> e+ e-)
    5    1.10159908E-10   # BR(Bd -> mu+ mu-)
    6    2.30601690E-08   # BR(Bd -> tau+ tau-)
    7    8.67804356E-14   # BR(Bs -> e+ e-)
    8    3.70725707E-09   # BR(Bs -> mu+ mu-)
    9    7.86319514E-07   # BR(Bs -> tau+ tau-)
   10    9.67342411E-05   # BR(B_u -> tau nu)
   11    9.99227677E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42154494E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93712333E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15799605E-03   # epsilon_K
   17    2.28166502E-15   # Delta(M_K)
   18    2.47905900E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28829441E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.51117300E-16   # Delta(g-2)_electron/2
   21    6.46074854E-12   # Delta(g-2)_muon/2
   22    1.82835077E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.27519767E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88722023E-01   # C7
     0305 4322   00   2    -4.37420647E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.81542948E-02   # C8
     0305 6321   00   2    -8.03285258E-05   # C8'
 03051111 4133   00   0     1.61735123E+00   # C9 e+e-
 03051111 4133   00   2     1.61771599E+00   # C9 e+e-
 03051111 4233   00   2     6.12750967E-05   # C9' e+e-
 03051111 4137   00   0    -4.44004333E+00   # C10 e+e-
 03051111 4137   00   2    -4.43733745E+00   # C10 e+e-
 03051111 4237   00   2    -4.59055692E-04   # C10' e+e-
 03051313 4133   00   0     1.61735123E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61771597E+00   # C9 mu+mu-
 03051313 4233   00   2     6.12750917E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44004333E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43733748E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.59055699E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50476514E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.94731731E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50476514E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.94731735E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50476514E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.94732811E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85824036E-07   # C7
     0305 4422   00   2     5.51491543E-06   # C7
     0305 4322   00   2     3.23692157E-07   # C7'
     0305 6421   00   0     3.30483802E-07   # C8
     0305 6421   00   2    -1.90592234E-06   # C8
     0305 6321   00   2     3.26390921E-08   # C8'
 03051111 4133   00   2     4.75180518E-07   # C9 e+e-
 03051111 4233   00   2     1.15415374E-06   # C9' e+e-
 03051111 4137   00   2    -4.44146032E-08   # C10 e+e-
 03051111 4237   00   2    -8.64741055E-06   # C10' e+e-
 03051313 4133   00   2     4.75179969E-07   # C9 mu+mu-
 03051313 4233   00   2     1.15415368E-06   # C9' mu+mu-
 03051313 4137   00   2    -4.44140535E-08   # C10 mu+mu-
 03051313 4237   00   2    -8.64741072E-06   # C10' mu+mu-
 03051212 4137   00   2     3.38835016E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.87381554E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     3.38835271E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.87381554E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     3.38907309E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.87381554E-06   # C11' nu_3 nu_3
