# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  15:45
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    9.84475109E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.16746739E+03  # scale for input parameters
    1   -5.18369513E+01  # M_1
    2   -7.12950403E+02  # M_2
    3    4.94680337E+03  # M_3
   11   -6.70081861E+02  # A_t
   12   -1.17470121E+03  # A_b
   13   -1.27084761E+02  # A_tau
   23    1.71417221E+03  # mu
   25    9.38170168E+00  # tan(beta)
   26    4.77842186E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.95868543E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.20877521E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.32716681E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.16746739E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.16746739E+03  # (SUSY scale)
  1  1     8.43186725E-06   # Y_u(Q)^DRbar
  2  2     4.28338856E-03   # Y_c(Q)^DRbar
  3  3     1.01863729E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.16746739E+03  # (SUSY scale)
  1  1     1.58959185E-04   # Y_d(Q)^DRbar
  2  2     3.02022451E-03   # Y_s(Q)^DRbar
  3  3     1.57637701E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.16746739E+03  # (SUSY scale)
  1  1     2.77395402E-05   # Y_e(Q)^DRbar
  2  2     5.73565713E-03   # Y_mu(Q)^DRbar
  3  3     9.64643172E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.16746739E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.70081881E+02   # A_t(Q)^DRbar
Block Ad Q=  3.16746739E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.17470117E+03   # A_b(Q)^DRbar
Block Ae Q=  3.16746739E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.27084760E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.16746739E+03  # soft SUSY breaking masses at Q
   1   -5.18369513E+01  # M_1
   2   -7.12950403E+02  # M_2
   3    4.94680337E+03  # M_3
  21    1.95611391E+07  # M^2_(H,d)
  22   -2.51596246E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.95868543E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.20877521E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.32716681E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19494321E+02  # h0
        35     4.77816339E+03  # H0
        36     4.77842186E+03  # A0
        37     4.78053819E+03  # H+
   1000001     1.00740371E+04  # ~d_L
   2000001     1.00508654E+04  # ~d_R
   1000002     1.00737829E+04  # ~u_L
   2000002     1.00540513E+04  # ~u_R
   1000003     1.00740383E+04  # ~s_L
   2000003     1.00508660E+04  # ~s_R
   1000004     1.00737841E+04  # ~c_L
   2000004     1.00540527E+04  # ~c_R
   1000005     2.42252059E+03  # ~b_1
   2000005     3.05448497E+03  # ~b_2
   1000006     3.05489742E+03  # ~t_1
   2000006     3.28418545E+03  # ~t_2
   1000011     1.00211002E+04  # ~e_L-
   2000011     1.00087019E+04  # ~e_R-
   1000012     1.00203409E+04  # ~nu_eL
   1000013     1.00211024E+04  # ~mu_L-
   2000013     1.00087047E+04  # ~mu_R-
   1000014     1.00203426E+04  # ~nu_muL
   1000015     1.00094803E+04  # ~tau_1-
   2000015     1.00217511E+04  # ~tau_2-
   1000016     1.00208246E+04  # ~nu_tauL
   1000021     5.33868402E+03  # ~g
   1000022     5.33008326E+01  # ~chi_10
   1000023     7.76835534E+02  # ~chi_20
   1000025     1.73610087E+03  # ~chi_30
   1000035     1.73703573E+03  # ~chi_40
   1000024     7.76655327E+02  # ~chi_1+
   1000037     1.73840713E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.01319769E-01   # alpha
Block Hmix Q=  3.16746739E+03  # Higgs mixing parameters
   1    1.71417221E+03  # mu
   2    9.38170168E+00  # tan[beta](Q)
   3    2.43213770E+02  # v(Q)
   4    2.28333155E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.96232040E-01   # Re[R_st(1,1)]
   1  2     8.67278686E-02   # Re[R_st(1,2)]
   2  1    -8.67278686E-02   # Re[R_st(2,1)]
   2  2     9.96232040E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.09511122E-02   # Re[R_sb(1,1)]
   1  2     9.99940035E-01   # Re[R_sb(1,2)]
   2  1    -9.99940035E-01   # Re[R_sb(2,1)]
   2  2     1.09511122E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.17795437E-01   # Re[R_sta(1,1)]
   1  2     9.93037882E-01   # Re[R_sta(1,2)]
   2  1    -9.93037882E-01   # Re[R_sta(2,1)]
   2  2     1.17795437E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99671363E-01   # Re[N(1,1)]
   1  2    -5.34105466E-04   # Re[N(1,2)]
   1  3    -2.55586105E-02   # Re[N(1,3)]
   1  4     1.90719602E-03   # Re[N(1,4)]
   2  1     8.04301838E-04   # Re[N(2,1)]
   2  2     9.98380017E-01   # Re[N(2,2)]
   2  3    -5.37197813E-02   # Re[N(2,3)]
   2  4    -1.87318077E-02   # Re[N(2,4)]
   3  1     1.94228447E-02   # Re[N(3,1)]
   3  2    -2.47579774E-02   # Re[N(3,2)]
   3  3    -7.06401378E-01   # Re[N(3,3)]
   3  4     7.07111652E-01   # Re[N(3,4)]
   4  1     1.67114165E-02   # Re[N(4,1)]
   4  2    -5.12259672E-02   # Re[N(4,2)]
   4  3    -7.05307050E-01   # Re[N(4,3)]
   4  4    -7.06851182E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.97104878E-01   # Re[U(1,1)]
   1  2     7.60385519E-02   # Re[U(1,2)]
   2  1    -7.60385519E-02   # Re[U(2,1)]
   2  2    -9.97104878E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99641638E-01   # Re[V(1,1)]
   1  2     2.67693203E-02   # Re[V(1,2)]
   2  1     2.67693203E-02   # Re[V(2,1)]
   2  2    -9.99641638E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02852626E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99381708E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.54922516E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.62727771E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43298565E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.79581833E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03393883E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.18115946E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04721540E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.34837559E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02976026E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99136882E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.15970918E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.23605268E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.21787231E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.43304768E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.79544247E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03380947E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.39513474E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04695497E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.34823146E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.50578843E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.03874939E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.30738809E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.52244204E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.44720469E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.60699367E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.72847763E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.43770544E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.11227135E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.97478589E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.46316415E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.13794894E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.92853943E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.94364145E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43302056E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.75982407E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.02841162E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.57451497E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.04774108E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.07740456E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.14948936E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43308252E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.75944683E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.02828121E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.57436111E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.04769598E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.07714549E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.57729216E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.45054761E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.65439638E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.99196705E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.53151546E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.03513732E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.00500161E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.23708808E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.14443742E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.35297550E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.86461847E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.44077580E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.56699439E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     8.02477127E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.25406495E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.60132545E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     8.87126949E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.55879120E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.14447229E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.35296448E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.86453698E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.44082899E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.56697178E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     8.02469472E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.26973841E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.60130996E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     8.93483136E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.55871960E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.91361218E+00   # ~b_1
#    BR                NDA      ID1      ID2
     7.06511886E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.75486474E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.37234709E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.31384748E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.40693133E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.35464372E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     6.49427717E+01   # ~b_2
#    BR                NDA      ID1      ID2
     6.51381527E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.80426316E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.41308343E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.60902877E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.57000613E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.42646204E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     8.85272607E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.50566653E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.31616541E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.19822513E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.47985571E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.44070762E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.59795449E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     8.02927060E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.78615788E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.60946234E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.09956341E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.55839725E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.31623567E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.19814104E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.47970408E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.44076049E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.59792948E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     8.02919353E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.81790587E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.60944709E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.13090869E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.55832554E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.44326069E+01   # ~t_1
#    BR                NDA      ID1      ID2
     7.43871073E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.78814744E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.17260702E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.17507632E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.59701669E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.53907824E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.78730954E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.08682082E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     7.85968127E+01   # ~t_2
#    BR                NDA      ID1      ID2
     9.19557487E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.63244947E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.25710334E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.22388972E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     5.33332733E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.41994007E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.66134870E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     2.16280330E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.13514756E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.28665397E-04   # chi^+_1
#    BR                NDA      ID1      ID2
     9.83655812E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.62670627E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.04092841E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.12302101E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.88325629E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.82516833E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.93263729E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.83841903E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.82645816E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.09251808E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.27531412E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     9.57578697E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.74541790E-02    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     1.01547380E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     4.20583723E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     5.59859173E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
DECAY   1000025     1.35286981E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.18421420E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.18421420E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.66382487E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.78439478E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.07254336E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.80204078E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.77094677E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.92984255E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.72122535E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.72122535E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.04068412E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.83061887E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.83061887E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.41653361E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.39039413E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.39704070E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.41411421E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.51535075E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.79103937E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.20564215E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.20564215E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.03394315E+02   # ~g
#    BR                NDA      ID1      ID2
     1.15664951E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.15664951E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.02215456E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.02215456E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.64215702E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.64215702E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.17840129E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.17840129E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.88407894E-03   # Gamma(h0)
     2.61868716E-03   2        22        22   # BR(h0 -> photon photon)
     1.18281418E-03   2        22        23   # BR(h0 -> photon Z)
     1.80181917E-02   2        23        23   # BR(h0 -> Z Z)
     1.66218049E-01   2       -24        24   # BR(h0 -> W W)
     8.57301550E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.56186561E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.47398357E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.13255669E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.59660294E-07   2        -2         2   # BR(h0 -> Up up)
     3.09838470E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.48965587E-07   2        -1         1   # BR(h0 -> Down down)
     2.34714699E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.23227485E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.12277420E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     5.05636908E+01   # Gamma(HH)
     8.17961822E-10   2        22        22   # BR(HH -> photon photon)
     4.44222690E-08   2        22        23   # BR(HH -> photon Z)
     2.19171107E-06   2        23        23   # BR(HH -> Z Z)
     3.80078665E-07   2       -24        24   # BR(HH -> W W)
     2.89341867E-06   2        21        21   # BR(HH -> gluon gluon)
     1.18487030E-09   2       -11        11   # BR(HH -> Electron electron)
     5.27635503E-05   2       -13        13   # BR(HH -> Muon muon)
     1.52398645E-02   2       -15        15   # BR(HH -> Tau tau)
     2.03555591E-12   2        -2         2   # BR(HH -> Up up)
     3.94919396E-07   2        -4         4   # BR(HH -> Charm charm)
     2.60929924E-02   2        -6         6   # BR(HH -> Top top)
     8.06314318E-08   2        -1         1   # BR(HH -> Down down)
     2.91645046E-05   2        -3         3   # BR(HH -> Strange strange)
     6.62427698E-02   2        -5         5   # BR(HH -> Bottom bottom)
     4.11802994E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.67342240E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.67342240E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.72672986E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.33732066E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.08130664E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.35550291E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.93477759E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.05641657E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.22726551E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.42528396E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.32519635E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.13949172E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.47138904E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.29171487E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     5.03031715E+01   # Gamma(A0)
     7.59545631E-09   2        22        22   # BR(A0 -> photon photon)
     3.31329803E-08   2        22        23   # BR(A0 -> photon Z)
     1.09059432E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.14946698E-09   2       -11        11   # BR(A0 -> Electron electron)
     5.11870252E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.47845210E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.85378425E-12   2        -2         2   # BR(A0 -> Up up)
     3.59648577E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.37589717E-02   2        -6         6   # BR(A0 -> Top top)
     7.82221806E-08   2        -1         1   # BR(A0 -> Down down)
     2.82928931E-05   2        -3         3   # BR(A0 -> Strange strange)
     6.42675614E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     3.97830697E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.68916576E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.68916576E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.56721169E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.38461815E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.03249621E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.98863372E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.35030659E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.98576458E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.44243043E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.22551655E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     4.14203086E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.13008063E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.56115765E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     5.74872465E-06   2        23        25   # BR(A0 -> Z h0)
     2.45573306E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.20937473E+01   # Gamma(Hp)
     1.47082614E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     6.28824112E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.77867292E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.69190805E-08   2        -1         2   # BR(Hp -> Down up)
     1.47465490E-06   2        -3         2   # BR(Hp -> Strange up)
     8.48240615E-07   2        -5         2   # BR(Hp -> Bottom up)
     2.52667034E-08   2        -1         4   # BR(Hp -> Down charm)
     3.17744885E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.18784763E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.32732401E-06   2        -1         6   # BR(Hp -> Down top)
     5.07901791E-05   2        -3         6   # BR(Hp -> Strange top)
     1.15574795E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.04806137E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.03427308E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.08296925E-09   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.62384853E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.60236916E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     6.73711218E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.60444518E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.33129265E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     5.55987755E-06   2        24        25   # BR(Hp -> W h0)
     1.97436021E-12   2        24        35   # BR(Hp -> W HH)
     1.10968803E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.10691806E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    8.81056346E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    8.80163264E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00101468E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.03468509E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.13615285E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.10691806E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    8.81056346E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    8.80163264E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99976286E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.37143235E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99976286E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.37143235E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27041733E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.53947030E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.25742809E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.37143235E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99976286E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.21950812E-04   # BR(b -> s gamma)
    2    1.58986615E-06   # BR(b -> s mu+ mu-)
    3    3.52493998E-05   # BR(b -> s nu nu)
    4    2.55412495E-15   # BR(Bd -> e+ e-)
    5    1.09109315E-10   # BR(Bd -> mu+ mu-)
    6    2.28407838E-08   # BR(Bd -> tau+ tau-)
    7    8.59817657E-14   # BR(Bs -> e+ e-)
    8    3.67313820E-09   # BR(Bs -> mu+ mu-)
    9    7.79099200E-07   # BR(Bs -> tau+ tau-)
   10    9.67834109E-05   # BR(B_u -> tau nu)
   11    9.99735581E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42000588E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93659914E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15736341E-03   # epsilon_K
   17    2.28165854E-15   # Delta(M_K)
   18    2.47976604E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28991436E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.01400233E-16   # Delta(g-2)_electron/2
   21   -4.33517995E-12   # Delta(g-2)_muon/2
   22   -1.22649778E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -5.76810253E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.90451507E-01   # C7
     0305 4322   00   2    -5.45067443E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.87086855E-02   # C8
     0305 6321   00   2    -7.98219093E-05   # C8'
 03051111 4133   00   0     1.61807283E+00   # C9 e+e-
 03051111 4133   00   2     1.61826716E+00   # C9 e+e-
 03051111 4233   00   2     1.95423002E-05   # C9' e+e-
 03051111 4137   00   0    -4.44076493E+00   # C10 e+e-
 03051111 4137   00   2    -4.43878051E+00   # C10 e+e-
 03051111 4237   00   2    -1.45940444E-04   # C10' e+e-
 03051313 4133   00   0     1.61807283E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61826716E+00   # C9 mu+mu-
 03051313 4233   00   2     1.95422997E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44076493E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43878052E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.45940445E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50492146E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.16181766E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50492146E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.16181766E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50492146E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.16181891E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825678E-07   # C7
     0305 4422   00   2    -9.55641499E-07   # C7
     0305 4322   00   2     1.16469454E-07   # C7'
     0305 6421   00   0     3.30485209E-07   # C8
     0305 6421   00   2    -1.14843100E-06   # C8
     0305 6321   00   2     2.17592501E-08   # C8'
 03051111 4133   00   2     2.63885018E-07   # C9 e+e-
 03051111 4233   00   2     3.69537739E-07   # C9' e+e-
 03051111 4137   00   2     1.02361610E-06   # C10 e+e-
 03051111 4237   00   2    -2.76029533E-06   # C10' e+e-
 03051313 4133   00   2     2.63884850E-07   # C9 mu+mu-
 03051313 4233   00   2     3.69537733E-07   # C9' mu+mu-
 03051313 4137   00   2     1.02361629E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.76029534E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.98964469E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     5.98022304E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.98964460E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     5.98022304E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.98962010E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     5.98022303E-07   # C11' nu_3 nu_3
