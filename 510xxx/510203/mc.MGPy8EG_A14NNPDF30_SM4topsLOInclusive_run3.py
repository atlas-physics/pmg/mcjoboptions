from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

import fileinput

# Make some excess of events - make sure we protect against maxEvents=-1
evgenConfig.nEventsPerJob = 2000
nevents=1.1*runArgs.maxEvents if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

bwcut = 15.0
topdecay = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \n"

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~ t t~ QED=2 QCD=4
output -f"""

process_dir = new_process(process)

# copy custom scale file
scaleToCopy      = 'custom_HT_4_dyn_scale.f'
scaleDestination = process_dir+'/../custom_HT_4_dyn_scale.f'
scalefile        = subprocess.Popen(['get_files','-data',scaleToCopy])
scalefile.wait()

if not os.access(scaleToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(scaleToCopy))


#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
#Fetch default NLO run_card.dat and set parameters
settings = {
    'maxjetflavor'  : 5,
    'dynamical_scale_choice' :0, #user-defined scale
    'custom_fcts': scaleDestination,
    'bwcutoff'  : bwcut,
    'nevents':int(nevents)
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
# Modify the param card
# set_top_params(process_dir,mTop=172.5,FourFS=False)

#---------------------------------------------------------------------------
# Cook the setscales file for the user defined dynamical scale
#---------------------------------------------------------------------------
# fileN = process_dir+'/SubProcesses/setscales.f'
# mark  = '      elseif(dynamical_scale_choice.eq.0) then'
# rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
#            'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
#            'cc      to use this code you must set                                            cc',
#            'cc                 dynamical_scale_choice = 0                                    cc',
#            'cc      in the run_card (run_card.dat)                                           cc',
#            'write(*,*) "User-defined scale not set"',
#            'stop 21',
#            'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
#            'rscale = 0',
#            'tmp = 0',
#            'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
#            ]

# replace = True # replace the first occurence of the mark, by defalt fac scale = ren scale
# for line in fileinput.input(fileN, inplace=1):
#     toKeep = True
#     for rmLine in rmLines:
#         if line.find(rmLine) >= 0:
#            toKeep = False
#            break
#     if toKeep:
#         print(line.strip('\n'))
#     if line.startswith(mark) and replace:
#         print ("""
# c         sum of the transverse mass divide by 4
# c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
#           rscale=0d0
#           do i=3,nexternal
#             rscale=rscale+dsqrt(max(0d0,(P(0,i)+P(3,i))*(P(0,i)-P(3,i))))
#           enddo
#           rscale=rscale/4d0
#             """)
#         replace = False

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set Nevents_for_max_weight 75
set BW_cut %i
set seed %i
%s
launch
"""%(bwcut, runArgs.randomSeed, topdecay))
mscard.close()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------
check_reset_proc_number(opts)

# Metadata
evgenConfig.description = 'Standard-Model 4tops production at LO with MadGraph5 and Pythia8'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.keywords += ['SM', 'top', '4top', 'LO']
evgenConfig.contact = ["nedaa.asbah@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Gen_tf.py --ecmEnergy=13000.0 --randomSeed=1234 --jobConfig=./../999999 --outputEVNTFile=tmp.EVNT.root --maxEvents=100
# pathena --trf "Gen_tf.py --ecmEnergy=13600.0 --randomSeed=%RNDM:100 --jobConfig=./999999 --outputEVNTFile=%OUT.EVNT.root --maxEvents=2000" --extFile=custom_HT_4_dyn_scale.f --outDS=user.mmiralle.MG5_353_4tops.v1 --split=20
