###############
# LO TEMPLATE
###############

      double precision function user_dynamical_scale(P)
c       allow to define your own dynamical scale, need to set dynamical_scale_choice to 0 (or 10) to use it
      implicit none
      include 'nexternal.inc'
      double precision P(0:3, nexternal)
c       Commmon to have access to all variable defined in the run_card
      include 'genps.inc'
      include 'run.inc'
c       sum of the transverse mass divide by 4
c       m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
      integer i
c      real*8 xptj,xptb,xpta,xptl,xmtc
c      real*8 xetamin,xqcut,deltaeta
c      common /to_specxpt/xptj,xptb,xpta,xptl,xmtc,xetamin,xqcut,deltaeta
      user_dynamical_scale = 0d0 
      do i=3,nexternal
          user_dynamical_scale=user_dynamical_scale+dsqrt(max(0d0,(P(0,i)+P(3,i))*(P(0,i)-P(3,i))))
      enddo
      user_dynamical_scale=user_dynamical_scale/4d0
      return
      end