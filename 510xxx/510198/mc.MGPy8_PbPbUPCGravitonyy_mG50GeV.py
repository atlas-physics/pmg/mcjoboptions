# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'Graviton production in Light by Light scattering PbPb, yy->G->yy, MG=50GeV'
evgenConfig.contact = [ "Malak Ait Tamlihat <malak.ait.tamlihat@cern.ch>" ]
evgenConfig.keywords += ['2photon','Graviton']
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

if hasattr(testSeq, "TestHepMC"): testSeq.remove(TestHepMC())

safety_factor = 1.1
nevents = int(safety_factor*runArgs.maxEvents)

# beam energy = sqrt(s_NN) / 2 * 208 (GeV)
energy = str ( runArgs.ecmEnergy / 2 * 208  )

#Defaults for run_card.dat
# General settings

extras = { 'lpp1':'2', 
           'lpp2':'2',
            'ebeam1' : energy ,  # beam 1 energy
           'ebeam2' : energy ,  # beam 2 energy
           'pdlabel' : 'edff' ,  
           'nb_proton1' : '82' ,  # 208 Pb 82
           'nb_proton2' : '82' ,  # 208 Pb 82
           'nb_neutron1' : '126' ,  # 208 Pb 82
           'nb_neutron2' : '126' ,  # 208 Pb 82
           'pta'        :'1.0',
           'ptamax'       :'-1.0',
           'etaa'      :'5.0',
           'etaamin'            :'-5.0',
           'draa'          :'0.0',
           'draamax' :'-1.0',
           'mmaa' :'2.0',
           'mmaamax'  : '-1.0',
          'use_syst'  : 'False',
           'systematics_program' : 'None',
            'nevents':str(nevents)
           }

# Import the graviton  model
process = """
import model DMspin2-full
generate a a > y2 > a a
output aay2aa
"""

# Define the process and create the run card from a template
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras);
print_cards()
    
# Set some values in the param card
#  Graviton masses
masses={'56':50.000000 #Y2
         }

# Decay Width
width = {'56':'Auto' #Y2
         }

# Coupling
couplings={'1':1.000000e+03,
'2':0.000000e+00,
'3':1.000000e+00,
'4':1.000000e+00,
'5':0.000000e+00,
'6':0.000000e+00,
'7':0.000000e+00,
'8':0.000000e+00,
'9':0.000000e+00}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir,params={'MASS':masses, 'DECAY':width,
'DMINPUTS':couplings})
# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3.0)
