evgenConfig.process     = "boosted ggH H"
evgenConfig.description = "aMcAtNlo, H+jet production with LHE H pT>250, mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = ["dylan.sheldon.rankin@cern.ch"]
evgenConfig.generators  = ["aMcAtNlo"]

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

gridpack_mode=True

if not is_gen_from_gridpack():

    process="""
    import model HC_NLO_X0_UFO-heft
    define p = g u c b d s u~ c~ d~ s~ b~
    define j = g u c b d s u~ c~ d~ s~ b~
    generate p p > x0 / t [QCD] @0
    add process p p > x0 j / t [QCD] @1
    add process p p > x0 j j / t [QCD] @2
    output -f"""
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8',
            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

masses={'25': '1.250000e+02'}
frblock= {               'kAza'  : 0.0,
                            'kAgg'  : 0.0,
                            'kAaa'  : 0.0,
                            'cosa'  : 1.0,
                            'kHdwR' : 0.0,
                            'kHaa'  : 0.0,
                            'kAll'  : 0.0,
                            'kHll'  : 0.0,
                            'kAzz'  : 0.0,
                            'kSM'   : 1.0,
                            'kHdwI' : 0.0,
                            'kHdz'  : 0.0,
                            'kAww'  : 1.0,
                            'kHgg'  : 1.0,
                            'kHda'  : 0.0,
                            'kHza'  : 0.0,
                            'kHww'  : 1.0,
                            'kHzz'  : 0.0,
                            'Lambda': 1000.0
}

params={}
params['MASS']=masses
params['frblock']=frblock
modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  
