# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Some includes that are necessary to interface MadGraph with Pythia
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

# Masses of the relevant particles (Heavy neutrinos are degenerate in mass)
MWR = 4000.
MN1 = MN2 = MN3 = 25.
MDD = 65.

# Number of events to produce
safety = 1.3  # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety

# Make sure DCH PDG IDs are known to TestHepMC:
with open('pdgid_extras.txt', 'w+') as pdgfile:
    pdgfile.write('''
    45
    60
    62
    64
    ''')

# Import the LRSM NLO UFO model and define the process
process = '''
import model mlrsm-nu-loop
define n = n1 n2 n3
generate g g > n n [noborn=QCD]
output -f
'''

# Set some values in the run card
# Define the process and create the run card from a template
process_dir = new_process(process)
settings = {
    'ickkw': 0,
    'nevents': nevents,
    'time_of_flight': 1e-25
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Set BSM particle masses in the param_card
masses = {
    'MN1': MN1,
    'MN2': MN2,
    'MN3': MN3,
    'MWR': MWR,
    'MDD': MDD
}

# BSM particle width set to auto
decays = {
    'WN1': 'auto',
    'WN2': 'auto',
    'WN3': 'auto',
    'WDD': 'auto',
    'WH': 'auto'
}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir, params={
                  'MASS': masses, 'DECAY': decays})

# Do the event generation
generate(process_dir=process_dir, runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Process g g > n n'
evgenConfig.contact = ['Blaž Leban <blaz.leban@cern.ch>']
evgenConfig.keywords += ['BSM', 'exotic']

arrange_output(process_dir=process_dir, runArgs=runArgs)

testSeq.TestHepMC.MaxTransVtxDisp = 100000000  # in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000  # in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000  # in MeV
