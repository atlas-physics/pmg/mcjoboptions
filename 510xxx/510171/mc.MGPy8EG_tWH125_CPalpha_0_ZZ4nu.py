#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "MG5_aMC@NLO+Pythia8 tWH, H->inv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'philipp.mogg@cern.ch']
evgenConfig.process = "tWH125, H->inv, W->all, t->all"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 3
evgenConfig.generators       = [ "aMcAtNlo", "Pythia8", "EvtGen" ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# H->ZZ->4v decay
#--------------------------------------------------------------

genSeq.Pythia8.Commands += ['25:onMode = off', # decay of Higgs
                          '25:onIfMatch = 23 23',
                          '23:onMode = off',
                          '23:mMin = 2.0',
                          '23:onIfAny = 12 14 16']

#--------------------------------------------------------------
# Missing Et filter and not 2 leptons
#--------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
include('GeneratorFilters/MultiElecMuTauFilter.py')
filtSeq.MultiElecMuTauFilter.MinPt = 18000
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
filtSeq.Expression = "MissingEtFilter or MultiElecMuTauFilter"
