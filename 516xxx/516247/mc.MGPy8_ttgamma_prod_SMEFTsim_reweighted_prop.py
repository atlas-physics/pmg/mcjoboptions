selected_operators = ['ctWRe','cHQ3']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ a QCD=2 QED=3 NPprop=2, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > uc~ ds NP=0) @0 NP=0 SMHLOOP=0\n' # semi-leptonic +
process_definition+= 'add process p p > t t~ a QCD=2 QED=3 NPprop=2, (t > w+ b NP=0, w+ > uc ds~ NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @1 NP=0 SMHLOOP=0\n' # semi-leptonic -
process_definition+= 'add process p p > t t~ a QCD=2 QED=3 NPprop=2, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @2 NP=0 SMHLOOP=0' # di-leptonic

fixed_scale = 431.25 # ~ 2.5*m(top)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+photon, production mode, top model, inclusive, reweighted, no EFT vertices, propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_ttgamma_prod_prop_reweighted.py")
