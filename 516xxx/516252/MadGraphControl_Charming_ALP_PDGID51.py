from MadGraphControl.MadGraphUtils import *
import sys

mode=0

# merging parameters
bwcutoff=15
dparameter=0.4
#nJetMax=1
maxjetflavor=6 # was 4
ptj=0

ickkw_value=0
scalefact=1.0
alpsfact=1


if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")

param_card_extras = {
      "ALPPARS": { 'fa':fa, 'cuR11':cuR11, 'cuR12':cuR12, 'cuR13':cuR13, 'cuR21':cuR21, 'cuR22':cuR22, 'cuR23':cuR23, 'cuR31':cuR31, 'cuR32':cuR32, 'cuR33':cuR33},
      "MASS": { 'Malp':Ma }
      }

process= """  
import model sm
import model ./Charming_ALP_UFO
define p = g u c d s u~ c~ d~ s~
define j = u c d s b u~ c~ d~ s~ b~
define l- = e- mu-                                                               
define l+ = e+ mu+                                                               
define vl = ve vm
define vl~ = ve~ vm~
define dj = g c
define dj~ = g c~
define q = u c
define q~ = u~ c~
generate p p > t t~, (t > W+ b, W+ > l+ vl), (t~ > ax q~, ax > dj dj~)
add process p p > t t~, (t > ax q, ax > dj dj~), (t~ > W- b~, W- > l- vl~)
output -f
launch
set Malp """+str(Ma)+"""
set cuR11 """+str(cuR11)+"""
set cuR12 """+str(cuR12)+"""
set cuR13 """+str(cuR13)+"""
set cuR21 """+str(cuR21)+"""
set cuR22 """+str(cuR22)+"""
set cuR23 """+str(cuR23)+"""
set cuR31 """+str(cuR31)+"""
set cuR32 """+str(cuR32)+"""
set cuR33 """+str(cuR33)+"""
set decay 9000005 auto
"""
Runname='run_01'     

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = int(nevents)

#process_dir = new_process()
process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'  :'3.0',
          'pdlabel'      :"'nn23lo1'",
          'lhaid'        :247000,
          'ickkw'        :ickkw_value, 
          'maxjetflavor' :maxjetflavor,'asrwgtflavor' :maxjetflavor,
          #'ptj'          :ptj, #'drjj'         :0.0,'etaj'         :5,'etab'         :5,
          'ptj':'0',
          'ptb':'0',
          'pta':'0',
          'ptl':'0',
          'etaj':'-1',
          'etab':'-1',
          'etaa':'-1',
          'etal':'-1',
          'drjj':'0',
          'drbb':'0',
          'drll':'0',
          'draa':'0',
          'drbj':'0',
          'draj':'0',
          'drjl':'0',
          'drab':'0',
          'drbl':'0',
          'dral':'0',
          'ktdurham'     :ktdurham,
          #'dparameter'   :'0.0', #dparameter,
          'bwcutoff'     :bwcutoff,
          'scalefact'    :scalefact,
          'pt_min_pdg'   :pt_min_pdg,
          'alpsfact'     :alpsfact,
          'nevents'      :nevents ,
          'time_of_flight': '1e-2'} 


print "MAX EVENTS: " + str(runArgs.maxEvents)

modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
modify_param_card(process_dir=process_dir,params=param_card_extras)

generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)

# replacing PDGID of ALP manually (so that Pythia recognises it)
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+Runname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+Runname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+Runname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 9000005:
            part1 = "       %d" % (51)
            part2 = line[9:]
            newlhe.write(part1+part2)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
    
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+Runname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+Runname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+Runname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+Runname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs, process_dir=process_dir, saveProcDir=False)  

############################

evgenConfig.description = 'ALP linear with vector V'
evgenConfig.keywords+=['ttbar','exotic','BSM']
evgenConfig.contact = ["Jackson Burzynski <jackson.carl.burzynski@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

PYTHIA8_Dparameter=0#dparameter
PYTHIA8_Process='pp>axZ' #TODO: change this
PYTHIA8_TMS=ktdurham
#PYTHIA8_nQuarksMerge=maxjetflavor

##Check which pdgid corresponds now to DM ax -> didnt do anything so changed to 51
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('9000005\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

# Turn off checks for displaced vertices.
# Other checks are fine.
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay 
testSeq.TestHepMC.MaxVtxDisp = 100000000 #In mm 
testSeq.TestHepMC.MaxTransVtxDisp = 100000000

evgenConfig.inputconfcheck=""

