selected_operators = ['cHbox','cHG','cHW','ctHRe','ctGRe','ctWRe','ctBRe','cHQ1','cHQ3','cHt','cHl311','cll1221','ctGIm','ctWIm','ctBIm','ctHIm']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ w+ j $$ a z h w+ w- QCD=1 QED=4 NP=1 NPcQj11=0 NPcQj18=0 NPctj1=0 NPctj8=0 NPcQu1=0 NPcQu8=0 NPctu1=0 NPctu8=0 NPcQj31=0 NPcQj38=0 NPctd1=0 NPctd8=0 NPcQd1=0 NPcQd8=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > uc~ ds NP=0), (w+ > l+ vl NP=0) @0 NPprop=0 SMHLOOP=0\n' # dileptonic ++
process_definition+= 'add process p p > t t~ w- j $$ a z h w+ w- QCD=1 QED=4 NP=1 NPcQj11=0 NPcQj18=0 NPctj1=0 NPctj8=0 NPcQu1=0 NPcQu8=0 NPctu1=0 NPctu8=0 NPcQj31=0 NPcQj38=0 NPctd1=0 NPctd8=0 NPcQd1=0 NPcQd8=0, (t > w+ b NP=0, w+ > uc ds~ NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0), (w- > l- vl~ NP=0) @1 NPprop=0 SMHLOOP=0\n' # dileptonic --
process_definition+= 'add process p p > t t~ w+ j $$ a z h w+ w- QCD=1 QED=4 NP=1 NPcQj11=0 NPcQj18=0 NPctj1=0 NPctj8=0 NPcQu1=0 NPcQu8=0 NPctu1=0 NPctu8=0 NPcQj31=0 NPcQj38=0 NPctd1=0 NPctd8=0 NPcQd1=0 NPcQd8=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0), (w+ > l+ vl NP=0) @2 NPprop=0 SMHLOOP=0\n' # trileptonic +
process_definition+= 'add process p p > t t~ w- j $$ a z h w+ w- QCD=1 QED=4 NP=1 NPcQj11=0 NPcQj18=0 NPctj1=0 NPctj8=0 NPcQu1=0 NPcQu8=0 NPctu1=0 NPctu8=0 NPcQj31=0 NPcQj38=0 NPctd1=0 NPctd8=0 NPcQd1=0 NPcQd8=0, (t > w+ b NP=0, w+ > l+ vl NP=0), (t~ > w- b~ NP=0, w- > l- vl~ NP=0), (w- > l- vl~ NP=0) @3 NPprop=0 SMHLOOP=0' # trileptonic -

fixed_scale = 425.0 # ~ 2*m(top)+m(W)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+W EWK, top model, multilepton (2LSS/3L), reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_ttwj_ewk_reweighted.py")
