from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*10. if runArgs.maxEvents>0 else 10.*evgenConfig.nEventsPerJob

testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

mode = 0
generation = 3


# write extra pdgids for pythia
pdgids = \
"""
9000005
-9000005
9000006
-9000006
9000007
-9000007
"""

f = open("pdgid_extras.txt", 'w')
f.write(pdgids)
f.close()

process_def = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model vector_LQ_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > ta- ta+ QED<=3 NP<=0 @0
add process p p > ta- ta+ j QED<=3 NP<=0 @1
add process p p > ta- ta+ j j QED<=3 NP<=0 @2
output -f
"""

process_dir = new_process(process_def)

lhaid=260000
pdflabel='lhapdf'

settings = { 'lhe_version' : '3.0', 
             'cut_decays'  : 'F', 
             'lhaid'       : lhaid,
             'pdlabel'     : "'"+pdflabel+"'",
             'ickkw'       : 0,
             'ptj'         : 20,
             'ptb'         : 20,
             'xqcut'       : 0.,
             'drjj'        : 0.0,
             'mmll'        : 120.,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'use_syst'    : 'False',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params = \
  { "npgpcoup" : { "1" : 0.0, "2" : 0.0, "3" : 0.0, "4" : 0.0, "5" : 0.0    },
    "npzpcoup" : { "1" : 0.0, "2" : 0.0, "3" : 0.0, "4" : 0.0, "5" : 0.0, "6" : 0.0   },
    "nplqcoup" : { "1" : 0.0, "2" : 0.0, "3" : 0.0, "4" : 0.0, "5" : 0.0    }
  }

modify_param_card(process_dir=process_dir,params=params)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""change process p p > ta- ta+ QED<=3 NP<=2 NP^2==4 
change process p p > ta- ta+ j QED<=3 NP<=2 NP^2==4 --add 
change process p p > ta- ta+ j j QED<=3 NP<=2 NP^2==4 --add
launch --rwgt_name=betaL33_1p5TeV
    set mass 9000007 1.500000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 1.
launch --rwgt_name=betaL33_2TeV
    set mass 9000007 2.000000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 1.
launch --rwgt_name=betaL33_2p5TeV
    set mass 9000007 2.500000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 1.
launch --rwgt_name=betaL33_3TeV
    set mass 9000007 3.000000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 1.
launch --rwgt_name=betaRd33_3TeV
    set mass 9000007 3.000000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 0.
    set nplqcoup 3 1.
launch --rwgt_name=betaLmRd33_3TeV
    set mass 9000007 3.000000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 1.
    set nplqcoup 3 -1.
launch --rwgt_name=betaL23_3TeV
    set mass 9000007 3.000000e+03 
    set nplqcoup 1 1.
    set nplqcoup 2 0.
    set nplqcoup 3 0.
    set nplqcoup 4 1.
launch --rwgt_name=zp33LL_3TeV
    set mass 9000006 3.000000e+03 
    set nplqcoup 1 0.
    set nplqcoup 2 0.
    set nplqcoup 3 0.
    set nplqcoup 4 0.
    set nplqcoup 5 0.
    set npzpcoup 1 1.
    set npzpcoup 2 1.
    set npzpcoup 3 1.
launch --rwgt_name=zp33RdRe_3TeV
    set mass 9000006 3.000000e+03 
    set nplqcoup 1 0.
    set nplqcoup 2 0.
    set nplqcoup 3 0.
    set nplqcoup 4 0.
    set nplqcoup 5 0.
    set npzpcoup 1 1.
    set npzpcoup 2 0.
    set npzpcoup 3 0.
    set npzpcoup 5 1.
    set npzpcoup 6 1.""")
reweight_card_f.close()

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True only for local testing!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_LQtchannel'
# evgenConfig.keywords+=["VLQ", 'tau']
evgenConfig.contact  = [ "chays@cern.ch" ]

process="pp>ta+ta-"
nJetMax=2

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Process=process
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# event filter
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,0.4)

from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
filtSeq += MultiBjetFilter()

filtSeq.MultiBjetFilter.NBJetsMin = 1
filtSeq.MultiBjetFilter.InclusiveEfficiency = 0.15

filtSeq.Expression = "MultiBjetFilter"

