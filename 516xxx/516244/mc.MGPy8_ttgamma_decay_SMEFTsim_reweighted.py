selected_operators = ['ctGRe','ctWRe','ctBRe','cHQ3','cQj11','cQj18','cQj31','cQj38','cQQ1','cQQ8','ctu1','ctu8','ctd1','ctd8','cQu1','cQu8','ctj1','ctj8','cQt1','cQt8','cQd1','cQd8','ctGIm','ctWIm','ctBIm']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b a), (t~ > ds uc~ b~) @0 NPprop=0 SMHLOOP=0 NP=1\n' # semi-leptonic +, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b), (t~ > ds uc~ b~ a) @1 NPprop=0 SMHLOOP=0 NP=1\n' # semi-leptonic +, photon in tbar decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > uc ds~ b a), (t~ > l- vl~ b~) @2 NPprop=0 SMHLOOP=0 NP=1\n' # semi-leptonic -, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > uc ds~ b), (t~ > l- vl~ b~ a) @3 NPprop=0 SMHLOOP=0 NP=1\n' # semi-leptonic -, photon in tbar decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b a), (t~ > l- vl~ b~) @4 NPprop=0 SMHLOOP=0 NP=1\n' # di-leptonic, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b), (t~ > l- vl~ b~ a) @5 NPprop=0 SMHLOOP=0 NP=1\n' # di-leptonic, photon in tbar decay

fixed_scale = 431.25 # ~ 2.5*m(top)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+photon, decay mode, top model, inclusive, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=5000

include("Common_SMEFTsim_topmW_ttgamma_decay_reweighted.py")
