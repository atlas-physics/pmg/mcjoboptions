# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *
from MadGraphControl.MadGraphParamHelpers import *
import math

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# Parameters of the job, parsed from joboption name
H_mass = 1000. # GeV
H_width = 30. #GeV
channel = "hadhad"

#parse channel and mass from joboption name: jofile
if "tautauhh" in jofile:
    channel = "hadhad"
elif "tautaulh" in jofile:
    channel = "lephad"
else:
    print(jofile)
    raise Exception("Unable to parse subchannel from joboption name.", jofile)
print("Channel set to be",channel)

if not "_" in jofile:
    raise Exception("Unexpected joboption name.",jofile)

for item in jofile.split("_"):
    if "bbH" in item:
        H_mass = int(item.replace("bbH",""))
        print("Higgs mass to be set to", H_mass)

# Due to the low filter efficiency, the number of generated events are set to safety times maxEvents,
# to avoid crashing due to not having enough events
safety = 10.0 
# factor of events more needed as LHE because of channel filter (+5 sigma safety):
calc_factor = lambda n, br : (5*math.sqrt(n)+n)/(n*br)

if channel == "hadhad":
    safety = calc_factor(runArgs.maxEvents, 0.41)
elif channel == "lephad":
    safety = calc_factor(runArgs.maxEvents, 0.45)

nevents = int(runArgs.maxEvents * safety)

#---------------------------------------------------------------------------------------------------
# Generating bbH 4FS process with H from bbbar fusion with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
generate p p > b b~ h [QCD]
output -f"""

# Define the process and create the run card from a template
process_dir = new_process(process)


#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
extras = {'nevents':nevents,
          'parton_shower' : 'PYTHIA8'}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Higgs mass is set to custom value
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['MASS']={'25':str(H_mass)}  

#set_higgs_params(process_dir)
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------  
generate(process_dir=process_dir, runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3,saveProcDir=False)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "TimeShower:weightGluonToQuark=1.",
                            "Check:epTolErr = 1e-2",
                            '25:onMode = off',
                            '25:onIfMatch = 15 15']

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended   

filtSeq += XtoVVDecayFilterExtended("XtoVVDecayFilterExtended")

filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2

if channel == "lephad":
    filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
    filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]  #pi0, K0L, pi+, eta, omega, K0S, K0, K+, K*+
elif channel == "hadhad":
    filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
    filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]

filtSeq.Expression = "(XtoVVDecayFilterExtended)"
