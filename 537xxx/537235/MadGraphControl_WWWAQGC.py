import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["SM", "AQGC"]
evgenConfig.contact = ["Tanvi Wamorkar <tanvi.wamorkar@cern.ch>"]
evgenConfig.description = "MadGraph+Pythia8 for WWW aQGC with param {}^2=={}".format(EFT_param, EFT_decomposition)

# Process card
process = "import model QAll_5_Aug21v2\n"
process += "define l+ = e+ mu+ ta+\n"
process += "define l- = e- mu- ta-\n"
process += "define v = ve vm vt\n"
process += "define v~ = ve~ vm~ vt~\n"
process += "define p = g u c d s u~ c~ d~ s~ b b~\n"
process += "define j = g u c d s b u~ c~ d~ s~ b~\n"

if Final_State == "3l":
    process_term = "generate p p  > w+ w+ w- > l+ v l+ v l- v~ QCD=0"
    process_term_conj = "add process p p  > w- w- w+ > l- v~ l- v~ l+ v QCD=0"
elif Final_State == "2l":
    process_term = "generate p p  > w+ w+ w- > l+ v l+ v j j QCD=0"
    process_term_conj = "add process p p  > w- w- w+ > l- v~ l- v~ j j QCD=0"     

if EFT_decomposition !=0 :
    process += "{} {}^2=={} \n ".format(process_term, EFT_param, EFT_decomposition)
else: process += "{} S0=0 S1=0 S2=0 M0=0 M1=0 M2=0 M3=0 M4=0 M5=0 M7=0 T0=0 T1=0 T2=0 T3=0 T4=0 T5=0 T6=0 T7=0 T8=0 T9=0 \n".format(process_term)    

process += "\n"
if EFT_decomposition !=0 :
    process += "{} {}^2=={} \n".format(process_term_conj, EFT_param, EFT_decomposition)
else: process += "{} S0=0 S1=0 S2=0 M0=0 M1=0 M2=0 M3=0 M4=0 M5=0 M7=0 T0=0 T1=0 T2=0 T3=0 T4=0 T5=0 T6=0 T7=0 T8=0 T9=0 \n".format(process_term_conj)   

process += "\noutput -f"
process_dir = new_process(process)

# Run card
#evgenConfig.nEventsPerJob=10000         
run_settings = {
    "nevents": runArgs.maxEvents*1.2 if runArgs.maxEvents > 0 else 1.2*evgenConfig.nEventsPerJob,
    "dynamical_scale_choice": 3,
    'hard_survey' : '1',
    'ptl' : '15.0'

    }
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_settings)

# Param card
param_settings = {"ANOINPUTS": {
    "1": "1e-20 # S0",
    "2": "1e-20 # S1",
    "3": "1e-20 # S2",
    "4": "1e-20 # M0",
    "5": "1e-20 # M1",
    "6": "1e-20 # M2",
    "7": "1e-20 # M3",
    "8": "1e-20 # M4",
    "9": "1e-20 # M5",
    "10": "1e-20 # M6",
    "11": "1e-20 # M7",
    "12": "1e-20 # T0",
    "13": "1e-20 # T1",
    "14": "1e-20 # T2",
    "15": "1e-20 # T3",
    "16": "1e-20 # T4",
    "17": "1e-20 # T5",
    "18": "1e-20 # T6",
    "19": "1e-20 # T7",
    "20": "1e-20 # T8",
    "21": "1e-20 # T9"}
    }

if EFT_decomposition != 0:
    for i, j in param_settings["ANOINPUTS"].items():
        if EFT_param in j:
            param_settings["ANOINPUTS"][i] = param_settings["ANOINPUTS"][i].replace("1e-20", "{}".format(operator_value))
modify_param_card(process_dir=process_dir, params=param_settings)

# Generate LHE file OTF
generate(process_dir=process_dir, runArgs=runArgs)#, grid_pack=True)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=False)

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
check_reset_proc_number(opts) # Force serial processing
include("Pythia8_i/Pythia8_MadGraph.py")


    

# from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
# filtSeq += MultiLeptonFilter()
# filtSeq.MultiLeptonFilter.Ptcut = 15000. #(MeV)
#filtSeq.MultiLeptonFilter.Etacut =  2.5
