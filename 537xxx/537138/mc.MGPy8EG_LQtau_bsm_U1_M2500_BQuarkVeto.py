from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
import re
import os
import subprocess

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

evgenConfig.nEventsPerJob = 1000
nevents = runArgs.maxEvents*25.0 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

job_option_name = get_physics_short()
print(job_option_name)

# Get LQ mass from jo file
matches = re.search("M([0-9]+).*", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))

# Merging settings
process_name = "pp>LEPTONS,NEUTRINOS"
maxjetflavor=5
ickkw=0
xqcut=0.0
nJetMax=2
ktdurham=15.0
dparameter=0.4

# dynamical_scale_choice
dynamical_scale_choice=3
# -1 : MadGraph5_aMC@NLO default (different for LO/NLO/ ickkw mode) same as previous version.
#  0 : Tag reserved for user define dynamical scale (need to be added in setscales.f).
#  1 : Total transverse energy of the event.
#  2 : sum of the transverse mass
#  3 : sum of the transverse mass divide by 2
#  4 : \sqrt(s), partonic energy (only for LO run)

#
ptcut=15.0

# minimum missing Et (sum of neutrino's momenta) (if not, set to 0.)
misset=0.

# maximum missing Et (sum of neutrino's momenta) (if not, set to -1.)
missetmax=-1.

# Kappa : for Yangs-Mills kappa=0 ; Minimal coupling kappa=1
lkappa=0.0

# Kappa tilde
lkappatilde=0.0

# gU : overall coupling strength
lgu=1.

# betaL32
betaL32=0.0

# betaR33
betaR33=0.0

# betaL23
betaL23=0.2

# betaL33
betaL33=1.0

#
process_def="""
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model ./vector_LQ_UFO_CKM
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
generate p p > ta- vt~ {npOrder}
add process p p > ta+ vt {npOrder}
add process p p > ta- vt~ j {npOrder}
add process p p > ta+ vt j {npOrder}
add process p p > ta- vt~ j j {npOrder}
add process p p > ta+ vt j j {npOrder}
output -f
"""

#
process_def_run=process_def

#
process_def_SM       = process_def_run.replace("{npOrder}", "NP==0")
process_def_allNonSM = process_def_run.replace("{npOrder}", "NP^2>0")
process_def_interfer = process_def_run.replace("{npOrder}", "NP^2==2")
process_def_BSM      = process_def_run.replace("{npOrder}", "NP==2")
process_def_all      = process_def_run.replace("{npOrder}", "")

#
process_ME_type = ""

# Get beam energy
beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

if "inf" in job_option_name :
    process_dir = new_process(process_def_interfer)
    process_ME_type = "only interference"
elif "bsm" in job_option_name :
    process_dir = new_process(process_def_BSM)
    process_ME_type = "only BSM (no interference)"
elif "bi" in job_option_name :
    process_dir = new_process(process_def_allNonSM)
    process_ME_type = "BSM+interference"
elif "sm" in job_option_name : # must be after "bsm"
    process_dir = new_process(process_def_SM)
    process_ME_type = "SM"
else:
    process_dir = new_process(process_def_all)
    process_ME_type = "SM+BSM+interference"

#
extras = {
    'dynamical_scale_choice': int(dynamical_scale_choice),
    'lhe_version': '3.0',
    'sde_strategy': 1,
    'ihtmin': 0.0,
    'misset': misset,
    'missetmax': missetmax,
    'cut_decays': 'F',
    'nevents': int(nevents),
    'ickkw': int(ickkw),
    'xqcut': xqcut,
    'asrwgtflavor': int(maxjetflavor),
    'maxjetflavor': int(maxjetflavor),
    'ktdurham': ktdurham,
    'dparameter': dparameter,
    'ptj': ptcut,
    'ptb': ptcut,
    'etab': '5.0',
    'etaa': '5.0',
    'etal': '5.0',
    'drjj': '0.0',
    'drll': '0.0',
    'draa': '0.0',
    'draj': '0.0',
    'drjl': '0.0',
    'dral': '0.0',
}

#----------------------------------------------------------------------------------------
# Setting model parameters
#----------------------------------------------------------------------------------------
#---Define model parameter: - for LQ production no couplings to Z_prime and heavy gluon
#                           - beta23 and beta33 taken from job_option_name (encoded cf table)
vLQ_par={
    'gu'         : '{:e}'.format(lgu),
    'betal33'    : '{:e}'.format(betaL33),
    'betard33'   : '{:e}'.format(betaR33),
    'betal23'    : '{:e}'.format(betaL23),
    'betal32'    : '{:e}'.format(betaL32),
    'kappau'     : '{:e}'.format(lkappa),
    'kappautilde': '{:e}'.format(lkappatilde),
    'betalql'    : '{:e}'.format(0.0),
    'betarql'    : '{:e}'.format(0.0),
    'betal3l'    : '{:e}'.format(0.0),
    'betalq3'    : '{:e}'.format(0.0),
    'betal2l'    : '{:e}'.format(0.0),
    'betalq2'    : '{:e}'.format(0.0),
}

vLQ_Mass={
    '42' : '{:e}'.format(lqmass),
    '43' : '{:e}'.format(1e9),
    '44' : '{:e}'.format(1e9),
    '61' : '{:e}'.format(1e9),
    '62' : '{:e}'.format(1e9),
    '71' : '{:e}'.format(1e9),
    '72' : '{:e}'.format(1e9),
}

vLQ_Width={
    '42' : 'Auto',
}

zP_par={
    'gzp'      : '{:e}'.format(0.0),
    'zetaq33'  : '{:e}'.format(0.0),
    'zetal33'  : '{:e}'.format(0.0),
    'zetaru33' : '{:e}'.format(0.0),
    'zetard33' : '{:e}'.format(0.0),
    'zetare33' : '{:e}'.format(0.0),
    'zetaqll'  : '{:e}'.format(0.0),
    'zetal22'  : '{:e}'.format(0.0),
    'zetal23'  : '{:e}'.format(0.0),
    'zetarull' : '{:e}'.format(0.0),
    'zetardll' : '{:e}'.format(0.0),
    'zetare22' : '{:e}'.format(0.0),
    'zetalqq'  : '{:e}'.format(0.0),
    'zetarqq'  : '{:e}'.format(0.0),
    'zetalq2'  : '{:e}'.format(0.0),
    'zetalll'  : '{:e}'.format(0.0),
    'zetarll'  : '{:e}'.format(0.0),
    'zetall2'  : '{:e}'.format(0.0),
}

gP_par={
    'ggp'       : '{:e}'.format(0.0),
    'kappaq33'  : '{:e}'.format(0.0),
    'kapparu33' : '{:e}'.format(0.0),
    'kappard33' : '{:e}'.format(0.0),
    'kappaqll'  : '{:e}'.format(0.0),
    'kapparull' : '{:e}'.format(0.0),
    'kappardll' : '{:e}'.format(0.0),
    'kappag1'   : '{:e}'.format(0.0),
    'kappag2'   : '{:e}'.format(0.0),
    'kappalqq'  : '{:e}'.format(0.0),
    'kapparqq'  : '{:e}'.format(0.0),
    'kappalq2'  : '{:e}'.format(0.0),
}

# UTfit: Rend.Lincei Sci.Fis.Nat. 34 (2023) 37-57, arXiv:2212.03894
CKM_par={
    'CKM11'    : '{:e}'.format(0.97431),
    'CKM12'    : '{:e}'.format(0.22517),
    'CKM13'    : '{:e}'.format(0.003715),
    'CKM21'    : '{:e}'.format(-0.22503),
    'CKM22'    : '{:e}'.format(0.97345),
    'CKM23'    : '{:e}'.format(0.0420),
    'CKM31'    : '{:e}'.format(0.00859),
    'CKM32'    : '{:e}'.format(-0.04128),
    'CKM33'    : '{:e}'.format(0.999111),
    'CKM13ang' : '{:e}'.format(-65.1),
    'CKM21ang' : '{:e}'.format(0.0351),
    'CKM22ang' : '{:e}'.format(-0.00187),
    'CKM31ang' : '{:e}'.format(-22.4),
    'CKM32ang' : '{:e}'.format(1.05),
}

params = {}

params['NPLQCOUP'] = vLQ_par
params['MASS'] = vLQ_Mass
params['DECAY'] = vLQ_Width
params['NPGPCOUP'] = gP_par
params['NPZPCOUP'] = zP_par
params['CKMBLOCK'] = CKM_par

modify_param_card(process_dir=process_dir, params=params)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

pdgfile = open("pdgid_extras.txt" , "w+")
pdgfile.write("""42
-42
43
-43
44
-44
61
-61
62
-62
71
-71
72
-72
""")
pdgfile.close()

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""
use_eventid True
""")
scan_Ms=[1500, 2000, 2500, 3000, 4000, 5000]
scan_gUs=[0.5, 1.0, 1.5, 2.5, 3.5]
scan_b23s=[0.0, 0.2, 0.6, 1.0, 1.4]
for scan_M in scan_Ms:
    for scan_gU in scan_gUs:
        for scan_b23 in scan_b23s:
            reweight_card_f.write("""
            launch --rwgt_name=MU{:s}_gU{:s}_23L{:s}
                set mass 42 {:e}
                set NPLQCOUP 1 {:e}
                set NPLQCOUP 4 {:e}
            """.format(str(scan_M),str(scan_gU).replace(".", "_"),str(scan_b23).replace(".", "_" ),scan_M,scan_gU,scan_b23))
#
reweight_card_f.write("""
launch --rwgt_name=33Rdp1_0
    set NPLQCOUP 3 1.
launch --rwgt_name=33Rdm1_0
    set NPLQCOUP 3 -1.
""")
#
reweight_card_f.write("""
launch --rwgt_name=kappa1_0
    set NPLQCOUP 6 1.0
    set NPLQCOUP 7 0.0
launch --rwgt_name=twokappa1_0
    set NPLQCOUP 6 1.0
    set NPLQCOUP 7 1.0
""")
#
reweight_card_f.write("""
launch --rwgt_name=MU10000
    set mass 42 10000.
launch --rwgt_name=MU15000
    set mass 42 15000.
launch --rwgt_name=MU20000
    set mass 42 20000.
""")
reweight_card_f.close()

#
print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs, lhe_version=3, saveProcDir=True)

#----------------------------------------------------------------------------------------
# EVGEN configuration
#----------------------------------------------------------------------------------------
evgenConfig.description = 'Vector LQ production of U1 {0:s}: tau(had)nu+0,1,2jets, mLQ={1:d}, misset={2:d}GeV, missetmax={3:d}GeV, bquark-veto'.format(process_ME_type,int(lqmass),int(misset),int(missetmax))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark', 'tau']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.contact = ['junichi.tanaka@cern.ch']

check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# CKKWL setting
PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process_name
PYTHIA8_nQuarksMerge=maxjetflavor
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on"]

genSeq.Pythia8.Commands += [ '15:onMode = on', # decay of taus
                             '15:offIfAny = 11 13',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]

# Setup HF filters
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.RequestBottom=False
HeavyFlavorBHadronFilter.RequestCharm=False
HeavyFlavorBHadronFilter.Request_cQuark=False
HeavyFlavorBHadronFilter.Request_bQuark=True
HeavyFlavorBHadronFilter.RequestSpecificPDGID=False
HeavyFlavorBHadronFilter.RequireTruthJet=False
HeavyFlavorBHadronFilter.BottomPtMin=0*GeV
HeavyFlavorBHadronFilter.BottomEtaMax=5.0
HeavyFlavorBHadronFilter.bPtMin=0*GeV
HeavyFlavorBHadronFilter.bEtaMax=5.0
filtSeq += HeavyFlavorBHadronFilter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter)"
