# Based on: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/507xxx/507449/H4l_VBFVHhad_SMEFT_AWU_CPodd_H4lFilter.py
# Tested on Release 22

from MadGraphControl.MadGraphUtils import *
import os

madgraph_models_path = "/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/"

mgmodels='/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/'
if os.path.exists('mgmodels_local'):
    shutil.rmtree('mgmodels_local')

os.mkdir( "mgmodels_local" )
sys.path.append(os.getcwd() + '/mgmodels_local')
os.environ['PYTHONPATH'] = os.environ['PYTHONPATH'] + ':' + os.getcwd() + '/mgmodels_local'

# This file is a configuration file.
# It is meant to be included in a joboption file, which should define the _PARAMS variable.
# It should be a map with keys corresponding to SMEFT couplings (cHWtil, cHBtil, cHWBtil).

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------

safe_factor = 8.0
# there is about of 10% dropped by Pythia, and event filter efficiency between 20-25%
nevents = safe_factor * (runArgs.maxEvents if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob)

# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False


#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

# fcard = open('proc_card_mg5.dat','w')


# masses = {
#         '15': '1.777000e+00',
#         '25': '1.250000e+02',
#         '24': '80.387000',
#         '9000003': '80.387000',
#         '9000004': '80.387000',
#         '251': '80.387000',
#         'DECAY  25': '4.995000e-03',
#         }
# yukawa = { '15': '1.777000e+00' }

#model="SMEFTsim_A_U35_MwScheme_UFO"
model="SMEFTsim_U35_alphaScheme_UFO"
restricted_model = "mgmodels_local/" + model + "_loc"
shutil.copytree(mgmodels+model,restricted_model)

thisDSID = os.path.basename(runArgs.jobConfig[0])

# if all(value == 0. for value in _PARAMS.values()):
#     shutil.copy(restricted_model+"/restrict_SMlimit_massless.dat", restricted_model+'/'+'restrict_masslessEFT_'+thisDSID+'.dat')
# else: 
#     modify_param_card(param_card_input=restricted_model+"/restrict_SMlimit_massless.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+thisDSID+'.dat',params={'SMEFT': _PARAMS})

_MASS = {'15': 1.777000e+00, '25': 1.250000e+02  }
_YUKAWA = {'ymtau': 1.777000e+00}
_DECAY = {'25': 'DECAY  25 4.995000e-03'} 

# modify_param_card(param_card_input=restricted_model+"/restrict_SMlimit_massless_SMEFT_H4l.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+thisDSID+'.dat',params={'FRBLOCK': _PARAMS})
modify_param_card(param_card_input=restricted_model+"/restrict_SMlimit_massless.dat",output_location=restricted_model+'/'+'restrict_masslessEFT_'+thisDSID+'.dat',params={'SMEFT': _PARAMS, 'MASS': _MASS, 'YUKAWA': _YUKAWA, 'DECAY': _DECAY})

model_loc = "./" + restricted_model
model_extension = "masslessEFT_"+thisDSID
my_process = """
import model """+model+"""_loc-"""+model_extension+"""

set gauge unitary

define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > j j h QCD=0 NP=1, h > l+ l- l+ l- NP=1

output -f
"""

if all(value == 0. for value in _PARAMS.values()):
    my_process = my_process.replace('NP=1', '')

process_dir = new_process( my_process )


#------------------------------------------------------------------------------
# creating param card, based on restricted model
#------------------------------------------------------------------------------

#template_param_card = "restrict_massless_cHWtil_cHBtil_cHWBtil_SMEFT_H4l.dat"
#modify_param_card(
    #process_dir=process_dir,
    #param_card_input = template_param_card,
    #param_card_backup = template_param_card + ".bak",
    #params = {'FRBLOCK': _PARAMS}
#)



#---------------------------------------------------------------------------------------------------
# creating run_card.dat for ggF
#---------------------------------------------------------------------------------------------------

# ktdurham cut
ktdurham = 40

extras = {
    'lhe_version'  : '3.0',
    #'pdlabel'      : "'nn23lo1'",
    #'lhaid'        : 230000,
    #'parton_shower'  :'PYTHIA8',
    'event_norm'   : 'sum',
    'cut_decays'   : 'T',
    'bwcutoff'       : '15.0',
    'pta'          : 0.,
    'ptl'          : 0.,
    'etal'         : -1.0,
    'drjj'         : 0.0,
    'draa'         : 0.0,
    'etaj'         : -1.0,
    'draj'         : 0.0,
    'drjl'         : 0.0,
    'dral'         : 0.0,
    'etaa'         : -1.0,
    'drll'         : 0.05,
    'ptj'          : 10.,
    'ptj1min'      : 0,
    'ptj1max'      : -1.0,
    'mmjj'         : 3.,
    'mmjjmax'      : -1.0,
    'nevents'      : int(nevents),
}

# from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':263000,
    'pdf_variations':[263000,260000,90400],
    'alternative_pdfs':[266000,265000,13100,25200],
    'scale_variations':[0.5,1.,2.],
}

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=extras)


#------------------------------------------------------------------------------
# run MadGraph
#------------------------------------------------------------------------------

#print_cards()

generate(process_dir=process_dir,
         runArgs=runArgs)


#-------------------------------------------------------------------------------
# Multi-core capability
#-------------------------------------------------------------------------------
check_reset_proc_number(opts)
arrange_output(process_dir=process_dir,
               runArgs=runArgs,
               lhe_version=3,
               saveProcDir=False)


evgenConfig.description = "VBF+VHhad 125 GeV Higgs production in the SMEFT model decaying to zz4l."
evgenConfig.keywords = ['Higgs', 'VBF', 'BSM', 'mH125']
evgenConfig.contact = ['Antoine Laudrain <antoine.laudrain@cern.ch>', 'Verena Walbrecht <verena.maria.walbrecht@cern.ch>', 'Jiawei Wang <jiawei.wang@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#---------------------------------------------------------------------------------------------------
# Filters
# Taken from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID346xxx/MC15.346552.MadGraphPythia8EvtGen_A14NNPDF23_3top_SM_4lFilt.py
#---------------------------------------------------------------------------------------------------

include("GeneratorFilters/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut    = 8000.
filtSeq.MultiLeptonFilter.Etacut   = 3.
filtSeq.MultiLeptonFilter.NLeptons = 3

include("GeneratorFilters/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt           = 4000.
filtSeq.FourLeptonMassFilter.MaxEta          = 4.
filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu     = False
filtSeq.FourLeptonMassFilter.AllowSameCharge = False

