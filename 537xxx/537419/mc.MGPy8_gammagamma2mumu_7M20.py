"""
Based on: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/506xxx/506938/mc.MGPy8_yymumuy_elastic.py
"""

from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':244800,
    'pdf_variations': None,
    'alternative_pdfs': None,
    'scale_variations': None,
    'alternative_dynamic_scales' : None,
}

# remove TestHepMC because this configuration is an "unconventional" usage of Madgraph
if hasattr(testSeq, "TestHepMC"): testSeq.remove(TestHepMC())

safety_factor = 1.1
nevents = int(safety_factor*runArgs.maxEvents)

# beam energy = sqrt(s_NN) / 2 * 208 (GeV)
energy = str ( runArgs.ecmEnergy / 2 * 208  )

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['SMINPUTS'] = {'aewm1':  '137.036' }  # 1/alpha_EM at q^2=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'fixed_ren_scale':'True',
           'fixed_fac_scale':'True',
           'scale': '10',
           'dsqrt_q2fact1': '10',
           'dsqrt_q2fact2': '10',
           'lpp1' : '2' ,  # photon from colliding ion for beam 1
           'lpp2' : '2' ,  # photon from colliding ion for beam 2
           'ebeam1' : energy ,  # beam 1 energy
           'ebeam2' : energy ,  # beam 2 energy
           'pdlabel1' : '"chff"' ,  # Charged Form Factor (Woods-Saxon potential) for beam 1
           'pdlabel2' : '"chff"' ,  # Charged Form Factor (Woods-Saxon potential) for beam 2
           'nb_proton1' : '82' ,  # 208 Pb 82
           'nb_proton2' : '82' ,  # 208 Pb 82
           'nb_neutron1' : '126' ,  # 208 Pb 82
           'nb_neutron2' : '126' ,  # 208 Pb 82
           'ptl' : '0.75' ,  # min pT of final state leptons
           'ptlmax': '-1.0' ,  # max pT of final state leptons (-1 = no cut)
           'etal' : '2.7' ,  # max abs(eta) of final state leptons
           'drll' : '0.0' ,  # min deltaR between final state leptons
           'mmll' : '7.0' ,  # min invariant mass between final state leptons
           'mmllmax' : '20.0' ,  # max invariant mass between final state leptons
           'nevents':str(nevents)
}

#---------------------------------------------------------------------------------------------------
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm-lepton_masses
define p = a g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate a a > mu+ mu-
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Use model sm-lepton_masses so that e and mu are not massless
#---------------------------------------------------------------------------------------------------

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------

modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=1,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "y y -> mu+ mu- Pb+Pb UPC with CHFF form factor"
evgenConfig.keywords = ["2photon", "2lepton"]
evgenConfig.contact = ['kartik.deepak.bhide@cern.ch']
evgenConfig.nEventsPerJob = 1000

from MCJobOptionUtils import LheConverterUpc as LC
lc = LC.LheConverterUpc(generator='Madgraph5', mode='Pythia8')
lc.fileName = "events.lhe"
genSeq += lc

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include('Pythia8_i/Pythia8_LHEF.py')
genSeq.Pythia8.Commands += ['SpaceShower:QEDshowerByL = 1']
