evgenConfig.description     = 'MadGraph5_aMC@NLO+Pythia8 bbH(tautau) production, mass 500 GeV hadhad channel'
evgenConfig.keywords        = ['Higgs', 'BSMHiggs', 'bbHiggs', '2tau']
evgenConfig.contact         = ["Adrian Salvador Salas <adrian.salvador.salas@cern.ch>"]
evgenConfig.generators      = ['aMcAtNlo', 'Pythia8']
evgenConfig.nEventsPerJob   = 10000

include ('aMCatNloPythia8EvtGenControl_bbHtautau.py')
