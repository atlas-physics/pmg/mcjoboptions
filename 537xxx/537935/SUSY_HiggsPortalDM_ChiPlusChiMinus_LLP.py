from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

import os

def get_lifetime(s:str) -> float:

    #string should be "0pxns" which gives a lifetime of 0.x ns
    s = s.strip("ns")
    s = s.replace("p",".")
    return float(s)

JOName = get_physics_short()
evgenLog.info("Job Option name: " + str(JOName))

jobConfigParts = JOName.split("_")

runName = "run_01"

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#Our MG5 command
process = "import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/DarkChargino \n define p = g u c d s b u~ c~ d~ s~ b~\ndefine j = g u c d s b u~ c~ d~ s~ b~\n"
process += """generate p p > x1+ x1- @1
add process p p > x1+ x1- j @2
add process p p > x1+ x1- j j @3
output -f"""

#param card stuff
mc1 = float(jobConfigParts[3])
delta_m = float(jobConfigParts[4])
mn1 = mc1 - delta_m
mn2 = mc1 - 0.16
tau = get_lifetime(jobConfigParts[5])

masses = {"1000022":mn1, "1000023":mn2,"1000024":mc1}

#set all the BSM particles stable 
decays = {"1000022":"""DECAY 1000022 0.00000000E+00""",
          "1000023":"""DECAY 1000023 0.00000000E+00""",
          "1000024":"""DECAY 1000024 0.00000000E+00"""}

#other parameters
#need to change ms, mt to be consistent with our particle masses for this to work properly
frblock = {"ms":mn1, "mt":mn2, "rr":1e-8}

#run card stuff
safety = 3 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety
ktdurham = max(min(1 / 4 * mc1, 500), 15)
settings = {"nevents": nevents, 
            "iseed": runArgs.randomSeed,
            "ptl": -1, 
            "etal": -1,
            "ickkw": 0,
            "drjj": 0.0,
            "lhe_version":"3.0",
            "cut_decays":"F",
            "maxjetflavor": 5,
            "pdgs_for_merging_cut": "1, 2, 3, 4, 5, 21",
            "asrwgtflavor": 5,
            "ktdurham": ktdurham, # set hard scale to 1/4 * chargino mass
            # we should not encounter cases where we hit those bounds, but just in case we ever need those grid points
            "ptj": min(20, ktdurham / 2 // 10 * 10), #avoid bias in merging
            "xqcut": 0, # for CKKWL merging
            "dparameter": 0.4} 

#modify param and run cards
process_dir = new_process(process)
modify_param_card(process_dir=process_dir, params={"MASS":masses, "DECAY":decays, "FRBLOCK":frblock})
modify_run_card(process_dir=process_dir, settings=settings,runArgs=runArgs)

# Do the event generation
generate(process_dir=process_dir, runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs)

#Shower with Pythia
if 'py1up' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py")
elif 'py1dw' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py")
elif 'py2up' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py")
elif 'py2dw' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py")
elif 'py3aup' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py")
elif 'py3adw' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py")
elif 'py3bup' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py")
elif 'py3bdw' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py")
elif 'py3cup' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
elif 'py3cdw' in JOName:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py")
else:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("Pythia8_i/Pythia8_MadGraph.py")
runArgs.inputGeneratorFile = outputDS
evgenConfig.description = 'MadGraph_DarkChargino'                                                                                                                                
evgenConfig.keywords += ['SUSY','BSM']
evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;AMSBC1ToEleBR=%s;AMSBC1ToMuBR=%s;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (mc1, mn1, tau, 0.5, 0.5)

#jet matching
#settings should be consistent with what is used by the slepton JO
PYTHIA8_nJetMax = 2
PYTHIA8_Process = "pp>{x1+,1000024}{x1-,-1000024}"
PYTHIA8_Dparameter = settings["dparameter"]
PYTHIA8_TMS = settings["ktdurham"]
PYTHIA8_nQuarksMerge = settings["maxjetflavor"]
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on"]

#this tells TestHepMC to leave these for Geant4
bonus_file = open('pdg_extras.dat', 'w')
bonus_file.write( '1000024 Chargino1 %s (MeV/c) fermion Chargino 1\n' % (f"{mc1 * 1000}") )
bonus_file.write( '1000022 Neutralino1 %s (MeV/c) fermion Neutralino 0\n' % (f"{mn1 * 1000}") )
bonus_file.write( '-1000024 Chargino1 %s (MeV/c) fermion Chargino -1\n' % (f"{mc1 * 1000}") )
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile = 'pdg_extras.dat'
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)
