# Job options for event generation with direct stau production.
# To be used for MC16 in r21.6.
# 
# Originally created for
#   https://its.cern.ch/jira/browse/ATLMCPROD-8490
# Based on previous version in r19:
#   https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/blob/master/common/MadGraph/MadGraphControl_SimplifiedModel_StauStau_Full.py
#
# (85)
# 

### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

### helpers
def StringToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

### parse job options name 
# N.B. 60 chars limit on PhysicsShort name...
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
additional_options = phys_short.split("_")[4:]
evgenLog.info("DirectStau: Additional options: " + str(additional_options))

# Extract job settings/masses etc.
mslep      = StringToFloat(phys_short.split('_')[2]) 
mn1        = StringToFloat(phys_short.split('_')[3])
masses['1000011'] = mslep
masses['1000013'] = mslep
masses['1000015'] = mslep
masses['2000011'] = mslep
masses['2000013'] = mslep
masses['2000015'] = mslep
masses['1000022'] = mn1

# Stau-stau + 2 partons, do stau decay in MadGraph (this is different from MC15 setup), no mixed mode
process = '''
generate    p p > ta1- ta1+    , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1
add process p p > ta1- ta1+ j  , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @2
add process p p > ta1- ta1+ j j, (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @3
add process p p > ta2- ta2+    , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @4
add process p p > ta2- ta2+ j  , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @5
add process p p > ta2- ta2+ j j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @6
'''
useguess = True

evgenLog.info('DirectStau: generation of stau-pair production; grid point decoded into m(stau) = %f GeV, m(N1) = %f GeV' % (mslep, mn1))

evgenConfig.contact = [ "mann@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'stau']
evgenConfig.description = 'Direct stau-pair production in simplified model, m_stauLR = %s GeV, m_N1 = %s GeV' % (mslep, mn1)

# Filter and event multiplier 
evt_multiplier = 2

if '2L8' in additional_options:

    # Filter that was used in MC15
    evgenLog.info('DirectStau: 2lepton8 filter is applied')

    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.NLeptons  = 2
    filtSeq.MultiElecMuTauFilter.MinPt = 8000.         # pt-cut on the lepton
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 1    # include hadronic taus

    filtSeq.Expression = "MultiElecMuTauFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier = 4

elif 'TFilt' in additional_options:
  
    # new filter for MC16
    evgenLog.info('DirectStau: Tau filter is applied.')
                  
    if not hasattr(filtSeq, "TauFilter" ):
        from GeneratorFilters.GeneratorFiltersConf import TauFilter
        filtSeq += TauFilter("TauFilter" )
 
    # pt and eta filter thresholds
    filtSeq.TauFilter.EtaMaxe   = 2.6 # 2.47
    filtSeq.TauFilter.EtaMaxmu  = 2.8 # 2.7
    filtSeq.TauFilter.EtaMaxhad = 2.6 # 2.5

    filtSeq.TauFilter.Ptcute    = 15.e3 # 18 GeV - 3*1 GeV
    filtSeq.TauFilter.Ptcutmu   = 13.e3 # 15 GeV - 4*0.4 GeV
    filtSeq.TauFilter.Ptcuthad  = 15.e3 # (20 GeV)

    filtSeq.TauFilter.UseNewOptions = False

    filtSeq.TauFilter.Ntaus = 2
    filtSeq.Expression = "TauFilter"    
        
    # set higher evt_multiplier when using filter
    evt_multiplier = 4

elif '2em8' in additional_options:

    # Filter that was used in MC15
    evgenLog.info('DirectStau: 2lepton8 filter (e/mu only) is applied')

    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.NLeptons  = 2
    filtSeq.MultiElecMuTauFilter.MinPt = 8000.         # pt-cut on the lepton
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # include hadronic taus

    filtSeq.Expression = "MultiElecMuTauFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier = 35
    if (mslep-mn1) <= 40: 
      evt_multiplier = 45
    if (mslep-mn1) <= 30: 
      evt_multiplier = 55 
    if (mslep-mn1) <= 20: 
      evt_multiplier = 100  
    if (mslep-mn1) <= 10: 
      evt_multiplier = 300

elif '1L20Filter' in additional_options:

    # Filter that was used in MC15
    evgenLog.info('DirectStau: >=1 e/mu pT>20GeV filter is applied')

    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.NLeptons  = 1
    filtSeq.MultiElecMuTauFilter.MinPt = 20000.         # pt-cut on the lepton
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8          # stay away from MS 2.7 just in case
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0    # include hadronic taus

    filtSeq.Expression = "MultiElecMuTauFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier = 20
    if (mslep-mn1) <= 25: evt_multiplier = 40 
    if (mslep-mn1) <= 10: evt_multiplier = 120
     
elif 'LepHad' in additional_options:

    evgenLog.info('DirectStau: 1emu20+1had15 filter is applied')
    
    # 1 tauHad pT>15 GeV filter
    if not hasattr(filtSeq, "TauFilter" ):
        from GeneratorFilters.GeneratorFiltersConf import TauFilter
        filtSeq += TauFilter("TauFilter" )
 
    # pt and eta filter thresholds
    filtSeq.TauFilter.EtaMaxe   = 2.6 # 2.47
    filtSeq.TauFilter.EtaMaxmu  = 2.8 # 2.7
    filtSeq.TauFilter.EtaMaxhad = 2.6 # 2.5

    filtSeq.TauFilter.Ptcute    = 20.e3 # 18 GeV - 3*1 GeV
    filtSeq.TauFilter.Ptcutmu   = 20.e3 # 15 GeV - 4*0.4 GeV
    filtSeq.TauFilter.Ptcuthad  = 15.e3 # (20 GeV)

    filtSeq.TauFilter.UseNewOptions = True
    filtSeq.TauFilter.Ntaus = 2
    filtSeq.TauFilter.Nhadtaus = 1
    filtSeq.TauFilter.Nleptaus = 1

    filtSeq.Expression = "TauFilter"
    
    # set higher evt_multiplier when using filter
    evt_multiplier = 20
    if (mslep-mn1) <= 25: evt_multiplier = 30 
    if (mslep-mn1) <= 10: evt_multiplier = 50
    
else:
  
    evgenLog.info('DirectStau: No filter is applied')

# need more at low stau masses
if mslep < 130 and '2em8' not in additional_options and '1L20Filter' not in additional_options and "LepHad" not in additional_options :
    evt_multiplier = 8 

# Configure our decays
decays['1000015'] = """DECAY   1000015     auto   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
"""
decays['2000015'] = """DECAY   2000015     auto   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_2 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     0.00000000E+00    2     1000015        25   # BR(~tau_2 -> ~tau_1 h)
     0.00000000E+00    2     1000015        35   # BR(~tau_2 -> ~tau_1 H)
     0.00000000E+00    2     1000015        36   # BR(~tau_2 -> ~tau_1 A)
     0.00000000E+00    2     1000015        23   # BR(~tau_2 -> ~tau_1 Z)
"""

# Configure stau mixing
param_blocks['selmix'] = {}
if 'mmix' in additional_options:

    # use maximally mixed staus
    evgenLog.info("DirectStau: Maxmix scenario selected.")
    param_blocks['selmix'][ '3   3' ] = '0.70710678118' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.70710678118' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '-0.70710678118' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '0.70710678118' # # RRl6x6

else:

    # No mixing
    param_blocks['selmix'][ '3   3' ] = '1.0' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.0' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '0.0' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '1.0' # # RRl6x6

if 's11' in additional_options:
  
    # only do stau1 modes
    evgenLog.info("DirectStau: Only do stau1-stau1 production modes.")
    process = '''
    generate    p p > ta1- ta1+    , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1
    add process p p > ta1- ta1+ j  , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @2
    add process p p > ta1- ta1+ j j, (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @3
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta1-,1000015}{ta1+,-1000015}"]

if 's22' in additional_options:
  
    # only do stau2 modes
    evgenLog.info("DirectStau: Only do stau2-stau2 production modes.")
    process = '''
    generate    p p > ta2- ta2+    , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @1
    add process p p > ta2- ta2+ j  , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @2
    add process p p > ta2- ta2+ j j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @3
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta2-,2000015}{ta2+,-2000015}"]

if 's12' in additional_options:

    # include mixed production modes
    evgenLog.info("DirectStau: Include mixed production modes.")
    process += '''
    add process p p > ta1- ta2+    , (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @7
    add process p p > ta1- ta2+ j  , (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @8
    add process p p > ta1- ta2+ j j, (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @9
    add process p p > ta2- ta1+    , (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @10
    add process p p > ta2- ta1+ j  , (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @11
    add process p p > ta2- ta1+ j j, (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @12
    '''

if "Dm" in additional_options:
    Dm = filter(lambda x: x.startswith("Dm"), additional_options)
    if Dm:  
        # introduce mass splitting
        deltaM = int(Dm[0][2:])
        masses['2000011'] = mslep + deltaM
        masses['2000013'] = mslep + deltaM
        masses['2000015'] = mslep + deltaM
        evgenLog.info('DirectStau: Mass of stau2 (etc.) set to %d GeV' % (masses['2000015']))

# post include
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
