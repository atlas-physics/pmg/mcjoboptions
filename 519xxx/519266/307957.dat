# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:19
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.69184855E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.39760063E+03  # scale for input parameters
    1   -4.61958911E+01  # M_1
    2   -1.21782767E+03  # M_2
    3    4.59896417E+03  # M_3
   11   -3.35944382E+03  # A_t
   12    8.31911441E+02  # A_b
   13    4.87900206E+02  # A_tau
   23   -2.68320225E+02  # mu
   25    1.61699558E+01  # tan(beta)
   26    2.01443363E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.11463785E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.66293249E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.86993346E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.39760063E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.39760063E+03  # (SUSY scale)
  1  1     8.40039013E-06   # Y_u(Q)^DRbar
  2  2     4.26739819E-03   # Y_c(Q)^DRbar
  3  3     1.01483460E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.39760063E+03  # (SUSY scale)
  1  1     2.72953418E-04   # Y_d(Q)^DRbar
  2  2     5.18611493E-03   # Y_s(Q)^DRbar
  3  3     2.70684259E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.39760063E+03  # (SUSY scale)
  1  1     4.76323674E-05   # Y_e(Q)^DRbar
  2  2     9.84886287E-03   # Y_mu(Q)^DRbar
  3  3     1.65641671E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.39760063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.35944618E+03   # A_t(Q)^DRbar
Block Ad Q=  3.39760063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.31911363E+02   # A_b(Q)^DRbar
Block Ae Q=  3.39760063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     4.87900093E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.39760063E+03  # soft SUSY breaking masses at Q
   1   -4.61958911E+01  # M_1
   2   -1.21782767E+03  # M_2
   3    4.59896417E+03  # M_3
  21    3.92786815E+06  # M^2_(H,d)
  22    1.34818851E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.11463785E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.66293249E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.86993346E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23228917E+02  # h0
        35     2.01429891E+03  # H0
        36     2.01443363E+03  # A0
        37     2.01953579E+03  # H+
   1000001     1.00850685E+04  # ~d_L
   2000001     1.00615968E+04  # ~d_R
   1000002     1.00847816E+04  # ~u_L
   2000002     1.00646203E+04  # ~u_R
   1000003     1.00850700E+04  # ~s_L
   2000003     1.00615985E+04  # ~s_R
   1000004     1.00847830E+04  # ~c_L
   2000004     1.00646215E+04  # ~c_R
   1000005     2.99861562E+03  # ~b_1
   2000005     4.21401627E+03  # ~b_2
   1000006     2.73647943E+03  # ~t_1
   2000006     4.21844575E+03  # ~t_2
   1000011     1.00206465E+04  # ~e_L-
   2000011     1.00089262E+04  # ~e_R-
   1000012     1.00198839E+04  # ~nu_eL
   1000013     1.00206508E+04  # ~mu_L-
   2000013     1.00089345E+04  # ~mu_R-
   1000014     1.00198882E+04  # ~nu_muL
   1000015     1.00112719E+04  # ~tau_1-
   2000015     1.00218748E+04  # ~tau_2-
   1000016     1.00210934E+04  # ~nu_tauL
   1000021     5.02996708E+03  # ~g
   1000022     4.43247243E+01  # ~chi_10
   1000023     2.80429772E+02  # ~chi_20
   1000025     2.83349868E+02  # ~chi_30
   1000035     1.31088442E+03  # ~chi_40
   1000024     2.77467636E+02  # ~chi_1+
   1000037     1.31040817E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.93868965E-02   # alpha
Block Hmix Q=  3.39760063E+03  # Higgs mixing parameters
   1   -2.68320225E+02  # mu
   2    1.61699558E+01  # tan[beta](Q)
   3    2.43124947E+02  # v(Q)
   4    4.05794285E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     4.68064089E-02   # Re[R_st(1,1)]
   1  2     9.98903979E-01   # Re[R_st(1,2)]
   2  1    -9.98903979E-01   # Re[R_st(2,1)]
   2  2     4.68064089E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.31723436E-03   # Re[R_sb(1,1)]
   1  2     9.99999132E-01   # Re[R_sb(1,2)]
   2  1    -9.99999132E-01   # Re[R_sb(2,1)]
   2  2    -1.31723436E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.05669940E-02   # Re[R_sta(1,1)]
   1  2     9.99176821E-01   # Re[R_sta(1,2)]
   2  1    -9.99176821E-01   # Re[R_sta(2,1)]
   2  2    -4.05669940E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.85639864E-01   # Re[N(1,1)]
   1  2    -2.83509469E-03   # Re[N(1,2)]
   1  3    -1.64962147E-01   # Re[N(1,3)]
   1  4     3.59654158E-02   # Re[N(1,4)]
   2  1     1.42386011E-01   # Re[N(2,1)]
   2  2     5.79202236E-02   # Re[N(2,2)]
   2  3     6.97063990E-01   # Re[N(2,3)]
   2  4    -7.00337965E-01   # Re[N(2,4)]
   3  1    -9.07442393E-02   # Re[N(3,1)]
   3  2     3.36859316E-02   # Re[N(3,2)]
   3  3    -6.97557965E-01   # Re[N(3,3)]
   3  4    -7.09960299E-01   # Re[N(3,4)]
   4  1     2.40124892E-03   # Re[N(4,1)]
   4  2    -9.97748700E-01   # Re[N(4,2)]
   4  3     1.73830300E-02   # Re[N(4,3)]
   4  4    -6.47270911E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.45715506E-02   # Re[U(1,1)]
   1  2    -9.99698074E-01   # Re[U(1,2)]
   2  1    -9.99698074E-01   # Re[U(2,1)]
   2  2     2.45715506E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.15681993E-02   # Re[V(1,1)]
   1  2     9.95798807E-01   # Re[V(1,2)]
   2  1     9.95798807E-01   # Re[V(2,1)]
   2  2    -9.15681993E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02870092E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.71528759E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.02436491E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     8.22201252E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40428516E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.61420282E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.62963912E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01283449E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.78680119E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06497594E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03256193E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.70794644E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.04143215E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.40215675E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.82975490E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40447887E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.61339305E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.69565984E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.35486652E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01242073E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.78628053E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06414222E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.13523811E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.99720394E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.95415117E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.88267442E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.20322958E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     8.94450003E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.64602739E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.45767156E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.36797630E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.37423691E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.87966189E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.89965154E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.26228594E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.83689866E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40432460E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.80082535E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.03239803E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.11670356E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02803713E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.25837600E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01709714E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40451830E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.79961541E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.03225610E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.11641256E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02762093E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.39502435E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01627090E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.45911956E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.47134661E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.03746076E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.91470021E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.24692983E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.79210379E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.52450206E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.20608468E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.51330612E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.02088124E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87585632E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.79388106E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.43696471E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.90063951E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.37779036E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.48009760E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.75411798E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.52461104E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.20608812E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.57081101E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.07850191E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.87562234E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.79397301E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.43717914E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.94573073E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.37767469E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.48007486E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.75399759E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.02344435E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.70632326E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.07124535E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.05321925E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.16731238E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.25669847E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.58855260E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.74991423E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.87479442E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.89424029E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.49198380E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.32517501E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.94850470E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.35185026E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     4.69620705E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.64934538E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.68795704E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.93479009E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.52144005E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.79378819E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.28444580E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.30479594E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.36504790E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.28285430E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.46855293E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.75372950E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.69628080E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.64927375E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.72581854E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.97380788E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.52129251E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.79387969E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.28441809E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.33560027E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.36493461E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.29210196E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.46853001E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.75360902E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.14799829E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.14196928E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.33234781E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38147106E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.11926199E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.70759918E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.31905409E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.63921933E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.58945108E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.56235108E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.64001298E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.12503960E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.30162904E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.80601154E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.57824457E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.44589773E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.93858709E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99904496E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.20968929E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.47935057E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.46160804E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.45045065E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42076879E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.96664187E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.56009498E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.20833811E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.42799165E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.15807758E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.84168093E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.20902472E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.80009060E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.19982074E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.31812617E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.28174434E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.28174434E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.61017827E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.71045436E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.66245824E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.32764017E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.89047434E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.68446098E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.90937159E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.90937159E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.61267404E+02   # ~g
#    BR                NDA      ID1      ID2
     2.26388301E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.26388301E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     4.06983682E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     4.06983682E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.91809468E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.91809468E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.10255905E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.10255905E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.59586195E-03   # Gamma(h0)
     2.38728217E-03   2        22        22   # BR(h0 -> photon photon)
     1.37466846E-03   2        22        23   # BR(h0 -> photon Z)
     2.38244670E-02   2        23        23   # BR(h0 -> Z Z)
     2.03931946E-01   2       -24        24   # BR(h0 -> W W)
     7.51161668E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.89278419E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.17638854E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.27509175E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.38292653E-07   2        -2         2   # BR(h0 -> Up up)
     2.68381754E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.67804191E-07   2        -1         1   # BR(h0 -> Down down)
     2.05361267E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.46061531E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     5.72911348E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.91323910E+01   # Gamma(HH)
     1.23438056E-07   2        22        22   # BR(HH -> photon photon)
     1.68607346E-07   2        22        23   # BR(HH -> photon Z)
     1.23898536E-05   2        23        23   # BR(HH -> Z Z)
     6.50167965E-06   2       -24        24   # BR(HH -> W W)
     3.41989906E-06   2        21        21   # BR(HH -> gluon gluon)
     4.31191107E-09   2       -11        11   # BR(HH -> Electron electron)
     1.91963276E-04   2       -13        13   # BR(HH -> Muon muon)
     5.54378119E-02   2       -15        15   # BR(HH -> Tau tau)
     8.73965402E-13   2        -2         2   # BR(HH -> Up up)
     1.69524614E-07   2        -4         4   # BR(HH -> Charm charm)
     1.27456443E-02   2        -6         6   # BR(HH -> Top top)
     3.34935798E-07   2        -1         1   # BR(HH -> Down down)
     1.21146053E-04   2        -3         3   # BR(HH -> Strange strange)
     3.23179588E-01   2        -5         5   # BR(HH -> Bottom bottom)
     7.24878695E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.60010769E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.60010769E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     6.38802469E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.92520144E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.97004244E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.89824132E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     6.30261623E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.77399231E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.91497237E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.97504448E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.07759217E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     7.07005982E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.89414955E+01   # Gamma(A0)
     2.98664242E-07   2        22        22   # BR(A0 -> photon photon)
     3.82737487E-07   2        22        23   # BR(A0 -> photon Z)
     3.26466502E-05   2        21        21   # BR(A0 -> gluon gluon)
     4.24642767E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.89047473E-04   2       -13        13   # BR(A0 -> Muon muon)
     5.45959303E-02   2       -15        15   # BR(A0 -> Tau tau)
     8.39996678E-13   2        -2         2   # BR(A0 -> Up up)
     1.62924754E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.26459117E-02   2        -6         6   # BR(A0 -> Top top)
     3.29845481E-07   2        -1         1   # BR(A0 -> Down down)
     1.19304974E-04   2        -3         3   # BR(A0 -> Strange strange)
     3.18271932E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     8.40111244E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.60995438E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.60995438E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     6.81440902E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.39621635E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.62460422E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.28254968E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.69994518E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.97130083E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.08406375E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.54655238E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.94187514E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.69159206E-05   2        23        25   # BR(A0 -> Z h0)
     1.22225008E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.97467176E+01   # Gamma(Hp)
     4.84105781E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.06970341E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.85429322E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     3.26889028E-07   2        -1         2   # BR(Hp -> Down up)
     5.49202403E-06   2        -3         2   # BR(Hp -> Strange up)
     3.56180424E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.39732742E-08   2        -1         4   # BR(Hp -> Down charm)
     1.17993461E-04   2        -3         4   # BR(Hp -> Strange charm)
     4.98780102E-04   2        -5         4   # BR(Hp -> Bottom charm)
     9.52671385E-07   2        -1         6   # BR(Hp -> Down top)
     2.09435798E-05   2        -3         6   # BR(Hp -> Strange top)
     3.49786968E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.13825876E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.98014833E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.90605462E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.54416827E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.00532066E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.52866294E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.56702944E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.63737859E-05   2        24        25   # BR(Hp -> W h0)
     2.71276777E-10   2        24        35   # BR(Hp -> W HH)
     2.38128303E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24583479E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.61542887E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.61467471E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00028844E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.53613196E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.82456754E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24583479E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.61542887E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.61467471E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99994347E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.65264270E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99994347E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.65264270E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26737006E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.64670574E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.01573623E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.65264270E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99994347E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.36479033E-04   # BR(b -> s gamma)
    2    1.58835043E-06   # BR(b -> s mu+ mu-)
    3    3.52487383E-05   # BR(b -> s nu nu)
    4    2.36432013E-15   # BR(Bd -> e+ e-)
    5    1.01001120E-10   # BR(Bd -> mu+ mu-)
    6    2.11464456E-08   # BR(Bd -> tau+ tau-)
    7    7.98303476E-14   # BR(Bs -> e+ e-)
    8    3.41035150E-09   # BR(Bs -> mu+ mu-)
    9    7.23460203E-07   # BR(Bs -> tau+ tau-)
   10    9.63753811E-05   # BR(B_u -> tau nu)
   11    9.95520790E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42149652E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93691924E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15801591E-03   # epsilon_K
   17    2.28166568E-15   # Delta(M_K)
   18    2.47982503E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28998883E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.53467923E-16   # Delta(g-2)_electron/2
   21    6.56123766E-12   # Delta(g-2)_muon/2
   22    1.85626100E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.53562900E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.05902643E-01   # C7
     0305 4322   00   2    -2.80457881E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.13060930E-01   # C8
     0305 6321   00   2    -3.23218874E-04   # C8'
 03051111 4133   00   0     1.62061446E+00   # C9 e+e-
 03051111 4133   00   2     1.62087997E+00   # C9 e+e-
 03051111 4233   00   2     4.63847483E-05   # C9' e+e-
 03051111 4137   00   0    -4.44330656E+00   # C10 e+e-
 03051111 4137   00   2    -4.44126491E+00   # C10 e+e-
 03051111 4237   00   2    -3.45807889E-04   # C10' e+e-
 03051313 4133   00   0     1.62061446E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62087994E+00   # C9 mu+mu-
 03051313 4233   00   2     4.63847054E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44330656E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44126494E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.45807857E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50490906E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.48780889E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50490906E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.48780976E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50490906E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.48805406E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85818160E-07   # C7
     0305 4422   00   2     2.23285174E-06   # C7
     0305 4322   00   2    -5.55044056E-07   # C7'
     0305 6421   00   0     3.30478769E-07   # C8
     0305 6421   00   2     2.77012468E-06   # C8
     0305 6321   00   2    -1.84505121E-07   # C8'
 03051111 4133   00   2    -1.17271456E-07   # C9 e+e-
 03051111 4233   00   2     1.07246654E-06   # C9' e+e-
 03051111 4137   00   2     2.51808570E-06   # C10 e+e-
 03051111 4237   00   2    -7.99908437E-06   # C10' e+e-
 03051313 4133   00   2    -1.17271796E-07   # C9 mu+mu-
 03051313 4233   00   2     1.07246649E-06   # C9' mu+mu-
 03051313 4137   00   2     2.51808612E-06   # C10 mu+mu-
 03051313 4237   00   2    -7.99908452E-06   # C10' mu+mu-
 03051212 4137   00   2    -5.26826556E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.73205319E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.26826538E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.73205319E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.26821674E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.73205318E-06   # C11' nu_3 nu_3
