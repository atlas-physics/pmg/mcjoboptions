# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:49
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.36697163E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.98465030E+03  # scale for input parameters
    1    4.42959207E+01  # M_1
    2   -1.78535179E+03  # M_2
    3    1.54275521E+03  # M_3
   11   -1.61927277E+03  # A_t
   12   -1.52116265E+03  # A_b
   13    7.16409717E+02  # A_tau
   23    4.25704327E+02  # mu
   25    5.31103936E+01  # tan(beta)
   26    2.61985890E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.18131569E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.79017570E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.32540748E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.98465030E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.98465030E+03  # (SUSY scale)
  1  1     8.38585822E-06   # Y_u(Q)^DRbar
  2  2     4.26001597E-03   # Y_c(Q)^DRbar
  3  3     1.01307903E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.98465030E+03  # (SUSY scale)
  1  1     8.94967538E-04   # Y_d(Q)^DRbar
  2  2     1.70043832E-02   # Y_s(Q)^DRbar
  3  3     8.87527355E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.98465030E+03  # (SUSY scale)
  1  1     1.56178380E-04   # Y_e(Q)^DRbar
  2  2     3.22927356E-02   # Y_mu(Q)^DRbar
  3  3     5.43110689E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.98465030E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.61927291E+03   # A_t(Q)^DRbar
Block Ad Q=  3.98465030E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.52116253E+03   # A_b(Q)^DRbar
Block Ae Q=  3.98465030E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.16409665E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.98465030E+03  # soft SUSY breaking masses at Q
   1    4.42959207E+01  # M_1
   2   -1.78535179E+03  # M_2
   3    1.54275521E+03  # M_3
  21    6.73384547E+06  # M^2_(H,d)
  22    1.45424833E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.18131569E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.79017570E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.32540748E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22282784E+02  # h0
        35     2.61886272E+03  # H0
        36     2.61985890E+03  # A0
        37     2.62287448E+03  # H+
   1000001     1.01198815E+04  # ~d_L
   2000001     1.00959753E+04  # ~d_R
   1000002     1.01195195E+04  # ~u_L
   2000002     1.00987631E+04  # ~u_R
   1000003     1.01198854E+04  # ~s_L
   2000003     1.00959821E+04  # ~s_R
   1000004     1.01195234E+04  # ~c_L
   2000004     1.00987637E+04  # ~c_R
   1000005     2.34001828E+03  # ~b_1
   2000005     4.19484814E+03  # ~b_2
   1000006     3.78147247E+03  # ~t_1
   2000006     4.19874484E+03  # ~t_2
   1000011     1.00201134E+04  # ~e_L-
   2000011     1.00093351E+04  # ~e_R-
   1000012     1.00193491E+04  # ~nu_eL
   1000013     1.00201334E+04  # ~mu_L-
   2000013     1.00093701E+04  # ~mu_R-
   1000014     1.00193677E+04  # ~nu_muL
   1000015     1.00191030E+04  # ~tau_1-
   2000015     1.00260207E+04  # ~tau_2-
   1000016     1.00246526E+04  # ~nu_tauL
   1000021     1.91396861E+03  # ~g
   1000022     4.38376297E+01  # ~chi_10
   1000023     4.35905787E+02  # ~chi_20
   1000025     4.40003967E+02  # ~chi_30
   1000035     1.89687242E+03  # ~chi_40
   1000024     4.35559601E+02  # ~chi_1+
   1000037     1.89684056E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.86833730E-02   # alpha
Block Hmix Q=  3.98465030E+03  # Higgs mixing parameters
   1    4.25704327E+02  # mu
   2    5.31103936E+01  # tan[beta](Q)
   3    2.42939804E+02  # v(Q)
   4    6.86366066E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     7.27921664E-02   # Re[R_st(1,1)]
   1  2     9.97347131E-01   # Re[R_st(1,2)]
   2  1    -9.97347131E-01   # Re[R_st(2,1)]
   2  2     7.27921664E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     4.22793945E-03   # Re[R_sb(1,1)]
   1  2     9.99991062E-01   # Re[R_sb(1,2)]
   2  1    -9.99991062E-01   # Re[R_sb(2,1)]
   2  2     4.22793945E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.95712456E-01   # Re[R_sta(1,1)]
   1  2     9.55276998E-01   # Re[R_sta(1,2)]
   2  1    -9.55276998E-01   # Re[R_sta(2,1)]
   2  2     2.95712456E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.94599792E-01   # Re[N(1,1)]
   1  2    -6.23441977E-04   # Re[N(1,2)]
   1  3    -1.03055700E-01   # Re[N(1,3)]
   1  4     1.22632608E-02   # Re[N(1,4)]
   2  1     6.43490232E-02   # Re[N(2,1)]
   2  2     3.79032788E-02   # Re[N(2,2)]
   2  3    -7.05171055E-01   # Re[N(2,3)]
   2  4    -7.05093134E-01   # Re[N(2,4)]
   3  1     8.14209219E-02   # Re[N(3,1)]
   3  2    -2.47610518E-02   # Re[N(3,2)]
   3  3    -7.01446164E-01   # Re[N(3,3)]
   3  4     7.07623348E-01   # Re[N(3,4)]
   4  1     1.04411742E-03   # Re[N(4,1)]
   4  2    -9.98974396E-01   # Re[N(4,2)]
   4  3    -9.30504438E-03   # Re[N(4,3)]
   4  4    -4.42999196E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.31784103E-02   # Re[U(1,1)]
   1  2     9.99913161E-01   # Re[U(1,2)]
   2  1    -9.99913161E-01   # Re[U(2,1)]
   2  2    -1.31784103E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     6.27136633E-02   # Re[V(1,1)]
   1  2     9.98031561E-01   # Re[V(1,2)]
   2  1     9.98031561E-01   # Re[V(2,1)]
   2  2    -6.27136633E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02892733E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.89269072E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.12547911E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.60433966E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.35594490E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.20393967E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.69288513E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.13753187E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01410460E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.12550399E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04630954E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07031788E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.81240291E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.12063075E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.55774483E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.07933765E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.35801886E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.19152746E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.44860089E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.63835305E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00950962E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.12378738E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.03708948E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.69873201E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.86875796E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.73108725E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.63353541E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.96000208E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.17746233E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.93156841E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.91904058E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.71398248E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.52694857E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.57648715E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.95799383E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.39914708E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.92725749E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.35598063E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.15990289E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.50944355E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02049968E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.54862882E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02288900E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35805421E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.14593385E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.50714164E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01589419E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.06927034E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01370807E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.94283845E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.39642788E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.05405985E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.10940306E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.03374463E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.20664078E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.46293548E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.47104002E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92447488E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.68494668E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.59745401E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.76667958E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.54775409E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.55125792E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.46409524E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.47070821E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92294050E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.68556528E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.59805066E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.11600692E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.76634275E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.54707985E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.55065266E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.94728943E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.88824987E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.89966553E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.87659403E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.77317226E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.26167632E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.93117109E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.19722738E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.13962797E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.05830806E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.96879989E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.11608428E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.01964753E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.28083782E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     4.20236399E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.71736577E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.72626716E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.63442675E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.92209337E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.21864680E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.95090142E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.70462081E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.68483129E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.62006721E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.24502030E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.76300027E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.02110309E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.51159123E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.55102025E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.63449959E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.92206563E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.24228549E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.97471088E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.70452849E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.68544954E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.61995308E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.26577122E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.76266370E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.68660197E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.51091939E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.55041500E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.23940920E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.56838721E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.14684156E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.14964961E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.01939940E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.29480738E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.62265066E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.23299258E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     5.10514422E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.91580393E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.53941503E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.06481369E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.07709034E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.91028047E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.66314864E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.84345193E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.24638492E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     7.47053547E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.12206991E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.91037925E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.91815280E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.97671322E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.32855623E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.66610185E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     6.99658964E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42925705E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.41453662E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41959547E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42441812E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.18185078E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.54249829E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     9.41503064E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     8.95838229E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.17511333E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.80574316E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.19368965E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.61359177E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.43900596E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.56061554E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.86131588E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.18351157E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.18351157E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.81139883E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.47660099E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.52129194E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.28928125E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.52382214E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.64434570E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.97680353E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.13384354E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     5.17990129E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.07245894E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     9.25442307E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.25442307E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.93756085E-02   # ~g
#    BR                NDA      ID1      ID2
     2.21168133E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.35001466E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.98833499E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.25571045E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     3.71060758E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.10409557E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     3.79831476E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.07037318E-01    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.38020962E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.38020962E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.29165559E-03   # Gamma(h0)
     2.52100262E-03   2        22        22   # BR(h0 -> photon photon)
     1.37142344E-03   2        22        23   # BR(h0 -> photon Z)
     2.30065815E-02   2        23        23   # BR(h0 -> Z Z)
     2.00744953E-01   2       -24        24   # BR(h0 -> W W)
     8.02486607E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.27330348E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.34564413E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.76295477E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.50120422E-07   2        -2         2   # BR(h0 -> Up up)
     2.91329809E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.12763464E-07   2        -1         1   # BR(h0 -> Down down)
     2.21621845E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.88934017E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     5.95387854E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.80470276E+01   # Gamma(HH)
     2.33065861E-08   2        22        22   # BR(HH -> photon photon)
     2.10027435E-08   2        22        23   # BR(HH -> photon Z)
     1.24923497E-07   2        23        23   # BR(HH -> Z Z)
     4.52851317E-08   2       -24        24   # BR(HH -> W W)
     1.30137484E-05   2        21        21   # BR(HH -> gluon gluon)
     1.15909335E-08   2       -11        11   # BR(HH -> Electron electron)
     5.16061280E-04   2       -13        13   # BR(HH -> Muon muon)
     1.49041524E-01   2       -15        15   # BR(HH -> Tau tau)
     2.92955927E-14   2        -2         2   # BR(HH -> Up up)
     5.68269980E-09   2        -4         4   # BR(HH -> Charm charm)
     3.86037033E-04   2        -6         6   # BR(HH -> Top top)
     8.55936744E-07   2        -1         1   # BR(HH -> Down down)
     3.09593398E-04   2        -3         3   # BR(HH -> Strange strange)
     7.32870473E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.32891504E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.75631898E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.75631898E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     7.08661332E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.70114282E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.47688772E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.27825939E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.09106791E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.33028622E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.14943897E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     4.66038478E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.91320335E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.14795665E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.29971381E+01   # Gamma(A0)
     1.13701044E-08   2        22        22   # BR(A0 -> photon photon)
     3.34450755E-08   2        22        23   # BR(A0 -> photon Z)
     2.01631348E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.14947566E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.11780788E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.47805602E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.75905046E-14   2        -2         2   # BR(A0 -> Up up)
     5.35138134E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.70341759E-04   2        -6         6   # BR(A0 -> Top top)
     8.48794883E-07   2        -1         1   # BR(A0 -> Down down)
     3.07010137E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.26760886E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.08860582E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.92599003E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.92599003E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     7.59735719E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.65739258E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.71134401E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.29274054E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     5.60635058E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.01727849E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.05563616E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     5.97672466E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.47695695E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.98037932E-07   2        23        25   # BR(A0 -> Z h0)
     1.99862906E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.00486887E+02   # Gamma(Hp)
     1.24333276E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.31563588E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.50356377E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.89474352E-07   2        -1         2   # BR(Hp -> Down up)
     1.33056717E-05   2        -3         2   # BR(Hp -> Strange up)
     7.89176738E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.70284754E-08   2        -1         4   # BR(Hp -> Down charm)
     2.84524514E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.10512967E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.36608766E-08   2        -1         6   # BR(Hp -> Down top)
     9.35636429E-07   2        -3         6   # BR(Hp -> Strange top)
     7.45050001E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.90259312E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.62970027E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.45364224E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.43871749E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.81988606E-07   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.39433696E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.43836523E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.64272489E-07   2        24        25   # BR(Hp -> W h0)
     1.40723736E-11   2        24        35   # BR(Hp -> W HH)
     3.37695893E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.84856632E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.82072905E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.82071391E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000537E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.49151550E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.54520179E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.84856632E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.82072905E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.82071391E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999980E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.04800920E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999980E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.04800920E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26672826E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.70627273E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.54374622E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.04800920E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999980E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.21628424E-04   # BR(b -> s gamma)
    2    1.58975004E-06   # BR(b -> s mu+ mu-)
    3    3.52435109E-05   # BR(b -> s nu nu)
    4    2.75026431E-15   # BR(Bd -> e+ e-)
    5    1.17487171E-10   # BR(Bd -> mu+ mu-)
    6    2.45376389E-08   # BR(Bd -> tau+ tau-)
    7    9.10002910E-14   # BR(Bs -> e+ e-)
    8    3.88750339E-09   # BR(Bs -> mu+ mu-)
    9    8.23056315E-07   # BR(Bs -> tau+ tau-)
   10    9.46739796E-05   # BR(B_u -> tau nu)
   11    9.77945963E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42033530E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93544764E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15768670E-03   # epsilon_K
   17    2.28166468E-15   # Delta(M_K)
   18    2.47945499E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28916344E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.69569973E-16   # Delta(g-2)_electron/2
   21   -2.43511163E-11   # Delta(g-2)_muon/2
   22   -6.90401811E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.41633143E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88185972E-01   # C7
     0305 4322   00   2    -1.16325289E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.04578185E-01   # C8
     0305 6321   00   2    -2.44617327E-04   # C8'
 03051111 4133   00   0     1.62757506E+00   # C9 e+e-
 03051111 4133   00   2     1.62796404E+00   # C9 e+e-
 03051111 4233   00   2     5.75299267E-04   # C9' e+e-
 03051111 4137   00   0    -4.45026717E+00   # C10 e+e-
 03051111 4137   00   2    -4.44786116E+00   # C10 e+e-
 03051111 4237   00   2    -4.24832545E-03   # C10' e+e-
 03051313 4133   00   0     1.62757506E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62796381E+00   # C9 mu+mu-
 03051313 4233   00   2     5.75297451E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45026717E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44786139E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.24832492E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50483094E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.18463370E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50483094E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.18463693E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50483094E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.18554580E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85824532E-07   # C7
     0305 4422   00   2     1.65096965E-07   # C7
     0305 4322   00   2     8.70014323E-07   # C7'
     0305 6421   00   0     3.30484228E-07   # C8
     0305 6421   00   2    -5.07441110E-05   # C8
     0305 6321   00   2    -5.96887867E-07   # C8'
 03051111 4133   00   2     2.47178617E-07   # C9 e+e-
 03051111 4233   00   2     1.18678544E-05   # C9' e+e-
 03051111 4137   00   2     3.92278372E-08   # C10 e+e-
 03051111 4237   00   2    -8.76555684E-05   # C10' e+e-
 03051313 4133   00   2     2.47175067E-07   # C9 mu+mu-
 03051313 4233   00   2     1.18678487E-05   # C9' mu+mu-
 03051313 4137   00   2     3.92322251E-08   # C10 mu+mu-
 03051313 4237   00   2    -8.76555856E-05   # C10' mu+mu-
 03051212 4137   00   2     9.04699474E-09   # C11 nu_1 nu_1
 03051212 4237   00   2     1.89506310E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     9.04714454E-09   # C11 nu_2 nu_2
 03051414 4237   00   2     1.89506310E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     9.08844331E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     1.89506309E-05   # C11' nu_3 nu_3
