# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:32
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.79410462E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.75129171E+03  # scale for input parameters
    1   -2.75709784E+02  # M_1
    2    3.56517477E+02  # M_2
    3    4.12207614E+03  # M_3
   11    3.69847212E+03  # A_t
   12    1.92146164E+03  # A_b
   13   -5.13564404E+01  # A_tau
   23    3.83131226E+02  # mu
   25    3.67555788E+01  # tan(beta)
   26    6.79215234E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.73490181E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.87031812E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.62090843E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.75129171E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.75129171E+03  # (SUSY scale)
  1  1     8.38747465E-06   # Y_u(Q)^DRbar
  2  2     4.26083712E-03   # Y_c(Q)^DRbar
  3  3     1.01327431E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.75129171E+03  # (SUSY scale)
  1  1     6.19490619E-04   # Y_d(Q)^DRbar
  2  2     1.17703218E-02   # Y_s(Q)^DRbar
  3  3     6.14340574E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.75129171E+03  # (SUSY scale)
  1  1     1.08105643E-04   # Y_e(Q)^DRbar
  2  2     2.23528183E-02   # Y_mu(Q)^DRbar
  3  3     3.75937632E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.75129171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.69847130E+03   # A_t(Q)^DRbar
Block Ad Q=  3.75129171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.92146176E+03   # A_b(Q)^DRbar
Block Ae Q=  3.75129171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -5.13564423E+01   # A_tau(Q)^DRbar
Block MSOFT Q=  3.75129171E+03  # soft SUSY breaking masses at Q
   1   -2.75709784E+02  # M_1
   2    3.56517477E+02  # M_2
   3    4.12207614E+03  # M_3
  21    2.40009554E+05  # M^2_(H,d)
  22    9.51146109E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.73490181E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.87031812E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.62090843E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25328192E+02  # h0
        35     6.79388156E+02  # H0
        36     6.79215234E+02  # A0
        37     6.86713138E+02  # H+
   1000001     1.00981788E+04  # ~d_L
   2000001     1.00732486E+04  # ~d_R
   1000002     1.00978089E+04  # ~u_L
   2000002     1.00767651E+04  # ~u_R
   1000003     1.00981824E+04  # ~s_L
   2000003     1.00732547E+04  # ~s_R
   1000004     1.00978125E+04  # ~c_L
   2000004     1.00767660E+04  # ~c_R
   1000005     2.76377615E+03  # ~b_1
   2000005     2.86459705E+03  # ~b_2
   1000006     2.86422957E+03  # ~t_1
   2000006     4.91308016E+03  # ~t_2
   1000011     1.00216848E+04  # ~e_L-
   2000011     1.00085265E+04  # ~e_R-
   1000012     1.00209122E+04  # ~nu_eL
   1000013     1.00217017E+04  # ~mu_L-
   2000013     1.00085584E+04  # ~mu_R-
   1000014     1.00209287E+04  # ~nu_muL
   1000015     1.00175353E+04  # ~tau_1-
   2000015     1.00265797E+04  # ~tau_2-
   1000016     1.00256219E+04  # ~nu_tauL
   1000021     4.57518245E+03  # ~g
   1000022     2.71331146E+02  # ~chi_10
   1000023     3.33704840E+02  # ~chi_20
   1000025     4.00244149E+02  # ~chi_30
   1000035     4.50657059E+02  # ~chi_40
   1000024     3.34990782E+02  # ~chi_1+
   1000037     4.52107827E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.78729204E-02   # alpha
Block Hmix Q=  3.75129171E+03  # Higgs mixing parameters
   1    3.83131226E+02  # mu
   2    3.67555788E+01  # tan[beta](Q)
   3    2.43018096E+02  # v(Q)
   4    4.61333334E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99534964E-01   # Re[R_st(1,1)]
   1  2     3.04935390E-02   # Re[R_st(1,2)]
   2  1    -3.04935390E-02   # Re[R_st(2,1)]
   2  2    -9.99534964E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     4.90120578E-02   # Re[R_sb(1,1)]
   1  2     9.98798187E-01   # Re[R_sb(1,2)]
   2  1    -9.98798187E-01   # Re[R_sb(2,1)]
   2  2     4.90120578E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.42538556E-01   # Re[R_sta(1,1)]
   1  2     9.89789250E-01   # Re[R_sta(1,2)]
   2  1    -9.89789250E-01   # Re[R_sta(2,1)]
   2  2     1.42538556E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.69223354E-01   # Re[N(1,1)]
   1  2    -1.65620786E-02   # Re[N(1,2)]
   1  3    -2.02710969E-01   # Re[N(1,3)]
   1  4    -1.38708511E-01   # Re[N(1,4)]
   2  1    -3.43874082E-02   # Re[N(2,1)]
   2  2    -7.00131448E-01   # Re[N(2,2)]
   2  3     5.40222005E-01   # Re[N(2,3)]
   2  4    -4.65611047E-01   # Re[N(2,4)]
   3  1     2.41560242E-01   # Re[N(3,1)]
   3  2    -6.98207899E-02   # Re[N(3,2)]
   3  3    -6.73889580E-01   # Re[N(3,3)]
   3  4    -6.94727674E-01   # Re[N(3,4)]
   4  1     3.27451657E-02   # Re[N(4,1)]
   4  2    -7.10398980E-01   # Re[N(4,2)]
   4  3    -4.61455613E-01   # Re[N(4,3)]
   4  4     5.30395853E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.56274248E-01   # Re[U(1,1)]
   1  2     7.54522440E-01   # Re[U(1,2)]
   2  1     7.54522440E-01   # Re[U(2,1)]
   2  2     6.56274248E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.57183711E-01   # Re[V(1,1)]
   1  2     6.53201981E-01   # Re[V(1,2)]
   2  1     6.53201981E-01   # Re[V(2,1)]
   2  2     7.57183711E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02100509E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.39492055E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.18173931E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.82565637E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.06959542E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44476841E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.70454498E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.57267117E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.06753521E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.45994309E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.62406951E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.46218638E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04085787E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.35875944E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.75177535E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.89207114E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.48393896E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.12106945E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     8.46561157E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44576401E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.70139275E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.57359712E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.37870393E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.46040156E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.62226692E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.45980808E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.07780377E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.51613684E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.14821073E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.32614103E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.90215465E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.17032046E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.48975475E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.71289593E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.84269276E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.56596135E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.34956247E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.75050133E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.78241479E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.28189701E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44482019E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.65792774E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.41578449E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.20347327E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.61091342E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.49267318E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     2.59448881E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44581564E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.65266782E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.41481205E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.20264666E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.60980696E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.49418743E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.59566210E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.72653753E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.41141228E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.18533170E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.00758013E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.34870105E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.85152626E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.87254175E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.06518581E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.04148671E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     6.45893534E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88914054E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.37755025E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.69429049E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.34944332E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     8.84608552E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.56095157E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     5.99013572E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     7.90357979E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.89379997E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.06574411E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.04159678E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     6.70565141E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88806286E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.37786747E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.69599234E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.35054499E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     9.04266047E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.56169896E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     5.99008246E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     7.90351704E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.89341307E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.13670911E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.82493047E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.55979928E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.15623506E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.96257687E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.01024146E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.79497346E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.13231088E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.93035524E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.00863858E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.61360030E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.72656038E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.91559606E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.15240232E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     5.23728183E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.03046286E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.49929016E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.57099497E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.37737679E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.48998702E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.46866294E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.44589648E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.97378771E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     5.92339460E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.89342677E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.23735500E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.03042022E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.50259671E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.57086241E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.37769367E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.48997372E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.46861502E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.44588718E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.97586598E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     5.92496961E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.89303981E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.14220983E+02   # ~t_1
#    BR                NDA      ID1      ID2
     3.36763389E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     1.43390883E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.65125862E-03    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.53838909E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.34117795E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.96388187E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.17444198E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.76482321E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.37784590E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.66667407E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.48607795E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.40530575E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.82287119E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.98658110E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.66468760E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.04717302E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.24760708E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     2.15385522E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     8.05153987E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     2.71279520E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     2.18815483E-03    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     4.03046399E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.13096299E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     6.95071209E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.13815764E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.41559390E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33599172E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.33168581E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11175036E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11174214E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10882971E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     7.82887388E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.87350238E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.83948363E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.27360361E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     4.46924816E-04    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     4.45891401E-04    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     1.48972352E-04    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     1.48970661E-04    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     1.48319543E-04    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.31665079E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     5.95186635E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18671036E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18147971E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52156181E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52153165E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.46806406E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.43265330E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.43255018E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.40362261E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03425114E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     4.96959439E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     1.54286338E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.83741408E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.63170251E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     7.87895254E-03    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     7.85010206E-03    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     1.01022548E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     1.01020992E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     9.78621341E-03    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     2.27983420E-03    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     2.27977072E-03    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     2.26193454E-03    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     1.35100057E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     3.23982605E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     3.23982605E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     3.23615646E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     3.23615646E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     1.07995657E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     1.07995657E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     1.07995023E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     1.07995023E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     1.07765495E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     1.07765495E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     1.03346901E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.87647431E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.87647431E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.77987375E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.46062526E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.21501329E-04    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     1.59679803E+02   # ~g
#    BR                NDA      ID1      ID2
     1.36142390E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.36142390E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.57501203E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.57501203E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.70987391E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.70987391E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.56791721E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.56791721E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.32920712E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.25377048E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.77849262E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.63626064E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.24351787E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.24351787E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.71734700E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.71734700E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.71563248E-03   # Gamma(h0)
     2.48044687E-03   2        22        22   # BR(h0 -> photon photon)
     1.61283082E-03   2        22        23   # BR(h0 -> photon Z)
     3.00829351E-02   2        23        23   # BR(h0 -> Z Z)
     2.46902918E-01   2       -24        24   # BR(h0 -> W W)
     7.60190662E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.00242989E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.22517199E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.41593197E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.27502085E-07   2        -2         2   # BR(h0 -> Up up)
     2.47465324E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.78225734E-07   2        -1         1   # BR(h0 -> Down down)
     2.09130907E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.53563592E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.20722277E+01   # Gamma(HH)
     5.55340607E-07   2        22        22   # BR(HH -> photon photon)
     1.16722613E-06   2        22        23   # BR(HH -> photon Z)
     1.02179654E-05   2        23        23   # BR(HH -> Z Z)
     1.92260108E-05   2       -24        24   # BR(HH -> W W)
     1.09346208E-04   2        21        21   # BR(HH -> gluon gluon)
     1.10212392E-08   2       -11        11   # BR(HH -> Electron electron)
     4.90494186E-04   2       -13        13   # BR(HH -> Muon muon)
     1.41623405E-01   2       -15        15   # BR(HH -> Tau tau)
     9.07542193E-14   2        -2         2   # BR(HH -> Up up)
     1.75976803E-08   2        -4         4   # BR(HH -> Charm charm)
     8.50649538E-04   2        -6         6   # BR(HH -> Top top)
     9.65356458E-07   2        -1         1   # BR(HH -> Down down)
     3.49158322E-04   2        -3         3   # BR(HH -> Strange strange)
     8.25429335E-01   2        -5         5   # BR(HH -> Bottom bottom)
     7.67640120E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.12962285E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.85188159E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.90420924E-04   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.48375509E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.16435264E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.27055648E+01   # Gamma(A0)
     1.31733300E-06   2        22        22   # BR(A0 -> photon photon)
     4.23054909E-06   2        22        23   # BR(A0 -> photon Z)
     1.39937197E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.03534855E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.60776326E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.33046552E-01   2       -15        15   # BR(A0 -> Tau tau)
     7.09172587E-14   2        -2         2   # BR(A0 -> Up up)
     1.37580189E-08   2        -4         4   # BR(A0 -> Charm charm)
     9.07826098E-04   2        -6         6   # BR(A0 -> Top top)
     9.06901377E-07   2        -1         1   # BR(A0 -> Down down)
     3.28015812E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.75518095E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.44371875E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.69178888E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.77445016E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.55298501E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.41199965E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.59104097E-05   2        23        25   # BR(A0 -> Z h0)
     1.20239446E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.24190537E+01   # Gamma(Hp)
     1.31633406E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.62773898E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.59182447E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.00578203E-06   2        -1         2   # BR(Hp -> Down up)
     1.66531236E-05   2        -3         2   # BR(Hp -> Strange up)
     9.55918838E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.71490666E-08   2        -1         4   # BR(Hp -> Down charm)
     3.62481024E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.33861973E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.03766625E-07   2        -1         6   # BR(Hp -> Down top)
     2.72699201E-06   2        -3         6   # BR(Hp -> Strange top)
     8.13496622E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.49643922E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.54376917E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.71119587E-05   2        24        25   # BR(Hp -> W h0)
     2.28675732E-09   2        24        35   # BR(Hp -> W HH)
     2.56933978E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.05007510E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.35092250E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.35097257E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99962934E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.77273439E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.40207477E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.05007510E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.35092250E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.35097257E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999547E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.52762102E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999547E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.52762102E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26152594E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.96002500E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.59348437E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.52762102E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999547E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.82550590E-04   # BR(b -> s gamma)
    2    1.58732636E-06   # BR(b -> s mu+ mu-)
    3    3.52518650E-05   # BR(b -> s nu nu)
    4    2.40226184E-15   # BR(Bd -> e+ e-)
    5    1.02621994E-10   # BR(Bd -> mu+ mu-)
    6    2.14885236E-08   # BR(Bd -> tau+ tau-)
    7    7.97276168E-14   # BR(Bs -> e+ e-)
    8    3.40596366E-09   # BR(Bs -> mu+ mu-)
    9    7.22578770E-07   # BR(Bs -> tau+ tau-)
   10    7.94377395E-05   # BR(B_u -> tau nu)
   11    8.20561436E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41793513E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92808688E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15715659E-03   # epsilon_K
   17    2.28167756E-15   # Delta(M_K)
   18    2.47987238E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29019835E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.32436317E-16   # Delta(g-2)_electron/2
   21    9.93745687E-12   # Delta(g-2)_muon/2
   22    2.81678803E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.43316537E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.50457197E-01   # C7
     0305 4322   00   2    -1.17915764E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.63003947E-01   # C8
     0305 6321   00   2    -1.32449425E-03   # C8'
 03051111 4133   00   0     1.62468635E+00   # C9 e+e-
 03051111 4133   00   2     1.62493178E+00   # C9 e+e-
 03051111 4233   00   2     9.21136630E-05   # C9' e+e-
 03051111 4137   00   0    -4.44737845E+00   # C10 e+e-
 03051111 4137   00   2    -4.44565262E+00   # C10 e+e-
 03051111 4237   00   2    -6.81925912E-04   # C10' e+e-
 03051313 4133   00   0     1.62468635E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62493169E+00   # C9 mu+mu-
 03051313 4233   00   2     9.20749747E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44737845E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44565271E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.81887576E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50497874E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.47523570E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50497874E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.47531923E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50497874E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.49886006E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85808283E-07   # C7
     0305 4422   00   2     1.22920972E-05   # C7
     0305 4322   00   2    -1.06316202E-07   # C7'
     0305 6421   00   0     3.30470309E-07   # C8
     0305 6421   00   2    -5.71302996E-06   # C8
     0305 6321   00   2    -2.36864086E-07   # C8'
 03051111 4133   00   2     1.04430966E-06   # C9 e+e-
 03051111 4233   00   2     7.20256311E-06   # C9' e+e-
 03051111 4137   00   2     3.82653878E-07   # C10 e+e-
 03051111 4237   00   2    -5.34490660E-05   # C10' e+e-
 03051313 4133   00   2     1.04430572E-06   # C9 mu+mu-
 03051313 4233   00   2     7.20256145E-06   # C9' mu+mu-
 03051313 4137   00   2     3.82658040E-07   # C10 mu+mu-
 03051313 4237   00   2    -5.34490710E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.73908629E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.15628888E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.73906532E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.15628888E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.73322233E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.15628888E-05   # C11' nu_3 nu_3
