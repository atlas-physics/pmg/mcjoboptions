# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:15
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.02685580E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.62261111E+03  # scale for input parameters
    1   -2.68786152E+02  # M_1
    2    2.62459727E+02  # M_2
    3    1.87176128E+03  # M_3
   11   -1.81507921E+03  # A_t
   12    1.88417206E+03  # A_b
   13    1.35332575E+03  # A_tau
   23   -1.91041259E+03  # mu
   25    3.93293292E+01  # tan(beta)
   26    2.94468987E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.76929594E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.43343587E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.91008650E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.62261111E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.62261111E+03  # (SUSY scale)
  1  1     8.38708193E-06   # Y_u(Q)^DRbar
  2  2     4.26063762E-03   # Y_c(Q)^DRbar
  3  3     1.01322686E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.62261111E+03  # (SUSY scale)
  1  1     6.62838418E-04   # Y_d(Q)^DRbar
  2  2     1.25939299E-02   # Y_s(Q)^DRbar
  3  3     6.57328007E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.62261111E+03  # (SUSY scale)
  1  1     1.15670151E-04   # Y_e(Q)^DRbar
  2  2     2.39169187E-02   # Y_mu(Q)^DRbar
  3  3     4.02243227E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.62261111E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.81507946E+03   # A_t(Q)^DRbar
Block Ad Q=  3.62261111E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.88417261E+03   # A_b(Q)^DRbar
Block Ae Q=  3.62261111E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.35332567E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.62261111E+03  # soft SUSY breaking masses at Q
   1   -2.68786152E+02  # M_1
   2    2.62459727E+02  # M_2
   3    1.87176128E+03  # M_3
  21    4.97899457E+06  # M^2_(H,d)
  22   -3.40279677E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.76929594E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.43343587E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.91008650E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22110225E+02  # h0
        35     2.94397555E+03  # H0
        36     2.94468987E+03  # A0
        37     2.94648039E+03  # H+
   1000001     1.01180157E+04  # ~d_L
   2000001     1.00928909E+04  # ~d_R
   1000002     1.01176502E+04  # ~u_L
   2000002     1.00957307E+04  # ~u_R
   1000003     1.01180205E+04  # ~s_L
   2000003     1.00928964E+04  # ~s_R
   1000004     1.01176540E+04  # ~c_L
   2000004     1.00957316E+04  # ~c_R
   1000005     3.79949516E+03  # ~b_1
   2000005     4.94133755E+03  # ~b_2
   1000006     3.44840478E+03  # ~t_1
   2000006     3.80561799E+03  # ~t_2
   1000011     1.00212282E+04  # ~e_L-
   2000011     1.00092184E+04  # ~e_R-
   1000012     1.00204588E+04  # ~nu_eL
   1000013     1.00212579E+04  # ~mu_L-
   2000013     1.00092381E+04  # ~mu_R-
   1000014     1.00204755E+04  # ~nu_muL
   1000015     1.00145979E+04  # ~tau_1-
   2000015     1.00300392E+04  # ~tau_2-
   1000016     1.00252834E+04  # ~nu_tauL
   1000021     2.27889547E+03  # ~g
   1000022     2.70725765E+02  # ~chi_10
   1000023     2.91998983E+02  # ~chi_20
   1000025     1.93396497E+03  # ~chi_30
   1000035     1.93402550E+03  # ~chi_40
   1000024     2.92173007E+02  # ~chi_1+
   1000037     1.93514022E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.49029939E-02   # alpha
Block Hmix Q=  3.62261111E+03  # Higgs mixing parameters
   1   -1.91041259E+03  # mu
   2    3.93293292E+01  # tan[beta](Q)
   3    2.43043409E+02  # v(Q)
   4    8.67119843E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.04398360E-01   # Re[R_st(1,1)]
   1  2     9.94535561E-01   # Re[R_st(1,2)]
   2  1    -9.94535561E-01   # Re[R_st(2,1)]
   2  2     1.04398360E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99837787E-01   # Re[R_sb(1,1)]
   1  2     1.80110815E-02   # Re[R_sb(1,2)]
   2  1    -1.80110815E-02   # Re[R_sb(2,1)]
   2  2    -9.99837787E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -5.13340670E-01   # Re[R_sta(1,1)]
   1  2     8.58184920E-01   # Re[R_sta(1,2)]
   2  1    -8.58184920E-01   # Re[R_sta(2,1)]
   2  2    -5.13340670E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99709983E-01   # Re[N(1,1)]
   1  2    -6.38070319E-04   # Re[N(1,2)]
   1  3     2.37529014E-02   # Re[N(1,3)]
   1  4    -3.91703628E-03   # Re[N(1,4)]
   2  1    -3.33586313E-04   # Re[N(2,1)]
   2  2    -9.99115768E-01   # Re[N(2,2)]
   2  3    -4.17269112E-02   # Re[N(2,3)]
   2  4    -5.14152050E-03   # Re[N(2,4)]
   3  1    -1.95700956E-02   # Re[N(3,1)]
   3  2     2.58681066E-02   # Re[N(3,2)]
   3  3    -7.06362141E-01   # Re[N(3,3)]
   3  4     7.07107049E-01   # Re[N(3,4)]
   4  1     1.40303496E-02   # Re[N(4,1)]
   4  2    -3.31378266E-02   # Re[N(4,2)]
   4  3     7.06220356E-01   # Re[N(4,3)]
   4  4     7.07076971E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.98259953E-01   # Re[U(1,1)]
   1  2    -5.89666608E-02   # Re[U(1,2)]
   2  1     5.89666608E-02   # Re[U(2,1)]
   2  2    -9.98259953E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99973580E-01   # Re[V(1,1)]
   1  2     7.26904127E-03   # Re[V(1,2)]
   2  1     7.26904127E-03   # Re[V(2,1)]
   2  2     9.99973580E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02170183E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99461629E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.55485150E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.82725333E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44646422E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.70580213E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03994125E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.85861024E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06729434E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.96540103E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04282663E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.95278044E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.39838535E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.22592600E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.08576761E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44752572E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.69948815E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03773471E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.31453756E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     5.49921111E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06286306E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.96396647E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.27313174E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.19984685E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.02249799E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.11014385E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.10217457E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.03858861E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.52674814E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.57379046E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.40638461E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.97515753E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.99840026E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.00606593E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.93677967E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.75772230E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44650320E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.66339597E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.03745723E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.72871102E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     4.66613377E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08750952E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44756274E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.65706932E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.03523906E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.72598899E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     4.66272740E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08309134E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     7.57394953E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.74638695E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.17921153E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.51709065E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.09014628E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.86702963E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.05103617E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.70699484E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.23131459E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.77648252E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92219098E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.54437698E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.63766149E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.19367030E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.03707657E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.36428851E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.42278324E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.23190885E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.77587252E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92138221E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.54471126E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.63761921E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.19347608E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.03703646E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.40388937E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.42245858E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.35250316E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.33888681E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     7.04834184E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.80075470E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.79998242E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.39665931E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.80229911E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.22544748E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     8.72973315E-03    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     3.09479338E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.88586398E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.13486228E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.91723008E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.91581599E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.25906691E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.81302153E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.81219244E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.26032356E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     5.68821090E-03    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     2.90903258E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     4.37154734E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.40262258E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.03946653E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.69588944E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.54424817E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.66127449E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.19479563E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.04061812E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.42254091E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.40269072E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.03943872E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.69580082E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.54458114E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.66121037E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.19459496E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.04057926E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.42221618E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.60255387E+02   # ~t_1
#    BR                NDA      ID1      ID2
     4.66593414E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.12534566E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.23526605E-03    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.00749945E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     9.99285228E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.49379389E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.04808229E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     6.50645276E-04    2     1000021         4   # BR(~t_1 -> ~g c)
     5.43351856E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.33467106E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.68139635E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     6.95579653E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.14791340E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.28314885E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.40406953E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.80611110E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.14714634E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     4.48107637E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.78624169E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.91138481E-11   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.45984529E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.40076101E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.05871579E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.05862460E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.02199664E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.40069446E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     9.62616277E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.89327607E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.88735783E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.95856329E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.55905077E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.77393028E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     3.36237781E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.06679316E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.98006974E-11   # chi^0_2
#    BR                NDA      ID1      ID2
     2.99037380E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.98051948E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.57150805E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.14956743E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.14930809E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.68970226E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.35259482E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.35177441E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.13537357E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.40260342E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.52694546E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.65827977E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.65827977E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.98596749E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.65210073E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.26087653E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.87697629E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.01626520E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     7.09931246E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.18578305E-02    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.18578305E-02    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.42973610E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.83727081E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.83727081E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.20818686E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.07418328E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.09531078E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.81293251E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.00300298E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     7.06589159E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.07984789E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.07984789E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.01176930E-02   # ~g
#    BR                NDA      ID1      ID2
     7.16831636E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     8.02257319E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     1.22238616E-03    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.22238564E-03    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.12113777E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.58737409E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     3.58738913E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     1.08819798E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.22982331E-03    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     2.22981990E-03    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     1.30929450E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.22896418E-03    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     2.22896678E-03    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     1.50609884E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.87350117E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.86950052E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     4.45851393E-03    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     4.45851393E-03    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     4.45851311E-03    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     4.45851311E-03    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     2.81170441E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.81170441E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     5.07926819E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.07926819E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.21375621E-03   # Gamma(h0)
     2.56644772E-03   2        22        22   # BR(h0 -> photon photon)
     1.38142979E-03   2        22        23   # BR(h0 -> photon Z)
     2.30346751E-02   2        23        23   # BR(h0 -> Z Z)
     2.01694100E-01   2       -24        24   # BR(h0 -> W W)
     8.18602032E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.29795779E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.35660974E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.79455226E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.50849255E-07   2        -2         2   # BR(h0 -> Up up)
     2.92747555E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.15894455E-07   2        -1         1   # BR(h0 -> Down down)
     2.22753795E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.91783679E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     7.22332144E+01   # Gamma(HH)
     1.71102993E-08   2        22        22   # BR(HH -> photon photon)
     5.79109811E-08   2        22        23   # BR(HH -> photon Z)
     4.01255351E-07   2        23        23   # BR(HH -> Z Z)
     1.24208594E-07   2       -24        24   # BR(HH -> W W)
     8.96938550E-06   2        21        21   # BR(HH -> gluon gluon)
     8.88569085E-09   2       -11        11   # BR(HH -> Electron electron)
     3.95630914E-04   2       -13        13   # BR(HH -> Muon muon)
     1.14262657E-01   2       -15        15   # BR(HH -> Tau tau)
     6.05938255E-14   2        -2         2   # BR(HH -> Up up)
     1.17547104E-08   2        -4         4   # BR(HH -> Charm charm)
     1.12475599E-03   2        -6         6   # BR(HH -> Top top)
     6.88413209E-07   2        -1         1   # BR(HH -> Down down)
     2.48972475E-04   2        -3         3   # BR(HH -> Strange strange)
     7.10123316E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.16187422E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     5.22350740E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     5.22350740E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.80571066E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.43851331E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.00582336E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.19849186E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     4.76567701E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.65652543E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     9.52723431E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.37591386E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.90618554E+01   # Gamma(A0)
     6.33252981E-09   2        22        22   # BR(A0 -> photon photon)
     1.13678139E-08   2        22        23   # BR(A0 -> photon Z)
     1.71469606E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.79778846E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.91715069E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.13131903E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.92668449E-14   2        -2         2   # BR(A0 -> Up up)
     1.14967015E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.11887429E-03   2        -6         6   # BR(A0 -> Top top)
     6.81580268E-07   2        -1         1   # BR(A0 -> Down down)
     2.46501745E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.03092203E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.24138062E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     5.46773100E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     5.46773100E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     6.20283942E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.48969397E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.30450957E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.14380599E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     5.24279871E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.98184393E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.91758098E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     6.73865792E-07   2        23        25   # BR(A0 -> Z h0)
     3.38780746E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.92482404E+01   # Gamma(Hp)
     9.97026608E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.26260015E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.20570568E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.63696635E-07   2        -1         2   # BR(Hp -> Down up)
     1.11993007E-05   2        -3         2   # BR(Hp -> Strange up)
     7.61012224E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.15291874E-08   2        -1         4   # BR(Hp -> Down charm)
     2.39168602E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.06568939E-03   2        -5         4   # BR(Hp -> Bottom charm)
     5.91294019E-08   2        -1         6   # BR(Hp -> Down top)
     1.64315987E-06   2        -3         6   # BR(Hp -> Strange top)
     7.19356573E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.48256571E-11   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.80565120E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.18616909E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.38773923E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     4.78698928E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.78975209E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.88705496E-07   2        24        25   # BR(Hp -> W h0)
     1.69347984E-12   2        24        35   # BR(Hp -> W HH)
     3.16094842E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.59681457E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.54683645E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.54679614E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002607E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.20431765E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.46497607E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.59681457E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.54683645E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.54679614E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999732E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.68164545E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999732E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.68164545E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26677812E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.08646286E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.21735250E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.68164545E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999732E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.33527645E-04   # BR(b -> s gamma)
    2    1.58847689E-06   # BR(b -> s mu+ mu-)
    3    3.52449812E-05   # BR(b -> s nu nu)
    4    2.14826715E-15   # BR(Bd -> e+ e-)
    5    9.17717232E-11   # BR(Bd -> mu+ mu-)
    6    1.92226930E-08   # BR(Bd -> tau+ tau-)
    7    7.27538784E-14   # BR(Bs -> e+ e-)
    8    3.10805030E-09   # BR(Bs -> mu+ mu-)
    9    6.59658278E-07   # BR(Bs -> tau+ tau-)
   10    9.56442496E-05   # BR(B_u -> tau nu)
   11    9.87968482E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42108861E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93624585E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15798889E-03   # epsilon_K
   17    2.28166617E-15   # Delta(M_K)
   18    2.47937460E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28903472E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.35934780E-16   # Delta(g-2)_electron/2
   21   -1.00870567E-11   # Delta(g-2)_muon/2
   22   -2.86176999E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.20927626E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.03750086E-01   # C7
     0305 4322   00   2    -1.90333681E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.07090388E-01   # C8
     0305 6321   00   2    -1.77435801E-04   # C8'
 03051111 4133   00   0     1.62356432E+00   # C9 e+e-
 03051111 4133   00   2     1.62379612E+00   # C9 e+e-
 03051111 4233   00   2     3.25981961E-04   # C9' e+e-
 03051111 4137   00   0    -4.44625642E+00   # C10 e+e-
 03051111 4137   00   2    -4.44392248E+00   # C10 e+e-
 03051111 4237   00   2    -2.42315808E-03   # C10' e+e-
 03051313 4133   00   0     1.62356432E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62379594E+00   # C9 mu+mu-
 03051313 4233   00   2     3.25981543E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44625642E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44392266E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.42315802E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50484675E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.24345663E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50484675E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.24345734E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50484675E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.24365676E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85795067E-07   # C7
     0305 4422   00   2    -9.16038142E-06   # C7
     0305 4322   00   2    -9.64428623E-07   # C7'
     0305 6421   00   0     3.30458989E-07   # C8
     0305 6421   00   2     1.60279272E-05   # C8
     0305 6321   00   2    -2.71213573E-08   # C8'
 03051111 4133   00   2     1.02672223E-06   # C9 e+e-
 03051111 4233   00   2     6.57871659E-06   # C9' e+e-
 03051111 4137   00   2     3.23812322E-07   # C10 e+e-
 03051111 4237   00   2    -4.89058087E-05   # C10' e+e-
 03051313 4133   00   2     1.02671837E-06   # C9 mu+mu-
 03051313 4233   00   2     6.57871498E-06   # C9' mu+mu-
 03051313 4137   00   2     3.23816286E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.89058136E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.02045212E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.05826987E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.02043169E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.05826987E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.01470124E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.05826987E-05   # C11' nu_3 nu_3
