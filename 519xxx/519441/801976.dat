# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:54
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.56349277E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.73822139E+03  # scale for input parameters
    1    4.50618617E+01  # M_1
    2    8.59797111E+02  # M_2
    3    3.67639003E+03  # M_3
   11   -5.03184618E+03  # A_t
   12   -1.68356774E+03  # A_b
   13    8.10094222E+02  # A_tau
   23    4.15776594E+02  # mu
   25    4.48627119E+01  # tan(beta)
   26    4.33955299E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.56184281E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.03551124E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.85495511E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.73822139E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.73822139E+03  # (SUSY scale)
  1  1     8.38645478E-06   # Y_u(Q)^DRbar
  2  2     4.26031903E-03   # Y_c(Q)^DRbar
  3  3     1.01315110E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.73822139E+03  # (SUSY scale)
  1  1     7.56038967E-04   # Y_d(Q)^DRbar
  2  2     1.43647404E-02   # Y_s(Q)^DRbar
  3  3     7.49753746E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.73822139E+03  # (SUSY scale)
  1  1     1.31934328E-04   # Y_e(Q)^DRbar
  2  2     2.72798347E-02   # Y_mu(Q)^DRbar
  3  3     4.58801942E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.73822139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.03184608E+03   # A_t(Q)^DRbar
Block Ad Q=  2.73822139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.68356777E+03   # A_b(Q)^DRbar
Block Ae Q=  2.73822139E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.10094227E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.73822139E+03  # soft SUSY breaking masses at Q
   1    4.50618617E+01  # M_1
   2    8.59797111E+02  # M_2
   3    3.67639003E+03  # M_3
  21    1.83779822E+07  # M^2_(H,d)
  22   -7.48322005E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.56184281E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.03551124E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.85495511E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26785928E+02  # h0
        35     4.33770168E+03  # H0
        36     4.33955299E+03  # A0
        37     4.33751101E+03  # H+
   1000001     1.00870090E+04  # ~d_L
   2000001     1.00630340E+04  # ~d_R
   1000002     1.00867005E+04  # ~u_L
   2000002     1.00664061E+04  # ~u_R
   1000003     1.00870211E+04  # ~s_L
   2000003     1.00630548E+04  # ~s_R
   1000004     1.00867124E+04  # ~c_L
   2000004     1.00664080E+04  # ~c_R
   1000005     2.94493492E+03  # ~b_1
   2000005     3.61089098E+03  # ~b_2
   1000006     2.07026231E+03  # ~t_1
   2000006     3.62169390E+03  # ~t_2
   1000011     1.00209999E+04  # ~e_L-
   2000011     1.00085843E+04  # ~e_R-
   1000012     1.00202390E+04  # ~nu_eL
   1000013     1.00210556E+04  # ~mu_L-
   2000013     1.00086906E+04  # ~mu_R-
   1000014     1.00202938E+04  # ~nu_muL
   1000015     1.00357920E+04  # ~tau_1-
   2000015     1.00397811E+04  # ~tau_2-
   1000016     1.00358385E+04  # ~nu_tauL
   1000021     4.08591935E+03  # ~g
   1000022     4.47886947E+01  # ~chi_10
   1000023     4.19042429E+02  # ~chi_20
   1000025     4.27089179E+02  # ~chi_30
   1000035     9.39399510E+02  # ~chi_40
   1000024     4.18505048E+02  # ~chi_1+
   1000037     9.38992566E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.19400898E-02   # alpha
Block Hmix Q=  2.73822139E+03  # Higgs mixing parameters
   1    4.15776594E+02  # mu
   2    4.48627119E+01  # tan[beta](Q)
   3    2.43335160E+02  # v(Q)
   4    1.88317202E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.23259347E-02   # Re[R_st(1,1)]
   1  2     9.96605459E-01   # Re[R_st(1,2)]
   2  1    -9.96605459E-01   # Re[R_st(2,1)]
   2  2     8.23259347E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.07709084E-02   # Re[R_sb(1,1)]
   1  2     9.99941992E-01   # Re[R_sb(1,2)]
   2  1    -9.99941992E-01   # Re[R_sb(2,1)]
   2  2     1.07709084E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     8.96152584E-01   # Re[R_sta(1,1)]
   1  2     4.43746038E-01   # Re[R_sta(1,2)]
   2  1    -4.43746038E-01   # Re[R_sta(2,1)]
   2  2     8.96152584E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.94060036E-01   # Re[N(1,1)]
   1  2    -1.43947764E-03   # Re[N(1,2)]
   1  3     1.07920003E-01   # Re[N(1,3)]
   1  4    -1.39944994E-02   # Re[N(1,4)]
   2  1    -8.62940310E-02   # Re[N(2,1)]
   2  2    -1.12772111E-01   # Re[N(2,2)]
   2  3     7.02989512E-01   # Re[N(2,3)]
   2  4    -6.96879859E-01   # Re[N(2,4)]
   3  1     6.60786882E-02   # Re[N(3,1)]
   3  2    -4.11720349E-02   # Re[N(3,2)]
   3  3    -7.01116713E-01   # Re[N(3,3)]
   3  4    -7.08783341E-01   # Re[N(3,4)]
   4  1     5.62069634E-03   # Re[N(4,1)]
   4  2    -9.92766459E-01   # Re[N(4,2)]
   4  3    -5.09349984E-02   # Re[N(4,3)]
   4  4     1.08576200E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.20989833E-02   # Re[U(1,1)]
   1  2     9.97397482E-01   # Re[U(1,2)]
   2  1     9.97397482E-01   # Re[U(2,1)]
   2  2     7.20989833E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.53819459E-01   # Re[V(1,1)]
   1  2     9.88098970E-01   # Re[V(1,2)]
   2  1     9.88098970E-01   # Re[V(2,1)]
   2  2     1.53819459E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02853582E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.88196777E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.42121758E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.35089615E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42614661E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.67444381E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     7.77904315E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.97677803E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.20421806E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04583862E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05811899E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.82461710E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     8.82056306E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.76029313E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.90414555E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42763312E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.66667012E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.28275688E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.19605624E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.97372214E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.20089962E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.03957823E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.74839561E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.27086922E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.33056237E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.07515248E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.86145793E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     6.88884460E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.77307355E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44107408E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.84451676E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.36177628E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.56833819E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.07293303E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.08794847E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.43012699E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42619350E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.76805472E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.36984086E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.80191998E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01267483E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.45827548E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.93297454E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42767976E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.75897482E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.36842235E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.80005405E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.00955559E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.55977324E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.92688484E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.84726922E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.77996025E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.05925052E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.39336294E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.32970014E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.36817564E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.59960206E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.59612355E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.92000195E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.89961103E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.88673332E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.05302567E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.09644593E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.80111067E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.25684671E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     6.67950275E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.26054737E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.07866064E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.59696516E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.91938795E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.10838454E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.89815904E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.88719465E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.05358887E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.39132324E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.09466776E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.25645074E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     6.73050572E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.26046575E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.07813679E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.46007162E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.14647888E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.42339105E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.39926076E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.04445849E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.83844759E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.59381117E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.18191849E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.33082010E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.95694974E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.87792696E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.30553061E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.19814130E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.34021289E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.99642456E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.10650980E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     4.76580550E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.76804862E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.85101796E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.89216177E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.69560794E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.61029833E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.88663718E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.98728361E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.05488705E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.23155484E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.04035759E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.23713145E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.07833234E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.76812264E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.85096924E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.92262938E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.72712479E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.61017771E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.88709796E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.98715396E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.05737590E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.23115173E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.09959558E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.23705327E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.07780843E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.12922857E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.53906588E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.30144449E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36076974E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.57016494E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73518231E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.27925316E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.33025696E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.19737842E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.49315648E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.53324219E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.40780863E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.67291310E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.20796872E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.08336957E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.41670304E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.00717847E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.82325912E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98071826E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.92816090E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     8.00316962E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.15624609E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.52174309E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.59784315E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.48204441E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.32577511E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.18014467E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.51549373E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.38997346E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.43394541E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.44199218E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.55791024E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.23115526E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.93741675E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.06212135E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.39811507E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.16607080E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.16607080E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.72646138E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.54314558E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.97674276E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.12335933E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.73722429E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.64222597E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.01960174E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.59647870E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.13322921E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.13322921E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.04992573E+02   # ~g
#    BR                NDA      ID1      ID2
     1.23521342E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.23521342E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     3.11725061E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.11725061E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.63385306E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.63385306E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.32974239E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.32974239E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.76291178E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     2.76291178E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.79914279E-03   # Gamma(h0)
     2.54414380E-03   2        22        22   # BR(h0 -> photon photon)
     1.78844067E-03   2        22        23   # BR(h0 -> photon Z)
     3.51821458E-02   2        23        23   # BR(h0 -> Z Z)
     2.80752345E-01   2       -24        24   # BR(h0 -> W W)
     7.71605292E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.59911138E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.04577582E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.89891827E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.30514862E-07   2        -2         2   # BR(h0 -> Up up)
     2.53312952E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.30969655E-07   2        -1         1   # BR(h0 -> Down down)
     1.92039560E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.10836018E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     7.01861634E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.49320318E+02   # Gamma(HH)
     5.20151924E-09   2        22        22   # BR(HH -> photon photon)
     1.97422680E-08   2        22        23   # BR(HH -> photon Z)
     7.21590248E-08   2        23        23   # BR(HH -> Z Z)
     1.38978678E-08   2       -24        24   # BR(HH -> W W)
     4.54681635E-06   2        21        21   # BR(HH -> gluon gluon)
     8.11018672E-09   2       -11        11   # BR(HH -> Electron electron)
     3.61144707E-04   2       -13        13   # BR(HH -> Muon muon)
     1.04308989E-01   2       -15        15   # BR(HH -> Tau tau)
     3.54158956E-14   2        -2         2   # BR(HH -> Up up)
     6.87110861E-09   2        -4         4   # BR(HH -> Charm charm)
     4.53940705E-04   2        -6         6   # BR(HH -> Top top)
     5.59027039E-07   2        -1         1   # BR(HH -> Down down)
     2.02232439E-04   2        -3         3   # BR(HH -> Strange strange)
     5.38082129E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.35856598E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.03985313E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.03985313E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.15156895E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     7.38219334E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.64479763E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.63981827E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.86904559E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.22499198E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.54687258E-06   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.75018186E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     6.99497803E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.73953945E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.85113932E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     3.61707662E-07   2        25        25   # BR(HH -> h0 h0)
     2.60572090E-05   2  -1000006   1000006   # BR(HH -> Stop1 stop1)
DECAY        36     1.41734478E+02   # Gamma(A0)
     3.91124306E-08   2        22        22   # BR(A0 -> photon photon)
     6.09715179E-08   2        22        23   # BR(A0 -> photon Z)
     7.65836548E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.86823306E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.50371779E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.01197540E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.14320067E-14   2        -2         2   # BR(A0 -> Up up)
     6.09802409E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.02604256E-04   2        -6         6   # BR(A0 -> Top top)
     5.42317850E-07   2        -1         1   # BR(A0 -> Down down)
     1.96188197E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.22012638E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.25587059E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.09071241E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.09071241E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.73069745E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     7.87202006E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.89489549E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.57185864E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.86535782E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.61442186E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.95978476E-08   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.91125974E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.00998565E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     5.11092148E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     8.77544620E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.75975423E-07   2        23        25   # BR(A0 -> Z h0)
     1.68579817E-13   2        23        35   # BR(A0 -> Z HH)
     7.92621065E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.69463017E+02   # Gamma(Hp)
     8.81497437E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.76867686E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.06599651E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.32421090E-07   2        -1         2   # BR(Hp -> Down up)
     9.02554997E-06   2        -3         2   # BR(Hp -> Strange up)
     6.12075368E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.52439739E-08   2        -1         4   # BR(Hp -> Down charm)
     1.91920389E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.57124674E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.05375894E-08   2        -1         6   # BR(Hp -> Down top)
     9.52133477E-07   2        -3         6   # BR(Hp -> Strange top)
     5.77996403E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.01740395E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.01733059E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     7.72640304E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     9.57049472E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     8.71027072E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     9.23079150E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     9.31132577E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.37304759E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.47016647E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.69156445E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.01269376E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.01266292E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001532E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.81529438E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.96854188E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.69156445E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.01269376E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.01266292E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999880E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.20024771E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999880E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.20024771E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25633789E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.80740646E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.23432097E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.20024771E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999880E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    2.97647914E-04   # BR(b -> s gamma)
    2    1.59432936E-06   # BR(b -> s mu+ mu-)
    3    3.52564979E-05   # BR(b -> s nu nu)
    4    3.15581433E-15   # BR(Bd -> e+ e-)
    5    1.34810470E-10   # BR(Bd -> mu+ mu-)
    6    2.80861606E-08   # BR(Bd -> tau+ tau-)
    7    1.05905675E-13   # BR(Bs -> e+ e-)
    8    4.52421054E-09   # BR(Bs -> mu+ mu-)
    9    9.55139966E-07   # BR(Bs -> tau+ tau-)
   10    9.62831914E-05   # BR(B_u -> tau nu)
   11    9.94568505E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42112300E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93652518E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15787637E-03   # epsilon_K
   17    2.28166441E-15   # Delta(M_K)
   18    2.48002064E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29060217E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.09146495E-16   # Delta(g-2)_electron/2
   21    1.74924895E-11   # Delta(g-2)_muon/2
   22    4.96299723E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.88148154E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.60625655E-01   # C7
     0305 4322   00   2     1.08761097E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -8.37044987E-02   # C8
     0305 6321   00   2    -2.57420634E-05   # C8'
 03051111 4133   00   0     1.61391155E+00   # C9 e+e-
 03051111 4133   00   2     1.61443206E+00   # C9 e+e-
 03051111 4233   00   2     4.25110311E-04   # C9' e+e-
 03051111 4137   00   0    -4.43660365E+00   # C10 e+e-
 03051111 4137   00   2    -4.43544084E+00   # C10 e+e-
 03051111 4237   00   2    -3.20462200E-03   # C10' e+e-
 03051313 4133   00   0     1.61391155E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61443178E+00   # C9 mu+mu-
 03051313 4233   00   2     4.25110028E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43660365E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43544112E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.20462233E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50509934E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.94954750E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50509934E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.94954778E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50509935E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.94962710E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85851885E-07   # C7
     0305 4422   00   2     1.34629409E-05   # C7
     0305 4322   00   2     3.19742134E-06   # C7'
     0305 6421   00   0     3.30507657E-07   # C8
     0305 6421   00   2    -6.93185020E-06   # C8
     0305 6321   00   2     1.20114749E-06   # C8'
 03051111 4133   00   2     9.86587002E-08   # C9 e+e-
 03051111 4233   00   2     8.09816980E-06   # C9' e+e-
 03051111 4137   00   2     2.60834662E-06   # C10 e+e-
 03051111 4237   00   2    -6.10493042E-05   # C10' e+e-
 03051313 4133   00   2     9.86544635E-08   # C9 mu+mu-
 03051313 4233   00   2     8.09816709E-06   # C9' mu+mu-
 03051313 4137   00   2     2.60835020E-06   # C10 mu+mu-
 03051313 4237   00   2    -6.10493124E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.41062149E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.32391613E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.41061966E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.32391613E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.41010796E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.32391612E-05   # C11' nu_3 nu_3
