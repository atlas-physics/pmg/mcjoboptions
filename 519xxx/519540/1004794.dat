# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:30
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.91972808E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.47063074E+03  # scale for input parameters
    1    6.16133485E+01  # M_1
    2    1.33416214E+03  # M_2
    3    3.75377568E+03  # M_3
   11   -2.84685657E+03  # A_t
   12    8.92627227E+02  # A_b
   13   -3.58466573E+02  # A_tau
   23    3.95538504E+02  # mu
   25    4.67312034E+00  # tan(beta)
   26    1.46398704E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.65046053E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.28367911E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.25874898E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.47063074E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.47063074E+03  # (SUSY scale)
  1  1     8.57419045E-06   # Y_u(Q)^DRbar
  2  2     4.35568875E-03   # Y_c(Q)^DRbar
  3  3     1.03583108E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.47063074E+03  # (SUSY scale)
  1  1     8.05156565E-05   # Y_d(Q)^DRbar
  2  2     1.52979747E-03   # Y_s(Q)^DRbar
  3  3     7.98463013E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.47063074E+03  # (SUSY scale)
  1  1     1.40505708E-05   # Y_e(Q)^DRbar
  2  2     2.90521242E-03   # Y_mu(Q)^DRbar
  3  3     4.88608937E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.47063074E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.84685802E+03   # A_t(Q)^DRbar
Block Ad Q=  3.47063074E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.92627069E+02   # A_b(Q)^DRbar
Block Ae Q=  3.47063074E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -3.58466518E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.47063074E+03  # soft SUSY breaking masses at Q
   1    6.16133485E+01  # M_1
   2    1.33416214E+03  # M_2
   3    3.75377568E+03  # M_3
  21    1.84945065E+06  # M^2_(H,d)
  22    1.48867585E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.65046053E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.28367911E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.25874898E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18600946E+02  # h0
        35     1.46438126E+03  # H0
        36     1.46398704E+03  # A0
        37     1.47139536E+03  # H+
   1000001     1.00966018E+04  # ~d_L
   2000001     1.00727805E+04  # ~d_R
   1000002     1.00962597E+04  # ~u_L
   2000002     1.00760654E+04  # ~u_R
   1000003     1.00966025E+04  # ~s_L
   2000003     1.00727807E+04  # ~s_R
   1000004     1.00962604E+04  # ~c_L
   2000004     1.00760665E+04  # ~c_R
   1000005     2.78119127E+03  # ~b_1
   2000005     4.36182687E+03  # ~b_2
   1000006     2.78084417E+03  # ~t_1
   2000006     4.33151841E+03  # ~t_2
   1000011     1.00206144E+04  # ~e_L-
   2000011     1.00087344E+04  # ~e_R-
   1000012     1.00198753E+04  # ~nu_eL
   1000013     1.00206148E+04  # ~mu_L-
   2000013     1.00087351E+04  # ~mu_R-
   1000014     1.00198756E+04  # ~nu_muL
   1000015     1.00089302E+04  # ~tau_1-
   2000015     1.00207198E+04  # ~tau_2-
   1000016     1.00199771E+04  # ~nu_tauL
   1000021     4.20861023E+03  # ~g
   1000022     5.90228476E+01  # ~chi_10
   1000023     3.99959586E+02  # ~chi_20
   1000025     4.02615739E+02  # ~chi_30
   1000035     1.42572533E+03  # ~chi_40
   1000024     3.96955405E+02  # ~chi_1+
   1000037     1.42514012E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.02546553E-01   # alpha
Block Hmix Q=  3.47063074E+03  # Higgs mixing parameters
   1    3.95538504E+02  # mu
   2    4.67312034E+00  # tan[beta](Q)
   3    2.43122811E+02  # v(Q)
   4    2.14325805E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99269931E-01   # Re[R_st(1,1)]
   1  2     3.82047801E-02   # Re[R_st(1,2)]
   2  1    -3.82047801E-02   # Re[R_st(2,1)]
   2  2     9.99269931E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999976E-01   # Re[R_sb(1,1)]
   1  2     2.21018485E-04   # Re[R_sb(1,2)]
   2  1    -2.21018485E-04   # Re[R_sb(2,1)]
   2  2     9.99999976E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.68088627E-02   # Re[R_sta(1,1)]
   1  2     9.99858721E-01   # Re[R_sta(1,2)]
   2  1    -9.99858721E-01   # Re[R_sta(2,1)]
   2  2     1.68088627E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.92695875E-01   # Re[N(1,1)]
   1  2     3.68425534E-03   # Re[N(1,2)]
   1  3    -1.13760744E-01   # Re[N(1,3)]
   1  4     3.99977298E-02   # Re[N(1,4)]
   2  1     1.08896020E-01   # Re[N(2,1)]
   2  2     6.60993739E-02   # Re[N(2,2)]
   2  3    -7.01606469E-01   # Re[N(2,3)]
   2  4     7.01085510E-01   # Re[N(2,4)]
   3  1     5.18783147E-02   # Re[N(3,1)]
   3  2    -2.44175612E-02   # Re[N(3,2)]
   3  3    -7.02797462E-01   # Re[N(3,3)]
   3  4    -7.09075561E-01   # Re[N(3,4)]
   4  1     2.27955367E-03   # Re[N(4,1)]
   4  2    -9.97507434E-01   # Re[N(4,2)]
   4  3    -2.97083219E-02   # Re[N(4,3)]
   4  4     6.39620005E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.19752180E-02   # Re[U(1,1)]
   1  2     9.99118652E-01   # Re[U(1,2)]
   2  1     9.99118652E-01   # Re[U(2,1)]
   2  2     4.19752180E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.05085282E-02   # Re[V(1,1)]
   1  2     9.95895680E-01   # Re[V(1,2)]
   2  1     9.95895680E-01   # Re[V(2,1)]
   2  2     9.05085282E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02844363E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.85490181E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.18218639E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.68296616E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.39621212E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.76061080E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.86318684E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00991170E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.10968039E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05426432E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02877902E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.85424955E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.18374751E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.69924081E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.39622896E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.76052114E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.86904123E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.00987565E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.10966705E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05419159E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.12587359E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.66873828E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.61653239E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.08768469E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.58631413E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     9.40434897E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.10182931E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.40071581E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.73779262E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.50851960E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.71669179E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.99976319E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.04176225E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.03378781E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.39624240E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.00661780E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.55678005E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02433889E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.15880803E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01465673E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39625921E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.00650960E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.55667725E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02430256E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.17073627E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01458469E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.40100150E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.97611418E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     8.52780049E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.01409858E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.52148478E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.99435028E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.47332456E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.01242478E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.21455672E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.89726678E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.73555724E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.14844030E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.43490727E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.31096861E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.31780648E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.26528680E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.07763424E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.47333408E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.01242414E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.21874771E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.89724992E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.73560068E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.14843560E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.43831306E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.31092844E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.37372403E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.26527919E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.07758317E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.71184642E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.22431840E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.49087811E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.33806665E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.59129516E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.26603385E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.77430400E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     5.17201541E+00   # ~b_2
#    BR                NDA      ID1      ID2
     4.65178732E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.72671782E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.31955520E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.04668806E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.63259795E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.16977864E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.10896373E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.10475218E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     5.62207574E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     5.59352970E-04    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     5.64534649E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.92757948E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.71169031E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.06931637E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60145906E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.73540087E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.97654901E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.80701627E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.30061957E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.07762155E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.25711993E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.07731785E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.64542309E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.92752753E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.74464270E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.10306012E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60133015E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.73544400E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.97654547E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.83469889E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.30058196E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.07830722E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.25711199E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.07726670E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.74028983E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.59965246E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.58590527E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.66911293E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.69156090E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.11546463E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.68464709E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.36356352E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.27128346E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.20975759E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.96725461E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.00142096E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.13427679E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.94645904E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.27192223E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     7.18367922E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.72653967E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     3.58414833E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.19722308E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.82593585E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98378639E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.62135671E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.53808313E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.68494696E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.47451484E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.49791388E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46772104E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.36782739E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.89376910E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.85231846E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.27448216E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.46405116E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     2.29818225E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     7.61923809E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     8.25783510E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.41921357E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     7.39571741E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.51889971E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     8.53809685E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.63823435E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.33468202E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.33468202E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.73232550E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.07409886E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.31342829E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.94986906E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     8.62016566E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.01152067E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.71975716E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.11019281E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.11019281E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.89527838E+01   # ~g
#    BR                NDA      ID1      ID2
     2.46475022E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.46475022E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.50327695E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.50327695E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     3.11611327E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.45329295E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.45874383E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.44866542E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.44866542E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     2.81808693E-03   # Gamma(h0)
     2.61518334E-03   2        22        22   # BR(h0 -> photon photon)
     1.10362952E-03   2        22        23   # BR(h0 -> photon Z)
     1.62738094E-02   2        23        23   # BR(h0 -> Z Z)
     1.52719563E-01   2       -24        24   # BR(h0 -> W W)
     8.56180439E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.67008777E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.52211630E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.27117485E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.59938120E-07   2        -2         2   # BR(h0 -> Up up)
     3.10375954E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.62490671E-07   2        -1         1   # BR(h0 -> Down down)
     2.39606770E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.37079223E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.48557461E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     3.89249784E+00   # Gamma(HH)
     2.62949470E-06   2        22        22   # BR(HH -> photon photon)
     1.00258406E-06   2        22        23   # BR(HH -> photon Z)
     7.81562986E-04   2        23        23   # BR(HH -> Z Z)
     6.45825512E-04   2       -24        24   # BR(HH -> W W)
     3.62879011E-04   2        21        21   # BR(HH -> gluon gluon)
     1.23971136E-09   2       -11        11   # BR(HH -> Electron electron)
     5.51856103E-05   2       -13        13   # BR(HH -> Muon muon)
     1.59364263E-02   2       -15        15   # BR(HH -> Tau tau)
     3.55491802E-11   2        -2         2   # BR(HH -> Up up)
     6.89492695E-06   2        -4         4   # BR(HH -> Charm charm)
     4.84531965E-01   2        -6         6   # BR(HH -> Top top)
     9.94678601E-08   2        -1         1   # BR(HH -> Down down)
     3.59780443E-05   2        -3         3   # BR(HH -> Strange strange)
     9.47560013E-02   2        -5         5   # BR(HH -> Bottom bottom)
     1.34955487E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     9.16396711E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.11389022E-01   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.60206670E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.65210166E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.00007112E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.23250209E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.61289079E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.32698240E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.96840883E+00   # Gamma(A0)
     4.14234842E-06   2        22        22   # BR(A0 -> photon photon)
     1.68908052E-06   2        22        23   # BR(A0 -> photon Z)
     5.59666326E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.19657090E-09   2       -11        11   # BR(A0 -> Electron electron)
     5.32651974E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.53819470E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.33355331E-11   2        -2         2   # BR(A0 -> Up up)
     6.46501192E-06   2        -4         4   # BR(A0 -> Charm charm)
     4.82709467E-01   2        -6         6   # BR(A0 -> Top top)
     9.60106066E-08   2        -1         1   # BR(A0 -> Down down)
     3.47275224E-05   2        -3         3   # BR(A0 -> Strange strange)
     9.14742656E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     2.72440059E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.19789854E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.61747261E-01   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.03618298E-01   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.41097290E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.16731509E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.22570963E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.18129422E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.02088569E-03   2        23        25   # BR(A0 -> Z h0)
     9.51740569E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.59741193E+00   # Gamma(Hp)
     1.28102861E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     5.47679736E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.54914631E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.93810766E-08   2        -1         2   # BR(Hp -> Down up)
     1.49511980E-06   2        -3         2   # BR(Hp -> Strange up)
     9.60327299E-07   2        -5         2   # BR(Hp -> Bottom up)
     3.41056534E-07   2        -1         4   # BR(Hp -> Down charm)
     3.94632073E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.34491195E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.60860365E-05   2        -1         6   # BR(Hp -> Down top)
     7.86841283E-04   2        -3         6   # BR(Hp -> Strange top)
     6.48579837E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.24626230E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.91380846E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.45757581E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.30166576E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     8.97299561E-04   2        24        25   # BR(Hp -> W h0)
     5.01550685E-09   2        24        35   # BR(Hp -> W HH)
     6.59323413E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24190858E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.19138629E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.18380537E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00347142E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.23202026E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.57916265E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24190858E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.19138629E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.18380537E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99931711E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.82892055E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99931711E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.82892055E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27330189E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.83776247E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.24220882E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.82892055E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99931711E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.39392721E-04   # BR(b -> s gamma)
    2    1.59062567E-06   # BR(b -> s mu+ mu-)
    3    3.53012921E-05   # BR(b -> s nu nu)
    4    2.58606673E-15   # BR(Bd -> e+ e-)
    5    1.10473834E-10   # BR(Bd -> mu+ mu-)
    6    2.31265694E-08   # BR(Bd -> tau+ tau-)
    7    8.71263468E-14   # BR(Bs -> e+ e-)
    8    3.72203473E-09   # BR(Bs -> mu+ mu-)
    9    7.89474430E-07   # BR(Bs -> tau+ tau-)
   10    9.67402656E-05   # BR(B_u -> tau nu)
   11    9.99289907E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43421354E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94165458E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16320355E-03   # epsilon_K
   17    2.28171940E-15   # Delta(M_K)
   18    2.48316568E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29807178E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.62761153E-17   # Delta(g-2)_electron/2
   21    1.55091722E-12   # Delta(g-2)_muon/2
   22    4.38690997E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -3.28736304E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07753953E-01   # C7
     0305 4322   00   2    -3.96147858E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.19759972E-01   # C8
     0305 6321   00   2    -4.92792766E-04   # C8'
 03051111 4133   00   0     1.62127302E+00   # C9 e+e-
 03051111 4133   00   2     1.62188905E+00   # C9 e+e-
 03051111 4233   00   2     5.75650980E-06   # C9' e+e-
 03051111 4137   00   0    -4.44396512E+00   # C10 e+e-
 03051111 4137   00   2    -4.44708874E+00   # C10 e+e-
 03051111 4237   00   2    -4.25332293E-05   # C10' e+e-
 03051313 4133   00   0     1.62127302E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62188904E+00   # C9 mu+mu-
 03051313 4233   00   2     5.75650890E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44396512E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44708874E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.25332285E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50602789E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.20784026E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50602789E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.20784045E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50602789E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.20789388E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823964E-07   # C7
     0305 4422   00   2     2.13074655E-06   # C7
     0305 4322   00   2     1.65712967E-07   # C7'
     0305 6421   00   0     3.30483741E-07   # C8
     0305 6421   00   2     5.23090600E-09   # C8
     0305 6321   00   2     4.04386012E-08   # C8'
 03051111 4133   00   2     3.73865325E-07   # C9 e+e-
 03051111 4233   00   2     1.36207264E-07   # C9' e+e-
 03051111 4137   00   2     3.62792323E-07   # C10 e+e-
 03051111 4237   00   2    -1.00887955E-06   # C10' e+e-
 03051313 4133   00   2     3.73865275E-07   # C9 mu+mu-
 03051313 4233   00   2     1.36207263E-07   # C9' mu+mu-
 03051313 4137   00   2     3.62792373E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.00887955E-06   # C10' mu+mu-
 03051212 4137   00   2    -5.19455590E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.18411940E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.19455569E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.18411940E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.19449510E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.18411939E-07   # C11' nu_3 nu_3
