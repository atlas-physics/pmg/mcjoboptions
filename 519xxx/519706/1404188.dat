# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:10
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.18638030E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.19341005E+03  # scale for input parameters
    1   -2.13303675E+02  # M_1
    2   -1.20043702E+03  # M_2
    3    4.82513677E+03  # M_3
   11   -6.02134278E+03  # A_t
   12    9.53424052E+02  # A_b
   13    6.51126832E+02  # A_tau
   23   -2.29753565E+02  # mu
   25    1.13678543E+01  # tan(beta)
   26    1.41092171E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.25088349E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.40829838E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.89648330E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.19341005E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.19341005E+03  # (SUSY scale)
  1  1     8.41674982E-06   # Y_u(Q)^DRbar
  2  2     4.27570891E-03   # Y_c(Q)^DRbar
  3  3     1.01681098E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.19341005E+03  # (SUSY scale)
  1  1     1.92266298E-04   # Y_d(Q)^DRbar
  2  2     3.65305966E-03   # Y_s(Q)^DRbar
  3  3     1.90667920E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.19341005E+03  # (SUSY scale)
  1  1     3.35518750E-05   # Y_e(Q)^DRbar
  2  2     6.93746362E-03   # Y_mu(Q)^DRbar
  3  3     1.16676725E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.19341005E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.02134260E+03   # A_t(Q)^DRbar
Block Ad Q=  2.19341005E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     9.53424048E+02   # A_b(Q)^DRbar
Block Ae Q=  2.19341005E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     6.51126834E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.19341005E+03  # soft SUSY breaking masses at Q
   1   -2.13303675E+02  # M_1
   2   -1.20043702E+03  # M_2
   3    4.82513677E+03  # M_3
  21    1.86898357E+06  # M^2_(H,d)
  22    5.18089398E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.25088349E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.40829838E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.89648330E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24362964E+02  # h0
        35     1.41099178E+03  # H0
        36     1.41092171E+03  # A0
        37     1.41099157E+03  # H+
   1000001     1.00496915E+04  # ~d_L
   2000001     1.00284165E+04  # ~d_R
   1000002     1.00495403E+04  # ~u_L
   2000002     1.00315194E+04  # ~u_R
   1000003     1.00496937E+04  # ~s_L
   2000003     1.00284183E+04  # ~s_R
   1000004     1.00495425E+04  # ~c_L
   2000004     1.00315220E+04  # ~c_R
   1000005     2.17483339E+03  # ~b_1
   2000005     2.92137819E+03  # ~b_2
   1000006     2.02044464E+03  # ~t_1
   2000006     2.38118262E+03  # ~t_2
   1000011     1.00203324E+04  # ~e_L-
   2000011     1.00086900E+04  # ~e_R-
   1000012     1.00195829E+04  # ~nu_eL
   1000013     1.00203369E+04  # ~mu_L-
   2000013     1.00086986E+04  # ~mu_R-
   1000014     1.00195873E+04  # ~nu_muL
   1000015     1.00111295E+04  # ~tau_1-
   2000015     1.00215944E+04  # ~tau_2-
   1000016     1.00208355E+04  # ~nu_tauL
   1000021     5.16703149E+03  # ~g
   1000022     1.90098744E+02  # ~chi_10
   1000023     2.38229839E+02  # ~chi_20
   1000025     2.59237216E+02  # ~chi_30
   1000035     1.28780079E+03  # ~chi_40
   1000024     2.33414621E+02  # ~chi_1+
   1000037     1.28688654E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.50382455E-02   # alpha
Block Hmix Q=  2.19341005E+03  # Higgs mixing parameters
   1   -2.29753565E+02  # mu
   2    1.13678543E+01  # tan[beta](Q)
   3    2.43505046E+02  # v(Q)
   4    1.99070007E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     7.80237932E-01   # Re[R_st(1,1)]
   1  2     6.25482829E-01   # Re[R_st(1,2)]
   2  1    -6.25482829E-01   # Re[R_st(2,1)]
   2  2     7.80237932E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99997638E-01   # Re[R_sb(1,1)]
   1  2     2.17361907E-03   # Re[R_sb(1,2)]
   2  1    -2.17361907E-03   # Re[R_sb(2,1)]
   2  2    -9.99997638E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.78486243E-02   # Re[R_sta(1,1)]
   1  2     9.99612152E-01   # Re[R_sta(1,2)]
   2  1    -9.99612152E-01   # Re[R_sta(2,1)]
   2  2    -2.78486243E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     7.72466211E-01   # Re[N(1,1)]
   1  2    -3.29454178E-02   # Re[N(1,2)]
   1  3    -4.85985633E-01   # Re[N(1,3)]
   1  4     4.07465969E-01   # Re[N(1,4)]
   2  1    -6.34184435E-02   # Re[N(2,1)]
   2  2     3.44298098E-02   # Re[N(2,2)]
   2  3    -6.99360259E-01   # Re[N(2,3)]
   2  4    -7.11117372E-01   # Re[N(2,4)]
   3  1     6.31874580E-01   # Re[N(3,1)]
   3  2     4.83532453E-02   # Re[N(3,2)]
   3  3     5.23845248E-01   # Re[N(3,3)]
   3  4    -5.69194725E-01   # Re[N(3,4)]
   4  1     2.92723293E-03   # Re[N(4,1)]
   4  2    -9.97692914E-01   # Re[N(4,2)]
   4  3     1.73016933E-02   # Re[N(4,3)]
   4  4    -6.55814867E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.44632767E-02   # Re[U(1,1)]
   1  2    -9.99700729E-01   # Re[U(1,2)]
   2  1    -9.99700729E-01   # Re[U(2,1)]
   2  2     2.44632767E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.27806717E-02   # Re[V(1,1)]
   1  2     9.95686571E-01   # Re[V(1,2)]
   2  1     9.95686571E-01   # Re[V(2,1)]
   2  2    -9.27806717E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02411895E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     5.96853033E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.02124908E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.99117418E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40570099E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.52075689E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.66649890E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01132246E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.75128578E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06619972E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02603774E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     5.96670690E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.11286675E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.99017637E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.90349145E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40579750E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     4.52205922E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.66805103E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01111738E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.75102995E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06578619E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.57390025E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.51628803E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.68662525E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.71327774E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.27398708E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     4.89015571E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.48214489E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.43231074E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.80531942E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.46196858E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.17138812E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.95424714E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.35650413E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.95110592E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40574169E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     6.22515134E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.46222825E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.62049811E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02993203E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.39536611E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01692709E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40583817E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     6.22472685E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.46212854E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.62031942E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02972550E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.46312228E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01651736E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.43303776E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.10734106E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.43455589E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.57090588E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.97261533E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.41996958E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.90321746E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.32674546E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.71578372E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     5.15958232E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87072517E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.59088476E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.29837065E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.65223841E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.25330065E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.62678613E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.52997628E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.67850992E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.32680038E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.71713558E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.16119916E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.87060420E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.59095043E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.29946740E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.67546878E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.26630436E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.62669846E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.01067350E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.52995921E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.67842382E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.37709954E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.00940422E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.20684404E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.13731204E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.30692580E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     6.70495997E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.30489023E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.06645927E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     5.78579224E+00   # ~b_2
#    BR                NDA      ID1      ID2
     2.51510448E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.77540603E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.09951638E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.23223915E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.57746459E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.42262507E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.18254154E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.98059099E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     5.99631655E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     9.05133654E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.49784773E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.96982566E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.00089887E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.98593695E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.50241871E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.59095304E+02   # ~u_L
#    BR                NDA      ID1      ID2
     8.65171572E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.04481391E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.61064867E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.36064253E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.51767124E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.67813458E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.49792305E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.96991138E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.04184209E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.98616625E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.50226492E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.59101838E+02   # ~c_L
#    BR                NDA      ID1      ID2
     8.66245898E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.04690649E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.61056359E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.36537755E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.51765389E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.67804840E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.07222824E+01   # ~t_1
#    BR                NDA      ID1      ID2
     7.23848067E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.10834263E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.67925043E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.59930122E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.34913737E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     7.75201304E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     4.29003879E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.25757172E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.10861679E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.02235231E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.05808422E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.06514840E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.58868970E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.19115429E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.01694451E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.72914559E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.50536615E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.65912286E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.34001779E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32785708E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11334269E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11332268E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10545976E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.21393714E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.15044188E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.48682800E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.75498995E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.45944777E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.39499897E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.03190590E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.65215152E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     6.33673092E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.79217643E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.46761754E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     9.11463230E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.19068072E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18085919E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52668608E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52662593E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.41475889E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.44527338E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.44505427E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.38404170E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.04163195E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     6.78727968E-05   # chi^0_3
#    BR                NDA      ID1      ID2
     2.11309817E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     1.43135268E-02    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     6.87255502E-03    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     6.85556136E-03    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     8.80634880E-03    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     8.80626166E-03    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     8.64395016E-03    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     1.98266538E-03    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     1.98264588E-03    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     1.97710120E-03    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.17834185E-02    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     1.37373885E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     1.30927696E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     1.76141149E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     1.76100166E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     1.11280126E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     3.97502850E-03    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     3.97356732E-03    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     3.58619891E-03    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     2.35550347E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.37116966E-01    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.37116966E-01    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.35562825E-01    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.35562825E-01    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     4.57055421E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     4.57055421E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     4.57030654E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     4.57030654E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     4.47069695E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     4.47069695E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     1.30171646E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.32841732E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.32841732E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.76029976E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.74479361E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.96432279E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.79857380E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.20422684E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.17812962E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.19503135E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.41665459E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.84373834E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.59798738E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.59798738E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.78283746E+02   # ~g
#    BR                NDA      ID1      ID2
     1.33806671E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.33806671E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.35706479E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.35706479E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.36872983E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.36872983E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     9.35680380E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     9.35680380E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.32101189E-03   # Gamma(h0)
     2.69567999E-03   2        22        22   # BR(h0 -> photon photon)
     1.65426313E-03   2        22        23   # BR(h0 -> photon Z)
     2.98208453E-02   2        23        23   # BR(h0 -> Z Z)
     2.49489885E-01   2       -24        24   # BR(h0 -> W W)
     8.27614944E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.88503603E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.17294807E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.26531368E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.36421479E-07   2        -2         2   # BR(h0 -> Up up)
     2.64779923E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.65947405E-07   2        -1         1   # BR(h0 -> Down down)
     2.04689607E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.44024011E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.26613617E+00   # Gamma(HH)
     7.49214399E-07   2        22        22   # BR(HH -> photon photon)
     4.39564588E-07   2        22        23   # BR(HH -> photon Z)
     1.39880182E-04   2        23        23   # BR(HH -> Z Z)
     1.21670062E-04   2       -24        24   # BR(HH -> W W)
     3.37471842E-05   2        21        21   # BR(HH -> gluon gluon)
     6.07800847E-09   2       -11        11   # BR(HH -> Electron electron)
     2.70558263E-04   2       -13        13   # BR(HH -> Muon muon)
     7.81309353E-02   2       -15        15   # BR(HH -> Tau tau)
     4.92498983E-12   2        -2         2   # BR(HH -> Up up)
     9.55227661E-07   2        -4         4   # BR(HH -> Charm charm)
     7.02114543E-02   2        -6         6   # BR(HH -> Top top)
     4.92789975E-07   2        -1         1   # BR(HH -> Down down)
     1.78237817E-04   2        -3         3   # BR(HH -> Strange strange)
     4.71843225E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.12929765E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.00249567E-01   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.19124814E-01   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.08618421E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     7.29749892E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     6.39083863E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.28672321E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.15401997E-02   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     7.86087146E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.36308702E+00   # Gamma(A0)
     1.37683301E-06   2        22        22   # BR(A0 -> photon photon)
     6.08467759E-07   2        22        23   # BR(A0 -> photon Z)
     1.69640158E-04   2        21        21   # BR(A0 -> gluon gluon)
     5.89299605E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.62323325E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.57533883E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.77536196E-12   2        -2         2   # BR(A0 -> Up up)
     9.26104186E-07   2        -4         4   # BR(A0 -> Charm charm)
     7.27006129E-02   2        -6         6   # BR(A0 -> Top top)
     4.77794667E-07   2        -1         1   # BR(A0 -> Down down)
     1.72814101E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.57504654E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.54638437E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.40430852E-01   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.93714600E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.10279348E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.82621862E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     4.96430723E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.62259099E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.75039656E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.82713038E-04   2        23        25   # BR(A0 -> Z h0)
     3.06083485E-34   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.76265702E+00   # Gamma(Hp)
     6.89578372E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.94816289E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.33906057E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.86206876E-07   2        -1         2   # BR(Hp -> Down up)
     8.13156998E-06   2        -3         2   # BR(Hp -> Strange up)
     5.19921231E-06   2        -5         2   # BR(Hp -> Bottom up)
     7.63937702E-08   2        -1         4   # BR(Hp -> Down charm)
     1.76377404E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.28076598E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.75836848E-06   2        -1         6   # BR(Hp -> Down top)
     1.25802369E-04   2        -3         6   # BR(Hp -> Strange top)
     5.72803018E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.70196261E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.75516827E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.02533134E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.53520479E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.67890699E-04   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.39477114E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.29288634E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.29228111E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00046834E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.26991290E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.73825439E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.39477114E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.29288634E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.29228111E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99992692E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.30752149E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99992692E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.30752149E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.24113921E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.63209335E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.15873985E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.30752149E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99992692E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.48458293E-04   # BR(b -> s gamma)
    2    1.58826458E-06   # BR(b -> s mu+ mu-)
    3    3.52634128E-05   # BR(b -> s nu nu)
    4    2.30752743E-15   # BR(Bd -> e+ e-)
    5    9.85749803E-11   # BR(Bd -> mu+ mu-)
    6    2.06373531E-08   # BR(Bd -> tau+ tau-)
    7    7.78582462E-14   # BR(Bs -> e+ e-)
    8    3.32610272E-09   # BR(Bs -> mu+ mu-)
    9    7.05550160E-07   # BR(Bs -> tau+ tau-)
   10    9.63727348E-05   # BR(B_u -> tau nu)
   11    9.95493454E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42363254E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93770406E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15905331E-03   # epsilon_K
   17    2.28167636E-15   # Delta(M_K)
   18    2.48128499E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29335255E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.02567786E-16   # Delta(g-2)_electron/2
   21    4.38509373E-12   # Delta(g-2)_muon/2
   22    1.24042849E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.54354312E-08   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.18137960E-01   # C7
     0305 4322   00   2    -4.53401138E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.25338303E-01   # C8
     0305 6321   00   2    -5.21457549E-04   # C8'
 03051111 4133   00   0     1.60518840E+00   # C9 e+e-
 03051111 4133   00   2     1.60560643E+00   # C9 e+e-
 03051111 4233   00   2     2.35776506E-05   # C9' e+e-
 03051111 4137   00   0    -4.42788050E+00   # C10 e+e-
 03051111 4137   00   2    -4.42727931E+00   # C10 e+e-
 03051111 4237   00   2    -1.79641942E-04   # C10' e+e-
 03051313 4133   00   0     1.60518840E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60560642E+00   # C9 mu+mu-
 03051313 4233   00   2     2.35776166E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42788050E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42727932E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.79641911E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50522085E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.90332783E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50522085E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.90332855E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50522085E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.90353210E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85813480E-07   # C7
     0305 4422   00   2     2.77294294E-06   # C7
     0305 4322   00   2    -6.48826389E-07   # C7'
     0305 6421   00   0     3.30474761E-07   # C8
     0305 6421   00   2     3.01812482E-06   # C8
     0305 6321   00   2    -2.49673827E-07   # C8'
 03051111 4133   00   2    -5.26608694E-07   # C9 e+e-
 03051111 4233   00   2     6.19544464E-07   # C9' e+e-
 03051111 4137   00   2     6.90229979E-06   # C10 e+e-
 03051111 4237   00   2    -4.72501091E-06   # C10' e+e-
 03051313 4133   00   2    -5.26608889E-07   # C9 mu+mu-
 03051313 4233   00   2     6.19544450E-07   # C9' mu+mu-
 03051313 4137   00   2     6.90230005E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.72501095E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.47515988E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.02667406E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.47515987E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.02667406E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.47515713E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.02667406E-06   # C11' nu_3 nu_3
