# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:19
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.05667366E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.17789315E+03  # scale for input parameters
    1    6.48211362E+01  # M_1
    2    8.35423446E+02  # M_2
    3    4.28316472E+03  # M_3
   11   -7.53240906E+03  # A_t
   12    6.57507971E+02  # A_b
   13    8.09086404E+02  # A_tau
   23   -2.68003494E+02  # mu
   25    5.00324909E+01  # tan(beta)
   26    2.99765879E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.10036355E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.22026534E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.60416866E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.17789315E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.17789315E+03  # (SUSY scale)
  1  1     8.38604666E-06   # Y_u(Q)^DRbar
  2  2     4.26011171E-03   # Y_c(Q)^DRbar
  3  3     1.01310180E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.17789315E+03  # (SUSY scale)
  1  1     8.43120497E-04   # Y_d(Q)^DRbar
  2  2     1.60192894E-02   # Y_s(Q)^DRbar
  3  3     8.36111337E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.17789315E+03  # (SUSY scale)
  1  1     1.47130692E-04   # Y_e(Q)^DRbar
  2  2     3.04219607E-02   # Y_mu(Q)^DRbar
  3  3     5.11647333E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.17789315E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.53240894E+03   # A_t(Q)^DRbar
Block Ad Q=  4.17789315E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     6.57507969E+02   # A_b(Q)^DRbar
Block Ae Q=  4.17789315E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.09086409E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.17789315E+03  # soft SUSY breaking masses at Q
   1    6.48211362E+01  # M_1
   2    8.35423446E+02  # M_2
   3    4.28316472E+03  # M_3
  21    9.12731019E+06  # M^2_(H,d)
  22    2.96598001E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.10036355E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.22026534E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.60416866E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28670911E+02  # h0
        35     2.99605014E+03  # H0
        36     2.99765879E+03  # A0
        37     2.99822463E+03  # H+
   1000001     1.01048024E+04  # ~d_L
   2000001     1.00803320E+04  # ~d_R
   1000002     1.01044422E+04  # ~u_L
   2000002     1.00829168E+04  # ~u_R
   1000003     1.01048047E+04  # ~s_L
   2000003     1.00803359E+04  # ~s_R
   1000004     1.01044444E+04  # ~c_L
   2000004     1.00829172E+04  # ~c_R
   1000005     2.74357165E+03  # ~b_1
   2000005     4.16283190E+03  # ~b_2
   1000006     4.05940640E+03  # ~t_1
   2000006     4.29983832E+03  # ~t_2
   1000011     1.00209637E+04  # ~e_L-
   2000011     1.00094414E+04  # ~e_R-
   1000012     1.00201995E+04  # ~nu_eL
   1000013     1.00209746E+04  # ~mu_L-
   2000013     1.00094613E+04  # ~mu_R-
   1000014     1.00202099E+04  # ~nu_muL
   1000015     1.00150757E+04  # ~tau_1-
   2000015     1.00241194E+04  # ~tau_2-
   1000016     1.00231783E+04  # ~nu_tauL
   1000021     4.76791671E+03  # ~g
   1000022     6.38703922E+01  # ~chi_10
   1000023     2.84941291E+02  # ~chi_20
   1000025     2.91442596E+02  # ~chi_30
   1000035     9.09918393E+02  # ~chi_40
   1000024     2.83699341E+02  # ~chi_1+
   1000037     9.09812231E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.98205822E-02   # alpha
Block Hmix Q=  4.17789315E+03  # Higgs mixing parameters
   1   -2.68003494E+02  # mu
   2    5.00324909E+01  # tan[beta](Q)
   3    2.42874954E+02  # v(Q)
   4    8.98595822E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     7.67755231E-01   # Re[R_st(1,1)]
   1  2     6.40743244E-01   # Re[R_st(1,2)]
   2  1    -6.40743244E-01   # Re[R_st(2,1)]
   2  2     7.67755231E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -2.96173411E-03   # Re[R_sb(1,1)]
   1  2     9.99995614E-01   # Re[R_sb(1,2)]
   2  1    -9.99995614E-01   # Re[R_sb(2,1)]
   2  2    -2.96173411E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.39161138E-01   # Re[R_sta(1,1)]
   1  2     9.90269750E-01   # Re[R_sta(1,2)]
   2  1    -9.90269750E-01   # Re[R_sta(2,1)]
   2  2    -1.39161138E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.86300730E-01   # Re[N(1,1)]
   1  2    -2.76930493E-03   # Re[N(1,2)]
   1  3    -1.61548517E-01   # Re[N(1,3)]
   1  4    -3.32457221E-02   # Re[N(1,4)]
   2  1     1.38130009E-01   # Re[N(2,1)]
   2  2     8.87561445E-02   # Re[N(2,2)]
   2  3     6.98453078E-01   # Re[N(2,3)]
   2  4     6.96567115E-01   # Re[N(2,4)]
   3  1     9.00250627E-02   # Re[N(3,1)]
   3  2    -4.89996141E-02   # Re[N(3,2)]
   3  3     6.96603052E-01   # Re[N(3,3)]
   3  4    -7.10097679E-01   # Re[N(3,4)]
   4  1     5.14385015E-03   # Re[N(4,1)]
   4  2    -9.94843563E-01   # Re[N(4,2)]
   4  3     2.84528138E-02   # Re[N(4,3)]
   4  4     9.72124612E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.02942904E-02   # Re[U(1,1)]
   1  2    -9.99187855E-01   # Re[U(1,2)]
   2  1     9.99187855E-01   # Re[U(2,1)]
   2  2    -4.02942904E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.37626722E-01   # Re[V(1,1)]
   1  2     9.90484167E-01   # Re[V(1,2)]
   2  1     9.90484167E-01   # Re[V(2,1)]
   2  2     1.37626722E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02875439E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.72830671E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.90513944E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     8.09182190E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42748503E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.48767354E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     8.15084864E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.99127967E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.00175533E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06842426E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06556258E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.65858599E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.06849256E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.79546357E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.62646771E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42932867E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.48010334E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.76904779E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.25595864E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.98743495E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.00046430E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06060364E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.55261693E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.26721044E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.70706071E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.59565025E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.22218915E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.33228189E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.55748189E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.94116408E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.73023715E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.36251457E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.34925748E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.17897992E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.04033402E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.41582097E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42753147E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.66705983E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.90759111E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02417630E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.16851404E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.96249969E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42937489E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.65589119E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.90384431E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02027935E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.29566258E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.95483700E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.94912779E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.34959872E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.13014042E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.21557197E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.75514653E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.37251420E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.85107306E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.12845212E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.21137272E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.88399739E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.14677819E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.30279898E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.97488994E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.05602906E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.05863291E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.34593284E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.42146962E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.84126224E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.85210182E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.12848932E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.72605614E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.45225468E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.05722580E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.88190997E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.14733089E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.30477636E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.38222842E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.46116837E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.05800654E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.40384946E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.42134327E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.84056107E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.58919924E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.28649699E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.40399122E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.38546401E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.86238638E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.87336344E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.66924078E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.92651571E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.07796573E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.45613755E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.44866146E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.57489226E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.27478091E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.80518125E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.40917019E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.00918471E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.85215122E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.02258728E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.36078347E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     8.54009709E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.62728401E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.55174260E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.14660761E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.16201291E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     9.28236152E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.03262056E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.73695302E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.39680066E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.84088011E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.02266006E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.36072108E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     8.57509395E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.66371214E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.55160440E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.14715987E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.16182563E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     9.31027149E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.03199589E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.82012682E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.39667681E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.84017884E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.82132392E+02   # ~t_1
#    BR                NDA      ID1      ID2
     7.19172650E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.56751899E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.06347645E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.19766380E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.39185089E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.74283210E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.04076878E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.96527094E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.05921505E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.98399132E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.98953995E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.56662170E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.11637031E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.07411737E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.69827880E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.38011469E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.10055583E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.03185139E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.46028419E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     3.34712650E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.95788166E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.68998542E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99933288E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     7.59079328E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.39851942E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.51850909E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.54086696E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47641482E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37378733E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.14273525E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.20482722E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.44647405E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.35904428E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.38068639E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.61926109E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.37630843E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.19309282E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.80656440E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     8.74828308E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.19312970E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.19312970E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.03036733E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.71026404E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.13041039E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.58062901E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.74824125E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.00233949E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     7.10058843E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.78153512E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.78153512E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.78342371E+01   # ~g
#    BR                NDA      ID1      ID2
     1.49251448E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.49251448E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     4.39179805E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.39179805E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.61562734E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     3.61562734E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.59450616E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.59450616E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.53933411E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.53933411E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     4.11992465E-03   # Gamma(h0)
     2.49603154E-03   2        22        22   # BR(h0 -> photon photon)
     1.93000765E-03   2        22        23   # BR(h0 -> photon Z)
     4.06052572E-02   2        23        23   # BR(h0 -> Z Z)
     3.13182183E-01   2       -24        24   # BR(h0 -> W W)
     7.43376282E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.39216162E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.95372920E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.63370069E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.24043849E-07   2        -2         2   # BR(h0 -> Up up)
     2.40760013E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.05789127E-07   2        -1         1   # BR(h0 -> Down down)
     1.82932120E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.86247587E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     4.09357954E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.22532193E+02   # Gamma(HH)
     3.59752469E-08   2        22        22   # BR(HH -> photon photon)
     6.72186283E-08   2        22        23   # BR(HH -> photon Z)
     9.84856768E-08   2        23        23   # BR(HH -> Z Z)
     2.97884050E-08   2       -24        24   # BR(HH -> W W)
     8.22670190E-06   2        21        21   # BR(HH -> gluon gluon)
     8.58956730E-09   2       -11        11   # BR(HH -> Electron electron)
     3.82447418E-04   2       -13        13   # BR(HH -> Muon muon)
     1.10455413E-01   2       -15        15   # BR(HH -> Tau tau)
     2.18534680E-14   2        -2         2   # BR(HH -> Up up)
     4.23931154E-09   2        -4         4   # BR(HH -> Charm charm)
     3.28846223E-04   2        -6         6   # BR(HH -> Top top)
     6.42308484E-07   2        -1         1   # BR(HH -> Down down)
     2.32307860E-04   2        -3         3   # BR(HH -> Strange strange)
     6.10576766E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.25860894E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.10891816E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.10891816E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.49733492E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.59979749E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.42217685E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.17294127E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.88883278E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.34300862E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.50764465E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.94995433E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     9.14422305E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.09634880E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.26136655E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     8.04929533E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.15827335E+02   # Gamma(A0)
     8.46022021E-10   2        22        22   # BR(A0 -> photon photon)
     1.48856298E-08   2        22        23   # BR(A0 -> photon Z)
     1.32875610E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.39570743E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.73817500E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.07963166E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.09497762E-14   2        -2         2   # BR(A0 -> Up up)
     4.06383791E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.20164384E-04   2        -6         6   # BR(A0 -> Top top)
     6.27778459E-07   2        -1         1   # BR(A0 -> Down down)
     2.27051335E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.96778665E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.54582062E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.58129116E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     8.58129116E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.96493373E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.66735773E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.40249054E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.34205230E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.07296003E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.37734162E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.20316855E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.31470729E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.08575274E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.19320876E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.50135409E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.69424147E-07   2        23        25   # BR(A0 -> Z h0)
     1.02159491E-13   2        23        35   # BR(A0 -> Z HH)
     2.26023999E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.32614937E+02   # Gamma(Hp)
     9.56009577E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.08723953E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.15610375E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.16998077E-07   2        -1         2   # BR(Hp -> Down up)
     1.04138080E-05   2        -3         2   # BR(Hp -> Strange up)
     6.62736374E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.90222129E-08   2        -1         4   # BR(Hp -> Down charm)
     2.22343388E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.28068045E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.27883098E-08   2        -1         6   # BR(Hp -> Down top)
     8.26058830E-07   2        -3         6   # BR(Hp -> Strange top)
     6.26045515E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.59628346E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.58175313E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.50877096E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     7.58222828E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.98540347E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.41060009E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     7.55860935E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.01921301E-09   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.48153375E-07   2        24        25   # BR(Hp -> W h0)
     4.98985527E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.83679565E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.50326647E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.50325015E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000652E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.92960954E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.99480652E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.83679565E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.50326647E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.50325015E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999973E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.68202991E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999973E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.68202991E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26373884E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.33076609E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.43242833E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.68202991E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999973E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.40630956E-04   # BR(b -> s gamma)
    2    1.58882203E-06   # BR(b -> s mu+ mu-)
    3    3.52634735E-05   # BR(b -> s nu nu)
    4    8.65058064E-16   # BR(Bd -> e+ e-)
    5    3.69499881E-11   # BR(Bd -> mu+ mu-)
    6    7.49094175E-09   # BR(Bd -> tau+ tau-)
    7    3.00337747E-14   # BR(Bs -> e+ e-)
    8    1.28291373E-09   # BR(Bs -> mu+ mu-)
    9    2.64767969E-07   # BR(Bs -> tau+ tau-)
   10    9.51547647E-05   # BR(B_u -> tau nu)
   11    9.82912290E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41793985E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93311007E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15695726E-03   # epsilon_K
   17    2.28165636E-15   # Delta(M_K)
   18    2.48071660E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29221927E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.81044706E-16   # Delta(g-2)_electron/2
   21   -1.62910392E-11   # Delta(g-2)_muon/2
   22   -4.62214846E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.70922327E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.11999833E-01   # C7
     0305 4322   00   2    -2.73897158E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.11445292E-01   # C8
     0305 6321   00   2    -2.18512921E-04   # C8'
 03051111 4133   00   0     1.62908529E+00   # C9 e+e-
 03051111 4133   00   2     1.62933109E+00   # C9 e+e-
 03051111 4233   00   2     6.50801127E-04   # C9' e+e-
 03051111 4137   00   0    -4.45177740E+00   # C10 e+e-
 03051111 4137   00   2    -4.45136021E+00   # C10 e+e-
 03051111 4237   00   2    -4.80051132E-03   # C10' e+e-
 03051313 4133   00   0     1.62908529E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62933085E+00   # C9 mu+mu-
 03051313 4233   00   2     6.50800151E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45177740E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45136046E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.80051125E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50526173E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.03749158E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50526173E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.03749174E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50526174E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.03753724E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85798581E-07   # C7
     0305 4422   00   2    -1.44083922E-05   # C7
     0305 4322   00   2    -2.50456394E-06   # C7'
     0305 6421   00   0     3.30461999E-07   # C8
     0305 6421   00   2     1.24677693E-05   # C8
     0305 6321   00   2    -8.22624176E-07   # C8'
 03051111 4133   00   2     2.81819284E-07   # C9 e+e-
 03051111 4233   00   2     1.27787362E-05   # C9' e+e-
 03051111 4137   00   2     1.68031311E-06   # C10 e+e-
 03051111 4237   00   2    -9.42632709E-05   # C10' e+e-
 03051313 4133   00   2     2.81812564E-07   # C9 mu+mu-
 03051313 4233   00   2     1.27787321E-05   # C9' mu+mu-
 03051313 4137   00   2     1.68031962E-06   # C10 mu+mu-
 03051313 4237   00   2    -9.42632831E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.31513754E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.03722793E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.31513437E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.03722793E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.31425787E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.03722793E-05   # C11' nu_3 nu_3
