###########################################
# MadGraph5, hh, hh->bb~bb~                 #
###########################################
from MadGraphControl.MadGraphUtils import *

# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
sf = 1.1
nevents = runArgs.maxEvents*sf if runArgs.maxEvents>0 else sf*evgenConfig.nEventsPerJob
#Generate resonant VBF-HH process with MadGraph
#Write extra pdgid file
pdg_file = open("pdgid_extras.txt", "w")
pdg_file.write("9000006\n-9000006")
pdg_file.close()

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}

parameters['MASS']={'Ms0': '2000'}

parameters['DECAY'] = {"Ws0": '400' }



extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'fixed_ren_scale':'F',
           'fixed_fac_scale':'F',
           'nevents':int(nevents)}


process="""
import model CHresonance_neutral_scalar_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > s0 > h h j j $$ z w+ w- / a j QED=4
output -f"""

process_dir=str(new_process(process))

#Fetch default LO run_card.dat and set parameters
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

modify_param_card(process_dir=process_dir,params=parameters)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

evgenConfig.generators = ["MadGraph", "Pythia8"]


#### Shower 
evgenConfig.description = 'qq->eta->hh, h->b b~, meta= 2000 GeV'
evgenConfig.keywords+=['VBF','hh']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['Zhen Wang <zhen.wang@cern.ch>','Ashutosh Kotwal <ashutosh.kotwal@duke.edu>','Shu Li <Shu.Li@cern.ch>']
# comment out inputfilecheck, to allow running on multiple inputs
#evgenConfig.inputfilecheck = 'VBFH125_sbi_4l_m4l130'
#evgenConfig.minevents = 1000

# Pythia8 showering
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [ "25:oneChannel = on 1.0 100 5 -5"]

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Filter for bbbb
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])

filtSeq.Expression = "HbbFilter"

