selected_operators = ['cQl111','cQl122','cQl133','cQl311','cQl322','cQl333','cte11','cte22','cte33','cQe11','cQe22','cQe33','ctl11','ctl22','ctl33','cleQt1Re11','cleQt1Re22','cleQt1Re33','cleQt3Re11','cleQt3Re22','cleQt3Re33']

# Note: the 4-quark operators ['cQj18','cQj38','ctu8','ctd8','cQj11','cQj31','ctj8','ctj1','ctu1','ctd1','cQd1','cQd8','cQu1','cQu8','cQQ1','cQQ8','cQt1','cQt8']
#       require QED=4, which includes SM EW corrections and is too CPU-intensive to be run without a gridpack

process_definition = 'generate p p > t t~ l+ l- QCD=2 QED=4 NP=1, (t > w+ b NP=0,  w+ > wdec wdec NP=0), (t~ > w- b~ NP=0, w- > wdec wdec NP=0) @0 NPprop=0 SMHLOOP=0'

fixed_scale = 436.0 # ~ 2*m(top)+m(Z)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+ll, top model, inclusive, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_topX_reweighted_150.py")
