# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

# Imports needed for manipulation of DHELAS in toponium PROC_* directory
import os, re
mglog = Logging.logging.getLogger('MadGraphUtils')

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents * safety if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob * safety

# Here is where we define the commands that will be passed to MadGraph

# Import the toponium model
process = """
import model toponium_eta
"""

# Define some multi-particle represenations
process += """
define charm = c c~
define up = u u~
define q = u u~ d d~ c c~ s s~
define e = e- e+
define mu = mu- mu+
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""

# Define the physics process to be simulated
process += """
generate g g > eta > W+ b W- b~, (W+ > l+ vl), (W- > l- vl~)
"""

# This defines the MadGraph outputs
process += """
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process)
settings = {'ickkw': 0, 'nevents':nevents, 'madspin':'off'}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Set some values in the param card
params = dict()
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict() # ensure NLO width for top by setting its BR
params['DECAY']['6'] =  'DECAY   6   1.320000e+00'
#params['DECAY']['6'] =  '''DECAY   6   1.320000e+00
#   1.000000e+00   2    5  24'''
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'

# toponium settings
params['mass']['6001'] = '3.43e+02' # 3.429000e+02
params['DECAY']['6001'] = '2.80e+00'
params['yukawa']['6000'] = '1.00e+00' # cy
params['yukawa']['6001'] = '1.570796e+00' # alphay (=pseudoscalar)


# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir,params=params)

# Make changes in DHELAS needed for toponium generation
mglog.info('Modifying DHELAS files as required for toponium process')

def edit_file_in_place(file_path):
    # Define the patterns and replacements
    patterns_replacements = {
        r'SQRT\(DBLE\((.*?)\)\)': r'SQRT(\1)',
        r'LOG\(DBLE\((.*?)\)\)': r'LOG(\1)'
    }
    # Read the content of the file
    with open(file_path, 'r') as file:
        content = file.read()
    # Perform the replacements
    for pattern, replacement in patterns_replacements.items():
        content = re.sub(pattern, replacement, content)
    # Write the modified content back to the file
    with open(file_path, 'w') as file:
        file.write(content)
    mglog.info(f"#### Edited file {file_path} ####")

for file_to_change in ["GGeta3_3.f", "GGeta5_3.f"]:
    edit_file_in_place( process_dir + "/Source/DHELAS/" + file_to_change )

# Recompile DHELAS
currdir=os.getcwd() # store current directory so we can return to it after DHELAS compilation
os.chdir('%s/Source/DHELAS/'%process_dir)
os.system('make')
os.chdir(currdir)

# Print the cards
print_cards()

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Toponium production'
evgenConfig.generators += ['MadGraph', 'Pythia8']
evgenConfig.keywords   += ['SM', 'top']
evgenConfig.contact     = ['katharina.behr@cern.ch' ]

arrange_output(process_dir=process_dir, runArgs=runArgs)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
whitelist = open("pdgid_extras.txt", "w") # hack needed for r21 production
whitelist.write("6001\n-6001\n")
whitelist.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

