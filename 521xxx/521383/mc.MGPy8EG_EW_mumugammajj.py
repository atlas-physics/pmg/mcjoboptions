#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "MadGraph+Pythia8 EW production of gamma+jj at LO"
evgenConfig.keywords = ["SM"]
evgenConfig.contact = ["ana.cueto@cern.ch"]
evgenConfig.nEventsPerJob = 10000
#evgenConfig.generators = ['MG', 'Py8']


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *


nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 3*evgenConfig.nEventsPerJob

gridpack_mode=False

process = """
set complex_mass_scheme
import model loop_sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate  p p > mu+ mu- a j j QCD=0 
output -f
"""

process_dir = new_process(process)


settings = {
           'maxjetflavor'  : 5,
           'ptj'           : 10.0,
           'ptb'           : 10.0,
           'drll'          : "0.1",
           'drjl'          : "0.1",
           'drjj'          : "0.1",
           'etaj'          : -1,
           'draa'          :"0.0",
           'dral'          :"0.1",
           'etaa'          : '3.0',
           'ptgmin'        : 10.0,
           'epsgamma'      :'0.1',
           'R0gamma'       :'0.1',
           'xn'            :'2',
           'isoEM'         :'T',
           'bwcutoff'      :'15',
           'nevents':int(nevents)
    }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)


############################
# Shower JOs will go here
# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")  
include("Pythia8_i/Pythia8_MadGraph.py")


genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil = on"]
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
