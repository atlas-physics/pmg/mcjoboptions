from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

#--------------------------------------------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------------------------------------------
evgenConfig.description = 'MadGraph configuration to produce a flat-pT Zprime sample, showered with Pythia8'
evgenConfig.keywords = ['Zprime','jets']
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.contact = ['yi.yu@cern.ch']
evgenConfig.nEventsPerJob = 10000


#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
nevents = 1.1 * runArgs.maxEvents if runArgs.maxEvents > 0 else 11000



#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
gridpack_mode=False

if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'nevents':int(nevents)
            }


#---------------------------------------------------------------------------------------------------
# Generating Z' process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process_def = """
import model Zp_flat_pT
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > v1 > all all
output -f
"""
process_dir = new_process(process_def)


#---------------------------------------------------------------------------------------------------
# Build a new run_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#--------------------------------------------------------------------------------------------------- 
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)


#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------  
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)
runArgs.inputGeneratorFile = outputDS+'.events'


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

if hasattr(testSeq, "TestHepMC"):
  testSeq.TestHepMC.MaxTransVtxDisp = 1300
  testSeq.TestHepMC.MaxVtxDisp = 1300
 