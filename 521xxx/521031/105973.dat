# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  17:54
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.31470836E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.89863591E+03  # scale for input parameters
    1    2.07983320E+02  # M_1
    2    1.62541438E+03  # M_2
    3    3.02214694E+03  # M_3
   11   -2.48945922E+03  # A_t
   12   -3.75426410E+00  # A_b
   13   -1.24429513E+03  # A_tau
   23    2.51449560E+02  # mu
   25    2.21643572E+01  # tan(beta)
   26    1.92604868E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.07562352E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.61420345E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.11753498E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.89863591E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.89863591E+03  # (SUSY scale)
  1  1     8.39290136E-06   # Y_u(Q)^DRbar
  2  2     4.26359389E-03   # Y_c(Q)^DRbar
  3  3     1.01392990E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.89863591E+03  # (SUSY scale)
  1  1     3.73807066E-04   # Y_d(Q)^DRbar
  2  2     7.10233426E-03   # Y_s(Q)^DRbar
  3  3     3.70699476E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.89863591E+03  # (SUSY scale)
  1  1     6.52320666E-05   # Y_e(Q)^DRbar
  2  2     1.34879225E-02   # Y_mu(Q)^DRbar
  3  3     2.26844667E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.89863591E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.48945988E+03   # A_t(Q)^DRbar
Block Ad Q=  3.89863591E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -3.75426358E+00   # A_b(Q)^DRbar
Block Ae Q=  3.89863591E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.24429503E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.89863591E+03  # soft SUSY breaking masses at Q
   1    2.07983320E+02  # M_1
   2    1.62541438E+03  # M_2
   3    3.02214694E+03  # M_3
  21   -1.00784656E+05  # M^2_(H,d)
  22    2.47316054E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.07562352E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.61420345E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.11753498E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21855705E+02  # h0
        35     1.92932494E+02  # H0
        36     1.92604868E+02  # A0
        37     2.14802051E+02  # H+
   1000001     1.01088641E+04  # ~d_L
   2000001     1.00849504E+04  # ~d_R
   1000002     1.01085012E+04  # ~u_L
   2000002     1.00881532E+04  # ~u_R
   1000003     1.01088654E+04  # ~s_L
   2000003     1.00849522E+04  # ~s_R
   1000004     1.01085025E+04  # ~c_L
   2000004     1.00881539E+04  # ~c_R
   1000005     2.27771935E+03  # ~b_1
   2000005     4.14720503E+03  # ~b_2
   1000006     3.65885301E+03  # ~t_1
   2000006     4.15413299E+03  # ~t_2
   1000011     1.00204727E+04  # ~e_L-
   2000011     1.00089092E+04  # ~e_R-
   1000012     1.00197080E+04  # ~nu_eL
   1000013     1.00204775E+04  # ~mu_L-
   2000013     1.00089184E+04  # ~mu_R-
   1000014     1.00197127E+04  # ~nu_muL
   1000015     1.00115317E+04  # ~tau_1-
   2000015     1.00218700E+04  # ~tau_2-
   1000016     1.00210684E+04  # ~nu_tauL
   1000021     3.48854036E+03  # ~g
   1000022     1.95485237E+02  # ~chi_10
   1000023     2.71062988E+02  # ~chi_20
   1000025     2.80674598E+02  # ~chi_30
   1000035     1.72594647E+03  # ~chi_40
   1000024     2.66925065E+02  # ~chi_1+
   1000037     1.72591129E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.75967892E-02   # alpha
Block Hmix Q=  3.89863591E+03  # Higgs mixing parameters
   1    2.51449560E+02  # mu
   2    2.21643572E+01  # tan[beta](Q)
   3    2.42975361E+02  # v(Q)
   4    3.70966352E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.97802859E-02   # Re[R_st(1,1)]
   1  2     9.95009495E-01   # Re[R_st(1,2)]
   2  1    -9.95009495E-01   # Re[R_st(2,1)]
   2  2     9.97802859E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.03663460E-03   # Re[R_sb(1,1)]
   1  2     9.99999463E-01   # Re[R_sb(1,2)]
   2  1    -9.99999463E-01   # Re[R_sb(2,1)]
   2  2     1.03663460E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     5.84774948E-02   # Re[R_sta(1,1)]
   1  2     9.98288727E-01   # Re[R_sta(1,2)]
   2  1    -9.98288727E-01   # Re[R_sta(2,1)]
   2  2     5.84774948E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.03364620E-01   # Re[N(1,1)]
   1  2     1.41914164E-02   # Re[N(1,2)]
   1  3    -3.42705862E-01   # Re[N(1,3)]
   1  4     2.57456131E-01   # Re[N(1,4)]
   2  1    -6.29536420E-02   # Re[N(2,1)]
   2  2     2.73505286E-02   # Re[N(2,2)]
   2  3     7.00626017E-01   # Re[N(2,3)]
   2  4     7.10219665E-01   # Re[N(2,4)]
   3  1    -4.24225291E-01   # Re[N(3,1)]
   3  2    -3.76939975E-02   # Re[N(3,2)]
   3  3     6.25771037E-01   # Re[N(3,3)]
   3  4    -6.53469719E-01   # Re[N(3,4)]
   4  1     1.45062842E-03   # Re[N(4,1)]
   4  2    -9.98814154E-01   # Re[N(4,2)]
   4  3    -9.29982976E-03   # Re[N(4,3)]
   4  4     4.77670808E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.31534026E-02   # Re[U(1,1)]
   1  2     9.99913490E-01   # Re[U(1,2)]
   2  1     9.99913490E-01   # Re[U(2,1)]
   2  2     1.31534026E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.75768437E-02   # Re[V(1,1)]
   1  2     9.97714072E-01   # Re[V(1,2)]
   2  1     9.97714072E-01   # Re[V(2,1)]
   2  2     6.75768437E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02452814E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.16188964E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.96096762E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.79848069E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37167894E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.04937484E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.24300430E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01561168E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.11103711E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05391269E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03176723E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.15099961E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.30814179E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.79870965E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.18776302E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.37204181E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.05061597E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.42223759E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.25274626E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01481597E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.11074380E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05231483E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.09935146E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.99181725E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     7.20664368E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.79352531E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.69772607E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.44294283E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.40729775E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.47167894E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.17528638E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.49076297E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.02960563E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.80306438E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.62709257E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37171593E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.92874756E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.19386897E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.14588554E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02464453E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.93227104E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02663076E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37207876E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.92665463E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.19355383E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.14558307E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02384630E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.19536878E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02504071E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47436543E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.37772880E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.11089968E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.06625095E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.81448902E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     7.21993798E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.60801021E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.22557693E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.37490099E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.62510313E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.90964120E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.46419841E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.82539264E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.60815381E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.12352268E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.29546396E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.22578020E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.37656380E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.63138881E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.90932037E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.46433733E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.82694939E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.60805036E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.12350214E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.29531120E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.32899740E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.31323129E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.23852539E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.94556708E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.50252119E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.66132800E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.13004075E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.33846982E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.66575021E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.49781645E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.02240368E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.52318335E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.68482088E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.03839952E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.13757179E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     6.39743135E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.87162651E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.39361383E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     6.32773833E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.64816565E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.46404639E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.28143489E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.63176018E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.60224399E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.43707797E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.11856330E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.29517405E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.39750456E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.87163140E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.42231854E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     6.33009946E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.64805579E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.46418501E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.28173582E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.65249464E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.60214182E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.57229226E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.11854271E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.29502123E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.55501080E+02   # ~t_1
#    BR                NDA      ID1      ID2
     7.88381773E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.40654551E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.06342536E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.74050965E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.65402503E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.06477850E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.54252270E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.23890279E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.73132653E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     6.89742829E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.52275085E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.57441509E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.06999991E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.55577633E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.94780123E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.18579690E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33583702E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.33287787E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11194633E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11193545E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10740332E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.03127820E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.38148744E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.27620834E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.12396150E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.73632854E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.22441013E-01    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.27865692E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.26464371E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.18175805E-01    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.29790275E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     1.14960620E-01    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.64357799E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.96761032E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.15838136E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.88579092E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.71260215E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     2.50682418E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18713244E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18415234E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52213632E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52212225E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.49448376E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.43499875E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.43496146E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.42331541E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03554698E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.19864198E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     1.10842164E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     9.92593995E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.14063479E-01    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.13896929E-01    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     1.46232423E-01    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     1.46241160E-01    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     1.69786452E-01    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     3.29836702E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     3.29937299E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     3.58020801E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.95576626E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     2.20643003E-04    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     1.74685415E-04    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     2.82908836E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     2.82587937E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     3.78329570E-04    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.50263062E-03    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.50263062E-03    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.44113758E-03    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.44113758E-03    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     5.00876238E-04    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     5.00876238E-04    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     5.00715124E-04    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     5.00715124E-04    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     4.58355440E-04    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     4.58355440E-04    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     3.13653086E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.22842858E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.22842858E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.13545454E-01    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     1.13545454E-01    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     4.46163426E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.48506047E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.32790010E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.10669187E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     3.63359342E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     3.45236675E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.00993763E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     6.47819354E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     8.06330876E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     1.44748150E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     8.92805782E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     5.00602846E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.92817526E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.52880644E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.24925527E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.15041952E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.15041952E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.46054463E+01   # ~g
#    BR                NDA      ID1      ID2
     4.91053627E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.91053627E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     8.39839721E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.98836852E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.15801144E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     3.58401981E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.28721301E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.30905472E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     4.17269061E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     4.17269061E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.60108527E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.60108527E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     1.14015595E-02   # Gamma(h0)
     7.25291071E-04   2        22        22   # BR(h0 -> photon photon)
     3.78966754E-04   2        22        23   # BR(h0 -> photon Z)
     6.26548250E-03   2        23        23   # BR(h0 -> Z Z)
     5.51446260E-02   2       -24        24   # BR(h0 -> W W)
     2.06464223E-02   2        21        21   # BR(h0 -> gluon gluon)
     7.22902083E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.21557274E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.27079275E-02   2       -15        15   # BR(h0 -> Tau tau)
     4.82444546E-08   2        -2         2   # BR(h0 -> Up up)
     9.36141893E-03   2        -4         4   # BR(h0 -> Charm charm)
     8.39168299E-07   2        -1         1   # BR(h0 -> Down down)
     3.03514429E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.14143899E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.03663604E+00   # Gamma(HH)
     2.95683573E-07   2        22        22   # BR(HH -> photon photon)
     1.76114203E-07   2        22        23   # BR(HH -> photon Z)
     2.62862670E-04   2        23        23   # BR(HH -> Z Z)
     8.54910208E-04   2       -24        24   # BR(HH -> W W)
     5.66454842E-04   2        21        21   # BR(HH -> gluon gluon)
     8.57824174E-09   2       -11        11   # BR(HH -> Electron electron)
     3.81625450E-04   2       -13        13   # BR(HH -> Muon muon)
     1.10118999E-01   2       -15        15   # BR(HH -> Tau tau)
     2.83945835E-12   2        -2         2   # BR(HH -> Up up)
     5.50864558E-07   2        -4         4   # BR(HH -> Charm charm)
     9.18629417E-07   2        -1         1   # BR(HH -> Down down)
     3.32262171E-04   2        -3         3   # BR(HH -> Strange strange)
     8.87480936E-01   2        -5         5   # BR(HH -> Bottom bottom)
DECAY        36     2.04528637E+00   # Gamma(A0)
     1.98998883E-07   2        22        22   # BR(A0 -> photon photon)
     1.26838489E-08   2        22        23   # BR(A0 -> photon Z)
     5.03116499E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.57976920E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.81693656E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.10176137E-01   2       -15        15   # BR(A0 -> Tau tau)
     6.26169956E-13   2        -2         2   # BR(A0 -> Up up)
     1.21527653E-07   2        -4         4   # BR(A0 -> Charm charm)
     9.19049928E-07   2        -1         1   # BR(A0 -> Down down)
     3.32414339E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.88603791E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.58626158E-06   2        23        25   # BR(A0 -> Z h0)
DECAY        37     4.50479535E-01   # Gamma(Hp)
     4.22490116E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     1.80627633E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.10848333E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     3.89998063E-06   2        -1         2   # BR(Hp -> Down up)
     6.33204633E-05   2        -3         2   # BR(Hp -> Strange up)
     4.01791473E-05   2        -5         2   # BR(Hp -> Bottom up)
     2.04317640E-07   2        -1         4   # BR(Hp -> Down charm)
     1.40608625E-03   2        -3         4   # BR(Hp -> Strange charm)
     5.62612640E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.92931772E-07   2        -1         6   # BR(Hp -> Down top)
     8.80789707E-06   2        -3         6   # BR(Hp -> Strange top)
     4.79653846E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.14508493E-04   2        24        25   # BR(Hp -> W h0)
     1.34624867E-05   2        24        35   # BR(Hp -> W HH)
     1.45142518E-05   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    3.76754726E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.88491183E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.91258730E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.94366416E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.66917111E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.03558723E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    3.76754726E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.88491183E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.91258730E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.98193998E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.80600187E-03        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.98193998E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.80600187E-03        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.14763384E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.08457513E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    6.21067175E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.80600187E-03        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.98193998E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.41714643E-04   # BR(b -> s gamma)
    2    1.61671737E-06   # BR(b -> s mu+ mu-)
    3    3.52723302E-05   # BR(b -> s nu nu)
    4    2.75332856E-15   # BR(Bd -> e+ e-)
    5    1.17618913E-10   # BR(Bd -> mu+ mu-)
    6    2.46131440E-08   # BR(Bd -> tau+ tau-)
    7    9.24552747E-14   # BR(Bs -> e+ e-)
    8    3.94968200E-09   # BR(Bs -> mu+ mu-)
    9    8.37501026E-07   # BR(Bs -> tau+ tau-)
   10    3.98808500E-05   # BR(B_u -> tau nu)
   11    4.11953913E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42265370E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92348587E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16013790E-03   # epsilon_K
   17    2.28176945E-15   # Delta(M_K)
   18    2.48105834E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29308816E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.25560439E-16   # Delta(g-2)_electron/2
   21    9.64342279E-12   # Delta(g-2)_muon/2
   22    2.72857388E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.76512919E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.93851232E-01   # C7
     0305 4322   00   2    -4.00425094E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.84522507E-01   # C8
     0305 6321   00   2    -3.64597335E-03   # C8'
 03051111 4133   00   0     1.62555207E+00   # C9 e+e-
 03051111 4133   00   2     1.62611199E+00   # C9 e+e-
 03051111 4233   00   2    -2.62519032E-04   # C9' e+e-
 03051111 4137   00   0    -4.44824417E+00   # C10 e+e-
 03051111 4137   00   2    -4.44843679E+00   # C10 e+e-
 03051111 4237   00   2     1.94891637E-03   # C10' e+e-
 03051313 4133   00   0     1.62555207E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62611194E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.62706498E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44824417E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44843684E+00   # C10 mu+mu-
 03051313 4237   00   2     1.94910441E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50539284E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -4.21538047E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50539284E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -4.21497369E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50539284E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -4.10034270E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85824534E-07   # C7
     0305 4422   00   2     8.09300138E-06   # C7
     0305 4322   00   2     7.88084770E-07   # C7'
     0305 6421   00   0     3.30484230E-07   # C8
     0305 6421   00   2    -4.35014757E-06   # C8
     0305 6321   00   2     1.46757410E-07   # C8'
 03051111 4133   00   2     2.95446504E-07   # C9 e+e-
 03051111 4233   00   2     2.32203021E-06   # C9' e+e-
 03051111 4137   00   2    -4.34751790E-07   # C10 e+e-
 03051111 4237   00   2    -1.71889606E-05   # C10' e+e-
 03051313 4133   00   2     2.95445737E-07   # C9 mu+mu-
 03051313 4233   00   2     2.32203000E-06   # C9' mu+mu-
 03051313 4137   00   2    -4.34751077E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.71889612E-05   # C10' mu+mu-
 03051212 4137   00   2     1.12681192E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.71782813E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.12681222E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.71782813E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.12689641E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.71782812E-06   # C11' nu_3 nu_3
