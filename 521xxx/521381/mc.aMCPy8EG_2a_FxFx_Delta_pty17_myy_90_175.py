#--------------------------------------------------------------                                                                                               
# EVGEN configuration                                                                                                                                         
#--------------------------------------------------------------                                                                                               

evgenConfig.description = "MadGraph+Pythia8 samples for diphoton + 2jets production at NLO with FxFxDelta matching, folding 441"
evgenConfig.keywords = ["SM","diphoton"]
evgenConfig.contact = ["tpelzer@cern.ch","ana.cueto@cern.ch"]
evgenConfig.generators = ["aMcAtNlo","Pythia8"]
evgenConfig.nEventsPerJob = 200

from PyJobTransforms.trfLogger import msg
msg.info("Entering joboption")

# --------------------------------------------------------------                                                                                              
# Generate events                                                                                                                                             
# --------------------------------------------------------------                                                                                              
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*4 if runArgs.maxEvents>0 else 4*evgenConfig.nEventsPerJob

gridpack_mode=False # set to True to produce a gridpack


if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    set nlo_mixed_expansion False
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > a a [QCD] @0
    add process p p > a a j [QCD] @1
    add process p p > a a j j [QCD] @2
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

dummyToCopy      = 'dummy_fct.f'
dummyDestination = process_dir+'/SubProcesses/dummy_fct.f'
dummyfile        = subprocess.Popen(['get_files','-data',dummyToCopy])
dummyfile.wait()

if not os.access(dummyToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(dummyToCopy))
shutil.copy(dummyToCopy,dummyDestination)


import os
#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower':'PYTHIA8', 
            'req_acc':0.001,
            'ickkw':3,
            'ptgmin':17,
            'etagamma':2.7,
            'R0gamma':0.1,
            'xn':2.0,
            'epsgamma':0.1,
            'm_ph_ph_max': 175.0,
            'm_ph_ph_min': 90.0,            
            'isoEM':True,
            'nevents':int(nevents),
            'mcatnlo_delta':True,
            'folding':"4, 4, 1"
            }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

config_settings = {
    'pythia8_path':os.getenv("PY8PATH")
    }

modify_config_card(settings=config_settings, process_dir=process_dir)



toFix = process_dir+'/SubProcesses/montecarlocounter.f'
msg.info("Trying to modify file "+toFix)
if not os.access(toFix,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(toFix))
os.rename(toFix, toFix+".backup")
old_file = open(toFix+".backup", 'r')
new_file = open(toFix, 'w')
for line in iter(old_file):
    if line.find("if(probne.gt.1.d0)then") >= 0:
        new_file.write("      if(probne.gt.1.d0 .and. abs(probne-1.d0).gt.1.d-10)then\n")
    else:
        new_file.write(line)
old_file.close()
new_file.close()
msg.info("Finished modifying file "+toFix)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode,required_accuracy=0.001)
msg.info("Finished generate")
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)
msg.info("Finished arrange_output")

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
PYTHIA8_nJetMax=2
PYTHIA8_qCut=20.

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

include("Pythia8_i/Pythia8_aMcAtNlo_decayMEC.py")
