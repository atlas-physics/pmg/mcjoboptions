from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*2. if runArgs.maxEvents>0 else 2.*evgenConfig.nEventsPerJob

process_def = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model SMEFTsim_topU3l_MwScheme_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define bq = b b~
generate p p > ta- ta+ NP<=1 NP^2==1
output -f
"""

process_dir = new_process(process_def)

lhaid=260000
pdflabel='lhapdf'

settings = { 'lhe_version' : '3.0', 
             'cut_decays'  : 'F', 
             'lhaid'       : lhaid,
             'pdlabel'     : "'"+pdflabel+"'",
             'ickkw'       : 0,
             'ptj'         : 20,
             'ptb'         : 20,
             'xqcut'       : 0.,
             'drjj'        : 0.0,
             'mmll'        : 120.,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'use_syst'    : 'False',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params = \
    { "mass" : { "1" : 0.0, "2" : 0.0, "3" : 0.0, "4" : 0.0, "5" : 0.0 },
      "smeft" : { "101" : 1. }
     }

modify_param_card(process_dir=process_dir,params=params)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""launch --rwgt_name=ceBRe
    set smeft 101 0.
    set smeft 102 1.
launch --rwgt_name=ceyRe
    set smeft 101 -0.55
    set smeft 102 1.
launch --rwgt_name=ceZRe
    set smeft 101 1.8
    set smeft 102 1.
launch --rwgt_name=cHl1
    set smeft 101 0.
    set smeft 102 0.
    set smeft 103 1.
launch --rwgt_name=cHl3
    set smeft 103 0.
    set smeft 104 1.
launch --rwgt_name=cHe
    set smeft 104 0.
    set smeft 105 1.
launch --rwgt_name=cld
    set smeft 105 0.
    set smeft 121 1.
launch --rwgt_name=ced
    set smeft 121 0.
    set smeft 115 1.
launch --rwgt_name=cje
    set smeft 115 0.
    set smeft 117 1.
launch --rwgt_name=clj1
    set smeft 117 0.
    set smeft 108 1.
launch --rwgt_name=clj3
    set smeft 108 0.
    set smeft 109 1.
launch --rwgt_name=clu
    set smeft 109 0.
    set smeft 119 1.
launch --rwgt_name=ceu
    set smeft 119 0.
    set smeft 113 1.
launch --rwgt_name=cll1
    set smeft 113 0.
    set smeft 107 1.
""")
reweight_card_f.close()

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True only for local testing!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_SMEFTtautau'
evgenConfig.contact  = [ "chays@cern.ch" ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
