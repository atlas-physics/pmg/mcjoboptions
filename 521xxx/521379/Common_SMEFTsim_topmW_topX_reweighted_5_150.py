from MadGraphControl.MadGraphUtils import *
from MCJobOptionUtils.JOsupport import get_physics_short

# TO USE THIS CONTROL FILE:
# - create a separate JO for your process, e.g. mc.MGPy8_ttll_SMEFTsim_reweighted.py
# - declare "selected_operators", a list of SMEFTsim operators that is a subset of the keys of the "eft_dict" below
# - declare "process_definition", a string that will be appended to "process" below and MUST contain at least the relevant MG generate command
# - declare "fixed_scale", a float to fix muR=muF (because MG doesn't run the WCs). Recommended: = sum(masses)_{final state particles}
# - declare "gridpack", a bool indicating whether generation (and possibly reweighting) is to be run from a gridpack
# - declare "evgenConfig.description", a sufficiently descriptive string for the sample metadata,
#                                      e.g. 'SMEFTsim 3.0 tt+ll, top model, inclusive, reweighted, EFT vertices, no propagator correction'
# - include this file, using the appropriate relative path

# general parameters
nevents     = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents    *= 1.1 # safety factor
mllcut      = 5
mllcut_max = 150
lhe_version = 3

# process definition
model = "SMEFTsim_top_MwScheme_UFO-massless_fourfermion"


process = '''
import model ''' + model + '''
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define wdec = l+ l- vl vl~ j
''' + process_definition + '''
output -f
'''
process_dir = new_process(process)

# run card settings
settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : 5,
    'pdlabel'               : 'lhapdf',
    'lhaid'                 : 262000,
    'use_syst'              : 'False',
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'mmll'                  : mllcut,
    'mmllmax'               : mllcut_max,
    'dynamical_scale_choice': '0',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'scale'                 : fixed_scale,
    'dsqrt_q2fact1'         : fixed_scale,
    'dsqrt_q2fact2'         : fixed_scale,
}

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=settings)

# SM param card settings
params = dict()
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'

modify_param_card(process_dir=process_dir,params=params)

# EFT param card settings
params = dict() 
params['SMEFTcutoff'] = dict()
params['SMEFTcutoff']['1'] = '1.000000e+03'

params['SMEFT'] = dict()

params['SMEFT']['133'] = 1.0 # cQl111
params['SMEFT']['134'] = 1.0 # cQl122
params['SMEFT']['135'] = 3.0 # cQl133
params['SMEFT']['136'] = 1.0 # cQl311
params['SMEFT']['137'] = 0.6 # cQl322
params['SMEFT']['138'] = 1.0 # cQl333
params['SMEFT']['148'] = 1.0 # cte11
params['SMEFT']['149'] = 1.0 # cte22
params['SMEFT']['150'] = 1.0 # cte33
params['SMEFT']['160'] = 1.0 # cQe11
params['SMEFT']['161'] = 1.0 # cQe22
params['SMEFT']['162'] = 1.0 # cQe33
params['SMEFT']['166'] = 1.0 # ctl11
params['SMEFT']['167'] = 1.0 # ctl22
params['SMEFT']['168'] = 1.0 # ctl33
params['SMEFT']['196'] = 2.0 # cleQt1Re11
params['SMEFT']['197'] = 2.0 # cleQt1Re22
params['SMEFT']['198'] = 2.0 # cleQt1Re33
params['SMEFT']['202'] = 0.4 # cleQt3Re11
params['SMEFT']['203'] = 0.4 # cleQt3Re22
params['SMEFT']['204'] = 0.4 # cleQt3Re33



modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)



# build a dictionary of EFT operators: dict[operator name] = (block, id, [values])
eft_dict = {
    'cQl111': ('SMEFT', 133, [-5,-1,1,5]),
    'cQl122': ('SMEFT', 134, [-5,-1,1,5]),
    'cQl133': ('SMEFT', 135, [-5,-3,3,5]),
    'cQl311': ('SMEFT', 136, [-5,-1,1,5]),
    'cQl322': ('SMEFT', 137, [-1.1,-0.6,0.6,1.1]),
    'cQl333': ('SMEFT', 138, [-5,-1,1,3,5]),
    'cte11':  ('SMEFT', 148, [-2.4,-1,1,3]),
    'cte22':  ('SMEFT', 149, [-3,-1,1,5]),
    'cte33':  ('SMEFT', 150, [-5,-1,1,5]),
    'cQe11':  ('SMEFT', 160, [-2.4,-1,1,3]),
    'cQe22':  ('SMEFT', 161, [-3,-1,1,5]),
    'cQe33':  ('SMEFT', 162, [-5,-1,1,5]),
    'ctl11':  ('SMEFT', 166, [-2.4,-1,1,3]),
    'ctl22':  ('SMEFT', 167, [-3,-1,1,5]),
    'ctl33':  ('SMEFT', 168, [-5,-1,1,5]),
    'cleQt1Re11':('SMEFT',196,[-4.,-3.,-2.,2.,3.,4.]),
    'cleQt1Re22':('SMEFT',197,[-4.,-3.,-2.,2.,3.,4.]),
    'cleQt1Re33':('SMEFT',198,[-4.,-3.,-2.,2.,3.,4.]),
    'cleQt3Re11':('SMEFT',202,[-1.2,-0.8,-0.4,0.4,0.8,1.2]),
    'cleQt3Re22':('SMEFT',203,[-1.2,-0.8,-0.4,0.4,0.8,1.2]),
    'cleQt3Re33':('SMEFT',204,[-1.2,-0.8,-0.4,0.4,0.8,1.2]),
}

# convert operator values to text format
def value_to_string(value):
    if value > 0:
        string = "p"
    else:
        string = "m"
    string += "{:.1f}".format(abs(value)).replace(".","p")
    return string


# define reweighting points
reweight_commands = "change rwgt_dir rwgt\n"
reweight_commands += "launch --rwgt_info=SM \n"
for operator in selected_operators:
    block = eft_dict[operator][0]
    idnumber = eft_dict[operator][1]
    value = 0
    reweight_commands += "set "+ block + " " +str(idnumber) + " " + str(value) + "\n"

for i,operator in enumerate(selected_operators):
    block = eft_dict[operator][0]
    idnumber = eft_dict[operator][1]
    for value in eft_dict[operator][2]:
        reweight_commands += "launch --rwgt_info=" + operator + "_" + value_to_string(value) + "\n"
        reweight_commands += "set "+ block + " " +str(idnumber) + " " + str(value) + "\n"
        for i2,operator2 in enumerate(selected_operators):
            if i2!=i:
                block2 = eft_dict[operator2][0]
                idnumber2 = eft_dict[operator2][1]
                value2 = 0
                reweight_commands += "set "+ block2 + " " + str(idnumber2) + " " + str(value2) + "\n"

for i,operator1 in enumerate(selected_operators):
    block1 = eft_dict[operator1][0]
    idnumber1 = eft_dict[operator1][1]
    for i2,operator2 in enumerate(selected_operators[i+1:]):
        block2 = eft_dict[operator2][0]
        idnumber2 = eft_dict[operator2][1]
        # avoid going beyond 1000 weights if possible
        #if len(selected_operators) <= 30:
            #for value in [-0.3,0.4]:
                #reweight_commands += "launch --rwgt_info=" + operator1 + "_" + value_to_string(value) + "_" + operator2 + "_" + value_to_string(value) + "\n"
                #reweight_commands += "set "+ block1 + " " + str(idnumber1) + " " + str(value) + "\n"
                #reweight_commands += "set "+ block2 + " " + str(idnumber2) + " " + str(value) + "\n"
                #for i3,operator3 in enumerate(selected_operators):
                    #if ((i3!=i) & (i3!=(i2+i+1))):
                        #block3 = eft_dict[operator3][0]
                        #idnumber3 = eft_dict[operator3][1]
                        #value3 = 0
                        #reweight_commands += "set "+ block3 + " " + str(idnumber3) + " " + str(value3) + "\n"
        #else:
        value1 = -0.3
        value2 = 0.4
        reweight_commands += "launch --rwgt_info=" + operator1 + "_" + value_to_string(value1) + "_" + operator2 + "_" + value_to_string(value2) + "\n"
        reweight_commands += "set "+ block1 + " " + str(idnumber1) + " " + str(value1) + "\n"
        reweight_commands += "set "+ block2 + " " + str(idnumber2) + " " + str(value2) + "\n"
        for i3,operator3 in enumerate(selected_operators):
            if ((i3!=i) & (i3!=(i2+i+1))):
                block3 = eft_dict[operator3][0]
                idnumber3 = eft_dict[operator3][1]
                value3 = 0
                reweight_commands += "set "+ block3 + " " + str(idnumber3) + " " + str(value3) + "\n"



    
# write to reweight card
reweight_card   = process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write(reweight_commands)
reweight_card_f.close()


print_cards()


generate(process_dir=process_dir,
         #grid_pack=gridpack,
         #gridpack_compile=False,
         required_accuracy=0.001,
         runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=False,
                          lhe_version=lhe_version)

evgenConfig.contact          = ['gianna.loeschcke.centeno@cern.ch']
evgenConfig.generators       = ['MadGraph','EvtGen','Pythia8']

check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
