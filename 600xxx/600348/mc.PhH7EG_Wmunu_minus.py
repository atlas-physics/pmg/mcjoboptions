# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w- > mu- vm~"

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 5000
