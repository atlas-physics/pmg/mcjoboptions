#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig721 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, at two lepton filter, ME NNPDF30 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.de','daniel.rauch@cern.ch','baptiste.ravina@cern.ch', 'ian.connelly@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 4

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

command = """
set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/HardNLOPDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/HardNLOPDF
"""
Herwig7Config.add_commands(command)
# Check this?
include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------                                                                                                                                                         
# Event filter                                                                                                                                                                                                            
#--------------------------------------------------------------                                                                                                                                                           
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
