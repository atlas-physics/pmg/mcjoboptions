#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

fname = "merged_lhef._0.events"

f = open(fname, "r")
lines = f.readlines()
f.close()

f = open(fname, 'w')
for line in lines:
  if not "#pdf" in line:
    f.write(line)
f.close()

include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.353 100 24 -24 ",   # WW decay
                             "25:addChannel = on 0.043 100 23 23  ",   # ZZ decay
                             "25:addChannel = on 0.104 100 15 -15 ",   # tautau decay 
                             "25:addChannel = on 0.5   100 22 22  ",   # yy decay 
                             "24:mMin = 0", # W minimum mass
                             "24:mMax = 99999", # W maximum mass
                             "23:mMin = 0", # Z minimum mass
                             "23:mMax = 99999", # Z maximum mass
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "BSM diHiggs production, decay to multi-lepton, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "BSM", "nonResonant", "ggF", "multlepton"]
evgenConfig.contact        = ['Shuiting Xin <Shuiting.Xin@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 12

# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hXXFilter", PDGParent = [25], PDGChild = [15,23,24])

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepOneFilter")
filtSeq.LepOneFilter.IncludeHadTaus = True
filtSeq.LepOneFilter.NLeptons = 1
filtSeq.LepOneFilter.MinPt = 7000
filtSeq.LepOneFilter.MinVisPtHadTau = 15000
filtSeq.LepOneFilter.MaxEta = 3

filtSeq += MultiElecMuTauFilter("LepTwoFilter")
filtSeq.LepTwoFilter.IncludeHadTaus = True
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.MinPt = 7000
filtSeq.LepTwoFilter.MinVisPtHadTau = 15000
filtSeq.LepTwoFilter.MaxEta = 3

filtSeq.Expression = "hyyFilter and hXXFilter and LepOneFilter and not LepTwoFilter"
