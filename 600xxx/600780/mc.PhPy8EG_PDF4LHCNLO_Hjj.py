# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "ggH H + 2 jet"
evgenConfig.description = "POWHEG MiNLO H + 2 jet production, mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact  = [ "Hongtao.Yang@cern.ch", "syed.haider.abidi@cern.ch"]
evgenConfig.nEventsPerJob = 2000


# --------------------------------------------------------------
# Process setup
# --------------------------------------------------------------
include('PowhegControl/PowhegControl_Hjj_Common.py')

# limited number of systematics
PowhegConfig.PDF = 90400
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5 ,1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]

# --------------------------------------------------------------
# Generate information
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower
# --------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
