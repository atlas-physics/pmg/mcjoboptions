# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG W+taunu production and BSM tau->3mu decay"
evgenConfig.keywords = ["BSM", "W", "tau", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]

evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg W_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_W_EW_Common.py")

PowhegConfig.decay_mode = "w- > tau- vt~"

PowhegConfig.no_ew   = 1
PowhegConfig.ptsqmin = 4. # AZNLO setting
PowhegConfig.PHOTOS_enabled = False

filterMultiplier = 2.5
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier

# tune Powheg settings
PowhegConfig.rwl_group_events = 100000
PowhegConfig.ncall1       = 200000
PowhegConfig.ncall2       = 200000
PowhegConfig.nubound      = 200000
PowhegConfig.itmx1        = 10
PowhegConfig.itmx2        = 20
PowhegConfig.storemintupb = 0 # smaller grids

# Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 2
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 1

### default PDF for AZNLO is CT10, then add the usuals, a few new ones and NNPDF3.0 replica
# CT10, NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
PowhegConfig.PDF = [10800, 260000, 25200, 13165, 90900, 265000, 266000, 303400]
 # NNPDF31_nnlo_as_0118, CT18NLO, CT18NNLO, CT18ANNLO, MSHT20nlo_as118, MSHT20nnlo_as118
PowhegConfig.PDF.extend([303600, 14400, 14000, 14200, 27100, 27400])
PowhegConfig.PDF.extend(range(260001, 260101)) # Include the NNPDF3.0 error set

# default scale variations
#PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]
#PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Pythia8 Shower & settings
# --------------------------------------------------------------    

include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#include('Pythia8_i/Pythia8_Photospp.py')

# force all taus to decay to three muons
genSeq.Pythia8.Commands += [' 15:oneChannel = 1 1.0 0 13 -13 13']

# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter

# filtSeq += MultiMuonFilter("OneMuonFilter")
# filtSeq.OneMuonFilter.Ptcut = 3250.
# filtSeq.OneMuonFilter.Etacut = 3.0
# filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 3.0
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 2000.
filtSeq.ThreeMuonsFilter.Etacut = 3.0
filtSeq.ThreeMuonsFilter.NMuons = 3

#filtSeq.Expression="OneMuonFilter and TwoMuonsFilter and ThreeMuonsFilter"
