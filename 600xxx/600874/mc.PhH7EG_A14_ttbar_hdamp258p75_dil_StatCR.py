evgenConfig.description = 'POWHEG+Herwig7 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, dilepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files + statistical CR model'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 8
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune = "H7.2-Default"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=False)
Herwig7Config.tune_commands()

command = """
set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/HardNLOPDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/HardNLOPDF
set /Herwig/Hadronization/ColourReconnector:Algorithm Statistical
"""
Herwig7Config.add_commands(command)

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()



#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

