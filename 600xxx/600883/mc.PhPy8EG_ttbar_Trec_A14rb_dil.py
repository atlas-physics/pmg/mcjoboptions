#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ttbar production with Top recoil A14-rb shower and dilepton tag filter"
evgenConfig.keywords = ["SM", "top","ttbar"]
evgenConfig.contact = ["amoroso@cern.ch","mvanadia@cern.ch"]

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 8

# # --------------------------------------------------------------
# # Load ATLAS defaults for the Powheg Z_EW process
# # --------------------------------------------------------------
# include("PowhegControl/PowhegControl_tt_Common.py")


# PowhegConfig.decay_mode = "t t~ > all"
# PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
# PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
# PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
# PowhegConfig.PDF          = [303600, 260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119 

# PowhegConfig.nEvents     *= 20    # Add safety factor                                                                                                                                                            

# #--------------------------------------------------------------
# # Generate events
# # --------------------------------------------------------------
# PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]
genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured=off' ]

# Pythia shower variations
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'UncertaintyBands:doVariations=on' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleFSR=10' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:FSRpTmin2Fac=6' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleISR=10' ]

genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured=off' ]
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.UserHooks += ['TopRecoilHook']


#--------------------------------------------------------------                                                                                                                                                    
# Powheg/Pythia matching
#--------------------------------------------------------------                                            
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
