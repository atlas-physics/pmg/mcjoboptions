#--------------------------------------------------------------
# POWHEG+Pythia8 gg->ZH, H->Zy->(vv/l+l-)y, Z->ll/vv production
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 420


## Input dataset
## mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590

#runArgs.inputGeneratorFile = '/eos/user/r/rmazini/datasets/mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590'

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']

else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs->Zy->vv/ll y at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 23',
                             '23:onMode = off',
                             '23:onIfAny = 11 12 13 14 15 16' ]

#--------------------------------------------------------------
# Lepon filter. This is used to select 2 leptons in final state
#--------------------------------------------------------------
if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "POWHEG+Pythia8 H+Z->vvy+l+l- production"
evgenConfig.keywords      = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact       = [ 'rachid.mazini@cern.ch' ]
evgenConfig.inputconfcheck= 'ggZH125_MINLO_Zy_VpT_13TeV'
evgenConfig.process       = "gg->ZH, H->Zy->(vv/l+l-)y, Z->ll/vv"
