# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG NC DY production with NLO EW correction, nCTEQ default + low mass dimuon filter"
evgenConfig.keywords = ["SM", "Z"]
evgenConfig.contact = ["olszewsk@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 50


# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Z_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.beam_1_type = 2  # n
PowhegConfig.beam_2_type = 2  # n

PowhegConfig.decay_mode = "z > mu+ mu-"
PowhegConfig.no_ew=1
PowhegConfig.ptsqmin=4
PowhegConfig.mass_low=7
PowhegConfig.PHOTOS_enabled = False
PowhegConfig.nEvents *= 100

#Gmu EW scheme inputs matching what used in the Powheg V1 samples
PowhegConfig.scheme=0
PowhegConfig.alphaem=0.00781653
PowhegConfig.mass_W=79.958059

PowhegConfig.PDF = range(103100, 103100+33 ) #nCTEQ15
PowhegConfig.PDF.extend(range(3104500, 3104500+251)) #nNNPDF1.0
PowhegConfig.PDF.extend(range(901300, 901300+97)) #EPPS16
PowhegConfig.PDF.extend(range(3206200, 3206200+59)) #TUJU19
 
PowhegConfig.mu_F = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0]                                                                                            
PowhegConfig.mu_R = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5]                              

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------                                                                                       
# Shower settings                                                                                                                                      
# --------------------------------------------------------------                                                                                        
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
#Switching off Pythia8 QED showering and shower weights                                                                                                      
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByOther = off",
                            "TimeShower:QEDshowerByL = off",
                            "TimeShower:QEDshowerByGamma = off",
                            "TimeShower:QEDshowerByL = off",
                            "SpaceShower:QEDshowerByQ = off",
                            "SpaceShower:QEDshowerByL = off",
                            "UncertaintyBands:doVariations=off"]


# Filter the di-muon J/psi decays
include('GeneratorFilters/DiLeptonMassFilter.py')
filtSeq.DiLeptonMassFilter.MinPt = 3500
filtSeq.DiLeptonMassFilter.MaxEta = 2.7
filtSeq.DiLeptonMassFilter.MinMass = 1000
filtSeq.DiLeptonMassFilter.MaxMass = 45000
filtSeq.DiLeptonMassFilter.MinDilepPt = -1
filtSeq.DiLeptonMassFilter.AllowElecMu =  False
filtSeq.DiLeptonMassFilter.AllowSameCharge = False


