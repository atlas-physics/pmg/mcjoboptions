#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 5 -5 ", # bb decay
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to bbbb, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar", "bottom"]
evgenConfig.contact        = ['Marco Valente <marco.valente@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 2

