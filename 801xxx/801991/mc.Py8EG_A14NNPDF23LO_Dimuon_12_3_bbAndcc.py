# JO for Pythia 8 hard QCD bb and cc with muon filters

evgenConfig.description = "QCD jet production bbbar and ccbar, with muon filters and with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["mdshapiro@lbl.gov, kehang.bai@cern.ch"]
evgenConfig.nEventsPerJob = 1000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:hardbbbar  = on",
                            "HardQCD:hardccbar  = on",
                            "PhaseSpace:pTHatMin = 20."]

testSeq.TestHepMC.MaxVtxDisp=100000.0
testSeq.TestHepMC.MaxTransVtxDisp=100000.0

# Our final offline selection is one muon above 15 GeV and one above 5 GeV
# for the generation, we will lower the cuts on each by 2 GeV

include('GeneratorFilters/xAODMuonFilter_Common.py')
filtSeq.xAODMuonFilter.Ptcut = 12000.
filtSeq.xAODMuonFilter.Etacut = 2.6

include('GeneratorFilters/xAODMultiMuonFilter_Common.py')
filtSeq.xAODMultiMuonFilter.Ptcut = 3000.
filtSeq.xAODMultiMuonFilter.Etacut = 2.6
filtSeq.xAODMultiMuonFilter.NMuons = 2

include('GeneratorFilters/xAODDiLeptonMassFilter_Common.py')
filtSeq.xAODDiLeptonMassFilter.MinPt = 0.0
filtSeq.xAODDiLeptonMassFilter.MaxEta = 2.6
filtSeq.xAODDiLeptonMassFilter.MinMass = 100.0
filtSeq.xAODDiLeptonMassFilter.MaxMass = 15000.
filtSeq.xAODDiLeptonMassFilter.AllowSameCharge = True

