##############################################################
# Job options for B- -> K1- ee -> K-pi+pi- ee
##############################################################

evgenConfig.description   = "Exclusive B- -> K1- ee -> K-pi+pi- ee decay production"
evgenConfig.process       = "Bd -> K1 ee -> Kpipi ee"
evgenConfig.keywords      = [ "bottom", "2electron", "exclusive" ]
evgenConfig.contact       = [ "ann-kathrin.perrevoort@cern.ch" ]
evgenConfig.nEventsPerJob = 200

# Create EvtGen decay rules
f = open("Bm_K1_Kpipi_EE_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias      Myanti-K*0       anti-K*0\n")
f.write("Decay Myanti-K*0\n")
f.write("1.000        K-        pi+                    PHSP;\n")
f.write("Enddecay\n")
f.write("Alias      MyOmega omega\n")
f.write("Decay MyOmega\n")
f.write("1.000        pi+        pi- VSS;\n")
f.write("Enddecay\n")
f.write("Alias MyK1_1270_- K_1-\n")
f.write("Decay MyK1_1270_-\n")
f.write("0.2716513        Myanti-K*0   pi-                    PHSP;\n")
f.write("0.3564309        rho0         K-                     PHSP;\n")
f.write("0.0042848        MyOmega      K-                     PHSP;\n")
f.write("0.3676330        K-  pi+ pi-                         PHSP;\n")
f.write("Enddecay\n")
f.write("Decay B-\n")
f.write("1.0000   MyK1_1270_-  e+ e- PHOTOS PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -521 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bm_K1_Kpipi_EE_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = False
filtSeq.BSignalFilter.LVL2MuonCutOn             = False
# filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutPT             = 10000.0
# filtSeq.BSignalFilter.LVL2MuonCutPT             = 10000.0
if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 4000.0
filtSeq.MultiElectronFilter.Etacut     = 2.7
filtSeq.BSignalFilter.B_PDGCode                 = -521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

