#################################################################################
# job options fragment for inclusive W^{-}->nutau10->mu4mu2mu1 for run2
# Equivalent of MC15.300500.Pythia8EvtGen_A14NNPDF23LO_NegWincl_nutau10_mu4mu2mu1.py
#################################################################################
# Not only qq->W production, qq->Wg and gq->Wq production channels are considered
# in this fragments.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV and tau>10GeV
#################################################################################

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
evgenConfig.description   = "NegW->nutau->3mu production"
evgenConfig.keywords      = [ "W", "muon", "tau", "BSM" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact       = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process       = "pp>W>nutau>3munu"

genSeq.Pythia8.Commands       += [ 'WeakSingleBoson:ffbar2W = on' ]
genSeq.Pythia8.Commands       += [ 'WeakBosonAndParton:qqbar2Wg = on' ]
genSeq.Pythia8.Commands       += [ 'WeakBosonAndParton:qg2Wq = on' ]

genSeq.Pythia8.Commands += [ ' 24:onMode = 2' ]
genSeq.Pythia8.Commands += [ ' 24:onIfMatch = -15 16' ]
genSeq.Pythia8.Commands += [ ' 15:onMode = 3' ]
genSeq.Pythia8.Commands += [ ' 15:addChannel = 2 1.0 0 13 -13 13' ]

#Add the Filters: (very very rough equivalent of the original MC15 file)
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter4")
filtSeq += MultiMuonFilter("MultiMuonFilter2")
filtSeq += MultiMuonFilter("MultiMuonFilter1")
filtSeq.MultiMuonFilter4.NMuons = 1
filtSeq.MultiMuonFilter2.NMuons = 2
filtSeq.MultiMuonFilter1.NMuons = 3
filtSeq.MultiMuonFilter4.Ptcut  = 4000.0
filtSeq.MultiMuonFilter2.Ptcut  = 2000.0
filtSeq.MultiMuonFilter1.Ptcut  = 1000.0
filtSeq.MultiMuonFilter4.Etacut = 3.0
filtSeq.MultiMuonFilter2.Etacut = 3.0
filtSeq.MultiMuonFilter1.Etacut = 3.0
