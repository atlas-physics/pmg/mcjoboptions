##############################################################
# Job options fragment for pp->Upsilon1S(mu3p5mu3p5)X
# Equivalent of MC15.300101.Pythia8BPhotospp_A14_CTEQ6L1_pp_Upsilon1S_mu3p5mu3p5.py
##############################################################

evgenConfig.description   = "Inclusive pp->Upsilon1S(mu3p5mu3p5) production with Photos"
evgenConfig.process       = "Upsilon(1S) -> 2mu"
evgenConfig.keywords      = [ "charmonium", "Upsilon", "2muon", "inclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_Bottomonium_Common.py")

#genSeq.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 1.' ]  # already defined in Bottomonium_Common

genSeq.Pythia8B.Commands += [ '553:onMode = off' ]
genSeq.Pythia8B.Commands += [ '553:3:onMode = on' ]

genSeq.Pythia8B.SignalPDGCodes = [ 553, -13,13 ]

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
