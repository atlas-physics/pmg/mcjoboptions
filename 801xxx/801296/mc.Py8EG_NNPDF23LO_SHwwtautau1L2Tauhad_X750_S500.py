include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.process     = "gg->X->SH->Ww+tautau, 1 lepton"
evgenConfig.description = "Generation of gg > X > SH > WWtautau > 1l2j2tauhad"
evgenConfig.keywords = ["BSMHiggs"]
evgenConfig.contact = ['Weiming Yao <weiming.yao@cern.ch>']
  
genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'HiggsA3:parity = 1',
                            'Higgs:clipWings = off',
                            '36:m0 = 750.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 500.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 15 -15',
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24',
]

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepOneFilter")
filtSeq.LepOneFilter.IncludeHadTaus = False
filtSeq.LepOneFilter.NLeptons = 1
filtSeq.LepOneFilter.MinPt = 7000
filtSeq.LepOneFilter.MaxEta = 3

filtSeq += MultiElecMuTauFilter("LepTwoFilter")
filtSeq.LepTwoFilter.IncludeHadTaus = False
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.MinPt = 7000
filtSeq.LepTwoFilter.MaxEta = 3

filtSeq += MultiElecMuTauFilter("LepThreeFilter")
filtSeq.LepThreeFilter.IncludeHadTaus = True
filtSeq.LepThreeFilter.NLeptons = 3
filtSeq.LepThreeFilter.MinPt = 7000
filtSeq.LepThreeFilter.MinVisPtHadTau = 15000
filtSeq.LepThreeFilter.MaxEta = 3

filtSeq.Expression = "LepOneFilter and LepThreeFilter and not LepTwoFilter"
