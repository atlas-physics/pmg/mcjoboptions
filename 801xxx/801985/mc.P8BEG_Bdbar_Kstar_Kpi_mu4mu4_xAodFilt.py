##############################################################
# Job options for B0bar -> anti-K*0 mumu
##############################################################

evgenConfig.description   = "Exclusive B0bar->anti-K*(Kpi)(mu4mu4) decay production"
evgenConfig.process       = "B0bar -> anti-K*(kpi) mumu"
evgenConfig.keywords      = [ "bottom", "B0", "2muon", "exclusive" ]
evgenConfig.contact       = [ "kin.yip.fung@cern.ch" ]
evgenConfig.nEventsPerJob = 2000

# Create EvtGen decay rules
f = open("Bdbar_KstarKpi_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_anti-K*0 anti-K*0\n")
f.write("Decay  my_anti-K*0\n")
f.write("1.0000   K- pi+   VSS;\n")
f.write("Enddecay\n")
f.write("Decay anti-B0\n")
f.write("1.0000   my_anti-K*0  mu+ mu-   PHOTOS BTOSLLBALL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/xAODBSignalFilter_Common.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -511 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bdbar_KstarKpi_MUMU_USER.DEC"

#ATLMCPROD-10179: revert to old evtgen file to avoid rare event failures
evgenConfig.auxfiles += ['2014Inclusive_17.dec']
decayfile_str = "2014Inclusive_17.dec"
genSeq.EvtInclusiveDecay.decayFile = decayfile_str

filtSeq.xAODBSignalFilter.LVL1MuonCutOn             = True
filtSeq.xAODBSignalFilter.LVL2MuonCutOn             = True
filtSeq.xAODBSignalFilter.LVL2MuonCutEta            = 2.7
filtSeq.xAODBSignalFilter.LVL1MuonCutEta            = 2.7
filtSeq.xAODBSignalFilter.LVL1MuonCutPT             = 4000.0
filtSeq.xAODBSignalFilter.LVL2MuonCutPT             = 4000.0

filtSeq.xAODBSignalFilter.B_PDGCode                 = -511
filtSeq.xAODBSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.xAODBSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.xAODBSignalFilter.Cuts_Final_hadrons_eta    = 2.7

