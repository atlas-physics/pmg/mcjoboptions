#Pythia8 minimum bias DD with A3

evgenConfig.description = "Double-diffractive events producing di-jets. Using the A3 NNPDF23LO tune and Rockefeller."
evgenConfig.keywords = ["QCD", "minBias","diffraction"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "malewick@cern.ch" ]
# how many events should be produced in one production job
evgenConfig.nEventsPerJob = 50

include ("Pythia8_i/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]
genSeq.Pythia8.Commands += ["Diffraction:doHard = on"]
genSeq.Pythia8.Commands += ["Diffraction:sampleType = 2"]
genSeq.Pythia8.Commands += ["SigmaDiffractive:PomFlux = 5"]
genSeq.Pythia8.Commands += ["PDF:PomSet = 6"]
genSeq.Pythia8.Commands += ["SigmaTotal:zeroAXB = off"]

# rel >=22
include("GeneratorFilters/xAODForwardProtonFilter_Common.py")

filtSeq.xAODForwardProtonFilter.xi_min = 0.01
filtSeq.xAODForwardProtonFilter.xi_max = 0.20
filtSeq.xAODForwardProtonFilter.beam_energy = 0.5*runArgs.ecmEnergy*GeV
filtSeq.xAODForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.xAODForwardProtonFilter.pt_max = 3.5*GeV

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

