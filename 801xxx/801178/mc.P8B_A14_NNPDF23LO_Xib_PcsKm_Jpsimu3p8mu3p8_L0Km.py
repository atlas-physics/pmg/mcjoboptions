#################################################################################
# Job options for Pythia8B_i generation of Xi_b->Pcs(->J/psi(mumu)Lambda0(ppi))K-
#################################################################################
evgenConfig.description = "Signal Xi_b->Pcs(->J/psi(mumu)Lambda0(ppi))K-"
evgenConfig.keywords = ["bottom","exclusive","Lambda_b0","Jpsi","2muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->Xi_b->Pcs(->J/psi(mumu)Lambda0(ppi))K-"
evgenConfig.nEventsPerJob = 200

#include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py")
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.Commands += ['StringFlav:probStoUD = 0.5']
genSeq.Pythia8B.Commands += ['StringFlav:popcornSmeson = 0.9']
#
genSeq.Pythia8B.VetoDoubleBEvents = True

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.'] 
genSeq.Pythia8B.QuarkPtCut = 9.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017/2019/2021
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000926'] # PDG 2021
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

#
# Xi_b:
#
genSeq.Pythia8B.Commands += ['5132:m0 = 5.7970']  # PDG 2021
genSeq.Pythia8B.Commands += ['5132:tau0 = 0.4713'] # PDG 2021
#
# Xi_b decays:
#
genSeq.Pythia8B.Commands += ['5132:onMode = 3']
#
genSeq.Pythia8B.Commands += ['5132:addChannel = 2 1.0 0 441232 -321']

#
# P_cs(4459)0, with LHCb mass and width
#
genSeq.Pythia8B.Commands += ['441232:new = Pcs(4459)0 Pcs(4459)bar0 2 0 0 4.4588 0.0173 4.25 5.25 0.']
genSeq.Pythia8B.Commands += ['441232:addChannel = 1 1. 0 443 3122']

genSeq.Pythia8B.SignalPDGCodes = [5132,441232,443,-13,13,3122,-321]
genSeq.Pythia8B.SignalPtCuts = [0.,0.,0.,3.8,3.8,1.4,0.475]
genSeq.Pythia8B.SignalEtaCuts = [100.,100.,100.,2.5,2.5,2.7,2.7]

genSeq.Pythia8B.NHadronizationLoops = 40
