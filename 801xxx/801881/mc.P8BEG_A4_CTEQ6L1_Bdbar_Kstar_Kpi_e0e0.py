##############################################################
# Uncut job options for B0bar -> anti-K*0 ee
##############################################################

evgenConfig.description   = "Exclusive B0bar->anti-K*(Kpi)(e0e0) decay production"
evgenConfig.process       = "B0bar -> anti-K*(kpi) ee"
evgenConfig.keywords      = [ "bottom", "B0", "2electron", "exclusive" ]
evgenConfig.contact       = [ "ann-kathrin.perrevoort@cern.ch" ]
evgenConfig.nEventsPerJob = 5000

# Create EvtGen decay rules
f = open("Bdbar_KstarKpi_EE_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_anti-K*0 anti-K*0\n")
f.write("Decay  my_anti-K*0\n")
f.write("1.0000   K- pi+   VSS;\n")
f.write("Enddecay\n")
f.write("Decay anti-B0\n")
f.write("1.0000   my_anti-K*0  e+ e-   PHOTOS BTOSLLBALL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -511 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bdbar_KstarKpi_EE_USER.DEC"
