# https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/share/DSID450xxx/MC15.450279.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_M3000_m80_m80.py
# W' -> WZ -> qqqq
# Wprime Mass (in GeV)

M_Wprime = 3000.
M_w = 570.
M_z = 570.



evgenConfig.contact = ["jroloff2@gmail.com"]
evgenConfig.description = "Wprime->WZ->qqqq mWprime = "+str(M_Wprime)+" GeV with NNPDF23LO PDF and A14 tune and m[W] = "+str(M_w)+", , m[Z] = "+str(M_z)
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZ>qqqq"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]# Allow Wprime
genSeq.Pythia8.Commands += ["Wprime:coup2WZ = 1."]    # Wprime Coupling to WZ
genSeq.Pythia8.Commands += ["34:m0 = "+str(M_Wprime)] # Wprime mass
genSeq.Pythia8.Commands += ["34:onMode = off"]# Turn off  decays
genSeq.Pythia8.Commands += ["34:onIfAll = 23 24"]# Turn on ->WZ
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all Z decays
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4"]# Turn on hadronic Z
genSeq.Pythia8.Commands += ["23:onMode = off"]# Turn off all W decays
genSeq.Pythia8.Commands += ["23:onIfAny = 1 2 3 4"]# Turn on hadronic W
genSeq.Pythia8.Commands += ["24:m0="+str(M_w)] #change the W mass
genSeq.Pythia8.Commands += ["23:m0="+str(M_z)] #change the Z mass

genSeq.Pythia8.Commands += ["24:doForceWidth=on"] #change the W width
genSeq.Pythia8.Commands += ["24:mWidth=0.1"]
genSeq.Pythia8.Commands += ["WeakZ0:gmZmode = 2"]
genSeq.Pythia8.Commands += ["23:doForceWidth=on"] #change the Z width
genSeq.Pythia8.Commands += ["23:mWidth=0.1"]
genSeq.Pythia8.Commands += ["34:doForceWidth=on"] #FIXED!
genSeq.Pythia8.Commands += ["34:mWidth=0.1"]

