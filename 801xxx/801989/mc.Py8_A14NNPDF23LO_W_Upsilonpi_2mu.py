#--------------------------------------------------------------
#Pythia8 pp/gg -> W -> Upsilon+pi ->2mu+pi production
#--------------------------------------------------------------

evgenConfig.nEventsPerJob = 10000
evgenConfig.description = "Pythia8 pp/gg -> W -> Upsilon+pi ->2mu+pi"
evgenConfig.keywords = ["W","Upsilon","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg -> W -> Upsilon+pi ->2mu+pi"
evgenConfig.generators += ['Pythia8']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")

genSeq.Pythia8.Commands += [
'WeakSingleBoson:ffbar2W = on',
'24:onMode = off',
'24:addChannel = 1 1.00 102 553 211',
'553:onMode = off',
'553:onIfMatch = 13 -13'
]

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   filtSeq += muonfilter1

filtSeq.muonfilter1.Ptcut = 3000.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 2 #minimum
