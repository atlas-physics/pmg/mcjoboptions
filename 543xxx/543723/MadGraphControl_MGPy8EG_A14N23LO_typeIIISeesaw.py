import subprocess
from fileinput import FileInput
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *

evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model'
evgenConfig.contact = ['miha.mali@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw', 'multilepton', 'multijet']


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#Ad the extra particles to the PDG file
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write('''
-8000020 #L- lepton
8000012 # electron neutrino
8000014 # muon neutrino
8000016 # tau neutrino
8000018 #N0 heavy neutrino
8000020 # L+ lepton
''')
pdgfile.close()

process = '''
import model typeIIIseesaw
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = v1 v2 v3
generate p p > tr0 tr+, tr0 > l+ W-, tr+ > vl W+
add process p p > tr0 tr+, tr0 > l+ W-, tr+ > l+ Z
add process p p > tr0 tr+, tr0 > l+ W-, tr+ > l+ h
add process p p > tr0 tr-, tr0 > l+ W-, tr- > vl W-
add process p p > tr0 tr-, tr0 > l+ W-, tr- > l- Z
add process p p > tr0 tr-, tr0 > l+ W-, tr- > l- h
add process p p > tr0 tr+, tr0 > l- W+, tr+ > vl W+
add process p p > tr0 tr+, tr0 > l- W+, tr+ > l+ Z
add process p p > tr0 tr+, tr0 > l- W+, tr+ > l+ h
add process p p > tr0 tr-, tr0 > l- W+, tr- > vl W-
add process p p > tr0 tr-, tr0 > l- W+, tr- > l- Z
add process p p > tr0 tr-, tr0 > l- W+, tr- > l- h
add process p p > tr0 tr+, tr0 > vl Z, tr+ > vl W+
add process p p > tr0 tr+, tr0 > vl Z, tr+ > l+ Z
add process p p > tr0 tr+, tr0 > vl Z, tr+ > l+ h
add process p p > tr0 tr-, tr0 > vl Z, tr- > vl W-
add process p p > tr0 tr-, tr0 > vl Z, tr- > l- Z
add process p p > tr0 tr-, tr0 > vl Z, tr- > l- h
add process p p > tr0 tr+, tr0 > vl h, tr+ > vl W+
add process p p > tr0 tr+, tr0 > vl h, tr+ > l+ Z
add process p p > tr0 tr+, tr0 > vl h, tr+ > l+ h
add process p p > tr0 tr-, tr0 > vl h, tr- > vl W-
add process p p > tr0 tr-, tr0 > vl h, tr- > l- Z
add process p p > tr0 tr-, tr0 > vl h, tr- > l- h
add process p p > tr+ tr-, tr+ > vl W+, tr- > vl W-
add process p p > tr+ tr-, tr+ > vl W+, tr- > l- Z
add process p p > tr+ tr-, tr+ > vl W+, tr- > l- h
add process p p > tr+ tr-, tr+ > l+ Z, tr- > vl W-
add process p p > tr+ tr-, tr+ > l+ Z, tr- > l- Z
add process p p > tr+ tr-, tr+ > l+ Z, tr- > l- h
add process p p > tr+ tr-, tr+ > l+ h, tr- > vl W-
add process p p > tr+ tr-, tr+ > l+ h, tr- > l- Z
add process p p > tr+ tr-, tr+ > l+ h, tr- > l- h
'''
process += '\noutput -f'


safefactor = 2  # generate the requested events due to filter efficiencies
safefactor = 5 if two_lepton_filter else safefactor
safefactor = 11 if three_lepton_filter else safefactor
safefactor = 100 if four_lepton_filter else safefactor
nevents = 5000 * safefactor
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents * safefactor


process_dir = new_process(process)
settings = {'ickkw': 0,
            'nevents':nevents,
            }
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params={
    'DECAY': {'Wtr0': 'Auto', 'Wtrch': 'Auto'},
    'MASS': {'mtr0': mLepton, 'mtrch': mLepton},  # set masses of heavy leptons
    'NEWMASSES': {'mtr': mLepton, 'mtrm': mLepton},
    'MIXING': {'Ve': 0.055000, 'Vm': 0.055000, 'Vtt': 0.055000}, #mixing
    # 'YUKAWA': {'yme': 1,'ymm': 1, 'ymtau':1, 'ymc':1,'ymb':1,'ymt':1} #Yukawa masses
    }

modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs)

# This is necessary for xAOD filters
# include ("GeneratorFilters/MultiLeptonWithParentFilter.py")

# Import the filters and add them to the filter sequence
if two_lepton_filter:
    include('GeneratorFilters/CreatexAODSlimContainers.py')
    from GeneratorFilters.GeneratorFiltersConf import xAODDecaysFinalStateFilter
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu0_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu1_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu2_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu0_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu1_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu0_tau0')
    filtSeq.Filter_e0_mu0_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu1_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu2_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu0_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu1_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu0_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu0_tau2.NElectrons = 0
    filtSeq.Filter_e0_mu0_tau2.NMuons = 0
    filtSeq.Filter_e0_mu0_tau2.NTaus = 2
    filtSeq.Filter_e0_mu1_tau1.NElectrons = 0
    filtSeq.Filter_e0_mu1_tau1.NMuons = 1
    filtSeq.Filter_e0_mu1_tau1.NTaus = 1
    filtSeq.Filter_e0_mu2_tau0.NElectrons = 0
    filtSeq.Filter_e0_mu2_tau0.NMuons = 2
    filtSeq.Filter_e0_mu2_tau0.NTaus = 0
    filtSeq.Filter_e1_mu0_tau1.NElectrons = 1
    filtSeq.Filter_e1_mu0_tau1.NMuons = 0
    filtSeq.Filter_e1_mu0_tau1.NTaus = 1
    filtSeq.Filter_e1_mu1_tau0.NElectrons = 1
    filtSeq.Filter_e1_mu1_tau0.NMuons = 1
    filtSeq.Filter_e1_mu1_tau0.NTaus = 0
    filtSeq.Filter_e2_mu0_tau0.NElectrons = 2
    filtSeq.Filter_e2_mu0_tau0.NMuons = 0
    filtSeq.Filter_e2_mu0_tau0.NTaus = 0
    filtSeq.Filter_e0_mu0_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu1_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu2_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu0_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu1_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu0_tau0.OutputLevel = DEBUG
    filtSeq.Expression = 'Filter_e0_mu0_tau2 or Filter_e0_mu1_tau1 or Filter_e0_mu2_tau0 or Filter_e1_mu0_tau1 or Filter_e1_mu1_tau0 or Filter_e2_mu0_tau0'
if three_lepton_filter:
    include('GeneratorFilters/CreatexAODSlimContainers.py')
    from GeneratorFilters.GeneratorFiltersConf import xAODDecaysFinalStateFilter
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu0_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu1_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu2_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu3_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu0_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu1_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu2_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu0_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu1_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu0_tau0')
    filtSeq.Filter_e0_mu0_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu1_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu2_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu3_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu0_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu1_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu2_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu0_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu1_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu0_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu0_tau3.NElectrons = 0
    filtSeq.Filter_e0_mu0_tau3.NMuons = 0
    filtSeq.Filter_e0_mu0_tau3.NTaus = 3
    filtSeq.Filter_e0_mu1_tau2.NElectrons = 0
    filtSeq.Filter_e0_mu1_tau2.NMuons = 1
    filtSeq.Filter_e0_mu1_tau2.NTaus = 2
    filtSeq.Filter_e0_mu2_tau1.NElectrons = 0
    filtSeq.Filter_e0_mu2_tau1.NMuons = 2
    filtSeq.Filter_e0_mu2_tau1.NTaus = 1
    filtSeq.Filter_e0_mu3_tau0.NElectrons = 0
    filtSeq.Filter_e0_mu3_tau0.NMuons = 3
    filtSeq.Filter_e0_mu3_tau0.NTaus = 0
    filtSeq.Filter_e1_mu0_tau2.NElectrons = 1
    filtSeq.Filter_e1_mu0_tau2.NMuons = 0
    filtSeq.Filter_e1_mu0_tau2.NTaus = 2
    filtSeq.Filter_e1_mu1_tau1.NElectrons = 1
    filtSeq.Filter_e1_mu1_tau1.NMuons = 1
    filtSeq.Filter_e1_mu1_tau1.NTaus = 1
    filtSeq.Filter_e1_mu2_tau0.NElectrons = 1
    filtSeq.Filter_e1_mu2_tau0.NMuons = 2
    filtSeq.Filter_e1_mu2_tau0.NTaus = 0
    filtSeq.Filter_e2_mu0_tau1.NElectrons = 2
    filtSeq.Filter_e2_mu0_tau1.NMuons = 0
    filtSeq.Filter_e2_mu0_tau1.NTaus = 1
    filtSeq.Filter_e2_mu1_tau0.NElectrons = 2
    filtSeq.Filter_e2_mu1_tau0.NMuons = 1
    filtSeq.Filter_e2_mu1_tau0.NTaus = 0
    filtSeq.Filter_e3_mu0_tau0.NElectrons = 3
    filtSeq.Filter_e3_mu0_tau0.NMuons = 0
    filtSeq.Filter_e3_mu0_tau0.NTaus = 0
    filtSeq.Filter_e0_mu0_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu1_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu2_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu3_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu0_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu1_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu2_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu0_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu1_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu0_tau0.OutputLevel = DEBUG
    filtSeq.Expression = 'Filter_e0_mu0_tau3 or Filter_e0_mu1_tau2 or Filter_e0_mu2_tau1 or Filter_e0_mu3_tau0 or Filter_e1_mu0_tau2 or Filter_e1_mu1_tau1 or Filter_e1_mu2_tau0 or Filter_e2_mu0_tau1 or Filter_e2_mu1_tau0 or Filter_e3_mu0_tau0'
if four_lepton_filter:
    include('GeneratorFilters/CreatexAODSlimContainers.py')
    from GeneratorFilters.GeneratorFiltersConf import xAODDecaysFinalStateFilter
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu0_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu1_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu2_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu3_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu4_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu0_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu1_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu2_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu3_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu0_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu1_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu2_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu0_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu1_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu0_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu0_tau5')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu1_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu2_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu3_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu4_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu5_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu0_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu1_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu2_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu3_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu4_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu0_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu1_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu2_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu3_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu0_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu1_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu2_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu0_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu1_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e5_mu0_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu0_tau6')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu1_tau5')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu2_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu3_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu4_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu5_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e0_mu6_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu0_tau5')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu1_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu2_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu3_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu4_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e1_mu5_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu0_tau4')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu1_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu2_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu3_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e2_mu4_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu0_tau3')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu1_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu2_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e3_mu3_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu0_tau2')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu1_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e4_mu2_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e5_mu0_tau1')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e5_mu1_tau0')
    filtSeq += xAODDecaysFinalStateFilter('Filter_e6_mu0_tau0')
    filtSeq.Filter_e0_mu0_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu1_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu2_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu3_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu4_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu0_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu1_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu2_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu3_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu0_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu1_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu2_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu0_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu1_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu0_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu0_tau5.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu1_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu2_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu3_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu4_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu5_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu0_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu1_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu2_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu3_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu4_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu0_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu1_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu2_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu3_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu0_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu1_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu2_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu0_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu1_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e5_mu0_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu0_tau6.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu1_tau5.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu2_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu3_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu4_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu5_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu6_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu0_tau5.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu1_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu2_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu3_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu4_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e1_mu5_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu0_tau4.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu1_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu2_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu3_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e2_mu4_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu0_tau3.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu1_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu2_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e3_mu3_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu0_tau2.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu1_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e4_mu2_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e5_mu0_tau1.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e5_mu1_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e6_mu0_tau0.PDGAllowedParents = [24, -24, 23, 25, 8000018, 8000020, -8000020]
    filtSeq.Filter_e0_mu0_tau4.NElectrons = 0
    filtSeq.Filter_e0_mu0_tau4.NMuons = 0
    filtSeq.Filter_e0_mu0_tau4.NTaus = 4
    filtSeq.Filter_e0_mu1_tau3.NElectrons = 0
    filtSeq.Filter_e0_mu1_tau3.NMuons = 1
    filtSeq.Filter_e0_mu1_tau3.NTaus = 3
    filtSeq.Filter_e0_mu2_tau2.NElectrons = 0
    filtSeq.Filter_e0_mu2_tau2.NMuons = 2
    filtSeq.Filter_e0_mu2_tau2.NTaus = 2
    filtSeq.Filter_e0_mu3_tau1.NElectrons = 0
    filtSeq.Filter_e0_mu3_tau1.NMuons = 3
    filtSeq.Filter_e0_mu3_tau1.NTaus = 1
    filtSeq.Filter_e0_mu4_tau0.NElectrons = 0
    filtSeq.Filter_e0_mu4_tau0.NMuons = 4
    filtSeq.Filter_e0_mu4_tau0.NTaus = 0
    filtSeq.Filter_e1_mu0_tau3.NElectrons = 1
    filtSeq.Filter_e1_mu0_tau3.NMuons = 0
    filtSeq.Filter_e1_mu0_tau3.NTaus = 3
    filtSeq.Filter_e1_mu1_tau2.NElectrons = 1
    filtSeq.Filter_e1_mu1_tau2.NMuons = 1
    filtSeq.Filter_e1_mu1_tau2.NTaus = 2
    filtSeq.Filter_e1_mu2_tau1.NElectrons = 1
    filtSeq.Filter_e1_mu2_tau1.NMuons = 2
    filtSeq.Filter_e1_mu2_tau1.NTaus = 1
    filtSeq.Filter_e1_mu3_tau0.NElectrons = 1
    filtSeq.Filter_e1_mu3_tau0.NMuons = 3
    filtSeq.Filter_e1_mu3_tau0.NTaus = 0
    filtSeq.Filter_e2_mu0_tau2.NElectrons = 2
    filtSeq.Filter_e2_mu0_tau2.NMuons = 0
    filtSeq.Filter_e2_mu0_tau2.NTaus = 2
    filtSeq.Filter_e2_mu1_tau1.NElectrons = 2
    filtSeq.Filter_e2_mu1_tau1.NMuons = 1
    filtSeq.Filter_e2_mu1_tau1.NTaus = 1
    filtSeq.Filter_e2_mu2_tau0.NElectrons = 2
    filtSeq.Filter_e2_mu2_tau0.NMuons = 2
    filtSeq.Filter_e2_mu2_tau0.NTaus = 0
    filtSeq.Filter_e3_mu0_tau1.NElectrons = 3
    filtSeq.Filter_e3_mu0_tau1.NMuons = 0
    filtSeq.Filter_e3_mu0_tau1.NTaus = 1
    filtSeq.Filter_e3_mu1_tau0.NElectrons = 3
    filtSeq.Filter_e3_mu1_tau0.NMuons = 1
    filtSeq.Filter_e3_mu1_tau0.NTaus = 0
    filtSeq.Filter_e4_mu0_tau0.NElectrons = 4
    filtSeq.Filter_e4_mu0_tau0.NMuons = 0
    filtSeq.Filter_e4_mu0_tau0.NTaus = 0
    filtSeq.Filter_e0_mu0_tau5.NElectrons = 0
    filtSeq.Filter_e0_mu0_tau5.NMuons = 0
    filtSeq.Filter_e0_mu0_tau5.NTaus = 5
    filtSeq.Filter_e0_mu1_tau4.NElectrons = 0
    filtSeq.Filter_e0_mu1_tau4.NMuons = 1
    filtSeq.Filter_e0_mu1_tau4.NTaus = 4
    filtSeq.Filter_e0_mu2_tau3.NElectrons = 0
    filtSeq.Filter_e0_mu2_tau3.NMuons = 2
    filtSeq.Filter_e0_mu2_tau3.NTaus = 3
    filtSeq.Filter_e0_mu3_tau2.NElectrons = 0
    filtSeq.Filter_e0_mu3_tau2.NMuons = 3
    filtSeq.Filter_e0_mu3_tau2.NTaus = 2
    filtSeq.Filter_e0_mu4_tau1.NElectrons = 0
    filtSeq.Filter_e0_mu4_tau1.NMuons = 4
    filtSeq.Filter_e0_mu4_tau1.NTaus = 1
    filtSeq.Filter_e0_mu5_tau0.NElectrons = 0
    filtSeq.Filter_e0_mu5_tau0.NMuons = 5
    filtSeq.Filter_e0_mu5_tau0.NTaus = 0
    filtSeq.Filter_e1_mu0_tau4.NElectrons = 1
    filtSeq.Filter_e1_mu0_tau4.NMuons = 0
    filtSeq.Filter_e1_mu0_tau4.NTaus = 4
    filtSeq.Filter_e1_mu1_tau3.NElectrons = 1
    filtSeq.Filter_e1_mu1_tau3.NMuons = 1
    filtSeq.Filter_e1_mu1_tau3.NTaus = 3
    filtSeq.Filter_e1_mu2_tau2.NElectrons = 1
    filtSeq.Filter_e1_mu2_tau2.NMuons = 2
    filtSeq.Filter_e1_mu2_tau2.NTaus = 2
    filtSeq.Filter_e1_mu3_tau1.NElectrons = 1
    filtSeq.Filter_e1_mu3_tau1.NMuons = 3
    filtSeq.Filter_e1_mu3_tau1.NTaus = 1
    filtSeq.Filter_e1_mu4_tau0.NElectrons = 1
    filtSeq.Filter_e1_mu4_tau0.NMuons = 4
    filtSeq.Filter_e1_mu4_tau0.NTaus = 0
    filtSeq.Filter_e2_mu0_tau3.NElectrons = 2
    filtSeq.Filter_e2_mu0_tau3.NMuons = 0
    filtSeq.Filter_e2_mu0_tau3.NTaus = 3
    filtSeq.Filter_e2_mu1_tau2.NElectrons = 2
    filtSeq.Filter_e2_mu1_tau2.NMuons = 1
    filtSeq.Filter_e2_mu1_tau2.NTaus = 2
    filtSeq.Filter_e2_mu2_tau1.NElectrons = 2
    filtSeq.Filter_e2_mu2_tau1.NMuons = 2
    filtSeq.Filter_e2_mu2_tau1.NTaus = 1
    filtSeq.Filter_e2_mu3_tau0.NElectrons = 2
    filtSeq.Filter_e2_mu3_tau0.NMuons = 3
    filtSeq.Filter_e2_mu3_tau0.NTaus = 0
    filtSeq.Filter_e3_mu0_tau2.NElectrons = 3
    filtSeq.Filter_e3_mu0_tau2.NMuons = 0
    filtSeq.Filter_e3_mu0_tau2.NTaus = 2
    filtSeq.Filter_e3_mu1_tau1.NElectrons = 3
    filtSeq.Filter_e3_mu1_tau1.NMuons = 1
    filtSeq.Filter_e3_mu1_tau1.NTaus = 1
    filtSeq.Filter_e3_mu2_tau0.NElectrons = 3
    filtSeq.Filter_e3_mu2_tau0.NMuons = 2
    filtSeq.Filter_e3_mu2_tau0.NTaus = 0
    filtSeq.Filter_e4_mu0_tau1.NElectrons = 4
    filtSeq.Filter_e4_mu0_tau1.NMuons = 0
    filtSeq.Filter_e4_mu0_tau1.NTaus = 1
    filtSeq.Filter_e4_mu1_tau0.NElectrons = 4
    filtSeq.Filter_e4_mu1_tau0.NMuons = 1
    filtSeq.Filter_e4_mu1_tau0.NTaus = 0
    filtSeq.Filter_e5_mu0_tau0.NElectrons = 5
    filtSeq.Filter_e5_mu0_tau0.NMuons = 0
    filtSeq.Filter_e5_mu0_tau0.NTaus = 0
    filtSeq.Filter_e0_mu0_tau6.NElectrons = 0
    filtSeq.Filter_e0_mu0_tau6.NMuons = 0
    filtSeq.Filter_e0_mu0_tau6.NTaus = 6
    filtSeq.Filter_e0_mu1_tau5.NElectrons = 0
    filtSeq.Filter_e0_mu1_tau5.NMuons = 1
    filtSeq.Filter_e0_mu1_tau5.NTaus = 5
    filtSeq.Filter_e0_mu2_tau4.NElectrons = 0
    filtSeq.Filter_e0_mu2_tau4.NMuons = 2
    filtSeq.Filter_e0_mu2_tau4.NTaus = 4
    filtSeq.Filter_e0_mu3_tau3.NElectrons = 0
    filtSeq.Filter_e0_mu3_tau3.NMuons = 3
    filtSeq.Filter_e0_mu3_tau3.NTaus = 3
    filtSeq.Filter_e0_mu4_tau2.NElectrons = 0
    filtSeq.Filter_e0_mu4_tau2.NMuons = 4
    filtSeq.Filter_e0_mu4_tau2.NTaus = 2
    filtSeq.Filter_e0_mu5_tau1.NElectrons = 0
    filtSeq.Filter_e0_mu5_tau1.NMuons = 5
    filtSeq.Filter_e0_mu5_tau1.NTaus = 1
    filtSeq.Filter_e0_mu6_tau0.NElectrons = 0
    filtSeq.Filter_e0_mu6_tau0.NMuons = 6
    filtSeq.Filter_e0_mu6_tau0.NTaus = 0
    filtSeq.Filter_e1_mu0_tau5.NElectrons = 1
    filtSeq.Filter_e1_mu0_tau5.NMuons = 0
    filtSeq.Filter_e1_mu0_tau5.NTaus = 5
    filtSeq.Filter_e1_mu1_tau4.NElectrons = 1
    filtSeq.Filter_e1_mu1_tau4.NMuons = 1
    filtSeq.Filter_e1_mu1_tau4.NTaus = 4
    filtSeq.Filter_e1_mu2_tau3.NElectrons = 1
    filtSeq.Filter_e1_mu2_tau3.NMuons = 2
    filtSeq.Filter_e1_mu2_tau3.NTaus = 3
    filtSeq.Filter_e1_mu3_tau2.NElectrons = 1
    filtSeq.Filter_e1_mu3_tau2.NMuons = 3
    filtSeq.Filter_e1_mu3_tau2.NTaus = 2
    filtSeq.Filter_e1_mu4_tau1.NElectrons = 1
    filtSeq.Filter_e1_mu4_tau1.NMuons = 4
    filtSeq.Filter_e1_mu4_tau1.NTaus = 1
    filtSeq.Filter_e1_mu5_tau0.NElectrons = 1
    filtSeq.Filter_e1_mu5_tau0.NMuons = 5
    filtSeq.Filter_e1_mu5_tau0.NTaus = 0
    filtSeq.Filter_e2_mu0_tau4.NElectrons = 2
    filtSeq.Filter_e2_mu0_tau4.NMuons = 0
    filtSeq.Filter_e2_mu0_tau4.NTaus = 4
    filtSeq.Filter_e2_mu1_tau3.NElectrons = 2
    filtSeq.Filter_e2_mu1_tau3.NMuons = 1
    filtSeq.Filter_e2_mu1_tau3.NTaus = 3
    filtSeq.Filter_e2_mu2_tau2.NElectrons = 2
    filtSeq.Filter_e2_mu2_tau2.NMuons = 2
    filtSeq.Filter_e2_mu2_tau2.NTaus = 2
    filtSeq.Filter_e2_mu3_tau1.NElectrons = 2
    filtSeq.Filter_e2_mu3_tau1.NMuons = 3
    filtSeq.Filter_e2_mu3_tau1.NTaus = 1
    filtSeq.Filter_e2_mu4_tau0.NElectrons = 2
    filtSeq.Filter_e2_mu4_tau0.NMuons = 4
    filtSeq.Filter_e2_mu4_tau0.NTaus = 0
    filtSeq.Filter_e3_mu0_tau3.NElectrons = 3
    filtSeq.Filter_e3_mu0_tau3.NMuons = 0
    filtSeq.Filter_e3_mu0_tau3.NTaus = 3
    filtSeq.Filter_e3_mu1_tau2.NElectrons = 3
    filtSeq.Filter_e3_mu1_tau2.NMuons = 1
    filtSeq.Filter_e3_mu1_tau2.NTaus = 2
    filtSeq.Filter_e3_mu2_tau1.NElectrons = 3
    filtSeq.Filter_e3_mu2_tau1.NMuons = 2
    filtSeq.Filter_e3_mu2_tau1.NTaus = 1
    filtSeq.Filter_e3_mu3_tau0.NElectrons = 3
    filtSeq.Filter_e3_mu3_tau0.NMuons = 3
    filtSeq.Filter_e3_mu3_tau0.NTaus = 0
    filtSeq.Filter_e4_mu0_tau2.NElectrons = 4
    filtSeq.Filter_e4_mu0_tau2.NMuons = 0
    filtSeq.Filter_e4_mu0_tau2.NTaus = 2
    filtSeq.Filter_e4_mu1_tau1.NElectrons = 4
    filtSeq.Filter_e4_mu1_tau1.NMuons = 1
    filtSeq.Filter_e4_mu1_tau1.NTaus = 1
    filtSeq.Filter_e4_mu2_tau0.NElectrons = 4
    filtSeq.Filter_e4_mu2_tau0.NMuons = 2
    filtSeq.Filter_e4_mu2_tau0.NTaus = 0
    filtSeq.Filter_e5_mu0_tau1.NElectrons = 5
    filtSeq.Filter_e5_mu0_tau1.NMuons = 0
    filtSeq.Filter_e5_mu0_tau1.NTaus = 1
    filtSeq.Filter_e5_mu1_tau0.NElectrons = 5
    filtSeq.Filter_e5_mu1_tau0.NMuons = 1
    filtSeq.Filter_e5_mu1_tau0.NTaus = 0
    filtSeq.Filter_e6_mu0_tau0.NElectrons = 6
    filtSeq.Filter_e6_mu0_tau0.NMuons = 0
    filtSeq.Filter_e6_mu0_tau0.NTaus = 0
    filtSeq.Filter_e0_mu0_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu1_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu2_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu3_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu4_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu0_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu1_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu2_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu3_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu0_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu1_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu2_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu0_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu1_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu0_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu0_tau5.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu1_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu2_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu3_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu4_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu5_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu0_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu1_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu2_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu3_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu4_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu0_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu1_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu2_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu3_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu0_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu1_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu2_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu0_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu1_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e5_mu0_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu0_tau6.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu1_tau5.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu2_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu3_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu4_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu5_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e0_mu6_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu0_tau5.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu1_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu2_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu3_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu4_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e1_mu5_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu0_tau4.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu1_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu2_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu3_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e2_mu4_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu0_tau3.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu1_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu2_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e3_mu3_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu0_tau2.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu1_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e4_mu2_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e5_mu0_tau1.OutputLevel = DEBUG
    filtSeq.Filter_e5_mu1_tau0.OutputLevel = DEBUG
    filtSeq.Filter_e6_mu0_tau0.OutputLevel = DEBUG
    filtSeq.Expression = 'Filter_e0_mu0_tau4 or Filter_e0_mu1_tau3 or Filter_e0_mu2_tau2 or Filter_e0_mu3_tau1 or Filter_e0_mu4_tau0 or Filter_e1_mu0_tau3 or Filter_e1_mu1_tau2 or Filter_e1_mu2_tau1 or Filter_e1_mu3_tau0 or Filter_e2_mu0_tau2 or Filter_e2_mu1_tau1 or Filter_e2_mu2_tau0 or Filter_e3_mu0_tau1 or Filter_e3_mu1_tau0 or Filter_e4_mu0_tau0 or Filter_e0_mu0_tau5 or Filter_e0_mu1_tau4 or Filter_e0_mu2_tau3 or Filter_e0_mu3_tau2 or Filter_e0_mu4_tau1 or Filter_e0_mu5_tau0 or Filter_e1_mu0_tau4 or Filter_e1_mu1_tau3 or Filter_e1_mu2_tau2 or Filter_e1_mu3_tau1 or Filter_e1_mu4_tau0 or Filter_e2_mu0_tau3 or Filter_e2_mu1_tau2 or Filter_e2_mu2_tau1 or Filter_e2_mu3_tau0 or Filter_e3_mu0_tau2 or Filter_e3_mu1_tau1 or Filter_e3_mu2_tau0 or Filter_e4_mu0_tau1 or Filter_e4_mu1_tau0 or Filter_e5_mu0_tau0 or Filter_e0_mu0_tau6 or Filter_e0_mu1_tau5 or Filter_e0_mu2_tau4 or Filter_e0_mu3_tau3 or Filter_e0_mu4_tau2 or Filter_e0_mu5_tau1 or Filter_e0_mu6_tau0 or Filter_e1_mu0_tau5 or Filter_e1_mu1_tau4 or Filter_e1_mu2_tau3 or Filter_e1_mu3_tau2 or Filter_e1_mu4_tau1 or Filter_e1_mu5_tau0 or Filter_e2_mu0_tau4 or Filter_e2_mu1_tau3 or Filter_e2_mu2_tau2 or Filter_e2_mu3_tau1 or Filter_e2_mu4_tau0 or Filter_e3_mu0_tau3 or Filter_e3_mu1_tau2 or Filter_e3_mu2_tau1 or Filter_e3_mu3_tau0 or Filter_e4_mu0_tau2 or Filter_e4_mu1_tau1 or Filter_e4_mu2_tau0 or Filter_e5_mu0_tau1 or Filter_e5_mu1_tau0 or Filter_e6_mu0_tau0'