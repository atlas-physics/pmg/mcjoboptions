
# mass of the seesaw heavy leptons
mLepton = 1100.0

# filters
two_lepton_filter = False
three_lepton_filter = True
four_lepton_filter = False

# load configuration
include('MadGraphControl_MGPy8EG_A14N23LO_typeIIISeesaw.py')

# metadata
evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model, Mass: 1100 GeV, Ve=Vu=Vt=0.055, three leptons'
evgenConfig.contact = ['miha.mali@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw', '3lepton']

                