
# mass of the seesaw heavy leptons
mLepton = 1400.0

# filters
two_lepton_filter = False
three_lepton_filter = False
four_lepton_filter = True

# load configuration
include('MadGraphControl_MGPy8EG_A14N23LO_typeIIISeesaw.py')

# metadata
evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model, Mass: 1400 GeV, Ve=Vu=Vt=0.055, 4+ leptons'
evgenConfig.contact = ['miha.mali@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw', '4lepton']

                