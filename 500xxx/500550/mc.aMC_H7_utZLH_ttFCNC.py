from MadGraphControl.MadGraphUtils import *
import os,subprocess,fileinput
from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

# General settings

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

print(runArgs)


name = 'ttFCNC'
runName='mc.aMC_H7_utZLH_'+str(name)

process = """
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/TopFCNC -modelname
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define qq~ = u~ c~
define qq = u c
generate p p > t t~ [QCD]
output -f"""

process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '3.0',
           'parton_shower' :'HERWIGPP',
           'nevents':int(nevents),
           }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#Parameter card

couplings = { 
	      '2': 1.0e-6,
	      '3': 1.0e-6,
	      '4': 1.0e-6,
	      '5': 1.0e-6,
	      '6': 1.0e-6,
	      '7': 1.0e-6,
	      '8': 1.0e-6,
	      '9': 1.0e-6,
	      '10': 1.0e-6,
	      '11': 1.0e-6,
	      '12': 1.0e-6,
	      '13': 1.0e-6,
	      '14': 1.0e-6,
	      '15': 5.0,
	      '16': 1.0e-6,
	      '17': -5.0,
	      '18': 1.0e-6,
	      '19': 1.0e-6,
	      '20': 1.0e-6,
	      '21': 1.0e-6,
	      '22': 1.0e-6,
	      '23': 1.0e-6,
	      '24': 1.0e-6,
	      '25': 1.0e-6,
	      '26': 1.0e-6,
	      '27': 1.0e-6,
	      '28': 1.0e-6,
	      '29': 1.0e-6,
	      '30': 1.0e-6,
	      '31': 1.0e-6,
	      '32': 1.0e-6,
	      '33': 1.0e-6,
	      '34': 1.0e-6,
	      '35': 1.0e-6,
	      '36': 1.0e-6,
	      '37': 1.0e-6,
	      '38': 1.0e-6,
	      '39': 1.0e-6,
	      '40': 1.0e-6,
	      '41': 1.0e-6,
	      '42': 1.0e-6,
	      '43': 1.0e-6,
	      '44': 1.0e-6,
	      '45': 1.0e-6,
            }


decays={'6':"""DECAY  6 1.570043e+00 #WT
         #  BR             NDA  ID1    ID2   ...
         9.367896e-01   2   5  24 
         6.321037e-02   2   2  23 """}

modify_param_card(param_card_input=None,process_dir=process_dir,params={'DECAY':decays,'DIM6':couplings})
#modify_param_card(param_card_input=None,process_dir=process_dir,params={'DIM6':couplings})

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > l+ vl
decay t~ > z u~, z > l+ l-
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

print_cards()

generate(runArgs=runArgs,process_dir=process_dir)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.nEventsPerJob = 10000
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.keywords+= ['FCNC', 'top', 'Z','lepton']
evgenConfig.contact = ['Ana Peixoto <ana.peixoto@cern.ch>']
evgenConfig.tune = "H7.1-Default"
runArgs.inputGeneratorFile=outputDS+".events"

check_reset_proc_number(opts)
include("Herwig7_i/Herwig7_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=4)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
