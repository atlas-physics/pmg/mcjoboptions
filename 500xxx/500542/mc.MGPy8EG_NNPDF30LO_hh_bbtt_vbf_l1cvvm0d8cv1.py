import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

safefactor=1.1/0.12
nevents=10000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor


mode=0
#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1.0000000',  # CV
                     'C2V': '-0.8000000',  # C2V
                     'C3':  '1.0000000'}  # C3

parameters['MASS']={'25':'1.250000e+02'} #MH 

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'nevents':int(nevents)}


#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
# Due to the parameters settings above, the resonant contribution is eliminated
# Thus, the non-resonant hh production with different trilinear Higgs couplings is enabled
#---------------------------------------------------------------------------------------------------


process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HHVBF_UFO
generate p p > h h j j $$ z w+ w- / a j QED=4
output -f"""

process_dir = new_process(process)
#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
#               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0,extras=extras)


#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
#build_param_card(param_card_old='%s/Cards/param_card.dat' % process_dir,param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)
   
###
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
modify_param_card(process_dir=process_dir,params=parameters)
print_cards()
#runName='run_01'     
#generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
generate(process_dir=process_dir,runArgs=runArgs)

#arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)  

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["MadGraph", "Pythia8"]
evgenConfig.description = "Non-resonant di-Higgs production through vector-boson-fusion (VBF) which decays to bbtautau (lephad)."

evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","tau","VBF"]
evgenConfig.contact = ['Caterina Vernieri <caterina.vernieri@cern.ch>']

evgenConfig.nEventsPerJob = 10000

#---------------------------------------------------------------------------------------------------

# Decaying hh to bbtautau with Pythia8

#---------------------------------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5", #bb decay
                            "25:addChannel = on 0.5 100 15 -15" ] # tautau decay

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Filter for bbtautau
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HTauTauFilter", PDGParent = [25], PDGChild = [15])

from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended("TauTauLepHadFilter")
filtSeq.TauTauLepHadFilter.PDGGrandParent = 25
filtSeq.TauTauLepHadFilter.PDGParent = 15
filtSeq.TauTauLepHadFilter.StatusParent = 2
filtSeq.TauTauLepHadFilter.PDGChild1 = [11,13]
filtSeq.TauTauLepHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]



#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.IncludeHadTaus = True
filtSeq.LepTauPtFilter.NLeptons = 2
filtSeq.LepTauPtFilter.MinPt = 13000.
filtSeq.LepTauPtFilter.MinVisPtHadTau = 15000.
filtSeq.LepTauPtFilter.MaxEta = 3.

filtSeq.Expression = "HbbFilter and HTauTauFilter and TauTauLepHadFilter and LepTauPtFilter"

