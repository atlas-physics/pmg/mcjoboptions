model="darkHiggs"
fs = "ee"
mDM1 = 5.
mDM2 = 5.
mZp = 700.
mHD = 700.

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
