from MadGraphControl.MadGraphUtils import *
import math
import os
import re
import MadGraphControl.MadGraph_PDF4LHC15NLO_Base_Fragment

#Parse information from jobConfig
THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]     
jobname = [f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]

## Format of the jobOptions is expected to be
##mc.MGPy8EG_{WpTb,WTb,Tb}_{had,lep}_{LH,RH}_{MASS}_{Coupling}.py
##WpTb = W' in schannel only
##WTb = s-channel single top only
##Tb = everything allowed (including interference)

parameters = re.split(r'[._]',jobname)

if not (len(parameters) == 8 or len(parameters) == 9):
        raise RuntimeError('Format of jobOptions not recognised, number of parametesr incorrect')

str_proc = parameters[2]
str_tdecay = parameters[3]
str_chir = parameters[4]
str_mass = parameters[5]
str_coupling = parameters[6]

##Deactivate rweighting
doRW = True
if len(parameters) == 9:
    if parameters[7] == "noRW":
        doRW=False
    else:
        raise RuntimeError('Extra option not recognised, only noRW allowed')
        
## Some outputs to allow for easy debugging
print ("Parameters parsed from the jobOptions")
print ("W\' Mass:  ", str_mass) 
print ("Coupling factor " , str(int(str_coupling) * 0.1))
print ("Process ", str_proc) 
print ("Top decay ", str_tdecay) 
print ("Chirality ", str_chir) 
if doRW:
    print ("Will do reweighting")
else:
    print ("Will not do reweighting")
print ("Will attempt to configure the job now")

gSM = (6.483972e-01)
gSF = int(str_coupling) * 0.1
MWp = int(str_mass)

## Coupling setting
rh_sigma = 0.026
lh_sigma = 0.0345
## Sigma value is chosen so that coupling=1 corresponds to ZTOP calculation
if str_chir == 'RH':
    gR = gSM*gSF
    gL = 0.0
    sigma = rh_sigma # This is for right handed
    gRL='gR'
elif str_chir == 'LH':
    gR = 0.0
    gL = gSM*gSF
    sigma = lh_sigma # This is for left handed
    gRL='gL'
else:
    raise RuntimeError('Chirality not recognised in jobname, check the jobOptions')

## Width depends on coupling factor too
WWp = (MWp*sigma)*(gSF)**2

## Setting process and top decay
if str_proc == 'WpTb' and str_tdecay == 'had':
    mg_proc="""generate p p > t b~ QED=0 NP=2, (t > b W+, W+ > j j)
add process p p > t~ b QED=0 NP=2, (t~ > b~ W-, W- > j j)
    """
elif str_proc == 'WpTb' and str_tdecay == 'lep':
    mg_proc="""generate p p > t b~ QED=0 NP=2, (t > b W+, W+ > l+ vl)
add process p p > t~ b QED=0 NP=2, (t~ > b~ W-, W- > l- vl~)
    """
elif str_proc == 'WTb' and str_tdecay == 'had':
    mg_proc="""generate p p > t b~ QED=2 NP=0, (t > b W+, W+ > j j)
add process p p > t~ b QED=2 NP=0, (t~ > b~ W-, W- > j j)
    """
elif str_proc == 'WTb' and str_tdecay == 'lep':
    mg_proc="""generate p p > t b~ QED=2 NP=0, (t > b W+, W+ > l+ vl)
add process p p > t~ b QED=2 NP=0, (t~ > b~ W-, W- > l- vl~)
    """
elif str_proc == 'Tb' and str_tdecay == 'had':
    mg_proc="""generate p p > t b~ QED=2 NP=2, (t > b W+, W+ > j j)
add process p p > t~ b QED=2 NP=2, (t~ > b~ W-, W- > j j)
    """
elif str_proc == 'Tb' and str_tdecay == 'lep':
    mg_proc="""generate p p > t b~ QED=2 NP=2, (t > b W+, W+ > l+ vl)
add process p p > t~ b QED=2 NP=2, (t~ > b~ W-, W- > l- vl~)
    """
else:
    raise RuntimeError('Process or top decay not recognised in jobname, check the jobOptions')

##Full process
process="""
import model WEff_UFO
define p = g u c d s u~ c~ d~ s~
define j = u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+mg_proc+"""
output -f
"""

process_dir = new_process(process)
           
## Define settings for the run card
nevents = 1.2*runArgs.maxEvents
settings = {
	    'cut_decays' :'F',
	    'bwcutoff':25,
	    'ptj':0.,
	    'ptl':0.,
	    'etaj':-1,
	    'etab':-1,
	    'etal':-1,
	    'drjj':0.,
	    'drll':0.,
	    'drjl':0.,
	    'auto_ptj_mjj':'T',
        'nevents':int(nevents)
	    }

params = {}
params['MASS']={'MWp': str(MWp)}
params['DECAY']= {'WWp': str(WWp)}
params['WPCOUP']={'gL' : str(gL),'gR' : str(gR)}

####Card modifications
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)
print_cards()


####Do reweighting
def weight_str(gRL,name,coupling,m_sigma):
    temp_str = """
launch --rwgt_name="""+name+"""
    set """+gRL+""" """+str(gSM*coupling)+"""
    set WWp """+str((MWp*m_sigma)*(coupling)**2)+"""
"""
    return temp_str

l_couplings = [0.05,0.1,0.2,0.3,0.4,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0]
l_names = ['gSF005','gSF01','gSF02','gSF03','gSF04','gSF05','gSF10','gSF15','gSF20','gSF25','gSF30','gSF35','gSF40','gSF45','gSF50']

rw_str=""

###Full set of LH and RH couplings
for coupling,name in zip(l_couplings,l_names):
    rw_str+=weight_str(gRL=gRL,name=name,coupling=coupling,m_sigma=sigma)

if doRW:
    rwcard = open(process_dir +'/Cards/reweight_card.dat','w')
    rwcard.write(rw_str)
    rwcard.close()

###########Generating and preparing for Pythia
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_MadGraph.py")

#Information Strings
evgenConfig.description = 'MadGraph Wprime->tb'
evgenConfig.keywords=[ 'BSM', 'Wprime', 'heavyBoson', 'resonance']
evgenConfig.contact=['hector.de.la.torre.perez@cern.ch']
evgenConfig.nEventsPerJob = 10000
