from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'MadGraph configuration to produce a flat-pT Zprime sample, showered with Pythia8'
evgenConfig.keywords = ['Zprime','jets']
evgenConfig.contact = ['philipp.windischhofer@cern.ch']

nevents = 1.1 * runArgs.maxEvents if runArgs.maxEvents > 0 else 11000

# always run MadGraph locally
MadGraph_mode = 0

# extra options used for generation
aux_opts = { 'lhe_version':'2.0', 
             'cut_decays':'F', 
             'pdlabel':"nn23lo1",
             'use_syst':"False"}

fcard_skeleton = """
import model Zp_flat_pT
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > v1 > all all
output -f
"""

# prepare the MadGraph script to set up the generation process
fcard = open('proc_card_mg5.dat', 'w')
fcard.write(fcard_skeleton)
fcard.close()

# make sure that the beam energy is set correctly
beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")
    
runName = 'run_01'
process_dir = new_process()
build_run_card(run_card_old = get_default_runcard(proc_dir = process_dir), 
               run_card_new = 'run_card.dat', nevts = int(nevents),
               rand_seed = runArgs.randomSeed, beamEnergy = beamEnergy,
               extras = aux_opts)
print_cards()

outputDS = runName + '._00001.events.tar.gz'
generate(run_card_loc = 'run_card.dat', param_card_loc = None, mode = MadGraph_mode, proc_dir = process_dir, run_name = runName, nevents = nevents)
arrange_output(run_name = runName, proc_dir = process_dir, outputDS = outputDS, lhe_version = 3, saveProcDir = True)  

testSeq.TestHepMC.MaxVtxDisp = 1500.0

# setup Pythia8 showering
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile = outputDS
