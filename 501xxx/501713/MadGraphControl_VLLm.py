from MadGraphControl.MadGraphUtils import *
import math
import os


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


nevents=int(3.0*runArgs.maxEvents)


process_string=str(0)
process_string = """
                import model sm
                import model VLLD_NLO --modelname
                generate p p > taup taup~, taup > z mu-, taup~ > z mu+
                add process p p > taup taup~, taup > h mu-, taup~ > h mu+
                add process p p > taup taup~, taup > z mu-, taup~ > h mu+
                add process p p > taup taup~, taup > h mu-, taup~ > z mu+
                add process p p > taup nup~, taup > h mu-, nup~ > w- mu+
                add process p p > nup taup~, nup > w+ mu-, taup~ > h mu+
                add process p p > taup nup~, taup > z mu-, nup~ > w- mu+
                add process p p > nup taup~, nup > w+ mu-, taup~ > z mu+
                add process p p > nup nup~, nup > w+ mu-, nup~ > w- mu+
                output -f
                """
process_dir = new_process(process_string)

model_pars_str = str(jofile) 
mtaup=int(0) 

for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    ss=s.replace("MVLL","")  
    print(ss)
    if ss.isdigit():
        mtaup = int(ss)        


# Set up the run card with mostly default values
run_card_extras = {
   'pdlabel' : 'lhapdf',
    'lhaid' : 247000,
    'scale' : str(mtaup),
    'dsqrt_q2fact1' : str(mtaup),
    'dsqrt_q2fact2' : str(mtaup),
    'nevents' :int(nevents)
}
params = {}
# Set masses
params['MASS'] = {'17':mtaup}
params['Couplings'] = {'1':'0','2':'0.1','3':'0'}
# params['VECTORLIKE'] = {'1','0.04'}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras)
modify_param_card(process_dir=process_dir,params=params) #param_card_extras

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
# Change to serial mode for Pythia8
# if 'ATHENA_PROC_NUMBER' in os.environ:
#     njobs = os.environ.pop('ATHENA_PROC_NUMBER')
#     if hasattr(opts,'nprocs'):
#         opts.nprocs = 0

# Shower
evgenConfig.description = 'MadGraph5+Pythia8 for Vectorlike leptons of mass {} GeV, inc lepton final state'.format(mtaup)
evgenConfig.contact = ['Merve Nazlim Agaras <merve.nazlim.agaras@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton']
evgenConfig.generators += ['MadGraph','Pythia8']
# runArgs.inputGeneratorFile=output_DS

# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut = 15000.0#MeV


include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")
