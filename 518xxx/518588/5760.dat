# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  11:41
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.71151273E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.47961740E+03  # scale for input parameters
    1   -1.96829091E+03  # M_1
    2   -1.90709822E+03  # M_2
    3    2.12405465E+03  # M_3
   11   -1.36495343E+03  # A_t
   12    3.56878037E+03  # A_b
   13   -3.81106355E+03  # A_tau
   23    2.37724529E+02  # mu
   25    3.60890423E+01  # tan(beta)
   26    8.50647446E+02  # m_A, pole mass
   31    5.92650567E+02  # M_L11
   32    5.92650567E+02  # M_L22
   33    1.53024772E+03  # M_L33
   34    1.71114489E+03  # M_E11
   35    1.71114489E+03  # M_E22
   36    9.79068448E+02  # M_E33
   41    3.76148362E+03  # M_Q11
   42    3.76148362E+03  # M_Q22
   43    2.26130059E+03  # M_Q33
   44    2.77669778E+03  # M_U11
   45    2.77669778E+03  # M_U22
   46    2.60597124E+03  # M_U33
   47    2.65829116E+03  # M_D11
   48    2.65829116E+03  # M_D22
   49    1.23726031E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.47961740E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.47961740E+03  # (SUSY scale)
  1  1     8.38759029E-06   # Y_u(Q)^DRbar
  2  2     4.26089586E-03   # Y_c(Q)^DRbar
  3  3     1.01328828E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.47961740E+03  # (SUSY scale)
  1  1     6.08264979E-04   # Y_d(Q)^DRbar
  2  2     1.15570346E-02   # Y_s(Q)^DRbar
  3  3     6.03208257E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.47961740E+03  # (SUSY scale)
  1  1     1.06146687E-04   # Y_e(Q)^DRbar
  2  2     2.19477683E-02   # Y_mu(Q)^DRbar
  3  3     3.69125357E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.47961740E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.36495342E+03   # A_t(Q)^DRbar
Block Ad Q=  2.47961740E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.56878041E+03   # A_b(Q)^DRbar
Block Ae Q=  2.47961740E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -3.81106348E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.47961740E+03  # soft SUSY breaking masses at Q
   1   -1.96829091E+03  # M_1
   2   -1.90709822E+03  # M_2
   3    2.12405465E+03  # M_3
  21    7.28097666E+05  # M^2_(H,d)
  22    4.45902393E+04  # M^2_(H,u)
  31    5.92650567E+02  # M_(L,11)
  32    5.92650567E+02  # M_(L,22)
  33    1.53024772E+03  # M_(L,33)
  34    1.71114489E+03  # M_(E,11)
  35    1.71114489E+03  # M_(E,22)
  36    9.79068448E+02  # M_(E,33)
  41    3.76148362E+03  # M_(Q,11)
  42    3.76148362E+03  # M_(Q,22)
  43    2.26130059E+03  # M_(Q,33)
  44    2.77669778E+03  # M_(U,11)
  45    2.77669778E+03  # M_(U,22)
  46    2.60597124E+03  # M_(U,33)
  47    2.65829116E+03  # M_(D,11)
  48    2.65829116E+03  # M_(D,22)
  49    1.23726031E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18664790E+02  # h0
        35     8.50649316E+02  # H0
        36     8.50647446E+02  # A0
        37     8.53993697E+02  # H+
   1000001     3.82304704E+03  # ~d_L
   2000001     2.72848800E+03  # ~d_R
   1000002     3.82232484E+03  # ~u_L
   2000002     2.84410158E+03  # ~u_R
   1000003     3.82304491E+03  # ~s_L
   2000003     2.72848449E+03  # ~s_R
   1000004     3.82232276E+03  # ~c_L
   2000004     2.84410122E+03  # ~c_R
   1000005     1.33608047E+03  # ~b_1
   2000005     2.32466673E+03  # ~b_2
   1000006     2.32185368E+03  # ~t_1
   2000006     2.64810074E+03  # ~t_2
   1000011     6.36293718E+02  # ~e_L-
   2000011     1.72234145E+03  # ~e_R-
   1000012     6.30841205E+02  # ~nu_eL
   1000013     6.36271579E+02  # ~mu_L-
   2000013     1.72232097E+03  # ~mu_R-
   1000014     6.30827929E+02  # ~nu_muL
   1000015     9.67520723E+02  # ~tau_1-
   2000015     1.54013756E+03  # ~tau_2-
   1000016     1.53758513E+03  # ~nu_tauL
   1000021     2.34817839E+03  # ~g
   1000022     2.41403081E+02  # ~chi_10
   1000023     2.45490761E+02  # ~chi_20
   1000025     1.93580310E+03  # ~chi_30
   1000035     1.94172171E+03  # ~chi_40
   1000024     2.43710638E+02  # ~chi_1+
   1000037     1.93669324E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.76937119E-02   # alpha
Block Hmix Q=  2.47961740E+03  # Higgs mixing parameters
   1    2.37724529E+02  # mu
   2    3.60890423E+01  # tan[beta](Q)
   3    2.43477512E+02  # v(Q)
   4    7.23601077E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.91270262E-01   # Re[R_st(1,1)]
   1  2     1.31845620E-01   # Re[R_st(1,2)]
   2  1    -1.31845620E-01   # Re[R_st(2,1)]
   2  2     9.91270262E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.11530165E-03   # Re[R_sb(1,1)]
   1  2     9.99995147E-01   # Re[R_sb(1,2)]
   2  1    -9.99995147E-01   # Re[R_sb(2,1)]
   2  2     3.11530165E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.52377375E-02   # Re[R_sta(1,1)]
   1  2     9.99883899E-01   # Re[R_sta(1,2)]
   2  1    -9.99883899E-01   # Re[R_sta(2,1)]
   2  2     1.52377375E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.77554534E-02   # Re[N(1,1)]
   1  2     3.19266336E-02   # Re[N(1,2)]
   1  3    -7.09955748E-01   # Re[N(1,3)]
   1  4    -7.03298138E-01   # Re[N(1,4)]
   2  1     1.46950938E-02   # Re[N(2,1)]
   2  2    -2.65510626E-02   # Re[N(2,2)]
   2  3    -7.04231459E-01   # Re[N(2,3)]
   2  4     7.09321611E-01   # Re[N(2,4)]
   3  1     5.31451556E-02   # Re[N(3,1)]
   3  2     9.97775190E-01   # Re[N(3,2)]
   3  3     3.84837107E-03   # Re[N(3,3)]
   3  4     4.00680905E-02   # Re[N(3,4)]
   4  1    -9.98320785E-01   # Re[N(4,1)]
   4  2     5.21574591E-02   # Re[N(4,2)]
   4  3     2.46550119E-03   # Re[N(4,3)]
   4  4     2.50824687E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -5.63925758E-03   # Re[U(1,1)]
   1  2     9.99984099E-01   # Re[U(1,2)]
   2  1    -9.99984099E-01   # Re[U(2,1)]
   2  2    -5.63925758E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.85542864E-02   # Re[V(1,1)]
   1  2     9.98284226E-01   # Re[V(1,2)]
   2  1     9.98284226E-01   # Re[V(2,1)]
   2  2    -5.85542864E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     1.87300180E-03   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.51890680E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.78858427E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     6.92466421E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     4.41653734E-03   # ~e^-_R
#    BR                NDA      ID1      ID2
     5.93749536E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.06165804E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
DECAY   1000013     6.31567499E-03   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.20195716E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.59268179E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.05348548E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.62532693E-02   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.92872637E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.66189719E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.37216041E-01    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     3.71492861E-03    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     4.60416509E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.53314592E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.47031242E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.99654167E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     4.09618302E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.88456976E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.81581088E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.50044930E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.49324795E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.96164863E-02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.76858779E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.20621633E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.02519588E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     2.39933385E-02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.44591162E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     9.86143410E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.56794497E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     4.14666075E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.66266748E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.83429698E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     9.65931651E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.95713848E-02    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     1.48569794E+01   # ~d_R
#    BR                NDA      ID1      ID2
     2.48925730E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.74979736E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     1.45236760E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.41608916E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.25767440E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.37278026E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.28045986E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.05757717E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     1.48709737E+01   # ~s_R
#    BR                NDA      ID1      ID2
     2.73689888E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.59655582E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.48689864E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     4.79770369E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.74046533E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     1.45247923E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.76471565E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.32152826E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.25718530E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.37251691E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.28035979E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.05694247E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.77517293E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.57048144E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.52209811E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.90742046E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     7.82796120E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.06152076E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.04383435E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.20005430E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.32091872E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.84571112E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.32298725E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.56958390E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     8.43121162E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     2.54008380E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     8.43077869E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     2.45622457E+01   # ~u_R
#    BR                NDA      ID1      ID2
     2.10397997E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     7.34600006E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.26194241E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     1.45195051E+02   # ~u_L
#    BR                NDA      ID1      ID2
     6.49860031E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.00650314E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.88304491E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.27590271E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.05467977E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     2.45641242E+01   # ~c_R
#    BR                NDA      ID1      ID2
     1.00737977E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.10405847E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     7.34541638E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.26117844E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     1.45206123E+02   # ~c_L
#    BR                NDA      ID1      ID2
     6.49809168E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.00642972E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.57431969E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.27580374E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.05404496E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.88990357E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.84662339E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.89627119E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.01160867E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.12113550E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.09841730E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.40409090E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.04752786E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
     1.63218458E-01    2     1000005        37   # BR(~t_1 -> ~b_1 H^+)
#    BR                NDA      ID1      ID2       ID3
     1.75926016E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.18904015E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.21689426E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.25090969E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.47798053E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.45235453E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.48599567E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     2.02437593E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.13172838E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     9.92521372E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.21841754E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.72509037E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     5.29894009E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.13562068E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.76644938E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.74814475E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     5.08450961E-03    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     5.74183631E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.18136691E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.18138681E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     2.00572813E-02    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     1.18222124E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.18223315E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     2.02210173E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     7.01635077E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     7.13288491E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.72783241E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.80810160E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     7.07198206E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.85131016E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     7.23720490E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.84899580E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.00207472E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.00695148E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.94907284E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.23978483E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     7.72461508E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     3.70161118E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.67058322E-03    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     4.74612738E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     4.71642503E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.07106359E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.06055773E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.31066870E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     6.34728357E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     6.72813448E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     6.72813448E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     2.24290382E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     2.24290382E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     2.20408988E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     2.20408988E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     5.81266146E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.14021616E-02    2     1000011       -11   # BR(chi^0_3 -> ~e^-_L e^+)
     6.14021616E-02    2    -1000011        11   # BR(chi^0_3 -> ~e^+_L e^-)
     6.14031959E-02    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     6.14031959E-02    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.39601885E-04    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.39601885E-04    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.03939068E-02    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     1.03939068E-02    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     5.50216077E-02    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     5.50216077E-02    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     5.50221580E-02    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     5.50221580E-02    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     9.38327850E-03    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     9.38327850E-03    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     7.42295394E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     7.42295394E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.87026037E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     4.87026037E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     2.62757369E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.92613117E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.47302428E-02    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.60796850E-02    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     3.95780878E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     4.51792466E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     3.07428360E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.72003451E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.52609121E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.59121646E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     3.13251153E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.13251153E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.44673598E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.22175153E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.22175153E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     9.03207749E-03    2     2000011       -11   # BR(chi^0_4 -> ~e^-_R e^+)
     9.03207749E-03    2    -2000011        11   # BR(chi^0_4 -> ~e^+_R e^-)
     3.22180584E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.22180584E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     9.03366113E-03    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     9.03366113E-03    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     1.12256849E-01    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.12256849E-01    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     5.56949006E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     5.56949006E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     4.78970742E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     4.78970742E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     4.78975500E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     4.78975500E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     8.32707644E-03    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     8.32707644E-03    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.83602379E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     1.83602379E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     4.20876771E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.20876771E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.55969932E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     2.55969932E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.45694016E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.70480790E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.28677207E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.40478177E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     3.43787039E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.92453512E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.67774343E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     2.36931781E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.41794053E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.47758584E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.73868321E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.73868321E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.22009664E+01   # ~g
#    BR                NDA      ID1      ID2
     2.53382828E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.53382828E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     4.93547199E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.93547199E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.22188805E-04    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.22188805E-04    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     4.08364742E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.14125983E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.60128537E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.60128537E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.33794785E-03   # Gamma(h0)
     2.19696863E-03   2        22        22   # BR(h0 -> photon photon)
     9.35494072E-04   2        22        23   # BR(h0 -> photon Z)
     1.38635759E-02   2        23        23   # BR(h0 -> Z Z)
     1.29944059E-01   2       -24        24   # BR(h0 -> W W)
     7.24925632E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.96702303E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.65419675E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.65808885E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.62615569E-07   2        -2         2   # BR(h0 -> Up up)
     3.15519439E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.97596657E-07   2        -1         1   # BR(h0 -> Down down)
     2.52305057E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.71915916E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.62358109E+01   # Gamma(HH)
     4.11744921E-08   2        22        22   # BR(HH -> photon photon)
     1.93034899E-08   2        22        23   # BR(HH -> photon Z)
     4.19916319E-06   2        23        23   # BR(HH -> Z Z)
     6.64833648E-06   2       -24        24   # BR(HH -> W W)
     6.56824527E-05   2        21        21   # BR(HH -> gluon gluon)
     1.06838755E-08   2       -11        11   # BR(HH -> Electron electron)
     4.75512508E-04   2       -13        13   # BR(HH -> Muon muon)
     1.42183592E-01   2       -15        15   # BR(HH -> Tau tau)
     1.21034624E-13   2        -2         2   # BR(HH -> Up up)
     2.34682241E-08   2        -4         4   # BR(HH -> Charm charm)
     1.31576071E-03   2        -6         6   # BR(HH -> Top top)
     9.43330456E-07   2        -1         1   # BR(HH -> Down down)
     3.41207954E-04   2        -3         3   # BR(HH -> Strange strange)
     8.54309073E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.91822140E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.32166827E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.89348532E-06   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.37507638E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.38956756E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.58466951E+01   # Gamma(A0)
     7.25412974E-08   2        22        22   # BR(A0 -> photon photon)
     4.44097514E-08   2        22        23   # BR(A0 -> photon Z)
     1.01172958E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.06753825E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.75134528E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.42072035E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.13428757E-13   2        -2         2   # BR(A0 -> Up up)
     2.19857045E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.48726977E-03   2        -6         6   # BR(A0 -> Top top)
     9.42574057E-07   2        -1         1   # BR(A0 -> Down down)
     3.40934287E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.53658936E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.31357008E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.07771057E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.86371270E-06   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.30582952E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     6.63810965E-06   2        23        25   # BR(A0 -> Z h0)
     9.25492985E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.47911662E+01   # Gamma(Hp)
     1.31526963E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.62318829E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.59054479E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.30932395E-07   2        -1         2   # BR(Hp -> Down up)
     1.54638921E-05   2        -3         2   # BR(Hp -> Strange up)
     9.42435357E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.39229278E-08   2        -1         4   # BR(Hp -> Down charm)
     3.35526586E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.31974135E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.15515093E-07   2        -1         6   # BR(Hp -> Down top)
     2.97098295E-06   2        -3         6   # BR(Hp -> Strange top)
     8.38531253E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.42757486E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.06181806E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     7.26075084E-06   2        24        25   # BR(Hp -> W h0)
     3.83385201E-11   2        24        35   # BR(Hp -> W HH)
     3.84457377E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.99390806E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.30241958E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.30241897E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000047E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.67334341E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.67802082E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.99390806E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.30241958E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.30241897E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    1.00000000E+00        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.12579671E-11        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    1.00000000E+00        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.12579671E-11        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26119273E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.04710229E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.49274919E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.12579671E-11        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    1.00000000E+00        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.48904556E-04   # BR(b -> s gamma)
    2    1.58673594E-06   # BR(b -> s mu+ mu-)
    3    3.52298671E-05   # BR(b -> s nu nu)
    4    2.99241680E-15   # BR(Bd -> e+ e-)
    5    1.27831168E-10   # BR(Bd -> mu+ mu-)
    6    2.66703364E-08   # BR(Bd -> tau+ tau-)
    7    9.87962391E-14   # BR(Bs -> e+ e-)
    8    4.22053381E-09   # BR(Bs -> mu+ mu-)
    9    8.92824979E-07   # BR(Bs -> tau+ tau-)
   10    8.66941491E-05   # BR(B_u -> tau nu)
   11    8.95517370E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42391954E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93323753E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15940264E-03   # epsilon_K
   17    2.28169231E-15   # Delta(M_K)
   18    2.47871474E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28701774E-11   # BR(K^+ -> pi^+ nu nu)
   20   -6.35253473E-15   # Delta(g-2)_electron/2
   21   -2.71588823E-10   # Delta(g-2)_muon/2
   22   -4.93484307E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.32124446E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.14469994E-01   # C7
     0305 4322   00   2    -6.68798581E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.38997476E-01   # C8
     0305 6321   00   2    -9.39222090E-04   # C8'
 03051111 4133   00   0     1.61006793E+00   # C9 e+e-
 03051111 4133   00   2     1.61100960E+00   # C9 e+e-
 03051111 4233   00   2     1.49006889E-05   # C9' e+e-
 03051111 4137   00   0    -4.43276003E+00   # C10 e+e-
 03051111 4137   00   2    -4.42884857E+00   # C10 e+e-
 03051111 4237   00   2    -1.08253772E-04   # C10' e+e-
 03051313 4133   00   0     1.61006793E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61100691E+00   # C9 mu+mu-
 03051313 4233   00   2     1.48829079E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43276003E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42885126E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.08239088E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50450385E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.34992493E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50450385E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.35029692E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50450468E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.45507689E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85826271E-07   # C7
     0305 4422   00   2    -6.87659499E-06   # C7
     0305 4322   00   2     3.25700458E-06   # C7'
     0305 6421   00   0     3.30485717E-07   # C8
     0305 6421   00   2    -2.43483696E-05   # C8
     0305 6321   00   2     7.68909210E-07   # C8'
 03051111 4133   00   2    -7.06411807E-07   # C9 e+e-
 03051111 4233   00   2     4.17556044E-06   # C9' e+e-
 03051111 4137   00   2     6.25977469E-06   # C10 e+e-
 03051111 4237   00   2    -3.15919117E-05   # C10' e+e-
 03051313 4133   00   2    -7.06391803E-07   # C9 mu+mu-
 03051313 4233   00   2     4.17554954E-06   # C9' mu+mu-
 03051313 4137   00   2     6.25979642E-06   # C10 mu+mu-
 03051313 4237   00   2    -3.15919562E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.31061867E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     6.85702814E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.31061976E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     6.85702814E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.29631934E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     6.85701970E-06   # C11' nu_3 nu_3
