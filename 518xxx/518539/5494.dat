# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  11:35
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.15046905E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.32496940E+03  # scale for input parameters
    1   -7.14942831E+02  # M_1
    2   -1.35108492E+03  # M_2
    3    1.28332191E+03  # M_3
   11   -3.47278634E+03  # A_t
   12   -7.46383027E+02  # A_b
   13   -5.72333228E+02  # A_tau
   23    8.74635996E+02  # mu
   25    3.04789684E+01  # tan(beta)
   26    9.95405156E+02  # m_A, pole mass
   31    7.74158642E+02  # M_L11
   32    7.74158642E+02  # M_L22
   33    1.67831980E+03  # M_L33
   34    1.49757732E+03  # M_E11
   35    1.49757732E+03  # M_E22
   36    8.00895776E+02  # M_E33
   41    8.40931960E+02  # M_Q11
   42    8.40931960E+02  # M_Q22
   43    1.46458722E+03  # M_Q33
   44    2.83896321E+03  # M_U11
   45    2.83896321E+03  # M_U22
   46    3.53837033E+03  # M_U33
   47    3.71253532E+03  # M_D11
   48    3.71253532E+03  # M_D22
   49    3.37197162E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.32496940E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.32496940E+03  # (SUSY scale)
  1  1     8.38888366E-06   # Y_u(Q)^DRbar
  2  2     4.26155290E-03   # Y_c(Q)^DRbar
  3  3     1.01344453E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.32496940E+03  # (SUSY scale)
  1  1     5.13788859E-04   # Y_d(Q)^DRbar
  2  2     9.76198831E-03   # Y_s(Q)^DRbar
  3  3     5.09517550E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.32496940E+03  # (SUSY scale)
  1  1     8.96599129E-05   # Y_e(Q)^DRbar
  2  2     1.85388264E-02   # Y_mu(Q)^DRbar
  3  3     3.11792561E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.32496940E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.47278632E+03   # A_t(Q)^DRbar
Block Ad Q=  2.32496940E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -7.46383068E+02   # A_b(Q)^DRbar
Block Ae Q=  2.32496940E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -5.72333227E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.32496940E+03  # soft SUSY breaking masses at Q
   1   -7.14942831E+02  # M_1
   2   -1.35108492E+03  # M_2
   3    1.28332191E+03  # M_3
  21    2.14938996E+05  # M^2_(H,d)
  22   -7.82721938E+05  # M^2_(H,u)
  31    7.74158642E+02  # M_(L,11)
  32    7.74158642E+02  # M_(L,22)
  33    1.67831980E+03  # M_(L,33)
  34    1.49757732E+03  # M_(E,11)
  35    1.49757732E+03  # M_(E,22)
  36    8.00895776E+02  # M_(E,33)
  41    8.40931960E+02  # M_(Q,11)
  42    8.40931960E+02  # M_(Q,22)
  43    1.46458722E+03  # M_(Q,33)
  44    2.83896321E+03  # M_(U,11)
  45    2.83896321E+03  # M_(U,22)
  46    3.53837033E+03  # M_(U,33)
  47    3.71253532E+03  # M_(D,11)
  48    3.71253532E+03  # M_(D,22)
  49    3.37197162E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22526709E+02  # h0
        35     9.95447825E+02  # H0
        36     9.95405156E+02  # A0
        37     9.99331202E+02  # H+
   1000001     9.68249434E+02  # ~d_L
   2000001     3.75448383E+03  # ~d_R
   1000002     9.64986964E+02  # ~u_L
   2000002     2.87863073E+03  # ~u_R
   1000003     9.68249013E+02  # ~s_L
   2000003     3.75448200E+03  # ~s_R
   1000004     9.64986798E+02  # ~c_L
   2000004     2.87863035E+03  # ~c_R
   1000005     1.53373923E+03  # ~b_1
   2000005     3.40592766E+03  # ~b_2
   1000006     1.53080854E+03  # ~t_1
   2000006     3.53112917E+03  # ~t_2
   1000011     7.97352393E+02  # ~e_L-
   2000011     1.50360600E+03  # ~e_R-
   1000012     7.93038467E+02  # ~nu_eL
   1000013     7.97341895E+02  # ~mu_L-
   2000013     1.50359330E+03  # ~mu_R-
   1000014     7.93031024E+02  # ~nu_muL
   1000015     8.06765931E+02  # ~tau_1-
   2000015     1.68816773E+03  # ~tau_2-
   1000016     1.68517245E+03  # ~nu_tauL
   1000021     1.45384146E+03  # ~g
   1000022     7.03006923E+02  # ~chi_10
   1000023     8.84135485E+02  # ~chi_20
   1000025     8.87231166E+02  # ~chi_30
   1000035     1.34142430E+03  # ~chi_40
   1000024     8.80924588E+02  # ~chi_1+
   1000037     1.34151937E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.22808917E-02   # alpha
Block Hmix Q=  2.32496940E+03  # Higgs mixing parameters
   1    8.74635996E+02  # mu
   2    3.04789684E+01  # tan[beta](Q)
   3    2.43536340E+02  # v(Q)
   4    9.90831425E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.98755149E-01   # Re[R_st(1,1)]
   1  2     4.98813911E-02   # Re[R_st(1,2)]
   2  1    -4.98813911E-02   # Re[R_st(2,1)]
   2  2     9.98755149E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99980090E-01   # Re[R_sb(1,1)]
   1  2     6.31034286E-03   # Re[R_sb(1,2)]
   2  1    -6.31034286E-03   # Re[R_sb(2,1)]
   2  2     9.99980090E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.18020555E-02   # Re[R_sta(1,1)]
   1  2     9.99762307E-01   # Re[R_sta(1,2)]
   2  1    -9.99762307E-01   # Re[R_sta(2,1)]
   2  2     2.18020555E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.85990662E-01   # Re[N(1,1)]
   1  2     1.19212256E-02   # Re[N(1,2)]
   1  3    -1.31000400E-01   # Re[N(1,3)]
   1  4    -1.02563119E-01   # Re[N(1,4)]
   2  1    -1.65472763E-01   # Re[N(2,1)]
   2  2    -1.14471219E-01   # Re[N(2,2)]
   2  3     6.93113515E-01   # Re[N(2,3)]
   2  4     6.92176827E-01   # Re[N(2,4)]
   3  1     1.99006711E-02   # Re[N(3,1)]
   3  2    -2.59190227E-02   # Re[N(3,2)]
   3  3    -7.06015561E-01   # Re[N(3,3)]
   3  4     7.07442008E-01   # Re[N(3,4)]
   4  1    -6.71876409E-03   # Re[N(4,1)]
   4  2     9.93016832E-01   # Re[N(4,2)]
   4  3     6.30442497E-02   # Re[N(4,3)]
   4  4     9.94879498E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.99743598E-02   # Re[U(1,1)]
   1  2     9.95944082E-01   # Re[U(1,2)]
   2  1    -9.95944082E-01   # Re[U(2,1)]
   2  2    -8.99743598E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.41485386E-01   # Re[V(1,1)]
   1  2     9.89940344E-01   # Re[V(1,2)]
   2  1     9.89940344E-01   # Re[V(2,1)]
   2  2    -1.41485386E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     4.61092344E-02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999950E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     4.57450155E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.80362417E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.93565403E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.77908100E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     4.61057241E-02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99999950E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.58444949E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.78240267E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.97758974E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     7.52289794E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.59403832E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     2.67916383E-04    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     2.29613715E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     6.96110865E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.97649269E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.41356338E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.23043413E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.42482620E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     7.19471310E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.87969801E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     5.03985494E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     4.99052970E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     4.64542272E-02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     4.64473923E-02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     6.98246102E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.10276506E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.79276651E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.41421693E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.41611513E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.67206220E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.79703214E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     1.00081849E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   1000001     3.64907644E-02   # ~d_L
#    BR                NDA      ID1      ID2
     9.17980890E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.33112663E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.60868099E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.60991629E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
DECAY   2000001     2.16092583E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.78047795E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.36997087E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.90978643E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     3.65496716E-02   # ~s_L
#    BR                NDA      ID1      ID2
     9.16671899E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.39176933E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.25327759E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.61571301E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
DECAY   2000003     2.16105245E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.78048020E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.51064556E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.90919751E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     1.84878824E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.27774197E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.28721698E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.46598523E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.00697221E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     6.88911639E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.33424636E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     6.73667337E-02    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.14356474E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.13007785E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.44896478E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.55669691E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.77980498E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.04041676E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.61657185E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     8.38759576E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.52337594E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.34666941E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.25995549E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.20359092E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     2.27148969E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.20484273E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     3.20929135E-02   # ~u_L
#    BR                NDA      ID1      ID2
     7.80597995E-01    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.06780419E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.58494828E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.47139014E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000002     1.31761497E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.19422847E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.09574400E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.56944796E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     3.21565537E-02   # ~c_L
#    BR                NDA      ID1      ID2
     7.79022429E-01    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.07626756E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.72703603E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.48487859E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   2000004     1.31763187E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.19418192E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.09879228E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.56931410E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     1.74129327E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.29947766E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.69665680E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.67374103E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.22806185E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.02644053E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.16777745E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     4.15530079E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.16160286E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.91529108E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     7.30455045E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.61654876E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.39400082E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.50540182E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.50603258E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.56476072E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     9.53887317E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.52741865E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     4.76665171E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.77537839E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     6.82550312E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.76133230E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     4.19894300E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     2.44252534E-03    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     5.27583684E-02    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     6.64359072E-03    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     6.89707723E-03    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     9.31258085E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     3.41292424E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.17776836E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     7.17797485E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     7.17534176E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     7.17554123E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.18451072E-01    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     1.18451459E-01    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     1.18157116E-01    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     1.18156106E-01    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     2.77827338E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     6.18115850E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     5.78747126E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     5.81118986E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.01591132E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.80984780E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.88342940E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.40336739E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.65461076E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.05126018E-02    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.05126018E-02    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.07092937E-02    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.07092937E-02    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     4.90881119E-02    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     4.90881119E-02    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     1.90343248E-04    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.90343248E-04    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.90372663E-04    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.90372663E-04    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     2.23901649E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.36223964E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     7.27963257E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.75682441E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.75682441E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.44853630E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     1.44853630E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     1.44875240E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     1.44875240E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     5.90430914E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.73588559E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     3.68147003E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.28295444E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.28295444E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.28306556E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.28306556E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     3.37015689E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.37015689E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.37022487E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.37022487E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     5.50934027E-02    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     5.50934027E-02    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     5.50936623E-02    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     5.50936623E-02    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     5.45581697E-02    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     5.45581697E-02    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     5.45583236E-02    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     5.45583236E-02    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     6.00104934E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     6.00104934E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.69698487E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.53470532E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.72811256E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     5.28898383E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.25328723E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.61357076E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.61357076E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.36009499E+01   # ~g
#    BR                NDA      ID1      ID2
     1.25634071E-01    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     1.25634071E-01    2    -1000002         2   # BR(~g -> ~u^*_L u)
     1.25633870E-01    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     1.25633870E-01    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.24297401E-01    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     1.24297401E-01    2    -1000001         1   # BR(~g -> ~d^*_L d)
     1.24297571E-01    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     1.24297571E-01    2    -1000003         3   # BR(~g -> ~s^*_L s)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.66132514E-03   # Gamma(h0)
     2.28392419E-03   2        22        22   # BR(h0 -> photon photon)
     1.26167398E-03   2        22        23   # BR(h0 -> photon Z)
     2.13563759E-02   2        23        23   # BR(h0 -> Z Z)
     1.85426672E-01   2       -24        24   # BR(h0 -> W W)
     7.25936232E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.49808209E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.44563098E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.05666573E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.52038605E-07   2        -2         2   # BR(h0 -> Up up)
     2.95016871E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.36851545E-07   2        -1         1   # BR(h0 -> Down down)
     2.30336053E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.16533693E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.29657298E+01   # Gamma(HH)
     1.95730731E-08   2        22        22   # BR(HH -> photon photon)
     5.06863096E-09   2        22        23   # BR(HH -> photon Z)
     4.82415315E-06   2        23        23   # BR(HH -> Z Z)
     6.50836732E-06   2       -24        24   # BR(HH -> W W)
     4.96367273E-05   2        21        21   # BR(HH -> gluon gluon)
     1.06376050E-08   2       -11        11   # BR(HH -> Electron electron)
     4.73475979E-04   2       -13        13   # BR(HH -> Muon muon)
     1.43586669E-01   2       -15        15   # BR(HH -> Tau tau)
     1.86381023E-13   2        -2         2   # BR(HH -> Up up)
     3.61497343E-08   2        -4         4   # BR(HH -> Charm charm)
     2.77632733E-03   2        -6         6   # BR(HH -> Top top)
     7.70791674E-07   2        -1         1   # BR(HH -> Down down)
     2.78837093E-04   2        -3         3   # BR(HH -> Strange strange)
     8.52803224E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.96546039E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.26716343E+01   # Gamma(A0)
     1.40361167E-07   2        22        22   # BR(A0 -> photon photon)
     9.12699542E-08   2        22        23   # BR(A0 -> photon Z)
     9.25956976E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.06353444E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.73375219E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.43555666E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.72447867E-13   2        -2         2   # BR(A0 -> Up up)
     3.34234504E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.96482292E-03   2        -6         6   # BR(A0 -> Top top)
     7.70636944E-07   2        -1         1   # BR(A0 -> Down down)
     2.78781161E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.52626519E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.19393278E-06   2        23        25   # BR(A0 -> Z h0)
     5.66192722E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.23606227E+01   # Gamma(Hp)
     1.32702824E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.67346009E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.60476814E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.25867618E-07   2        -1         2   # BR(Hp -> Down up)
     1.20865943E-05   2        -3         2   # BR(Hp -> Strange up)
     9.19883576E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.56062390E-08   2        -1         4   # BR(Hp -> Down charm)
     2.61694704E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.28816249E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.25261072E-07   2        -1         6   # BR(Hp -> Down top)
     5.27323072E-06   2        -3         6   # BR(Hp -> Strange top)
     8.37370913E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.51086597E-06   2        24        25   # BR(Hp -> W h0)
     9.68804432E-11   2        24        35   # BR(Hp -> W HH)
     1.02317557E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.68741635E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    9.28998773E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    9.28967515E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003365E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.04281541E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.07646391E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.68741635E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    9.28998773E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    9.28967515E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999733E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.67136524E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999733E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.67136524E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26067969E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.16594558E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.28609639E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.67136524E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999733E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.36034752E-04   # BR(b -> s gamma)
    2    1.58706255E-06   # BR(b -> s mu+ mu-)
    3    3.52204651E-05   # BR(b -> s nu nu)
    4    2.81943205E-15   # BR(Bd -> e+ e-)
    5    1.20441527E-10   # BR(Bd -> mu+ mu-)
    6    2.51297226E-08   # BR(Bd -> tau+ tau-)
    7    9.20109349E-14   # BR(Bs -> e+ e-)
    8    3.93066805E-09   # BR(Bs -> mu+ mu-)
    9    8.31554551E-07   # BR(Bs -> tau+ tau-)
   10    9.19944417E-05   # BR(B_u -> tau nu)
   11    9.50267363E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41997199E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93415759E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15750167E-03   # epsilon_K
   17    2.28167113E-15   # Delta(M_K)
   18    2.47912033E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28530625E-11   # BR(K^+ -> pi^+ nu nu)
   20   -6.39869675E-15   # Delta(g-2)_electron/2
   21   -2.73565571E-10   # Delta(g-2)_muon/2
   22   -4.65484337E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.65248352E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98634420E-01   # C7
     0305 4322   00   2    -4.18478291E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.34472867E-01   # C8
     0305 6321   00   2    -6.35363618E-04   # C8'
 03051111 4133   00   0     1.60761563E+00   # C9 e+e-
 03051111 4133   00   2     1.60808479E+00   # C9 e+e-
 03051111 4233   00   2     4.42639037E-04   # C9' e+e-
 03051111 4137   00   0    -4.43030773E+00   # C10 e+e-
 03051111 4137   00   2    -4.42555420E+00   # C10 e+e-
 03051111 4237   00   2    -3.36808071E-03   # C10' e+e-
 03051313 4133   00   0     1.60761563E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60808346E+00   # C9 mu+mu-
 03051313 4233   00   2     4.42632719E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43030773E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42555554E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.36807948E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50432653E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.31447459E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50432653E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.31448604E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50434140E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.31770320E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85856005E-07   # C7
     0305 4422   00   2    -5.86970295E-05   # C7
     0305 4322   00   2     1.12867275E-05   # C7'
     0305 6421   00   0     3.30511187E-07   # C8
     0305 6421   00   2    -2.50530834E-04   # C8
     0305 6321   00   2     2.92559615E-06   # C8'
 03051111 4133   00   2    -1.14426361E-07   # C9 e+e-
 03051111 4233   00   2     1.01748493E-05   # C9' e+e-
 03051111 4137   00   2     4.52667666E-05   # C10 e+e-
 03051111 4237   00   2    -7.74351765E-05   # C10' e+e-
 03051313 4133   00   2    -1.14312515E-07   # C9 mu+mu-
 03051313 4233   00   2     1.01748319E-05   # C9' mu+mu-
 03051313 4137   00   2     4.52668674E-05   # C10 mu+mu-
 03051313 4237   00   2    -7.74352499E-05   # C10' mu+mu-
 03051212 4137   00   2    -9.62886576E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.68166012E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.62887782E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.68166012E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.36370399E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.68165833E-05   # C11' nu_3 nu_3
