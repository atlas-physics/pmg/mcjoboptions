# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  15:29
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.30479659E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.04060289E+03  # scale for input parameters
    1   -1.85263237E+02  # M_1
    2   -1.09187612E+03  # M_2
    3    3.05263287E+03  # M_3
   11   -5.94605929E+03  # A_t
   12    4.61690949E+02  # A_b
   13   -1.76678608E+03  # A_tau
   23   -4.63495152E+02  # mu
   25    1.24245514E+01  # tan(beta)
   26    2.25056837E+03  # m_A, pole mass
   31    7.14193877E+02  # M_L11
   32    7.14193877E+02  # M_L22
   33    5.33752914E+02  # M_L33
   34    1.07381861E+03  # M_E11
   35    1.07381861E+03  # M_E22
   36    5.14297973E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.97964252E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.21745972E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.42094825E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.04060289E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.04060289E+03  # (SUSY scale)
  1  1     8.41148513E-06   # Y_u(Q)^DRbar
  2  2     4.27303445E-03   # Y_c(Q)^DRbar
  3  3     1.01617497E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.04060289E+03  # (SUSY scale)
  1  1     2.10006940E-04   # Y_d(Q)^DRbar
  2  2     3.99013185E-03   # Y_s(Q)^DRbar
  3  3     2.08261077E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.04060289E+03  # (SUSY scale)
  1  1     3.66477466E-05   # Y_e(Q)^DRbar
  2  2     7.57759169E-03   # Y_mu(Q)^DRbar
  3  3     1.27442627E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.04060289E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.94606564E+03   # A_t(Q)^DRbar
Block Ad Q=  4.04060289E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     4.61690844E+02   # A_b(Q)^DRbar
Block Ae Q=  4.04060289E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.76678550E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.04060289E+03  # soft SUSY breaking masses at Q
   1   -1.85263237E+02  # M_1
   2   -1.09187612E+03  # M_2
   3    3.05263287E+03  # M_3
  21    4.81109310E+06  # M^2_(H,d)
  22    1.11010207E+05  # M^2_(H,u)
  31    7.14193877E+02  # M_(L,11)
  32    7.14193877E+02  # M_(L,22)
  33    5.33752914E+02  # M_(L,33)
  34    1.07381861E+03  # M_(E,11)
  35    1.07381861E+03  # M_(E,22)
  36    5.14297973E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.97964252E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.21745972E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.42094825E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25697655E+02  # h0
        35     2.25041836E+03  # H0
        36     2.25056837E+03  # A0
        37     2.25228536E+03  # H+
   1000001     1.01104445E+04  # ~d_L
   2000001     1.00858059E+04  # ~d_R
   1000002     1.01100925E+04  # ~u_L
   2000002     1.00890646E+04  # ~u_R
   1000003     1.01104450E+04  # ~s_L
   2000003     1.00858063E+04  # ~s_R
   1000004     1.01100930E+04  # ~c_L
   2000004     1.00890652E+04  # ~c_R
   1000005     4.50914029E+03  # ~b_1
   2000005     5.01188291E+03  # ~b_2
   1000006     3.25340454E+03  # ~t_1
   2000006     5.01827286E+03  # ~t_2
   1000011     7.40707711E+02  # ~e_L-
   2000011     1.07854392E+03  # ~e_R-
   1000012     7.36071714E+02  # ~nu_eL
   1000013     7.40702887E+02  # ~mu_L-
   2000013     1.07853796E+03  # ~mu_R-
   1000014     7.36067648E+02  # ~nu_muL
   1000015     5.18018736E+02  # ~tau_1-
   2000015     5.66150599E+02  # ~tau_2-
   1000016     5.59214850E+02  # ~nu_tauL
   1000021     3.54815505E+03  # ~g
   1000022     1.79594753E+02  # ~chi_10
   1000023     4.67064631E+02  # ~chi_20
   1000025     4.70930393E+02  # ~chi_30
   1000035     1.15198482E+03  # ~chi_40
   1000024     4.64557651E+02  # ~chi_1+
   1000037     1.15199872E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -7.68542749E-02   # alpha
Block Hmix Q=  4.04060289E+03  # Higgs mixing parameters
   1   -4.63495152E+02  # mu
   2    1.24245514E+01  # tan[beta](Q)
   3    2.42919915E+02  # v(Q)
   4    5.06505799E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.74685016E-02   # Re[R_st(1,1)]
   1  2     9.98347320E-01   # Re[R_st(1,2)]
   2  1    -9.98347320E-01   # Re[R_st(2,1)]
   2  2     5.74685016E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -2.84472705E-03   # Re[R_sb(1,1)]
   1  2     9.99995954E-01   # Re[R_sb(1,2)]
   2  1    -9.99995954E-01   # Re[R_sb(2,1)]
   2  2    -2.84472705E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.42780915E-01   # Re[R_sta(1,1)]
   1  2     9.89754318E-01   # Re[R_sta(1,2)]
   2  1    -9.89754318E-01   # Re[R_sta(2,1)]
   2  2    -1.42780915E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.92037172E-01   # Re[N(1,1)]
   1  2    -4.89417470E-03   # Re[N(1,2)]
   1  3    -1.14749767E-01   # Re[N(1,3)]
   1  4     5.16796695E-02   # Re[N(1,4)]
   2  1     1.17818625E-01   # Re[N(2,1)]
   2  2     8.80165327E-02   # Re[N(2,2)]
   2  3     7.00146614E-01   # Re[N(2,3)]
   2  4    -6.98689187E-01   # Re[N(2,4)]
   3  1     4.43177617E-02   # Re[N(3,1)]
   3  2    -3.19270229E-02   # Re[N(3,2)]
   3  3     7.03587471E-01   # Re[N(3,3)]
   3  4     7.08506367E-01   # Re[N(3,4)]
   4  1     4.11798834E-03   # Re[N(4,1)]
   4  2    -9.95595200E-01   # Re[N(4,2)]
   4  3     3.98983738E-02   # Re[N(4,3)]
   4  4    -8.47429036E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -5.63721167E-02   # Re[U(1,1)]
   1  2    -9.98409828E-01   # Re[U(1,2)]
   2  1    -9.98409828E-01   # Re[U(2,1)]
   2  2     5.63721167E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.19841724E-01   # Re[V(1,1)]
   1  2     9.92793010E-01   # Re[V(1,2)]
   2  1     9.92793010E-01   # Re[V(2,1)]
   2  2    -1.19841724E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     8.30938800E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.58283368E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.24446489E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.17715020E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     5.09829655E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.88892495E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     9.73994279E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.36755797E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     8.31241228E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.57938254E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.26145790E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.73566320E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     9.17359659E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.09994763E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.88569363E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.81467336E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.44536678E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.59747024E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000015     1.97515219E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.93230542E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.09757559E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.22546952E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.44641312E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     6.08052648E-01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.59743621E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.42883143E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.52512582E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.09698599E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     8.57683631E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     9.55798577E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.47555556E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.07450603E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.92793610E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     8.57978673E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.55463283E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.47246800E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.07302002E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.96164499E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     5.90887105E-01   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.58534495E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.39542179E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.08922201E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     4.01286025E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.16873137E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.97753649E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.26172297E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.90878269E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.45066035E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.96741466E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.67270246E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.77608533E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.78008532E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.16004785E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.23527371E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.16879519E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.97751197E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.28691635E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.90868079E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.45072924E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.96745299E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.69360042E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.77603286E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.82841815E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.16003789E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.23519819E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.17279663E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.08858188E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.08052228E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.05725949E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.13795022E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.22765951E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     8.36168788E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.18319231E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.44619895E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.84811691E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.72156951E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.17405705E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.12854064E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.28548245E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.09221960E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.71599782E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     6.34070257E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.49475000E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.91133881E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.64491293E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.45051579E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.76095798E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.06552757E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.75899357E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.70838273E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.14701248E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.23498569E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.34077580E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.49471127E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.93936661E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.64480196E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.45058435E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.76095490E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.08943279E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.75894381E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.71262122E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.14700212E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.23491013E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.34317928E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.31410522E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.31081016E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36839415E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.65275705E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.64903858E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     9.38156001E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.30757202E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.33889936E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.50476228E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.54165855E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.93511489E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.83966008E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.15295499E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.93304664E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.30013424E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.22655336E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.55982418E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99123010E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     8.41848234E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.34122584E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.42824985E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     7.42838624E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     2.39691311E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.22045741E-01    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     7.47466796E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     7.47478189E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.24719653E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.15264932E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.11288327E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.11088795E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.10104587E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.57842037E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.54946972E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.88783225E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.77729338E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.67036284E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.32889546E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     5.15420152E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.53205643E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.46756742E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.49799722E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.44599265E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.44599265E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.44605869E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.44605869E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.11120658E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.11120658E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     5.66322606E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     5.66322606E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.53800865E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.53800865E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.53806259E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.53806259E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     5.90333534E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     5.90333534E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.07959672E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.07959672E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.21354164E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.54173170E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     9.09455329E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.82099662E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.65491194E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.64504484E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.68974690E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.68974690E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.10655807E+00   # ~g
#    BR                NDA      ID1      ID2
     4.82996647E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.82996647E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.50840690E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.00509569E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.19959480E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.05818398E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     6.81901095E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     5.02746457E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     8.68980964E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.05190102E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.60032256E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.49055254E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.66758417E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     5.95961111E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.95961111E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.60726578E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.60726578E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.76467727E-03   # Gamma(h0)
     2.47035463E-03   2        22        22   # BR(h0 -> photon photon)
     1.64235326E-03   2        22        23   # BR(h0 -> photon Z)
     3.10814847E-02   2        23        23   # BR(h0 -> Z Z)
     2.53259418E-01   2       -24        24   # BR(h0 -> W W)
     7.59600512E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.90500468E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.18183750E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.29184237E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.38392484E-07   2        -2         2   # BR(h0 -> Up up)
     2.68582901E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.66926275E-07   2        -1         1   # BR(h0 -> Down down)
     2.05044034E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.45385686E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.26503374E+01   # Gamma(HH)
     2.28777406E-07   2        22        22   # BR(HH -> photon photon)
     3.51463723E-07   2        22        23   # BR(HH -> photon Z)
     1.54253347E-05   2        23        23   # BR(HH -> Z Z)
     6.91056006E-06   2       -24        24   # BR(HH -> W W)
     3.35497041E-06   2        21        21   # BR(HH -> gluon gluon)
     2.64957795E-09   2       -11        11   # BR(HH -> Electron electron)
     1.17961707E-04   2       -13        13   # BR(HH -> Muon muon)
     3.43732959E-02   2       -15        15   # BR(HH -> Tau tau)
     1.40327713E-12   2        -2         2   # BR(HH -> Up up)
     2.72203215E-07   2        -4         4   # BR(HH -> Charm charm)
     2.05201433E-02   2        -6         6   # BR(HH -> Top top)
     1.92630869E-07   2        -1         1   # BR(HH -> Down down)
     6.96738564E-05   2        -3         3   # BR(HH -> Strange strange)
     1.82976680E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.47632947E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.07968967E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.07968967E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.08431308E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.66839354E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.33805012E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.06393046E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.62441498E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.78998956E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.04648890E-05   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.00076046E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.12816861E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.47379360E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.05750023E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     9.85866374E-05   2        25        25   # BR(HH -> h0 h0)
     2.37492263E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     2.11566897E-13   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     2.11566897E-13   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     5.90164530E-07   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     2.37229733E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     9.04511055E-09   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     9.04511055E-09   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     5.89759886E-07   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     3.22318771E-03   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     6.24059598E-03   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     6.24059598E-03   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     3.47933161E-03   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     2.24729932E+01   # Gamma(A0)
     4.67235209E-07   2        22        22   # BR(A0 -> photon photon)
     8.41088729E-07   2        22        23   # BR(A0 -> photon Z)
     2.75079876E-05   2        21        21   # BR(A0 -> gluon gluon)
     2.59948005E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.15730882E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.37230472E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.34656206E-12   2        -2         2   # BR(A0 -> Up up)
     2.61182663E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.01954554E-02   2        -6         6   # BR(A0 -> Top top)
     1.88988325E-07   2        -1         1   # BR(A0 -> Down down)
     6.83564714E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.79519149E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.32823644E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.06816373E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.06816373E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     8.89845195E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     3.17923209E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.36344716E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.14346862E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.04027923E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     4.59503207E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.40885806E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.45608403E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.00703660E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.12381848E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.49247565E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.16815072E-05   2        23        25   # BR(A0 -> Z h0)
     3.23327400E-37   2        25        25   # BR(A0 -> h0 h0)
     2.10067823E-13   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     2.10067823E-13   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     8.98105626E-09   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     8.98105626E-09   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     9.66520345E-03   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     9.66520345E-03   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     2.28197874E+01   # Gamma(Hp)
     2.77880525E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.18802603E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.36041135E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.85170098E-07   2        -1         2   # BR(Hp -> Down up)
     3.11507506E-06   2        -3         2   # BR(Hp -> Strange up)
     1.98931455E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.26880806E-08   2        -1         4   # BR(Hp -> Down charm)
     6.70325825E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.78575599E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.53129748E-06   2        -1         6   # BR(Hp -> Down top)
     3.34853021E-05   2        -3         6   # BR(Hp -> Strange top)
     2.11042739E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.05223043E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.67516359E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.11526187E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.08795522E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.83062393E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.05620385E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.07483876E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.42424958E-09   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     2.14251149E-05   2        24        25   # BR(Hp -> W h0)
     1.35265511E-12   2        24        35   # BR(Hp -> W HH)
     8.89861745E-13   2        24        36   # BR(Hp -> W A0)
     9.24299565E-06   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     4.32407837E-13   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     9.23933995E-06   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     1.84866537E-08   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     1.69812030E-02   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     2.08339523E-03   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.15896777E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.54453581E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.54369477E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00054482E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.93314684E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.47796453E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.15896777E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.54453581E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.54369477E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99988039E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.19605248E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99988039E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.19605248E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26941976E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.01929105E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.09609931E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.19605248E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99988039E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.34741634E-04   # BR(b -> s gamma)
    2    1.58869996E-06   # BR(b -> s mu+ mu-)
    3    3.52540128E-05   # BR(b -> s nu nu)
    4    2.36340298E-15   # BR(Bd -> e+ e-)
    5    1.00961943E-10   # BR(Bd -> mu+ mu-)
    6    2.11384012E-08   # BR(Bd -> tau+ tau-)
    7    7.97543762E-14   # BR(Bs -> e+ e-)
    8    3.40710608E-09   # BR(Bs -> mu+ mu-)
    9    7.22778380E-07   # BR(Bs -> tau+ tau-)
   10    9.65956080E-05   # BR(B_u -> tau nu)
   11    9.97795650E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42214541E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93726469E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15823733E-03   # epsilon_K
   17    2.28166773E-15   # Delta(M_K)
   18    2.48027717E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29099795E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.07583172E-15   # Delta(g-2)_electron/2
   21    1.31506545E-10   # Delta(g-2)_muon/2
   22    2.08483453E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.12967942E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.04142642E-01   # C7
     0305 4322   00   2    -2.49764501E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.11145968E-01   # C8
     0305 6321   00   2    -2.83324499E-04   # C8'
 03051111 4133   00   0     1.62660345E+00   # C9 e+e-
 03051111 4133   00   2     1.62676639E+00   # C9 e+e-
 03051111 4233   00   2     3.46230384E-05   # C9' e+e-
 03051111 4137   00   0    -4.44929555E+00   # C10 e+e-
 03051111 4137   00   2    -4.44773786E+00   # C10 e+e-
 03051111 4237   00   2    -2.56062143E-04   # C10' e+e-
 03051313 4133   00   0     1.62660345E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62676611E+00   # C9 mu+mu-
 03051313 4233   00   2     3.46230243E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44929555E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44773814E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.56062146E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50502087E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.53648094E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50502087E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.53648115E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50502088E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.53652912E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85815974E-07   # C7
     0305 4422   00   2     1.49694575E-06   # C7
     0305 4322   00   2    -6.61995860E-07   # C7'
     0305 6421   00   0     3.30476897E-07   # C8
     0305 6421   00   2     4.31509295E-06   # C8
     0305 6321   00   2    -2.50611842E-07   # C8'
 03051111 4133   00   2    -7.87206081E-07   # C9 e+e-
 03051111 4233   00   2     7.42834694E-07   # C9' e+e-
 03051111 4137   00   2     5.08748784E-06   # C10 e+e-
 03051111 4237   00   2    -5.49502184E-06   # C10' e+e-
 03051313 4133   00   2    -7.87206049E-07   # C9 mu+mu-
 03051313 4233   00   2     7.42834616E-07   # C9' mu+mu-
 03051313 4137   00   2     5.08748965E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.49502207E-06   # C10' mu+mu-
 03051212 4137   00   2    -9.63114630E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.18813690E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.63114670E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.18813690E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.64033734E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.18813486E-06   # C11' nu_3 nu_3
