import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import math
import os

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


nevents=int(3.2*runArgs.maxEvents)

#MFm1 = 200
MF = int(0)
MS = int(0)
model_pars_str = str(jofile)

print(model_pars_str)
for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'MVLL' in s:
        ss=s.replace("MVLL","")
        print(ss)
        if ss.isdigit():
            MF = int(ss)
    if 'MScal'in s:
       sss = s.replace("MScal","")
       sss = sss.replace(".py","")
       print(sss)
       if sss.isdigit():
          MS = int(sss)
    if 'mu' in s:
        vll_e = 0
        vll_mu = 0.1
        
    if 'el' in s:
        vll_e = 0.1
        vll_mu = 0
if vll_e > 0:
    MFm1 = MF
    MFz1 = MF
    MS11 = MS21 = MS31 = MS
if vll_mu > 0:
    MFm2 = MF
    MFz2 = MF
    MS12 = MS22 = MS32 = MS
	
Epsilon = 1e-2

process_string=str(0)

if vll_e > 0:
    process_string = """
                    import model sm
                    import model VLL_Doublet_Mixing_UFO

                    define p = g u c d s u~ c~ d~ s~
                    define j = g u c d s u~ c~ d~ s~
                    define l+ = e+ mu+
                    define l- = e- mu-
                    define vl = ve vm vt
                    define vl~ = ve~ vm~ vt~
                    define psi = psiem
                    define psi~ = psiem~
                    define psiz = psiez
                    define psiz~ = psiez~

                    generate p p > psi psi~
                    add process p p > psi psiz~
                    add process p p > psiz psi~
                    add process p p > psiz psiz~
                    output -f
                    """

if vll_mu > 0:
    process_string = """
                    import model sm
                    import model VLL_Doublet_Mixing_UFO

                    define p = g u c d s u~ c~ d~ s~
                    define j = g u c d s u~ c~ d~ s~
                    define l+ = e+ mu+
                    define l- = e- mu-
                    define vl = ve vm vt
                    define vl~ = ve~ vm~ vt~
                    define psi = psimum
                    define psi~ = psimum~
                    define psiz = psimuz
                    define psiz~ = psimuz~

                    generate p p > psi psi~
                    add process p p > psi psiz~
                    add process p p > psiz psi~
                    add process p p > psiz psiz~
                    output -f
                    """


process_dir = new_process(process_string)

# Set up the run card with mostly default values
run_card_extras = {
            'lhe_version':'3.0',
            # 'pdlabel':"'lhapdf'",
            # 'lhaid':' 260400',
            'nevents' :int(nevents)
}

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
-9000005
9000005
-9000006
9000006
9000008
-9000008
-9000009
9000009
9000011
-9000011
-9000012
9000012
9000014
-9000014
-9000015
9000015
9000017
-9000017
-9000018
9000018
""")
pdgfile.close()

params = {}
if vll_mu >0:
    params['MASS'] = {'9000006':MFm2, '9000009':MFz2, '9000012':MS12, '9000015':MS22, '9000018':MS32}
if vll_e >0:
    params['MASS'] = {'9000005':MFm1, '9000008':MFz1, '9000011':MS11, '9000014':MS21, '9000017':MS31}

params['NP'] = {'3':Epsilon}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras)


# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
# if os.access(madspin_card,os.R_OK):
#     os.unlink(madspin_card)
mscard = open(madspin_card,'w')
if vll_e >0:
    mscard.write("""
    # set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    # set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
    set seed %i
    decay w+ > all all
    decay w- > all all
    decay z > all all
    decay h > all all
    
    decay s11 > e- e+
    decay s21 > mu- e+
    decay s31 > ta- e+
    decay s11~ > e+ e-
    decay s21~ > mu+ e-
    decay s31~ > ta+ e-
    
    
    decay psi > z e-
    decay psi~ > z e+
    decay psi > h e-
    decay psi~ > h e+
    
    decay psi > s11~ e-
    decay psi > s21~ mu-
    decay psi > s31~ ta-
    decay psi~ > s11 e+
    decay psi~ > s21 mu+
    decay psi~ > s31 ta+
    
    decay psiz > w+ e-
    decay psiz~ > w- e+
    decay psiz > s11~ ve
    decay psiz > s21~ vm
    decay psiz > s31~ vt
    decay psiz~ > s11 ve~
    decay psiz~ > s21 vm~
    decay psiz~ > s31 vt~
    launch""" % (runArgs.randomSeed))

if vll_mu >0:
    mscard.write("""
    # set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    # set Nevents_for_max_weight 200 # number of events for the estimate of the max. weight
    set seed %i
    decay w+ > all all
    decay w- > all all
    decay z > all all
    decay h > all all
    
    decay s12 > e- mu+
    decay s22 > mu- mu+
    decay s32 > ta- mu+
    decay s12~ > e+ mu-
    decay s22~ > mu+ mu-
    decay s32~ > ta+ mu-
    
    
    decay psi > z mu-
    decay psi~ > z mu+
    decay psi > h mu-
    decay psi~ > h mu+
    
    decay psi > s12~ e-
    decay psi > s22~ mu-
    decay psi > s32~ ta-
    decay psi~ > s12 e+
    decay psi~ > s22 mu+
    decay psi~ > s32 ta+
    
    decay psiz > w+ mu-
    decay psiz~ > w- mu+
    decay psiz > s12~ ve
    decay psiz > s22~ vm
    decay psiz > s32~ vt
    decay psiz~ > s12 ve~
    decay psiz~ > s22 vm~
    decay psiz~ > s32 vt~
    launch""" % (runArgs.randomSeed))

mscard.close()


print_cards()
modify_param_card(process_dir=process_dir,params=params) #param_card_extras
generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")

# Shower
evgenConfig.description = 'MadGraph5+Pythia8 for Vectorlike leptons of mass  GeV, inc lepton final state'#.format(mtaup)
evgenConfig.contact = ['Merve Nazlim Agaras <merve.nazlim.agaras@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton']
evgenConfig.generators += ['MadGraph','Pythia8']


# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut = 15000.0#MeV
 
