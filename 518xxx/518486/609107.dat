# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  13:22
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.40378192E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.65443743E+03  # scale for input parameters
    1   -5.42750879E+02  # M_1
    2   -1.94707917E+03  # M_2
    3    3.02595255E+03  # M_3
   11   -3.83449043E+03  # A_t
   12    3.09652029E+02  # A_b
   13   -2.65821888E+02  # A_tau
   23   -4.03402701E+02  # mu
   25    4.21677167E+00  # tan(beta)
   26    5.00043595E+02  # m_A, pole mass
   31    1.89420630E+03  # M_L11
   32    1.89420630E+03  # M_L22
   33    3.99181710E+02  # M_L33
   34    1.23114946E+03  # M_E11
   35    1.23114946E+03  # M_E22
   36    1.88278906E+03  # M_E33
   41    2.79162066E+03  # M_Q11
   42    2.79162066E+03  # M_Q22
   43    2.60916184E+03  # M_Q33
   44    3.29818510E+03  # M_U11
   45    3.29818510E+03  # M_U22
   46    1.05794944E+03  # M_U33
   47    3.62212450E+03  # M_D11
   48    3.62212450E+03  # M_D22
   49    2.30661514E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.65443743E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.65443743E+03  # (SUSY scale)
  1  1     8.61691294E-06   # Y_u(Q)^DRbar
  2  2     4.37739177E-03   # Y_c(Q)^DRbar
  3  3     1.04099230E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.65443743E+03  # (SUSY scale)
  1  1     7.30149913E-05   # Y_d(Q)^DRbar
  2  2     1.38728484E-03   # Y_s(Q)^DRbar
  3  3     7.24079918E-02   # Y_b(Q)^DRbar
Block Ye Q=  1.65443743E+03  # (SUSY scale)
  1  1     1.27416499E-05   # Y_e(Q)^DRbar
  2  2     2.63456909E-03   # Y_mu(Q)^DRbar
  3  3     4.43091180E-02   # Y_tau(Q)^DRbar
Block Au Q=  1.65443743E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.83449042E+03   # A_t(Q)^DRbar
Block Ad Q=  1.65443743E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.09652035E+02   # A_b(Q)^DRbar
Block Ae Q=  1.65443743E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -2.65821891E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  1.65443743E+03  # soft SUSY breaking masses at Q
   1   -5.42750879E+02  # M_1
   2   -1.94707917E+03  # M_2
   3    3.02595255E+03  # M_3
  21    5.22608701E+04  # M^2_(H,d)
  22   -1.95013820E+05  # M^2_(H,u)
  31    1.89420630E+03  # M_(L,11)
  32    1.89420630E+03  # M_(L,22)
  33    3.99181710E+02  # M_(L,33)
  34    1.23114946E+03  # M_(E,11)
  35    1.23114946E+03  # M_(E,22)
  36    1.88278906E+03  # M_(E,33)
  41    2.79162066E+03  # M_(Q,11)
  42    2.79162066E+03  # M_(Q,22)
  43    2.60916184E+03  # M_(Q,33)
  44    3.29818510E+03  # M_(U,11)
  45    3.29818510E+03  # M_(U,22)
  46    1.05794944E+03  # M_(U,33)
  47    3.62212450E+03  # M_(D,11)
  48    3.62212450E+03  # M_(D,22)
  49    2.30661514E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.17641451E+02  # h0
        35     5.01869357E+02  # H0
        36     5.00043595E+02  # A0
        37     5.08695895E+02  # H+
   1000001     2.85245980E+03  # ~d_L
   2000001     3.65894850E+03  # ~d_R
   1000002     2.85159438E+03  # ~u_L
   2000002     3.34239522E+03  # ~u_R
   1000003     2.85245973E+03  # ~s_L
   2000003     3.65894849E+03  # ~s_R
   1000004     2.85159439E+03  # ~c_L
   2000004     3.34239508E+03  # ~c_R
   1000005     2.35718414E+03  # ~b_1
   2000005     2.63887319E+03  # ~b_2
   1000006     1.03219244E+03  # ~t_1
   2000006     2.65179546E+03  # ~t_2
   1000011     1.90776317E+03  # ~e_L-
   2000011     1.23353005E+03  # ~e_R-
   1000012     1.90591370E+03  # ~nu_eL
   1000013     1.90776306E+03  # ~mu_L-
   2000013     1.23352980E+03  # ~mu_R-
   1000014     1.90591358E+03  # ~nu_muL
   1000015     4.31277215E+02  # ~tau_1-
   2000015     1.88491237E+03  # ~tau_2-
   1000016     4.24461425E+02  # ~nu_tauL
   1000021     3.12011298E+03  # ~g
   1000022     3.99019913E+02  # ~chi_10
   1000023     4.12887195E+02  # ~chi_20
   1000025     5.49778183E+02  # ~chi_30
   1000035     1.97785521E+03  # ~chi_40
   1000024     4.10038332E+02  # ~chi_1+
   1000037     1.97806339E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.43184606E-01   # alpha
Block Hmix Q=  1.65443743E+03  # Higgs mixing parameters
   1   -4.03402701E+02  # mu
   2    4.21677167E+00  # tan[beta](Q)
   3    2.43921447E+02  # v(Q)
   4    2.50043597E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.19484060E-02   # Re[R_st(1,1)]
   1  2     9.95763773E-01   # Re[R_st(1,2)]
   2  1    -9.95763773E-01   # Re[R_st(2,1)]
   2  2     9.19484060E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.40433131E-03   # Re[R_sb(1,1)]
   1  2     9.99994205E-01   # Re[R_sb(1,2)]
   2  1    -9.99994205E-01   # Re[R_sb(2,1)]
   2  2    -3.40433131E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99999714E-01   # Re[R_sta(1,1)]
   1  2     7.56194824E-04   # Re[R_sta(1,2)]
   2  1    -7.56194824E-04   # Re[R_sta(2,1)]
   2  2    -9.99999714E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -2.61624755E-01   # Re[N(1,1)]
   1  2     4.01949702E-02   # Re[N(1,2)]
   1  3     6.88467595E-01   # Re[N(1,3)]
   1  4    -6.75240122E-01   # Re[N(1,4)]
   2  1     2.52690483E-02   # Re[N(2,1)]
   2  2    -1.71910287E-02   # Re[N(2,2)]
   2  3     7.05273932E-01   # Re[N(2,3)]
   2  4     7.08275812E-01   # Re[N(2,4)]
   3  1     9.64837715E-01   # Re[N(3,1)]
   3  2     1.28663110E-02   # Re[N(3,2)]
   3  3     1.68186407E-01   # Re[N(3,3)]
   3  4    -2.01583665E-01   # Re[N(3,4)]
   4  1     1.46502393E-03   # Re[N(4,1)]
   4  2    -9.98961106E-01   # Re[N(4,2)]
   4  3     1.77309092E-02   # Re[N(4,3)]
   4  4    -4.19544707E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.50510731E-02   # Re[U(1,1)]
   1  2    -9.99686173E-01   # Re[U(1,2)]
   2  1    -9.99686173E-01   # Re[U(2,1)]
   2  2     2.50510731E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.92991988E-02   # Re[V(1,1)]
   1  2     9.98240254E-01   # Re[V(1,2)]
   2  1     9.98240254E-01   # Re[V(2,1)]
   2  2    -5.92991988E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.04844005E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.40081554E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.70785911E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.15221058E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     2.05636518E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.70197987E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.58267031E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     4.66260631E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.04870753E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.40187079E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.87219921E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.15160806E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     2.05660801E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.70708722E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.08439619E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.58156966E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.66205544E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.08337961E-04   # ~tau^-_1
#    BR                NDA      ID1      ID2
     8.68949679E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.47182749E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.62907938E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     8.11957126E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.67631891E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.76415453E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.09164323E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     8.21951773E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.72773440E-04    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.74500601E-04    2     1000016       -37   # BR(~tau^-_2 -> ~nu_tau H^-)
     1.35427587E-04    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.37158586E-04    2     1000015        36   # BR(~tau^-_2 -> ~tau^-_1 A^0)
     1.29189412E-04    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
     1.39766361E-04    2     1000015        35   # BR(~tau^-_2 -> ~tau^-_1 H^0)
DECAY   1000012     2.09013035E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.18838105E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.43386833E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.52053779E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.56742477E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     2.09036946E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.18824503E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.43347529E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.51956244E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.57856647E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     9.38959970E-04   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.71341216E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     5.42300276E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.23235782E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   1000001     1.05476532E+01   # ~d_L
#    BR                NDA      ID1      ID2
     8.61739192E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.36829212E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.78794255E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.20396002E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.43289132E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.41137460E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000001     2.34547581E+01   # ~d_R
#    BR                NDA      ID1      ID2
     5.82041408E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.74570857E-02    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.16668178E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     1.05487902E+01   # ~s_L
#    BR                NDA      ID1      ID2
     8.62117605E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.41691536E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.78766847E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.20361415E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.53114377E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.41067889E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   2000003     2.34550286E+01   # ~s_R
#    BR                NDA      ID1      ID2
     5.82310684E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.74563484E-02    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.16657513E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     1.64381569E+00   # ~b_1
#    BR                NDA      ID1      ID2
     1.19439401E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     7.02403592E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     6.69570992E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.39141830E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.78684389E-04    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     1.32762237E-03    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.35218717E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.52900492E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.00364791E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.04505997E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.64706532E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.95474484E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.30636656E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.72907261E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     7.74966861E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   1000002     1.05883381E+01   # ~u_L
#    BR                NDA      ID1      ID2
     1.83008456E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.74795141E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     3.18376705E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.99548335E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     6.35918713E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000002     1.14407535E+01   # ~u_R
#    BR                NDA      ID1      ID2
     4.33925024E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.03967603E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.74922908E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     3.81280030E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     1.05894750E+01   # ~c_L
#    BR                NDA      ID1      ID2
     2.32376376E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.74794886E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     3.18341675E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.00446657E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     6.35850445E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   2000004     1.14431191E+01   # ~c_R
#    BR                NDA      ID1      ID2
     4.34331441E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.58080472E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.74807648E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.07639820E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
     3.81192636E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     3.04611380E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.23210887E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.31039498E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.73381428E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.08410129E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.49145591E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.69720909E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.82861693E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.42489854E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.38532911E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     7.87375097E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.99096835E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.16581021E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.55548523E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.92947197E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     3.64405769E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     6.08554986E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.44890893E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.23245914E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.14965248E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.14913119E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.01984826E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     5.00257263E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.47806429E-04    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     8.47809035E-04    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.57787331E-01    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     8.89572928E-04    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     8.89574658E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.57823994E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     9.38033340E-03    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     8.58877740E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     9.54450026E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     9.69632061E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     8.35922993E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     6.03719863E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     6.28264074E-03    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     9.51000391E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.09391477E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.01644880E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     6.02980453E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.52656570E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.40343421E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.04504616E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.42054472E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.83438589E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     2.31261558E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.04090358E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.31103019E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.33463952E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.33391622E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     4.17268674E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.01172315E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.00915030E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.37747587E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.78772874E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.75942743E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.83892618E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.83892618E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.83388453E-02    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     2.83388453E-02    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     2.60667587E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.60667587E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.91344947E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.52694278E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.11574548E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.93899841E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     5.37404028E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.91167566E-04    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.91167566E-04    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.91168466E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.91168466E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     7.32103895E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     7.32103895E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     4.12982850E-04    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     4.12982850E-04    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     4.12984203E-04    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     4.12984203E-04    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     7.36698493E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     7.36698493E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     4.45252043E-03    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     4.45252043E-03    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     8.88898811E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     8.88898811E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.30960984E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     8.30960984E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     1.12702306E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.66065637E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.15837018E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.78176905E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.10586824E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.56999350E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     7.36635982E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.99532268E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     8.11495427E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.52593384E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     7.32607739E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     1.50411951E-03    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.92886892E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.55270983E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.05042604E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.55353254E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.55353254E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.13231562E+02   # ~g
#    BR                NDA      ID1      ID2
     1.10644299E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     1.10644299E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     1.10643162E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     1.10643162E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     3.15804392E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.15804392E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.16609918E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     3.16609918E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.09964113E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     1.09964113E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     1.09964161E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     1.09964161E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     7.51445607E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     7.51445607E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.30488837E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.30488837E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.09651486E-04    3     1000025         2        -2   # BR(~g -> chi^0_3 u u_bar)
     1.09652227E-04    3     1000025         4        -4   # BR(~g -> chi^0_3 c c_bar)
DECAY        25     3.55320820E-03   # Gamma(h0)
     2.01143255E-03   2        22        22   # BR(h0 -> photon photon)
     7.86858303E-04   2        22        23   # BR(h0 -> photon Z)
     1.12540389E-02   2        23        23   # BR(h0 -> Z Z)
     1.07506707E-01   2       -24        24   # BR(h0 -> W W)
     6.47484012E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.22664608E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.76967263E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.99052136E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.49920946E-07   2        -2         2   # BR(h0 -> Up up)
     2.90885085E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.29194002E-07   2        -1         1   # BR(h0 -> Down down)
     2.63733288E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.04157254E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     8.96620638E-01   # Gamma(HH)
     5.42665892E-06   2        22        22   # BR(HH -> photon photon)
     1.00785405E-06   2        22        23   # BR(HH -> photon Z)
     7.85139616E-03   2        23        23   # BR(HH -> Z Z)
     1.66414807E-02   2       -24        24   # BR(HH -> W W)
     2.01522033E-03   2        21        21   # BR(HH -> gluon gluon)
     1.80659674E-09   2       -11        11   # BR(HH -> Electron electron)
     8.03942489E-05   2       -13        13   # BR(HH -> Muon muon)
     2.34226930E-02   2       -15        15   # BR(HH -> Tau tau)
     1.05993710E-10   2        -2         2   # BR(HH -> Up up)
     2.05507314E-05   2        -4         4   # BR(HH -> Charm charm)
     6.88845088E-01   2        -6         6   # BR(HH -> Top top)
     1.69212365E-07   2        -1         1   # BR(HH -> Down down)
     6.12022213E-05   2        -3         3   # BR(HH -> Strange strange)
     1.61213686E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.47300050E-11   2        23        36   # BR(HH -> Z A0)
     9.98416825E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.17632357E+00   # Gamma(A0)
     7.32999313E-06   2        22        22   # BR(A0 -> photon photon)
     2.00879884E-06   2        22        23   # BR(A0 -> photon Z)
     2.29077868E-03   2        21        21   # BR(A0 -> gluon gluon)
     1.37078524E-09   2       -11        11   # BR(A0 -> Electron electron)
     6.10004183E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.77726844E-02   2       -15        15   # BR(A0 -> Tau tau)
     6.76715072E-11   2        -2         2   # BR(A0 -> Up up)
     1.31214415E-05   2        -4         4   # BR(A0 -> Charm charm)
     8.47041838E-01   2        -6         6   # BR(A0 -> Top top)
     1.28456598E-07   2        -1         1   # BR(A0 -> Down down)
     4.64613442E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.22454288E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.03103594E-02   2        23        25   # BR(A0 -> Z h0)
     1.44344633E-34   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.16922030E+00   # Gamma(Hp)
     1.39532100E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     5.96543255E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.68732369E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.14510918E-07   2        -1         2   # BR(Hp -> Down up)
     1.88602699E-06   2        -3         2   # BR(Hp -> Strange up)
     1.19136282E-06   2        -5         2   # BR(Hp -> Bottom up)
     6.57208518E-07   2        -1         4   # BR(Hp -> Down charm)
     5.55137313E-05   2        -3         4   # BR(Hp -> Strange charm)
     1.66852873E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.61248503E-05   2        -1         6   # BR(Hp -> Down top)
     1.22375239E-03   2        -3         6   # BR(Hp -> Strange top)
     9.70227145E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.13337971E-02   2        24        25   # BR(Hp -> W h0)
     1.69875806E-08   2        24        35   # BR(Hp -> W HH)
     5.54229813E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.08897146E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.76921919E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.77811633E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.94996308E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.12429817E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.62392900E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.08897146E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.76921919E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.77811633E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99893134E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.06866408E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99893134E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.06866408E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.22675153E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.42959517E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.39916186E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.06866408E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99893134E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.10465737E-04   # BR(b -> s gamma)
    2    1.59900935E-06   # BR(b -> s mu+ mu-)
    3    3.54558747E-05   # BR(b -> s nu nu)
    4    2.51247372E-15   # BR(Bd -> e+ e-)
    5    1.07330028E-10   # BR(Bd -> mu+ mu-)
    6    2.24688183E-08   # BR(Bd -> tau+ tau-)
    7    8.46762791E-14   # BR(Bs -> e+ e-)
    8    3.61736804E-09   # BR(Bs -> mu+ mu-)
    9    7.67285668E-07   # BR(Bs -> tau+ tau-)
   10    9.63592216E-05   # BR(B_u -> tau nu)
   11    9.95353869E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.50635621E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.96730863E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.19422147E-03   # epsilon_K
   17    2.28204286E-15   # Delta(M_K)
   18    2.49545938E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.32533631E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.41876357E-16   # Delta(g-2)_electron/2
   21    1.46162562E-11   # Delta(g-2)_muon/2
   22    9.37387192E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -3.35157759E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.77407885E-01   # C7
     0305 4322   00   2    -1.72018023E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.87076814E-01   # C8
     0305 6321   00   2    -1.78451484E-03   # C8'
 03051111 4133   00   0     1.59522551E+00   # C9 e+e-
 03051111 4133   00   2     1.59747317E+00   # C9 e+e-
 03051111 4233   00   2    -2.83549583E-06   # C9' e+e-
 03051111 4137   00   0    -4.41791761E+00   # C10 e+e-
 03051111 4137   00   2    -4.43614232E+00   # C10 e+e-
 03051111 4237   00   2     2.23520955E-05   # C10' e+e-
 03051313 4133   00   0     1.59522551E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59747313E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.83551431E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.41791761E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43614236E+00   # C10 mu+mu-
 03051313 4237   00   2     2.23521136E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50932147E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -4.86992817E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50932147E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -4.86992417E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50932045E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -4.86893406E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85816751E-07   # C7
     0305 4422   00   2     1.45208097E-06   # C7
     0305 4322   00   2    -1.86107718E-06   # C7'
     0305 6421   00   0     3.30477563E-07   # C8
     0305 6421   00   2     2.84758852E-06   # C8
     0305 6321   00   2    -9.41336169E-07   # C8'
 03051111 4133   00   2    -3.87912232E-06   # C9 e+e-
 03051111 4233   00   2     5.21612256E-08   # C9' e+e-
 03051111 4137   00   2     3.08945376E-05   # C10 e+e-
 03051111 4237   00   2    -3.98385218E-07   # C10' e+e-
 03051313 4133   00   2    -3.87912170E-06   # C9 mu+mu-
 03051313 4233   00   2     5.21612236E-08   # C9' mu+mu-
 03051313 4137   00   2     3.08945383E-05   # C10 mu+mu-
 03051313 4237   00   2    -3.98385223E-07   # C10' mu+mu-
 03051212 4137   00   2    -6.68412602E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     8.67211826E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.68412603E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     8.67211826E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.70229067E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     8.67187281E-08   # C11' nu_3 nu_3
