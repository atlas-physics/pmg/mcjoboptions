# based on the JobOptions MC15.429304
# the same as 421443, but with xAOD filter

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7.1-Default"
evgenConfig.description    = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7.1-Default tune, exactly one lepton filter, Jpsi->mumu filter, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton', 'Jpsi']
# evgenConfig.contact        = ['aknue@cern.ch']
evgenConfig.contact        = ['dpanchal@utexas.edu']
# evgenConfig.inputfilecheck="TXT"

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.nEvents   *= 10.    # compensate filter efficiency
PowhegConfig.generate()

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['B2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'B2Jpsimumu.DEC'

## NonAllHad filter
#include('GeneratorFilters/TTbarWToLeptonFilter.py')
#filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
#filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# filtSeq.Expression="TTbarWToLeptonFilter"

# Jpsi->mumu filter
include('GeneratorFilters/xAODTTbarWithJpsimumuFilter_Common.py')
filtSeq.xAODTTbarWithJpsimumuFilter.JpsipTMinCut = 5000.

# filtSeq.Expression="TTbarWithJpsimumuFilter"


