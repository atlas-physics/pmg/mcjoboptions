include("utilsOptions.py")
include("utilsDecays.py")


##---


class lptMadGraph:
    ## <<>>

    ##---

    @staticmethod
    def runEpilog():
        genSeq.Pythia8.Commands += ["Merging:Process = guess"]

        genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

        testSeq.TestHepMC.MaxVtxDisp = 1e8
        testSeq.TestHepMC.MaxTransVtxDisp = 1e8
        run_settings['time_of_flight']=1e-19

    ##---

    @staticmethod
    def setupProductionMode(type):
        process = '''
import model RPVMSSM_UFO
define j = g u c d s u~ c~ d~ s~
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~

define X = n1
'''

        if lptOptions.jobType.N1N1 == type:
            process += "define Y = n1"

        else:
            process += "define Y = n2"

        process += '''

generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
add process p p > X Y j j RPV=0 QED<=2 / susystrong @3
'''

        return(process)

    ##---

    @staticmethod
    def updateMasses(mass):
        masses['1000022'] = mass
        masses['1000023'] = mass


    @staticmethod
    def updateDecays(mass, lifetime, ns, doTaus, doUpType, doDownType, doBottom, doTop):
        headerN1 = f"DECAY 1000022 {lptDecays.getWidth(lifetime, ns)}"
        headerN2 = f"DECAY 1000023 0.0"

        decayChannels, branchingRatio = lptDecays.getDecayProducts(
            mass, doTaus, doUpType, doDownType, doBottom, doTop
        )

        decayTableLine = "\n{0} {1:<8} {2:<8} {3:<8} {4:<8} {5:<8}"
        decayTable = decayTableLine.format("#", "BR", "NDA", "ID1", "ID2", "ID3")
        for decayChannel in decayChannels:
            decayTable += decayTableLine.format(" ", f"{branchingRatio:6.5f}", 3, decayChannel[0], decayChannel[1], decayChannel[2])

        decays['1000022'] = headerN1 + decayTable
        decays['1000023'] = headerN2 + decayTable

    ##---


    @staticmethod
    def setupEventFilter():
        pass


    ##---

    @staticmethod
    def runGeneration():
        type, mass, lifetime, ns, doTaus, doUpType, doDownType, doBottom, doTop = (
            lptOptions.getJobOptions(3)
        )

        lptMadGraph.updateMasses(mass)
        lptMadGraph.updateDecays(mass, lifetime, ns, doTaus, doUpType, doDownType, doBottom, doTop)

        ##---

        lptMadGraph.setupEventFilter()
        lptMadGraph.runEpilog()

        ##---

        return(lptMadGraph.setupProductionMode(type))

##---


process = lptMadGraph.runGeneration()


##---


evgenConfig.description = "MadGraph Neutralino Production: pp -> N1N1 / N1N2"
evgenConfig.keywords = ["BSM", "SUSY", "RPV", "Neutralino"]
evgenConfig.process = "pp -> N1N1, pp -> N1N1"


##---


evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ["luke.francis.mcelhinney@cern.ch", "andy.wharton@cern.ch"]
