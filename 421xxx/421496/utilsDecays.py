import itertools


##---


class lptDecays:
    higgsID = 25
    n1ID = 1000022
    n2ID = 1000023

    ##---

    c = 299.792458
    hbar = 6.582119514e-16
    ps2ns = 1e-3

    ##---

    l_m = (11, 13)
    tau_m = (15,)

    tauMass = 1.8

    @staticmethod
    def getPositiveLeptons(leptons):
        return tuple(-ID for ID in leptons)

    @staticmethod
    def getLeptons(doTaus):
        leptons = (lptDecays.l_m + lptDecays.tau_m) if doTaus else lptDecays.l_m

        return (leptons, lptDecays.getPositiveLeptons(leptons))

    ##---

    uc = (2, 4)
    ds = (1, 3)
    t = (6,)
    b = (5,)

    bMass = 4.2
    tMass = 173.2

    @staticmethod
    def getUpDownPairs(upType, downType):
        return tuple(itertools.product(upType, downType))

    @staticmethod
    def getQuarkPairsByCharge(upDownPairs, Positive):
        func = (lambda uT, dT: (uT, -dT)) if Positive else (lambda uT, dT: (-uT, dT))
        return tuple(func(uT, dT) for uT, dT in upDownPairs)

    @staticmethod
    def getQuarks(mass, doUpType, doDownType, doBottom, doTop):
        decayProductMass = lptDecays.tauMass + lptDecays.bMass + lptDecays.tMass

        upTypeQuarks = ()
        downTypeQuarks = ()

        if doUpType:
            upTypeQuarks += lptDecays.uc

        if doDownType:
            downTypeQuarks += lptDecays.ds

        if doBottom:
            downTypeQuarks += lptDecays.b

        if (decayProductMass < mass) and doTop:
            upTypeQuarks += lptDecays.t

        quarkPairs = lptDecays.getUpDownPairs(upTypeQuarks, downTypeQuarks)

        return (
            lptDecays.getQuarkPairsByCharge(quarkPairs, True),
            lptDecays.getQuarkPairsByCharge(quarkPairs, False),
        )

    @staticmethod
    def getDecayChannels(leptons, quarks):
        return tuple(
            (lepton, quarks[0], quarks[1])
            for l_c, q_c in zip(leptons, quarks)
            for lepton, quarks in itertools.product(l_c, q_c)
        )

    # ---

    @staticmethod
    def getCTau(lifetime, ns=True):
        return lptDecays.c * (lifetime if ns else (lifetime * lptDecays.ps2ns))

    @staticmethod
    def getWidth(lifetime, ns=True):
        return lptDecays.hbar / (lifetime if ns else (lifetime * lptDecays.ps2ns))

    def getDecayProducts(
        mass,
        doTaus=True,
        doUpType=True,
        doDownType=True,
        doBottom=True,
        doTop=True,
    ):
        leptons = lptDecays.getLeptons(doTaus)
        quarks = lptDecays.getQuarks(mass, doUpType, doDownType, doBottom, doTop)

        decayChannels = lptDecays.getDecayChannels(leptons, quarks)

        ## ToDo: Check the number of decay channels.

        branchingRatio = 1.0 / len(decayChannels)

        return (decayChannels, branchingRatio)
