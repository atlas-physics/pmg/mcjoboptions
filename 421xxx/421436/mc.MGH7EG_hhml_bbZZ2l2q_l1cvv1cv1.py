import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=15
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1.0000000',  # CV
                     'C2V': '1.0000000',  # C2V
                     'C3':  '1.0000000'}  # C3

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02'} #MH 

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'nevents':int(nevents)}

#---------------------------------------------------------------------------------------------------
# Generating non-resonant VBF-Only HH process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HHVBF_UFO
generate p p > h h j j $$ z w+ w- / a j QED=4
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for CV, C2V, C3
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Herwig7"]
evgenConfig.description = "Non-resonant LO di-Higgs production through vector-boson-fusion (VBF) which decays to bbZZ (Z->qq, Z->ll)."
evgenConfig.keywords = ["hh","SM", "SMHiggs", "nonResonant", "VBF", "bbbar","ZZ"]
evgenConfig.contact = ['Tulin Varol Mete <Tulin.Varol@cern.ch>']

evgenConfig.nEventsPerJob = 10000
evgenConfig.tune = "H7.1-Default"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands("""
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0; h0->b,bbar;
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/Z0:SelectDecayModes Z0->d,dbar; Z0->u,ubar; Z0->s,sbar; Z0->c,cbar; Z0->b,bbar; Z0->e-,e+; Z0->mu-,mu+; Z0->tau-,tau+;
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hZZFilter", PDGParent = [25], PDGChild = [23])
filtSeq += ParentChildFilter("ZllFilter", PDGParent = [23], PDGChild = [11,13,15])
filtSeq += ParentChildFilter("ZqqFilter", PDGParent = [23], PDGChild = [1,2,3,4,5])

# -- require HH->bbZZ
filtSeq.Expression = "hbbFilter and hZZFilter and ZllFilter and ZqqFilter"
