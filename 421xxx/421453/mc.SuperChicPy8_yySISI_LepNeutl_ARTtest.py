evgenConfig.description = "SuperChic4 MC gamma + gamma pp collisions at 13000 GeV to 2 sleptons"
#evgenConfig.keywords = ["2photon","2muon","dissociation"]
evgenConfig.contact = ["maura.barros@cern.ch"]
evgenConfig.generators += ['SuperChic']
evgenConfig.nEventsPerJob = 1000

from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun

print("#####################")
print(os.environ['LHAPATH'])
print("#####################")
#class with the superchic initialization parameters.  Please see SuperChicUtils for a complete list of tunable parameters.
scConfig = SuperChicConfig(runArgs)

scConfig.isurv     = 4                    # Model of soft survival (from 1 -> 4, corresponding to arXiv:1306.2149)
scConfig.PDFname   = 'MMHT2015qed_nnlo'   # PDF set name
scConfig.PDFmember = 0                    # PDF member
scConfig.proc      = 76                   # Process number (Slepton pair production); Please consult Superchic Manual https://superchic.hepforge.org/
scConfig.beam      = 'prot'               # Beam type ('prot', 'ion')
scConfig.sfaci     = True                 # Include soft survival effects
scConfig.diff      = 'el'                 # Interaction: elastic ('el'), single ('sd','sda','sdb') and double ('dd') dissociation.
scConfig.genunw    = True                 # Generate unweighted events
scConfig.ymin      = -5.0                 # Minimum object rapidity Y_X
scConfig.ymax      = 5.0                  # Maximum object rapidity Y_X
scConfig.mmin      = 10                   # Minimum object mass M_X
scConfig.mmax      = 2000                 # Maximum object mass M_X
scConfig.gencuts   = True                 # Generate cuts below
scConfig.scorr     = True                 # Include spin correlations
scConfig.fwidth    = True                 # Include finite width
scConfig.ptxmax    = 10000000000          # Cut on proton pt 
scConfig.ptamin    = 5.0                  # Minimum pT of outgoing object a 
scConfig.ptbmin    = 5.0                  # Minimum pT of outgoing object b 
scConfig.etaamin   = -5.0                 # Minimum eta of outgoing object a
scConfig.etaamax   = 5.0                  # Maximum eta of outgoing object a
scConfig.etabmin   = -5.0                 # Minimum eta of outgoing object b
scConfig.etabmax   = 5.0                  # Maximum eta of outgoing object b
scConfig.acoabmax  = 100000000000
scConfig.mcharg    = 120                  # Chargino/Slepton mass
scConfig.mneut     = 100                  # Neutralino mass

SuperChicRun(scConfig, genSeq)
include('Pythia8_jj_Common.py')


