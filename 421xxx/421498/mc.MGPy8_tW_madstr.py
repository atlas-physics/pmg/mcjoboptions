# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
       'central_pdf':280000,
       'scale_variations':[0.5,1.,2.],
    }


# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety


# Define the physics process to be simulated
process = """
set nb_core 14
import model loop_sm-no_b_mass
generate p p > t w- [QCD]
add process p p > t~ w+ [QCD]
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process, plugin="MadSTR")


settings = {'nevents':nevents,
            'istr': 1, # change istr parameter here
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir)

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Single top with W' 
evgenConfig.contact = [ "Aman Desai" ]
evgenConfig.keywords += ['SM','top']
evgenConfig.nEventsPerJob = 100

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
