# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *

# For file manipulations
import os

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents*safety if runArgs.maxEvents>0 else safety*evgenConfig.nEventsPerJob


# Define the physics process to be simulated
process = """
set nb_core 14
import model loop_sm-no_b_mass
generate p p > t w- [QCD]
add process p p > t~ w+ [QCD]
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process, plugin="MadSTR")


settings = {'nevents':nevents,
            'istr': 1,
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir)


madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                                                                    
mscard.write(f"""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set ms_dir DR_DECAYS
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

print('Checking time stamps after generate() was called')
import glob
for item in glob.glob(process_dir+'/DR_DECAYS/decay_me/SubProcesses/P*/check'):
    print(f'For {item} : {os.stat(item)}')


# Apply DR
from MadGraphControl.DiagramRemoval import do_DRX
do_DRX(DRmode=1,process_dir=process_dir+'/DR_DECAYS')

# Remove the old decayed events
import shutil
shutil.rmtree(process_dir+'/Events/run_01_decayed_1')

# Recompile the MS directories
import subprocess
original_dir = os.getcwd()
for adir in ['decay_me','full_me','production_me']:
    os.chdir(process_dir+'/DR_DECAYS/'+adir+'/Source')
    fix_makefile1 = subprocess.Popen(['sed','-i',"'/clean:/,+20d'",'makefile'],stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    (out,err) = fix_makefile1.communicate()
    with open('makefile','a') as patched_makefile:
        patched_makefile.write('''
clean:
    $(RM) *.o $(LIBRARIES) $(BINARIES)
    if [ -d $(PDFDIR) ]; then cd $(PDFDIR); make clean; cd ..; fi
    if [ -d PDF/gammaUPC ]; then cd PDF/gammaUPC; make clean; cd ../../; fi
    if [ -d DHELAS ]; then cd DHELAS; make clean; cd ..; fi
    if [ -d CERNLIB ]; then cd CERNLIB; make clean; cd ..; fi
    if [ -d MODEL ]; then cd MODEL; make clean; cd ..; fi
    if [ -d RUNNING ]; then cd RUNNING; make clean; cd ..; fi
    if [ -d BIAS/dummy ]; then cd BIAS/dummy; make clean; cd ../..; fi
    if [ -d BIAS/ptj_bias ]; then cd BIAS/ptj_bias; make clean; cd ../..; fi
    if [ -d $(CUTTOOLSDIR) ]; then cd $(CUTTOOLSDIR); make clean; cd ..; fi
    if [ -d $(IREGIDIR) ]; then cd $(IREGIDIR); make clean; cd ..; fi
    for i in `ls -d ../SubProcesses/P*`; do cd $$i; make clean; cd -; done;
''')
    clean_make = subprocess.Popen(['make','clean'], stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    (out,err) = clean_make.communicate()
    make_command = subprocess.Popen(['make','-j1'], stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    (out,err) = make_command.communicate()
    os.chdir(original_dir)

# Re-run MadSpin - imported with the * above
os.chdir(process_dir)

print('Checking time stamps before add_madspin() call')
import glob
for item in glob.glob(process_dir+'/DR_DECAYS/decay_me/SubProcesses/P*/check'):
    print(f'For {item} : {os.stat(item)}')



add_madspin(process_dir='.')
os.chdir(original_dir)

# These details are important information about the JOs
evgenConfig.description = 'Single top with W' 
evgenConfig.contact = [ "Aman Desai" ]
evgenConfig.keywords += ['SM','top']
evgenConfig.nEventsPerJob=10000

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
