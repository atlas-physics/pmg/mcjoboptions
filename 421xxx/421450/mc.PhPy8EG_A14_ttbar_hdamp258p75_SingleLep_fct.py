# based on 601229 (the same JOs but EvtGen common fragment modified)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune for single lepton events.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'aknue@cern.ch' ]
evgenConfig.nEventsPerJob = 10000

include('PowhegControl/PowhegControl_tt_Common.py')

if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 00000 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"

# Initial settings
PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118 - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                          # Include the NNPDF error set
#PowhegConfig.PDF.extend(range(25201, 25251))                            # Include the MMHT2014nlo68clas118 error set
PowhegConfig.PDF.extend(range(90901, 90931))                            # Include the PDF4LHC15_nlo_30 error set

PowhegConfig.PDF.extend([14000, 14200, 27400, 304400, 331600])  # Add some recent PDF set central values: CT18NNLO, CT18ANNLO, MSHT20nnlo_as118, NNPDF31_nnlo_as_0118_hessian, NNPDF40_nnlo_hessian_pdfas(as=0.118)
PowhegConfig.PDF.extend([14400, 14600, 27100, 331700])  # Add some recent PDF set central values: CT18NLO, CT18ANLO, MSHT20nlo_as118, NNPDF40_nlo_as_01180

filterMultiplier = 2.5 # safety factor
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier
PowhegConfig.rwl_group_events = 50000

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]



#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


