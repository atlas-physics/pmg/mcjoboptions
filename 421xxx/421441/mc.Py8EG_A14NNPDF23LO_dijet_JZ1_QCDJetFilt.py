#based on 369000
# JO for Pythia 8 jet jet JZ0 slice

evgenConfig.description = "Dijet truth jet slice JZ0, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.generators += ["Pythia8"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include ("GeneratorFilters/QCDJetFilter_JZX.py")
QCDJZSlice(1,0.6,filtSeq)

evgenConfig.nEventsPerJob = 1000
