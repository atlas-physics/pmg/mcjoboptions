#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

# Set up an alias to python3 for containers where python is unavailable
PYTHON=$(command -v python)
if [ -z $PYTHON ] ; then PYTHON=$(command -v python3) ; fi

# Original directory
ORIGDIR=$PWD

# Directory for which to run athena
dir=$1
printInfo "INFO: Will run athena for $dir"

# Extract directory names
DSID=$(basename $dir)
DSIDxxx=$(dirname $dir)

# Move to DSID directory (actually 1 directory up since athena won't run if we go in the same dir...)
cd $DSIDxxx

# There should be only 1 file called log.generate.short in each of these directories
if [ ! -f $DSID/log.generate.short ] ; then
  printWarning "WARNING: No log.generate.short in $dir"
  printWarning "athena running will be skipped for this directory, however make sure that the requesters have tested the jO localy!!!"
  cd $ORIGDIR
  exit 0
  # The above solution is not ideal - only temporary until we enforce running athena for all new jO
  # exit 1
else
  printGood "OK: log.generate.short found"
fi

# Check if the job is expected to last more than 1 hour
cpu=$(grep CPU $DSID/log.generate.short | awk '{print $8}')
if [ -z $cpu ]; then
  printWarning "WARNING: No CPU information was provided - fingers crossed you know what you're doing ..."
else
  if $PYTHON -c 'import sys; sys.exit(not(float(sys.argv[1]) > 1.0))' "$cpu"; then
    printError "ERROR: Job is expected to last more than 1h - time estimate: $cpu hours"
    printError "It is not possible to run this job in the CI. Contact the MC software coordinators"
    exit 1
  else
      printGood "OK: Job expected to last less than 1h - time estimate: $cpu hours"
  fi
fi
echo -e "\033[0m" # switch back terminal colour to black

# Setup ATLAS
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

# Get ecmEnergy from the log.generate.short file
ecmEnergy=$(grep 'ecmEnergy =' $DSID/log.generate.short | awk '{print $NF}')
if [ -z $ecmEnergy ] ; then ecmEnergy=13000 ; fi

# Find number of events to run from jO
jOPath=$(ls $ORIGDIR/$DSIDxxx/$DSID/mc.*.py)
nEventsPerJob=$($PYTHON $ORIGDIR/scripts/jo_utils.py --parameter="nEventsPerJob" --jO=$jOPath)

# Number of events to run = max(1,0.01*nEventsPerJob)
nEvents=$($PYTHON -c 'import sys ; print(1 if (1-0.01*float(sys.argv[1])>0) else int(0.01*float(sys.argv[1])))' "$nEventsPerJob")
  
# random seed used for test production
randomSeed=$(grep 'randomSeed =' $DSID/log.generate.short | awk '{print $NF}')

# Check if it's an LHE-only jO
if [[ $(grep LHEonly $DSID/log.generate.short | awk '{print $NF}') == "True" ]] ; then
  LHEonly=true
  nEvents=1
else
  LHEonly=false
fi

# Check if it's an EVNT-to-EVNT job
if [[ $(grep 'EVNT to EVNT' $DSID/log.generate.short | awk '{print $NF}') == "True" ]] ; then
  EVNTtoEVNT=true
else
  EVNTtoEVNT=false
fi

# Check if it's a multicore process
cores=$(grep 'ATHENA_PROC_NUMBER =' $DSID/log.generate.short | awk '{print $NF}')
if [ ! -z $cores ] ; then
  # If the user asked for multi-core mode then use 4 cores and 4 events otherwise the jobs are very slow
  export ATHENA_PROC_NUMBER=4
  nEvents=4
  printInfo "INFO: will run in multi-core mode - cores originally requested: $cores - cores to be used: $ATHENA_PROC_NUMBER - events to generate: $nEvents"
else
  printInfo "INFO: will run in single-core mode"
fi

# Get the release to use from the log.generate.short file
rel=$(grep 'using release' $DSID/log.generate.short | awk '{print $NF}')
relName=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $1}')
relVer=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $2}')
minorRel=$(echo $relVer | awk 'BEGIN {FS="."} ; {print $3}')

# Set up release
asetup $relVer,$relName

# Create a temporary directory from which we will run athena
mkdir tmp_$DSID
cd tmp_$DSID
TMP_DSID_DIR=$PWD

# Find suffix of scope for GRID files based on ecmEnergy
case "${ecmEnergy%.*}" in
  14000)
    ECMENERGY="14TeV"
  ;;
  13000)
    ECMENERGY="13TeV"
  ;;
  13600)
    ECMENERGY="13p6TeV"
  ;;
  8000)
    ECMENERGY="8TeV"
  ;;
  8160)
    ECMENERGY="8p16TeV"
  ;;
  7000)
    ECMENERGY="7TeV"
  ;;
  5020)
    ECMENERGY="5TeV"
  ;;
  5360)
    ECMENERGY="5p36TeV"
  ;;
  2760)
    ECMENERGY="2p76TeV"
  ;;
  *)
    printError "ERROR: unknown ecmEnergy: $ecmEnergy"
    exit 1
    ;;
esac

# Check if external LHE/EVNT files are used
inputGeneratorFile=$(grep inputGeneratorFile ../$DSID/log.generate.short)
inputEVNT_PreFile=$(grep inputEVNT_PreFile ../$DSID/log.generate.short)

# Find scope for GRID files - use wildcards here to avoid having to parse paths which might differ from case to case
if [[ ! -z $inputGeneratorFile ]] ; then
  if [[ $inputGeneratorFile == *"group.phys-gener"* ]] ; then
    scope="group.phys-gener"
  elif [[ $inputGeneratorFile == *"mc15"* ]] ; then
    scope="mc15_$ECMENERGY"
  elif [[ $inputGeneratorFile == *"mc21"* ]] ; then
    scope="mc21_$ECMENERGY"
  elif [[ $inputGeneratorFile == *"mc23"* ]] ; then
    scope="mc23_$ECMENERGY"
  else
    printError "ERROR: could not find scope"
    exit 1
  fi
  printInfo "Scope for GRID file: $scope"
fi

# Find scope for EVNT files for E2E jobs
if [[ ! -z $inputEVNT_PreFile ]] ; then
  scope=$(echo $inputEVNT_PreFile | grep -o -E 'mc[0-9]{2}_.[0-9]*p?[0-9]*TeV' | head -1)
  printInfo "Scope for EVNT file: $scope"
fi

# If external LHE/EVNT is used setup rucio and download file
if [[ ! -z $inputGeneratorFile || ! -z $inputEVNT_PreFile ]] ; then
  # Set rucio account
  export RUCIO_ACCOUNT=cgutscho
  # Set up voms proxy
  printInfo "Job with external input files requested. Setting up voms proxy..."
  lsetup "rucio -w"
  echo $GRID_PW | voms-proxy-init --voms atlas --pwstdin --cert /root/.globus/usercert.pem --key /root/.globus/userkey.pem
  # Get the actual files to download
  if [[ ! -z $inputGeneratorFile ]] ; then
    echo $inputGeneratorFile | awk -F'inputGeneratorFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' -v scope=$scope '{printf "%s:%s\n", scope, $NF}'  > filesToDownload
  fi
  if [[ ! -z $inputEVNT_PreFile ]] ; then
    # Input files for EVNT-to-EVNT job
    # 13TeV EVNT files had mc16_ECMENERGY scope but sometimes might have mc15_ECMENERGY (if not merged)
    # 13p6TeV input files always have mc21 in the scope - the ecmEnergy is hardcoded since it's not printed in log.afterburn
    echo $inputEVNT_PreFile | awk -F'inputEVNT_PreFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' -v scope=$scope '{printf "%s:%s\n", scope, $NF}' > filesToDownload
  fi
  # Download the file
  printInfo "Will download the following files:"
  cat filesToDownload
  echo "----------------------------------"
  rucio get `cat filesToDownload`
  mv mc*_*/* . && rm -rf mc*_*
  mv group.phys-gener/* . && rm -rf group.phys-gener
  # Check if the file has been downloaded - if not exit with error
  for file in `cat filesToDownload | awk -F ':' '{print $2}' | uniq` ; do
    if [ ! -f "$file" ] ; then
        printError "ERROR: file $file could not be downloaded from the grid"
        exit 1
    else
      printGood "OK: file $file successfully downloaded"
    fi
  done
fi

# If there is a GRID file then copy it to the directory where we run athena
# First we need to construct the filename from the ecmEnergy (there might be more than 1 GRID files in the DSID dir)
GRIDfile=$(ls ../$DSID/mc*_$ECMENERGY.*.GRID.tar.gz 2>/dev/null)
if [ ! -z "$GRIDfile" ] ; then
  link=$GRIDfile
  # We need to iterate here to resolve relative links
  relLink=0
  while $(test -L $link) ; do
    link=$(readlink $link)
    # Jump to the directory where the parent link is located
    cd $(dirname $link) 2> /dev/null
    let relLink=$relLink+1
    if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
      printError "ERROR: file $GRIDfile is a link that points back to itself"
      exit 1
    fi
  done
  # Go back to DSID directory where we started
  cd $TMP_DSID_DIR
  
  printInfo "INFO: GRID file $GRIDfile will be copied from $link"
  GRIDfilename=$(basename $link)
  # Remove the link
  rm -f $GRIDfile
  # And copy the actual file
  if [[ "$link" == "/eos/user"* ]] ; then
    HOST=root://eosuser.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  elif [[ "$link" == "/eos/atlas"*  ]] ; then
    HOST=root://eosatlas.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  else
    cp $link ../$DSID/$GRIDfilename
  fi
  
  if [ -f ../$DSID/$GRIDfilename ] ; then
    printGood "OK: File successfully copied"
  else
    printError "ERROR: Copying GRID file failed"
    exit 1
  fi
fi

# Set up generation command
com="Gen_tf.py --ecmEnergy=$ecmEnergy \
  --jobConfig=../$DSID \
  --maxEvents=$nEvents"
if [ ! -z $randomSeed ] ; then com="$com --randomSeed=$randomSeed"; fi
if $LHEonly; then # LHE only generation
  com="${com} --outputTXTFile=LHE.TXT.tar.gz"
else # EVNT generation
  com="${com} --outputEVNTFile=EVNT.root"
  # EVNT generation with input LHE files
  if [[ ! -z $inputGeneratorFile ]] ; then
    com="${com} --inputGeneratorFile=$(echo $inputGeneratorFile | awk -F'inputGeneratorFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' '{printf "%s,", $NF} ' | sed 's/,*$//g')"
  fi
  # EVNT-to-EVNT transform with input EVNT files
  if $EVNTtoEVNT; then
    com="${com} --steering=afterburn --inputEVNT_PreFile=$(echo $inputEVNT_PreFile | awk -F'inputEVNT_PreFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' '{printf "%s,", $NF} ' | sed 's/,*$//g')"
  fi
fi

# Run athena
printInfo "Will now run $com"
set -e # to catch any error from the execution of Gen_tf
eval $com
set +e # to avoid exiting from the script if any other setup command fails in the loop

# Update the number of jobs processed
let jobsProcessed=$jobsProcessed+1

# Rename log.generate output from transform to log.generate_ci
# which is uploaded as an artifact
if [ -f log.generate ] ; then
  mv log.generate ../$DSID/log.generate_ci
elif [ -f log.afterburn ] ; then
  # NB: we name the file log.generate_ci so that the next job (logParser) can find it
  mv log.afterburn ../$DSID/log.generate_ci
else
  printError "ERROR: no log file produced"
  exit 1
fi

# Move to main directory
cd $ORIGDIR

exit 0

