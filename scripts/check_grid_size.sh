#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

# First get any changes from master
git pull --rebase origin master

printInfo "Find links added in the last commit..."
# Find number of files that have been:
# Added(A), Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
# (i.e. regular file, symlink, submodule, …​) changed (T), are Unmerged (U),
# are Unknown (X), or have had their pairing Broken (B).
changed=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMCRTUXB | grep 'GRID.tar.gz'))

if (( ${#changed[@]} == 0 )) ; then
  printGood "No GRID.tar.gz files added/modified since last commit"
  exit 0
fi

basedir=$PWD

# Loop over all links
for file in "${changed[@]}" ; do

  printInfo "Checking file: $file..."
  
  # Go into directory where the GRID file is
  dir=$(dirname $file)
  cd $dir
  
  # Check where the file is located
  link=$(basename $file)
  # We need to iterate here to resolve relative links
  relLink=0
  while $(test -L $link) ; do
    link=$(readlink $link)
    # Jump to the directory where the parent link is located
    cd $(dirname $link) 2> /dev/null
    let relLink=$relLink+1
    if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
      printError "ERROR: file $file is a link that points back to itself"
      exit 1
    fi
  done

  if [[ "$link" == "/eos/user"* ]] ; then
    HOST=root://eosuser.cern.ch
  elif [[ "$link" == "/eos/atlas"*  ]] ; then
    HOST=root://eosatlas.cern.ch
  elif [[ "$link" == "/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/"*  ]] ; then
    printGood "OK: The link is pointing to /cvmfs. This means the GRID file size has already been checked previously. Nothing more to check."
    exit 0
  else
    printError "The link is not pointing to /eos/atlas or /eos/user"
    readlink $file
    printError "Please transfer the file to either /eos/atlas or /eos/user"
    exit 1
  fi

  printInfo "Will now check size of file $link"
  # Check size
  size=$(eos $HOST ls -l $link | awk '{print $5}')

  # Fail if size > 100 MB
  if (( $size > 100000000 )) ; then
    size_mb=$(python3 -c "print(\"%.1fMB\" % (${size}/1e6))")
    printError "ERROR: $file -> $link size : $size_mb"
    printError "Files above 100MB could be problematic. Please contact the MCJobOptions experts if you're unsure."
    exit 1
  else
    printGood "OK: $file size : $size"
  fi
  
  cd $basedir
done

exit 0

