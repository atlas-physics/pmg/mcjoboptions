#!/usr/bin/python

from __future__ import print_function
import os, re, string, subprocess, sys, ast

# This is needed for Gen_tf
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/scripts")
import jo_utils

# function to get list of unique DSIDs from a specific branch in git remote
def dsidInBranch(dsid, branch):
    cmd_git_content = rf'git ls-tree -r --name-only {branch}'
    return bool(re.match(f'/{dsid}/', jo_utils.bashExec(cmd_git_content)))
    
# function to check if a DSID is free
def isFreeDSID(dsid):
    isFree=True
    # Get current branch
    try:
        currentBranch=os.environ["CI_COMMIT_REF_NAME"]
    except:
        command="git rev-parse --abbrev-ref HEAD"
        currentBranch = jo_utils.bashExec(command)
    # Get all remote branches excluding current branch
    cmd_git_branches = "git for-each-ref --format=%(refname) refs/remotes/origin/"
    for branch in jo_utils.bashExec(cmd_git_branches).split('\n'):
        if branch == currentBranch:
            continue
        if dsidInBranch(dsid, branch):
            print(f'\tERROR: {dsid} used in branch: {branch}')
            isFree=False
            break
    return isFree

def makeDSIDblocks(freeDSIDs, minDSID, maxDSID):
    """Function to return list of consecutive DSIDs within a given range.
       Implementation based on a solution by Frank Sauerburger."""
    freeConsecutiveDSIDs = []
    previousDSID = freeDSIDs[0]
    current_block = [previousDSID]
    for dsid in freeDSIDs[1:]:
        if dsid == previousDSID+1:
            current_block.append(dsid)
        else:
            freeConsecutiveDSIDs.append(current_block)
            current_block = [dsid]
        previousDSID = dsid
    freeConsecutiveDSIDs.append(current_block)
    return freeConsecutiveDSIDs

def findSmallestFreeDSIDblock(allowed_ranges, length):
    """Function to find a list of free DSIDs. 
       Returns the first free DSID block it finds."""
    occupiedDSIDs = set()
    cmd_git_branches = "git for-each-ref --format=%(refname) refs/remotes/origin/"
    for branch in jo_utils.bashExec(cmd_git_branches).split('\n'):
      cmd_git_content = rf'git ls-tree -r --name-only {branch}'
      pat = re.compile(jo_utils.git_dsid_pattern(allowed_ranges))
      # Pattern match returns strings so use a map to convert to integers
      occupiedDSIDs.update(map(int,pat.findall(jo_utils.bashExec(cmd_git_content))))
    # Get list of free DSIDs
    freeDSIDblocks = [ ]
    for grp in allowed_ranges:
      # Get minimum and maximum DSID for generator
      minDSID, maxDSID = jo_utils.get_dsid_minmax(grp)
      # Get the free DSIDs - here we use a set difference which is fast
      # Set difference needs to be sorted
      freeDSIDs = sorted(set(range(minDSID,maxDSID+1)) - occupiedDSIDs)
      # Assemple the free DSIDs in consecutive blocks with len >= length
      freeDSIDblocks += [b for b in makeDSIDblocks(freeDSIDs, minDSID, maxDSID) if len(b)>=length]
      if len(freeDSIDblocks):
        break
    # Return the smallest DSID block that is of length >= length
    return freeDSIDblocks[0]
    
# function to check the DSID range
# increment is used to handle several DSIDs
def check_dsidrange(generatorName, dsidList):
    if not len(dsidList):
      # nothing to be done
      return False

    # Get allowed DSID ranges according to generator name
    allowedDSIDranges = jo_utils.OPEN_BLOCKS[generatorName]

    # Get the lowest block of free DSIDs between minDSID and maxDSID that is at least of size len(dsidList)
    freeDSIDblock = findSmallestFreeDSIDblock(allowedDSIDranges, len(dsidList))

    # Loop over dsid list and check if they are in the correct range
    bookedDSIDs=0 # to keep track of how many DSIDs have been assigned
    DSIDinWrongRange=False
    for dsid in dsidList:
        # Check if the DSID is within (any of) the allowed range(s)
        isCorrectRange = any([ re.match(fr'^{pat}$', dsid) for pat in allowedDSIDranges ])
        # 421xxx is handled as an exception since only ART tests should go there
        if dsid.isdigit() and (isCorrectRange or dsid.startswith("421")):
            print("\tOK: correct DSID range")
            # If the DSID is already in the master branch we don't need to test if it's free
            if dsidInBranch(dsid, "refs/remotes/origin/master"):
                continue
            else:  # Check if the DSID is not used by any other branch in remote
                if isFreeDSID(dsid):
                    print(f'\tOK: {dsid} not used by other branches')
                else:
                    DSIDinWrongRange=True
        else:
            DSIDinWrongRange=True
        if DSIDinWrongRange:
            print(f'\tERROR: DSID: {dsid}, generator: {generatorName} => allowed DSID range(s):')
            for pat in allowedDSIDranges:
                print('\t\t\t' + jo_utils.get_dsid_range(pat))
            currentDSIDxxx = f"{dsid[:1]}/{dsid[:4]}xxx/" if dsid.isdigit() and len(dsid) == 7 else \
                             f"{dsid[:3]}xxx/" if dsid.isdigit() and len(dsid) == 6 else ""
            freeDSID = str(freeDSIDblock[bookedDSIDs])
            freeDSIDxxx = f"{freeDSID[:1]}/{freeDSID[:4]}xxx/" if freeDSID.isdigit() and len(freeDSID) == 7 else \
                          f"{freeDSID[:3]}xxx/" if freeDSID.isdigit() and len(freeDSID) == 6 else ""
            print(f"\t\tMove {currentDSIDxxx}{dsid} -> {freeDSIDxxx}{freeDSID}")
            bookedDSIDs += 1
            
    return DSIDinWrongRange
        

# function to check contents of jO file
def check_jOcontent(file, isTopLevel=True):
    jOFile = os.path.abspath(file)
    errors=False
    warnings=False
    with open(jOFile, "rb") as f:
        jOcontent=f.read()
        # Check that there is no MC15JobOptions include
        if b'MC15JobOptions' in jOcontent:
            print(f'\tERROR: file {jOFile} contains includes pointing to MC15JobOptions')
            errors=True
        else:
            print("\tOK: file does not contain includes pointing to MC15JobOptions")
        # Check that the file is in unix format
        if b'\r\n' in jOcontent:
            print(f'\tERROR: file {jOFile} is in DOS format. Use dos2unix to convert it.')
            errors=True
        else:
            print("\tOK: file in unix format")
        # Check that the file does not contain any evgenConfig.minevents parameter
        if jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "evgenConfig", "minevents") is not None:
            print(f'\tERROR: file {jOFile} contains deprecated evgenConfig.minevents parameter. Use evgenConfig.nEventsPerJob instead.')
            errors=True
        else:
            print("\tOK: evgenConfig.minevents not found")
        # Check that the top-level jO file sets evgenConfig.nEventsPerJob parameter
        if jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "evgenConfig", "nEventsPerJob") is None and isTopLevel:
            print(f'\tERROR: file {jOFile} does not set evgenConfig.nEventsPerJob.')
            errors=True
        else:
            print("\tOK: evgenConfig.nEventsPerJob is set")
        # Check inputFilesPerJob
        if b'evgenConfig.inputFilesPerJob' in jOcontent:
            # Check that evgenConfig.inputFilesPerJob is integer
            inputFilesPerJob=jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "evgenConfig", "inputFilesPerJob")
            if not isinstance(inputFilesPerJob, int):
                print(f'\tERROR: file {jOFile} contains evgenConfig.inputFilesPerJob = {inputFilesPerJob} <- this should be an integer')
                errors=True
            else:
                print("\tOK: evgenConfig.inputFilesPerJob is integer")
        # Check that generatorName is not set in the jO, since it should be set automatically
        if jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "evgenConfig", "generators") is not None:
            print(f'\tERROR: file {jOFile} contains evgenConfig.generators. This should not be set unless you are using a special setup.')
            warnings=True
        else:
            print("\tOK: evgenConfig.generators not found")
        # Check that the file does not contain evgenConfig.inputfilecheck
        if jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "evgenConfig", "inputfilecheck") is not None:
            print(f'\tERROR: file {jOFile} contains evgenConfig.inputfilecheck. You should remove this.')
            errors=True
        else:
            print("\tOK: evgenConfig.inputfilecheck not found")
        # Check that the file does not contain runArgs.inputGeneratorFile
        if jo_utils._read_param_from_jo_withDict(jOcontent.decode(), "runArgs", "inputGeneratorFile") is not None:
            print(f'\tERROR: file {jOFile} tries to set runArgs.inputGeneratorFile. You should avoid this.')
            errors=True
        else:
            print("\tOK: runArgs.inputGeneratorFile is not set in jO")
        # Check that the file does not try to overwrite os.environ('ATHENA_PROC_NUMBER')
        changedNProc, val = jo_utils._env_set(jOcontent.decode(), "ATHENA_PROC_NUMBER")
        if changedNProc:
            print(f'\tERROR: file {jOFile} tries to set ATHENA_PROC_NUMBER to {val}. This is not allowed!')
            errors=True
        else:
            print("\tOK: ATHENA_PROC_NUMBER not modified")
        # Check if jO is trying to import certain modules
        if jo_utils._check_import(jOcontent.decode(), ["TestHepMC","TestLHE"]):
            msg = f'\tERROR: file {jOFile} tries to import TestHepMC/TestLHE. '\
                  f'These have already been scheduled by the transform and a second instantiation is not allowed.'
            print(msg)
            errors=True
    return errors, warnings

# Function to check python syntax
def test_py3_syntax(file):
    try:
        ast.parse(open(file, 'rb').read())
        print("\tOK: File syntax is python3 compatible")
        return 0
    except SyntaxError as exc:
        print("\tERROR: File syntax is not python3 compatible")
        print(f"\t\t{exc}")
        return 1
