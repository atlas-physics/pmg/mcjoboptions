#!/usr/bin/python

from __future__ import print_function
import os, re, string, subprocess, sys

# This is needed for Gen_tf
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/scripts")

# Function to check the jO naming
def check_naming(jofile):
    joparts = jofile.split(".")
    officialJO = False
    error=False
    if joparts[0].startswith("mc"):
        officialJO = True
        ## Check that the JO does not appear to be an old one, since we can't use those
        if joparts[0] != "mc":
            print("\tThe jO file name prefix should be mc not " + joparts[0])
            error=True
        else:
            print("\tOK: New jobOption file")
        ## Check that there are exactly 3 name parts separated by '.': mc,  physicsShort, .py
        if len(joparts) != 3:
            print("\tERROR:" + jofile + " name format is wrong: must be of the form mc.<physicsShort>.py: please rename.")
            error=True
        else:
            print("\tOK: name format correct" )
                
    ## Check the length limit on the physicsShort portion of the filename
    jo_physshortpart = joparts[1]
    if len(jo_physshortpart) > 50:
        print("\tERROR: " + jo_physshortpart + " contains a physicsShort field of more than 50 characters: please rename.")
        error=True
    else:
        print("\tOK: " + jo_physshortpart + " physicsShort less than 50 characters long")

    jo_physshortparts = jo_physshortpart.split("_")
    ## There must be at least 2 physicsShort sub-parts separated by '_': gens, (tune)+PDF, and process
    if len(jo_physshortparts) < 2:
        print("\tERROR: " + jofile + " has too few physicsShort fields separated by '_': should contain <generators>(_<tune+PDF_if_available>)_<process>. Please rename.")
        error=True
    else:
        print("\tOK: 2 physicsShort parts found")

    # Check if the filename contains special characters
    special="!@~#$%^&*()-+"
    if any(c in jofile for c in special):
        print("\tERROR: " + jofile + " contains special characters. The following characters are not allowed " + special + ". Please rename.")
        error=True
    else:
        print("\tOK: file name does not contain any special characters")

    # If this is a Sherpa jO check that Sherpa version is not given in the name in full
    if jo_physshortparts[0] == "Sh":
        if jo_physshortparts[1][:3].isdigit() and len(jo_physshortparts[1][:3]) > 2:
            print("\tERROR: " + jofile + " contains the full Sherpa version in the name. Only 2 digits are allowed corresponding to major and minor release.")
            error=True
        else:
            print("\tOK: does not contain full Sherpa version in name")

    # Where the Generator list files are located, need to change if the txt file is placed somewhere else.
    File_path = os.path.dirname(os.path.abspath(__file__))
    GenListFile = File_path+'/../common/GeneratorList.txt'
    if os.path.exists(GenListFile):
        print ("\tOK: Loading generator list file from "+ GenListFile)
    elif os.path.exists('/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/common/GeneratorList.txt'):
        print ("\tOK: Loading generator list file from cvmfs")
        GenListFile = '/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/common/GeneratorList.txt'
    else:
        print ("\tERROR: Generator list file not found!")
        error=True
    #read Generator name in a txt file in common folder. 
    Generators = {}
    with open(GenListFile) as f:
        for line in f:
            (key, val) = line.split()
            Generators[key] = val

    Generators_abb = list(Generators.values())
    if joparts[0].startswith("mc"): #< if this is an "official" JO
        genpart = jo_physshortparts[0]
        #Check No Generator full name in physicsshort. except the for some generator doesn't have short name. 
        GenFullNameNo = 0
        for Generator, Generator_abb in Generators.items():
            if genpart.find(Generator)!=-1 and Generator!=Generator_abb:
                print ("\tERROR: Generator full name "+Generator+" is found, please use abbreviation "+Generator_abb)
                GenFullNameNo += 1
        if GenFullNameNo>0:
            print ("\tERROR: %s generator full names are found! Please use abbreviation instead." %GenFullNameNo)
            error=True
        else:
            print ("\tOK: No generator full name is found")

        #Check No unrecognized generator abbreviation in physicsshort.                                                                                                                                     
        generator_list=[]
        i = 0
        while i<len(genpart):
            for step in range(1, len(genpart)+1-i):
                if genpart[i:i+step] in Generators_abb:
                    generator_list.append(genpart[i:i+step])
                    i+=step
                    break
                elif step==len(genpart)-i:
                    print ("\tERROR: Not recognised generator abbreviation " + genpart[i:i+step])
                    i+=step
                    error=True
        print ("\tGenerators used: ", generator_list)
        
        # Check if there is a dash in the filename
        if "-" in jofile:
            print("\tERROR: " + jofile + " contains a dash which is not allowed: please remove it.")
            error=True
            
        # Return the first name of the generator
        if error:
            return None
        else:
            if "valid" in jofile.lower():
                return "Val"
            else:
                return generator_list[0]

