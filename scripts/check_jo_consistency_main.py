#! /usr/bin/env python3

from __future__ import print_function
import os, re, string, subprocess, sys
from check_jo_consistency import *
from check_jo_consistency_helpers import *
from jo_utils import ALLOWED_PATHS

"""
Script to check the DSID, naming and content of jO files.
Usage:
    1. provide list of files as command-line arguments
    2. if no argument is provided, files are obtained from git diff of current HEAD to master branch
"""

# Main script
def main():
    print("\n===> Checking jobOption consistency...\n")
    files=list()
    # If command line arguments are provided use those
    if (len(sys.argv) > 1):
        for arg in range(1,len(sys.argv)):
            files.append(sys.argv[arg])
    # Otherwise get list of files from git diff-tree command
    else:
        command="git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMR | grep -E \"mc.*.py\""
        # filter list of string obtained from git diff to remove empty strings (no file modified)
        files=list(filter(None,os.popen(command).read().strip().split("\n")))
    
    hion_gens = [ "Hijing", "Hydjet", "HvyN", "Starlight", "AMPT", "SuperChic" ]
    error_DSIDrange, error_jOcontent, warning_jOcontent = 0, 0, 0
    if files:
        # Keep track of the DSIDs that correspond to each generator
        newDSIDs = {"MG":  [], "aMC": [], "Ph":  [], "Sh":  [], "Py8":  [], "P8B": [], "H7": [], "HI": [], "Val": [], "Misc": []}
        # Loop over jO files to check for errors and count DSIDs needed
        for file in files:
            # Check if file exists
            if not os.path.exists(file):
                print(f'ERROR: file {file} does not exist!')
                sys.exit(1)
            try:
                # Extract naming
                dsiddir, jofile = os.path.split(file)
                if not re.match(ALLOWED_PATHS, dsiddir):
                    raise ValueError
                dsidxxx, dsiddir = os.path.split(dsiddir)
                pre = ''
                if '/' in dsidxxx:
                    pre, dsidxxx = dsidxxx.split('/')
                # Check that the jO lives in a directory named like (N)NNNxxx
                if len(dsiddir) == 7 and (dsidxxx[:4] != dsiddir[:4] or pre != dsiddir[:1]):
                    msg = f'ERROR: modified jO lives in directory {pre}/{dsidxxx}/{dsiddir}. '\
                          f'Need to put it in {dsiddir[:1]}/{dsiddir[:4]}xxx/{dsiddir}'
                    print(msg)
                    error_DSIDrange = True
                elif dsidxxx[:3] != dsiddir[:3]:
                    msg = f'ERROR: modified jO lives in directory {dsidxxx}/{dsiddir}. '\
                          f'Need to put it in {dsiddir[:3]}xxx/{dsiddir}'
                    print(msg)
                    error_DSIDrange = True
                  
            except ValueError:
                dsiddir, jofile = os.path.split(file)
            
            print(f'\nModified jO file: {file} - jO file = {jofile}')
            # Get the name of the first generator
            firstGenerator = check_naming(jofile)
            if firstGenerator == None:
                print("Errors occured. Exiting.")
                sys.exit(1)

            # Keep count of how many new DSIDs we need per generator
            if firstGenerator in newDSIDs: # Most commonly used generators
                # For the generators that have the same DSID range make sure we pick DSIDs from the same block
                if firstGenerator == "aMC": firstGenerator="MG"
                elif firstGenerator == "P8B": firstGenerator="Py8"
                newDSIDs[firstGenerator].append(dsiddir)
            else:
                if firstGenerator in hion_gens:
                    newDSIDs["HI"].append(dsiddir)
                else: # Everything else
                    newDSIDs["Misc"].append(dsiddir)


            # Check whether the file includes any MC15JobOption includes and if it's in unix format
            error_jOcontent, warning_jOcontent = check_jOcontent(file)
            
            # Check python syntax
            error_jOcontent += test_py3_syntax(file)

            # Extract a control file if present
            with open(file,'r') as f:
                lines = f.readlines()
            for line in lines:
                if "include" in line:
                    match = re.search(r"\((['\"]?)(.*?)\1?\)", line)
                    if match:
                        path = match.group(2)
                        controlFile = os.path.join(os.path.dirname(file), path.split('/')[-1])
                        if os.path.exists(controlFile):
                            print(f'\n\tFound control file for: {file} -> {controlFile}')
                        else:
                            continue

                        # Apply consistency checks in control file
                        error_jOcontent += check_jOcontent(controlFile, isTopLevel=False)[0]
                        error_jOcontent += test_py3_syntax(controlFile)
                        
        # Loop over list of DSIDs for each generator and check if they are in the correct DSID range
        for key in newDSIDs:
            error_DSIDrange += check_dsidrange(key, newDSIDs[key])

    else:
        print("OK: no modified jO files")
    
    if error_DSIDrange or error_jOcontent:
        print("\n\n===> Errors found - check output of individual checks above")
        sys.exit(1)
    else:
        if warning_jOcontent:
            print("\n\n===> Warnings found - check output of individual checks above")
            sys.exit(2)
        else:
            print("\n\n===> All checks successfull")
            sys.exit(0)

if __name__== "__main__":
    main()
    

