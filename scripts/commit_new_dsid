#! /usr/bin/env python3

"""\
Register JobOptions in the MCJobOptions repository

Example:
  %(prog)s [options]
"""

####################
# Helper functions #
####################

import os, re, operator

from jo_utils import bashExec, checkWhitelist, ALLOWED_PATHS
from jo_utils import printError, printWarning, printOK, printInfo


def parseDSIDList(dsid_stream):
  """A function to parse the DSIDs to be committed to git"""
  return sum(((list(map(str, range(*[int(j) + k for k,j in enumerate(i.split('-'))]))) \
           if '-' in i else [str(i)]) for i in dsid_stream.split(',')), []) if dsid_stream else []


def makeRanges(dsidSeq):
    """A function to determine consecutive DSIDs and work out ranges.
    Takes as input an array of string-DSIDs.
    Prints out a string of comma-separated individual DSIDs / DSID ranges,
    e.g. ['123456', '600000', '600001', '600002', '987654' ]
    -> 123456,600000-600002,987654"""
    from itertools import groupby
    from operator import itemgetter
    dsids = [ int(d) for d in dsidSeq ] # turn strings to integers
    groups = [ list(map(itemgetter(1), l)) for k, l in groupby(enumerate(dsids), lambda item: item[0]-item[1]) ]
    # "groups" is an array of arrays. The inner arrays contain at least one DSID. 
    # If there's more than one DSID in an array, they must be consecutive, i.e. form a range.
    return ','.join([ '%i-%i' % (g[0], g[-1]) if len(g) > 1 else str(g[0]) for g in groups ])



def bookDSIDs(DSIDtoMove, dryRun = False):
  """\
    DSID is a string like: "999xxx/999995 -> 800xxx/800037", extracted
    by regex from the scripts/check_jo_consistency_main.py output
  """
  DSIDdict = { }
  # Loop over DSIDs that have to be moved
  for dsid in DSIDtoMove:
    initialDSIDdir, _, finalDSIDdir = dsid.split()
    finalDir, finalDSID = finalDSIDdir.split('/')
    # standard structure is 100xxx/100000 or 1/1000xxx/1000000
    if re.match(ALLOWED_PATHS, initialDSIDdir):
      # this covers cases where the initial DSIDs are placed in dummy directories 
      initialDSID = int(initialDSIDdir.split('/')[-1])
    else:
      # this covers cases where the initial DSIDs are assigned arbitrary names
      initialDSID = initialDSIDdir
    if dryRun: # Dry-run: inform the user which DSIDs will be assigned
      printWarning(f'\t\tWill move {initialDSIDdir} to {finalDSIDdir}', notag = True)
    else: # book the DSID
      # If the dummy directory does not exist then we have to create it first
      os.makedirs(finalDir, exist_ok=True)
      if os.path.isdir(finalDSIDdir):
        msg = f'\t\tERROR: Attempt to copy {initialDSIDdir} to {finalDSIDdir} '\
              f'but the latter already exists in your local repository.'
        printError(msg, notag = True)
        sys.exit(20)
      else:
        printOK(f'\t\tCopying {initialDSIDdir} to {finalDSIDdir}', notag = True)
        bashExec(f'cp -a {initialDSIDdir} {finalDSIDdir}')
      # Keep track of the DSIDs that are being assigned
      DSIDdict[initialDSIDdir] = finalDSIDdir
  # Now that we have the dictionary of moved DSIDs, loop once again over moved DSIDs and fix the links pointing to old DSIDs
  for old_dsid, new_dsid in DSIDdict.items():
    # Check if there are any links in the new directory
    links = bashExec(f'find {new_dsid} -type l').splitlines()
    for link in links:
      # Read where the link is pointing to
      oldLinkedFile = bashExec(f'readlink {link}')
      if oldLinkedFile.startswith('/eos') or oldLinkedFile.startswith('/cvmfs'):  continue # absolute path, nothing to be done
      if not re.match(r'^\.', oldLinkedFile):
        printError(f'ERROR: {link} is not a relative link or is pointing to a file in the same directory', notag = True)
        sys.exit(20)
      # Does the link point to a DSID directory?
      DSIDmatches = re.compile(r'([0-9]{6,7})').findall(oldLinkedFile)
      if len(DSIDmatches) == 0:
        printWarning(f'Check that {link} still points to a reasonable target!')
        continue
      # Extract the directory where the link is pointing to
      linkedDSID = DSIDmatches[0]
      linkedDir = f'{linkedDSID[:3]}xxx/{linkedDSID}' if len(linkedDSID) == 6 else \
                      f'{linkedDSID[0]}/{linkedDSID[:4]}xxx/{linkedDSID}'
      # Find if DSIDtoMove is inside the DSIDdict
      # We need to search here by substring in the dictionary keys since if a DSID
      # is outside the main directory a link might point to ../../100xxx/100000/file.txt
      # while DSIDtoMove will be ../100xxx/100000
      newTargetDir = linkedDir
      for key in DSIDdict.keys():
        if operator.contains(key,linkedDir):
          newTargetDir = DSIDdict[key]
      # Find where newTargetDir is located relative to the oldDir
      oldLinkedFileName = oldLinkedFile.split('/')[-1]
      relPath = os.path.relpath(f'{newTargetDir}/{oldLinkedFileName}', new_dsid) # linked file in different DSID directory
      # Replace old link with new one
      bashExec(f'rm -f {link}')
      bashExec(f'ln -s {relPath} {link}')
  return DSIDdict




if __name__ == "__main__":

  import argparse, sys, glob

  # Check Python version
  if not (sys.version_info.major >= 3 and sys.version_info.minor >= 6):
    printError('Python version < 3.6 being used. Please update to v3.6 or higher')
    sys.exit(10)

  parser = argparse.ArgumentParser(usage=__doc__)
  parser.add_argument(dest='job_option_directory', nargs="*",
                      help='one or more JobOption directory to be registered')
  parser.add_argument('-d', '--dsid', dest="DSID", default="",
                      help='takes as arguments a comma-separated list of DSIDs to commit (DSID1-DSID5 specifies a range of DSIDs)')
  parser.add_argument('-a', '--auto', dest="AUTO", action="store_true", default=False,
                      help='launch pipeline for automatically merging jO')
  parser.add_argument('-b', '--branch', dest="BRANCH", default="", help='specify the name of the branch to be created')
  parser.add_argument('--dry-run', dest="DRY_RUN", action="store_true", default=False,
                       help='Dry run: will perform jO consistency checks, and print out the list of DSIDs to be assigned.' \
                       + ' It will not copy DSIDs to the correct location or push to git')
  parser.add_argument('-n', '--nogit', dest="GIT", action="store_false", default=True,
                      help='Same as dry run, but including the assignment of the DSIDs (copy dummy DSIDs to correct location).' \
                      + ' Nothing will be pushed to git')
  parser.add_argument('-m', dest="COMMIT_MESSAGE", default="", 
                      help='Commit message to be added to git commit (here you can also specify which CI jobs to skip')
  parser.add_argument('-u', dest="CPU_OPT", action="store_true", default=False, 
                      help='will ignore CPU timing information when running the logParser checks (only to be used by experts)')

  if len(sys.argv) == 1:
    printError("no arguments specified")
    parser.print_help(sys.stderr)
    sys.exit(2)

  args = parser.parse_args()

  if args.DRY_RUN:
    args.GIT = False

  ####################
  #   Main script    #
  ####################

  # The absolute path of where the mcjoboptions directory resides
  MCJODIR = os.getcwd()

  for item in args.job_option_directory:
    if os.path.isdir(item):
      printOK(f"Found jO directory {item}")
    else:
      printError(f'Unrecognised option {item}')
      sys.exit(4)
  newDSID = args.job_option_directory

  # Make sure obligatory arguments have been specified
  if args.COMMIT_MESSAGE == '':
    printError("you have to specify a commit message with -m=\"message\"")
    sys.exit(5)

  newDSID += parseDSIDList(args.DSID)
  if len(newDSID) == 0:
    printError("no DSIDs specified to commit. Use -d=DSID1-DSID5,DSID6...")
    sys.exit(6)
  
  # Turn on automatic MR opening
  if args.AUTO:
    printInfo("Automatic MR creation requested")
    args.COMMIT_MESSAGE += " [auto]"
  
  # Parse commit message
  skipAthena = False
  if '[skip' in args.COMMIT_MESSAGE:
    if '[skip nfiles]'    not in args.COMMIT_MESSAGE and \
       '[skip modfiles]'  not in args.COMMIT_MESSAGE and \
       '[skip athena]'    not in args.COMMIT_MESSAGE and \
       '[skip logparser]' not in args.COMMIT_MESSAGE:
        printError('you are trying to skip a pipeline with an invalid option.')
        printError('Valid options are: [skip nfiles],[skip modfiles],[skip athena],[skip logparser]')
        sys.exit(1)
    elif '[skip athena]' in args.COMMIT_MESSAGE or '[skip logparser]' in args.COMMIT_MESSAGE:
      skipAthena = True

  remoteName = None
  try:
    for line in bashExec('git remote -v').split('\n'):
      if '/atlas-physics/pmg/mcjoboptions.git' in line and '(push)' in line:
        remoteName = line.split('\t')[0]
        printInfo(f'will use following remote for pushing: {remoteName}')
  except (...):
     pass
  if remoteName is None:
    printError('cannot find remote name corresponding to atlas-physics/pmg/mcjoboptions.git')
    sys.exit(7)

  # Update all the local branches
  if args.GIT:
    bashExec(f'git fetch {remoteName} --prune')

  # Get the name of the current branch
  currentBranch = bashExec('git rev-parse --abbrev-ref HEAD')
  if args.GIT and currentBranch != "master":
    msg = f'running from branch: {currentBranch}. '\
          f'You must switch to the master branch before executing the script'
    printError(msg)
    sys.exit(8)

  # Make sure that there are no staged commits
  stagedCommits = bashExec('git diff --name-status --cached')
  if stagedCommits != '':
    printError('you are on the master branch and have the following staged commits:')
    print(stagedCommits)
    printError('You need to unstage these commits before running the script', notag = True)
    sys.exit(9)

  if args.BRANCH == '':
    # Name of new branch
    import getpass
    USER = getpass.getuser()
    args.BRANCH = f'dsid_auto_{USER}_{newDSID[0]}' if args.AUTO else f'dsid_{USER}_{newDSID[0]}'
  # Remove dots and slashes from args.BRANCH
  args.BRANCH = args.BRANCH.replace('.', '').replace('/', '') 
  printInfo(f'Will use branch: {args.BRANCH}...')
  # Check if branch already exists
  bashExec('git fetch --all')
  branchExists = args.BRANCH in bashExec('git branch')

  # If branch exists check it out
  if branchExists:
    if args.GIT:
      printInfo(f'Branch \"{args.BRANCH}\" exists. Checking it out...')
      try:
        bashExec(f'git checkout {args.BRANCH}')
      except (...):
        sys.exit(11)
    else:
      printInfo(f'Branch \"{args.BRANCH}\" exists. Will just check it out...')
  else: 
    # Otherwise create new one
    if args.GIT:
      printInfo(f'Creating new branch \"{args.BRANCH}\"')
      # Checkout a new branch starting from remoteName/master
      temp, rc = bashExec(f'git checkout -b {args.BRANCH} {remoteName}/master', checkStatus = True)
      if rc:
        sys.exit(12)
    else:
      printInfo(f'Will create new branch \"{args.BRANCH}\"')

  # Check jO naming and DSID ranges
  printInfo('Checking jO consistency and DSID ranges ...')
  jOs = [ ]
  for dsid in newDSID:
    dsidDIR = str(dsid)
    if args.DSID != '':
      #dsidDIR = d2p(dsidDIR)
      if len(dsid) == 6:
        dsidDIR = f'{dsid[:3]}xxx/{dsid}'
      else:
        dsidDIR = f'{dsid[:1]}/{dsid[:4]}xxx/{dsid}'

    # Check if the directory conntains any files
    jO = glob.glob(f'{dsidDIR}/mc.*.py')
    if not jO:
      printError(f'Directory {dsidDIR} does not contain any jO files')
      sys.exit(13)
    jOs += jO

  jO_str = ' '.join(jOs)
  consistency_cmd = f'python3 scripts/check_jo_consistency_main.py {jO_str}'
  jOConsistencyChecks, rc = bashExec(consistency_cmd, checkStatus = True)

  # Check jO consistency and DSID assignment
  DSIDmap = { }
  if rc != 0:
    DSIDtoMove = re.compile(r'Move (.*)').findall(jOConsistencyChecks)
    if len(DSIDtoMove):
      # jOs are in the wrong DSID range and have to be moved
      DSIDmap = bookDSIDs(DSIDtoMove, args.DRY_RUN)
    else:
      # Serious error in jO consistency
      printError('\tERROR in jO consistency checks', notag = True)
      print(jOConsistencyChecks)
      sys.exit(14)
  else:
    # DSID is correct and no jO concistency error found
    printOK(f'\t{jO_str} in correct DSID range', notag = True)

  # Loop over new DSID directories to be added
  remappedSeq = [ ]
  for dsid in newDSID:
    # Directory should be located in
    dsiddir = str(dsid) # if it's a custom name
    if re.match(r'^[0-9]{6}$', dsiddir): # 6-digit case
      dsiddir = f'{dsiddir[:3]}xxx/{dsiddir}'
    elif re.match(r'^[0-9]{7}$', dsiddir): # 7-digit case
      dsiddir = f'{dsiddir[0]}/{dsiddir[:4]}xxx/{dsiddir}'
    if dsiddir in DSIDmap:
      remappedSeq.append(DSIDmap[dsiddir].split('/')[-1])
      dsiddir = DSIDmap[dsiddir]
    else:
      remappedSeq.append(dsid)

    if os.path.isdir(dsiddir):
      printInfo(f'New DSID directory: {dsiddir} ...')
    else:
      printError(f'Directory {dsiddir} does not exist')
      sys.exit(15)

    # Check if they contain a log.generate or log.afterburn file
    logType = None
    isLogG = os.path.isfile(f'{dsiddir}/log.generate')
    isLogA = os.path.isfile(f'{dsiddir}/log.afterburn')
    if isLogG and isLogA:
      msg = f'\tERROR: {dsiddir} contains both a log.generate '\
            f'and a log.afterburn file. You must only keep one.'
      printError(msg, notag = True)
      sys.exit(16)
    elif isLogG:
      logType = "log.generate"
      printOK('\tOK: log.generate file found.', notag = True)
    elif isLogA:
      logType = "log.afterburn"
      printOK('\tOK: log.afterburn file found.', notag = True)
    else:
      printWarning(f'\tWARNING: {dsiddir} does not contain log.generate or log.afterburn', notag = True)
      printWarning(f'\tYou should run athena locally and add the log.generate or log.afterburn file in {dsiddir}', notag = True)
      printWarning(f'\tThe athena and logParser jobs will not run in the CI', notag = True)

    # If athena was run locally and the log file is found in the directory to commit
    # perform some checks to make sure that the new DSIDs conform to the rules
    if logType:
      # Run the logParser 
      logOpt = ' -u' if args.CPU_OPT else ''
      joFile = glob.glob(f'{dsiddir}/mc.*.py')[0]
      logparser_cmd = f'python3 scripts/logParser.py {logOpt} -i {dsiddir}/{logType} -j {joFile}'
      LPO, rc = bashExec(logparser_cmd, checkStatus = True)
      if rc:
        print(LPO)
        printError('\tERROR: logParser run failed.', notag = True)
        sys.exit(17)
       
      # Check if local log file passes the logParser checks
      Nerror = int(re.compile(r'Errors.*Warnings').findall(LPO)[0].split()[2])
      if Nerror:
        print(LPO)
        printError('\tERROR: log file contains errors', notag = True)
        printError('\tFix them before committing anything!', notag = True)
        sys.exit(18)
      else:
        printOK('\tOK: log file contains no errors', notag = True)

      # The following parameters are needed for running athena in the CI
      tags = [ 
        'estimated CPU for CI job',
        'using release',
        'ecmEnergy',
        'inputGeneratorFile',
        'inputEVNT_PreFile',
        'randomSeed',
        'EVNT to EVNT',
        'LHEonly',
        'ATHENA_PROC_NUMBER',
        'platform'
      ]
      # Remove colours from logParserOutput and extract necessary information
      LPO_noCols = re.sub(r'\x1b\[[0-9]{1,3}[mK]', '', LPO)
      greps = sum((re.compile(f'.*{tag}.*').findall(LPO_noCols)[:1] for tag in tags), [])
      # Pipe information into log.generate.short
      with open(f'{dsiddir}/log.generate.short', 'w') as logShort:
        logShort.write('\n'.join(greps))
      
      # Check if job lasts more than 1h - this will cause the pipeline to fail
      if not args.CPU_OPT:
        cpu = float(greps[0].split()[7])
        if cpu > 1.0:
          if skipAthena: 
            printOK(f'\tOK: CI job time estimate: {cpu} hours, but athena will not run in the CI', notag = True)
          else:
            msg = f'\tERROR: CI job is expected to last more than 1h - time estimate: {cpu} hours\n' \
                  f'\tIt is not possible to run this job in the CI. Contact the MC software coordinators'
            printError(msg, notag = True)
            sys.exit(19)
        else:
          printOK(f'\tOK: CI job expected to last less than 1h - time estimate: {cpu} hours', notag = True)

    # Loop over files in new DSID directory and add them to the commit
    printInfo('\tWill now add files to git commit', notag = True)
    for newfile in glob.glob(f'{dsiddir}/**/*', recursive = True):
      if os.path.isdir(newfile):  continue
      # Check if the file is in the whitelist and add it in the commit
      if checkWhitelist(newfile,args.DRY_RUN):
        # Special case: [skip athena] or [skip logparser] have been specified in the commit message
        # Should not commit log.generate.short
        if skipAthena and 'log.generate.short' in newfile:
           print(f'\t\tSkipping: {newfile} since the logParser CI job will not be run')
           continue
        else:
          if args.GIT:
            # Sparse checkout
            if os.path.isfile(".git/config.worktree"):
              bashExec(f'git add --sparse {newfile}')
            # Default
            else:
              bashExec(f'git add {newfile}')
            print(f'\t\tAdded: {newfile}')
          else:
            print(f'\t\tWill add: {newfile}')
      else:
        print(f'\t\tFile: {newfile} cannot be added to the commit. Skipping.')

  if len(DSIDmap) and not args.GIT:

    new_range = makeRanges(remappedSeq)
    printOK('\nThe following DSIDs have been assigned:', notag = True)
    for old_dsiddir in sorted(DSIDmap.keys()):
      old_id = old_dsiddir.split('/')[-1]
      new_id = DSIDmap[old_dsiddir].split('/')[-1]
      print(f'\t{old_id} -> {new_id}')
    msg = f'Run: ./scripts/commit_new_dsid -d {new_range} '\
          f'-m \"{args.COMMIT_MESSAGE}\" to push them to git\n'
    printOK(msg, notag = True)

  # Push the changes
  if args.GIT:
    gitCache = bashExec('git diff --cached')
    if gitCache:
      bashExec(f'git commit -q -m "{args.COMMIT_MESSAGE}"')
      branchName = bashExec('git rev-parse --abbrev-ref HEAD')
      printInfo(f'\nYou have prepared branch {branchName}.', notag = True)
      printInfo('This contains the following changes:')
      print(bashExec('git diff --stat master HEAD'))
      msg = f'\nIf this looks good, you should now run '\
            f'\'git push -u {remoteName} {branchName}\' to upload.'
      printInfo(msg, notag = True)
    else:
      printError('nothing added to the commit. Please check error messages above.')

  sys.exit(0)

