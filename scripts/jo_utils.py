# NB: This needs to be useable both in python2 and python3
# since it gets called from several releases

from __future__ import print_function
import argparse, os, re, subprocess, shlex

# canonical path structures are 100xxx/100000 or 1/1000xxx/1000000
ALLOWED_PATHS = r'([0-9]{3}xxx/[0-9]{6}|[0-9]{1}/[0-9]{4}xxx/[0-9]{7})'

# allowed DSID blocks, encodes as regular expressions
# leading '^' is implied here!
OPEN_BLOCKS = { 
  #"ART"  : [r'421\d{3}', ],
  "MG"   : [r'5\d{5}', ],
  "aMC"  : [r'5\d{5}', ],
  "Ph"   : [r'6\d{5}', ],
  "Sh"   : [r'7\d{5}', ],
  "Py8"  : [r'8[0-2]\d{4}', ],
  "P8B"  : [r'8[0-2]\d{4}', ],
  "H7"   : [r'8[3-5]\d{4}', ],
  "HI"   : [r'8[6-9]\d{4}', ],
  "Misc" : [r'9[0-4]\d{4}', ],
  "Val"  : [r'9[5-9]\d{4}', r'10\d{5}'],
}

def git_dsid_pattern(allowed_blocks):
  """Constructs a regex to pick out the allowed DSID
     from a string containing patterns like
     'xxx/123456/' or 'xxx/1234567/'."""
  return r'xxx/(' + '|'.join(allowed_blocks) + r')/'

def get_dsid_range(pat):
  """Constructs a string a la first - last integer for a
     given regex, which will end in '\d{n}' with integer n."""
  minDSID = int(pat[-2])*'0'
  maxDSID = int(pat[-2])*'9'
  while pat.find(r'[') >= 0:
    minDSID = pat[pat.find('[')+1] + minDSID
    maxDSID = pat[pat.find(']')-1] + maxDSID
    pat = re.sub('\[.*?\]', '', pat, count = 1)
  minDSID = pat[:pat.find(r'\d')] + minDSID
  maxDSID = pat[:pat.find(r'\d')] + maxDSID
  return '{}-{}'.format(minDSID, maxDSID)

def get_dsid_minmax(pat):
  minDSID, maxDSID = get_dsid_range(pat).split('-')
  return int(minDSID), int(maxDSID)


def bashExec(cmd, debug = False, checkStatus = False):
  if debug:
    print(cmd)
    return ''
  p = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE)
  if checkStatus:
    return p.stdout.decode('utf-8').strip(), p.returncode 
  return p.stdout.decode('utf-8').strip()


def _parse(dsids):
    """A function to determine consecutive DSIDs and work out ranges.
    Takes as input an array of string-DSIDs.
    Prints out a string of comma-separated individual DSIDs / DSID ranges,
    e.g. 123456,600000,600001,600002,987654 -> 123456,600000-600002,987654"""
    from itertools import groupby
    from operator import itemgetter
    dsids = [ int(d) for d in dsids ] # turn strings to integers
    groups = [ list(map(itemgetter(1), l)) for k, l in groupby(enumerate(dsids), lambda item: item[0]-item[1]) ]
    # "groups" is an array of arrays. The inner arrays contain at least one DSID. 
    # If there's more than one DSID in an array, they must be consecutive, i.e. form a range.
    print( ','.join([ '%i-%i' % (g[0], g[-1]) if len(g) > 1 else str(g[0]) for g in groups ]) )


def checkWhitelist(path, dryRun = False):
  """ Function to check if a file is in the whitelist 
  of files that are allowed to be added in a commit"""
  # Rules for DSID DIR
  DIR = r'.*' if dryRun else ALLOWED_PATHS
  if os.path.isfile(path) or os.path.islink(path):
    # Regular file checks
    re_checks = [
      r'.*\.py', r'.*\.f',
      r'pdgid_extras.txt',
      # pdt or dec files to allow changes in decay tables
      r'.*\.pdt', r'.*\.dec', r'.*\.DEC',
      r'powheg.input',
      r'log\.generate\.short',
      r'MadGraphControl/.*\.py',
      r'PowhegControl/.*\.py',
      r'Herwig7_i/.*\.py',
      r'Pythia8_i/.*\.py',
      r'Sherpa_i/.*\.py',
    ]
    isWhitelisted = any([ re.match('{}/{}'.format(DIR, tag), path) for tag in re_checks ])
    # dat files have to live in the top-level jO directory
    isTopLevelDAT = re.match('{}/.*\.dat'.format(DIR), path) and not re.match('{}/.*/.*\.dat'.format(DIR), path)
    # only symlinks allowed for gridpacks
    isGridpack = os.path.islink(path) and re.match('{}/mc_.*TeV\..*\.GRID\.tar\.gz'.format(DIR), path)
    return isWhitelisted or isTopLevelDAT or isGridpack
  # ... else 
  print('Unknown file/filetype: {}'.format(path))
  return False


# A function to read a parameter from a jO file
# Taken from https://github.com/retmas-dv/deftcore/blob/master/taskengine/taskdef.py#L289
# ignore_case is set to False by default unlike ProdSys
def _read_param_from_jo(jo, names, ignore_case=False):
    value = None
    if ignore_case:
        names = [name.lower() for name in names]
        jo = jo.lower()
    if any(p in jo for p in names):
        for line in jo.splitlines():
            if any(p in line for p in names):
                try:
                    if '#' in line:
                        line = line[:line.find('#')]
                    # The int conversion will throw an exception if nEventsPerJob is not a number
                    value = int(line.replace(' ', '').split('=')[-1])
                    break
                except Exception as ex:
                    pass
    return value


# A function to read a parameter from a jO file using a dictionary
# The difference with the previous one is that it follows
# See https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/issues/98
def _read_param_from_jo_withDict(jo, type, param):
    locals = {type: argparse.Namespace()}
    for line in jo.splitlines():
        if "os.system" in line or "subprocess.call" in line or "subprocess.Popen" in line: continue # for security
        if line.strip().startswith('print'): continue
        try:
            exec(line.strip(), {}, locals)
        except:
            pass
    return getattr(locals[type], param) if hasattr(locals[type], param) else None


# A function to check if environment variables are set in jO file
def _env_set(jo, param):
    # Get original environment variables
    orig_env = dict(os.environ)
    # Clear environment variables
    os.environ.clear()
    # dictionary to store environment variables
    locals = dict()
    # Execute jO
    for line in jo.splitlines():
        if "os.system" in line or "subprocess.call" in line or "subprocess.Popen" in line: continue # for security
        if line.strip().startswith('print'): continue
        try:
            exec(line.strip(), globals(), locals)
        except:
            pass
        # Get value of environment variable - if it has been set break
        if os.environ.get(param) is not None:
             break
    # Restore environment
    os.environ.update(orig_env)
    if os.environ.get(param) is not None:
        return [True, os.environ.get(param)]
    else:
        return [False, None] 


# A function to check if jO imports a certain module
def _check_import(jo, listOfModules):
    patternList=[]
    for module in listOfModules:
        patternList.append(re.compile('from.*import.*{}.*'.format(module)))
        patternList.append(re.compile('import.*{}.*'.format(module)))
    # Combine patterns
    for line in jo.splitlines():
        if not line.startswith("#"):
            if any(pattern.match(line) for pattern in patternList):
                return True
    return False


### Steering ###
if __name__ == "__main__":
    import argparse
    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("DSIDs", nargs="*")
    parser.add_argument("--jO", dest="jOFile", default=None)
    parser.add_argument("--parameter", dest="parameter", action="store", default=None)
    parser.add_argument("--dsid-parser", dest="dsid_parser", action="store_true", default=False)
    args = parser.parse_args()
    
    if args.dsid_parser and len(args.DSIDs) > 0:
        # Check if list of DSIDs have been supplied
        _parse(args.DSIDs)
    elif args.jOFile is None:
        # Check if jO file has been defined
        raise Exception("ERROR: --jO argument not specified")
    else:
        # Read jO contents
        with open(args.jOFile, 'r') as jofile:
            job_options_file_content = jofile.read()
    
    # Extract different parameters
    if args.parameter == "nEventsPerJob":
        nEventsPerJob=_read_param_from_jo(job_options_file_content, ['evgenConfig.nEventsPerJob'])
        if not nEventsPerJob:
            nEventsPerJob=10000
        print(nEventsPerJob)
    elif args.parameter == "minevents":
        print(_read_param_from_jo(job_options_file_content, ['evgenConfig.minevents']))

def printError(txt, notag = False):
  tag = 'ERROR: '
  print("\033[1;31m{}{}\033[0m".format(' '*len(tag) if notag else tag, txt))

def printWarning(txt, notag = False):
  tag = 'WARNING: '
  print("\033[1;33m{}{}\033[0m".format(' '*len(tag) if notag else tag, txt))

def printOK(txt, notag = False):
  tag = 'OK: '
  print("\033[1;32m{}{}\033[0m".format(' '*len(tag) if notag else tag, txt))

def printInfo(txt, notag = False):
  tag = 'INFO: '
  print("\033[1;34m{}{}\033[0m".format(' '*len(tag) if notag else tag, txt))


