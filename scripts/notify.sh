#!/usr/bin/env bash

# For colour
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

# Function to check existing EVNT files
checkEVNT() {
  arr=("$@")

  # Setup ATLAS
  source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh >&/dev/null
  
  # Set rucio account
  export RUCIO_ACCOUNT=cgutscho
  # Set up voms proxy
  lsetup "rucio -w" >&/dev/null
  echo $GRID_PW | voms-proxy-init --voms atlas --pwstdin --cert /root/.globus/usercert.pem --key /root/.globus/userkey.pem
  
  # List of possible scopes that are being searched for
  scopes=($(rucio list-scopes | grep -E 'mc.*_.*TeV' | grep -vE '(mc09|mc10|mc11|mc12|mc14)'))
  
  # Types of files to check
  contTypes=("evgen.EVNT" "TXT")
  
  for i in "${arr[@]}"; do
    DSID=$(echo $i | awk -F'/' '{print $2}')
    printInfo "Check: $DSID"
    for contType in "${contTypes[@]}"; do
      for scope in "${scopes[@]}"; do
        did=($(rucio list-dids --filter 'type=CONTAINER' "$scope.$DSID.*.$contType.*/" | grep CONTAINER | awk '{print $2}'))
        if [[ ${#did[@]} -gt 1 ]] ; then 
          printWarning "WARNING: $DSID HAS MULTIPLE CONTAINERS PRODUCED!"
          for i in "${did[@]}" ; do 
            declare -i nfiles
            nfiles=$(rucio list-files $i | grep 'Total files' | awk '{print $NF}')
            if (( nfiles != 0 )) ; then
              printError "DSID: $i has $nfiles $contType files already produced !"
            else 
              printGood "OK: no $contType files for $i found in rucio but double-check obsoletion!"
            fi
          done
        elif [[ ${#did[@]} -eq 1 ]] ; then 
          declare -i nfiles
          nfiles=$(rucio list-files ${did[@]} | grep 'Total files' | awk '{print $NF}')
          if (( nfiles != 0 )) ; then
            printError "DSID: $did has $nfiles $contType files already produced !"
            return 2

          else
            printGood "OK: $did has $nfiles $contType files already produced !"
          fi
        else
          printGood "OK: no $contType files for $scope.$DSID found in rucio"
        fi
      done
    done
  done
  
  return 1
}

# Function to reassign MR to conveners
reassignMR() {
  export PATH=$PWD/scripts:$PATH
  
  printInfo "Reassigning MR: ${CI_MERGE_REQUEST_IID}"
  
  # Reassign the open MR to the PMG conveners
  BODY="{
      \"id\": ${PROJECT_ID},
      \"merge_request_iid\": \"${CI_MERGE_REQUEST_IID}\",
      \"body\": \"This MR has modified existing DSID directories. PMG conveners need to approve it\r\n\r\n/reassign @atlas-phys-pmg-conveners\"
  }";
  curl --silent --request POST \
  --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "${BODY}" \
  "${PROJECT_URL}/${PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions";
}

# Function to cancel automatic merging
cancelAutoMerge() {
  export PATH=$PWD/scripts:$PATH
  
  printInfo -f "\nCanceling automatic merge MR: ${CI_MERGE_REQUEST_IID}"

  # Cancel automatic merge
  curl --silent --request POST \
       --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
       "${PROJECT_URL}/${PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/cancel_merge_when_pipeline_succeeds";
}

##############
# Project info
##############
PROJECT_ID=62692
PROJECT_URL=https://gitlab.cern.ch/api/v4/projects

# Run check_modified_files.sh script to check if there were modified files
modFiles=($(./scripts/check_modified_files.sh | grep xxx | awk '{print $2}'))

# If there are modified files send an email to conveners otherwise job is successful
# (the user should not have needed to specify [skip modfiles] in their commit)
if (( ${#modFiles[@]} > 0 )) ; then

  # Print warning
  printWarning "WARNING: [skip modfiles] used and files modified. MR will be reassigned to PMG conveners."
  
  # Send email to conveners (keep this commented in case we want to put it back since it wasn't easy to find how to send the email)
  #echo -e "Subject: WARNING: mcjoboptions job with modified files \n\n Job: $CI_JOB_URL \n\n MR: $CI_OPEN_MERGE_REQUESTS" > mail.txt
  #curl --user "$SERVICE_ACCOUNT":"$K8S_SECRET_SERVICE_PASSWORD" --ssl --url 'smtp://smtp.cern.ch:587' --mail-from $SERVICE_ACCOUNT"@cern.ch" --mail-rcpt 'atlas-phys-pmg-conveners@cern.ch' --upload-file mail.txt
      
  # Reassign MR to conveners
  reassignMR
  
  # Cancel auto merge
  cancelAutoMerge
  
  # Print which DSIDs have existing EVNTs
  checkEVNT "${modFiles[@]}"
  failureCode=$?

  return $failureCode
# No files modified
else
  printGood "OK: [skip modfiles] used but no file modified"
fi

return 0