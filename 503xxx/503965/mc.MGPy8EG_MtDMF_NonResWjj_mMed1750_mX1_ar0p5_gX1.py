#import MadGraphControl.MadGraph_NNPDF23nnloas0119qed_Base_Fragment
import MadGraphControl.MadGraphUtils

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':263000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[263000], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *
name_jobfile = jofiles
print "%%%%%%%%%%%%%%%%%%%%%%%%%  Jobfile   %%%%%%%%%%%%%%%%%%%%%%%%"
print name_jobfile
config_names = jofile.split('.')[1].split('_')
print "%%%%%%%%%%%%%%%%%%%%%%%% Config Names %%%%%%%%%%%%%%%%%%%%%%%"
print config_names
mMed = int(config_names[3][4:])
print "%%%%%%%%%%%%%%%%%%%%%%%%% Mediator Mass %%%%%%%%%%%%%%%%%%%%%"
print mMed
mDM = int(config_names[4][2:])
print "%%%%%%%%%%%%%%%%%%%%%%%%  Chi Mass %%%%%%%%%%%%%%%%%%%%%%%%%%"
print mDM
coupling_ar = float(config_names[5][2:].replace ('p','.'))
print "%%%%%%%%%%%%%%%%%%%%%%%%  Coupling a_r %%%%%%%%%%%%%%%%%%%%%%%%%%"
print coupling_ar
coupling_gg = float(config_names[6][2:].replace ('p','.'))
print "%%%%%%%%%%%%%%%%%%%%%%%%  Coupling gX %%%%%%%%%%%%%%%%%%%%%%%%%%"
print coupling_gg

#nevents = runArgs.maxEvents*1.0 if runArgs.maxEvents>0 else 1.2*evgenConfig.nEventsPerJob
nevents = int (runArgs.maxEvents*1.2) if runArgs.maxEvents>0 else int (1.2*evgenConfig.nEventsPerJob)
#nevents = 30000
ebeam1 = float(runArgs.ecmEnergy/2)
ebeam2 = float(runArgs.ecmEnergy/2)

process="""
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/MonotopDMF_UFO -modelname
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t psi psibar, (t > b w+, w+ > j j)
add process p p > t~ psi psibar, (t~ > b~ w-, w- > j j)
output -f"""

process_dir = new_process(process)
#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version'   :'3.0',
             'cut_decays'    :'F',
             'asrwgtflavor'  :'5',
             'ebeam1'        :ebeam1,
             'ebeam2'        :ebeam2,
             'iseed'         :runArgs.randomSeed,
             'nevents'       :int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#Parameter card

masses={'32': mMed, #V mass
        '1000023': mDM}  #psi mass

couplings={'1': coupling_ar,
           '10': coupling_gg }

decays={'32': 'DECAY 32 Auto', #V width
        '1000023': 'DECAY 1000023 Auto'}  #psi width

params={}
params['MASS']=masses
params['COUPX']=couplings
params['DECAY']=decays

modify_param_card(param_card_input=None,process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)

# hacking LHE file
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/run_01/unweighted_events.lhe.gz'])
unzip1.wait()
oldlhe = open(process_dir+'/Events/run_01/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/run_01/unweighted_events2.lhe','w')

#init = True

for line in oldlhe:
#    if init==True:
        if '-1000023' in line:
            line = line.replace('-1000023','1000022')
        elif '-1000023' in line:
            line = line.replace('1000023','1000022')
        newlhe.write(line)
    
oldlhe.close()
newlhe.close()

# re-zipping hacked LHE
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/run_01/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/run_01/unweighted_events2.lhe.gz',process_dir+'/Events/run_01/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/run_01/unweighted_events.lhe')


arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  



#### Shower 
evgenConfig.description = 'MadGraph_nonresonant_monotop'
evgenConfig.contact  = ["paolo.sabatini@cern.ch"]


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


