from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
model_string = phys_short.split('_')[3]
mphi = phys_short.split('_')[4].replace("p","")
mchi = phys_short.split('_')[5].replace("c","")
if "p" in str(mchi):
    mchi = str(mchi).replace("p",".")
mphi = float(mphi)
mchi = float(mchi)

evgenLog.info('Processing model with masses: (mphi, mchi) = (%e,%e)' %(mphi, mchi))

if "ttscalar" in phys_short.lower() or "ttpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0 --modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > xd xd~ t t~ / a z w+ w- 
add process p p > xd xd~ t t~ j / a z w+ w- 
output -f
"""
    
    ktdurham=40
    if mphi/4 > 40:
        ktdurham = mphi/4
    evgenLog.info('ktdurham set to %i' %ktdurham)    
    process="pp>{phi,54}tt~"
    evgenConfig.process = "pp>ttxdxd"
    nJetMax = 1

elif "twscalar" in phys_short.lower() or "twpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0_5f_ybMSbar --modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > xd xd~ t w- QCD=999 QED=999
add process p p > xd xd~ t~ w+ QCD=999 QED=999
output -f
"""

    ktdurham=0
    evgenConfig.process = "pp>tWxdxd"
    nJetMax = 0

elif "tjscalar" in phys_short.lower() or "tjpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0_5f_ybMSbar --modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > t~ xd xd~ j $$ w+ w- QCD=999 QED=999
add process p p > t  xd~ xd j $$ w+ w- QCD=999 QED=999
output -f
"""

    ktdurham=0

    evgenConfig.process = "pp>tjxdxd"
    nJetMax = 0

if "1LMET60orMET150" in phys_short:
    evgenLog.info('1lepton and MET 60 filter or MET 150 is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 60000.

    filtSeq += MissingEtFilter("MissingEtFilterHard")
    filtSeq.MissingEtFilterHard.METCut = 150000.

    filtSeq.Expression = "(LeptonFilter and MissingEtFilter) or MissingEtFilterHard"

#elif "1LMET150orMET190" in phys_short:
elif "1LorMET" in phys_short:
    evgenLog.info('1lepton and MET 150 filter or MET 190 is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 150000.

    filtSeq += MissingEtFilter("MissingEtFilterHard")
    filtSeq.MissingEtFilterHard.METCut = 190000.

    filtSeq.Expression = "(LeptonFilter and MissingEtFilter) or MissingEtFilterHard"

elif "lowMET" in phys_short:
    evgenLog.info('not 1lepton and MET 150 to 190 filter or MET 100 to 150 is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 150000.

    filtSeq += MissingEtFilter("MissingEtFilterLoose")
    filtSeq.MissingEtFilterLoose.METCut = 100000.

    filtSeq += MissingEtFilter("MissingEtFilterHard")
    filtSeq.MissingEtFilterHard.METCut = 190000.

    filtSeq.Expression = "((not LeptonFilter) and MissingEtFilter and (not MissingEtFilterHard)) or (MissingEtFilterLoose and (not MissingEtFilter))"

elif "1LandLowMET" in phys_short:
    evgenLog.info('1lepton and MET below 100 is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 100000.

    filtSeq.Expression = "LeptonFilter and (not MissingEtFilter)"

elif "2L" in phys_short:
    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.MinPt  = 18000.
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.MultiElecMuTauFilter.NLeptons = 2
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0

    filtSeq.Expression = "MultiElecMuTauFilter"


if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=evgenConfig.nEventsPerJob*evt_multiplier

run_settings = {'lhe_version':'3.0',
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 260000,
          'ickkw'      : '0',
          'ktdurham'   : ktdurham,
          'maxjetflavor':5, # 5 flavor scheme
          'asrwgtflavor':5, # 5 flavor scheme
          'xptb':0,
          'ptj':20.,
          'drjj':0.,
          'xqcut':0.
          }

# Note: For most processes, the generation speed can be improcved by setting ptj and mjj to xqcut, which is done automatically if the flag auto_ptj_mjj is set to T
# https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Matching
run_settings['auto_ptj_mjj'] = 'F'

# Specify PDG merging cuts for MG 2.6.2
run_settings['pdgs_for_merging_cut']='1, 2, 3, 4, 5, 21'

# Turn off internal systematic weights (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure#AGENE_1542_Inconsistent_Weights)
# (otherwise crashes in 19.2.5.35)
#run_settings['event_norm']='sum'
run_settings['use_syst']='F'
run_settings['nevents'] = nevents


# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# Set up the MadSpin card (if needed)
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'                                                                                                                                   
mscard = open(madspin_card_loc,'w')  

if "2L" in phys_short:
    mscard.write("""
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    set seed %i                                                                                                                                                               
    # specify the decay for the final state particles                                                                                                                         
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    decay t > w+ b, w+ > l+ vl
    decay t~ > w- b~, w- > l- vl~ 
    decay w+ > l+ vl 
    decay w- > l- vl~ 
    # running the actual code                                                              
    launch"""%runArgs.randomSeed)      
else:
    mscard.write("""
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    set seed %i                                                                                                                                                               
    # specify the decay for the final state particles                                                                                                                         
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    decay t > w+ b
    decay t~ > w- b~
    decay w+ > all all
    decay w- > all all
    # running the actual code                                                                                                                                                 
    launch"""%runArgs.randomSeed)   

mscard.close()

# this is a dictionary
params = {}
params["dminputs"] = {}
params["dminputs"]["gSXd"] = 0.0 
params["dminputs"]["gSd11"] = 0.0
params["dminputs"]["gSu11"] = 0.0
params["dminputs"]["gSd22"] = 0.0
params["dminputs"]["gSu22"] = 0.0
params["dminputs"]["gSd33"] = 0.0 
params["dminputs"]["gSu33"] = 0.0 
params["dminputs"]["gPXd"] = 0.0 
params["dminputs"]["gPd11"] = 0.0
params["dminputs"]["gPu11"] = 0.0
params["dminputs"]["gPd22"] = 0.0
params["dminputs"]["gPu22"] = 0.0
params["dminputs"]["gPd33"] = 0.0 
params["dminputs"]["gPu33"] = 0.0 
params["dminputs"]["gSh1"] = 0.0 
params["dminputs"]["gSb"] = 0.0 

params['MASS']={'1000022':mchi,'54':mphi}
params['DECAY']={'54':'DECAY  54 Auto # WY0'}

if "scalar" in phys_short.lower():
    params["dminputs"]["gSXd"] = 1.0 
    params["dminputs"]["gSd33"] = 1.0 
    params["dminputs"]["gSu33"] = 1.0 
elif "pseudo" in phys_short.lower():    
    params["dminputs"]["gPXd"] = 1.0 
    params["dminputs"]["gPd33"] = 1.0 
    params["dminputs"]["gPu33"] = 1.0 




modify_param_card(process_dir=process_dir,params=params)

# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Set the evgen metadata
evgenConfig.description = 'DM+%s with matching, m_med = %s GeV, m_chi = %s GeV'%(model_string, mphi, mchi)
evgenConfig.keywords = ["exotic","BSM","WIMP", "SUSY"]
evgenConfig.contact = ["Claudia Seitz <claudia.seitz@cern.ch>, Priscilla Pani <ppani@cern.ch>, Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "1000022:all = Chi Chi~ 1 0 0 %s " %mchi,
                           ]

if nJetMax>0:
    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
                                "Merging:doKTMerging = on",
                                "Merging:ktType = 1",
                                "Merging:nJetMax = %i"%nJetMax,
                                "Merging:Process = %s"%process,
                                "Merging:TMS = %f"%run_settings['ktdurham'],
                                "Merging:nQuarksMerge = %s" %run_settings['maxjetflavor'],
                                "Merging:Dparameter = 0.4",
                                ]



