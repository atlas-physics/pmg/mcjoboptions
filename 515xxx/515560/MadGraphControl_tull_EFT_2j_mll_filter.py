import re
import os
import math
import subprocess

from MadGraphControl.MadGraphUtils import *

nevents=runArgs.maxEvents
LHE_EventMultiplier = 1.1
if LHE_EventMultiplier > 0 :
    nevents=runArgs.maxEvents*LHE_EventMultiplier



if el_or_mu == 1 and u_or_c == 1 :
    my_process = """
    import model 4fermitop_up_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- t NP^2==4
    add process p p > e+ e- t~ NP^2==4
    add process p p > e+ e- t j NP^2==4
    add process p p > e+ e- t~ j NP^2==4
    output -f"""
elif el_or_mu == 2 and u_or_c == 1 :
    my_process = """
    import model 4fermitop_up_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- t NP^2==4
    add process p p > mu+ mu- t~ NP^2==4
    add process p p > mu+ mu- t j NP^2==4
    add process p p > mu+ mu- t~ j NP^2==4
    output -f"""
elif el_or_mu == 1 and u_or_c == 2 :
    my_process = """
    import model 4fermitop_charm_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- t NP^2==4
    add process p p > e+ e- t~ NP^2==4
    add process p p > e+ e- t j NP^2==4
    add process p p > e+ e- t~ j NP^2==4
    output -f"""
elif el_or_mu == 2 and u_or_c == 2 :
    my_process = """
    import model 4fermitop_charm_5F --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- t NP^2==4
    add process p p > mu+ mu- t~ NP^2==4
    add process p p > mu+ mu- t j NP^2==4
    add process p p > mu+ mu- t~ j NP^2==4
    output -f"""

  

mmll = 400.0

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(my_process)
extras = {'pdlabel': "'lhapdf'",
          'lhaid'      : '263000',
          'ickkw'      : '1',
          'ptj'   : '20.0',
          'ptl'   : '10.0',
          'mmll'   : mmll,
          'xqcut'   : '30.0',
          'nevents'   : nevents,
          'maxjetflavor': '5'}

couplings = {'lambdas': str(Lambda),
          'fsrr'      : str(fsrr),
          'ftrr'      : str(ftrr),
          'fvll'   : str(fvll),
          'fvlr'   : str(fvlr),
          'fvrl'   : str(fvrl),
          'fvrr'   : str(fvrr)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params={}
params['DIM6']=couplings
modify_param_card(process_dir=process_dir,params=params)



print_cards()


generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3, saveProcDir=True)



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



evgenConfig.description = 'Production of tull EFT'
evgenConfig.keywords += ['BSM', 'exotic']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> tjll'
evgenConfig.tune = 'A14 NNPDF23LO'
evgenConfig.contact = ["Yoav Afik <yafik@cern.ch>"]


