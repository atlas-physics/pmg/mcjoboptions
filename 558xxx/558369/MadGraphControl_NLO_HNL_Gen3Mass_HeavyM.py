import os
import sys
from AthenaCommon.Include import include
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment


# Reading physics_short and setting final state
phys_short = get_physics_short()
model_string = phys_short.split('_')[2]
mhnl_input = phys_short.split('_')[2].replace("HNL","")
mhnl_str= ''
for entry in mhnl_input:
    if entry =='p':
        mhnl_str=mhnl_str+'.'
    else:
        mhnl_str =mhnl_str+entry
mhnl = float(mhnl_str)
channel = phys_short.split('_')[4]

lep = [None]*3
digit = 0

for i in range(3):
    if channel[digit:digit+2]=='mu':
        lep[i] = 'mu'
        digit+=2
    elif channel[digit:digit+2]=='ta':
        lep[i] = 'ta'
        digit+=2
    elif channel[digit:digit+1]=='e':
        lep[i] = 'e'
        digit+=1
    elif channel[digit:digit+1]=='j':
        lep[i] = 'j'
        digit+=1
    else:
        evgenLog.error("lepton %i does not have correct type, "%i)

if len(channel[digit:])!=0:
    evgenLog.error("channel name too long, check")


# Settings for HNL lifetime
ctaustring = phys_short.split('_')[3].replace("ctau","") # ctau is the lifetime of the HNL
ctau_str= ''
for entry in ctaustring:
    if entry =='p':
        ctau_str=ctau_str+'.'
    else:
        ctau_str =ctau_str+entry
ctau = float(ctau_str)
if ctau > 0:
    tofoption = 0
else:
    tofoption= -1
evgenLog.info('physics short: %s' %phys_short)
evgenLog.info('Processing model with HNL mass and average lifetime: (mhnl, ctau) = (%e,%e)' %(mhnl, ctau))

# MG Process card
prcard_lines = []
prcard_lines.append("import model SM_HeavyN_Gen3Mass_NLO")
prcard_lines.append("define p = u c d s b u~ c~ d~ s~ b~ g")
prcard_lines.append("define j = p")
prcard_lines.append("define mu = mu+ mu-")
prcard_lines.append("define e = e+ e-")
prcard_lines.append("define ta = ta+ ta-")
prcard_lines.append("define vvm = vm vm~")
prcard_lines.append("define vve = ve ve~")
prcard_lines.append("define vvt = vt vt~")
prcard_lines.append("define w = w+ w-")
prcard_lines.append("define vv =vm vm~ ve ve~ vt vt~")
if lep[0]=="mu": 
    prcard_lines.append("generate p p > mu n1 [QCD]")
if lep[0]=="e":
    prcard_lines.append("add process p p > e n1 [QCD]")
if lep[0]=="ta":
    prcard_lines.append("add process p p > ta n1 [QCD]")   
# prcard_lines.append("display diagrams")
prcard_lines.append("output -f")
gen_process = "\n".join(prcard_lines)

# General settings
gridpack_mode=False
    
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=500*evt_multiplier

# Run card parameters
run_settings = {'lhe_version':'3.0',
                #'pdlabel'    : "'lhapdf'",
                #'lhaid'      : 260000,
                'ickkw'      : '0',
                'maxjetflavor':5, # 5 flavor scheme
                'bwcutoff': 15.0,
                'time_of_flight': tofoption,
                'parton_shower':'PYTHIA8',
                'nevents' : nevents,
                'time_of_flight' : 6.66666666667e-12

}

# Set up the process
process_dir = new_process(gen_process)
# Write run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# Dictionary for param card settings
params = {}

# HNL params
params['MASS']={'mN1':mhnl,'mN2':1e10,'mN3':1e10}#, '15':1.777e0}
params['numixing']={'VeN1' :1,'VeN2' :0,'VeN3' :0,
                    'VmuN1':1,'VmuN2':0,'VmuN3':0,
                    'VtaN1':1,'VtaN2':0,'VtaN3':0}

if ctau > 0:
    width= 197.4635212e-15/ctau # hbar*c [GeV*mm] / c*tau [mm]                 
    params['DECAY']={'WN1':'DECAY 9900012 {} # WN1'.format(width)} #we will need to add decay of tau
else:
    params['DECAY']={'WN1':'DECAY  9900012 Auto # WN1'}
# SM params
params['sminputs']={'aEWM1':1.325070e+02,'Gf':1.166390e-05 ,'aS':1.180000e-01}
# Write param card
modify_param_card(process_dir=process_dir,params=params)

# Set up the MadSpin card
madspin_lines = []
madspin_lines.append("set BW_cut 15")
madspin_lines.append("set spinmode onshell")
madspin_lines.append("set max_weight_ps_point 400")  # number of PS to estimate the maximum for each event"
madspin_lines.append("set seed %i")
if lep[1]=="e" and lep[2]=="e":
    madspin_lines.append("decay n1 > e w, w > e vv")
if lep[1]=="e" and lep[2]=="mu":
    madspin_lines.append("decay n1 > e w, w > mu vv")
if lep[1]=="e" and lep[2]=="ta":
    madspin_lines.append("decay n1 > e w, w > ta vv")
if lep[1]=="mu" and lep[2]=="e":
    madspin_lines.append("decay n1 > mu w, w > e vv")
if lep[1]=="mu" and lep[2]=="mu":
    madspin_lines.append("decay n1 > mu w, w > mu vv")
if lep[1]=="mu" and lep[2]=="ta":
    madspin_lines.append("decay n1 > mu w, w > ta vv")
if lep[1]=="ta" and lep[2]=="e":
    madspin_lines.append("decay n1 > ta w, w > e vv")
if lep[1]=="ta" and lep[2]=="mu":
    madspin_lines.append("decay n1 > ta w, w > mu vv")
if lep[1]=="ta" and lep[2]=="ta":
    madspin_lines.append("decay n1 > ta w, w > ta vv")
if lep[1]=="e" and lep[2]=="j":
    madspin_lines.append("decay n1 > e w, w > j j")
if lep[1]=="mu" and lep[2]=="j":
    madspin_lines.append("decay n1 > mu w, w > j j")
if lep[1]=="ta" and lep[2]=="j":
    madspin_lines.append("decay n1 > ta w, w > j j")

madspin_lines.append("launch")
mad_process = "\n".join(madspin_lines)
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write(mad_process%runArgs.randomSeed)
mscard.close()

# Generate the events                                                          
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
print('grid_pack?',gridpack_mode)

if ctau>=1:
    min_disp = 2.0 # [mm]fiducial volume is 4-300mm                            
    tau = (min_disp)/(1000.0*3e8) # in seconds, [mm] / [mm/m] * [m/s]          
    #Add min displacement cut                                                  
    add_lifetimes(process_dir=process_dir,threshold=tau)


arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Set the evgen metadata
evgenConfig.description = 'HNL %s (with matching), m_hnl = %s GeV, channel = %s '%(model_string, mhnl, channel)
evgenConfig.keywords = ["exotic","BSM","neutrino"]
evgenConfig.contact = ["Iacopo Longarini <iacopo.longarini@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

testSeq.TestHepMC.MaxTransVtxDisp = 1000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 4000000 #in mm
