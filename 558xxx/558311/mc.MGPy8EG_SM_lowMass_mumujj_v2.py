# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# These details are important information about the JOs
evgenConfig.description = 'SM  VBF Z/a -> mu mu mll(40,140).'
evgenConfig.contact = [ "Diego Baron <diego.baron@cern.ch>" ]
evgenConfig.keywords += ['2muon', 'VBF']

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1