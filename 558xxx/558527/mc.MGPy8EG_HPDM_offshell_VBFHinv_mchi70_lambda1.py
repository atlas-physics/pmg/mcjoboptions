USERparams = {}
USERparams["mdm"] = 70
USERparams["fcomp"] = 500
USERparams["cd"] = 0
USERparams["lamhs"] = 1

reweight = True # Determines whether to store alternative weights for tanb and sinp
reweights = [
    "lamhs_0.1",
    "lamhs_0.5",
    "lamhs_2",
    "lamhs_3",
    "lamhs_4",
    "lamhs_5",
    "lamhs_7",
    "lamhs_10",
    "lamhs_15",
    "lamhs_20",
]
# Number of events to generate
evgenConfig.nEventsPerJob=10000

# Event multiplier
LHE_EventMultiplier = 10 #For Filter, Multiplier on maxEvents

include("MadGraphControl_Py8G_HPDM_offshell_Hinv.py")

