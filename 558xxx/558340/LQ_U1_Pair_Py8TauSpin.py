# Job options for vector leptoquark (U1) pair production
# To be used for MC20 in r23.6
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os
import re
import sys
import glob
import math

# Set number of events to be generated
nevents=runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# tell TestHepMC about the vLQ PDG IDs
bonus_file = open("pdgid_extras.txt", "w")
bonus_file.write("9000007\n-9000007")
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdgid_extras.txt'

job_option_name = get_physics_short()

matchesMass = re.search("_M([0-9]+)", job_option_name)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matchesMass.group(1))

# Set couplings
decays = []
JOlist = job_option_name.split("_")

gU = 3.0
if "gU" in JOlist:
    gU = eval( JOlist[ JOlist.index("gU")+1 ].replace('p','.'))

betaL11 = 0.0
if "bL11" in JOlist:
    betaL11 = eval( JOlist[ JOlist.index("bL11")+1 ].replace('p','.'))
if betaL11 != 0:
    decays.extend(['deL', 'uveL'])

betaL12 = 0.0
if "bL12" in JOlist:
    betaL12 = eval( JOlist[ JOlist.index("bL12")+1 ].replace('p','.'))
if betaL12 != 0:
    decays.extend(['dmuL', 'uvmL'])

betaL13 = 0.0
if "bL13" in JOlist:
    betaL13 = eval( JOlist[ JOlist.index("bL13")+1 ].replace('p','.'))
if betaL13 != 0:
    decays.extend(['dtaL', 'uvtL'])

betaL21 = 0.0
if "bL21" in JOlist:
    betaL21 = eval( JOlist[ JOlist.index("bL21")+1 ].replace('p','.'))
if betaL21 != 0:
    decays.extend(['seL', 'cveL'])

betaL22 = 0.0
if "bL22" in JOlist:
    betaL22 = eval( JOlist[ JOlist.index("bL22")+1 ].replace('p','.'))
if betaL22 != 0:
    decays.extend(['smuL', 'cvmL'])

betaL23 = 0.0
if "bL23" in JOlist:
    betaL23 = eval( JOlist[ JOlist.index("bL23")+1 ].replace('p','.'))
if betaL23 != 0:
    decays.extend(['staL', 'cvtL'])

betaL31 = 0.0
if "bL31" in JOlist:
    betaL31 = eval( JOlist[ JOlist.index("bL31")+1 ].replace('p','.'))
if betaL31 != 0:
    decays.extend(['beL', 'tveL'])

betaL32 = 0.0
if "bL32" in JOlist:
    betaL32 = eval( JOlist[ JOlist.index("bL32")+1 ].replace('p','.'))
if betaL32 != 0:
    decays.extend(['bmuL', 'tvmL'])

betaL33 = 0.0
if "bL33" in JOlist:
    betaL33 = eval( JOlist[ JOlist.index("bL33")+1 ].replace('p','.'))
if betaL33 != 0:
    decays.extend(['btaL', 'tvtL'])

betaR11 = 0.0
if "bR11" in JOlist:
    betaR11 = eval( JOlist[ JOlist.index("bR11")+1 ].replace('p','.'))
if betaR11 != 0:
    decays.append('deR')

betaR12 = 0.0
if "bR12" in JOlist:
    betaR12 = eval( JOlist[ JOlist.index("bR12")+1 ].replace('p','.'))
if betaR12 != 0:
    decays.append('dmuR')

betaR13 = 0.0
if "bR13" in JOlist:
    betaR13 = eval( JOlist[ JOlist.index("bR13")+1 ].replace('p','.'))
if betaR13 != 0:
    decays.append('dtaR')

betaR21 = 0.0
if "bR21" in JOlist:
    betaR21 = eval( JOlist[ JOlist.index("bR21")+1 ].replace('p','.'))
if betaR21 != 0:
    decays.append('seR')

betaR22 = 0.0
if "bR22" in JOlist:
    betaR22 = eval( JOlist[ JOlist.index("bR22")+1 ].replace('p','.'))
if betaR22 != 0:
    decays.append('smuR')

betaR23 = 0.0
if "bR23" in JOlist:
    betaR23 = eval( JOlist[ JOlist.index("bR23")+1 ].replace('p','.'))
if betaR23 != 0:
    decays.append('staR')

betaR31 = 0.0
if "bR31" in JOlist:
    betaR31 = eval( JOlist[ JOlist.index("bR31")+1 ].replace('p','.'))
if betaR31 != 0:
    decays.append('beR')

betaR32 = 0.0
if "bR32" in JOlist:
    betaR32 = eval( JOlist[ JOlist.index("bR32")+1 ].replace('p','.'))
if betaR32 != 0:
    decays.append('bmuR')

betaR33 = 0.0
if "bR33" in JOlist:
    betaR33 = eval( JOlist[ JOlist.index("bR33")+1 ].replace('p','.'))
if betaR33 != 0:
    decays.append('btaR')

kappaU = 0.0
kappaUtilde = 0.0
lqType = ""
if "YM" in JOlist:
    kappaU = 0.0
    kappaUtilde = 0.0
    lqType = "YM"
elif "min" in JOlist:
    kappaU = 1.0
    kappaUtilde = 1.0
    lqType = "min"
else:
    raise RuntimeError("No LQ type (YM, min) sepcified.")

# assign correct decays
decayLines = []
for dec in decays:
    if dec[0] in ['u', 'c', 't']:
        if dec[1] in ['e', 'm', 't']:
            raise RuntimeError("Fermion charges cannot add up to LQ charge.")
        decayStatement = 'decay vlq > '+dec[0]+' '+dec[1]+dec[2]+'~'
        if dec[0]=="t": decayStatement += ', (t > w+ b, w+ > all all)'
        decayLines.append(decayStatement)
        decayStatement = 'decay vlq~ > '+dec[0]+'~ '+dec[1]+dec[2]
        if dec[0]=="t": decayStatement += ', (t~ > w- b~, w- > all all)'
        decayLines.append(decayStatement)
    elif dec[0] in ['d', 's', 'b']:
        if dec[1] == 'v':
            raise RuntimeError("Fermion charges cannot add up to LQ charge.")
        decayStatement = 'decay vlq > '+dec[0]+' '+dec[1]+dec[2]+'+'
        decayLines.append(decayStatement)
        decayStatement = 'decay vlq~ > '+dec[0]+'~ '+dec[1]+dec[2]+'-'
        decayLines.append(decayStatement)
    else:
        raise RuntimeError("Unexpected quark flavour.")


# ecmEnergy given when running Gen_tf.py
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process='import model vector_LQ_UFO_allflavours \ngenerate p p > vlq vlq~\noutput -f')

# Fetch default NLO run_card.dat and set parameters
# 260800 corresponds to NNPDF30_nlo_as_0118_mc
settings = {
    'nevents'      :int(nevents),
    'scale'        :lqmass,
    'dsqrt_q2fact1':lqmass,
    'dsqrt_q2fact2':lqmass
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'MASS':{
        '9000007' : '{:e}'.format(lqmass)
    },
    'NPLQCOUP':{
        '1'       : '{:e}'.format(gU),
        '2'       : '{:e}'.format(betaL11),
        '3'       : '{:e}'.format(betaL12),
        '4'       : '{:e}'.format(betaL13),
        '5'       : '{:e}'.format(betaL21),
        '6'       : '{:e}'.format(betaL22),
        '7'       : '{:e}'.format(betaL23),
        '8'       : '{:e}'.format(betaL31),
        '9'       : '{:e}'.format(betaL32),
        '10'      : '{:e}'.format(betaL33),
        '11'      : '{:e}'.format(betaR11),
        '12'      : '{:e}'.format(betaR12),
        '13'      : '{:e}'.format(betaR13),
        '14'      : '{:e}'.format(betaR21),
        '15'      : '{:e}'.format(betaR22),
        '16'      : '{:e}'.format(betaR23),
        '17'      : '{:e}'.format(betaR31),
        '18'      : '{:e}'.format(betaR32),
        '19'      : '{:e}'.format(betaR33),
        '20'      : '{:e}'.format(kappaU),
        '21'      : '{:e}'.format(kappaUtilde)
    },
    'DECAY':{
        '9000007' : 'Auto'       
    }
}
modify_param_card(process_dir=process_dir, params=parameters)

# MadSpin
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card, 'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer * 
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#*    Manual:                                               *
#*    cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MadSpin     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                 # cut on how far the particle can be off-shell
# set spinmode onshell          # Use one of the madspin special mode
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all\n"""%runArgs.randomSeed)

for l in decayLines:
    mscard.write(l+'\n')

mscard.write("""
# running the actual code
launch""")
mscard.close()

# Print cards on screen
print_cards(run_card='run_card.dat', param_card='param_card.dat', madspin_card='madspin_card.dat')

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=False, runArgs=runArgs)

# Metadata
evgenConfig.description = ('Pair production of U1 vector leptoquarks, type {0}, mLQ={1:d}').format(lqType, int(lqmass))
evgenConfig.keywords+=['BSM','exotic','leptoquark'] #, 'vector'
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> vlq vlq~'
evgenConfig.contact = ["Volker Austrup <volker.andreas.austrup@cern.ch>"]
#runArgs.inputGeneratorFile=outputDS

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
# include("Pythia8_i/Pythia8_TauolaPP.py")

genSeq.Pythia8.Commands += [ '15:onMode = on', # decay of taus
                             #'15:offIfAny = 11 13',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]