USERparams = {}
USERparams["mdm"] = 100
USERparams["fcomp"] = 100
USERparams["cd"] = 1
USERparams["lamhs"] = 0.0

reweight = True # Determines whether to store alternative weights for tanb and sinp
reweights = [
    "fcomp_20",
    "fcomp_30",
    "fcomp_40",
    "fcomp_50",
    "fcomp_60",
    "fcomp_70",
    "fcomp_120",
    "fcomp_150",
    "fcomp_200",
    "fcomp_250",
    "fcomp_300",
    "fcomp_400",
    "fcomp_500",
]
# Number of events to generate
evgenConfig.nEventsPerJob=10000

# Event multiplier
LHE_EventMultiplier = 10 #For Filter, Multiplier on maxEvents

include("MadGraphControl_Py8G_HPDM_offshell_Hinv.py")

