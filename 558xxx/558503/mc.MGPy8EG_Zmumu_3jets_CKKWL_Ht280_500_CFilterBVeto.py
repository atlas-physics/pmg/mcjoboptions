ihtmin=280
ihtmax=500
est_eff=0.09
est_nevt=120000
include("MadGraphControl_Zmumu_3jets_CKKWL.py")
genSeq.Pythia8.useRndmGenSvc = False
evgenConfig.nEventsPerJob = 10000

# Set up HF filters
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter

include("GeneratorFilters/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (HeavyFlavorCHadronPt4Eta3_Filter)"




