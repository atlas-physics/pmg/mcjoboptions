import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'Zll + 0, 1, 2, 3 jets, MG LO, Py8 CKKW, 5FNS'
evgenConfig.contact = ["yi.yu@cern.ch","qibin.liu@cern.ch"]
evgenConfig.keywords += ['Z','jets']
evgenConfig.generators += ["MadGraph", "Pythia8"]

# General settings
est_gen_eff=0.01
if 'est_eff' in locals() or 'est_eff' in globals():
    est_gen_eff=est_eff
est_gen_nevt=700000
if 'est_nevt' in locals() or 'est_nevt' in globals():
    est_gen_nevt=est_nevt
nevents = int(runArgs.maxEvents/est_gen_eff*1.3) if runArgs.maxEvents>0 else int(est_gen_nevt*1.3)

# Shower/merging settings  
maxjetflavor=5
ickkw=0
nJetMax=3
ktdurham=30      
dparameter=0.2
parton_shower='PYTHIA8'

# MG Particle cuts
mllcut=40

# Madgraph run card and shower settings
gridpack_mode=True

process="pp>e+e-"
if not is_gen_from_gridpack():
    mgproc = """
    import model loop_sm-no_b_mass
    define p = p b b~
    define j = p
    generate p p > e+ e- @0
    add process p p > e+ e- j @1
    add process p p > e+ e- j j @2
    add process p p > e+ e- j j j @3
    output -f"""

    process_dir = str(new_process(mgproc))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)


settings  = { 'lhe_version'    : '3.0',
           'cut_decays'     : 'F', 
           'maxjetflavor'   : maxjetflavor,
           'asrwgtflavor'   : maxjetflavor,
           'ickkw'          : 0,
           'xqcut'          : 0,
           'ptj'            : 10,      
           'ptb'            : 10,      
           'mmll'           : mllcut,      
           'ihtmin'         : ihtmin,
           'ihtmax'         : ihtmax,
           'mmjj'           : 0,
           'drjj'           : 0,
           'drll'           : 0,
           'drjl'           : 0.,       
           'ptl'            : 0,
           'etal'           : 10,
           'etab'           : 6,
           'etaj'           : 6,
           'ktdurham'       : ktdurham,    
           'dparameter'     : dparameter,
           'nevents'        : nevents  }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

check_reset_proc_number(opts)

#### Shower 
PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process                                                      
PYTHIA8_nQuarksMerge=maxjetflavor
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")                       

genSeq.Pythia8.Commands += ["Merging:unorderedASscalePrescrip = 0"]
genSeq.Pythia8.useRndmGenSvc = False