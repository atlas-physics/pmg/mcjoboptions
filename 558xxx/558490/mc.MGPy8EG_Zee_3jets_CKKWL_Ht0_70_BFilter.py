ihtmin=0
ihtmax=70
est_eff=0.02
est_nevt=610000
include("MadGraphControl_Zee_3jets_CKKWL.py")
genSeq.Pythia8.useRndmGenSvc = False
evgenConfig.nEventsPerJob = 10000

# Set up HF filters
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
include("GeneratorFilters/BHadronFilter.py")
HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
HeavyFlavorBHadronFilter.RequireTruthJet = True
HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
HeavyFlavorBHadronFilter.JetEtaMax = 2.9
HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
filtSeq += HeavyFlavorBHadronFilter

