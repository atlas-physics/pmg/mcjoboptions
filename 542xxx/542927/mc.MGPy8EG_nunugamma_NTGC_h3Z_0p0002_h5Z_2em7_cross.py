import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short


definitions="""import model NTGC_VF
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > vl vl~ a H1A^2==0 H1Z^2==0 H2A^2==0 H2Z^2==0 H3A^2==0 H3Z^2==1 H4A^2==0 H4Z^2==0 H5A^2==0 H5Z^2==1 H6A^2==0 H6Z^2==0 F4A^2==0 F4Z^2==0 F5A^2==0 F5Z^2==0
"""

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define cuts and other run card settings
settings={'dynamical_scale_choice':'3', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'event_norm':'average',
        'lhe_version': '3.0',
        'etaa' : '3',
        'ptgmin' : "140.0",
        'epsgamma' : "0.1",
        'R0gamma' : "0.1", 
        'xn'    : "2.0",
        'isoEM' : "True",
	'maxjetflavor' : '5'
}
#############################################################################################

process=definitions+'output -f'

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
param_dict = {
    'ANTGC':{'f4a' : '0.0', 'f4Z' : '0.0', 'f5a' : '0.0', 'f5Z' : '0.0', 'h1a' : '0.0', 'h1Z' : '0.0', 'h3a': '0.0','h3Z': '0.0002', 'h4a': '0.0', 'h4Z': '0.0','h5a': '0.0','h5Z': '0.0000002', 'h6a' : '0.0', 'h6Z' : '0.0'},
}
modify_param_card(process_dir=process_dir,params=param_dict)

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#############################################################################################
# add meta data
evgenConfig.description = 'MadGraph nunugamma nTGC with h3Z=0.0002 and h5Z=0.0000002 cross term'
evgenConfig.keywords+=['SM','Z','photon','diboson']
evgenConfig.contact = ['Artur Semushin <Artur.Semushin@cern.ch>']
#############################################################################################

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



