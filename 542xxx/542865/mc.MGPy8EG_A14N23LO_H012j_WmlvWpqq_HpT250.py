evgenConfig.process     = "boosted ggH H->WW->qqlv"
evgenConfig.description = "aMcAtNlo+PYTHIA8+EVTGEN, H+jet production with LHE H pT>250, H->WW->qqlv mh=125 GeV, Wm->lv, Wp->qq"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.contact     = ["dylan.sheldon.rankin@cern.ch"]
evgenConfig.generators  = ["aMcAtNlo","Pythia8"]
evgenConfig.inputFilesPerJob = 3
evgenConfig.nEventsPerJob = 10000

nevents = runArgs.maxEvents*3.0 if runArgs.maxEvents>0 else 3.0*evgenConfig.nEventsPerJob

############################
# Shower JOs will go here
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# Matching settings
genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:nQmatch          = 5",     # Should match maxjetflavour in 'mg5'.
    "JetMatching:jetAlgorithm     = 2",
    "JetMatching:slowJetPower     = 1",
    "JetMatching:coneRadius       = 1.0",   # Default.
    "JetMatching:etaJetMax        = 999.",
    "JetMatching:nJetMax          = 2",     # *** Must match highest Born-level jet multiplicity. ***
    "JetMatching:qCut             = 30.0",  # Merging scale for FxFx.
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = 10.0"   # Should match ptj.
    "SLHA:useDecayTable           = off",
    "25:m0                        = 125.0",
    "25:onMode                    = off",
    "25:onIfMatch                 = 24 -24",
    "24:onMode                    = off",
    "24:mMin                      = 2.0",
    "24:onPosIfAny               = 1 2 3 4 5",
    "24:onNegIfAny               = 11 13 15",
]

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("ElMuFilter")

filtSeq.ElMuFilter.Ptcut = 20000.
filtSeq.ElMuFilter.Etacut = 5.0

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HadWFilter")

filtSeq.HadWFilter.PDGParent = [24]
filtSeq.HadWFilter.PtMinParent = 150000.
filtSeq.HadWFilter.PDGChild = [1, 2, 3, 4, 5]

filtSeq.Expression = "(ElMuFilter) and (HadWFilter)"
