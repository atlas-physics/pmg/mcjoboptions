from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

Mdp = 2000.0  # GeV
vevD = 1e-08  # GeV
yuk_diag = 1.0
yuk_off_diag = 0.7071067811865475
safety = 1.1
modify_yuk = True

nevents = runArgs.maxEvents * safety

with open('pdgid_extras.txt', 'w+') as pdgfile:
    pdgfile.write('''61
-61''')

genSeq.Pythia8.Commands += [
    '24:onMode = off',
]

process = '''
import model TypeII_NLO_v_1_4_yuk_UFO
'''

process += '''generate p p > d++ d-- [QCD]'''

process += '''
output -f
'''

process_dir = new_process(process)

settings = {
    'ickkw': 0,
    'nevents': nevents,
}

modify_run_card(
    process_dir=process_dir,
    runArgs=runArgs,
    settings=settings,
)

# delta M = Mdp - Mdpp = 0
masses = {
    'MDP': Mdp,
    'MDPP': Mdp,
}

vevD = {
    'vevD': f'{vevD}',
}

decays = {
    'WDP': '0.0',
    'WD0': '0.0',
    'Wchi': '0.0',
    'WZ': '0.0',
    'WW': '0.0',
    'WT': '0.0',
    'WH': '0.0',
    'ghz': '0.0',
    'ghwp': '0.0',
    'ghwm': '0.0',
}

decays['WDPP'] = '''1.011029e-05 # wdpp default
    1.666667e-01 2 -13 -11 # mu e
    1.666667e-01 2 -11 -11 # e e
    1.666667e-01 2 -13 -13 # mu mu
    1.666667e-01 2 -15 -11 # tau e
    1.666667e-01 2 -15 -13 # tau mu
    1.666667e-01 2 -15 -15 # tau tau
'''

ydelta = {
    'yDL1x1': f'{yuk_diag}',
    'yDL1x2': f'{yuk_off_diag}',
    'yDL1x3': f'{yuk_off_diag}',
    'yDL2x1': f'{yuk_off_diag}',
    'yDL2x2': f'{yuk_diag}',
    'yDL2x3': f'{yuk_off_diag}',
    'yDL3x1': f'{yuk_off_diag}',
    'yDL3x2': f'{yuk_off_diag}',
    'yDL3x3': f'{yuk_diag}',
}

params = {
    'MASS': masses,
    'DECAY': decays,
    'VEVDELTA': vevD,
}

if modify_yuk:
    params['YDELTA'] = ydelta

modify_param_card(
    process_dir=process_dir,
    params=params,
)

generate(process_dir=process_dir, runArgs=runArgs)

evgenConfig.description = 'Type-II Seesaw model with mdp = 2000.0 GeV'
evgenConfig.contact = ['Jan Gavranovic <jan.gavranovic@cern.ch>']
evgenConfig.keywords += ['BSM', 'exotic', 'sameSign', 'multilepton', 'chargedHiggs', 'seeSaw', 'BSMHiggs']

arrange_output(process_dir=process_dir, runArgs=runArgs)
