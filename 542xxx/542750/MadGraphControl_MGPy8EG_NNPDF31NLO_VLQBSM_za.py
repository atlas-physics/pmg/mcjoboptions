from MadGraphControl.MadGraphUtils import *
import re
import math
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
print phys_short
runName='test'
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

MassTP = int(phys_short.split('_')[1].replace("TL",""))
MassS = int(phys_short.split('_')[2].replace("S",""))
WoMTP = 0.001*MassTP
WoMS = 0.001*MassS
runArgs.massTP = MassTP
runArgs.massS  = MassS
runArgs.WoMTP  = WoMTP
runArgs.WoMS   = WoMS

print MassTP
print MassS
print WoMTP
print WoMS

# parameters from MG
runArgs.MT  = 172.5               # top mass
runArgs.MZ  = 91.18760            # Z mass
runArgs.MW  = 79.82436            # W mass
runArgs.vev = 246.22056907348590  # vev
runArgs.cw  = 0.87538612427783857 # cw 
runArgs.sw  = 0.48342438232036300 # cw
runArgs.ee  = 0.31345100004952897 # ee

# couplings to get width/mass ratios
kappa_TlSt = 2*math.sqrt(runArgs.WoMTP*math.pi)*(runArgs.massTP**2) / 5 / math.sqrt(5*((-runArgs.massS**2 + runArgs.MT**2 + runArgs.massTP**2)*math.sqrt( runArgs.massS**4 + (runArgs.MT**2 - runArgs.massTP**2)**2 - 2*runArgs.massS**2*(runArgs.MT**2 + runArgs.massTP**2) )))
kappa_KP10AA = (8*math.sqrt(2*runArgs.WoMS*math.pi**5)*runArgs.vev)/(5*math.sqrt(5)*runArgs.ee**2*runArgs.massS)
kappa_KP10ZA = (4*math.sqrt(2*runArgs.WoMS*math.pi**5)*runArgs.cw*runArgs.massS**2*runArgs.sw*runArgs.vev)/(5*runArgs.ee**2*math.sqrt(5*(runArgs.massS**2 - runArgs.MZ**2)**3))


print ("Mass of Tp:  ", runArgs.massTP)
print ("Mass of S:  ", runArgs.massS)
print ("TLSt coupling:  ", kappa_TlSt)
print ("SAA coupling:  ", kappa_KP10AA)
print ("SZA coupling:  ", kappa_KP10ZA)
gen_process = None
gen_process = """
import model eVLQ_S012_diagonalCKM_5FNS_only3rd_NLO_UFO
define ferm = ve vm vt ve~ vm~ vt~ mu- ta- e- mu+ ta+ e+ u c d s u~ c~ d~ s~
generate    p p > tp tp~, (tp  > s0 t , s0 > a a, (t  > w+ b , (w+ > ferm ferm))), (tp~ > s0 t~, s0 > z a, (t~ > w- b~, (w- > ferm ferm)), z > ferm ferm)
add process p p > tp tp~, (tp~ > s0 t~, s0 > a a, (t~ > w- b~, (w- > ferm ferm))), (tp  > s0 t , s0 > z a, (t  > w+ b , (w+ > ferm ferm)), z > ferm ferm)
output -f
"""


#process_dir = new_process()
safety = 1.1
nevents = runArgs.maxEvents * safety

run_settings = {'lhe_version':'3.0',
                'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
                'asrwgtflavor': 5,
                'maxjetflavor':5,  #5 flavor scheme
                'ptj':20.,
                'nevents': nevents
            }

process_dir = new_process(gen_process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

params = {}
params["MASS"] = {'6000006':runArgs.massTP, '6100001':runArgs.massS}
params["ks10tl"] = {'3':kappa_TlSt}
params["kp10vv"] = {'4':kappa_KP10ZA, '5':kappa_KP10AA}
params["DECAY"] = {'6000006':'DECAY 6000006 auto', '6100001':'DECAY 6100001 auto'}
modify_param_card(process_dir=process_dir,params=params)
print_cards()

runName='run_01'
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN for pair VLQ decaying to scalars and scalar decaying to aa and za"

evgenConfig.keywords = ["BSM", "BSMtop", "exotic"]

evgenConfig.process = "Taaza"

evgenConfig.contact =  ['luca.panizzi@physics.uu.se']
