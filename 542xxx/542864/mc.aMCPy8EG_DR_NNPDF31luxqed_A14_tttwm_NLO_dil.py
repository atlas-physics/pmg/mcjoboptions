from MadGraphControl.MadGraphUtils import *
# from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
import os


# customised pdf base fragment
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
      'central_pdf': 324900, # NNPDF31_nlo_as_0118_luxqed
      'pdf_variations':[324900,303000,93300], # NNPDF31_nlo_as_0118_luxqed,NNPDF30_nlo_as_0118_hessian,PDF4LHC21_40_pdfas
      'alternative_pdfs':[266000, #NNPDF30_nlo_as_0119
                          265000, #NNPDF30_nlo_as_0117
                          303200, #NNPDF30_nnlo_as_0118_hessian
                          27400, #MSHT20nnlo_as118
                          27100, #MSHT20nlo_as118
                          14000, #CT18NNLO
                          14400, #CT18NLO
                          304400, #NNPDF31_nnlo_as_0118_hessian
                          304200, #NNPDF31_nlo_as_0118_hessian
                          331500, #NNPDF40_nnlo_as_01180_hessian
                          331700, #NNPDF40_nlo_as_01180
                          14200, #CT18ANNLO
                          14300, #CT18XNNLO
                          14100, #CT18ZNNLO
                          13100, #CT14nlo
                          25200, #MMHT2014nlo68clas118
                          ],
      'scale_variations':[0.5,1.,2.],
}


# General settings
# runName = 'run_01'
# gridpack_dir  = 'madevent/' # to generate from gridpack
# True: generate gridpack; False: Run events
gridpack_mode=False

evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*3.0 if runArgs.maxEvents>0 else 3.0*evgenConfig.nEventsPerJob


mgproc="generate p p > t t t~ W- [QCD]"

if not is_gen_from_gridpack():
      process="""
      import model loop_qcd_qed_sm
      define p = 21 2 4 1 3 -2 -4 -1 -3 5 -5 22 # pass to 5 flavors
      define j = p
      """+mgproc+"""
      output -f
      """
      process_dir = new_process(process)
else:
      process_dir = MADGRAPH_GRIDPACK_LOCATION



print("""========================================


Add patch.
The patch is implemented by theorist G. Durieux.
https://github.com/gdurieux/triple-top-nlo/tree/master
This works effectively as a diagram removal scheme to remove the overlap with the SM 4top.


========================================
""")

### get patch
def patch(model_PROC = "PROCNLO_loop_qcd_qed_sm_0"):
      PATCH="""--- %s/SubProcesses/P0_gb_tttxwm/matrix_4.f     2023-02-21 19:03:36.000000001 +0100
+++ %s/SubProcesses/P0_gb_tttxwm/matrix_4.f     2023-02-21 19:03:36.000000001 +0100
@@ -486,7 +486,7 @@
       CALL FFV1_0(W(1,7),W(1,16),W(1,9),GC_11,AMP(4))
 C     Amplitude(s) for diagram number 5
       CALL FFV1_0(W(1,12),W(1,14),W(1,9),GC_11,AMP(5))
-      CALL FFV2_2(W(1,7),W(1,6),GC_124,MDL_MT,MDL_WT,W(1,16))
+      CALL FFV2_2(W(1,7),W(1,6),0.0D-05*GC_124,MDL_MT,2.0D0,W(1,16))
 C     Amplitude(s) for diagram number 6
       CALL FFV1_0(W(1,16),W(1,4),W(1,15),GC_11,AMP(6))
 C     Amplitude(s) for diagram number 7
--- %s/SubProcesses/P0_gb_tttxwm/matrix_6.f   2023-02-21 19:03:37.000000001 +0100
+++ %s/SubProcesses/P0_gb_tttxwm/matrix_6.f   2023-02-21 19:03:37.000000001 +0100
@@ -558,7 +558,7 @@
       CALL FFV1_0(W(1,7),W(1,16),W(1,9),GC_11,AMP(4))
 C     Amplitude(s) for diagram number 5
       CALL FFV1_0(W(1,12),W(1,14),W(1,9),GC_11,AMP(5))
-      CALL FFV2_2(W(1,7),W(1,6),GC_124,MDL_MT,MDL_WT,W(1,16))
+      CALL FFV2_2(W(1,7),W(1,6),0.0D-05*GC_124,MDL_MT,2.0D0,W(1,16))
 C     Amplitude(s) for diagram number 6
       CALL FFV1_0(W(1,16),W(1,4),W(1,15),GC_11,AMP(6))
 C     Amplitude(s) for diagram number 7
--- %s/SubProcesses/P0_bg_tttxwm/matrix_2.f     2023-02-21 19:03:37.000000001 +0100
+++ %s/SubProcesses/P0_bg_tttxwm/matrix_2.f     2023-02-21 19:03:37.000000001 +0100
@@ -558,7 +558,7 @@
       CALL FFV1_0(W(1,7),W(1,16),W(1,9),GC_11,AMP(4))
 C     Amplitude(s) for diagram number 5
       CALL FFV1_0(W(1,12),W(1,14),W(1,9),GC_11,AMP(5))
-      CALL FFV2_2(W(1,7),W(1,6),GC_124,MDL_MT,MDL_WT,W(1,16))
+      CALL FFV2_2(W(1,7),W(1,6),0.0D-05*GC_124,MDL_MT,2.0D0,W(1,16))
 C     Amplitude(s) for diagram number 6
       CALL FFV1_0(W(1,16),W(1,4),W(1,15),GC_11,AMP(6))
 C     Amplitude(s) for diagram number 7
--- %s/SubProcesses/P0_bg_tttxwm/matrix_5.f   2023-02-21 19:03:37.000000001 +0100
+++ %s/SubProcesses/P0_bg_tttxwm/matrix_5.f   2023-02-21 19:03:37.000000001 +0100
@@ -486,7 +486,7 @@
       CALL FFV1_0(W(1,7),W(1,16),W(1,9),GC_11,AMP(4))
 C     Amplitude(s) for diagram number 5
       CALL FFV1_0(W(1,12),W(1,14),W(1,9),GC_11,AMP(5))
-      CALL FFV2_2(W(1,7),W(1,6),GC_124,MDL_MT,MDL_WT,W(1,16))
+      CALL FFV2_2(W(1,7),W(1,6),0.0D-05*GC_124,MDL_MT,2.0D0,W(1,16))
 C     Amplitude(s) for diagram number 6
       CALL FFV1_0(W(1,16),W(1,4),W(1,15),GC_11,AMP(6))
 C     Amplitude(s) for diagram number 7""" % (model_PROC,model_PROC,model_PROC,model_PROC,model_PROC,model_PROC,model_PROC,model_PROC)
      return PATCH

if not is_gen_from_gridpack():
      os.system("patch -p0 <<< \""+patch(model_PROC = "PROCNLO_loop_qcd_qed_sm_0")+"\" ")



print("""========================================


Modify run card


========================================
""")

bwcut = 15.0

#Fetch default LO run_card.dat and set parameters
run_settings = {
      'nevents'                : nevents,
      'req_acc'                : 0.0003,
      'ptj'                    : 10.,
      'etaj'                   : -1.,
      'dynamical_scale_choice' : 3, # for NLO this should be the same as default -1
      'mur_over_ref'           : 0.25, # scale the central scale by a factor of 0.25 - effectively becomes HT/8
      'muf_over_ref'           : 0.25,
      'parton_shower'          :  "PYTHIA8",
      'bwcutoff'               : bwcut,
}

modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=run_settings)

set_top_params(process_dir,mTop=172.5,FourFS=False)

params = {}

## trying to fix madspin run in gridpack run
masses = {'15': 0.0} # this one gets fixed at 0. by the model. causes conflict when running in gridpack mode
if is_gen_from_gridpack() :
      params['MASS']=masses

decays = {
      # Width of W. has to be set to 0.
      # Gauthier's original config sets complex_mass_scheme to true. But this doesn't work in ATLAS.
      # According to Gauthier, it's ok to turn it off. But then it's essential to set WW to 0.
      '24': """DECAY  24   0.""",
      # Width of top. ditto.
      # but anyway set to zero by MG as final state particle
      '6': """DECAY 6   0.""",
}
params['DECAY']=decays

modify_param_card(process_dir=process_dir,params=params)


#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------
topdecay = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \ndecay w+ > all all \ndecay w- > all all\n"

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card_loc,os.R_OK):
      os.unlink(madspin_card_loc)
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#set Nevents_for_max_weigth 75
set ms_dir %s/MadSpin
set BW_cut %i
set seed %i
%s
launch
"""%(process_dir, bwcut, runArgs.randomSeed, topdecay))
mscard.close()


print("""========================================


Print run card


========================================
""")


print_cards()




print("""========================================


Generate


========================================
""")



generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower
evgenConfig.description = 'aMCAtNLO tttWm @ NLO'
evgenConfig.keywords+=['SM','top']
evgenConfig.contact= ["yang.qin@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

include("GeneratorFilters/xAODMultiLeptonFilter_Common.py")
filtSeq.xAODMultiLeptonFilter.Ptcut    = 10000.
filtSeq.xAODMultiLeptonFilter.Etacut   = 3.
filtSeq.xAODMultiLeptonFilter.NLeptons = 2
