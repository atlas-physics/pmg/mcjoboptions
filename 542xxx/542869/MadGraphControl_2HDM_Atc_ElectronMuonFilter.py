from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import*
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import *

# Evgen Config
evgenConfig.description = "MadGraph+Pythia8 for ppHtc with different Higgs mass points"
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.keywords = ["BSM", "QCD"]
evgenConfig.process = "generate g g > h3 > tp cp / h1 h2 h+ u d s c [noborn=QCD]"
evgenConfig.contact = ["Burhani Taher Saifuddin <burhani.taher.saifuddin@cern.ch>"]

# Process card
process = """import model 2HDM_NLO_CPV
define tp = t t~
define cp = c c~
generate g g > h3 > tp cp / h1 h2 h+ u d s c [noborn=QCD] 
output -f"""
process_dir = new_process(process)

# Higgs Masses
mh1 = 125.1
mh3 = pHiggs_mass #Pseudoscalar Higgs mass
mh2 = mh3         #Scalar Higgs mass

# param card
params = dict() 

# Higgs mixing
params['HIGGS'] = {
    '5': 1.000000e-02, #mixh
    '6': 0.000000e+00, #mixh2
    '7': 0.000000e+00  #mixh3
}

# Yukawa 
params['YUKAWA'] = {
    '5': 4.700000e+00 # ymb
}

# Higgs Mass
params['MASS'] = {
    '25': mh1, #SM higgs
    '35': mh2, #BSM scalar higgs
    '36': mh3  #BSM pseudoscalar higgs
}

# Higgs Decay Width
params['DECAY'] = {
    '25': '4.967e-03',
    '35': sHiggs_Decay, #BSM scalar higgs
    '36': pHiggs_Decay  #BSM pseudoscalar higgs
}

# Lepton-type Yukawa matrices
params['YUKAWAGLR'] = {
    '1 1': 2.940000e-06,
    '1 2': 0.000000e+00,
    '1 3': 0.000000e+00,
    '2 1': 0.000000e+00,
    '2 2': 6.060000e-04,
    '2 3': 1.020000e-02,
    '3 1': 0.000000e+00,
    '3 2': 1.020000e-02,
    '3 3': 1.020000e-02
}

# Down-type Yukawa matrices
params['YUKAWAGDR'] = {
    '1 1': 0.000000e+00,
    '1 2': 0.000000e+00,
    '1 3': 0.000000e+00,
    '2 1': 0.000000e+00,
    '2 2': 5.740000e-04,
    '2 3': 0.000000e+00,
    '3 1': 0.000000e+00,
    '3 2': 0.000000e+00,
    '3 3': 2.690000e-02
}

# Up-type Yukawa matrices
params['YUKAWAGUR'] = {
    '1 1': 0.000000e+00,
    '1 2': 0.000000e+00,
    '1 3': 0.000000e+00,
    '2 1': 0.000000e+00,
    '2 2': 8.156030e-03,
    '2 3': 5.000000e-01, # rhotc
    '3 1': 0.000000e+00,
    '3 2': 5.000000e-01, # rhotc
    '3 3': mh3 / 750     # rhott
}
modify_param_card(process_dir=process_dir, params=params)

#number of events
nevents = runArgs.maxEvents*7 if runArgs.maxEvents>0 else 7*evgenConfig.nEventsPerJob

# Set a fixed renormalization and factorization scale
settings = {'fixed_ren_scale': 'True',
	    'fixed_fac_scale': 'True',
            'nevents':int(nevents),
            'scale': mh3 / 2,
            'dsqrt_q2fact1': mh3 / 2,
            'dsqrt_q2fact2': mh3 / 2
           }
            
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
check_reset_proc_number(opts) # Force serial processing
include("Pythia8_i/Pythia8_MadGraph.py")

# Decay with Pythia8
genSeq.Pythia8.Commands  += [ '6:onMode = off', '6:onIfAny = 24 5' ]
genSeq.Pythia8.Commands  += [ '24:onMode = off', '24:onIfAny = 11 13 15' ]
genSeq.Pythia8.Commands  += [ '15:onMode = off', '15:onIfAny = 11 13' ]

# Generator filters
if not hasattr( filtSeq, "ElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
  filtSeq += ElectronFilter()
  print ("ElectronFilter added ...")
  pass

filtSeq.ElectronFilter = filtSeq.ElectronFilter
filtSeq.ElectronFilter.Ptcut = 23000.

if not hasattr( filtSeq, "MuonFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MuonFilter
  filtSeq += MuonFilter()
  print ("MuonFilter added ...")
  pass

filtSeq.MuonFilter = filtSeq.MuonFilter
filtSeq.MuonFilter.Ptcut = 23000.

filtSeq.Expression = "(ElectronFilter and not MuonFilter) or (not ElectronFilter and MuonFilter)"
