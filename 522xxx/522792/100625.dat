# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  12:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.78139418E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.62775163E+03  # scale for input parameters
    1    1.31785657E+02  # M_1
    2    1.46972546E+03  # M_2
    3    3.01236757E+03  # M_3
   11   -3.01874842E+03  # A_t
   12    9.62448660E+02  # A_b
   13   -1.04378930E+03  # A_tau
   23    4.25790649E+02  # mu
   25    1.65986089E+00  # tan(beta)
   26    1.04822640E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.90105524E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.33033812E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.74798080E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.62775163E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.62775163E+03  # (SUSY scale)
  1  1     9.78840240E-06   # Y_u(Q)^DRbar
  2  2     4.97250842E-03   # Y_c(Q)^DRbar
  3  3     1.18251763E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.62775163E+03  # (SUSY scale)
  1  1     3.26485392E-05   # Y_d(Q)^DRbar
  2  2     6.20322245E-04   # Y_s(Q)^DRbar
  3  3     3.23771203E-02   # Y_b(Q)^DRbar
Block Ye Q=  4.62775163E+03  # (SUSY scale)
  1  1     5.69740884E-06   # Y_e(Q)^DRbar
  2  2     1.17804345E-03   # Y_mu(Q)^DRbar
  3  3     1.98127528E-02   # Y_tau(Q)^DRbar
Block Au Q=  4.62775163E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.01874981E+03   # A_t(Q)^DRbar
Block Ad Q=  4.62775163E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     9.62448551E+02   # A_b(Q)^DRbar
Block Ae Q=  4.62775163E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.04378919E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.62775163E+03  # soft SUSY breaking masses at Q
   1    1.31785657E+02  # M_1
   2    1.46972546E+03  # M_2
   3    3.01236757E+03  # M_3
  21    5.70804675E+05  # M^2_(H,d)
  22    7.67728275E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.90105524E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.33033812E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.74798080E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.00240799E+02  # h0
        35     1.05120072E+03  # H0
        36     1.04822640E+03  # A0
        37     1.06045460E+03  # H+
   1000001     1.01146479E+04  # ~d_L
   2000001     1.00902994E+04  # ~d_R
   1000002     1.01144513E+04  # ~u_L
   2000002     1.00934636E+04  # ~u_R
   1000003     1.01146480E+04  # ~s_L
   2000003     1.00902994E+04  # ~s_R
   1000004     1.01144513E+04  # ~c_L
   2000004     1.00934637E+04  # ~c_R
   1000005     2.90917410E+03  # ~b_1
   2000005     4.94790836E+03  # ~b_2
   1000006     4.32309320E+03  # ~t_1
   2000006     4.95388005E+03  # ~t_2
   1000011     1.00208218E+04  # ~e_L-
   2000011     1.00089896E+04  # ~e_R-
   1000012     1.00202225E+04  # ~nu_eL
   1000013     1.00208218E+04  # ~mu_L-
   2000013     1.00089896E+04  # ~mu_R-
   1000014     1.00202225E+04  # ~nu_muL
   1000015     1.00089881E+04  # ~tau_1-
   2000015     1.00208242E+04  # ~tau_2-
   1000016     1.00202227E+04  # ~nu_tauL
   1000021     3.51331249E+03  # ~g
   1000022     1.25329256E+02  # ~chi_10
   1000023     4.48359673E+02  # ~chi_20
   1000025     4.48545507E+02  # ~chi_30
   1000035     1.56600071E+03  # ~chi_40
   1000024     4.42982165E+02  # ~chi_1+
   1000037     1.56532608E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.18207938E-01   # alpha
Block Hmix Q=  4.62775163E+03  # Higgs mixing parameters
   1    4.25790649E+02  # mu
   2    1.65986089E+00  # tan[beta](Q)
   3    2.42861858E+02  # v(Q)
   4    1.09877859E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.36204232E-02   # Re[R_st(1,1)]
   1  2     9.96497679E-01   # Re[R_st(1,2)]
   2  1    -9.96497679E-01   # Re[R_st(2,1)]
   2  2     8.36204232E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.39046163E-06   # Re[R_sb(1,1)]
   1  2     1.00000000E+00   # Re[R_sb(1,2)]
   2  1    -1.00000000E+00   # Re[R_sb(2,1)]
   2  2    -1.39046163E-06   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.31866078E-02   # Re[R_sta(1,1)]
   1  2     9.99913053E-01   # Re[R_sta(1,2)]
   2  1    -9.99913053E-01   # Re[R_sta(2,1)]
   2  2     1.31866078E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.90682266E-01   # Re[N(1,1)]
   1  2     6.94846053E-03   # Re[N(1,2)]
   1  3    -1.08871298E-01   # Re[N(1,3)]
   1  4     8.15316316E-02   # Re[N(1,4)]
   2  1    -1.91944303E-02   # Re[N(2,1)]
   2  2     9.83931695E-03   # Re[N(2,2)]
   2  3     7.05661808E-01   # Re[N(2,3)]
   2  4     7.08220428E-01   # Re[N(2,4)]
   3  1    -1.34816427E-01   # Re[N(3,1)]
   3  2    -6.85918331E-02   # Re[N(3,2)]
   3  3     6.98882552E-01   # Re[N(3,3)]
   3  4    -6.99058560E-01   # Re[N(3,4)]
   4  1     2.18002204E-03   # Re[N(4,1)]
   4  2    -9.97572086E-01   # Re[N(4,2)]
   4  3    -4.18525074E-02   # Re[N(4,3)]
   4  4     5.56196724E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -5.90648906E-02   # Re[U(1,1)]
   1  2     9.98254145E-01   # Re[U(1,2)]
   2  1     9.98254145E-01   # Re[U(2,1)]
   2  2     5.90648906E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.85611499E-02   # Re[V(1,1)]
   1  2     9.96909297E-01   # Re[V(1,2)]
   2  1     9.96909297E-01   # Re[V(2,1)]
   2  2     7.85611499E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02722861E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.81518923E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.67089462E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.81094628E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38549600E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.68152839E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.26922060E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.00814918E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.21253923E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.03887983E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02728365E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.81508241E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.69811642E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.81119390E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.38549876E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.68151349E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.27017940E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00814323E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.21253483E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.03886782E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.04435665E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.78132297E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.12061125E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.87994472E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.11982467E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.61945930E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.16203247E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.38611966E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.68091716E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.85080396E-04    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.54191021E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.00642412E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.18189920E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.03539526E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38544375E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.14945863E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.28013785E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.02214460E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.91415521E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02245055E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.38544651E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.14944044E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.28013531E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.02213859E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.91612931E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02243864E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.38622317E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.14431440E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.27941810E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.02044542E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.47224169E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.01908403E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.20681622E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.90495885E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.64310426E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.90927358E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.46022749E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.00581469E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.18157736E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.66179328E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     4.14785006E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.13307355E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.27525507E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.20681775E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.90495812E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.64370777E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.90927113E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.46027791E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.00580237E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.18207285E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.66175509E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     4.21383570E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.13306631E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.27519926E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.73202380E+00   # ~b_1
#    BR                NDA      ID1      ID2
     9.17327940E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.69553935E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.25635527E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.30589686E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     2.92241106E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.50651951E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.85847270E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.15297781E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.00944888E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.59792451E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.23091949E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.28531277E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.54733509E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     6.37881650E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.46702472E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     6.39721139E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.64676904E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.46014017E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.71328457E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.09685044E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.65293250E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.33798167E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.13001176E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.27510280E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.37891518E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.46697613E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     6.43501086E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.64661948E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.46019018E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.71331740E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.12926903E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.65289664E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.33895960E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.13000419E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.27504691E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.83484342E+02   # ~t_1
#    BR                NDA      ID1      ID2
     3.70282150E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.07636150E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.02650952E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.61222426E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.12983382E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.15648059E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.39220174E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.92548245E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.14297628E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.31651763E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.26676447E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.24356308E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.06090123E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.22021614E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.21467600E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.28921738E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.76425259E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     4.02539495E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98193077E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.80692267E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.11544842E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.38468908E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.31745106E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.29305206E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     4.75095694E-04    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.48129694E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     4.58394181E-03    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     2.29210193E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     5.94068577E-03    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     2.28633699E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     6.28009072E-03    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     8.45797025E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.05856578E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.06162584E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.94040033E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     9.42462173E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.74922857E-02    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.62131956E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.59184326E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     9.74081298E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.15130739E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.25529796E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.25529796E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.96973924E-03    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     4.96973924E-03    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.24742036E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.38753319E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.45051706E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.53807634E-04    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     2.44354338E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.41300091E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.68263499E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.19079826E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     1.48972734E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     4.75536850E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     3.47942770E-03    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.25408320E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.35217662E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.89521034E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.89521034E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.05465768E+01   # ~g
#    BR                NDA      ID1      ID2
     4.87158869E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.87158869E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     5.57067090E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.89784234E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.79141821E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.08418462E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.94080988E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     5.97536077E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     5.97536077E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     4.03668634E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.03668634E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.37197543E-03   # Gamma(h0)
     1.66909701E-03   2        22        22   # BR(h0 -> photon photon)
     5.16431080E-05   2        22        23   # BR(h0 -> photon Z)
     8.52174492E-04   2        23        23   # BR(h0 -> Z Z)
     8.45128243E-03   2       -24        24   # BR(h0 -> W W)
     6.20761720E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.88344247E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.06167263E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.82191688E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.97644341E-07   2        -2         2   # BR(h0 -> Up up)
     3.83351496E-02   2        -4         4   # BR(h0 -> Charm charm)
     8.29483705E-07   2        -1         1   # BR(h0 -> Down down)
     3.00003117E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.99738108E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.30097815E+01   # Gamma(HH)
     3.82828673E-06   2        22        22   # BR(HH -> photon photon)
     1.03756450E-06   2        22        23   # BR(HH -> photon Z)
     8.57122058E-04   2        23        23   # BR(HH -> Z Z)
     1.08694805E-03   2       -24        24   # BR(HH -> W W)
     9.53305738E-04   2        21        21   # BR(HH -> gluon gluon)
     4.38233434E-11   2       -11        11   # BR(HH -> Electron electron)
     1.95059260E-06   2       -13        13   # BR(HH -> Muon muon)
     5.63257886E-04   2       -15        15   # BR(HH -> Tau tau)
     7.18112042E-11   2        -2         2   # BR(HH -> Up up)
     1.39267135E-05   2        -4         4   # BR(HH -> Charm charm)
     9.13507937E-01   2        -6         6   # BR(HH -> Top top)
     3.67871907E-09   2        -1         1   # BR(HH -> Down down)
     1.33059308E-06   2        -3         3   # BR(HH -> Strange strange)
     3.51242127E-03   2        -5         5   # BR(HH -> Bottom bottom)
     2.64879146E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     5.71094271E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.83657458E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.62057116E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.57178768E-08   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.00777163E-05   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.39216338E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.95939671E-11   2        23        36   # BR(HH -> Z A0)
     8.54465588E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.36612504E+01   # Gamma(A0)
     5.26302317E-06   2        22        22   # BR(A0 -> photon photon)
     1.70242406E-06   2        22        23   # BR(A0 -> photon Z)
     1.13879729E-03   2        21        21   # BR(A0 -> gluon gluon)
     4.08048740E-11   2       -11        11   # BR(A0 -> Electron electron)
     1.81623764E-06   2       -13        13   # BR(A0 -> Muon muon)
     5.24466972E-04   2       -15        15   # BR(A0 -> Tau tau)
     6.45421285E-11   2        -2         2   # BR(A0 -> Up up)
     1.25155231E-05   2        -4         4   # BR(A0 -> Charm charm)
     9.21248898E-01   2        -6         6   # BR(A0 -> Top top)
     3.42681291E-09   2        -1         1   # BR(A0 -> Down down)
     1.23948046E-06   2        -3         3   # BR(A0 -> Strange strange)
     3.27603553E-03   2        -5         5   # BR(A0 -> Bottom bottom)
     4.07506557E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.43219003E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.13815957E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.68065164E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.74883050E-07   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.00949600E-05   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.36834861E-05   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.17540535E-03   2        23        25   # BR(A0 -> Z h0)
     3.25422393E-42   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.54072470E+01   # Gamma(Hp)
     3.61199990E-11   2       -11        12   # BR(Hp -> Electron nu_e)
     1.54424274E-06   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.36797521E-04   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.69812369E-09   2        -1         2   # BR(Hp -> Down up)
     4.38685416E-08   2        -3         2   # BR(Hp -> Strange up)
     2.81303932E-08   2        -5         2   # BR(Hp -> Bottom up)
     5.74706378E-07   2        -1         4   # BR(Hp -> Down charm)
     1.33923830E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.95831057E-06   2        -5         4   # BR(Hp -> Bottom charm)
     5.99517407E-05   2        -1         6   # BR(Hp -> Down top)
     1.30714788E-03   2        -3         6   # BR(Hp -> Strange top)
     9.35770096E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.95215522E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.01583210E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     7.83118845E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.08592934E-03   2        24        25   # BR(Hp -> W h0)
     5.96572787E-09   2        24        35   # BR(Hp -> W HH)
     2.40725723E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.21308200E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.83382997E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.75513817E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.02856183E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.34396369E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.62958203E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.21308200E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.83382997E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.75513817E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99423222E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.76778132E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99423222E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.76778132E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.31559525E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.29845818E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    6.04734208E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.76778132E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99423222E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.54004852E-04   # BR(b -> s gamma)
    2    1.61256404E-06   # BR(b -> s mu+ mu-)
    3    3.57751270E-05   # BR(b -> s nu nu)
    4    2.63299010E-15   # BR(Bd -> e+ e-)
    5    1.12478346E-10   # BR(Bd -> mu+ mu-)
    6    2.35461143E-08   # BR(Bd -> tau+ tau-)
    7    8.86983493E-14   # BR(Bs -> e+ e-)
    8    3.78919050E-09   # BR(Bs -> mu+ mu-)
    9    8.03710997E-07   # BR(Bs -> tau+ tau-)
   10    9.67911671E-05   # BR(B_u -> tau nu)
   11    9.99815700E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.59937194E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.00067547E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.23108559E-03   # epsilon_K
   17    2.28242735E-15   # Delta(M_K)
   18    2.51668532E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.37767169E-11   # BR(K^+ -> pi^+ nu nu)
   20    6.42702091E-18   # Delta(g-2)_electron/2
   21    2.74775171E-13   # Delta(g-2)_muon/2
   22    7.77456779E-11   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.29515232E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.22437445E-01   # C7
     0305 4322   00   2    -6.55498526E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.35062654E-01   # C8
     0305 6321   00   2    -7.77910024E-04   # C8'
 03051111 4133   00   0     1.63170586E+00   # C9 e+e-
 03051111 4133   00   2     1.63510207E+00   # C9 e+e-
 03051111 4233   00   2     9.97932134E-07   # C9' e+e-
 03051111 4137   00   0    -4.45439796E+00   # C10 e+e-
 03051111 4137   00   2    -4.50416678E+00   # C10 e+e-
 03051111 4237   00   2    -5.99160115E-06   # C10' e+e-
 03051313 4133   00   0     1.63170586E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63510207E+00   # C9 mu+mu-
 03051313 4233   00   2     9.97932084E-07   # C9' mu+mu-
 03051313 4137   00   0    -4.45439796E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.50416678E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.99160110E-06   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.51610128E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.29270575E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.51610128E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.29270576E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.51610128E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.29270867E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821770E-07   # C7
     0305 4422   00   2     8.82220680E-07   # C7
     0305 4322   00   2     6.02965821E-08   # C7'
     0305 6421   00   0     3.30481862E-07   # C8
     0305 6421   00   2    -1.23038051E-07   # C8
     0305 6321   00   2     1.37802685E-08   # C8'
 03051111 4133   00   2     1.14326155E-07   # C9 e+e-
 03051111 4233   00   2     2.50591943E-08   # C9' e+e-
 03051111 4137   00   2     1.28315552E-06   # C10 e+e-
 03051111 4237   00   2    -1.60226797E-07   # C10' e+e-
 03051313 4133   00   2     1.14326148E-07   # C9 mu+mu-
 03051313 4233   00   2     2.50591943E-08   # C9' mu+mu-
 03051313 4137   00   2     1.28315553E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.60226797E-07   # C10' mu+mu-
 03051212 4137   00   2    -2.52717489E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.45822605E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.52717488E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.45822605E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.52717393E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.45822604E-08   # C11' nu_3 nu_3
