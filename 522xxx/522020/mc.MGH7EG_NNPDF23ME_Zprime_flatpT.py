from MadGraphControl.MadGraphUtils import *

#--------------------------------------------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------------------------------------------
evgenConfig.description = 'MadGraph configuration to produce a flat-pT Zprime sample, showered with Herwig7'
evgenConfig.keywords = ['Zprime','jets']
evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.contact = ['yi.yu@cern.ch']
evgenConfig.tune = "H7.1-Default"
evgenConfig.nEventsPerJob = 10000

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
nevents = 1.1 * runArgs.maxEvents if runArgs.maxEvents > 0 else 11000


#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
gridpack_mode=False

beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

settings = { 'lhe_version':'2.0', 
             'cut_decays':'F', 
             'pdlabel':"nn23lo1",
             'use_syst':"False",
             'nevents':int(nevents),
             'ebeam1': beamEnergy,
             'ebeam2': beamEnergy
            }

#---------------------------------------------------------------------------------------------------
# Generating Z' process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process_def = """
import model Zp_flat_pT
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > v1 > all all
output -f
""" 
process_dir = new_process(process_def)

#---------------------------------------------------------------------------------------------------
# Build a new run_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#--------------------------------------------------------------------------------------------------- 
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)


#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)
runArgs.inputGeneratorFile = outputDS+'.events'


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename = runArgs.inputGeneratorFile, me_pdf_order = "LO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()
