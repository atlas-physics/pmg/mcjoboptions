include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
import math 
JOName=get_physics_short()

jobConfigParts = JOName.split('_')

# specifying SUSY masses subject to new naming convention, intermediate masses determined from these two
mSquark = float(jobConfigParts[3])
mLSP = float(jobConfigParts[4].rstrip('.py'))
mCN = float(math.floor(1/2 * (mSquark + mLSP)))
mStau = float(math.floor(1/2 * (mLSP + mCN)))

#Using the five flavour scheme, should be picked up by the post-include
flavourScheme = 5


# only consider sq_L
masses['1000001'] = mSquark
masses['1000002'] = mSquark
masses['1000003'] = mSquark
masses['1000004'] = mSquark
masses['1000023'] = mCN
masses['1000024'] = mCN
masses['1000015'] = mStau
masses['1000016'] = mStau
masses['1000022'] = mLSP

decaytype = str(jobConfigParts[2])

#Changed the generation from j to jb
process = '''
import model MSSM_SLHA2
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define pb = g u c d s b u~ c~ d~ s~ b~
define jb = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define fu = u c e+ mu+ ta+
define fu~ = u~ c~ e- mu- ta-
define fd = d s ve~ vm~ vt~
define fd~ = d~ s~ ve vm vt
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susyweak = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susysl = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+
define susyv = sve svm svt
define susyv~ = sve~ svm~ svt~
generate pb pb > susylq susylq~ $ go susyweak @1
add process pb pb > susylq susylq~ jb $ go susyweak @2
add process pb pb > susylq susylq~ jb jb $ go susyweak @3
'''



# Decays
decays['6'] = """ DECAY         6     1.50833649E+00   # top decays
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
"""

decays['1000001'] = """ DECAY   1000001     1.00000000E+00   # sdown_L decays
     5.00000000E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.00000000E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
"""
decays['1000002'] = """ DECAY   1000002     1.00000000E+00   # sup_L decays
     5.00000000E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.00000000E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
"""
decays['1000003'] = """ DECAY   1000003     1.00000000E+00   # sstrange_L decays
     5.00000000E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.00000000E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
"""
decays['1000004'] = """ DECAY   1000004     1.00000000E+00   # scharm_L decays
     5.00000000E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.00000000E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
"""
decays['1000015'] = """ DECAY   1000015     3.25037203E+00   # stau_1 decays
#           BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
"""

decays['1000016'] = """ DECAY   1000016     6.07063864E+00   # snu_tauL decays
#           BR         NDA      ID1       ID2       ID3
     1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
"""
decays['1000023'] = """DECAY   1000023     9.37327589E-04   # neutralino2 decays
#           BR         NDA      ID1       ID2       ID3
     0.00000000E+00    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     0.00000000E+00    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     0.00000000E+00    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     0.00000000E+00    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     2.50000000E-01    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     2.50000000E-01    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
     0.00000000E+00    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     0.00000000E+00    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     0.00000000E+00    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     0.00000000E+00    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
     2.50000000E-01    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
     2.50000000E-01    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )
"""
decays['1000024'] = """ DECAY   1000024     7.00367294E-03   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     0.00000000E+00    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL  e+  )
     0.00000000E+00    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     5.00000000E-01    2     1000016       -15   # BR(~chi_1+ -> ~nu_tau1 tau+)
     0.00000000E+00    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     0.00000000E+00    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
     5.00000000E-01    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)
"""

njets = 2


#Add more events in the compressed reigon
if mSquark-mLSP <= 200 and '_J85' in JOName:
    evt_multiplier = 40.
elif mSquark-mLSP <= 240 and '_J85' in JOName:
    evt_multiplier = 10.


filters = []

#1tau + 1 jet filter
if '_J85' in JOName:
    filters.append("TruthJetFilter")
    evgenLog.info('Adding truth jet filter, Pt > 85 GeV')
    include("GeneratorFilters/FindJets.py")
    CreateJets(prefiltSeq, 0.4)    
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter("TruthJetFilter")
    filtSeq.TruthJetFilter.jet_pt1 = 85000.
    filtSeq.TruthJetFilter.NjetMinPt = 0.
    filtSeq.TruthJetFilter.NjetMaxEta = 2.8
    filtSeq.TruthJetFilter.Njet = 1
# truth tau filter
if "1tau" in jobConfigParts[-1]:
    filters.append("MultiElecMuTauFilter")
    evgenLog.info('Adding 1 tau filter')
    include("GeneratorFilters/MultiElecMuTauFilter.py")
    MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    MultiElecMuTauFilter.NLeptons = 1
    MultiElecMuTauFilter.MinPt = 1e8
    MultiElecMuTauFilter.MaxEta = 2.8
    MultiElecMuTauFilter.MinVisPtHadTau =  10000
    MultiElecMuTauFilter.IncludeHadTaus = 1

if filters:
   filtSeq.Expression = ' and '.join(filters)



evgenLog.info('Registered generation of squark grid '+str(runArgs.jobConfig[0]))
evgenConfig.contact  = [ "thillers@cern.ch" ]
evgenConfig.keywords += [ 'squark', 'simplifiedModel' ]
evgenConfig.description = 'squark simplified model, two-step decays via C1/N2 then stau/sneutrino, m_sq = %s GeV, m_C1 = m_N2 = %s GeV, m_stau = m_snu = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000024'],masses['1000015'],masses['1000022']) 

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# Pythia8 merging
if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
