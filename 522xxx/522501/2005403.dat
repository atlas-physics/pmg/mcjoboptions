# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:58
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.14353309E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.61303216E+03  # scale for input parameters
    1    4.56354099E+01  # M_1
    2    1.56119288E+03  # M_2
    3    1.64862268E+03  # M_3
   11    6.94754386E+02  # A_t
   12    2.17293709E+02  # A_b
   13    5.24541779E+02  # A_tau
   23    6.03041416E+02  # mu
   25    1.04251432E+00  # tan(beta)
   26    8.73903869E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.32407355E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.90087614E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.26381413E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.61303216E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.61303216E+03  # (SUSY scale)
  1  1     1.16180352E-05   # Y_u(Q)^DRbar
  2  2     5.90196188E-03   # Y_c(Q)^DRbar
  3  3     1.40355197E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.61303216E+03  # (SUSY scale)
  1  1     2.43385648E-05   # Y_d(Q)^DRbar
  2  2     4.62432731E-04   # Y_s(Q)^DRbar
  3  3     2.41362297E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.61303216E+03  # (SUSY scale)
  1  1     4.24725753E-06   # Y_e(Q)^DRbar
  2  2     8.78198152E-04   # Y_mu(Q)^DRbar
  3  3     1.47698482E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.61303216E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.94754347E+02   # A_t(Q)^DRbar
Block Ad Q=  3.61303216E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.17293711E+02   # A_b(Q)^DRbar
Block Ae Q=  3.61303216E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     5.24541786E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.61303216E+03  # soft SUSY breaking masses at Q
   1    4.56354099E+01  # M_1
   2    1.56119288E+03  # M_2
   3    1.64862268E+03  # M_3
  21   -6.99872926E+04  # M^2_(H,d)
  22    5.68830417E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.32407355E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.90087614E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.26381413E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     8.66324633E+01  # h0
        35     8.79554421E+02  # H0
        36     8.73903869E+02  # A0
        37     8.86626939E+02  # H+
   1000001     1.01183407E+04  # ~d_L
   2000001     1.00943965E+04  # ~d_R
   1000002     1.01182758E+04  # ~u_L
   2000002     1.00980321E+04  # ~u_R
   1000003     1.01183414E+04  # ~s_L
   2000003     1.00943966E+04  # ~s_R
   1000004     1.01182766E+04  # ~c_L
   2000004     1.00980335E+04  # ~c_R
   1000005     3.36318727E+03  # ~b_1
   2000005     4.31677938E+03  # ~b_2
   1000006     3.36651569E+03  # ~t_1
   2000006     3.87760005E+03  # ~t_2
   1000011     1.00203996E+04  # ~e_L-
   2000011     1.00085273E+04  # ~e_R-
   1000012     1.00199354E+04  # ~nu_eL
   1000013     1.00203996E+04  # ~mu_L-
   2000013     1.00085274E+04  # ~mu_R-
   1000014     1.00199354E+04  # ~nu_muL
   1000015     1.00085406E+04  # ~tau_1-
   2000015     1.00204065E+04  # ~tau_2-
   1000016     1.00199418E+04  # ~nu_tauL
   1000021     2.02978173E+03  # ~g
   1000022     3.94181470E+01  # ~chi_10
   1000023     6.21400833E+02  # ~chi_20
   1000025     6.24085891E+02  # ~chi_30
   1000035     1.65962396E+03  # ~chi_40
   1000024     6.18076329E+02  # ~chi_1+
   1000037     1.65958553E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -7.27036813E-01   # alpha
Block Hmix Q=  3.61303216E+03  # Higgs mixing parameters
   1    6.03041416E+02  # mu
   2    1.04251432E+00  # tan[beta](Q)
   3    2.43139268E+02  # v(Q)
   4    7.63707972E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99997316E-01   # Re[R_st(1,1)]
   1  2     2.31668495E-03   # Re[R_st(1,2)]
   2  1    -2.31668495E-03   # Re[R_st(2,1)]
   2  2    -9.99997316E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999992E-01   # Re[R_sb(1,1)]
   1  2     1.26436930E-04   # Re[R_sb(1,2)]
   2  1    -1.26436930E-04   # Re[R_sb(2,1)]
   2  2     9.99999992E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     4.04125722E-04   # Re[R_sta(1,1)]
   1  2     9.99999918E-01   # Re[R_sta(1,2)]
   2  1    -9.99999918E-01   # Re[R_sta(2,1)]
   2  2     4.04125722E-04   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.97054113E-01   # Re[N(1,1)]
   1  2     3.74780939E-03   # Re[N(1,2)]
   1  3    -5.54194142E-02   # Re[N(1,3)]
   1  4     5.28936446E-02   # Re[N(1,4)]
   2  1    -7.66509229E-02   # Re[N(2,1)]
   2  2    -7.66410555E-02   # Re[N(2,2)]
   2  3     7.03036866E-01   # Re[N(2,3)]
   2  4    -7.02844185E-01   # Re[N(2,4)]
   3  1    -1.77266303E-03   # Re[N(3,1)]
   3  2     1.12255666E-03   # Re[N(3,2)]
   3  3     7.06972848E-01   # Re[N(3,3)]
   3  4     7.07237576E-01   # Re[N(3,4)]
   4  1    -2.14216604E-03   # Re[N(4,1)]
   4  2     9.97051073E-01   # Re[N(4,2)]
   4  3     5.34532014E-02   # Re[N(4,3)]
   4  4    -5.50211230E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.53936082E-02   # Re[U(1,1)]
   1  2     9.97153852E-01   # Re[U(1,2)]
   2  1     9.97153852E-01   # Re[U(2,1)]
   2  2     7.53936082E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.76292653E-02   # Re[V(1,1)]
   1  2     9.96982295E-01   # Re[V(1,2)]
   2  1     9.96982295E-01   # Re[V(2,1)]
   2  2     7.76292653E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02853555E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.94161942E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.83059947E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
DECAY   1000011     1.37780666E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.95430981E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.39228697E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00315418E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.61150316E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.02137684E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02856607E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.94155927E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.83206212E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
DECAY   1000013     1.37780818E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.95430024E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.39282926E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.00315089E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.61149917E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.02137017E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.03716354E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.92463513E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.24417305E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.30732295E-04    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.53231393E-04    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.37823895E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.95158413E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.54543078E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.56506622E-04    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.00222707E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.60951544E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.01949999E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37768381E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.21030581E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.04693847E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.01708713E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.82902494E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01953150E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37768534E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.21029561E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.04693399E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.01708379E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.83012164E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01952490E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.37811627E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.20742147E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.04567116E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.01614250E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.13914607E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.01766462E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.39289126E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.57795881E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92377535E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.63649634E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.69489126E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.03075480E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.85796285E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     5.81871213E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.71052203E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.51835207E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.39289219E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.57795811E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92377421E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.63656703E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.69487766E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.03098201E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.85792347E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     5.89873854E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.71044817E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.51828303E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.55415265E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.90492683E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.84798118E-04    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.29977983E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.76095783E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     6.85601315E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.20085337E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.09227619E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.14496808E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.78261257E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.14606647E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.27274790E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.88026682E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.56511623E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.96324351E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.73813114E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.70193529E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.63646802E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.55763481E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.17230441E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.85051543E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.16889582E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.70714368E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.51831621E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.56525602E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.96319164E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.78345678E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.70175719E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.63653851E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.55764489E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.21206028E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.85047871E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.16933619E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.70706534E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.51824712E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.54705147E+02   # ~t_1
#    BR                NDA      ID1      ID2
     3.16995224E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.37432075E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.40239250E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.44504327E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     7.17014077E-04    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.63216493E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.16913460E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     7.56167349E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.56433576E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.97154424E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.55701910E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.57340415E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.50243452E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.14386987E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.30118538E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.50856093E-01    2     1000021         6   # BR(~t_2 -> ~g t)
DECAY   1000024     4.70099055E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.92442431E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     7.55754970E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.63684290E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.53785194E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.19398456E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.20107964E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     6.08605392E-03    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.04739918E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     2.17337291E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.86204857E-03    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     2.17658734E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     6.94875828E-03    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.37277009E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.56504571E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.41691234E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.25590779E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.08438832E-04    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     9.98270651E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.12071161E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     4.84805770E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     9.95133496E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.05882598E-03    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.75928775E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     2.64378666E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.16131201E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.16131201E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.26001073E-03    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     6.26001073E-03    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.21044962E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.61454071E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.77425534E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.17746102E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.57786060E-04    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     3.13890428E-04    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     9.25174881E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.68912666E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.72157519E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.31945655E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.31945655E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.56106009E-02   # ~g
#    BR                NDA      ID1      ID2
     1.02997459E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.04475672E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.55492578E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.55493044E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.34674389E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.83920661E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.32275435E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.21475946E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.36646155E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.14214593E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.45678099E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.45678099E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.79105195E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.79105195E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     1.75703484E-03   # Gamma(h0)
     1.36451898E-03   2        22        22   # BR(h0 -> photon photon)
     4.88360421E-05   2        23        23   # BR(h0 -> Z Z)
     2.33500820E-04   2       -24        24   # BR(h0 -> W W)
     5.50787502E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.80010922E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.02447090E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.70907955E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.99654062E-07   2        -2         2   # BR(h0 -> Up up)
     3.87184081E-02   2        -4         4   # BR(h0 -> Charm charm)
     8.42131700E-07   2        -1         1   # BR(h0 -> Down down)
     3.04574779E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.10341703E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     6.51541713E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.85781145E+01   # Gamma(HH)
     5.90688772E-06   2        22        22   # BR(HH -> photon photon)
     1.56873748E-06   2        22        23   # BR(HH -> photon Z)
     5.20670396E-04   2        23        23   # BR(HH -> Z Z)
     7.98769091E-04   2       -24        24   # BR(HH -> W W)
     1.59779259E-03   2        21        21   # BR(HH -> gluon gluon)
     9.42291084E-12   2       -11        11   # BR(HH -> Electron electron)
     4.19394476E-07   2       -13        13   # BR(HH -> Muon muon)
     1.21101538E-04   2       -15        15   # BR(HH -> Tau tau)
     8.34098018E-11   2        -2         2   # BR(HH -> Up up)
     1.61753137E-05   2        -4         4   # BR(HH -> Charm charm)
     9.70272337E-01   2        -6         6   # BR(HH -> Top top)
     8.09951748E-10   2        -1         1   # BR(HH -> Down down)
     2.92955101E-07   2        -3         3   # BR(HH -> Strange strange)
     7.67470705E-04   2        -5         5   # BR(HH -> Bottom bottom)
     8.87685952E-06   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.79541815E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.73605735E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.38250734E-10   2        23        36   # BR(HH -> Z A0)
     8.48009001E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.03677984E+01   # Gamma(A0)
     6.85167991E-06   2        22        22   # BR(A0 -> photon photon)
     1.91227930E-06   2        22        23   # BR(A0 -> photon Z)
     1.85987775E-03   2        21        21   # BR(A0 -> gluon gluon)
     8.32290851E-12   2       -11        11   # BR(A0 -> Electron electron)
     3.70434944E-07   2       -13        13   # BR(A0 -> Muon muon)
     1.06965866E-04   2       -15        15   # BR(A0 -> Tau tau)
     7.16645078E-11   2        -2         2   # BR(A0 -> Up up)
     1.38956338E-05   2        -4         4   # BR(A0 -> Charm charm)
     9.80274283E-01   2        -6         6   # BR(A0 -> Top top)
     7.16145154E-10   2        -1         1   # BR(A0 -> Down down)
     2.59027330E-07   2        -3         3   # BR(A0 -> Strange strange)
     6.81216058E-04   2        -5         5   # BR(A0 -> Bottom bottom)
     6.01201473E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.56723718E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.17107908E-05   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     7.29083739E-04   2        23        25   # BR(A0 -> Z h0)
     8.14030134E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.96088653E+01   # Gamma(Hp)
     6.47555948E-12   2       -11        12   # BR(Hp -> Electron nu_e)
     2.76850385E-07   2       -13        14   # BR(Hp -> Muon nu_mu)
     7.83084597E-05   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.54494887E-10   2        -1         2   # BR(Hp -> Down up)
     8.04334296E-09   2        -3         2   # BR(Hp -> Strange up)
     5.15328837E-09   2        -5         2   # BR(Hp -> Bottom up)
     6.20076501E-07   2        -1         4   # BR(Hp -> Down charm)
     1.36363009E-05   2        -3         4   # BR(Hp -> Strange charm)
     7.42211222E-07   2        -5         4   # BR(Hp -> Bottom charm)
     6.31782501E-05   2        -1         6   # BR(Hp -> Down top)
     1.37749541E-03   2        -3         6   # BR(Hp -> Strange top)
     9.86394189E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.15429246E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.28599280E-04   2        24        25   # BR(Hp -> W h0)
     8.07823640E-10   2        24        35   # BR(Hp -> W HH)
     1.52265961E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.21903842E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.16493227E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.08683611E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.07185643E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.48245504E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    9.20101930E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.21903842E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.16493227E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.08683611E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.98590686E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.40931419E-03        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.98590686E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.40931419E-03        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.37436060E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.43357900E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.59385489E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.40931419E-03        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.98590686E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.63424698E-04   # BR(b -> s gamma)
    2    1.66515218E-06   # BR(b -> s mu+ mu-)
    3    3.68665191E-05   # BR(b -> s nu nu)
    4    2.74880267E-15   # BR(Bd -> e+ e-)
    5    1.17425256E-10   # BR(Bd -> mu+ mu-)
    6    2.45538625E-08   # BR(Bd -> tau+ tau-)
    7    9.28530957E-14   # BR(Bs -> e+ e-)
    8    3.96664833E-09   # BR(Bs -> mu+ mu-)
    9    8.39390229E-07   # BR(Bs -> tau+ tau-)
   10    9.67987049E-05   # BR(B_u -> tau nu)
   11    9.99893562E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.95733416E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.12825150E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.37856363E-03   # epsilon_K
   17    2.28396842E-15   # Delta(M_K)
   18    2.59348838E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.55968920E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.59721606E-18   # Delta(g-2)_electron/2
   21    6.82859441E-14   # Delta(g-2)_muon/2
   22    1.93079997E-11   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -3.50413329E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.31816420E-01   # C7
     0305 4322   00   2    -8.12287913E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.44431923E-01   # C8
     0305 6321   00   2    -9.43386390E-04   # C8'
 03051111 4133   00   0     1.62370380E+00   # C9 e+e-
 03051111 4133   00   2     1.63373391E+00   # C9 e+e-
 03051111 4233   00   2     8.26642493E-07   # C9' e+e-
 03051111 4137   00   0    -4.44639590E+00   # C10 e+e-
 03051111 4137   00   2    -4.60214402E+00   # C10 e+e-
 03051111 4237   00   2    -5.03946963E-06   # C10' e+e-
 03051313 4133   00   0     1.62370380E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63373391E+00   # C9 mu+mu-
 03051313 4233   00   2     8.26642478E-07   # C9' mu+mu-
 03051313 4137   00   0    -4.44639590E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.60214402E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.03946962E-06   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.53905339E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.08919709E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.53905339E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.08919709E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.53905339E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.08919800E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819630E-07   # C7
     0305 4422   00   2     1.04723556E-06   # C7
     0305 4322   00   2     2.36410302E-08   # C7'
     0305 6421   00   0     3.30480029E-07   # C8
     0305 6421   00   2    -7.98282680E-07   # C8
     0305 6321   00   2    -2.53652987E-08   # C8'
 03051111 4133   00   2     6.08428764E-07   # C9 e+e-
 03051111 4233   00   2     1.85181300E-08   # C9' e+e-
 03051111 4137   00   2     3.39294726E-07   # C10 e+e-
 03051111 4237   00   2    -1.17940368E-07   # C10' e+e-
 03051313 4133   00   2     6.08428758E-07   # C9 mu+mu-
 03051313 4233   00   2     1.85181300E-08   # C9' mu+mu-
 03051313 4137   00   2     3.39294732E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.17940368E-07   # C10' mu+mu-
 03051212 4137   00   2    -3.72880416E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.54978277E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.72880414E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.54978277E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.72879712E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.54978276E-08   # C11' nu_3 nu_3
