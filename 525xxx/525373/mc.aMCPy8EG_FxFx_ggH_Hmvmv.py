#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators   += ["MadGraph", "Pythia8"]
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.process     = "gg->H->WW*->mvmv"
evgenConfig.description = "Madgraph+PYTHIA8+EVTGEN, ggH H->WW->mvmv"
evgenConfig.contact     = [ 'jiayi.chen@cern.ch' ]
evgenConfig.nEventsPerJob = 2000
evgenConfig.inputFilesPerJob = 3


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = 20.0",
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 8.0",
                            "JetMatching:nJetMax          = 2",
                            "JetMatching:jetAlgorithm     = 2",
                            "JetMatching:slowJetPower     = 1", 
                            "JetMatching:nQmatch          = 5", 
                            "JetMatching:eTjetMin         = 20.0", 
                            "SpaceShower:rapidityOrder    = off", # FxFx + A14 prescription
                            "SpaceShower:pTmaxFudge       = 1.0", # FxFx + A14 prescription
]

genSeq.Pythia8.UserHooks += [ 'JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS    = True

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiMuonFilter("Multi1MuonFilter")
filtSeq += MultiMuonFilter("Multi2MuonFilter")

Multi1MuonFilter = filtSeq.Multi1MuonFilter
Multi1MuonFilter.Ptcut      = 10000.
Multi1MuonFilter.Etacut     = 3.0
Multi1MuonFilter.NMuons = 1

Multi2MuonFilter = filtSeq.Multi2MuonFilter
Multi2MuonFilter.Ptcut      = 7000.
Multi2MuonFilter.Etacut     = 3.0
Multi2MuonFilter.NMuons = 2

filtSeq.Expression = "(Multi1MuonFilter) and (Multi2MuonFilter)"
