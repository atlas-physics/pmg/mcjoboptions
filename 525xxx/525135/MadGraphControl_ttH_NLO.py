from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphParamHelpers import set_top_params

evgenConfig.nEventsPerJob = 1000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = 2.5* nevents # account for filter efficiency

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
if process=="ttH_semilep":
    mgproc="generate p p > t t~ h [QCD]"
    name = "ttbarH_semilep"
    process="pp>tt~h"
    topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''

elif process=="ttH_dilep":
    mgproc="generate p p > t t~ h [QCD]"
    name='ttbarH_dilep'
    process="pp>tt~h"
    topdecay='''decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- >  l- vl~'''

else:
    raise RuntimeError("process not found")

process_string="""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define q = u c d s b t
define q~ = u~ c~ d~ s~ b~ t~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
"""+mgproc+"""
output -f
"""

#Fetch default LO run_card.dat and set parameters
extras = {
          'parton_shower'  :'PYTHIA8',
          'nevents':nevents,
          'bwcutoff'       : 50.}


process_dir = new_process(process_string)
modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

set_top_params(process_dir,mTop=172.5,FourFS=True)
print_cards()

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
fMadSpinCard = open(madspin_card,'w')
fMadSpinCard.write('''set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                   runArgs=runArgs,
                   lhe_version=3,saveProcDir=False)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------
check_reset_proc_number(opts)

#evgenConfig.keywords += keyword
evgenConfig.keywords+=['ttHiggs','ttbar','Higgs']
evgenConfig.contact = ["neelam.kumari@cern.ch"]
evgenConfig.generators += ["Pythia8"]
evgenConfig.description = 'ttH semiLep production at NLO with MadGraph5 and Pythia8'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

if process=="ttH_semilep":
  include('GeneratorFilters/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
  filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0
