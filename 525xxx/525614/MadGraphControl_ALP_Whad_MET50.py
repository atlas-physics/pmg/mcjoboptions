from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment

mode=0

ickkw_value=0
#scalefact=1.0
alpsfact=1

safety = 2.2
nevents=int(runArgs.maxEvents*safety)

param_card_extras = {
      "ALPPARS": { 'CWtil':CWtil, 'fa':fa, 'CBtil': -0.28702319343*CWtil},
      "MASS": { 'Ma':Ma }
      }

alp_process = """
import model sm
import model ALP_invisible_linear_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define W = W+ W-
generate p p > ax W , W > j j
output -f
"""  

process_dir = new_process(alp_process)

#Missing Et Filter
evgenLog.info('MET ' + str(METFilter/1000.)  + ' filter is applied')
print 'evgenlog.info'
include('GeneratorFilters/MissingEtFilter.py')
print 'included the filter py'
filtSeq.MissingEtFilter.METCut = METFilter
filtSeq.Expression = "MissingEtFilter"

#process_dir = new_process()

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'  :'3.0',
#          'pdlabel'      :"'nn23lo1'",
#          'lhaid'        :247000,
          'ickkw'        :ickkw_value,
#          'maxjetflavor' :maxjetflavor,
#          'asrwgtflavor' :maxjetflavor,
#          'ptj'          :ptj,
          'drjj'         :0.0,
          'etaj'         :5,
          'etab'         :5,
          'ktdurham'     :ktdurham,
#          'dparameter'   :dparameter,
#          'bwcutoff'     :bwcutoff,
#          'scalefact'    :scalefact,
          'pt_min_pdg'   :pt_min_pdg,
          'alpsfact'     :alpsfact,
          'nevents'      :nevents}

print "MAX EVENTS: " + str(runArgs.maxEvents)
print "NEVENTS:  "   +str(nevents)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
modify_param_card(process_dir=process_dir,params=param_card_extras)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 
  

############################

evgenConfig.description = 'ALP linear with vector W'
evgenConfig.keywords+=['W','exotic','BSM']
evgenConfig.contact = ["Danijela Bogavac <danijela.bogavac@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

bonus_file = open('pdg_extras.dat','w')
bonus_file.write('51\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
