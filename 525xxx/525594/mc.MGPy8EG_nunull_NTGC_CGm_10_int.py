import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
#

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

#############################################################################################
### For simple processes only this part and the metadata at the bottom need to be changed

# create dictionary of processes this JO can create
definitions="""import model NTGC_all
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
generate p p > ve ve~ mu+ mu- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
add process p p > ve ve~ ta+ ta- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
add process p p > vm vm~ e+ e- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
add process p p > vm vm~ ta+ ta- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
add process p p > vt vt~ e+ e- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
add process p p > vt vt~ mu+ mu- GP==0 GM^2==1 BTW==0 BW==0 BB==0 WW==0
"""

evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

# define cuts and other run card settings
settings={'dynamical_scale_choice':'3', # make sure to explicitly set a scale, otherwise it might end up being different for interference terms
        # see here for the scales: https://answers.launchpad.net/mg5amcnlo/+faq/2014
        'nevents': int(nevents),
        'event_norm':'average',
        'lhe_version':"3.0",
        'ptl':"5",
        'drll':"0.0",
        'mmll':"40",
        'misset':"10",
        'etal':'2.8',
        'bwcutoff'    :"15",
        'maxjetflavor': '5',
        'asrwgtflavor':"5",
        'sde_strategy': '2',
        'hard_survey': '3'
}
#############################################################################################

process=definitions+'output -f'

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
param_dict = {
    'DIM8':{'cg' : '0.', 'cgm' : '10.', 'cbtwl4' : '0.', 'cbwl4' : '0.', 'cbbl4' : '0.', 'cwwl4' : '0.'},
}
modify_param_card(process_dir=process_dir,params=param_dict)

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

# Helper for resetting process number
check_reset_proc_number(opts)

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#############################################################################################
# add meta data
evgenConfig.description = 'MadGraph ZZ->nunull with CGm=10 int term in full Celine+Ellis nTGC formalism'
evgenConfig.keywords+=['SM','Z','diboson','QCD']
evgenConfig.contact = ['Evgeny Soldatov <Evgeny.Soldatov@cern.ch>']
#############################################################################################
