import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short 


phys_short = get_physics_short() # looks like: MGPy8_vlqvll2hdm_singlet_myy_H_750_e4_500
model_type = phys_short.split('_')[2] # singlet (ne = 5000017, 5000018) or doublet (nl = 6000017, 6000018)
lepton_channel = phys_short.split('_')[3][0] # m or e, muon or electron channel
final_state_parts = phys_short.split('_')[3][1:] # yy or bb, final state particles

extracted_mH = phys_short.split('_')[5]
extracted_e4 = phys_short.split('_')[7]


if(final_state_parts == "yy"): final_state_parts = "a a"
elif(final_state_parts == "bb"): final_state_parts = "b b~"
else: print("------> final_state_parts not recognized:", final_state_parts)

if(model_type == "singlet"): model_type = "e"
elif(model_type == "doublet"): model_type = "l"
else: print("------> model_type not recognized:", model_type)

if(lepton_channel == "e"): lepton_channel = "e"
elif(lepton_channel == "m"): lepton_channel = "mu"
elif(lepton_channel == "t"): lepton_channel = "ta"
else: print("------> lepton_channel not recognized:", lepton_channel)

print("------> final_state_parts:", final_state_parts,
  "\n------> model_type:", model_type,
  "\n------> lepton_channel:", lepton_channel,
  "\n------> extracted_mH:", extracted_mH,
  "\n------> extracted_e4:", extracted_e4)


# To load model with restrict (must have restrict_ prefix. I.e restrict_myName.dat): 
# import model vlqvll2hdm_full_UFO-myName --modelname 

# Singlet: 5000017, 5000018
    # generate p p > h02 > ne+ mu-, (ne+ > h01 mu+, (h01 > a a))
    # add process p p > h02 > ne- mu+, (ne- > h01 mu-, (h01 > a a))
# Doublet: 6000017, 6000018
    # generate p p > h02 > nl+ mu-, (nl+ > h01 mu+, (h01 > a a))
    # add process p p > h02 > nl- mu+, (nl- > h01 mu-, (h01 > a a))

process = f"""
import model vlqvll2hdm_full_UFO --modelname 
define p = g u c b d s u~ c~ d~ s~ b~
define j = g u c b d s u~ c~ d~ s~ b~
generate p p > h02 > n{model_type}+ {lepton_channel}- QED<=2, (n{model_type}+ > h01 {lepton_channel}+, (h01 > {final_state_parts}))                
add process p p > h02 > n{model_type}- {lepton_channel}+ QED<=2, (n{model_type}- > h01 {lepton_channel}-, (h01 > {final_state_parts}))                 
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = {'nevents'    :int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


params={}
# Some variables set to same value to maintain model consistency and reduce MG warnings
params['MASS'] = { 
  '5000017' : extracted_e4, # Mne 
  '5000018' : extracted_e4, # Mnnv 
  '6000017' : extracted_e4, # Mnl 
  '6000018' : extracted_e4, # Mnv 
  '35' : extracted_mH, # MH02 
  '37' : extracted_mH  # MH 
}

modify_param_card(process_dir=process_dir,params=params)
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)





#### Shower           
evgenConfig.description = 'MadGraph_VLL'
evgenConfig.keywords+=['bsm', 'exotic','Higgs', 'heavyBoson', 'diphoton']
evgenConfig.contact = ['Casey Dominik Bellgraph <casey.dominik.bellgraph@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
