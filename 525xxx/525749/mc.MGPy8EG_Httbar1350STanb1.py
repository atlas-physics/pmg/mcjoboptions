evgenConfig.description = "A/H->ttbar, interference study"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["BSMHiggs", "ttbar"]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 5000
evgenConfig.contact  = ["katharina.behr@cern.ch", "yizhou.cai@cern.ch","scalvet@clermont.in2p3.fr"]

### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

### Event filter
include("GeneratorFilters/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
