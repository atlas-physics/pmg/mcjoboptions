#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators   += ["MadGraph", "Pythia8"]
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.process     = "gg->H->WW*->evev"
evgenConfig.description = "MadGraph+PYTHIA8+EVTGEN, ggH H->WW->evev"
evgenConfig.contact     = [ 'jiayi.chen@cern.ch' ]
evgenConfig.nEventsPerJob = 2000
evgenConfig.inputFilesPerJob = 3

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = 20.0",
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 8.0",
                            "JetMatching:nJetMax          = 2",
                            "JetMatching:jetAlgorithm     = 2",
                            "JetMatching:slowJetPower     = 1", 
                            "JetMatching:nQmatch          = 5", 
                            "JetMatching:eTjetMin         = 20.0", 
                            "SpaceShower:rapidityOrder    = off", # FxFx + A14 prescription
                            "SpaceShower:pTmaxFudge       = 1.0", # FxFx + A14 prescription
]

genSeq.Pythia8.UserHooks += [ 'JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS    = True

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiElectronFilter("Multi1ElectronFilter")
filtSeq += MultiElectronFilter("Multi2ElectronFilter")

Multi1ElectronFilter = filtSeq.Multi1ElectronFilter
Multi1ElectronFilter.Ptcut      = 10000.
Multi1ElectronFilter.Etacut     = 3.0
Multi1ElectronFilter.NElectrons = 1

Multi2ElectronFilter = filtSeq.Multi2ElectronFilter
Multi2ElectronFilter.Ptcut      = 7000.
Multi2ElectronFilter.Etacut     = 3.0
Multi2ElectronFilter.NElectrons = 2

filtSeq.Expression = "(Multi1ElectronFilter) and (Multi2ElectronFilter)"
