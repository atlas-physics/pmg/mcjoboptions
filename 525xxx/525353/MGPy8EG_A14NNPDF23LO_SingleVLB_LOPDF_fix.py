# https://gitlab.cern.ch/avroy/MG_Reweight_testinATLAS2/-/blob/new_MCWorkFlow_Updated/MGPy8EG_A14NNPDF23LO_SingleVLQ_LOPDF.py
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraphUtils
import re
import subprocess
import sys

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':263400,
    # 'pdf_variations':[263400],
    # 'alternative_pdfs':[262400,13202],
    # 'scale_variations':[0.5,1.,2.],
    'use_syst':False,
}


# event configuration
#evgenConfig.nEventsPerJob = 10000

vlq_ids = {'X':"6000005", 
           'T':"6000006", 
           'B':"6000007", 
           'Y':"6000008"}
needs_gridpack = ["ZTZt"]
if not hasattr(runArgs, 'outputTXTFile'):
    runArgs.outputTXTFile = 'tmp_final_events'
print(runArgs)

# get additional python modules
get_vlqcoupling = subprocess.Popen(['get_files', '-jo', 'VLQCouplingCalculator.py'])
if get_vlqcoupling.wait():
    print("Could not copy VLQCouplingCalculator.py")
    sys.exit(2)

get_lhehacker = subprocess.Popen(['get_files', '-jo', 'lhe_hacker.py'])
if get_lhehacker.wait():
    print("Could not copy lhe_hacker.py")
    sys.exit(2)

from VLQCouplingCalculator import *
from lhe_hacker import *

#### Some Variables
MAX_TRIAL = 2     ## Maximum number of trials allowed for failures in event generation or reweighting
SAFE_FACTOR = 5.0  ## Number of events generated = SAFE_FACTOR * max_events
runName='run_01'   ## Run name for event generation

#### Process Descriptions with Full VLQ Decay Chain
#### Implements complete decay chain of immediate daughter particles of the VLQs
#### Higgs decay is not implemented here, taken care of in Pythia


all_VLQ_processes_fulldecay = {
    'WXWt':'''add process p p > j x t~ / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (t~ > ferm ferm b~), (x > w+ t, w+ > ferm ferm, t > ferm ferm b)
              add process p p > j x~ t / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (t > ferm ferm b), (x~ > w- t~, w- > ferm ferm, t~ > ferm ferm b~)''',
    'WTWb':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > WW bb, WW > ferm ferm)',
    'WTZt':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > z tt, z > ferm ferm, tt > ferm ferm bb)',
    'WTHt':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > h tt, tt > ferm ferm bb)',
    'ZTWb':'''add process p p > j TPTP tt / tp tp~ p b b~ y y~ bp bp~ x x~ w+ w- h a, (tt > ferm ferm bb), (TPTP > WW bb, WW > ferm ferm)''',
    'ZTZt':'''generate p p > j z t t~ / p y y~ bp bp~ x x~ w+ w- h a VLQ=2 QED=1 QCD=1, (t~ > ferm ferm b~), (z > ferm ferm), (t > ferm ferm b)''',
    'ZTHt':'''add process p p > j h t t~ /p y y~ bp bp~ x x~ w+ w- h a VLQ=2 QED=1 QCD=1, (t~ > ferm ferm b~), (t > ferm ferm b)''',
    'WBWt':'''add process p p > j bp t~ / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (t~ > ferm ferm b~), (bp > w- t, w- > ferm ferm, t > ferm ferm b)
              add process p p > j bp~ t / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (t > ferm ferm b), (bp~ > w+ t~, w+ > ferm ferm, t~ > ferm ferm b~)''',
    'WBZb':'''add process p p > j BPBP tt / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (tt > ferm ferm bb), (BPBP > z bb, z > ferm ferm)''',
    'WBHb': '''generate  p p > j bb h tt /tp tp~ p y y~ x x~ z h a VLQ=2 QED=1 QCD=1, tt > ferm ferm bb''',
    'ZBWt':'''add process p p > j bp b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp > w- t, w- > ferm ferm, t > ferm ferm b)
              add process p p > j bp~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp~ > w+ t~, w+ > ferm ferm, t~ > ferm ferm b~)''',
    'ZBZb':'''add process p p > j bp b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp > z b, z > ferm ferm)
              add process p p > j bp~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp~ > z b~, z > ferm ferm)''',
    'ZBHb':'''generate  p p > j b~ h b /tp tp~ p t t~ y y~ x x~ w+ w- h a VLQ==2 QED=1''',
    'WYWb':'''add process p p > j y b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (y > w- b, w- > ferm ferm)
              add process p p > j y~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (y~ > w+  b~, w+ > ferm ferm)''',
}


#### Process Descriptions with Min VLQ Decay Chain
#### Implements decay of VLQs to immediate daughters only

all_VLQ_processes_mindecay = {
    'WXWt':'''add process p p > j x t~ / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (x > w+ t)
              add process p p > j x~ t / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (x~ > w- t~)''',
    'WTWb':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > WW bb)',
    'WTZt':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > z tt)',
    'WTHt':'add process p p > j TPTP bb / tp tp~ p t t~ y y~ bp bp~ x x~ z h a, (TPTP > h tt)',
    'ZTWb':'''add process p p > j TPTP tt / tp tp~ p b b~ y y~ bp bp~ x x~ w+ w- h a, (TPTP > WW bb)''',
    'ZTZt':'''generate p p > j z t t~ / p y y~ bp bp~ x x~ w+ w- h a VLQ=2 QED=1 QCD=1''',
    'ZTHt':'''add process p p > j h t t~ / p y y~ bp bp~ x x~ w+ w- h a VLQ=2 QED=1 QCD=1''',
    'WBWt':'''add process p p > j bp t~ / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (bp > w- t)
              add process p p > j bp~ t / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (bp~ > w+ t~)''',
    'WBZb':'''add process p p > j BPBP tt / tp tp~ p b b~ y y~ bp bp~ x x~ z h a, (BPBP > z bb)''',
    'WBHb': '''generate  p p > j bb h tt /tp tp~ p y y~ x x~ z h a VLQ=2 QED=1 QCD=1''',
    'ZBWt':'''add process p p > j bp b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp > w- t)
              add process p p > j bp~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp~ > w+ t~)''',
    'ZBZb':'''add process p p > j bp b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp > z b)
              add process p p > j bp~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (bp~ > z b~)''',
    'WYWb':'''add process p p > j y b~ / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (y > w- b)
              add process p p > j y~ b / tp tp~ p t t~ y y~ bp bp~ x x~ w+ w- h a, (y~ > w+ b~)''',
}

#### Additional parameters to build the run card
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

extras = { 'nevents': evgenConfig.nEventsPerJob * SAFE_FACTOR,
           'iseed': str(runArgs.randomSeed),
           'xqcut': "0.",
           'lhe_version'   : '3.0',
           'cut_decays'    : 'F',
           'bwcutoff'      : '10000',
           'event_norm'    : 'average',
           'drjj'          :  -1.0,
           'drll'          :  -1.0,
           'draa'          :  -1.0,
           'draj'          :  -1.0,
           'drjl'          :  -1.0,
           'dral'          :  -1.0,
           'etal'          :  -1.0,
           'etaj'          :  -1.0,
           'etaa'          :  -1.0,
}


#### Find the process details from top level JobOption

def findprocdetails():

    '''
    The Top level JO should have a name of the following form:

    mc.MGPy8EG_${PROC}${MASS}${CHIRALITY}${COUPLING}${FLAGS}.py

    PROC: Single VLQ process in the VQAq form (e.g. WTHt, ZBHb)
    MASS: Pole mass of VLQ in GeV
    CHIRALITY: LH or RH
    COUPLING: Value of Kappa*100, formatted with preceeding zeros if needed. (e.g. '035' represents kappa = 0.35
    FLAGS: Other essential flags
           _sigonly -> allows event generation for particles only
           _sigbaronly -> allows event generation for anti-particles only
           _norwt -> Reweighting is not applied
    '''

    THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
    jobname = [f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
    for proc in all_VLQ_processes_fulldecay.keys():
        if proc in jobname:
            process = proc
            break
    ProdMode = process[0]
    VLQMode = process[1]
    DecayMode = process[2]

    if 'LH' in jobname: chirality = 'LH'
    elif 'RH' in jobname: chirality = 'RH'

    Mass = int(re.findall(r'\d+',re.findall(r'\d+' + chirality,jobname)[0])[0])*1.0
    Kappa = int(re.findall(r'\d+', jobname)[-1])*0.01

    if '_sigbaronly' in jobname: doSig, doSigbar = False, True
    elif '_sigonly'  in jobname: doSig, doSigbar = True,  False
    else: doSig, doSigbar = True, True

    if '_norwt' in jobname: dorwt = False
    else: dorwt = True
    return VLQMode, ProdMode, DecayMode, process, chirality, Mass, Kappa, doSig, doSigbar, dorwt


## Creates the process strings necessary for creating the process_dir. fcardmode = fulldecay or mindecay

def processmaker(processmode='fulldecay'):
    if processmode == 'mindecay':
        procmap_to_use = all_VLQ_processes_mindecay
    else:
        procmap_to_use = all_VLQ_processes_fulldecay

    this_procs = procmap_to_use[runArgs.vlqprocess].split('\n')

    if len(this_procs) > 1 and not runArgs.dosigbar:
        proc_to_use = this_procs[0].strip() + '\n'
    elif len(this_procs) > 1 and not runArgs.dosig:
        proc_to_use = this_procs[1].strip() + '\n'
    else:
        proc_to_use = ""
        for ii in range(len(this_procs)): proc_to_use += this_procs[ii].strip() + '\n'
    process = "set zerowidth_tchannel False\n"
    process += "import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/VLQ_v4_4FNS_UFO-3rd\n"
    process += "define p = g u c d s u~ c~ d~ s~\n"
    process += "define j = g u c d s u~ c~ d~ s~\n"
    process += "define bb = b b~\n"
    process += "define WW = w+ w-\n"
    process += "define tt = t t~\n"
    process += "define ferm = ve vm vt ve~ vm~ vt~ mu- ta- e- mu+ ta+ e+ u c d s b u~ c~ d~ s~ b~\n"
    process += "define TPTP = tp tp~\n"
    process += "define BPBP = bp bp~\n"
    process += "define XX = x x~\n"
    process += "define YY = y y~\n"
    process += proc_to_use
    process += "output -f"
    return process

#### Creates the param card dictionary based on the process details
#### Currently only assumes only third generation couplings

def paramdictmaker():
    chiralityIndex = runArgs.chirality.replace('H','')
    all_blocks = ["k"+a+b+c for a in ["t","b"] for b in ["l","r"] for c in ["w", "h", "z"]] + ["k"+a+b+"w" for a in ["y","x"] for b in ["l","r"]]
    all_vars = ["K"+a+b+c+d for a in ["T","B"] for b in ["L","R"] for c in ["w", "h", "z"] for d in ["1","2","3"]] + ["K"+a+b+d for a in ["Y","X"] for b in ["L","R"] for d in ["1","2","3"]]

    paramdict = {}
    for block in ['mass', 'decay'] + all_blocks:
        paramdict[block] = {}

    
    for vlqmode in vlq_ids.keys():
        paramdict['mass'][vlq_ids[vlqmode]] = str(runArgs.mass)
        paramdict['decay'][vlq_ids[vlqmode]] = str(runArgs.gamma)

    if runArgs.vlqmode in ['X', 'Y']:
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'w']['K' + runArgs.vlqmode + chiralityIndex + '3'] = str(runArgs.kw)
    else:
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'w']['K' + runArgs.vlqmode + chiralityIndex + 'w3'] = str(runArgs.kw)
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'z']['K' + runArgs.vlqmode + chiralityIndex + 'z3'] = str(runArgs.kz)
        paramdict['k'+runArgs.vlqmode.lower()+chiralityIndex.lower()+'h']['K' + runArgs.vlqmode + chiralityIndex + 'h3'] = str(runArgs.kh)

    return paramdict


def Vars():
    chiralityIndex = runArgs.chirality.replace('H','')
    all_vars = ["K"+a+b+c+d for a in ["T","B"] for b in ["L","R"] for c in ["w", "h", "z"] for d in ["1","2","3"]] + ["K"+a+b+d for a in ["Y","X"] for b in ["L","R"] for d in ["1","2","3"]]
    if runArgs.vlqmode in ['X', 'Y']:
        vars_to_change = ['M' + runArgs.vlqmode,
                          'W' + runArgs.vlqmode,
                          'K' + runArgs.vlqmode + chiralityIndex + '3']
    else:
        vars_to_change = ['M' + runArgs.vlqmode + 'P',
                          'W' + runArgs.vlqmode + 'P',
                          'K' + runArgs.vlqmode + chiralityIndex + 'w3',
                          'K' + runArgs.vlqmode + chiralityIndex + 'z3',
                          'K' + runArgs.vlqmode + chiralityIndex + 'h3']
    return  vars_to_change

#### makes a reweight card based for an input choice of mass and coupling grid

def rewtcardmaker(ms, Ks, process_dir):
    tagnames = []
    launch_line = "launch --rwgt_name="
    f = open(process_dir + "/Cards/reweight_card.dat", "w")
    for m in ms:
        for K in Ks:
            tagname = ('M' + str(int(m/100)) + 'K{0:03d}').format(int(K*100))
            tagnames.append(tagname)
            print tagname
            c = VLQCouplingCalculator(float(m), runArgs.vlqmode)
            if runArgs.vlqmode in ['X', 'Y']:
                c.setKappaxi(K, 1.0, 0.0)
            else:
                c.setKappaxi(K, 0.5, 0.25)
            kappas = c.getKappas()
            gamma = c.getGamma()
            Modified_Vars = [float(m), gamma, kappas[0], kappas[1], kappas[2]]
            print K, gamma
            f.write(launch_line + tagname + '\n')
            for vlqmode in vlq_ids.keys():
                f.write('\tset MASS ' + vlq_ids[vlqmode] + ' ' + str(Modified_Vars[0]) + '\n')
                f.write('\tset DECAY ' + vlq_ids[vlqmode] + ' ' + str(Modified_Vars[1]) + '\n')
            for ii in range(len(paramlist)):
                if str(paramlist[ii])[0] not in ['M','W']:
                    f.write('\tset ' + str(paramlist[ii]) + ' ' + str(Modified_Vars[ii]) + '\n')
            f.write('\n\n')
    f.flush()
    f.close()
    return tagnames

runArgs.vlqmode, runArgs.prodmode, runArgs.decaymode, \
runArgs.vlqprocess, runArgs.chirality, runArgs.mass, \
runArgs.kappa, runArgs.dosig, runArgs.dosigbar, runArgs.dorwt = findprocdetails()

if runArgs.vlqprocess in needs_gridpack:
    grid_pack = True
else:
    grid_pack = False


M_grid = [runArgs.mass-100., runArgs.mass] ## Mass grid for reweighting
K_grid = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6] ## Kappa grid for reweighting
me_exec = os.environ['MADPATH'] + '/bin/mg5_aMC' ## MadGraph executable

if not os.access(me_exec, os.R_OK):
    print ("mg5_aMC not located. Aborting!!")
    sys.exit(2)

c = VLQCouplingCalculator(runArgs.mass, runArgs.vlqmode)

if runArgs.vlqmode in ['X', 'Y']:
    c.setKappaxi(runArgs.kappa, 1.0, 0.0)
else:
    c.setKappaxi(runArgs.kappa, 0.5, 0.25)

[runArgs.kw, runArgs.kz, runArgs.kh, ks] = c.getKappas()
runArgs.gamma = c.getGamma()

print ("VLQ:   ", runArgs.vlqmode)
print ("Mass:  ", runArgs.mass)
print ("Kappa: ", runArgs.kappa)
print ("Process:  ", runArgs.vlqprocess)


#### Make the proc cards for full decay and min decay


process_fulldecay = processmaker('fulldecay')

paramlist = Vars()
params = paramdictmaker()
#### Process generation for full decay chain

process_dir_fullDecay = new_process(process_fulldecay)
if grid_pack and not is_gen_from_gridpack():
    modify_config_card(process_dir=process_dir_fullDecay,settings={'cluster_type':'condor'})
modify_run_card(process_dir=process_dir_fullDecay, settings=extras)
modify_param_card(process_dir=process_dir_fullDecay, params=params)
MadGraphControl.MadGraphUtils.MADGRAPH_RUN_NAME = runName + "_fullDecay"

if runArgs.vlqprocess not in all_VLQ_processes_mindecay.keys() and runArgs.dorwt:
    tagnames = rewtcardmaker(M_grid, K_grid, process_dir_fullDecay)
    
generate(process_dir=process_dir_fullDecay, runArgs=runArgs, grid_pack=grid_pack)

if grid_pack:
    try:
        _runName_ = [ f for f in os.listdir(process_dir_fullDecay + '/Events/') if f.startswith("GridRun")][0]
    except:
        print ("ERROR: Event Generation with full decay chain failed. Aborting!")
        sys.exit(2)
    full_decay_events = process_dir_fullDecay + '/Events/' + _runName_ + '/unweighted_events.lhe'
else:
    full_decay_events = process_dir_fullDecay + '/Events/' + runName + '_fullDecay/unweighted_events.lhe'

if os.path.exists(full_decay_events + '.gz') == False and os.path.exists(full_decay_events) == False:
    print ("ERROR: Event Generation with full decay chain failed. Aborting!")
    sys.exit(2)

arrange_output(process_dir=process_dir_fullDecay, runArgs=runArgs, lhe_version=3, saveProcDir=True)

#### Start building the reweighting scenario

if runArgs.vlqprocess in all_VLQ_processes_mindecay and runArgs.dorwt:
    print "Reweighting is enabled\n\n\n"
    if grid_pack:
        print "Since Gridpack is allowed, we need to get rid of the gridpack to allow on-the-fly production of minDecay"
        if is_gen_from_gridpack():
            print "Renaming gridpack!"
            process_dir_fullDecay = "PROC_FullDecay"
            subprocess.call("mv " + MADGRAPH_GRIDPACK_LOCATION + " " + process_dir_fullDecay, shell = True)
            full_decay_events = full_decay_events.replace(MADGRAPH_GRIDPACK_LOCATION, process_dir_fullDecay)
            print "Status of Gridpack: " + str(is_gen_from_gridpack())
            
    process_mindecay = processmaker('mindecay')
    process_dir_minDecay = new_process(process_mindecay)
    modify_run_card(process_dir=process_dir_minDecay, settings=extras)
    rewtcardmaker([runArgs.mass], [0.5], process_dir_minDecay) #### Make A Dummy Reweight Card to be used for reweighting with minDecay event generation
    modify_param_card(process_dir=process_dir_minDecay, params=params)
    MadGraphControl.MadGraphUtils.MADGRAPH_RUN_NAME = runName + "_minDecay"
    generate(process_dir=process_dir_minDecay, runArgs=runArgs)
    arrange_output(process_dir=process_dir_minDecay, runArgs=runArgs, lhe_version=3, saveProcDir=True)
    min_decay_events = process_dir_minDecay + '/Events/' + runName + '_minDecay/unweighted_events.lhe'

    if os.path.exists(min_decay_events + '.gz') == False and os.path.exists(min_decay_events) == False:

        print "ERROR: Event Generation with min decay chain failed. Aborting!"
        sys.exit(2)


    hack_status = lhe_hacker(lhe_minDecay  = min_decay_events, #process_dir_minDecay + '/Events/run_01_minDecay/unweighted_events.lhe',
                        lhe_fullDecay = full_decay_events, #process_dir_fullDecay + '/Events/run_01_fullDecay/unweighted_events.lhe',
                        vlq           = runArgs.vlqmode,
                        decay         = runArgs.decaymode)

    if hack_status :
        print " \n\n\n LHE Hacker was successful \n\n\n"
    else:
        print " \n\n\n LHE Hacker was NOT successful \n\n\n"
        sys.exit(1)

    placeback_status = False

    ME_script = open('script.txt','w')
    ME_script.write('''
    launch ''' + process_dir_minDecay + ''' -i
    reweight run_RWT
    ''')

    ME_script.flush()
    ME_script.close()
else:
    hack_status = False
    placeback_status = False

if hack_status and runArgs.dorwt:
    print "Starting Reweighting Sequence\n\n"
    subprocess.call('mkdir -p ' + process_dir_minDecay+'/Events/run_RWT/', shell=True)
    subprocess.call('cp unweighted_events.lhe ' + process_dir_minDecay+'/Events/run_RWT/', shell=True)
    tagnames = rewtcardmaker(M_grid, K_grid, process_dir_minDecay)
    did_it_work = False
    trial_count = 0
    while not did_it_work and trial_count < MAX_TRIAL:
        trial_count += 1
        reweight_now = subprocess.Popen(['python', me_exec, ' script.txt'])
        reweight_now.wait()
        sys.stdout.flush()
        try:
            subprocess.call('gunzip ' + process_dir_minDecay + '/Events/run_RWT/unweighted_events.lhe.gz', shell=True)
            print "found gzipped file in run_RWT and unzipped it"
        except:
            print "did not find gzipped file. Already unzipped?"
            pass
        for tagname in tagnames:
            did_it_work = False
            thisfile = open(process_dir_minDecay + '/Events/run_RWT/unweighted_events.lhe' , 'r')
            for line in thisfile:
                if "<weight id='" + tagname +"'" in line:
                    print tagname, " reweighting worked"
                    did_it_work = True
                    break
                else:
                    continue
            if not did_it_work:
                print tagname, " reweighting did not work. Retrying!"
                sys.stdout.flush()
                break
        sys.stdout.flush()

    if did_it_work:
        placeback_status = placeback(lhe_fullDecay  = full_decay_events,
                                     lhe_reweighted = process_dir_minDecay  + '/Events/run_RWT/unweighted_events.lhe')
    else:
        placeback_status = False

    if placeback_status:
        print "Placeback Successful.\n\n\n"
        if not runArgs.outputTXTFile == 'tmp_final_events':
            subprocess.call('cp tmp_final_events.events {}.events'.format(runArgs.outputTXTFile), shell=True)
        subprocess.call('tar -czf {}.events.tar.gz {}.events'.format(runArgs.outputTXTFile, runArgs.outputTXTFile), shell=True)
    else:
        print "Placeback Unsuccessful.\n\n\n"
        sys.exit(2)

if not (hack_status and placeback_status):
    subprocess.call('cp tmp_LHE_events.events {}.events'.format(runArgs.outputTXTFile), shell=True)
    subprocess.call('tar -czf {}.events.tar.gz {}.events'.format(runArgs.outputTXTFile, runArgs.outputTXTFile), shell=True)


runArgs.inputGeneratorFile = runArgs.outputTXTFile


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN and A15NNPDF23LO for VLQ single " + runArgs.vlqmode + " to " + runArgs.vlqprocess[2:] + " while produced via " + runArgs.prodmode
evgenConfig.keywords = ["BSM", "BSMtop", "exotic"]
evgenConfig.process = runArgs.vlqprocess
evgenConfig.contact =  ['avik.roy@cern.ch, fschenck@cern.ch']
