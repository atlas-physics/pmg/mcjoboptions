from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

mhh=3.000000e+03
whh=1.000000e-02

#---------------------------------------------------------------------------------------------------
# Setting mHH and WHH for param_card.dat
#---------------------------------------------------------------------------------------------------
params_bsm={'1560':mhh, #MHH
            '1561':whh} #WHH

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
params_mass={'25':1.250000e+02,
             '1560':mhh} #MH

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
#---------------------------------------------------------------------------------------------------
# Import the models
process = """
import model sm
import model HeavyHiggsTHDM
"""

# Define multi-particle representations
process += """
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
"""

# Define process to be simulated
process += """
generate p p > hh > h h
"""

# Define MadGraph outputs
process += """
output -f
"""

# Make sure Heavy Scalar PDG ID is known to TestHepMC:
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
1560
""")
pdgfile.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Filter efficiency is low
# Thus, setting the number of generated events to 100 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=10
nevents=20000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat:
#---------------------------------------------------------------------------------------------------
extras = { 'nevents':nevents,
           'ebeam1':beamEnergy,
           'ebeam2':beamEnergy,
           'fixed_ren_scale': 'True', # 
           'fixed_fac_scale': 'True', #
           'scale':mhh/2, #
           'dsqrt_q2fact1':mhh/2, #
           'dsqrt_q2fact2':mhh/2 #
         }

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Used values given in "params_bsm" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "params_mass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params={'MASS':params_mass,'BSM':params_bsm})

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------

modify_run_card(process_dir=process_dir,settings=extras)

# Print param and run cards
print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs)

runArgs.inputGeneratorFile=outputDS

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Di-Higgs production through 1000 GeV Heavy Higgs resonance which decays to hh > bbWW single lepton channel"
evgenConfig.keywords = ["hh","BSM", "BSMHiggs", "resonance", "bottom"]

evgenConfig.contact = ['JaeJin Hong <jae.jin.hong@cern.ch>']

#---------------------------------------------------------------------------------------------------

# Decaying hh to bbWW with Pythia8

#---------------------------------------------------------------------------------------------------

genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5", # bb decay
                            "25:addChannel = on 0.5 100 24 -24", # WW decay
                            "24:mMin = 0", # W minimum mass
                            "24:mMax = 99999", # W maximum mass
                            "23:mMin = 0", # Z minimum mass
                            "23:mMax = 99999", # Z maximum mass
                            ] # Let W to decay following SM BR

#---------------------------------------------------------------------------------------------------
# Filter for bbWW
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5]) # h > b b
filtSeq += ParentChildFilter("HWWFilter", PDGParent = [25], PDGChild = [24]) # h > W W

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
filtSeq += DecaysFinalStateFilter("OneLepFilter", PDGAllowedParents = [24, -24], NQuarks = 2, NChargedLeptons = 1, NNeutrinos = 1)

filtSeq.Expression = "HbbFilter and HWWFilter and OneLepFilter"