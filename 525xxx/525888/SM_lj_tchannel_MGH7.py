import os

from MadGraphControl.MadGraphUtils import *

# read out the lepton flavour from physics short
nevents = runArgs.maxEvents*1.6 if runArgs.maxEvents>0 else 1.6*evgenConfig.nEventsPerJob

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
job_option_name = get_physics_short()
JO_name_list = job_option_name.split('_')
input_order = JO_name_list[1]
lepton_flavour = JO_name_list[-2]
ptl_min, ptl_max = (pt for pt in JO_name_list[-1].split('to'))
lepton_flavours = ['el', 'mu', 'tau']

# make sure that the beam energy is set correctly
beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = '{0} + light jet t-channel production from lepton PDFs'.format(
    lepton_flavour)
evgenConfig.keywords    += ['SM']
evgenConfig.process     = 'pp -> l j'
evgenConfig.generators 	= ["MadGraph", "Herwig7", "EvtGen"]
evgenConfig.contact     = ["Gustavs Kehris <gustavs.kehris@cern.ch>, Daniel Buchin <daniel.buchin@cern.ch>, Michael Holzbock <michael.holzbock@cern.ch>"]
evgenConfig.tune       	= "H7.2-Default"

# --------------------------------------------------------------
# Produce lepton-quark scattering in MadGraph 
# --------------------------------------------------------------
my_process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~\n"""

if lepton_flavour == "el":
    my_process += """define p = g u c d s b e- u~ c~ d~ s~ b~ e+ \n"""
    my_process += """generate p p > e- j \n"""
    my_process += """add process p p > e+ j \n"""
    my_process += """output -f\n"""

elif lepton_flavour == "mu":
    my_process += """define p = g u c d s b mu- u~ c~ d~ s~ b~ mu+ \n"""
    my_process += """generate p p > mu- j \n"""
    my_process += """add process p p > mu+ j \n"""
    my_process += """output -f\n"""

elif lepton_flavour == "tau":
    my_process += """define p = g u c d s b ta- u~ c~ d~ s~ b~ ta+ \n"""
    my_process += """generate p p > ta- j \n"""
    my_process += """add process p p > ta+ j \n"""
    my_process += """output -f\n"""
else:
    raise RuntimeError("This is not a lepton flavour: {:s}.".format(lepton_flavour))

process_dir = new_process(my_process)
settings = {'lhe_version': '3.0',
            'lpp1': 1,
            'lpp2': 1,
            'ebeam1': beamEnergy,
            'ebeam2': beamEnergy,
            'polbeam1': 0.0,
            'polbeam2': 0.0,
            'pdlabel1': "lhapdf",
            'pdlabel2': "lhapdf",
            'lhaid': 82400,
            'ickkw': 0,
            'sde_strategy': 1,
            'ptj': 20,
            'nevents': nevents}
settings['ptl'] = int(ptl_min)
if ptl_max not in ['', 'Inf']: settings['ptlmax'] = int(ptl_max)

modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=settings)

# No parameters to change for this SM process
parameters = {}
modify_param_card(process_dir=process_dir,params=parameters)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

outputDS = arrange_output(runArgs=runArgs, process_dir=process_dir, lhe_version=3, saveProcDir=True)
runArgs.inputGeneratorFile = outputDS + ".events"

# --------------------------------------------------------------
# Herwig PS
# --------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")

pdf_order = "NLO"
me_order  = input_order

# configure Herwig7 set EventGenerator:EventHandler:LuminosityFunction:Energy 6500.*GeV
Herwig7Config.add_commands("""
set /Herwig/Generators/EventGenerator:UseStdout No
### Mismatching of UE and hard process can occur quite often in muon initiated processes
set /Herwig/Generators/EventGenerator:MaxErrors 100000
# set /Herwig/Generators/EventGenerator:EventHandler:LuminosityFunction:Energy {} GeV

### We are dealing with lepton PDFs!
set /Herwig/Partons/RemnantDecayer:AllowLeptons Yes

### Necessary commands when running a process with intial state leptons
set /Herwig/Particles/e-:PDF /Herwig/Partons/NoPDF
set /Herwig/Particles/e+:PDF /Herwig/Partons/NoPDF
""".format(beamEnergy))

Herwig7Config.me_pdf_commands(order=pdf_order, name="LUXlep-NNPDF31_nlo_as_0118_luxqed")   #LUXlep-NNPDF31_nlo_as_0118_luxqed  #NNPDF23_lo_as_0130_qed (247000)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename = runArgs.inputGeneratorFile, me_pdf_order = pdf_order)

Herwig7Config.add_commands("""
### MadGraph specific Herwig options
set /Herwig/EventHandlers/LHEReader:QNumbers Yes
set /Herwig/EventHandlers/LHEReader:Decayer /Herwig/Decays/Mambo

### Setting shower PDFs to LUXlep likely necessary as LUXlep is the only PDF set including leptons!
set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/Hard{0}PDF
set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/Hard{0}PDF

#####
### These commands are needed to drastically reduce the ratio of the
### "Remnant extraction failed in ShowerHandler::cascade() from primary interaction"
### error.
set /Herwig/Shower/ShowerHandler:PDFARemnant /Herwig/Partons/Hard{0}PDF
set /Herwig/Shower/ShowerHandler:PDFBRemnant /Herwig/Partons/Hard{0}PDF
#####

set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/Hard{0}PDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/Hard{0}PDF
set /Herwig/EventHandlers/LHEReader:WeightWarnings false
""".format(pdf_order))

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
