import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'Zll + 2 b-jets, MG Pythia, 4FNS'
evgenConfig.contact = ["yi.yu@cern.ch"]
evgenConfig.keywords += ['Z','jets']
evgenConfig.generators += ["aMcAtNlo"]

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = 1.1 * runArgs.maxEvents if runArgs.maxEvents>0 else 1.1 * evgenConfig.nEventsPerJob

#Madgraph run card and shower settings
# Shower/merging settings  
maxjetflavor=4
parton_shower='PYTHIA8'

# MG Particle cuts
# mllcut=40 

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm
    define p = g u c d s u~ c~ d~ s~
    define j = p
    define l+ = e+ mu+
    define l- = e- mu-
    generate p p > l+ l- b b~ [QCD]
    output -f"""

    process_dir = str(new_process(process))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':321500, # NNPDF31 (pch, 4fns, nlo)
    'pdf_variations':[321500, 322100], # list of pdfs ids for which all variations (error sets) will be included as weights 
    'alternative_pdfs':[322100], # NNPDF40 (pch, 4fns, nlo)
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
    }

#Fetch default run_card.dat and set parameters
settings = {
            'maxjetflavor'  : int(maxjetflavor),
            'parton_shower' : parton_shower,
            'nevents'       : int(nevents),
            'jetradius'     : 0.4,
            'ptj'           : 8,
            'etaj'          : 10,
        }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(required_accuracy=0.0001,process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


