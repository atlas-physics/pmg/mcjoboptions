#! /usr/bin/env python

import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('MadGraph23Control_BulkRS')

## to run systematic variations on the fly, new in MG2.6.2 (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
## the following setting is needed only if the default base fragments can not be used
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[247000], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[246800,13205,25000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

def BulkRS_Generation(run_number=100000,
                      gentype="WW",
                      decaytype="lvqq",
                      scenario="c10",
                      stringy="m1000",
                      mass=1000,
                      rand_seed=1234,
                      beamEnergy=6500.,
                      scalevariation=1.0,
                      alpsfact=1.0,
                      pdf='nn23lo',
                      lhaid=247000):



    # -----------------------------------------------------------------------
    # Need to arrange several things:
    # -- proc_card.dat, this will be determined mostly by the run number.
    #    once we have this, we can also initialize the process in madgraph
    #
    # -- param_card.dat, this is mostly invariant, but does depend on the model scenario
    #
    # -- run_card.dat, this is also mostly invariant
    #
    # At that point we can generate the events, arrange the output, and cleanup.
    # -----------------------------------------------------------------------
    
    # -----------------------------------------------------------------------
    # proc card
    #
    # first, figure out what process we should run
    processes=[]
    pythiachans=[]
    if gentype == "WW" or gentype == "ww":
        if decaytype == "lvlv" or decaytype == "llvv":
            processes.append("p p > hh, (hh > w+ w-, w+ > l+ vl, w- > l- vl~)")
        elif decaytype == "jjjj" or decaytype == "qqqq":
            processes.append("p p > hh, (hh > w+ w-, w+ > jprime jprime, w- > jprime jprime)")
        elif decaytype == "lvqq" or decaytype == "lvjj":
            processes.append("p p > hh, (hh > w+ w-, w+ > jprime jprime, w- > l- vl~)")
            processes.append("p p > hh, (hh > w+ w-, w- > jprime jprime, w+ > l+ vl)")
        else:
            mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
            return -1,pythiachans

    elif gentype == "ZZ" or gentype == "zz":
        if decaytype == "llll":
            processes.append("p p > hh, (hh > z z, z > l+ l-, z > l- l+)")
        elif decaytype == "llvv" or gentype == "vvll":
            processes.append("p p > hh, (hh > z z, z > l+ l-, z > vl vl~)")
        elif decaytype == "jjjj" or decaytype == "qqqq":
            processes.append("p p > hh, (hh > z z, z > jprime jprime, z > jprime jprime)")
        elif decaytype == "llqq" or decaytype == "lljj":
            processes.append("p p > hh, (hh > z z, z > jprime jprime, z > l- l+)")
        elif decaytype == "vvqq" or decaytype == "vvjj" or decaytype == "qqvv" or decaytype == "jjvv":
            processes.append("p p > hh, (hh > z z, z > jprime jprime, z > vl vl~)")
        else:
            mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
            return -1,pythiachans

    elif gentype == "HH" or gentype == "hh":
        processes.append("p p > hh, (hh > h h)")

        # let pythia decay the Higgs' to get the right BR's
        if decaytype == "qqqq" or decaytype == "jjjj":
            pythiachans.append("25:oneChannel = on 0.569 100 5 -5 ")
            pythiachans.append("25:addChannel = on 0.0287 100 4 -4 ")
        elif decaytype == "bbbb":
            pythiachans.append("25:oneChannel = on 0.569 100 5 -5 ")
        else:
            mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
            return -1,pythischans

    elif gentype == "tt":
        processes.append("p p > hh, (hh > t t~, t > jprime l+ vl, t~ > jprime l- vl~)")
        processes.append("p p > hh, (hh > t t~, t > jprime jprime jprime, t~ > jprime jprime jprime)")
        processes.append("p p > hh, (hh > t t~, t > jprime jprime jprime, t~ > jprime l- vl~)")
        processes.append("p p > hh, (hh > t t~, t > jprime l+ vl, t~ > jprime jprime jprime)")

        pythiachans.append("")

    else:
        mglog.error('Could not configure process for %s > %s' % (gentype, decaytype))
        return -1,pythiachans
            
    # Figure out how to modify the template.
    processstring=""
    processcount=1
    for i in processes:
        if processstring=="":
            processstring="generate %s QED=99 QCD=99 @%d " % (i, processcount)
        else:
            processstring+="\nadd process %s QED=99 QCD=99 @%d " % (i, processcount)
        processcount+=1

    #---------------------------------------------------------------------------
    # Number of Events
    #---------------------------------------------------------------------------
    safefactor = 1.5 
    if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  
        nevents = int(runArgs.maxEvents*safefactor)
    else:
        nevents = int(evgenConfig.nEventsPerJob*safefactor)

    # Write the proc card
    proc_card_name = "proc_card_mg5.dat"
    fcard = open(proc_card_name,'w')

    process_str="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False
    set gauge unitary
    set complex_mass_scheme False
    import model sm
    
    # Define multiparticle labels
    define p = g u c d s u~ c~ d~ s~ b b~
    define j = g u c d s u~ c~ d~ s~ b b~
    define jprime = g u c d s u~ c~ d~ s~ b b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    
    import model SMRS_Decay
    
    %s

    output -f
    """ % processstring

    fcard.write(process_str)
    fcard.close()

    # Generate the new process!
    thedir = new_process(process_str)
    # -----------------------------------------------------------------------


    # -----------------------------------------------------------------------
    # run card
    ## generator cuts
    extras = {
        'ptj':"0",
        'ptb':"0",
        'pta':"0",
        'ptl':"0",
        'etaj':"-1",
        'etab':"-1",
        'etaa':"-1",
        'etal':"-1",
        'drjj':"0",
        'drbb':"0",
        'drll':"0",
        'draa':"0",
        'drbj':"0",
        'draj':"0",
        'drjl':"0",
        'drbl':"0",
        'dral':"0",
        'nevents': nevents,
    }

    ## TODO: are the following needed, since fixed_ren_scale and fixed_fac_scale are F by default?
    extras["scale"] = mass
    extras["dsqrt_q2fact1"] = mass
    extras["dsqrt_q2fact2"] = mass

    ## no need to specify pdfsets, if using the base fragment
    ## (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
    #extras["pdlabel"]=pdf
    #extras["lhaid"]=lhaid

    str_run_card='run_card.dat'
    # -----------------------------------------------------------------------


    # -----------------------------------------------------------------------
    # param card needs to be built with correct widths
    ## couplings
    params={}
    FRBLOCK={}
    FRBLOCK["c"]  = float(scenario[1:])/10.
    params['FRBLOCK']=FRBLOCK
    ## masses, decays
    masses={
        "25"  : 125., ## SM Higgs
        "39" : int(mass),  ## new particles
    }
    decays={
        "39": "DECAY 39 Auto",
    }
    params['mass'] = masses
    params['decay'] = decays

    str_param_card='param_card.dat'

    # make run card
    modify_param_card(process_dir=thedir,params=params)

    # update run card
    modify_run_card(process_dir=thedir,runArgs=runArgs,settings=extras)
    # -----------------------------------------------------------------------

    # -----------------------------------------------------------------------
    # Generation
    # See if things are where we expected them to be...
    if 1==thedir:
        mglog.error('Error in process generation!')
        return -1,''
    if not os.access(thedir,os.R_OK):
        mglog.error('Could not find process directory '+thedir+' !!  Bailing out!!')
        return -1,''
    else:
        mglog.info('Using process directory '+thedir)

    if generate(process_dir=thedir,grid_pack=False,runArgs=runArgs):
        mglog.error('Error generating events!')
        return -1,pythiachans
    # -----------------------------------------------------------------------

    # -----------------------------------------------------------------------
    # Cleanup
    outputDS=""
    try:
        arrange_output(process_dir=thedir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

    except:
        mglog.error('Error arranging output dataset!')
        return -1,pythiachans

    keepOutput=True
    if not keepOutput:
        mglog.info('Removing process directory...')
        shutil.rmtree(thedir,ignore_errors=True)

    mglog.info('All done generating events!!')
    return outputDS,pythiachans
    # -----------------------------------------------------------------------


# multi-core running, if allowed!
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts


## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

# decode dataset short name, 
# new r21 format:
# mc.MGPy8EG_A14NNPDF23LO_RS_G_WW_qqqq_c10_m3000
graviton=shortname[3]
gravdecay=shortname[4]
VVdecay=shortname[5]
scenario=shortname[6]
mass=int(shortname[7][1:])

# PDF information, in MadGraph's PDF naming scheme.  
# Should probably extract this from shortname[1] in some fancy way.
# For now, specify lhapdf and the ID so that madgraph doesn't get 
# confused trying to interpret some shorthand name (which it can).
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=247000 # NNPDF23_lo_as_0130_qed

# Run MadGraph!
runArgs.inputGeneratorFile,pythiachans=BulkRS_Generation(999,
                                                         #runArgs.runNumber,     # run number
                                                         gravdecay,             # How the G* decays
                                                         VVdecay,               # How the SM bosons decay
                                                         scenario,              # k/mPl, for instance
                                                         ("m%d" % mass),        # dataset tag (production and decay not needed here)
                                                         mass,                  # Graviton mass
                                                         runArgs.randomSeed,    # random seed
                                                         runArgs.ecmEnergy/2.,  # beam energy
                                                         1.0,                   # scale variation
                                                         1.0,                   # PS variation
                                                         pdf,                   # PDF information
                                                         lhaid)

# Some more information
evgenConfig.description = "Bulk RS Graviton Signal Point"
evgenConfig.keywords = ["exotic", "BSM", "RandallSundrum", "warpedED", "graviton"]
evgenConfig.contact = ["yizhou.cai@cern.ch"]
# Obtained from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_BulkRS_tt.py
# Following https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/513xxx/513486/MadGraphControl_BulkRS.py
evgenConfig.process = "pp>G*>%s>%s" % (gravdecay,VVdecay) # e.g. pp>G*>WW>qqqq

# Finally, run the parton shower...
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# ...and pull in MadGraph-specific stuff
include("Pythia8_i/Pythia8_MadGraph.py")

# Make Pythia decay the Higgs, if needed
genSeq.Pythia8.Commands += pythiachans
