import os

from AthenaCommon.Include import include
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short 
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment



#reading the name of the other file that contains the info of the process

phys_short = get_physics_short()
model_string = phys_short.split('_')[2]
mhnl_input = phys_short.split('_')[2].replace("HNL","")
mhnl_str= ''
for entry in mhnl_input:
    if entry =='p':
        mhnl_str=mhnl_str+'.'
    else:
        mhnl_str =mhnl_str+entry
mhnl = float(mhnl_str)
#mhnl scan1:[2,3]

channel = phys_short.split('_')[4]


lep = [None]*3
digit = 0

for i in range(3):
    if channel[digit:digit+2]=='mu':
        lep[i] = 'mu'
        digit+=2
    elif channel[digit:digit+2]=='ta':
        lep[i] = 'ta'
        digit+=2
    elif channel[digit:digit+1]=='e':
        lep[i] = 'e'
        digit+=1
    elif channel[digit:digit+2]=='pi':
        lep[i] = 'pi'
        digit+=2
    
    else:
        evgenLog.error("lepton %i does not have correct type, "%i)

if len(channel[digit:])!=0:
    evgenLog.error("channel name too long, check")

ctaustring = phys_short.split('_')[3].replace("ctau","") # ctau is the lifetime of the HNL
# if int(str(ctaustring)) == 0:
#     ctau = -1.0
# elif int(str(ctaustring[:1])) != 0:
#     ctau = float(str(ctaustring))
# else:
ctau_str= ''
for entry in ctaustring:
    if entry =='p':
        ctau_str=ctau_str+'.'
    else:
        ctau_str =ctau_str+entry
ctau = float(ctau_str)


    #ctau = 0.1*float(str(ctaustring))
if ctau > 0:
    tofoption = 0
else:
    tofoption= -1

evgenLog.info('physics short: %s' %phys_short)

evgenLog.info('Processing model with HNL mass and average lifetime: (mhnl, ctau) = (%e,%e)' %(mhnl, ctau))

#genline0 = "set auto_convert_model T"  #to install de Gen3Mass model

genline1 = "import model SM_HeavyN_Meson_NLO"
genline2 = "define p = u c d s u~ c~ d~ s~ g"
genline3 = "define j = p"
#genline4 = "define mue = ta+ ta- mu+ mu- e+ e-"
#genline5 = "define nx = vt vt~ vm vm~ ve ve~"
genline4 = "define mu = mu+ mu-"
genline5 = "define e = e+ e-"
genline6 = "define ta = ta+ ta-"
genline7 = "define vvm = vm vm~"
genline8 = "define vve = ve ve~"
genline9 = "define vvt = vt vt~"
genline10 = "define vv =vm vm~ ve ve~ vt vt~"
genline11 ="define pix = pi+ pi-"
# Next if takes into account if you have a tau in any of the 3 spots, if you have one at 1 and one at 2, and if you don't have any

if lep[0]=="mu" and lep[1]=="mu" and lep[2]=="pi":
    genline12 = "generate p p > mu n1, n1 > mu pix"
if lep[0]=="e" and lep[1]=="e" and lep[2]=="pi":
    genline12 = "generate  p p > e n1, n1 > e pix"
if lep[0]=="ta" and lep[1]=="ta" and lep[2]=="pi":
    genline12 = "generate  p p > ta n1, n1 > ta pix" 
if lep[0]=="mu" and lep[1]=="e" and lep[2]=="pi":
    genline12 = "generate p p > mu n1, n1 > e pix" 
if lep[0]=="e" and lep[1]=="mu" and lep[2]=="pi":
    genline12 = "generate p p > e n1, n1 > mu pix" 
if lep[0]=="ta" and lep[1]=="e" and lep[2]=="pi":
    genline12 = "generate  p p > ta n1, n1 > e pix"
if lep[0]=="e" and lep[1]=="ta" and lep[2]=="pi":
    genline12 = "generate  p p > e n1, n1 > ta pix"
if lep[0]=="ta" and lep[1]=="mu" and lep[2]=="pi":
    genline12 = "generate  p p > ta n1, n1 > mu pix"
if lep[0]=="mu" and lep[1]=="ta" and lep[2]=="pi":
    genline12 = "generate  p p > mu n1, n1 > ta pix "

genline13 = "output -f"
gen_process = genline1+"\n"+genline2+"\n"+genline3+"\n"+genline4+"\n"+genline5+"\n"+genline6+"\n"+genline7+"\n"+genline8+"\n"+genline9+"\n"+genline10+"\n"+genline11+"\n"+genline12+"\n"+genline13

# General settings
gridpack_mode=False


if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=500*evt_multiplier

run_settings = {'lhe_version':'3.0',
                #'pdlabel'    : "'lhapdf'",
                #'lhaid'      : 260000,
                'ickkw'      : '0',
                'maxjetflavor':4, # 4 flavor scheme
                'ptl':10,
                'bwcutoff': 20.0,
                'time_of_flight': tofoption
               
}

#run_settings['use_syst']='F'
run_settings['nevents'] = nevents
run_settings['time_of_flight']= 6.66666666667e-12


#run_settings['SMALL_WIDTH_TREATMENT']=1e-12
# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# this is a dictionary
params = {}

params['mass']={'72':mhnl,'74':1e10,'76':1e10}#, '15':1.777e0}
#params['numixing']={'1':0,'2':0,'3':0,'4':0,'5':0,'6':0,'7':0,'8':0,'9':0}

if ctau > 0:
    width= 197.4635212e-15/ctau # hbar*c [GeV*mm] / c*tau [mm]                 
    params['DECAY']={'72':'DECAY 72 {} # WN1'.format(width)} #we will need to add decay of tau
else:
    params['DECAY']={'72':'DECAY  72 Auto # WN1'}

#width_N= 1.00e-6 
#params['DECAY']={'72':'DECAY 72 {} # WN1'.format(width_N)}
#width_tau= 1e-6 #marie uses e-12 
#params['DECAY']={'15':'DECAY 15 {} # WTA'.format(width_tau)}
params['sminputs']={'1':1.325070e+02,'2':1.166390e-05 ,'3':1.180000e-01}
params['numixing']={'1':1,'2':0,'3':0,'4':1,'5':0,'6':0,'7':1,'8':0,'9':0}


modify_param_card(process_dir=process_dir,params=params)


# Generate the events                                                          
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
print('grid_pack?',gridpack_mode)

if ctau>=1:
    min_disp = 2.0 # [mm]fiducial volume is 4-300mm                            
    tau = (min_disp)/(1000.0*3e8) # in seconds, [mm] / [mm/m] * [m/s]          
    #Add min displacement cut                                                  
    add_lifetimes(process_dir=process_dir,threshold=tau)


# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Set the evgen metadata
evgenConfig.description = 'HNL %s (with matching), m_hnl = %s GeV, channel = %s '%(model_string, mhnl, channel)
evgenConfig.keywords = ["exotic","BSM","neutrino"]
evgenConfig.contact = ["Marzieh Bahmani <marzieh.bahmani@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 400000000 #in mm

